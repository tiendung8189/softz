﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Main;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms, Dialogs, ActnList,
  Menus, ComCtrls, ExtCtrls, isEnv, AdvMenus, Jpeg, ExtDlgs, RzGroupBar, StdCtrls,
  RzSplit, fcStatusBar, ADODb, ImgList, fcImager, RzPanel, ToolWin, SystemCriticalU,
  AdvToolBar, NativeXml;

type
  TFrmMain = class(TForm)
    MyActionList: TActionList;
    CmdQuit: TAction;
    CmdSetpass: TAction;
    CmdAbout: TAction;
    CmdParams: TAction;
    CmdTonkhoDK: TAction;
    CmdDmnhom: TAction;
    CmdDmvt: TAction;
    CmdNhap: TAction;
    CmdXuat: TAction;
    CmdDmncc: TAction;
    CmdDmkhac: TAction;
    CmdKhoaso: TAction;
    CmdDmkh: TAction;
    CmdThu: TAction;
    CmdChi: TAction;
    CmdBc: TAction;
    CmdBanle: TAction;
    CmdNhapkhac: TAction;
    CmdXuatkhac: TAction;
    CmdDmkhac3: TAction;
    CmdForceDropDown: TAction;
    CmdNhaptra: TAction;
    CmdXuattra: TAction;
    CmdChuyenkho: TAction;
    CmdDmkho: TAction;
    CmdInlabel: TAction;
    CmdChikhac: TAction;
    ImgLarge: TImageList;
    CmdHelp: TAction;
    CmdSetBkgr: TAction;
    CmdClearBkgr: TAction;
    CmdBkgrOption: TAction;
    PicDlg: TOpenPictureDialog;
    CmdKiemke: TAction;
    CmdSetDateTime: TAction;
    CmdDmVIP: TAction;
    CmdDmmau: TAction;
    CmdDmsize: TAction;
    CmdDdhNCC: TAction;
    CmdCounter: TAction;
    CmdLock: TAction;
    CmdFaq: TAction;
    CmdMyCustomer: TAction;
    PopBkGr: TAdvPopupMenu;
    Gnhnhnn1: TMenuItem;
    Xahnhnn1: TMenuItem;
    N30: TMenuItem;
    Canhchnh2: TMenuItem;
    PopFunc: TAdvPopupMenu;
    ItemCat: TMenuItem;
    Outlook2: TMenuItem;
    TaskList2: TMenuItem;
    N1: TMenuItem;
    ItemCloseAll: TMenuItem;
    PopKinhdoanh: TAdvPopupMenu;
    PopDanhmuc: TAdvPopupMenu;
    PopNhansu: TAdvPopupMenu;
    Nhmhngha1: TMenuItem;
    Danhmchngha2: TMenuItem;
    Danhschnhcungcp1: TMenuItem;
    Khchhngthnthit1: TMenuItem;
    Danhmckhohng1: TMenuItem;
    Danhmcmu2: TMenuItem;
    Danhmcsize2: TMenuItem;
    Ccquyc2: TMenuItem;
    Danhmckhc2: TMenuItem;
    CmdDmdialy: TAction;
    Vtral1: TMenuItem;
    CoolBar1: TCoolBar;
    ToolBar1: TToolBar;
    ToolButton3: TToolButton;
    ToolBar2: TToolBar;
    BtKinhdoanh: TToolButton;
    BtNhansu: TToolButton;
    BtDanhmuc: TToolButton;
    ToolButton2: TToolButton;
    Panel1: TPanel;
    RzSizePanel1: TRzSizePanel;
    GbFunc: TRzGroupBar;
    GrHanghoa: TRzGroup;
    GrCongno: TRzGroup;
    GrHethong: TRzGroup;
    PaBkGr: TPanel;
    BkGr: TfcImager;
    PaDesc: TPanel;
    LbDesc2: TLabel;
    Bevel3: TBevel;
    LbDesc1: TLabel;
    Bevel2: TBevel;
    PopDulieu: TAdvPopupMenu;
    PopHethong: TAdvPopupMenu;
    PopTrogiup: TAdvPopupMenu;
    Tnkhouk1: TMenuItem;
    N16: TMenuItem;
    Ktthc2: TMenuItem;
    Danhschquythungn2: TMenuItem;
    N17: TMenuItem;
    imtkhu2: TMenuItem;
    Hngdnsdng2: TMenuItem;
    N21: TMenuItem;
    Cchipthnggp1: TMenuItem;
    Danhschkhchhng1: TMenuItem;
    N31: TMenuItem;
    Giithiu2: TMenuItem;
    Status: TfcStatusBar;
    N22: TMenuItem;
    CmdTygia: TAction;
    Tgi1: TMenuItem;
    CmdDondhKH: TAction;
    Kimkkho2: TMenuItem;
    N13: TMenuItem;
    nthngkhchhng1: TMenuItem;
    nthng2: TMenuItem;
    Vnchuynnib1: TMenuItem;
    Xutkhc2: TMenuItem;
    Xuttr2: TMenuItem;
    Phiuxut2: TMenuItem;
    N4: TMenuItem;
    Nhpkhc2: TMenuItem;
    Nhptr2: TMenuItem;
    Phiunhp2: TMenuItem;
    CmdNhapDC: TAction;
    CmdXuatDC: TAction;
    N20: TMenuItem;
    iuchnhphiunhp1: TMenuItem;
    iuchnhphiuxut1: TMenuItem;
    CmdDmvtBo: TAction;
    CmdNhaptrabl: TAction;
    CmdHoadonGTGT: TAction;
    N25: TMenuItem;
    CmdThuHT: TAction;
    CmdChiHT: TAction;
    CmdThuThungan: TAction;
    CmdChiThungan: TAction;
    CmdKetchuyen: TAction;
    CmdThukhac: TAction;
    GrBanle: TRzGroup;
    CmdKhuyenmai: TAction;
    CmdReconnect: TAction;
    CmdTonkho: TAction;
    BtInBarcode: TToolButton;
    CmdExport: TAction;
    CmdKhuyenmai3: TAction;
    CmdNhaptrabl2: TAction;
    CmdLoaiVipCT: TAction;
    ChitkhutheoloithVIP1: TMenuItem;
    CmdDmTaikhoan: TAction;
    CmdThu2: TAction;
    CmdChi2: TAction;
    N26: TMenuItem;
    XutExcel1: TMenuItem;
    BtSanxuat: TToolButton;
    PopSanxuat: TAdvPopupMenu;
    Khas1: TMenuItem;
    GrPhanQuyen: TRzGroup;
    CmdListUser: TAction;
    CmdListGroup: TAction;
    CmdFunctionRight: TAction;
    CmdReportRight: TAction;
    CmdFlexsible: TAction;
    CmdCustomGrid: TAction;
    CmdDictionary: TAction;
    CmdFuncAdmin: TAction;
    CmdRepAdmin: TAction;
    CmdAdmin: TAction;
    CmdDmKhNcc: TAction;
    CmdLoaiVIP: TAction;
    LoithVIP1: TMenuItem;
    CmdLoaiVip2: TAction;
    CmdLoaiVipCt2: TAction;
    LoithVIPS1: TMenuItem;
    ChitkhutheoloithVIPS1: TMenuItem;
    Khchhng1: TMenuItem;
    Nhcungcp2: TMenuItem;
    CmdReportEncrypt: TAction;
    Inmvch2: TMenuItem;
    CmdExOption: TAction;
    CmdImOption: TAction;
    CmdConfig: TAction;
    Cuhnhhthng1: TMenuItem;
    N12: TMenuItem;
    CmdDmNganhang: TAction;
    Ngnhng1: TMenuItem;
    BtKetoan: TToolButton;
    CmdCongnoKH: TAction;
    CmdCongnoNCC: TAction;
    CmdDmHotro: TAction;
    Danhmchtr1: TMenuItem;
    CmdPhieuQuatang: TAction;
    GrGiathanh: TRzGroup;
    CmdDmChiphiPOM: TAction;
    CmdDmhhNhom_POM: TAction;
    CmdDmHesoDodam: TAction;
    CmdDmHesoDungtich: TAction;
    CmdDmTP: TAction;
    CmdDmNVL: TAction;
    CmdDmBTP: TAction;
    PopKetoan: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem11: TMenuItem;
    MenuItem12: TMenuItem;
    MenuItem13: TMenuItem;
    MenuItem14: TMenuItem;
    MenuItem15: TMenuItem;
    MenuItem16: TMenuItem;
    Danhmcchun1: TMenuItem;
    Danhmckinhdoanh1: TMenuItem;
    Danhmcsnxut1: TMenuItem;
    Danhmcnhns1: TMenuItem;
    N10: TMenuItem;
    N29: TMenuItem;
    N32: TMenuItem;
    N33: TMenuItem;
    N34: TMenuItem;
    NhmNVLBTPTP1: TMenuItem;
    Nguynvtliu1: TMenuItem;
    Bnthnhphm1: TMenuItem;
    hnhphm1: TMenuItem;
    N6: TMenuItem;
    Hsm1: TMenuItem;
    Hsdungtch1: TMenuItem;
    N8: TMenuItem;
    Danhschchiph1: TMenuItem;
    Danhmchtr2: TMenuItem;
    Danhmckton1: TMenuItem;
    NhpXutTn1: TMenuItem;
    nthng1: TMenuItem;
    Bnl1: TMenuItem;
    Hanbnl1: TMenuItem;
    Nhptrbnlhan1: TMenuItem;
    Nhptrbnl1: TMenuItem;
    N14: TMenuItem;
    Chitkhukhuynmi2: TMenuItem;
    Khuynmitheohnghab2: TMenuItem;
    Khuynmitheohanhngha2: TMenuItem;
    N18: TMenuItem;
    hqutng1: TMenuItem;
    CmdDmPhongban: TAction;
    CmdDmChucvu: TAction;
    CmdDmNhomlv: TAction;
    CmdDmVangmat: TAction;
    CmdDmPhucap: TAction;
    CmdDmQuatrinh: TAction;
    CmdDmHopdong: TAction;
    CmdDmDKKhambenh: TAction;
    chcphngban1: TMenuItem;
    ChcdanhChcv1: TMenuItem;
    Nhmlmvic1: TMenuItem;
    Loiphcp1: TMenuItem;
    Loihpnglaong1: TMenuItem;
    Ldovngmt1: TMenuItem;
    Loiqutrnhlmvic1: TMenuItem;
    Ningkkhmbnh1: TMenuItem;
    CmdDmHotroSX: TAction;
    CmdDmHotroKT: TAction;
    N35: TMenuItem;
    CmdDmHotroNS: TAction;
    Danhmchtr3: TMenuItem;
    CmdhethongTaikhoanKT: TAction;
    CmdDmChiphiKT: TAction;
    Danhmctikhon1: TMenuItem;
    HthngtikhonKton1: TMenuItem;
    N36: TMenuItem;
    Danhmchtr4: TMenuItem;
    hu1: TMenuItem;
    Chi1: TMenuItem;
    Hangitrgiatng1: TMenuItem;
    CmdBcSX: TAction;
    CmdBcKD: TAction;
    Boco1: TMenuItem;
    Bocosnxut1: TMenuItem;
    CmdBcNS: TAction;
    Boconhns1: TMenuItem;
    N2: TMenuItem;
    CmdBcKT: TAction;
    Bocokton1: TMenuItem;
    nkho1: TMenuItem;
    N3: TMenuItem;
    CmdDmnv: TAction;
    CmdProfile: TAction;
    CmdQuatrinh: TAction;
    CmdKhenthuong: TAction;
    CmdCongtac: TAction;
    CmdGiadinh: TAction;
    CmdThietbi: TAction;
    CmdDaotao: TAction;
    CmdThoiviec: TAction;
    CmdTuyendungHoso: TAction;
    CmdTuyendungKetqua: TAction;
    Hsnhnvin1: TMenuItem;
    Lchs1: TMenuItem;
    Qutrnhlmvic1: TMenuItem;
    KhenthngKlut1: TMenuItem;
    incngoi1: TMenuItem;
    Gitisnthitb1: TMenuItem;
    oto1: TMenuItem;
    Nghvic1: TMenuItem;
    uyndng1: TMenuItem;
    Hsngvin1: TMenuItem;
    Ktquphngvn1: TMenuItem;
    Bocotuyndng1: TMenuItem;
    N9: TMenuItem;
    Boconhns2: TMenuItem;
    CmdTMDT: TAction;
    Linktsnthngmaiint1: TMenuItem;
    CmdDmCalamviec: TAction;
    CmdDmNgayle: TAction;
    CmdBangcong: TAction;
    CmdVangmat: TAction;
    CmdPhepnam: TAction;
    CmdLichlamviec: TAction;
    CmdThaisan: TAction;
    CmdDmMCC: TAction;
    CmdVantay: TAction;
    CmdCongtho: TAction;
    CmdLaydulieucong: TAction;
    N11: TMenuItem;
    Mychmcng1: TMenuItem;
    Mychmcng2: TMenuItem;
    CpmthLyvntay1: TMenuItem;
    CmdBangluong: TAction;
    Githnh1: TMenuItem;
    Quytrnhsnxut1: TMenuItem;
    N19: TMenuItem;
    CmdGiathanh: TAction;
    CmdBcNSTuyendung: TAction;
    CmdBcNSCong: TAction;
    CmdDmKetoan: TAction;
    CmdDmNhansu: TAction;
    CmdDmSanxuat: TAction;
    CmdDmKinhdoanh: TAction;
    CmdQuytrinhSX: TAction;
    CmdHrLichsu: TAction;
    CmdHrThietbi: TAction;
    CmdHrTuyendung: TAction;
    BtBaocao: TToolButton;
    PopBaocao: TAdvPopupMenu;
    Bocokinhdoanh1: TMenuItem;
    Bocosnxut2: TMenuItem;
    N23: TMenuItem;
    Boconhns3: TMenuItem;
    Bocotuyndng2: TMenuItem;
    Bocochmcng1: TMenuItem;
    N24: TMenuItem;
    Bocokton2: TMenuItem;
    CmdUnlockChungtu: TAction;
    MkhaPhiuangsa1: TMenuItem;
    CmdTbMaychamcong: TAction;
    CmdDONDATHANG: TAction;
    CmdNHAPXUATKHO: TAction;
    CmdBANLEMENU: TAction;
    CmdGrThu: TAction;
    CmdGrChi: TAction;
    CmdDanhmuc: TAction;
    CmdDmTaisan: TAction;
    Danhmctisn1: TMenuItem;
    CmdDmvtTS: TAction;
    CmdDmnhomTS: TAction;
    CmdDmnccTS: TAction;
    CmdDmPhongbanTS: TAction;
    CmdDmnvTS: TAction;
    CmdDmmauTS: TAction;
    CmdDmSizeTS: TAction;
    CmdDmHotroTS: TAction;
    Danhmctisn2: TMenuItem;
    Nhmtisn1: TMenuItem;
    N27: TMenuItem;
    Nhcungcp1: TMenuItem;
    N28: TMenuItem;
    PhngbanBphn1: TMenuItem;
    Nhnvin1: TMenuItem;
    N37: TMenuItem;
    Mu1: TMenuItem;
    Size1: TMenuItem;
    Htr1: TMenuItem;
    CmdDanhmucNhomNganhFB: TAction;
    CmdDanhmucNhomNganhFB1: TMenuItem;
    CmdDanhmucNhomNganhFB2: TMenuItem;
    CmdDanhmucFB: TAction;
    CmdDanhmuchotroFB: TAction;
    DanhmchtrFB1: TMenuItem;
    CmdDanhmucBAN: TAction;
    Danhmcbn2: TMenuItem;
    N38: TMenuItem;
    CmdDanhmucChiPhiFB: TAction;
    CmdDanhmucChiPhiFB1: TMenuItem;
    CmdDmNPL_FB: TAction;
    CmdDmhh_FB: TAction;
    N39: TMenuItem;
    hcn1: TMenuItem;
    Nguynphliu1: TMenuItem;
    ToolButton1: TToolButton;
    PopFnB: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    CmdNhapFB: TAction;
    GrFB: TRzGroup;
    CmdNhapkhacFB: TAction;
    CmdXuatkhacFB: TAction;
    CmdKiemkeFB: TAction;
    CmdDieukhoFB: TAction;
    N40: TMenuItem;
    NhpkhcFB1: TMenuItem;
    N41: TMenuItem;
    KimkFB1: TMenuItem;
    CmdKhuyenmaiFB: TAction;
    CmdKhuyemaiComboFB: TAction;
    CmdKhuyenmaiChietkhauFB: TAction;
    CmdKhuyenmaidachieuFB: TAction;
    CmdBaocaoFB: TAction;
    CmdNhapxuatkhoFB: TAction;
    NhpXutKhoFB1: TMenuItem;
    CmdDondathangFB: TAction;
    CmdDondhNCCFB: TAction;
    PhiunhpFB1: TMenuItem;
    N42: TMenuItem;
    NhpkhcFB2: TMenuItem;
    XutkhcFB1: TMenuItem;
    N43: TMenuItem;
    iukhoFB1: TMenuItem;
    KhuynmitheoCombo1: TMenuItem;
    Khuynmitheochitkhunggi1: TMenuItem;
    Khuynmitheoslng1: TMenuItem;
    CmdXuattraFB: TAction;
    XuttrFB1: TMenuItem;
    CmdDmchiphi: TAction;
    N15: TMenuItem;
    Danhmcchiph1: TMenuItem;
    CmdTonkhoFB: TAction;
    CmdTonkhoFB1: TMenuItem;
    BocoFB1: TMenuItem;
    N44: TMenuItem;
    KimkFB3: TMenuItem;
    CmdCheBienFB: TAction;
    ChbinFB1: TMenuItem;
    CmdLoaiVipCTFB: TAction;
    ChitkhutheoloithVIPFB1: TMenuItem;
    CmdHoaDonBanLeFB: TAction;
    HanbnlFB1: TMenuItem;
    CmdNhapTraBanLeFB: TAction;
    NhptrbnlFB1: TMenuItem;
    CmdDmBacLuong: TAction;
    Bclng1: TMenuItem;
    CmdDmDiaDiemLamViec: TAction;
    aimlmvic1: TMenuItem;
    CmdDmNguonTuyenDung: TAction;
    Nguntuyndng1: TMenuItem;
    CmdDmThietBiTaiSanLaoDong: TAction;
    isnlaong1: TMenuItem;
    CmdDmCanhBao: TAction;
    Cnhbonhn1: TMenuItem;
    CmdDmBieuMau: TAction;
    Danhmcbiumu1: TMenuItem;
    CmdHrHopDongLaoDong: TAction;
    Hpnglaong1: TMenuItem;
    CmdDangky: TAction;
    ngkvngmt1: TMenuItem;
    ngkthaisn1: TMenuItem;
    N45: TMenuItem;
    Nghvic2: TMenuItem;
    N46: TMenuItem;
    Bocochmcng2: TMenuItem;
    CmdChamcong: TAction;
    CmdTinhluong: TAction;
    Chmcng1: TMenuItem;
    Calmvic1: TMenuItem;
    Ngyltt1: TMenuItem;
    N5: TMenuItem;
    Bngchmcng1: TMenuItem;
    Bngtnhlng1: TMenuItem;
    Qunlphpnm1: TMenuItem;
    CmdTamung: TAction;
    CmdDmLyDoKTKL: TAction;
    LdoKhenthngKlut1: TMenuItem;
    CmdDataRight: TAction;
    N48: TMenuItem;
    CmdTangCa: TAction;
    Lchlmvic1: TMenuItem;
    N47: TMenuItem;
    CmdMangThai: TAction;
    ngkmangthai1: TMenuItem;
    N7: TMenuItem;
    uyndng2: TMenuItem;
    ngktngca2: TMenuItem;
    CmdNghiBu: TAction;
    Qunlnghb1: TMenuItem;
    CmdDsKhachHang: TAction;
    CmdKetCa: TAction;
    CmdChuyenDvt: TAction;
    Chuninvtnh1: TMenuItem;
    procedure CmdQuitExecute(Sender: TObject);
    procedure CmdSetpassExecute(Sender: TObject);
    procedure CmdAboutExecute(Sender: TObject);
    procedure CmdParamsExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdTonkhoDKExecute(Sender: TObject);
    procedure CmdDmnhomExecute(Sender: TObject);
    procedure CmdXuatExecute(Sender: TObject);
    procedure CmdNhapExecute(Sender: TObject);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure CmdDmkhacExecute(Sender: TObject);
    procedure CmdDmkhExecute(Sender: TObject);
    procedure CmdThuExecute(Sender: TObject);
    procedure CmdChiExecute(Sender: TObject);
    procedure CmdBcExecute(Sender: TObject);
    procedure CmdKhoasoExecute(Sender: TObject);
    procedure CmdBanleExecute(Sender: TObject);
    procedure CmdNhapkhacExecute(Sender: TObject);
    procedure CmdXuatkhacExecute(Sender: TObject);
    procedure CmdDmkhac3Execute(Sender: TObject);
    procedure CmdForceDropDownExecute(Sender: TObject);
    procedure CmdNhaptraExecute(Sender: TObject);
    procedure CmdXuattraExecute(Sender: TObject);
    procedure CmdChuyenkhoExecute(Sender: TObject);
    procedure CmdDmkhoExecute(Sender: TObject);
    procedure CmdInlabelExecute(Sender: TObject);
    procedure CmdChikhacExecute(Sender: TObject);
    procedure CmdHelpExecute(Sender: TObject);
    procedure CmdSetBkgrExecute(Sender: TObject);
    procedure CmdClearBkgrExecute(Sender: TObject);
    procedure CmdBkgrOptionExecute(Sender: TObject);
    procedure CmdKiemkeExecute(Sender: TObject);
    procedure CmdSetDateTimeExecute(Sender: TObject);
    procedure CmdDmVIPExecute(Sender: TObject);
    procedure CmdDmmauExecute(Sender: TObject);
    procedure CmdDmsizeExecute(Sender: TObject);
    procedure CmdDdhNCCExecute(Sender: TObject);
    procedure PopBkGrPopup(Sender: TObject);
    procedure CmdCounterExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdLockExecute(Sender: TObject);
    procedure CmdFaqExecute(Sender: TObject);
    procedure CmdMyCustomerExecute(Sender: TObject);
    procedure ItemCloseAllClick(Sender: TObject);
    procedure ItemCatClick(Sender: TObject);
    procedure PopFuncPopup(Sender: TObject);
    procedure CmdDmdialyExecute(Sender: TObject);
    procedure TntFormResize(Sender: TObject);
    procedure CmdTygiaExecute(Sender: TObject);
    procedure CmdDondhKHExecute(Sender: TObject);
    procedure CmdNhapDCExecute(Sender: TObject);
    procedure CmdXuatDCExecute(Sender: TObject);
    procedure CmdDmvtBoExecute(Sender: TObject);
    procedure CmdNhaptrablExecute(Sender: TObject);
    procedure CmdHoadonGTGTExecute(Sender: TObject);
    procedure CmdThuHTExecute(Sender: TObject);
    procedure CmdChiHTExecute(Sender: TObject);
    procedure CmdThuThunganExecute(Sender: TObject);
    procedure CmdChiThunganExecute(Sender: TObject);
    procedure CmdKetchuyenExecute(Sender: TObject);
    procedure CmdThukhacExecute(Sender: TObject);
    procedure CmdKhuyenmaiExecute(Sender: TObject);
    procedure CmdChietkhauBanleExecute(Sender: TObject);
    procedure CmdReconnectExecute(Sender: TObject);
    procedure CmdTonkhoExecute(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure FormActivate(Sender: TObject);
    procedure MyActionListExecute(Action: TBasicAction; var Handled: Boolean);
    procedure CmdKhuyenmai3Execute(Sender: TObject);
    procedure CmdNhaptrabl2Execute(Sender: TObject);
    procedure CmdLoaiVipCTExecute(Sender: TObject);
    procedure CmdDmTaikhoanExecute(Sender: TObject);
    procedure CmdThu2Execute(Sender: TObject);
    procedure CmdChi2Execute(Sender: TObject);
    procedure CmdListUserExecute(Sender: TObject);
    procedure CmdListGroupExecute(Sender: TObject);
    procedure CmdFunctionRightExecute(Sender: TObject);
    procedure CmdReportRightExecute(Sender: TObject);
    procedure CmdDmKhNccExecute(Sender: TObject);
    procedure CmdLoaiVIPExecute(Sender: TObject);
    procedure CmdLoaiVip2Execute(Sender: TObject);
    procedure CmdLoaiVipCt2Execute(Sender: TObject);
    procedure CmdConfigExecute(Sender: TObject);
    procedure Ngnhng1Click(Sender: TObject);
    procedure CmdDmNganhangExecute(Sender: TObject);
    procedure CmdCongnoKHExecute(Sender: TObject);
    procedure CmdCongnoNCCExecute(Sender: TObject);
    procedure CmdDmHotroExecute(Sender: TObject);
    procedure CmdPhieuQuatangExecute(Sender: TObject);
    procedure CmdDmChiphiPOMExecute(Sender: TObject);
    procedure CmdDmhhNhom_POMExecute(Sender: TObject);
    procedure CmdDmHesoDodamExecute(Sender: TObject);
    procedure CmdDmHesoDungtichExecute(Sender: TObject);
    procedure CmdDmNVLExecute(Sender: TObject);
    procedure CmdDmBTPExecute(Sender: TObject);
    procedure CmdDmHotroKTExecute(Sender: TObject);
    procedure CmdDmHotroNSExecute(Sender: TObject);
    procedure CmdDmHotroSXExecute(Sender: TObject);
    procedure CmdDmTPExecute(Sender: TObject);
    procedure CmdDmVangmatExecute(Sender: TObject);
    procedure CmdDmPhongbanExecute(Sender: TObject);
    procedure CmdDmChucvuExecute(Sender: TObject);
    procedure CmdDmNhomlvExecute(Sender: TObject);
    procedure CmdDmPhucapExecute(Sender: TObject);
    procedure CmdDmQuatrinhExecute(Sender: TObject);
    procedure CmdDmHopdongExecute(Sender: TObject);
    procedure CmdDmDKKhambenhExecute(Sender: TObject);
    procedure CmdDmCalamviecExecute(Sender: TObject);
    procedure CmdDmNgayleExecute(Sender: TObject);
    procedure CmdBangcongExecute(Sender: TObject);
    procedure CmdVangmatExecute(Sender: TObject);
    procedure CmdPhepnamExecute(Sender: TObject);
    procedure CmdLichlamviecExecute(Sender: TObject);
    procedure CmdThaisanExecute(Sender: TObject);
    procedure CmdBangluongExecute(Sender: TObject);
    procedure CmdDmnvExecute(Sender: TObject);
    procedure CmdProfileExecute(Sender: TObject);
    procedure CmdQuatrinhExecute(Sender: TObject);
    procedure CmdKhenthuongExecute(Sender: TObject);
    procedure CmdCongtacExecute(Sender: TObject);
    procedure CmdGiadinhExecute(Sender: TObject);
    procedure CmdThietbiExecute(Sender: TObject);
    procedure CmdDaotaoExecute(Sender: TObject);
    procedure CmdThoiviecExecute(Sender: TObject);
    procedure CmdTuyendungHosoExecute(Sender: TObject);
    procedure CmdTuyendungKetquaExecute(Sender: TObject);
    procedure CmdDmMCCExecute(Sender: TObject);
    procedure CmdVantayExecute(Sender: TObject);
    procedure CmdCongthoExecute(Sender: TObject);
    procedure CmdLaydulieucongExecute(Sender: TObject);
    procedure CmdKyluatExecute(Sender: TObject);
    procedure CmdBcKDExecute(Sender: TObject);
    procedure CmdBcSXExecute(Sender: TObject);
    procedure CmdBcNSTuyendungExecute(Sender: TObject);
    procedure CmdBcNSExecute(Sender: TObject);
    procedure CmdBcKTExecute(Sender: TObject);
    procedure CmdBcNSCongExecute(Sender: TObject);
    procedure CmdBcNSLuongExecute(Sender: TObject);
    procedure CmdhethongTaikhoanKTExecute(Sender: TObject);
    procedure CmdDmChiphiKTExecute(Sender: TObject);
    procedure CmdDmKetoanExecute(Sender: TObject);
    procedure CmdDmNhansuExecute(Sender: TObject);
    procedure CmdHrLichsuExecute(Sender: TObject);
    procedure CmdHrDaotaoExecute(Sender: TObject);
    procedure CmdHrThietbiExecute(Sender: TObject);
    procedure CmdDmSanxuatExecute(Sender: TObject);
    procedure CmdHrTuyendungExecute(Sender: TObject);
    procedure CmdGiathanhExecute(Sender: TObject);
    procedure CmdQuytrinhSXExecute(Sender: TObject);
    procedure CmdUnlockChungtuExecute(Sender: TObject);
    procedure CmdTbMaychamcongExecute(Sender: TObject);
    procedure CmdDONDATHANGExecute(Sender: TObject);
    procedure CmdNHAPXUATKHOExecute(Sender: TObject);
    procedure CmdBANLEMENUExecute(Sender: TObject);
    procedure CmdGrThuExecute(Sender: TObject);
    procedure CmdGrChiExecute(Sender: TObject);
    procedure CmdDanhmucExecute(Sender: TObject);
    procedure CmdDmTaisanExecute(Sender: TObject);
    procedure CmdDmvtTSExecute(Sender: TObject);
    procedure CmdDmnhomTSExecute(Sender: TObject);
    procedure CmdDmnccTSExecute(Sender: TObject);
    procedure CmdDmPhongbanTSExecute(Sender: TObject);
    procedure CmdDmnvTSExecute(Sender: TObject);
    procedure CmdDmmauTSExecute(Sender: TObject);
    procedure CmdDmSizeTSExecute(Sender: TObject);
    procedure CmdDmHotroTSExecute(Sender: TObject);
    procedure CmdDanhmucNhomNganhFBExecute(Sender: TObject);
    procedure CmdDanhmucFBExecute(Sender: TObject);
    procedure CmdDanhmuchotroFBExecute(Sender: TObject);
    procedure CmdDanhmucBANExecute(Sender: TObject);
    procedure CmdDanhmucChiPhiFBExecute(Sender: TObject);
    procedure CmdDmNPL_FBExecute(Sender: TObject);
    procedure CmdDmhh_FBExecute(Sender: TObject);
    procedure CmdNhapFBExecute(Sender: TObject);
    procedure CmdNhapkhacFBExecute(Sender: TObject);
    procedure CmdXuatkhacFBExecute(Sender: TObject);
    procedure CmdKiemkeFBExecute(Sender: TObject);
    procedure CmdDieukhoFBExecute(Sender: TObject);
    procedure CmdKhuyenmaiFBExecute(Sender: TObject);
    procedure CmdNhapxuatkhoFBExecute(Sender: TObject);
    procedure CmdKhuyemaiComboFBExecute(Sender: TObject);
    procedure CmdKhuyenmaiChietkhauFBExecute(Sender: TObject);
    procedure CmdKhuyenmaidachieuFBExecute(Sender: TObject);
    procedure CmdBaocaoFBExecute(Sender: TObject);
    procedure CmdDondathangFBExecute(Sender: TObject);
    procedure CmdDondhNCCFBExecute(Sender: TObject);
    procedure CmdXuattraFBExecute(Sender: TObject);
    procedure CmdDmchiphiExecute(Sender: TObject);
    procedure CmdTonkhoFBExecute(Sender: TObject);
    procedure CmdCheBienFBExecute(Sender: TObject);
    procedure CmdLoaiVipCTFBExecute(Sender: TObject);
    procedure CmdHoaDonBanLeFBExecute(Sender: TObject);
    procedure CmdNhapTraBanLeFBExecute(Sender: TObject);
    procedure CmdDmBacLuongExecute(Sender: TObject);
    procedure CmdDmDiaDiemLamViecExecute(Sender: TObject);
    procedure CmdDmNguonTuyenDungExecute(Sender: TObject);
    procedure CmdDmThietBiTaiSanLaoDongExecute(Sender: TObject);
    procedure CmdDmCanhBaoExecute(Sender: TObject);
    procedure CmdDmBieuMauExecute(Sender: TObject);
    procedure CmdHrHopDongLaoDongExecute(Sender: TObject);
    procedure CmdChamcongExecute(Sender: TObject);
    procedure CmdTinhluongExecute(Sender: TObject);
    procedure CmdDangkyExecute(Sender: TObject);
    procedure CmdKhenThuongKyLuatExecute(Sender: TObject);
    procedure CmdDmLyDoKTKLExecute(Sender: TObject);
    procedure CmdDmKinhdoanhExecute(Sender: TObject);
    procedure CmdDataRightExecute(Sender: TObject);
    procedure CmdTangCaExecute(Sender: TObject);
    procedure CmdMangThaiExecute(Sender: TObject);
    procedure CmdNghiBuExecute(Sender: TObject);
    procedure CmdDsKhachHangExecute(Sender: TObject);
    procedure CmdKetCaExecute(Sender: TObject);
    procedure CmdChuyenDvtExecute(Sender: TObject);
  private
	r: WORD;
    mFixLogon: Boolean;
	procedure LoadBkgrImage;
    procedure SetBkgrText;
  public
  end;

var
  FrmMain: TFrmMain;

implementation

uses
	{$IFDEF _MULTILANG_}
	// Multilizer
    IvI18N,
    IvResDll,
    IvResLangD,
    {$ENDIF}

	MainData, ExCommon, Dmvt, Tonkho, Params, Rights, GmsRep, Nhap, Xuat,
    Dmkhac, Dmncc, Dmkh, isMsg, Banle, Thu, Chi, NXKhac, Dmkhac3, Khoaso, isDb,
    ChuyenKho, Dmkho, isType, BkgrOption, Scan, Kiemke, isStr, DmVIP, Dmmau,
    Dmsize, isDba, DondhNCC, DondhKH, isLib, Dmdl, Dmquaytn, Tygia, DmhhNhom,
    DieuchinhNhap, DieuchinhXuat, DmvtBo, HoadonGTGT, NhapTra,
    XuatTra, Inlabel, Ketchuyentien, ThuThungan, ChiThungan, Chikhac, Thukhac,
    Khuyenmai, DmKhuyenmai, TonkhoDK, FlexRep, Khuyenmai3, Nhaptrabl2,  ChiHT,
    ThuHT, DmLoaiVipCT, isCommon, DmTK, Thu2, Chi2, DmKho2, isOdbc, isLogin,
    ListUser, ListGroup, FunctionAccess, ReportAccess, AdminData, DmKhNcc,
    isResourceString, DmLoaiVIP,
    Params2, DmNganhang, CongnoKH, CongnoNCC, DmHotro, PhieuQuatang, GuidEx,


  //HR
    DmPhongban, Dmnv, DmVangmat, DmNgayle,
    DevList, DmCalamviec, DmChucvu, Vantay, LogReader, WaitProgress, DevRAW,
    DmNhomlv, DmQTLV, DmPhucap, DmHopdong, DmNoichuabenh, Bangcong,
    Bangluong, DangkyVangmat, DangkyThaisan, Phepnam, HopdongLaodong, DevLog,
  DmPhongbanNotCN, UnLockPhieu,
  DmBacLuong, DmDiaDiemLamViec, DmNguonTuyenDung, DmThietBiTaiSanLaoDong,
  POM_DmhhNhom, POM_DmChiphi, POM_DmHesoDD, POM_DmHesoDT, POM_DmvtNVL,
  POM_DmvtBTP, POM_Dmvt,DmChiphi, DmHotro_HR, DmCanhBao, DmBieuMau, DmLyDoKTKL,
  KhenThuongKyLuat, HoSonv, LichSuCongTac, ThietBiTaiSan, QuaTrinhLamViec, DangkyTangCa,
  NghiBu, DaoTao, LichLamViec, DangkyMangThai,

  //TS
  POM_BangSanluong, TS_DmhhNhom, TS_DmNcc, TS_Dmvt,

  //FB
  FB_DmhhNhom,  FB_DmChiphi, FB_DmBan, FB_DmvtNVL, FB_Dmhh, FB_ChuyenKho, FB_Kiemke, FB_PhieuNhap,
  FB_NXKhac, FB_XuatTra, FB_DondhNCC, FB_Tonkho, FB_DmvtBo, FB_Khuyenmai, FB_CheBien, FB_Khuyenmai3,
  FB_DmLoaiVipCT, FB_Banle, FB_Nhaptrabl, DataAccess, NhapTraBanLe, DmTKKeToan, KetCa,
  ChuyenDvt;


{$R *.DFM}
{$R exRes.res}

{$REGION '<< Some of reusable procedures >>'}
(*==============================================================================
** Load background image
**------------------------------------------------------------------------------
*)
procedure TFrmMain.LoadBkgrImage;
const
	cDrawStyle: array[0..5] of TfcImagerDrawStyle = (dsCenter, dsNormal,
        dsProportional, dsProportionalCenter, dsStretch, dsTile);
var
	s: String;
	n: Integer;
begin
    // Set background image
    s := RegRead('Background', 'FileName', '');
    if FileExists(s) then
    begin
        BkGr.Visible := True;
        try
            BkGr.Picture.LoadFromFile(s);
        except;
        end;
    end;

    with BkGr do
		if Visible then
	    begin
            Transparent := RegRead('Transparent', BKGR_TRANSPARENT);
            TransparentColor := RegRead('Transparent Color', BKGR_TRANSPARENT_COLOR);
            n := RegRead('Draw Style', BKGR_DRAW_STYLE);
            DrawStyle := cDrawStyle[n];
        end;

    // Background text
    PaDesc.Visible := RegRead('Show Text', BKGR_SHOWTEXT);
    if PaDesc.Visible then
    begin
        // Text
        n := RegRead('Text Color', BKGR_TEXT_COLOR);
        LbDesc1.Font.Color := n;
        LbDesc2.Font.Color := n;
    end;
end;

(*==============================================================================
** Set background text
**------------------------------------------------------------------------------
*)
procedure TFrmMain.SetBkgrText;
begin
    if PaDesc.Visible then
    begin
	    LbDesc1.Caption := '';//UpperCase(isDoubleChr(sysDesc1), loUserLocale);
    	LbDesc2.Caption := '';//UpperCase(isDoubleChr(sysDesc2), loUserLocale);
    end;
end;
{$ENDREGION}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.MyActionListExecute(Action: TBasicAction;
  var Handled: Boolean);
begin
	Handled := MyActionList.Tag = 0;
//    if Handled then //NTD
//        TestDbConnection;
end;

procedure TFrmMain.Ngnhng1Click(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormActivate(Sender: TObject);
begin
	if Tag = 1 then
    begin
	    with GbFunc do
    	begin
	    	ScrollPosition := 0;
	        Groups[0].Reposition;
    	end;

		Tag := 2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
var
    i: Integer;
begin
    // Save GroupBar state
    with GbFunc do
        for i := 0 to GroupCount - 1 do
            RegWrite(Self.Name, Groups[i].Name, Groups[i].Opened);

    try
//        DataMain.isEnv1.Logoff; //NTD
    except
    end;
    DbeFinal;

    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.FormShow(Sender: TObject);
var
    i: Integer;
begin
	TMyForm(Self).Init2;
	LoadCustomIcon;
    Status.Panels[4].Text := 'DB: ' + isDescriptionServer;
    DbeInitial;

	// Initial
    FrmScan := Nil;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);

    // Enable system functions
//    SetApplicationVersion;
    SetApplicationFunctions(Self);

    CmdDataRight.Visible := sysIsDataAccess;
    GrPhanQuyen.Visible := sysIsAdmin;
    
    sysIsBarcode := GetFuncState('SZ_STAMP');
    BtInBarcode.Visible := sysIsBarcode and (not sysIsAdmin);

    // Desktop
	SetBkgrText;
    LoadBkgrImage;

    // Create System ODBC data sources
    isConfigSqlDsn2(Handle, isDatabaseServer + '_ODBC', '', isNameServer, isDatabaseServer);

  // Create DNS Text
    isConfigTxtDsn2(Handle, TXT_DSN, 'Softz', sysAppPath + '\External Data');


    // Load GroupBar state
    with GbFunc do
    begin
        Style := TRzGroupBarStyle(RegRead(Self.Name, 'RzGroupStyle', 0));
        for i := 0 to GroupCount - 1 do
            Groups[i].Opened := RegReadBool(Self.Name, Groups[i].Name, True);
	    ScrollPosition := 0;
        Groups[0].Reposition;
    end;
    MyActionList.Tag := 1;
    SystemCritical.IsCritical := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuatrinhExecute(Sender: TObject);
begin
    r := GetRights('HR_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmQuaTrinhLamViec, FrmQuaTrinhLamViec);
    FrmQuaTrinhLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuitExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdQuytrinhSXExecute(Sender: TObject);
begin
    r := GetRights('POM_QUYTRINH');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetpassExecute(Sender: TObject);
begin
    DataMain.isLogon.ResetPassword; //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdAboutExecute(Sender: TObject);
begin
//	Application.CreateForm(TFrmAbout, FrmAbout);
//    FrmAbout.ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdParamsExecute(Sender: TObject);
var
	n: Integer;
begin
//	r := GetRights('SZ_TSO');
//    if r = R_DENY then
//    	Exit;

	Application.CreateForm(TFrmParams, FrmParams);
    n := FrmParams.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPhepnamExecute(Sender: TObject);
begin
    r := GetRights('HR_PHEPNAM');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmPhepnam, FrmPhepnam);
    FrmPhepnam.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdPhieuQuatangExecute(Sender: TObject);
begin
    r := GetRights('SZ_PHIEU_QUATANG');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPhieuQuatang, FrmPhieuQuatang);
    FrmPhieuQuatang.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdProfileExecute(Sender: TObject);
begin
    r := GetRights('HR_PROFILE');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmHoSonv, FrmHoSonv);
    FrmHoSonv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhenthuongExecute(Sender: TObject);
begin
    r := GetRights('HR_KHENTHUONG_KL');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmKhenThuongKyLuat, FrmKhenThuongKyLuat);
    FrmKhenThuongKyLuat.Execute(r);
end;

procedure TFrmMain.CmdKhenThuongKyLuatExecute(Sender: TObject);
begin
    r := GetRights('HR_KHENTHUONG_KL');
    if r = R_DENY then
    	Exit;


end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhoasoExecute(Sender: TObject);
begin
	r := GetRights('SZ_KHOASO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhoaso, FrmKhoaso);
    FrmKhoaso.Execute(r);
    DataMain.InitParams;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTonkhoDKExecute(Sender: TObject);
begin
	r := GetRights('SZ_TONKHO_DK');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTonkhoDK, FrmTonkhoDK);
    FrmTonkhoDK.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTonkhoExecute(Sender: TObject);
begin
    r := GetRights('SZ_TONKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTonkho, FrmTonkho);
    FrmTonkho.Execute(r);
end;

procedure TFrmMain.CmdTonkhoFBExecute(Sender: TObject);
begin
    r := GetRights('FB_TONKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Tonkho, FrmFB_Tonkho);
    FrmFB_Tonkho.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTuyendungHosoExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTuyendungKetquaExecute(Sender: TObject);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnhomExecute(Sender: TObject);
begin
	r := GetRights('SZ_DM_NGANH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmhhNhom, FrmDmhhNhom);
    FrmDmhhNhom.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNhomlvExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NHOMLV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmNhomlv, FrmDmNhomlv);
    FrmDmNhomlv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnhomTSExecute(Sender: TObject);
begin
    r := GetRights('ACC_DM_NGANH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTS_DmhhNhom, FrmTS_DmhhNhom);
    FrmTS_DmhhNhom.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNPL_FBExecute(Sender: TObject);
begin
    r := GetRights('FB_DM_NPL');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmvtNVL, FrmFB_DmvtNVL);
    FrmFB_DmvtNVL.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnvExecute(Sender: TObject);
begin
    r := GetRights('HR_NHANVIEN');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmnv, FrmDmnv);
    FrmDmnv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNVLExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_NVL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtNVL, FrmPOM_DmvtNVL);
    FrmPOM_DmvtNVL.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnvTSExecute(Sender: TObject);
begin
    r := GetRights('TS_DM_NHANVIEN');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmnv, FrmDmnv);
    FrmDmnv.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmPhongbanExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_PHONGBAN');
    if r = R_DENY then
    	Exit;

	if sysIsHrChinhanh then
    begin
        Application.CreateForm(TFrmDmPhongban, FrmDmPhongban);
        FrmDmPhongban.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmPhongbanNotCN, FrmDmPhongbanNotCN);
        FrmDmPhongbanNotCN.Execute(r);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmPhongbanTSExecute(Sender: TObject);
begin
    r := GetRights('TS_DM_PHONGBAN');
    if r = R_DENY then
    	Exit;

	if sysIsHrChinhanh then
    begin
        Application.CreateForm(TFrmDmPhongban, FrmDmPhongban);
        FrmDmPhongban.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmPhongbanNotCN, FrmDmPhongbanNotCN);
        FrmDmPhongbanNotCN.Execute(r);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmPhucapExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_PHUCAP');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmPhucap, FrmDmPhucap);
    FrmDmPhucap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmQuatrinhExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_QUATRINH_LV');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmQTLV, FrmDmQTLV);
    FrmDmQTLV.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmSanxuatExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmvtExecute(Sender: TObject);
begin
	r := GetRights('SZ_DM_MAVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvt, FrmDmvt);
    FrmDmvt.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmvtTSExecute(Sender: TObject);
begin
    r := GetRights('ACC_DM_VT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTs_Dmvt, FrmTs_Dmvt);
    FrmTs_Dmvt.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDONDATHANGExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmvtBoExecute(Sender: TObject);
begin
	r := GetRights('SZ_KHUYENMAI_HANGHOABO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvtBo, FrmDmvtBo);
    FrmDmvtBo.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhoExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_KHO');
    if r = R_DENY then
    	Exit;

    if sysIsDrc then
    begin
        Application.CreateForm(TFrmDmkho2, FrmDmkho2);
        FrmDmkho2.Execute(r);
    end else
    begin
        Application.CreateForm(TFrmDmkho, FrmDmkho);
        FrmDmkho.Execute(r);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmKinhdoanhExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmVangmatExecute(Sender: TObject);
begin
	r := GetRights('HR_DM_VANGMAT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmVangmat, FrmDmVangmat);
    FrmDmVangmat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmVIPExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_VIP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmVIP, FrmDmVIP);
    FrmDmVIP.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapExecute(Sender: TObject);
begin
	r := GetRights('SZ_NHAP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhap, FrmNhap);
    FrmNhap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapFBExecute(Sender: TObject);
begin
    r := GetRights('FB_PHIEU_NHAP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_PhieuNhap, FrmFB_PhieuNhap);
    FrmFB_PhieuNhap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNghiBuExecute(Sender: TObject);
begin
    r := GetRights('HR_NGHIBU');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmNghiBu, FrmNghiBu);
    FrmNghiBu.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapDCExecute(Sender: TObject);
begin
	r := GetRights('SZ_NHAP_DC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDieuchinhNhap, FrmDieuchinhNhap);
    FrmDieuchinhNhap.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatExecute(Sender: TObject);
begin
	r := GetRights('SZ_XUAT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmXuat, FrmXuat);
    FrmXuat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatDCExecute(Sender: TObject);
begin
	r := GetRights('SZ_XUAT_DC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDieuchinhXuat, FrmDieuchinhXuat);
    FrmDieuchinhXuat.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptraExecute(Sender: TObject);
begin
	r := GetRights('SZ_NHAPTRA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptra, FrmNhaptra);
    FrmNhaptra.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNHAPXUATKHOExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapxuatkhoFBExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuattraExecute(Sender: TObject);
begin
	r := GetRights('SZ_XUATTRA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmXuattra, FrmXuattra);
    FrmXuattra.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuattraFBExecute(Sender: TObject);
begin
    r := GetRights('FB_XUATTRA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_XuatTra, FrmFB_XuatTra);
    FrmFB_XuatTra.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapkhacExecute(Sender: TObject);
begin
	r := GetRights('SZ_NHAPKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNXKhac, FrmNXKhac);
    FrmNXKhac.Execute('N', r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhapkhacFBExecute(Sender: TObject);
begin
    r := GetRights('FB_NHAPKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_NXKhac, FrmFB_NXKhac);
    FrmFB_NXKhac.Execute('N', r);
end;

procedure TFrmMain.CmdNhapTraBanLeFBExecute(Sender: TObject);
begin
    r := GetRights('FB_NHAPTRA_BANLE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Nhaptrabl, FrmFB_Nhaptrabl);
    FrmFB_Nhaptrabl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatkhacExecute(Sender: TObject);
begin
	r := GetRights('SZ_XUATKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNXKhac, FrmNXKhac);
    FrmNXKhac.Execute('X', r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdXuatkhacFBExecute(Sender: TObject);
begin
    r := GetRights('FB_XUATKHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_NXKhac, FrmFB_NXKhac);
    FrmFB_NXKhac.Execute('X', r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChuyenDvtExecute(Sender: TObject);
begin
    r := GetRights('SZ_CHUYENDOI_DVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChuyenDvt, FrmChuyenDvt);
    FrmChuyenDvt.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChuyenkhoExecute(Sender: TObject);
begin
	r := GetRights('SZ_CKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChuyenKho, FrmChuyenKho);
    FrmChuyenKho.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKiemkeExecute(Sender: TObject);
begin
	r := GetRights('SZ_KIEMKE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKiemke, FrmKiemke);
    FrmKiemke.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKiemkeFBExecute(Sender: TObject);
begin
    r := GetRights('FB_KIEMKE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Kiemke, FrmFB_Kiemke);
    FrmFB_Kiemke.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKyluatExecute(Sender: TObject);
begin
    r := GetRights('HR_KYLUAT');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnccExecute(Sender: TObject);
begin
	r := GetRights('SZ_DM_NCC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmkhNcc, FrmDmkhNcc);
    FrmDmkhNcc.Execute(r, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmnccTSExecute(Sender: TObject);
begin
    r := GetRights('ACC_DM_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTS_Dmncc, FrmTS_Dmncc);
    FrmTS_Dmncc.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNganhangExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_NGANHANG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNganhang, FrmDmNganhang);
    FrmDmNganhang.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNgayleExecute(Sender: TObject);
begin
    r := GetRights('HR_NGAYLE');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNgayle, FrmDmNgayle);
    FrmDmNgayle.Execute(r);
end;

procedure TFrmMain.CmdDmNguonTuyenDungExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_NGUON_TUYENDUNG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmNguonTuyenDung, FrmDmNguonTuyenDung);
    FrmDmNguonTuyenDung.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmNhansuExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhExecute(Sender: TObject);
begin
    r := GetRights('SZ_DM_KHACHHANG');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmDmkhNcc, FrmDmkhNcc);
    FrmDmkhNcc.Execute(r, 0);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmKhNccExecute(Sender: TObject);
begin
    r := GetRights('SZ_DM_KH_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhNcc, FrmDmkhNcc);
    FrmDmkhNcc.Execute(r, -1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhacExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhac, FrmDmkhac);
    FrmDmkhac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmkhac3Execute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_KHAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmkhac3, FrmDmkhac3);
    FrmDmkhac3.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTangCaExecute(Sender: TObject);
begin
    r := GetRights('HR_TANGCA');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyTangCa, FrmDangkyTangCa);
    FrmDangkyTangCa.Execute(r, 0);
end;

procedure TFrmMain.CmdTbMaychamcongExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThaisanExecute(Sender: TObject);
begin
    r := GetRights('HR_THAISAN');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyThaisan, FrmDangkyThaisan);
    FrmDangkyThaisan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThietbiExecute(Sender: TObject);
begin
    r := GetRights('HR_THIETBI_DUNGCU');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmThietBiTaiSan, FrmThietBiTaiSan);
    FrmThietBiTaiSan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThoiviecExecute(Sender: TObject);
begin
    r := GetRights('HR_NGHIVIEC');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThu2Execute(Sender: TObject);
begin
    r := GetRights('ACC_THU_PHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThu2, FrmThu2);
    FrmThu2.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuExecute(Sender: TObject);
begin
	r := GetRights('ACC_THU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThu, FrmThu);
    FrmThu.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuHTExecute(Sender: TObject);
begin
	r := GetRights('ACC_THU_PHIEUHT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmThuHT, FrmThuHT);
    FrmThuHT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThuThunganExecute(Sender: TObject);
begin
	r := GetRights('ACC_THU_THUNGAN');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmThuThungan, FrmThuThungan);
    FrmThuThungan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTinhluongExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGLUONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmBangluong, FrmBangluong);
    FrmBangluong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiExecute(Sender: TObject);
begin
	r := GetRights('ACC_CHI');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChi, FrmChi);
    FrmChi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiHTExecute(Sender: TObject);
begin
	r := GetRights('ACC_CHI_PHIEUHT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChiHT, FrmChiHT);
    FrmChiHT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChiThunganExecute(Sender: TObject);
begin
	r := GetRights('ACC_CHI_THUNGAN');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChiThungan, FrmChiThungan);
    FrmChiThungan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKetCaExecute(Sender: TObject);
begin
    r := GetRights('SZ_KETCA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKetCa, FrmKetCa);
    FrmKetCa.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKetchuyenExecute(Sender: TObject);
begin
	r := GetRights('ACC_KETCHUYEN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKetchuyentien, FrmKetchuyentien);
    FrmKetchuyentien.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdThukhacExecute(Sender: TObject);
begin
	r := GetRights('ACC_THU_KHAC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmThukhac, FrmThukhac);
    FrmThukhac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChikhacExecute(Sender: TObject);
begin
	r := GetRights('ACC_CHI_KHAC');
    if r = R_DENY then
    	Exit;
        
	Application.CreateForm(TFrmChikhac, FrmChikhac);
    FrmChikhac.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('1') (*1*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcKDExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('KinhDoanh')(*10*)  then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcKTExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('KeToan') (*30*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcNSCongExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('CongLuong') (*22*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcNSExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('NhanSu') (*20*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcNSLuongExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('TinhLuong') (*23*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcNSTuyendungExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('TuyenDung') (*24*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBcSXExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('SanXuat') (*40*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBangcongExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGCONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmBangcong, FrmBangcong);
    FrmBangcong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBangluongExecute(Sender: TObject);
begin
    r := GetRights('HR_BANGLUONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmBangluong, FrmBangluong);
    FrmBangluong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBanleExecute(Sender: TObject);
begin
	r := GetRights('SZ_SUABILL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmBanle, FrmBanle);
    FrmBanle.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBANLEMENUExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBaocaoFBExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGmsRep, FrmGmsRep);
    if not FrmGmsRep.Execute('FnB') (*60*) then
    begin
    	DenyMsg;
    	FrmGmsRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdForceDropDownExecute(Sender: TObject);
begin
   (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFunctionRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmFuncAccess, FrmFuncAccess);
    FrmFuncAccess.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGiadinhExecute(Sender: TObject);
begin
    r := GetRights('HR_GIADINH');
    if r = R_DENY then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdInlabelExecute(Sender: TObject);
begin
	r := GetRights('SZ_STAMP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmInlabel, FrmInlabel);
    FrmInlabel.ShowModal
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHelpExecute(Sender: TObject);
begin
	Application.HelpCommand(HH_DISPLAY_TOC, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdhethongTaikhoanKTExecute(Sender: TObject);
var
    s: string;
begin
    r := GetRights('ACC_DM_TAIKHOAN_TK');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmTKKeToan, FrmDmTKKeToan);
    FrmDmTKKeToan.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGiathanhExecute(Sender: TObject);
begin
    r := GetRights('POM_GIATHANH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_BangSanluong, FrmPOM_BangSanluong);
    FrmPOM_BangSanluong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGrChiExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdGrThuExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetBkgrExecute(Sender: TObject);
begin
	// Get file name
	with PicDlg do
		if not Execute then
        	Exit;

    // Save to registry
    RegWrite('Background', 'FileName', PicDlg.FileName);

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdClearBkgrExecute(Sender: TObject);
begin
	// Delete file name
    RegDelete('Background', 'FileName');

    // Update background
    LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdBkgrOptionExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmBkgrOption, FrmBkgrOption);
    with FrmBkgrOption do
    	if Execute then
	       LoadBkgrImage;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReconnectExecute(Sender: TObject);
begin
//     DbConnect; //NTD
    if not ConnectToServer(isNameServer, isUserServer, isPasswordServer, isDatabaseServer) then
    begin
      ErrMsg(StrFailedToConnectToDatabaseServer);
      Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdReportRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmRepAccess, FrmRepAccess);
    FrmRepAccess.Execute(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdSetDateTimeExecute(Sender: TObject);
begin
	WinExec('control timedate.cpl', SW_NORMAL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmmauExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_MAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmmau, FrmDmmau);
    FrmDmmau.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmmauTSExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_MAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmmau, FrmDmmau);
    FrmDmmau.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmMCCExecute(Sender: TObject);
begin
    r := GetRights('HR_MAYCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDevList, FrmDevList);
    FrmDevList.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmsizeExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_SIZE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmsize, FrmDmsize);
    FrmDmsize.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmSizeTSExecute(Sender: TObject);
begin
    r := GetRights('TS_DM_SIZE');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmsize, FrmDmsize);
    FrmDmsize.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmTaikhoanExecute(Sender: TObject);
var
    s: string;
begin
    r := GetRights('ACC_DM_TAIKHOAN_NH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmTK, FrmDmTK);
    FrmDmTK.Execute(r, s);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmTaisanExecute(Sender: TObject);
begin
    //
end;

procedure TFrmMain.CmdDmThietBiTaiSanLaoDongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_THIETBI_TAISAN_LAODONG');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmThietBiTaiSanLaoDong, FrmDmThietBiTaiSanLaoDong);
    FrmDmThietBiTaiSanLaoDong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmTPExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_TP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_Dmvt, FrmPOM_Dmvt);
    FrmPOM_Dmvt.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmBacLuongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_BACLUONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmBacLuong, FrmDmBacLuong);
    FrmDmBacLuong.Execute(r);
end;

procedure TFrmMain.CmdDmBieuMauExecute(Sender: TObject);
begin
   r := GetRights('SZ_PUB_DM_BIEUMAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmBieuMau, FrmDmBieuMau);
    FrmDmBieuMau.Execute(r);
end;

procedure TFrmMain.CmdDmBTPExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_BTP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmvtBTP, FrmPOM_DmvtBTP);
    FrmPOM_DmvtBTP.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmCalamviecExecute(Sender: TObject);
begin
	r := GetRights('HR_CALAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmCalamviec, FrmDmCalamviec);
    FrmDmCalamviec.Execute(r);
end;

procedure TFrmMain.CmdDmCanhBaoExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_NHACVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmCanhBao, FrmDmCanhBao);
    FrmDmCanhBao.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmchiphiExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_CHIPHI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChiphi, FrmDmChiphi);
    FrmDmChiphi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmChiphiPOMExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_CHIPHI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmChiphi, FrmPOM_DmChiphi);
    FrmPOM_DmChiphi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmChiphiKTExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmChucvuExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_CHUCVU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmChucvu, FrmDmChucvu);
    FrmDmChucvu.Execute(r);
end;

procedure TFrmMain.CmdDmDiaDiemLamViecExecute(Sender: TObject);
begin
   r := GetRights('HR_DM_NOI_LAMVIEC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmDiaDiemLamViec, FrmDmDiaDiemLamViec);
    FrmDmDiaDiemLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmdialyExecute(Sender: TObject);
begin
	r := GetRights('SZ_PUB_DM_DIALY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmdl, FrmDmdl);
    FrmDmdl.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmDKKhambenhExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_DK_KHAMBENH');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmNoichuabenh, FrmDmNoichuabenh);
    FrmDmNoichuabenh.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHesoDodamExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_HESO_DODAM');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmhesoDD, FrmPOM_DmhesoDD);
    FrmPOM_DmhesoDD.Execute(r);
end;

procedure TFrmMain.CmdDmHesoDungtichExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_HESO_DUNGTICH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmhesoDT, FrmPOM_DmhesoDT);
    FrmPOM_DmhesoDT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmhhNhom_POMExecute(Sender: TObject);
begin
    r := GetRights('POM_DM_NGANH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmPOM_DmhhNhom, FrmPOM_DmhhNhom);
    FrmPOM_DmhhNhom.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmhh_FBExecute(Sender: TObject);
begin
    r := GetRights('FB_DM_HH');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmFB_Dmhh, FrmFB_Dmhh);
    FrmFB_Dmhh.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHopdongExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOPDONG');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDmHopdong, FrmDmHopdong);
    FrmDmHopdong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHotroExecute(Sender: TObject);
begin
    r := GetRights('SZ_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHotroKTExecute(Sender: TObject);
begin
    r := GetRights('ACC_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHotroNSExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro_HR, FrmDmHotro_HR);
    FrmDmHotro_HR.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHotroSXExecute(Sender: TObject);
begin
    r := GetRights('SZ_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 3);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmHotroTSExecute(Sender: TObject);
begin
    r := GetRights('ASS_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 5);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmKetoanExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDangkyExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmucBANExecute(Sender: TObject);
begin
    r := GetRights('FB_DM_BAN');
    if r = R_DENY then
        Exit;

    Application.CreateForm(TFrmFB_DmBan, FrmFB_DmBan);
    FrmFB_DmBan.Execute(r);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmucChiPhiFBExecute(Sender: TObject);
begin
   r := GetRights('FB_DM_CHIPHI');
    if r = R_DENY then
    	Exit;
   Application.CreateForm(TFrmFB_DmChiphi, FrmFB_DmChiphi);
   FrmFB_DmChiphi.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmucExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmucFBExecute(Sender: TObject);
begin
  //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmuchotroFBExecute(Sender: TObject);
begin
    r := GetRights('FB_DM_HOTRO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 6);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDanhmucNhomNganhFBExecute(Sender: TObject);
begin
  r := GetRights('FB_DM_NGANH');
    if r = R_DENY then
    	Exit;
  Application.CreateForm(TFrmFB_DmhhNhom, FrmFB_DmhhNhom);
    FrmFB_DmhhNhom.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDaotaoExecute(Sender: TObject);
begin

    r := GetRights('HR_DAOTAO');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDaoTao, FrmDaoTao);
    FrmDaoTao.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDataRightExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDataAccess, FrmDataAccess);
    FrmDataAccess.Execute(0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDdhNCCExecute(Sender: TObject);
begin
	r := GetRights('SZ_DDH_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDondhNCC, FrmDondhNCC);
    FrmDondhNCC.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDieukhoFBExecute(Sender: TObject);
begin
    r := GetRights('FB_DIEUKHO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_ChuyenKho, FrmFB_ChuyenKho);
    FrmFB_ChuyenKho.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDondathangFBExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDondhKHExecute(Sender: TObject);
begin
    r := GetRights('SZ_DDH_KH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDondhKH, FrmDondhKH);
    FrmDondhKH.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDondhNCCFBExecute(Sender: TObject);
begin
    r := GetRights('FB_DONDH_NCC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_DondhNCC, FrmFB_DondhNCC);
    FrmFB_DondhNCC.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDsKhachHangExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_KHACHHANG');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmKh, FrmDmKh);
    FrmDmKh.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdExportExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmFlexRep, FrmFlexRep);
    if not FrmFlexRep.Execute(1) then
    begin
    	DenyMsg;
    	FrmFlexRep.Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PopBkGrPopup(Sender: TObject);
begin
	PopBkGr.Items[0].Caption := CmdSetBkGr.Caption +
    	Format(' (%dx%d)', [PaBkGr.Width, PaBkGr.Height]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdConfigExecute(Sender: TObject);
var
    n: Integer;
begin
    r := GetRights('SZ_TSO');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmParams2, FrmParams2);
    n := FrmParams2.Execute(r);

    if n <> 0 then
    begin
	    DataMain.InitParams;
        SetBkgrText;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongnoKHExecute(Sender: TObject);
begin
    if GetRights('ACC_CONGNO_KH') = R_DENY then
        Exit;

    Application.CreateForm(TFrmCongnoKH, FrmCongnoKH);
    FrmCongnoKH.Execute(Date, '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongnoNCCExecute(Sender: TObject);
begin
    if GetRights('ACC_CONGNO_NCC') = R_DENY then
        Exit;

    Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
    FrmCongnoNCC.Execute(Date, '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongtacExecute(Sender: TObject);
begin
    r := GetRights('HR_CONGTAC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmLichSuCongTac, FrmLichSuCongTac);
    FrmLichSuCongTac.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCongthoExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmDevRAW, FrmDevRAW);
	FrmDevRAW.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCounterExecute(Sender: TObject);
begin
	r := GetRights('SZ_QUAYTN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmquaytn, FrmDmquaytn);
    FrmDmquaytn.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLaydulieucongExecute(Sender: TObject);
begin
    devLogAll := False;
    devLogFromDate := 0;

    Application.CreateForm(TFrmDevLog, FrmDevLog);
    if not FrmDevLog.Execute then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLichlamviecExecute(Sender: TObject);
begin
    r := GetRights('HR_LICHLAMVIEC');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmLichLamViec, FrmLichLamViec);
    FrmLichLamViec.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdListGroupExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListGroup, FrmListGroup);
    FrmListGroup.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdListUserExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmListUser, FrmListUser);
    FrmListUser.Execute(R_FULL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVip2Execute(Sender: TObject);
begin
    r := GetRights('SZ_DM_LOAIVIP_KH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmloaiVIP, FrmDmloaiVIP);
    FrmDmloaiVIP.Execute(r, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVipCt2Execute(Sender: TObject);
begin
    r := GetRights('SZ_DM_LOAIVIP_KH_CT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLoaiVipCT, FrmDmLoaiVipCT);
    FrmDmLoaiVipCT.Execute(r, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVipCTExecute(Sender: TObject);
begin
    r := GetRights('SZ_DM_LOAIVIP_CT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLoaiVipCT, FrmDmLoaiVipCT);
    FrmDmLoaiVipCT.Execute(r);
end;

procedure TFrmMain.CmdLoaiVipCTFBExecute(Sender: TObject);
begin
    r := GetRights('FB_DM_LOAIVIP_CT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_DmLoaiVipCT, FrmFB_DmLoaiVipCT);
    FrmFB_DmLoaiVipCT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLoaiVIPExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_LOAIVIP');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmloaiVIP, FrmDmloaiVIP);
    FrmDmloaiVIP.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdLockExecute(Sender: TObject);
begin
//    DataMain.isEnv1.Lock //NTD
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdFaqExecute(Sender: TObject);
begin
	Faqs;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdMangThaiExecute(Sender: TObject);
begin
    r := GetRights('HR_MANGTHAI');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyMangThai, FrmDangkyMangThai);
    FrmDangkyMangThai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdMyCustomerExecute(Sender: TObject);
begin
	Customers;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCloseAllClick(Sender: TObject);
var
	i: Integer;
begin
	with GbFunc do
   		for i := 0 to GroupCount - 1 do
        	Groups[i].Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.ItemCatClick(Sender: TObject);
begin
	case (Sender as TComponent).Tag of
    0:
    	with GbFunc do
        	if Style <> gbsCategoryView then
	        	Style := gbsCategoryView;
    1:
    	with GbFunc do
        	if Style <> gbsOutlook then
	        	Style := gbsOutlook;
    2:
    	with GbFunc do
        	if Style <> gbsTaskList then
	        	Style := gbsTaskList;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.PopFuncPopup(Sender: TObject);
begin
	with PopFunc, GbFunc do
    begin
    	Items[0].Checked := Style = gbsCategoryView;
    	Items[1].Checked := Style = gbsOutlook;
    	Items[2].Checked := Style = gbsTasklist;
    	Items[4].Enabled := Items[0].Checked;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.TntFormResize(Sender: TObject);
begin
	StatusBarAdjustSize(Status);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdTygiaExecute(Sender: TObject);
begin
    r := GetRights('SZ_PUB_DM_TYGIA');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmTygia, FrmTygia);
    FrmTygia.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdUnlockChungtuExecute(Sender: TObject);
begin
    r := GetRights('SZ_UNLOCK');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmUnlockPhieu, FrmUnlockPhieu);
    FrmUnlockPhieu.Execute();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdVangmatExecute(Sender: TObject);
begin
    r := GetRights('HR_VANGMAT');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmDangkyVangmat, FrmDangkyVangmat);
    FrmDangkyVangmat.Execute(r, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdVantayExecute(Sender: TObject);
begin
    r := GetRights('HR_VANTAY');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmVantay, FrmVantay);
    FrmVantay.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptrabl2Execute(Sender: TObject);
begin
    r := GetRights('SZ_NHAPTRA_BL2');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptrabl2, FrmNhaptrabl2);
    FrmNhaptrabl2.Execute(r);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdNhaptrablExecute(Sender: TObject);
begin
    r := GetRights('SZ_NHAPTRA_BL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmNhaptraBanLe, FrmNhaptraBanLe);
    FrmNhaptraBanLe.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHoaDonBanLeFBExecute(Sender: TObject);
begin
    r := GetRights('FB_HOADON_BANLE');
    if r = R_DENY then
    	Exit;

    Application.CreateForm(TFrmFB_Banle, FrmFB_Banle);
    FrmFB_Banle.Execute(r);
end;

procedure TFrmMain.CmdHoadonGTGTExecute(Sender: TObject);
begin
    r := GetRights('ACC_HOADON_GTGT');
    if r = R_DENY then
    	Exit;

    if not exDefaultWarehouse then
    	Exit;

	Application.CreateForm(TFrmHoadonGTGT, FrmHoadonGTGT);
    FrmHoadonGTGT.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHrDaotaoExecute(Sender: TObject);
begin
    //
end;

procedure TFrmMain.CmdHrHopDongLaoDongExecute(Sender: TObject);
begin

    r := GetRights('HR_HOPDONG_LD');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmHopdongLaodong, FrmHopdongLaodong);
    FrmHopdongLaodong.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHrLichsuExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHrThietbiExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdHrTuyendungExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyemaiComboFBExecute(Sender: TObject);
begin
   r := GetRights('FB_KHUYENMAI_COMBO');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_DmvtBo, FrmFB_DmvtBo);
    FrmFB_DmvtBo.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmai3Execute(Sender: TObject);
begin
    r := GetRights('SZ_KHUYENMAI_DACHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhuyenmai3, FrmKhuyenmai3);
    FrmKhuyenmai3.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmaiChietkhauFBExecute(Sender: TObject);
begin
    r := GetRights('FB_KHUYENMAI_CHIETKHAU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Khuyenmai, FrmFB_Khuyenmai);
    FrmFB_Khuyenmai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmaiExecute(Sender: TObject);
begin
    r := GetRights('SZ_KHUYENMAI');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmKhuyenmai, FrmKhuyenmai);
    FrmKhuyenmai.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmaiFBExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdKhuyenmaidachieuFBExecute(Sender: TObject);
begin
    r := GetRights('FB_KHUYENMAI_DACHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_Khuyenmai3, FrmFB_Khuyenmai3);
    FrmFB_Khuyenmai3.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdDmLyDoKTKLExecute(Sender: TObject);
begin
    r := GetRights('HR_DM_LYDO_KTKL');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmLyDoKTKL, FrmDmLyDoKTKL);
    FrmDmLyDoKTKL.Execute(r);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChamcongExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdCheBienFBExecute(Sender: TObject);
begin
    r := GetRights('FB_CHEBIEN');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmFB_CheBien, FrmFB_CheBien);
    FrmFB_CheBien.Execute(r);
end;

procedure TFrmMain.CmdChi2Execute(Sender: TObject);
begin
    r := GetRights('ACC_CHI_PHIEU');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmChi2, FrmChi2);
    FrmChi2.Execute(r);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMain.CmdChietkhauBanleExecute(Sender: TObject);
begin
    r := GetRights('SZ_CHIETKHAU_BANLE');
    if r = R_DENY then
    	Exit;
end;

initialization
	{$IFDEF _MULTILANG_}
    sysEnglish := GetArg('E');
    if sysEnglish = False then
    else if not	SetNewResourceDll(LANG_ENGLISH) then
    {$ENDIF}
    	sysEnglish := False;
    if sysEnglish then
        DataMain.isEnv1.Language := ENGLISH;
end.

