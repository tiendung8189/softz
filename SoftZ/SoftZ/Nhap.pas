﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Nhap;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,Variants,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Messages, Windows,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Grids, Wwdbgrid, ToolWin,
  Wwdotdot, Wwdbcomb, Buttons, AdvEdit, DBAdvEd, wwcheckbox, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmNhap = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXLK_TENDT: TWideStringField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaMaster: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    QrNXPTTT: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    DMVT_CAPNHAT_GIANHAP: TADOCommand;
    CmdExBarcode: TAction;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdFromOrder: TAction;
    CmdEmptyDetail: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    spDONDH_Select_Full: TADOStoredProc;
    CHECK_NCC: TADOCommand;
    Bevel1: TBevel;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    QrNXTC_SOTIEN: TFloatField;
    PaHoadon: TisPanel;
    PaHoadon2: TPanel;
    TntLabel3: TLabel;
    CbLOAITHUE: TwwDBLookupCombo;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    DBText2: TDBText;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    TntLabel5: TLabel;
    QrNXHOADON_SO: TWideStringField;
    QrNXHOADON_SERI: TWideStringField;
    QrNXHOADON_NGAY: TDateTimeField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrCTLK_VAT_VAO: TFloatField;
    QrNXLCT: TWideStringField;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    btnDmhh: TToolButton;
    btn2: TToolButton;
    CmdDmvt: TAction;
    N4: TMenuItem;
    CmdSapthutu: TAction;
    Splithtmthng1: TMenuItem;
    N5: TMenuItem;
    CmdThamkhaoGia: TAction;
    hamkhoginhp1: TMenuItem;
    QrCTLK_QD1: TIntegerField;
    QrNXSCT2: TWideStringField;
    spUpdateDDH: TADOCommand;
    QrCTQD1: TIntegerField;
    QrNXPHIEUGIAOHANG: TWideStringField;
    QrNXKHOA: TGuidField;
    QrNXKHOA2: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    CmdDmncc: TAction;
    SepChecked: TToolButton;
    QrCTLK_TINHTRANG: TWideStringField;
    BtnIn2: TToolButton;
    QrCTLK_VAT_RA: TFloatField;
    QrNXLYDO: TWideStringField;
    QrNXLK_LYDO: TWideStringField;
    CmdImportExcel: TAction;
    LydliutfileExcel1: TMenuItem;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepBarcode: TToolButton;
    QrCTLOC: TWideStringField;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    CmdFromNhap: TAction;
    Lytphiunhp1: TMenuItem;
    spCHUNGTU_Select_Full: TADOStoredProc;
    CmdCheckton: TAction;
    N7: TMenuItem;
    Kimtraslngtnkho1: TMenuItem;
    N8: TMenuItem;
    ExportraExceltlidliu1: TMenuItem;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    ExportraExceltlidliu2: TMenuItem;
    QrNXNguoiGiao: TWideStringField;
    QrNXNguoiNhan: TWideStringField;
    QrNXTyLeCKHD: TFloatField;
    QrNXSoLuongHop: TFloatField;
    QrNXSoTienCKMH: TFloatField;
    QrNXThanhTienSauCKMH: TFloatField;
    QrNXSoTienCKHD: TFloatField;
    QrNXThanhTienSauCKHD: TFloatField;
    QrNXLoaiThue: TWideStringField;
    QrNXThueSuat: TFloatField;
    QrNXThanhToanChuaCL: TFloatField;
    QrNXSoTienCL: TFloatField;
    QrNXThanhToanChuaThue: TFloatField;
    QrNXSoTienThue: TFloatField;
    QrNXSoTienThue_5: TFloatField;
    QrNXSoTienThue_10: TFloatField;
    QrNXSoTienThue_Khac: TFloatField;
    QrNXThanhTienSi: TFloatField;
    QrNXThanhTienLe: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrNXHanThanhToan: TIntegerField;
    QrNXHinhThuc_Gia: TWideStringField;
    QrNXCALC_NgayThanhToan: TDateTimeField;
    QrNXLK_HanThanhToan: TIntegerField;
    QrNXThanhTien: TFloatField;
    QrNXThanhToan: TFloatField;
    QrCTLK_Tenvt: TWideStringField;
    QrCTLK_Dvt: TWideStringField;
    QrCTLK_DvtHop: TWideStringField;
    QrCTSoLuongHop: TFloatField;
    QrCTSoLuongLe: TFloatField;
    QrCTDonGiaThamKhao: TFloatField;
    QrCTDonGiaHop: TFloatField;
    QrCTTyLeCKMH: TFloatField;
    QrCTSoTienCKMH: TFloatField;
    QrCTThanhTienSauCKMH: TFloatField;
    QrCTTyLeCKHD: TFloatField;
    QrCTSoTienCKHD: TFloatField;
    QrCTThanhTienSauCKHD: TFloatField;
    QrCTThanhToanChuaCL: TFloatField;
    QrCTSoTienCL: TFloatField;
    QrCTThanhToanChuaThue: TFloatField;
    QrCTThueSuat: TFloatField;
    QrCTSoTienThue: TFloatField;
    QrCTSoTienThue_5: TFloatField;
    QrCTSoTienThue_10: TFloatField;
    QrCTSoTienThue_Khac: TFloatField;
    QrCTThanhToan: TFloatField;
    QrCTDonGiaSi: TFloatField;
    QrCTThanhTienSi: TFloatField;
    QrCTDonGiaLe: TFloatField;
    QrCTThanhTienLe: TFloatField;
    QrCTTyLeLai: TFloatField;
    QrCTTyLeLaiSi: TFloatField;
    QrCTHanSuDung: TDateTimeField;
    QrCTMaBo: TWideStringField;
    QrCTLK_GiaVon: TFloatField;
    QrCTLK_GiaNhapChuaThue: TFloatField;
    QrCTLK_GiaNhap: TFloatField;
    QrCTLK_GiaSi: TFloatField;
    QrCTLK_LOAITHUE: TWideStringField;
    QrCTCALC_DonGiaThamKhaoHop: TFloatField;
    QrCTGiaVon: TFloatField;
    QrCTDonGia: TFloatField;
    QrCTSoLuong: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTLoaiThue: TWideStringField;
    QrCTLK_TenThue2: TWideStringField;
    QrCTGhiChu: TWideStringField;
    CbLyDo: TDbLookupComboboxEh2;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMADT: TDBEditEh;
    CmdXemCongNo: TAction;
    QrCTDonGiaCKMH: TFloatField;
    QrCTDonGiaCKHD: TFloatField;
    QrCTDonGiaChuaThue: TFloatField;
    QrCTSoTienThueChuaRound: TFloatField;
    QrNXSoTienThueChuaRound: TFloatField;
    QrCTThanhToanTinhThue: TFloatField;
    QrCTDonGiaTinhThue: TFloatField;
    CbGia: TDbLookupComboboxEh2;
    CmdRecalc: TAction;
    QrNXThanhToanTinhThue: TFloatField;
    QrCTDonGiaSiChuaThue: TFloatField;
    QrCTDonGiaLeChuaThue: TFloatField;
    QrNXSoLuong: TFloatField;
    EdSoPhieu: TDBEditEh;
    EdPhieuGiaoHangSo: TDBEditEh;
    DBEditEh1: TDBEditEh;
    DBEditEh2: TDBEditEh;
    EdNumHanThanhToan: TDBNumberEditEh;
    DBEditEh3: TDBEditEh;
    DBMemoEh1: TDBMemoEh;
    EdSOHD: TDBEditEh;
    DBEditEh4: TDBEditEh;
    CmdThue: TAction;
    DBEditEh5: TDBEditEh;
    QrCTLK_MaNoiBo: TWideStringField;
    QrCTSoLuongPhieuGoc: TFloatField;
    PaTotal: TPanel;
    ImgTotal: TImage;
    PaThanhtoan: TPanel;
    EdTriGiaTT: TDBNumberEditEh;
    PaSotien1: TPanel;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    PaSotien: TPanel;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    PaSotien2: TPanel;
    EdTLCK: TDBNumberEditEh;
    DBNumberEditEh6: TDBNumberEditEh;
    DBNumberEditEh5: TDBNumberEditEh;
    PaThue: TPanel;
    DBNumberEditEh7: TDBNumberEditEh;
    EdTienVAT: TDBNumberEditEh;
    PopIn: TAdvPopupMenu;
    heogibn1: TMenuItem;
    N11: TMenuItem;
    Khngngi1: TMenuItem;
    DBEditEh6: TDBEditEh;
    QrNXHoaDon_Mau: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrCTMAVTValidate(Sender: TField);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdExBarcodeExecute(Sender: TObject);
    procedure CmdFromOrderExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrNXMADTChange(Sender: TField);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTThanhTienChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdDmvtExecute(Sender: TObject);
    procedure CmdSapthutuExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CmdThamkhaoGiaExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure CmdDmnccExecute(Sender: TObject);
    procedure QrCTLOAITHUEChange(Sender: TField);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdFromNhapExecute(Sender: TObject);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure QrNXTyLeCKHDChange(Sender: TField);
    procedure CmdXemCongNoExecute(Sender: TObject);
    procedure QrCTThanhTienSauCKMHChange(Sender: TField);
    procedure QrCTThanhTienSauCKHDChange(Sender: TField);
    procedure QrCTThanhToanChange(Sender: TField);
    procedure QrNXSoTienThueChuaRoundChange(Sender: TField);
    procedure QrCTThanhToanTinhThueChange(Sender: TField);
    procedure CmdRecalcExecute(Sender: TObject);
    procedure CmdThueExecute(Sender: TObject);
    procedure CbKhoHangDropDown(Sender: TObject);
    procedure CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);

  private
	mLCT: String;
	mCanEdit, b1Ncc, mLuugianhap, bDuplicate, mTriggerDG, mTriggerSL,
        bClSotien, bCk, bCkHD, mObsolete, bAllowChangePriceForm: Boolean;
	mLydo, mMakho, mPrefix, mHTGia: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

	    (*
    	** Functions
	    *)
    procedure CapnhatGianhap;
    procedure UpdateDDH;
    procedure escapeKey(pSleepTime: Variant);
  public
	procedure Execute(r: WORD);
  end;

var
  FrmNhap: TFrmNhap;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, Dmvt, Sapthutu, GuidEx, isCommon,
    ChonDsma, isLib, ChonDondh2, TheodoiGia, exThread, Dmncc, DmKhNcc, isFile,
    ImportExcel, Tienthue, CongnoNCC, MasterData, InlabelChungtu, ChonPhieunhap,
  CheckTonkho, OfficeData;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAP';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.Execute;
begin
    mLCT := 'NMUA';

    // Audit setting
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormCreate(Sender: TObject);
begin
    frDate.Init;
    frNavi.DataSet := QrNX;
	ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

	// Params
    b1Ncc := FlexConfigBool(FORM_CODE, '1 nha cung cap');
//    ckDatt.Visible := FlexConfigBool(FORM_CODE, 'Checkbox "Da thanh toan"');
    mLuuGianhap := FlexConfigBool(FORM_CODE, 'Cap nhat gia');
    CmdFromOrder.Visible := FlexConfigBool(FORM_CODE, 'Lay tu DDH');
    mPrefix := FlexConfigString('DM_HH', 'Barcode Prefix');

    bClSotien := FlexConfigBool(FORM_CODE, 'Chenhlech Sotien');
    bCk := FlexConfigBool(FORM_CODE, 'Chietkhau');
    bCkHD := FlexConfigBool(FORM_CODE, 'Chietkhau Hoadon');

    CmdImportExcel.Visible := FlexConfigBool(FORM_CODE, 'Import Excel');
    bDuplicate := FlexConfigBool(FORM_CODE, 'Duplicate Mavt');

    bAllowChangePriceForm := FlexConfigBool(FORM_CODE, 'Combobox "Thay doi hinh thuc gia"');

    //Lay gia tri mac dinh cho HTGia
    mHTGia := sysHTGia;

    if mHTGia <> '' then
    begin
        cbGia.Visible := False;
    end;

    // Initial
    mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := RegReadString(Name, 'Lydo', GetSysParam('DEFAULT_PTNHAP'));

  	mTrigger := False;
    mTriggerCK := False;
    mTriggerDG := False;
    mTriggerSL := False;
    mObsolete := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2('_IDI_RECEIPT');

    Wait(PREPARING);
    InitFmtAndRoud(mLct);

	// Open database
	with DataMain do
    begin
		OpenDataSets([QrDMVT, QrPTNHAP, QrDMLOAITHUE, QrDMNCC, QrDMKHO, QrHINHTHUC_GIA]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TyLeLai'], sysPerFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, ctCurFmt);
    	SetDisplayFormat(QrNX, ['TyLeCKHD'], sysPerFmt);
        SetDisplayFormat(QrNX, ['SoLuong'], ctQtyFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, ctCurFmt);
    	SetDisplayFormat(QrCT, ['SoLuong', 'SoLuongHop', 'SoLuongLe'], ctQtyFmt);
	    SetDisplayFormat(QrCT, ['TyLeCKMH', 'TyLeCKHD', 'TyLeLai', 'TyLeLaiSi'], sysPerFmt);
        SetDisplayFormat(QrCT, ['DonGia', 'DonGiaHop', 'DonGiaThamKhao', 'CALC_DonGiaThamKhaoHop',
                                    'DonGiaSi', 'DonGiaLe'], ctPriceFmt);
        SetDisplayFormat(QrCT, ['ThueSuat'], sysTaxFmt);
        SetDisplayFormat(QrCT, ['SoTienThue'], sysFloatFmtTwo);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not bCk then
    begin
        PaSotien1.Visible := False;
        grRemoveFields(GrDetail, ['TyLeCKMH', 'SoTienCKMH', 'ThanhTienSauCKMH']);
    end;

    if not bCkHD then
    begin
        PaSotien2.Visible := False;
        grRemoveFields(GrDetail, ['TyLeCKHD', 'SoTienCKHD', 'ThanhTienSauCKHD']);
    end;

    if not bClSotien then
    begin
        grRemoveFields(GrDetail, ['SoTienCL', 'ThanhToanChuaCL']);
    end;

    if not sysIsThue then
    begin
        PaThue.Visible := False;
        PaHoadon.Visible := False;
        grRemoveFields(GrBrowse, ['SoTienThue', 'HOADON_SO', 'HOADON_SERI', 'HOADON_NGAY']);
        grRemoveFields(GrDetail, ['ThanhToanChuaThue', 'LK_TenThue', 'ThueSuat',
                'SoTienThue', 'ThanhToan']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsBarcode then
    begin
        CmdExBarcode.Visible := False;
        SepBarcode.Visible := False;

        grRemoveFields(GrDetail, ['B1']);
    end;

    PaSotien.Visible := bCk or bCkHD or sysIsThue;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Lydo'], [mMakho, mLydo]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

	(*
	** Actions
	*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

	with DataMain do
    begin
    	QrDMVT.Requery;
        QrPTNHAP.Requery;
        QrDMNCC.Requery;
        QrDMKHO.Requery;
    end;
            
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdRecalcExecute(Sender: TObject);
var
    bm: TBytes;
begin
    with QrCT do
    begin
        DisableControls;
        bm := BookMark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('ThanhToanTinhThue').AsFloat := FieldByName('ThanhToanTinhThue').AsFloat;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from CHUNGTU_CT a, DM_VT_FULL b where a.KHOA = CHUNGTU.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from CHUNGTU_CT where KHOA = CHUNGTU.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
    	    if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSapthutuExecute(Sender: TObject);
begin
	CmdSave.Execute;
    Application.CreateForm(TFrmSapthutu, FrmSapthutu);
    if FrmSapthutu.Execute(QrCT) then
    begin
    	with QrCT do
        begin
            UpdateBatch;
            Requery;
        end;
        GrDetail.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSaveExecute(Sender: TObject);
begin
    DataMain.Conn.BeginTrans;
    try
        QrCT.CheckBrowseMode;
        QrNX.Post;
        exSaveDetails(QrCT);
        UpdateDDH;
        CapnhatGianhap;
        DataMain.Conn.CommitTrans;
    except
        ErrMsg('Có lỗi trong quá trình thực hiện, vui lòng thực hiện lại.');
        DataMain.Conn.RollbackTrans;
        CmdReRead.Execute;
        ActiveSheet(PgMain, 0);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmCheckTonkho, FrmCheckTonkho);
    with QrCT do
	    if FrmCheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('LK_Tenvt').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDelExecute(Sender: TObject);
begin
    exCheckLoc(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
    begin
        DataMain.Conn.BeginTrans;
        try
            MarkDataSet(QrNX);
            UpdateDDH;
            DataMain.Conn.CommitTrans;
        except
            ErrMsg('Có lỗi trong quá trình thực hiện, vui lòng thực hiện lại.');
            DataMain.Conn.RollbackTrans;
            CmdReRead.Execute;
            ActiveSheet(PgMain, 0);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDmnccExecute(Sender: TObject);
var
    r: WORD;
    mRet: Boolean;
    loai: Integer;
begin
    if GetFuncState('SZ_DM_KH_NCC') then
    begin
        r := GetRights('SZ_DM_KH_NCC');
        if r = R_DENY then
            Exit;

        loai := -1;
    end else
    begin
        r := GetRights('SZ_DM_NCC');
        if r = R_DENY then
            Exit;

        loai := 1;
    end;

    Application.CreateForm(TFrmDmKhNcc, FrmDmKhNcc);
    FrmDmKhNcc.Execute(r, loai, False);

    if mRet then
        DataMain.QrDmncc.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdDmvtExecute(Sender: TObject);
var
    r: WORD;
    mRet : Boolean;
begin
    r := GetRights('SZ_DM_MAVT');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmvt, FrmDmvt);
    mRet := FrmDmvt.Execute(r, False);

    if mRet then
    	DataMain.QrDMVT.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		EdTLCK.SetFocus;
        except
        end
    else if (ActiveControl = PaTotal) or (ActiveControl.Parent = PaTotal) then
		try
    		CbNgay.SetFocus;
        except
        end
    else if (ActiveControl = PaHoadon2) or (ActiveControl.Parent = PaHoadon2) then
        GrDetail.SetFocus
    else
    	try
    		EdSOHD.SetFocus;
        except
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsNX)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdThamkhaoGiaExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmTheodoiGia, FrmTheodoiGia);
    with QrNX do
	    FrmTheodoiGia.Execute(mLCT,
            QrCT.FieldByName('MAVT').AsString,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdThueExecute(Sender: TObject);
var
    p: TPoint;
begin
    p.x := GrDetail.Left + GrDetail.Width;
    p.y := PaTotal.Top;
    p := ClientToScreen(p);

    Application.CreateForm(TFrmTienthue, FrmTienthue);
    with FrmTienthue do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y - Height + 2;
    	Execute(DsNX);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdTotalExecute(Sender: TObject);
var
    candoi: Double;
begin
    candoi := 0;
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('ThueSuat').AsFloat <> 0 then
            begin
                Break;
            end;

            Next;
        end;

        if FieldByName('SoTienCL').AsFloat <> 0 then
        begin
            Edit;
            FieldByName('SoTienCL').AsFloat := 0;
            CheckBrowseMode;
        end;
    end;

    with QrNX do
    begin
        if (FieldByName('ThanhToan').AsFloat <> 0) then
        begin
            if FieldByName('HinhThuc_Gia').AsString = sysGiaCoVAT then
                candoi := FieldByName('ThanhToan').AsFloat
                            - FieldByName('SoTienThue').AsFloat
                            - FieldByName('ThanhToanChuaThue').AsFloat
            else
                candoi := FieldByName('ThanhToanChuaThue').AsFloat
                            + FieldByName('SoTienThue').AsFloat
                            - FieldByName('ThanhToan').AsFloat
        end;
    end;

    if candoi <> 0 then
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('ThueSuat').AsFloat <> 0 then
            begin
                Break;
            end;

            Next;
        end;

        Edit;
        FieldByName('SoTienCL').AsFloat := candoi;
        CheckBrowseMode;
    end;
	vlTotal1.Sum;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdXemCongNoExecute(Sender: TObject);
begin
    if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrNX do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED_PN = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';
procedure TFrmNhap.CmdFromNhapExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue: String;
    mSoluong, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;

	// Chon Don dat hang
	Application.CreateForm(TFrmChonPhieunhap, FrmChonPhieunhap);
    with QrNX do
	    n := FrmChonPhieunhap.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString);

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with spCHUNGTU_Select_Full do
    begin
    	Prepared := True;
        Parameters.ParamByName('@KHOA').Value := TGuidEx.ToString(n);;
        ExecProc;

        // Co mat hang sai ma
        if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED_PN, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            // Cap nhat nha cung cap
            mNcc := FieldByName('MADT').AsString;
            mKho := FieldByName('MAKHO').AsString;
            mSCT2 := FieldByName('SCT').AsString;
            mHTGia := FieldByName('HinhThuc_Gia').AsString;
            mTlckHD := FieldByName('TyLeCKHD').AsFloat;


            if (QrNX.FieldByName('MADT').AsString <> mNcc) or
               (QrNX.FieldByName('MAKHO').AsString <> mKho) then
            begin
                SetEditState(QrNX);
                QrNX.FieldByName('MADT').AsString := mNcc;
                QrNX.FieldByName('MAKHO').AsString := mKho;
                QrNX.FieldByName('HinhThuc_Gia').AsString := mHTGia;
                QrNX.FieldByName('TyLeCKHD').AsFloat := mTlckHD;
    //            QrNX.FieldByName('SCT2').AsString := mSCT2;
    //            QrNX.FieldByName('KHOA2').AsInteger := n;
            end;


            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MAVT').AsString;
                mSoluong := FieldByName('SoLuong').AsFloat;
                mDongia := FieldByName('DonGia').AsFloat;
                mTlck := FieldByName('TyLeCKMH').AsFloat;
                mLThue := FieldByName('LoaiThue').AsString;

                with QrCT do
                if not Locate('MAVT', mMavt, []) then
                begin
                    Append;
                    FieldByName('MAVT').AsString := mMavt;
                    FieldByName('DonGia').AsFloat := mDongia;
                    FieldByName('SoLuong').AsFloat := mSoluong;
                    FieldByName('TyLeCKMH').AsFloat := mTlck;
                    FieldByName('LoaiThue').AsString := mLThue;
                end;

                Next;
            end;

            Active := False;
            QrCT.CheckBrowseMode;

        finally
            ClearWait;
        end;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Đơn đặt hàng có mặt hàng sai mã. Tiếp tục?';
procedure TFrmNhap.CmdFromOrderExecute(Sender: TObject);
var
	n: TGUID;
    mNcc, mKho, mMavt, mSCT2, mHTGia, mLThue, sTinhTrang: String;
    mSoluong, mSoluongConlai, mDongia, mTlck, mTlckHD: Double;
begin
	QrCT.CheckBrowseMode;
    sTinhTrang := sysDDH_TT_APPR + ','+ sysDDH_TT_PART;

    // Chon Don dat hang
	Application.CreateForm(TFrmChonDondh2, FrmChonDondh2);
    with QrNX do
	    n := FrmChonDondh2.Execute(
        	not QrCT.IsEmpty,
        	FieldByName('MADT').AsString,
            FieldByName('MAKHO').AsString, 'DM_NCC', 'DHN', sTinhTrang);
	FrmChonDondh2.Free;

    if TGuidEx.IsEmptyGuid(n) then
    	Exit;

    // Lay chi tiet
    with spDONDH_Select_Full do
    begin
    	Prepared := True;
        Parameters.ParamByName('@KHOA').Value := TGuidEx.ToString(n);
        ExecProc;

        // Co mat hang sai ma
        if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
        	if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
            	Exit;

        // Khong co mat hang nao
        Active := True;
        if IsEmpty then
        begin
            Active := False;
            Exit;
        end;

        try
            Wait('Đang xử lý...');
            First;
            // Cap nhat nha cung cap
            mNcc := FieldByName('MADT').AsString;
            mKho := FieldByName('MAKHO').AsString;
            mSCT2 := FieldByName('SCT').AsString;
            mHTGia := FieldByName('HinhThuc_Gia').AsString;
            mTlckHD := FieldByName('TyLeCKHD').AsFloat;

            if (QrNX.FieldByName('MADT').AsString <> mNcc) or
               (QrNX.FieldByName('MAKHO').AsString <> mKho) or
               (QrNX.FieldByName('SCT2').AsString <> mSCT2) then
            begin
                SetEditState(QrNX);
                QrNX.FieldByName('MADT').AsString := mNcc;
                QrNX.FieldByName('MAKHO').AsString := mKho;
                QrNX.FieldByName('HinhThuc_Gia').AsString := mHTGia;
                QrNX.FieldByName('TyLeCKHD').AsFloat := mTlckHD;
                QrNX.FieldByName('SCT2').AsString := mSCT2;
                TGuidField(QrNX.FieldByName('KHOA2')).AsGuid := n;
            end;

            // Chi tiet
            while not Eof do
            begin
                mMavt := FieldByName('MAVT').AsString;
                mSoluong := FieldByName('SoLuong').AsFloat;
                mSoluongConlai := FieldByName('SoLuongConLai').AsFloat;
                mDongia := FieldByName('DonGia').AsFloat;
                mTlck := FieldByName('TyLeCKMH').AsFloat;
                mLThue := FieldByName('LoaiThue').AsString;

                with QrCT do
                begin
                    if mSoluongConlai > 0 then
                    begin
                        Append;
                        FieldByName('MAVT').AsString := mMavt;
                        FieldByName('DonGia').AsFloat := mDongia;
                        FieldByName('SoLuongPhieuGoc').AsFloat := mSoluong;
                        FieldByName('SoLuong').AsFloat := mSoluongConlai;
                        FieldByName('TyLeCKMH').AsFloat := mTlck;
                        FieldByName('LoaiThue').AsString := mLThue;
                    end;
                end;

                Next;
            end;

            Active := False;
            QrCT.CheckBrowseMode;
        finally
            ClearWait;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdImportExcelExecute(Sender: TObject);
var
    s, sFld, mFile: string;
    i, k, n: Integer;
    sLog: TStrings;
    b: Boolean;
begin
    if BlankConfirm(QrNX, ['MADT', 'HinhThuc_Gia']) then
        Abort;

    // Get file name
    mFile := isGetOpenFileName('XLSX;XLS', 1, sysAppPath);;
    if mFile = '' then
    	Exit;

    sLog := TStringList.Create();
    //File excel
    if SameText(Copy(ExtractFileExt(mFile), 2, 3), 'XLS') then
    begin
        b := FrmImportExcel.Execute(mFile, FORM_CODE, QrCT, '', False, 'MAVT');
        if b then
        try
            with FrmImportExcel.QrExcel do
            begin
                k := -1;
                n :=  FieldCount - 1;
                try
                    for i := 0 to n do
                    begin
                        if SameText(Fields[i].DisplayLabel, 'MAVT') then
                        begin
                            sFld := Fields[i].FieldName;
                            k := i;
                            Break;
                        end;
                    end;
                    s := FieldByName(sFld).AsString;
                except
                    FrmImportExcel.DisConnectToExcel;
                    ErrMsg(RS_INVALID_DATA);
                    Exit;
                end;

                First;
                while not Eof do
                begin
                    s := TrimRight(TrimLeft(FieldByName(sFld).AsString));
                    if s <> '' then
                    begin
                        if not DataMain.QrDMVT.Locate('MAVT;TINHTRANG', VarArrayOf([s, '01']), []) then
                        begin
                            sLog.Add(s + #9 + ';Error: Sai mã hàng');
                            Next;
                            Continue;
                        end;

                        if bDuplicate then
                        begin
                            QrCT.Append;
                            QrCT.FieldByName('MAVT').AsString := s
                        end else
                        begin
                            if (not QrCT.Locate('MAVT', s, []))  then
                            begin
                                QrCT.Append;
                                QrCT.FieldByName('MAVT').AsString := s
                            end
                            else
                                SetEditState(QrCT);
                        end;

                        for i := 0 to n do
                            if i <> k then
                                QrCT.FieldByName(Fields[i].DisplayLabel).Value := Fields[i].Value;
                        QrCT.CheckBrowseMode;
                    end;

                    Next;
                end;
            end;
        except
            ErrMsg(RS_INVALID_DATA);
            b := False;
        end;
    end;

    FrmImportExcel.DisConnectToExcel;
    if sLog.Count > 0 then
        ShowLog('importError.log', sLog);
    sLog.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdExBarcodeExecute(Sender: TObject);
begin
	CmdSave.Execute;
    with QrNX do
    begin
        Application.CreateForm(TFrmInlabelChungtu, FrmInlabelChungtu);
        FrmInlabelChungtu.Excecute(TGuidField(FieldByName('KHOA')).AsGuid, mLCT);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdPrintExecute(Sender: TObject);
var
    n: Integer;
    repName: string;
begin
	CmdSave.Execute;

    n := (Sender as TComponent).Tag;
    case n of
        0: repName := FORM_CODE;
    else
        repName := FORM_CODE + '_' + IntToStr(n);
    end;

    ShowReport(Caption, repName,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT, bDondh: Boolean;
    n: Integer;
begin
	// Master
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
        bDondh := FieldByName('SCT2').AsString <> '';
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn2.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
	CmdRefresh.Enabled := bBrowse;
    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdTotal.Enabled  := n = 1;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    CbbNhaCungCap.Enabled := not bDondh;

	// Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;

    if b1Ncc then
	    CbbNhaCungCap.ReadOnly := not bEmptyCT;
    CmdEmptyDetail.Enabled := not bEmptyCT;
    CmdThamkhaoGia.Enabled := not bEmptyCT;
    CbGia.ReadOnly := not bEmptyCT and not bAllowChangePriceForm;
    CmdFromOrder.Enabled := bEmptyCT;

    CmdCheckton.Enabled := not bEmptyCT;
    CmdExBarcode.Enabled := (not bEmpty) and (not bEmptyCT) and (not bDeleted);
end;

    (*
    **  Master DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
//		FieldByName('HOADON_NGAY').AsDateTime := d;
		FieldByName('LYDO').AsString          := mLydo;
		FieldByName('MAKHO').AsString         := mMakho;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('HinhThuc_Gia').AsString  := Iif(mHTGia = '', '03', mHTGia);
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['NGAY', 'MAKHO', 'MADT']) then
        	Abort;
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;

 	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		mMakho := FieldByName('MAKHO').AsString;
        mLydo := FieldByName('LYDO').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	with DataSet do
    begin
		// Validate: da co phieu thu / chi
    	if FieldByName('TC_SOTIEN').AsFloat <> 0 then
	    begin
    	    ErrMsg(RS_DA_THUCHI);
        	Abort;
	    end;

    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
    with QrNX do
    begin
        if FieldByName('HanThanhToan').IsNull then
        begin
            FieldByName('CALC_NgayThanhToan').Clear;
        end
        else
            FieldByName('CALC_NgayThanhToan').AsDateTime :=
                FieldByName('NGAY').AsDateTime +
                FieldByName('HanThanhToan').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CHANGE_PRICE_FORM = 'Hình thức giá đã thay đổi, phải tính lại giá cả.';
procedure TFrmNhap.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
	if mTrigger then
    	Exit;

	if QrCT.IsEmpty then
    	Exit;

    Msg(RS_INVALID_CHANGE_PRICE_FORM);
    CmdRecalc.Execute;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.UpdateDDH;
begin
    if QrNX.FieldByName('SCT2').AsString = '' then
        Exit;

    with spUpdateDDH do
    begin
        Prepared := True;
        Parameters.ParamByName('@KHOADH').Value := TGuidEx.ToString(QrNX.FieldByName('KHOA2'));
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXMADTChange(Sender: TField);
begin
//	exDotMadt(DataMain.QrDMNCC, Sender);
    with QrNX do
    begin
        // Default han thanh toan
        FieldByName('HanThanhToan').AsInteger := FieldByName('LK_HanThanhToan').AsInteger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXTyLeCKHDChange(Sender: TField);
var
    bTrigger: Boolean;
    tlck, ck, sotien1: Double;
begin
    if mTrigger then
    	Exit;

	with QrNX do
    begin
        bTrigger := mTrigger;
        mTrigger := True;

        sotien1 := FieldByName('ThanhTienSauCKMH').AsFloat;
        if Sender.FieldName = 'SoTienCKHD' then
        begin
            ck := FieldByName('SoTienCKHD').AsFloat;
            if ck = 0 then
                tlck := 0
            else
                tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, Sotien1)));

            FieldByName('TyLeCKHD').AsFloat := tlck;
        end else
        begin
            tlck := FieldByName('TyLeCKHD').AsFloat;
            ck := exVNDRound(sotien1 * tlck / 100.0, ctCurRound);

            FieldByName('SoTienCKHD').AsFloat := ck;
        end;

        mTrigger := bTrigger;
    end;

    if Sender.FieldName <> 'ThanhTienSauCKMH' then
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('TyLeCKHD').AsFloat := tlck;

            Next;
        end;

        CheckBrowseMode;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrNXSoTienThueChuaRoundChange(Sender: TField);
begin
    with QrNX do
    begin
        FieldByName('SoTienThue').AsFloat := exVNDRound(FieldByName('SoTienThueChuaRound').AsFloat, ctCurRound)
    end;
end;

(*
    ** End: Master DB
    *)

    (*
    **  Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_GIAVON_LONHON_GIABAN = 'Giá vốn nhập hàng > Giá bán lẻ. Xác nhận?';
procedure TFrmNhap.QrCTBeforePost(DataSet: TDataSet);
var
    mGiaban, mSL, mThanhtien: Double;
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_TINHTRANG').AsString <> '01' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

//        mGiaban := FieldByName('DONGIA_LE').AsFloat;
//        mSL := FieldByName('SOLUONG').AsFloat;
//        mThanhtien := FieldByName('THANHTIEN').AsFloat;
//
//        if (mSL <> 0) then
//        begin
//            if mThanhtien/mSL > mGiaban then
//                if not YesNo(RS_GIAVON_LONHON_GIABAN, 1) then
//                    Abort;
//        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
   		Abort;
    SetEditState(QrNX);
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if BlankConfirm(QrNX, ['MADT', 'HinhThuc_Gia']) then
        Abort;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
        FieldByName('CALC_DonGiaThamKhaoHop').AsFloat :=
            FieldByName('DonGiaThamKhao').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTLOAITHUEChange(Sender: TField);
begin
    with QrCT do
    begin
        if (FieldByName('LoaiThue').AsString = 'TTT') or (FieldByName('LoaiThue').AsString = 'KCT') then
            FieldByName('ThueSuat').Clear
        else
            FieldByName('ThueSuat').AsFloat := FieldByName('LK_VAT_VAO').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTMAVTValidate(Sender: TField);
var
	s: String;
    bm: TBytes;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
	if s = '' then
    	Exit;

    if not bDuplicate then
    //Trung mat hang
    with QrCT do
    begin
        if IsDuplicateCode2(QrCT, Sender, bm) then
        begin
            //if YesNo(RS_DUPLICATE_CODE) then
            begin
                try
                    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
                    Abort;
                finally
                    Cancel;
                    GotoBookmark(bm);
                    Edit;
                    mgMyThread := TExThread.Create(escapeKey, 50);
                end;
            end;
        end;
    end;

	// Nhieu nha cung cap
	if not b1Ncc then
    	Exit;

	// Validate MADT
    if IsDotSelect(s) = 0 then
        with CHECK_NCC do
        begin
            Prepared := True;
            Parameters.ParamByName('MAVT').Value := s;
            Parameters.ParamByName('MADT').Value := QrNX.FieldByName('MADT').AsString;
            if Execute.RecordCount < 1 then
            begin
                ErrMsg(RS_ITEM_CODE_FAIL2);
                Abort;
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTMAVTChange(Sender: TField);
var
	ck, giaVon, gia, giaSI, giaLE, donGiaSiChuaThue, donGiaLeChuaThue : Double;
    lthue: String;
    Qd1: Integer;
begin
    if b1Ncc then
		exDotMavt(1, Sender, 'MADT=''' + QrNX.FieldByName('MADT').AsString + '''')
    else
		exDotMavt(1, Sender);

    // Update referenced fields
    with QrCT do
    begin
        ck := 0;  gia := 0; giaSI := 0; giaLE := 0; lthue := 'T00';
        with DataMain.spCHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@MABH').Value := Sender.AsString;
            Parameters.ParamByName('@MADT').Value := QrNX.FieldByName('MADT').AsString;
            Parameters.ParamByName('@HINHTHUC_GIA').Value := QrNX.FieldByName('HinhThuc_Gia').AsString;

            Open;
            if RecordCount <> 0 then
            begin
                Qd1 := FieldByName('QD1').AsInteger;
                ck := FieldByName('TyLeCKMH').AsFloat;
                giaVon := FieldByName('GiaVon').AsFloat;
                gia := FieldByName('DonGia').AsFloat;
                giaSI := FieldByName('DonGiaSi').AsFloat;
                giaLE := FieldByName('DonGiaLe').AsFloat;
                lthue := FieldByName('LoaiThue').AsString;
                donGiaSiChuaThue := FieldByName('GiaSiChuaThue').AsFloat;
                donGiaLeChuaThue := FieldByName('GiaBanChuaThue').AsFloat;
            end;
        end;
        FieldByName('QD1').AsInteger := Qd1;
        FieldByName('DonGiaSiChuaThue').AsFloat := donGiaSiChuaThue;
        FieldByName('DonGiaLeChuaThue').AsFloat := donGiaLeChuaThue;

        FieldByName('LoaiThue').AsString := lthue;
        FieldByName('GiaVon').AsFloat := giaVon;
        FieldByName('DonGiaSi').AsFloat := giaSI;
        FieldByName('DonGiaLe').AsFloat := giaLE;
        FieldByName('DonGiaThamKhao').AsFloat := gia;
        FieldByName('DonGia').AsFloat := gia;
        FieldByName('TyLeCKMH').AsFloat := ck;

        FieldByName('TyLeCKHD').AsFloat := QrNX.FieldByName('TyLeCKHD').AsFloat;

        if DataMain.IsPrintStamp(FieldByName('MAVT').AsString) then
            FieldByName('B1').AsBoolean := True;
	end;

	GrDetail.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
    Qd1, dongia, dongiaHop: Double;
    mavt: String;
begin
    if mTriggerDG then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            Qd1 := FieldByName('QD1').AsInteger; if Qd1 = 0 then Qd1 := 1;
            dongia := FieldByName('DonGia').AsFloat;
            dongiaHop := FieldByName('DonGiaHop').AsFloat;

            if Sender.FieldName = 'DonGiaHop' then
            begin
                dongia := exVNDRound(dongiaHop / Qd1, ctPriceRound);
            end else
            begin
                dongiaHop := exVNDRound(dongia * Qd1, ctPriceRound);
            end;

            bTrigger := mTriggerDG;
            mTriggerDG := True;
            FieldByName('DonGia').AsFloat := dongia;
            FieldByName('DonGiaHop').AsFloat := dongiaHop;
            mTriggerDG := bTrigger;

//            bTrigger := mTrigger;
//            mTrigger := True;
//            if Sender.FieldName = 'DonGia' then
//            begin
//                f := exVNDRound(Sender.AsFloat, ctPriceRound);
//
//                FieldByName('DonGia').AsFloat := f;
//                FieldByName('DonGiaHop').AsFloat := exVNDRound(
//                    f * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
//            end
//            else if Sender.FieldName = 'DonGiaHop' then
//            begin
//                f := exVNDRound(Sender.AsFloat, ctPriceRound);
//
//                FieldByName('DonGiaHop').AsFloat := f;
//                FieldByName('DonGia').AsFloat := exVNDRound(
//                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
//            end;
//            mTrigger := bTrigger;

        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    Qd1, thanhTien, x, sl, sl2, sl2le: Double;
begin
    if mTriggerSL then
    	Exit;

	with QrCT do
    begin
        Qd1 := FieldByName('QD1').AsInteger; if Qd1 = 0 then Qd1 := 1;

        bTrigger := mTriggerSL;
        mTriggerSL := True;
        if Sender <> Nil then
            if Sender.FullName = 'SoLuong' then
            begin
                sl := exVNDRound(Sender.AsFloat, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
                x := exVNDRound(sl / Qd1, ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Qd1);

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;
            end
            else if (Sender.FullName = 'SoLuongHop') or (Sender.FullName = 'SoLuongLe') then
            begin

                sl2 := exVNDRound(FieldByName('SoLuongHop').AsFloat, ctQtyRound);
                sl2le := exVNDRound(FieldByName('SoLuongLe').AsFloat, ctQtyRound);

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;

                sl := exVNDRound((sl2 * Qd1) + sl2le, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
            end;
        mTriggerSL := bTrigger;

        thanhTien := exVNDRound(FieldByName('SoLuong').AsFloat * FieldByName('DonGia').AsFloat, ctCurRound);
        FieldByName('ThanhTienSi').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaSi').AsFloat, ctCurRound);
        FieldByName('ThanhTienLe').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaLe').AsFloat, ctCurRound);

        QrCTThanhTienChange(Sender);
//        FieldByName('ThanhTien').AsFloat := thanhTien;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTThanhTienChange(Sender: TField);
var
    r: Extended;
    soluong, dongia, dongiaCK, thanhtien, ck, tlck, thanhtienSauCK: Double;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        r := FieldByName('QD1').AsInteger; if r = 0 then r := 1;

        soluong := FieldByName('SoLuong').AsFloat;
        dongia := FieldByName('DonGia').AsFloat;
        dongiaCK := FieldByName('DonGiaCKMH').AsFloat;
        tlck := FieldByName('TyLeCKMH').AsFloat;
        Ck := FieldByName('SoTienCKMH').AsFloat;
        thanhtien := FieldByName('ThanhTien').AsFloat;

        //Sender là Số lượng hoặc thành tiền
        if Sender.FieldName = 'ThanhTien' then
        begin
            dongia := SafeDiv(thanhtien, soluong);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('DonGia').AsFloat := dongia;
            mTrigger := pTrigger;
        end
        else
        begin

            thanhtien := exVNDRound(soluong * dongia, ctCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('ThanhTien').AsFloat := thanhtien;
            mTrigger := pTrigger;
        end;

        //Tiếp tục tính toán cho các cột còn lại
        if Sender.FieldName = 'DonGiaCKMH' then
        begin
            tlck := (1 - SafeDiv(dongiaCK, dongia))*100;
            Ck := exVNDRound(thanhtien * tlck / 100.0, ctCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCKMH').AsFloat := tlck;
            FieldByName('SoTienCKMH').AsFloat := ck;
            mTrigger := pTrigger;
        end
        else if Sender.FieldName = 'SoTienCKMH' then
        begin
            tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, thanhtien)));
            dongiaCK := dongia * (1 - tlck/100);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCKMH').AsFloat := tlck;
            FieldByName('DonGiaCKMH').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end else
        begin
            dongiaCK := dongia * (1 - tlck/100);
            Ck := exVNDRound(thanhtien * tlck / 100.0, ctCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('SoTienCKMH').AsFloat := ck;
            FieldByName('DonGiaCKMH').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end;

        thanhtienSauCK := thanhtien - ck;
        FieldByName('ThanhTienSauCKMH').AsFloat := thanhtienSauCK;

        FieldByName('TyLeLai').AsFloat :=
            DataMain.TinhTL_LAI(FieldByName('DonGiaChuaThue').AsFloat, FieldByName('DonGiaLeChuaThue').AsFloat);

        FieldByName('TyLeLaiSi').AsFloat :=
            DataMain.TinhTL_LAI(FieldByName('DonGiaChuaThue').AsFloat, FieldByName('DonGiaSiChuaThue').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTThanhTienSauCKMHChange(Sender: TField);
var
    r: Extended;
    dongiaCK, dongiaCKHD,
        thanhtienSauCK, tlckHD, ckHD, thanhtienSauCKHD: Double;
begin
    with QrCT do
    begin
        r := FieldByName('QD1').AsInteger; if r = 0 then r := 1;
        dongiaCK := FieldByName('DonGiaCKMH').AsFloat;
        thanhtienSauCK := FieldByName('ThanhTienSauCKMH').AsFloat;
        tlckHD := FieldByName('TyLeCKHD').AsFloat;

        dongiaCKHD := exVNDRound(dongiaCK * (1 - tlckHD/100), -2);
        ckHD := exVNDRound( thanhtienSauCK * tlckHD / 100.0, ctCurRound);
        thanhtienSauCKHD := thanhtienSauCK -  ckHD;

        FieldByName('SoTienCKHD').AsFloat := ckHD;
        FieldByName('DonGiaCKHD').AsFloat := dongiaCKHD;
        FieldByName('ThanhTienSauCKHD').AsFloat := thanhtienSauCKHD;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTThanhTienSauCKHDChange(Sender: TField);
begin
    with QrCT do
    begin
        FieldByName('DonGiaTinhThue').AsFloat := FieldByName('DonGiaCKHD').AsFloat;
        FieldByName('ThanhToanTinhThue').AsFloat := FieldByName('ThanhTienSauCKHD').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTThanhToanTinhThueChange(Sender: TField);
var
    mHinhthuc: String;
    donGiaTinhThue, dgChuaVAT, ts, sl,
    thanhToanTinhThue, thanhtoanChuaVATcl, cl, thanhtoanChuaVAT, thue, thanhtoan : Double;
    r: Extended;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    mHinhthuc := QrNX.FieldByName('HinhThuc_Gia').AsString;

    with QrCT do
    begin
        r := FieldByName('QD1').AsInteger; if r = 0 then r := 1;
        donGiaTinhThue := FieldByName('DonGiaTinhThue').AsFloat;
        ts := FieldByName('ThueSuat').AsFloat;
        sl := FieldByName('SoLuong').AsFloat;
        thanhToanTinhThue := FieldByName('ThanhToanTinhThue').AsFloat;
        cl := FieldByName('SoTienCL').AsFloat;

        if (mHinhthuc = sysGiaCoVAT) then
        begin
            thanhtoan := thanhToanTinhThue;

            thanhtoanChuaVATcl := exVNDRound((donGiaTinhThue /(1 + ts/100)) * sl, -2);
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl + cl, ctCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);
        end
        else
        begin
            thanhtoanChuaVATcl := thanhToanTinhThue;
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl, ctCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);

            thanhtoan := exVNDRound(thanhtoanChuaVATcl + thue + cl, ctCurRound);
        end;

        pTrigger := mTrigger;
        mTrigger := True;
        FieldByName('DonGiaChuaThue').AsFloat := dgChuaVAT;
        FieldByName('ThanhToanChuaCL').AsFloat := thanhtoanChuaVATcl ;
        FieldByName('SoTienCL').AsFloat := cl;
        FieldByName('ThanhToanChuaThue').AsFloat := thanhtoanChuaVAT;
        FieldByName('SoTienThueChuaRound').AsFloat := thue;

        if Ts = 5 then
        begin
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_5').AsFloat := Thue;
        end
        else if Ts = 10 then
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := Thue;
        end else
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := Thue;
        end;

        FieldByName('SoTienThue').AsFloat := Thue;
        FieldByName('ThanhToan').AsFloat := thanhtoan;
        mTrigger := pTrigger;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.QrCTThanhToanChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CapnhatGianhap;
begin
	if not mLuuGianhap then
    	Exit;

    with DMVT_CAPNHAT_GIANHAP do
    begin
    	Prepared := True;
        Parameters.ParamByName('@KHOA').Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));;
        Execute;
    end;
end;
	(*
    ** End: Detail DB
    *)

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SoLuong').AsFloat);
        ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('SoTienCKMH').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienCKMH').AsFloat);
        ColumnByName('ThanhTienSauCKMH').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSauCKMH').AsFloat);
        ColumnByName('SoTienCKHD').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienCKHD').AsFloat);
        ColumnByName('ThanhToanChuaCL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToanChuaCL').AsFloat);
        ColumnByName('SoTienCL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienCL').AsFloat);
        ColumnByName('ThanhTienSauCKHD').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSauCKHD').AsFloat);
        ColumnByName('ThanhToanChuaThue').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToanChuaThue').AsFloat);

		ColumnByName('SoTienThue').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienThue').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.BtnInClick(Sender: TObject);
begin
   (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbKhoHangCloseUp(Sender: TObject; Accept: Boolean);
begin
    (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbKhoHangDropDown(Sender: TObject);
begin
    (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhap.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

end.
