﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ReceiptDesc;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Vcl.Buttons;

type
  TFrmReceiptDesc = class(TForm)
    EdGhichu: TEdit;
    BtnCancel: TBitBtn;
    CmdReturn: TBitBtn;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
    { Private declarations }
  public
    function Execute(var s: String): Boolean;
  end;

var
  FrmReceiptDesc: TFrmReceiptDesc;

implementation

{$R *.DFM}

uses
    isLib, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.CmdReturnClick(Sender: TObject);
begin
     if EdGhichu.Text = '' then
        begin
            ErrMsg('Phải nhập ghi chú mới được xóa phiếu.');
            Exit;
        end;
     ModalResult := mrOk;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmReceiptDesc.Execute;
begin
	EdGhichu.Text := s;
    Result := ShowModal = mrOk;
    if Result then
        s := EdGhichu.Text;
    Free
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReceiptDesc.FormKeyPress(Sender: TObject; var Key: Char);
begin
//	if Key = #13 then
//       CmdReturnClick(nil);
end;

end.
