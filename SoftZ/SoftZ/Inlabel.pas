﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Inlabel;

interface

uses
  SysUtils, Classes, Controls, Forms, ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, ADODb, Db, Wwfltdlg2, Menus, wwdbdatetimepicker, AppEvnts, AdvMenus,
  wwfltdlg, wwDialog, Grids, Wwdbigrd, Wwdbgrid, ToolWin, Wwkeycb;

type
  TFrmInlabel = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdExport: TAction;
    ToolBar1: TToolBar;
    btExport: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMADT: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTSTAMP: TIntegerField;
    QrDMVTTENDT: TWideStringField;
    QrDMVTGIABAN: TFloatField;
    QrDMVTMANHOM: TWideStringField;
    CmdSum: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    Panel2: TPanel;
    Label65: TLabel;
    EdDate: TwwDBDateTimePicker;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    Status: TStatusBar;
    QrDMVTMABH: TWideStringField;
    QrDMVTSTAMP_PRINTED: TFloatField;
    QrDMVTMANGANH: TWideStringField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    CmdRefresh: TAction;
    ToolButton1: TToolButton;
    CmdIntem: TAction;
    ToolButton2: TToolButton;
    Panel3: TPanel;
    Label4: TLabel;
    EdMaSearch: TwwIncrementalSearch;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    PopIn: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem5: TMenuItem;
    BtIn: TToolButton;
    ToolButton4: TToolButton;
    CmdMavach2: TAction;
    Mu35x25x31: TMenuItem;
    Mu35x15x31: TMenuItem;
    Mu35x15x41: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSumExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdIntemExecute(Sender: TObject);
    procedure EdMaSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EdMaSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);
    procedure CmdMavach2Execute(Sender: TObject);
    procedure BtInClick(Sender: TObject);
  private
  	mExPath: String;
  	mStamp: Integer;
	mTrigger: Boolean;
    procedure SaveUncomplete;
    procedure LoadUncomplete;
  public
  end;

var
  FrmInlabel: TFrmInlabel;

implementation

uses
	ExCommon, isDb, isMsg, isStr, MainData, isLib, isFile, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'IN_LABEL';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    EdDate.Date := Date;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.SaveUncomplete;
begin
    if not DirectoryExists(mExPath) then
        if not ForceDirectories(mExPath) then
        begin
            ErrMsg(RS_FILE_IO);
            Exit;
        end;

    QrDMVT.Filter := 'STAMP<>0';
//	TextExport(QrDMVT, ['MAVT', 'STAMP'], mExPath + 'Stamp.csv', TEncoding.UTF8);
    TextExport(QrDMVT, ['MAVT', 'STAMP'], mExPath + 'Stamp.csv');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.LoadUncomplete;
var
    i: Integer;
	s, x: String;
    ls, fs: TStrings;
begin
	// Check existing
    s := mExPath + 'Stamp.csv';
	if not FileExists(s) then
    	Exit;

    // Proc.
    ls := TStringList.Create;
    ls.LoadFromFile(s);

    // Update stamp count
    fs := TStringList.Create;
    with QrDMVT do
    begin
    	DisableControls;

        for i := 0 to ls.Count - 1 do
        begin
        	isStrBreak(ls[i], ',', fs);
        	s := fs[0];
            x := fs[1];

            if Locate('MAVT', s, []) then
            begin
            	Edit;
                FieldByName('STAMP').AsInteger := StrToIntDef(x, 0);
                Post;
            end;
	    end;

        First;
    	EnableControls;
    end;
    ls.Free;
    fs.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.FormShow(Sender: TObject);
begin
	mStamp := GetSysParam('STAMP_OF_PAGE');
    mExPath := IncludeTrailingPathDelimiter(GetSysParam('FOLDER_BARCODE'));

    Wait(PREPARING);
    with QrDMVT do
    begin
    	Open;
        Sort := 'MAVT';
	    LoadUncomplete;
    	Sort := '';
    end;

    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
    	SetDisplayFormat(QrDMVT, ['STAMP'], '#');
    end;
    ClearWait;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMVT, FORM_CODE, Filter);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	SaveUncomplete;
    try
        CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdExportExecute(Sender: TObject);
var
    ngay: TDateTime;
    s: String;
begin
	DataMain.LabelOpen;
	ngay := EdDate.Date;

    with QrDMVT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'STAMP>0';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MABH').AsString,
                FieldByName('STAMP').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;

    if mExPath = '' then
        s := isGetSaveFileName('CSV')
    else
        s := mExPath + 'Label.csv';

	if s = '' then
    	Exit;

    DataMain.LabelClose(s,
    	GetSysParam('STAMP_CSV_HEADER'),
        GetSysParam('STAMP_OVERWRITE'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdIntemExecute(Sender: TObject);
var
    ngay: TDateTime;
    s: String;
begin
	DataMain.LabelOpen;
	ngay := EdDate.Date;

    with QrDMVT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'STAMP>0';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MABH').AsString,
                FieldByName('STAMP').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;


    DataMain.LabelClose2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdMavach2Execute(Sender: TObject);
var
    n: Integer;
    ngay: TDateTime;
    s, filename: String;
begin
	DataMain.LabelOpen;
	ngay := EdDate.Date;

    with QrDMVT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'STAMP>0';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MABH').AsString,
                FieldByName('STAMP').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;

    n := (sender as TComponent).Tag;
    case n of
        1: filename := RP_LABEL_35x22x3;
        2: filename := RP_LABEL_35x15x3;
        3: filename := RP_LABEL_25x15x4;
        4: filename := RP_LABEL_20x10x2
    else
        filename := RP_LABEL_35x25x3
    end;
    DataMain.LabelCloseFR(filename);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdRefreshExecute(Sender: TObject);
begin
    with QrDMVT do
    begin
        if Active then
            Close;
    	Open;
        Sort := 'MABH';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_STAMPS = '%d tem';
    RS_STAMPS_PAGE = '%d tem = %d trang %d tem';

procedure TFrmInlabel.CmdSumExecute(Sender: TObject);
var
	n: Integer;
	bm: TBytes;
begin
	with QrDMVT do
    begin
    	CheckBrowseMode;
    	bm := BookMark;
        DisableControls;
        Filter := 'STAMP<>0';
        First;

        n := 0;
        while not Eof do
        begin
			n := n + FieldByName('STAMP').AsInteger;
        	Next;
        end;

        Filter := '';
        First;
        BookMark := bm;
        EnableControls;
    end;

    if mStamp = 0 then
	    Status.Panels[2].Text := Format(RS_STAMPS, [n])
    else
	    Status.Panels[2].Text := Format(RS_STAMPS_PAGE,
        	[n, n div mStamp, n mod mStamp]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.EdMaSearchKeyPress(Sender: TObject; var Key: Char);
var
    s: String;
begin
    if (Key = #13) or (Key = #8) then
    begin
        s := (Sender as TwwIncrementalSearch).Text;

        with (Sender as TwwIncrementalSearch).DataSource.DataSet do
        begin
            if s = '' then
                Filter := '';
            Filtered := s <> '';
            First;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.EdMaSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
var
	s, s1: String;
begin
    with (LookupTable as TCustomADODataSet) do
    begin
        s := '';
        s1 := DataMain.StripToneMark(SearchValue);
        if s1 <> '' then
        begin
            s := '[MABH]'  + ' like ' + QuotedStr('%' + s1 + '%')
                  + ' or [TENVT_KHONGDAU]'  + ' like ' + QuotedStr('%' + s1 + '%')
                  ;
        end;
        Filter := s;
        Filtered := s <> '';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.FormResize(Sender: TObject);
begin
	Status.Panels[0].Width := Width - 180 - 300;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabel.BtInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

end.
