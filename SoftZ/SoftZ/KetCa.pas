﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit KetCa;

interface

uses
  SysUtils, Classes, Controls, Forms, Vcl.Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2, System.Variants;

type
  TFrmKetCa = class(TForm)
    ToolMain: TToolBar;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    CmdTotal: TAction;
    CmdCancel: TAction;
    Label1: TLabel;
    CmdDel: TAction;
    CmdSearch: TAction;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    Bevel1: TBevel;
    frNavi: TfrNavi;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrDMKHOLOC: TWideStringField;
    EdTENDT: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBDateTimePicker;
    Label4: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    cbbKho: TDbLookupComboboxEh2;
    QrBHKhoa: TLargeintField;
    QrBHSct: TWideStringField;
    QrBHMaLoc: TWideStringField;
    QrBHMaKho: TWideStringField;
    QrBHMaQuay: TWideStringField;
    QrBHQuay: TWideStringField;
    QrBHNhanVienThuNgan: TIntegerField;
    QrBHNgayBatDau: TDateTimeField;
    QrBHNgayKetThuc: TDateTimeField;
    QrBHSctBatDau: TWideStringField;
    QrBHSctKetThuc: TWideStringField;
    QrBHSoLuongSct: TIntegerField;
    QrBHTongSoTienNhan: TFloatField;
    QrBHThanhToanTienMat: TFloatField;
    QrBHThanhToanTheNganHang: TFloatField;
    QrBHThanhToanChuyenKhoan: TFloatField;
    QrBHThanhToanViDienTu: TFloatField;
    QrBHThanhToanViDienTu_VNPAY: TFloatField;
    QrBHThanhToanViDienTu_MOMO: TFloatField;
    QrBHThanhToanKhac: TFloatField;
    QrBHTienMatNhan: TFloatField;
    QrBHTienMatNop: TFloatField;
    QrBHTienMatChenhLech: TFloatField;
    QrBHCHECKED: TBooleanField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHLOC: TWideStringField;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHTHUNGAN: TWideStringField;
    QrBHLK_TENKHO: TWideStringField;
    LbHH1: TLabel;
    EdHH1: TwwDBEdit;
    Label18: TLabel;
    EdCK: TwwDBEdit;
    QrBHXOA: TWideStringField;
    QrCTBHKhoaCT: TLargeintField;
    QrCTBHKhoa: TLargeintField;
    QrCTBHMenhGia: TIntegerField;
    QrCTBHSoLuongNhan: TIntegerField;
    QrCTBHTienMatNhan: TFloatField;
    QrCTBHSoLuongNop: TIntegerField;
    QrCTBHTienMatNop: TFloatField;
    QrCTBHGhiChu: TWideStringField;
    QrCTBHLOC: TWideStringField;
    QrBHIMG: TIntegerField;
    QrBHNG_HUY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    spCHECK_DELETE_KETCA: TADOCommand;
    QrBHGhiChu: TWideMemoField;
    QrCTBHRSTT: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure ItemAllClick(Sender: TObject);
    procedure cbbKhoDropDown(Sender: TObject);
    procedure cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
    procedure QrCTBHCalcFields(DataSet: TDataSet);
  private
    mCanEdit, mClose: Boolean;
    mVATMode: Integer;
    mPrinter: TexPrintBill;

    // List filter
    fType, mFilter: Integer;
   	fTungay, fDenngay: TDateTime;
    fKho, fSQL, fStr: String;
  public
  	procedure Execute(r: WORD; bClose: Boolean = True);
    function  IsCheckDelete(_pKhoa: Largeint; _pFORM_CODE: String; _pMsg: Boolean = True): Boolean;
  end;

var
  FrmKetCa: TFrmKetCa;

implementation

uses
	isDb, ExCommon, MainData, Rights, RepEngine, ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'KETCA';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.Execute(r: WORD; bClose: Boolean);
begin
	mCanEdit := rCanEdit(r);
    mClose := bClose;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    AddAllFields(QrBH, 'KETCA', 0);

    frNavi.DataSet := QrBH;

    // Initial
    fType := 2;
    fStr := '';
    mFilter := 0;
    fSQL := QrBH.SQL.Text;
	EdFrom.Date := Date - sysLateDay;
	EdTo.Date := Date;

    //exBillInitial(mPrinter);
    //mPrinter.PrintPreview := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrLOC, QrDMKHO]);

    cbbKho.Value := sysDefKho;
    QrDMQUAY.Open;


    SetShortDateFormat(QrBH, ShortDateFormat);
    with QrBH do
    begin
        SetDisplayFormat(QrBH, sysCurFmt);
        SetDisplayFormat(QrBH, ['SoLuongSct'], sysQtyFmt);
        SetDisplayFormat(QrBH, ['NgayBatDau', 'NgayKetThuc'], DateTimeFmt);
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
    	SetDisplayFormat(QrCTBH, ['SoLuongNhan', 'SoLuongNop'], sysQtyFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    with QrBH.SQL do
    begin
       Add(' and ([NgayBatDau] between :Tungay1 and :Denngay1' +
       ' or [NgayKetThuc] between :Tungay2 and :Denngay2' +
       ' or :Tungay3 between [NgayBatDau] and [NgayKetThuc]' +
       ' or :Denngay3 between [NgayBatDau] and [NgayKetThuc])');
        fSQL := Text;
    end;

    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    //exBillFinal(mPrinter);
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrBH, QrCTBH, QrDMKHO, QrDMQUAY]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdRefreshExecute(Sender: TObject);
var
	s, sKho: String;
begin
    if not VarIsNull(cbbKho.Value) then
        sKho := cbbKho.Value
    else
        sKho := '';

    if (sKho <> fKho) or
       (EdFrom.Date <> fTungay) or
	   (EdTo.Date   <> fDenngay) then
    begin
		fTungay := EdFrom.Date;
		fDenngay := EdTo.Date;
        fKho := sKho;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ sKho + '''');
            SQL.Add(' order by NgayBatDau desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay + 1;
            Parameters[2].Value := fTungay;
            Parameters[3].Value := fDenngay + 1;
            Parameters[4].Value := fTungay;
            Parameters[5].Value := fDenngay + 1;
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdPrintExecute(Sender: TObject);
var
	k: Largeint;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  FieldByName('KHOA').AsLargeInt;
	end;

    ShowReport(Caption, 'RP_POSZ_KETCA', [sysLogonUID, k]);

//    if mPrinter.BillPrint(k) then
//        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdDelExecute(Sender: TObject);
var
    s: String;
    _khoa: Largeint;
begin
    with QrBH do
    begin

        if FieldByName('DELETE_BY').AsInteger <> 0 then
            Exit;

        _khoa := FieldByName('KHOA').AsLargeInt;
        s := FieldByName('GhiChu').AsString;

        if not IsCheckDelete(_khoa, FORM_CODE) then
            Exit;

        Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
        if not FrmReceiptDesc.Execute(s) then
            Exit;
        if not YesNo(RS_CONFIRM_XOAPHIEU, 1) then
            Exit;
    	Edit;
        FieldByName('GhiChu').AsString := s;
        MarkDataSet(QrBH);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDelete: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;

        bDelete := FieldByName('DELETE_BY').AsInteger > 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and (not bDelete);
    CmdDel.Caption := GetMarkCaption(QrBH);

    CmdPrint.Enabled := not bEmpty;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse; 
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

procedure TFrmKetCa.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
    QrCTBH.Parameters[1].Value := QrBH.FieldByName('LOC').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrCTBHCalcFields(DataSet: TDataSet);
begin
    with QrCTBH do
    if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := '';
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;

    with QrDMQUAY do
    begin
        Close;
        Open;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.cbbKhoDropDown(Sender: TObject);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrBH do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clGray;
            Exit;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.ItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmKetCa.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKetCa.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKetCa.IsCheckDelete(_pKhoa: Largeint; _pFORM_CODE: String;
  _pMsg: Boolean): Boolean;
var
    s: String;
begin
    with spCHECK_DELETE_KETCA do
    begin
        Prepared := True;
        Parameters[1].Value := _pKhoa;
        Parameters[2].Value := _pFORM_CODE;

        Execute;
        Result := Parameters[0].Value = 0;

        if not Result and (_pMsg) then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end;
    end;
end;

end.
