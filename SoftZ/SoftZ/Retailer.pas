﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Retailer;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, wwdblook, Db, ADODB, Buttons, ActnList, kbmMemTable;

type
  TFrmRetailer = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    CbKho: TwwDBLookupCombo;
    QrDMKHO: TADOQuery;
    QrDMQUAY: TADOQuery;
    QrDMQUAYTENMAY: TWideStringField;
    QrDMQUAYGHICHU: TWideStringField;
    QrDMQUAYQUAY: TWideStringField;
    QrDMQUAYPRINTER: TWideStringField;
    CmdOK: TBitBtn;
    BitBtn2: TBitBtn;
    Action: TActionList;
    CmdSave: TAction;
    Label2: TLabel;
    cbQuay: TwwDBLookupCombo;
    CmdDefault: TAction;
    tbTemp: TkbmMemTable;
    DsTemp: TDataSource;
    tbTempMAKHO: TWideStringField;
    tbTempQUAY: TWideStringField;
    QrDMQUAYMAKHO: TWideStringField;
    BitBtn1: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdDefaultExecute(Sender: TObject);
    procedure tbTempMAKHOChange(Sender: TField);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CbKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure CbKhoBeforeDropDown(Sender: TObject);
    procedure CbKhoCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
  private
  	mQuay0: String;
  public
	function Execute(var pKho, pQuay: String; const pQuay0: String): Boolean;
  end;

var
  FrmRetailer: TFrmRetailer;

implementation

uses
    ExCommon, isDb, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmRetailer.Execute(var pKho, pQuay: String;
	const pQuay0: String): Boolean;
begin
	mQuay0 := pQuay0;
    OpenDataSets([QrDMKHO, QrDMQUAY]);
    with tbTemp do
    begin
        Open;
        Append;
        FieldByName('MAKHO').AsString := pKho;
        FieldByName('QUAY').AsString  := pQuay;
    end;
	Result := ShowModal = mrOK;
    if Result then
    with tbTemp do
    begin
        pKho := FieldByName('MAKHO').AsString;
        pQuay := FieldByName('QUAY').AsString;
    end;
    CloseDataSets([QrDMQUAY, QrDMKHO, tbTemp]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    CbKho.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.tbTempMAKHOChange(Sender: TField);
begin
    with QrDMQUAY do
    begin
        Filter := 'MAKHO=' + QuotedStr(Sender.AsString);
        First;
        tbTemp.FieldByName('QUAY').Value := QrDMQUAY.FieldByName('QUAY').Value;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.CbKhoBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC=' + QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.CbKhoCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.CbKhoNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.CmdDefaultExecute(Sender: TObject);
begin
    with tbTemp do
    begin
        FieldByName('MAKHO').AsString := sysDefKho;
        FieldByName('QUAY').AsString := mQuay0;
    end;
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRetailer.CmdSaveExecute(Sender: TObject);
begin
    ModalResult := mrOK;
end;

end.
