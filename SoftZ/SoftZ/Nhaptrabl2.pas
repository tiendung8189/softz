﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Nhaptrabl2;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, isDb,
  frameNavi,  frameKho, frameScanCode2, wwDialog, Mask, Grids, Wwdbgrid, ToolWin,
  wwdbedit, Wwdotdot, Wwdbcomb, DBGridEh, DBLookupEh, DbLookupComboboxEh2,
  DBCtrlsEh, System.Variants;

type
  TFrmNhaptrabl2 = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    Label10: TLabel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTRSTT: TIntegerField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    GrDetail: TwwDBGrid2;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    CmdDetail: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_BY: TIntegerField;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXIMG: TIntegerField;
    QrNXXOA: TWideStringField;
    QrCTSTT: TIntegerField;
    DBEdit1: TDBMemo;
    QrCTTENVT: TWideStringField;
    Panel2: TPanel;
    Label6: TLabel;
    DBEdit5: TDBEdit;
    Label16: TLabel;
    DBEdit14: TDBEdit;
    vlTotal: TisTotal;
    Bevel1: TBevel;
    frNavi: TfrNavi;
    N2: TMenuItem;
    Lctheomthng1: TMenuItem;
    QrNXSOLUONG: TFloatField;
    QrCTSOLUONG: TFloatField;
    QrCTDONGIA: TFloatField;
    QrCTLK_DVT: TWideStringField;
    QrCTLK_DONGIA: TFloatField;
    CmdUpdateDetail: TAction;
    PopDetail: TAdvPopupMenu;
    CmdDelDetail: TAction;
    Xachitit1: TMenuItem;
    CmdUpdateQty: TAction;
    QrNXMAKHO: TWideStringField;
    QrNXTHANHTOAN: TFloatField;
    QrNXLK_TENKHO: TWideStringField;
    QrCTMAVT: TWideStringField;
    QrNXQUAY: TWideStringField;
    DBEdit2: TDBEdit;
    QrNXLCT: TWideStringField;
    CmdAudit: TAction;
    ImgTotal: TImage;
    DBText2: TDBText;
    TntLabel6: TLabel;
    QrCTGHICHU: TWideStringField;
    TntLabel1: TLabel;
    DBEdit4: TDBEdit;
    CmdListRefesh: TAction;
    frKHO: TfrKHO;
    frScanCode21: TfrScanCode2;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrCTLOC: TWideStringField;
    QrNX_id: TLargeintField;
    QrNXThanhTien: TFloatField;
    QrNXThanhTienSauCK: TFloatField;
    QrNXSoTienCK: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrNXTyLeCKHD: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTThanhTienSauCK: TFloatField;
    QrCTTyLeCKHD: TFloatField;
    QrCTTyLeCKVip: TFloatField;
    QrCTTyLeCKPhieu: TFloatField;
    QrCTTyLeCKBo: TFloatField;
    QrCTTyLeCKVipNhomHang: TFloatField;
    QrCTTyLeCKMH: TFloatField;
    QrCTSoTienCK: TFloatField;
    QrCTThueSuat: TFloatField;
    QrCTMaBo: TWideStringField;
    QrCTTyLeCK: TFloatField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure CmdDelDetailExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrNXSOTIENChange(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrNXSOTIEN1Change(Sender: TField);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrCTMAVTChange(Sender: TField);
    procedure QrCTTyLeCKVipChange(Sender: TField);
  private
    mLCT: String;
	mCanEdit, mReadOnly: Boolean;
   	fTungay, fDenngay: TDateTime;
    fKho: String;
    mKhoa: TGUID;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmNhaptrabl2: TFrmNhaptrabl2;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, ChonDsma, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAPTRA_BL2';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.Execute;
begin
    mLCT := 'NTBL2';
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    ImgTotal.Picture.Bitmap.LoadFromResourceName(hInstance, 'IDB_TOTAL');

    // Initial
    fType := 3;
    fStr := '';
    fSQL := QrNX.SQL.Text;

    frScanCode21.Init(QrCT,'SOLUONG','MAVT',True);

    mKhoa := TGuidEx.EmptyGuid;
  	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.FormShow(Sender: TObject);
begin
	// Open Database
    with DataMain do
    begin
    	OpenDataSets([QrDMVT, QrDMTT_Nhaptrabl, QrDMKHO]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrDMVT,['TyLeLai'], sysPerFmt);
    end;

    frKHO.Init2(Date - sysLateDay, Date, CmdRefresh);

    with QrNX do
    begin
    	SetDisplayFormat(QrNX, sysCurFmt);
	    SetDisplayFormat(QrNX, ['SOLUONG'], sysQtyFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SOLUONG'], sysQtyFmt);
	    SetDisplayFormat(QrCT, ['TyLeCKMH'], sysPerFmt);
    end;

    // Customize
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    CmdReRead.Execute;
end;

procedure TFrmNhaptrabl2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if ActiveControl = frScanCode21.EdCode then
        Exit;

	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    Try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;

        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdRefreshExecute(Sender: TObject);
var
	s, pKho : String;
begin
    if not VarIsNull(frKHO.cbbKho.Value) then
        pKho := frKHO.cbbKho.Value
    else
        pKho := '';

   	if (frKHO.edFrom.Date <> fTungay) or
	   (frKHO.edTo.Date   <> fDenngay) or
       (pKho <> fKho) then
    begin
		fTungay  := frKHO.EdFrom.Date;
        fDenngay := frKHO.EdTo.Date;
        fKho 	 := pKho;

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			if fKho <> '' then
            	SQL.Add(' and MAKHO = ''' + fKho + '''');
            //  Loc theo mat hang
            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from BANLE_CT where KHOA = BANLE.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
    	QrDMVT.Requery;
    	QrDMTT_NHAPTRABL.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
	QrNX.Post;

    // Xoa cac dong thua
	mTrigger := True;
    with QrCT do
    begin
        CheckBrowseMode;
        DisableControls;
        First;

        while not Eof do
        begin
            if FieldByName('SOLUONG').AsFloat = 0 then
                Delete
            else
                Next;
		end;
		EnableControls;
    end;

    with QrCT do
    begin
        DisableControls;
        First;
        while not Eof do
        begin
            if FieldByName('STT').AsInteger <> FieldByName('RSTT').AsInteger then
            begin
                Edit;
                FieldByName('STT').AsInteger := FieldByName('RSTT').AsInteger;
            end;
            Next;
        end;
        CheckBrowseMode;
        EnableControls;
        UpdateBatch;
    end;

	mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdTotalExecute(Sender: TObject);
begin
    vlTotal.Sum;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1);
    CmdDel.Caption := GetMarkCaption(QrNX);

    CmdDetail.Enabled := not bEmpty;

    CmdPrint.Enabled  := not bEmpty;
    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    //CmdSearch.Enabled := bBrowse and (n = 0);
    CmdFilter.Enabled := bBrowse and (n = 0);
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    mReadOnly := False;
    CmdUpdateDetail.Enabled := mCanEdit and (not mReadOnly);

    bEmpty := QrCT.IsEmpty;
    CmdDelDetail.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);
    CmdUpdateQty.Enabled := mCanEdit and (not bEmpty) and (not mReadOnly);

end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

	(*
    ** Master events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXAfterInsert(DataSet: TDataSet);
var
	sKho: String;
begin
     if not VarIsNull(frKHO.cbbKho.Value) then
        sKho := frKHO.cbbKho.Value
    else
        sKho := sysDefKho;

	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('_id').Value := NewIntID;
		FieldByName('NGAY').AsDateTime := Now;
		FieldByName('LCT').AsString := mLCT;
        FieldByName('MAKHO').AsString := sKho;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY']) then
	    	Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;

    if QrCT.IsEmpty then
    begin
        Msg('Chưa nhập chi tiết hàng hóa trả lại.');
        Abort;
    end;

    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXBeforeEdit(DataSet: TDataSet);
begin

	if mTrigger then
    	Exit;
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXSOTIENChange(Sender: TField);
begin
    with QrNX do
        FieldByName('THANHTOAN').AsFloat :=
			FieldByName('ThanhTien').AsFloat -
            FieldByName('SoTienCK').AsFloat;
            //FieldByName('CHIETKHAU').AsFloat -
            //FieldByName('CHIETKHAU_MH').AsFloat;
end;

	(*
    ** Detail events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTSOLUONGChange(Sender: TField);
var
	dg, sl, ckmh: Double;
begin
	with QrCT do
    begin
    	dg := FieldByName('DONGIA').AsFloat;
        sl := FieldByName('SOLUONG').AsFloat;

		ckmh := sl * exVNDRound(
        	dg * (FieldByName('TyLeCKHD').AsFloat / 100), sysCurRound);

    	FieldByName('ThanhTien').AsFloat := dg*sl;
    	FieldByName('SoTienCK').AsFloat := ckmh;

		(* Khong tinh CKHD cho cac mat hang co CKMH roi
		** Dung field SOTIEN1 de tinh tong tien tham gia CKHD
        *)
        if ckmh = 0 then
	    	FieldByName('ThanhTienSauCK').AsFloat := dg*sl;
	end;
    vlTotal.Update;
    GrDetail.InvalidateCurrentRow;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTTyLeCKVipChange(Sender: TField);
begin
    with QrCT do
		FieldByName('TyLeCK').AsFloat :=
                max(FieldByName('TyLeCKBo').AsFloat, max(
                    max(FieldByName('TyLeCKVip').AsFloat, FieldByName('TyLeCKVipNhomHang').AsFloat),
                    max(FieldByName('TyLeCKHD').AsFloat, FieldByName('TyLeCKMH').AsFloat)
                ));


end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

		if FieldByName('LK_TENVT').AsString = '' then
        begin
			ErrMsg(RS_ITEM_CODE_FAIL1);
        	Abort;
        end;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
        
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTAfterCancel(DataSet: TDataSet);
begin
    vlTotal.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTAfterDelete(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    vlTotal.Update(True);

    GrDetail.InvalidateCurrentRow;
//    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrCTMAVTChange(Sender: TField);
var x : Double;
begin
	with QrCT do
    begin
		x := FieldByName('LK_DONGIA').AsFloat;
        FieldByName('DONGIA').AsFloat := x;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdDetailExecute(Sender: TObject);
begin
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdDelDetailExecute(Sender: TObject);
begin
    if not YesNo('Xóa toàn bộ chi tiết mặt hàng. Tiếp tục?') then
        Exit;
    EmptyDataSet(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptrabl2.QrNXSOTIEN1Change(Sender: TField);
begin
	with QrNX do
    	FieldByName('SoTienCK').AsFloat := exVNDRound(
	    	FieldByName('ThanhTienSauCK').AsFloat * FieldByName('TyLeCKHD').AsFloat / 100, sysCurRound);
end;


end.
