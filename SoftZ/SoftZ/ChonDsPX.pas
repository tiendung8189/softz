﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsPX;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls,
  wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwDialog, wwfltdlg,
  wwFltDlg2;

type
  TFrmChonDsPX = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPhieu: TDataSource;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    SpBt1: TSpeedButton;
    SpBt2: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMAKHExit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure SpBt1Click(Sender: TObject);
  private
	mMADT: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pMADT: String): Boolean;
  end;

var
  FrmChonDsPX: TFrmChonDsPX;

implementation

uses
	isDb, ExCommon, isLib, Thu2, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsPX.Execute;
begin
	mMADT := pMADT;
    DsPhieu.DataSet := FrmThu2.QrNX2;

    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DS_PHIEU_PX', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
    GrBrowse.SetFocus;
    // Default date

    CmdRefresh.Execute;
    SpBt1.OnClick(SpBt1);

    SetDisplayFormat(FrmThu2.QrNX2, sysCurFmt);
    SetShortDateFormat(FrmThu2.QrNX2);
    SetDisplayFormat(FrmThu2.QrNX2, ['NGAY'], DateTimeFmt);

    if not sysIsThue then
    begin
        grRemoveFields(GrBrowse, ['HOADON_SO', 'HOADON_SERI', 'HOADON_NGAY']);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.SpBt1Click(Sender: TObject);
var
    bm: TBytes;
begin
    with FrmThu2.QrNX2 do
    begin
        bm := Bookmark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SELECTED').AsBoolean := (Sender as TSpeedButton).Tag = 0;
            Next;
        end;
        Bookmark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPhieu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.CmdRefreshExecute(Sender: TObject);
var
    s: String;
    Tungay, Denngay: TDateTime;
begin
    Tungay  := Date;
    Denngay := Date;

//   	if (mTungay <> Tungay)  or (mDenngay <> Denngay)   then
	begin
        mTungay := Tungay;
        mDenngay := Denngay;

        with FrmThu2.QrNX2 do
        begin
			Close;

            Parameters.ParamByName('@MADT').Value := mMADT;
            Parameters.ParamByName('@NGAYD').Value := mTungay;
            Parameters.ParamByName('@NGAYC').Value := mDenngay;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.CbMAKHExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPX.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

end.
