object FrmChi: TFrmChi
  Left = 212
  Top = 129
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Phi'#7871'u Chi Ti'#7873'n Nh'#224' Cung C'#7845'p'
  ClientHeight = 600
  ClientWidth = 872
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    872
    600)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 872
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 872
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object SepChecked: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton11: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 872
    Height = 560
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 510
        Width = 864
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 864
        Height = 510
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object GrBrowse: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 864
          Height = 461
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'IMG;ImageIndex;Original Size'
            'IMG2;ImageIndex;Original Size')
          Selected.Strings = (
            'IMG'#9'3'#9#9'F'
            'IMG2'#9'3'#9#9'F'
            'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
            'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
            'MADT'#9'10'#9'M'#227#9'F'#9#272#417'n v'#7883' nh'#7853'n'
            'LK_TENDT'#9'40'#9'T'#234'n'#9'F'#9#272#417'n v'#7883' nh'#7853'n'
            'SOTIEN'#9'15'#9'S'#7889' ti'#7873'n'#9'F'
            'LK_PTTT'#9'25'#9'H'#236'nh th'#7913'c thanh to'#225'n'#9'F'
            'LK_LYDO'#9'40'#9'L'#253' do'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgAllowInsert]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopMaster
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnDblClick = GrBrowseDblClick
          OnEnter = CmdRefreshExecute
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
        end
        inline frDate: TfrNGAY
          Left = 0
          Top = 0
          Width = 864
          Height = 49
          Align = alTop
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 864
          inherited Panel1: TPanel
            Width = 864
            ParentColor = False
            ExplicitWidth = 864
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaMaster: TisPanel
        Left = 0
        Top = 0
        Width = 864
        Height = 217
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        HeaderCaption = ' :: Phi'#7871'u chi'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 58
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Panel1: TPanel
          Left = 2
          Top = 18
          Width = 860
          Height = 52
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object DBText1: TDBText
            Left = 6
            Top = 5
            Width = 21
            Height = 17
            DataField = 'XOA'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 77
            Top = 9
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object CbNGAY: TwwDBDateTimePicker
            Left = 112
            Top = 5
            Width = 165
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NGAY'
            DataSource = DsTC
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ShowButton = True
            TabOrder = 0
          end
          object EdSoPhieu: TDBEditEh
            Left = 340
            Top = 5
            Width = 185
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            CharCase = ecUpperCase
            Color = 15794175
            ControlLabel.Width = 50
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' phi'#7871'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'SCT'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object CbHinhThuc: TDbLookupComboboxEh2
            Left = 673
            Top = 5
            Width = 181
            Height = 22
            ControlLabel.Width = 54
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'PTTT'
            DataSource = DsTC
            DropDownBox.Columns = <
              item
                FieldName = 'DGIAI'
              end>
            DropDownBox.ListSource = DataMain.DsPTTT
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA'
            ListField = 'DGIAI'
            ListSource = DataMain.DsPTTT
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 2
            Visible = True
          end
          object CbbLyDo: TDbLookupComboboxEh2
            Left = 112
            Top = 29
            Width = 413
            Height = 22
            ControlLabel.Width = 30
            ControlLabel.Height = 16
            ControlLabel.Caption = 'L'#253' do'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'LyDo'
            DataSource = DsTC
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
              end>
            DropDownBox.ListSource = DataMain.DsLYDO_CHI
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = DataMain.DsLYDO_CHI
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 3
            Visible = True
          end
          object DBEditEh1: TDBEditEh
            Left = 673
            Top = 29
            Width = 181
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            CharCase = ecUpperCase
            Color = 15794175
            ControlLabel.Width = 136
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' phi'#7871'u theo h'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'SCT2'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 4
            Visible = True
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 166
          Width = 860
          Height = 49
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 4
          object DBMemoEh1: TDBMemoEh
            Left = 112
            Top = 25
            Width = 681
            Height = 22
            ControlLabel.Width = 49
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AutoSize = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'GhiChu'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            ParentCtl3D = False
            ShowHint = True
            TabOrder = 2
            Visible = True
            WantReturns = True
          end
          object DBEditEh4: TDBEditEh
            Left = 112
            Top = 1
            Width = 237
            Height = 22
            BevelKind = bkFlat
            BorderStyle = bsNone
            ControlLabel.Width = 65
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ng'#432#7901'i nh'#7853'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'Nguoi'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object EdNumSoTien: TDBNumberEditEh
            Left = 673
            Top = 1
            Width = 121
            Height = 22
            ControlLabel.Width = 40
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' ti'#7873'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'SoTien'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaTK: TPanel
          Left = 2
          Top = 118
          Width = 860
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object CbTenTK: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 237
            Height = 22
            ControlLabel.Width = 56
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#224'i kho'#7843'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MATK'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TENTK'
                Width = 236
              end
              item
                FieldName = 'MATK'
                Width = 155
              end>
            DropDownBox.ListSource = DataMain.DsDMTK_NB
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 413
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MATK'
            ListField = 'TENTK'
            ListSource = DataMain.DsDMTK_NB
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdSTK: TDBEditEh
            Left = 352
            Top = 1
            Width = 173
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MATK'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object EdChinhanh: TDBEditEh
            Left = 352
            Top = 25
            Width = 173
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_CHINHANH'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object EdNganhang: TDBEditEh
            Left = 112
            Top = 25
            Width = 237
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Width = 61
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_NGANHANG'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            Visible = True
          end
        end
        object PaMALOC: TPanel
          Left = 2
          Top = 70
          Width = 860
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object CbbMaLoc: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 333
            Height = 22
            ControlLabel.Width = 51
            ControlLabel.Height = 16
            ControlLabel.Caption = #272#7883'a '#273'i'#7875'm'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MALOC'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TEN'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 331
              end
              item
                FieldName = 'LOC'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 60
              end>
            DropDownBox.ListSource = DataMain.DsLOC
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 412
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'LOC'
            ListField = 'TEN'
            ListSource = DataMain.DsLOC
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdMaLoc: TDBEditEh
            Left = 448
            Top = 1
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MALOC'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaMADT: TPanel
          Left = 2
          Top = 94
          Width = 860
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object CbbNhaCungCap: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 333
            Height = 22
            ControlLabel.Width = 68
            ControlLabel.Height = 16
            ControlLabel.Caption = #272#417'n v'#7883' nh'#7853'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MADT'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TENDT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 331
              end
              item
                FieldName = 'MADT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 60
              end>
            DropDownBox.ListSource = DataMain.DsDMNCC
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 412
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MADT'
            ListField = 'TENDT'
            ListSource = DataMain.DsDMNCC
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdMaDT: TDBEditEh
            Left = 448
            Top = 1
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MADT'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <
              item
                Action = CmdXemCongNo
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 730
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 730
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 324
    Top = 356
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdXemCongNo: TAction
      Caption = 'CmdXemCongNo'
      OnExecute = CmdXemCongNoExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTC
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MST'
      'LK_PTTT'
      'NGUOI')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 296
    Top = 356
  end
  object QrTC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTCBeforeOpen
    BeforeInsert = QrTCBeforeInsert
    AfterInsert = QrTCAfterInsert
    BeforeEdit = QrTCBeforeEdit
    BeforePost = QrTCBeforePost
    AfterPost = QrTCAfterPost
    AfterCancel = QrTCAfterCancel
    AfterScroll = QrTCAfterScroll
    OnCalcFields = QrTCCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'THUCHI'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      '   and'#9'LOC = :LOC')
    Left = 380
    Top = 356
    object QrTCXOA: TWideStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrTCIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrTCIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrTCCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrTCNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrTCNGAYValidate
    end
    object QrTCSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrTCLK_LYDO: TWideStringField
      DisplayLabel = 'L'#253' do'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_CHI
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LYDO'
      Size = 50
      Lookup = True
    end
    object QrTCPTTT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldName = 'PTTT'
      Visible = False
      OnChange = QrTCPTTTChange
    end
    object QrTCLK_PTTT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c '
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = DataMain.QrPTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Size = 100
      Lookup = True
    end
    object QrTCMADT: TWideStringField
      DisplayLabel = #272#417'n v'#7883' nh'#7853'n'
      FieldName = 'MADT'
      Size = 15
    end
    object QrTCLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n '#273#417'n v'#7883
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 50
      Lookup = True
    end
    object QrTCMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      Visible = False
      FixedChar = True
      Size = 5
    end
    object QrTCLK_TENKHO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrTCCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTCUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTCDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrTCCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrTCUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrTCDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrTCLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrTCKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTCLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrTCMATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'MATK'
      OnChange = QrTCMATKChange
      Size = 50
    end
    object QrTCLK_TENTK: TWideStringField
      DisplayLabel = 'Ch'#7911' t'#224'i kho'#7843'n'
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_CHINHANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CHINHANH'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCMALOC: TWideStringField
      FieldName = 'MALOC'
      Size = 5
    end
    object QrTCLK_TENLOC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENLOC'
      LookupDataSet = DataMain.QrLOC
      LookupKeyFields = 'LOC'
      LookupResultField = 'TEN'
      KeyFields = 'MALOC'
      Size = 200
      Lookup = True
    end
    object QrTCSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrTCGhiChu: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrTCNguoi: TWideStringField
      DisplayLabel = 'Ng'#432#7901'i'
      FieldName = 'Nguoi'
      Size = 50
    end
    object QrTCSoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrTCSOTIENChange
    end
    object QrTCThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrTCLyDo: TWideStringField
      FieldName = 'LyDo'
      Size = 70
    end
  end
  object DsTC: TDataSource
    DataSet = QrTC
    Left = 380
    Top = 384
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 140
    Top = 328
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 172
    Top = 328
  end
end
