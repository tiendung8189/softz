﻿with DataSet do
begin
	if FieldByName('CANCEL_BY').AsInteger <> 0 then
    begin
		FieldByName('XOA').AsString := 'X';
		FieldByName('IMG3').AsInteger := 2;
    end
	else
    begin
		FieldByName('XOA').Clear;
		FieldByName('IMG3').AsInteger := -1;
    end;
end;
