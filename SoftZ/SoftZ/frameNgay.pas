﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameNgay;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, wwdblook, DBGridEh, Vcl.Mask,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TfrNGAY = class(TFrame)
    Panel1: TPanel;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    cbbLoc: TDbLookupComboboxEh2;
    procedure Panel1Exit(Sender: TObject);
  private
  public
  	procedure Init; overload;
  	procedure Init(const d1, d2: TDateTime); overload;
  end;

implementation

uses
	ExCommon, isCommon, isMsg, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNGAY.Init;
begin
    Init(Date - sysLateDay, Date)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNGAY.Init(const d1, d2: TDateTime);
begin
    DataMain.QrLOC.Open;
    EdFrom.Date := d1;
	EdTo.Date := d2;
    cbbLoc.Value := sysLoc;
    cbbLoc.Enabled := sysIsCentral;
    cbbLoc.Visible := sysIsDrc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrNGAY.Panel1Exit(Sender: TObject);
begin
    if EdTo.DateTime < EdFrom.DateTime then
    begin
        ErrMsg(RS_PERIOD);
        EdFrom.SelectAll;
        EdFrom.SetFocus;
    end;
end;

end.
