﻿(*==============================================================================
** System Preferences
**------------------------------------------------------------------------------
*)
unit Params2;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  Db, ADODB, wwDataInspector, Buttons,
  ActnList, wwdblook, StdCtrls, Grids;

type
  TFrmParams2 = class(TForm)
    ActionList: TActionList;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdCancel: TAction;
    Inspect: TwwDataInspector;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    QrPARAMS: TADOQuery;
    QrPARAMSKHOA: TAutoIncField;
    QrPARAMSDEFAULT_MAKHO: TWideStringField;
    QrPARAMSIS_CENTRAL: TBooleanField;
    QrPARAMSSTAMP_OF_PAGE: TIntegerField;
    QrPARAMSDEFAULT_PTNHAP: TWideStringField;
    QrPARAMSDEFAULT_PTXUAT: TWideStringField;
    QrPARAMSDEFAULT_PTTT: TWideStringField;
    QrPARAMSDEFAULT_LATE_DAY: TIntegerField;
    QrPARAMSSTOCK_DATE: TDateTimeField;
    DsPARAMS: TDataSource;
    QrPARAMSFOLDER_EXPORT: TWideStringField;
    QrPARAMSFOLDER_IMPORT: TWideStringField;
    QrPARAMSFOLDER_REPORT: TWideStringField;
    QrPARAMSFMT_GMS_CUR: TWideStringField;
    QrPARAMSFMT_GMS_QTY: TWideStringField;
    QrPARAMSFMT_GMS_DATE: TWideStringField;
    QrPARAMSFMT_POS_CUR: TWideStringField;
    QrPARAMSFMT_POS_QTY: TWideStringField;
    QrPARAMSFMT_POS_DATE: TWideStringField;
    QrPARAMSFMT_GMS_THOUSAND_SEP: TWideStringField;
    QrPARAMSFMT_GMS_DECIMAL_SEP: TWideStringField;
    QrPARAMSFMT_POS_THOUSAND_SEP: TWideStringField;
    QrPARAMSFMT_POS_DECIMAL_SEP: TWideStringField;
    QrPARAMSDEFAULT_LYDO_THU: TWideStringField;
    QrPARAMSDEFAULT_LYDO_CHI: TWideStringField;
    CmdAdmin: TAction;
    QrPARAMSSTAMP_CSV_HEADER: TBooleanField;
    QrPARAMSFOLDER_BARCODE: TWideStringField;
    QrPARAMSSTAMP_OVERWRITE: TBooleanField;
    QrPARAMSBEGIN_BALANCE: TBooleanField;
    QrPARAMSBEGIN_MONTH: TIntegerField;
    QrPARAMSBEGIN_YEAR: TIntegerField;
    QrPARAMSFMT_GMS_USD: TWideStringField;
    QrPARAMSSTAMP_PREFIX: TWideStringField;
    QrPARAMSROUNDING_CUR: TIntegerField;
    QrPARAMSSTAMP_UTF8: TBooleanField;
    QrPARAMSSEARCH_MATCH_ANY: TBooleanField;
    QrPARAMSBL_SOGIO_TRAHANG: TFloatField;
    QrPARAMSBL_DONVI_TIENTHOI: TFloatField;
    QrPARAMSBL_NHACNHO_TIENTHOI: TBooleanField;
    QrPARAMSHEADER1: TWideStringField;
    QrPARAMSHEADER2: TWideStringField;
    QrPARAMSHEADER3: TWideStringField;
    QrPARAMSFT_DECIMAL_SEPARATOR: TWideStringField;
    QrPARAMSFT_THOUSAND_SEPARATOR: TWideStringField;
    QrPARAMSFT_QTY_DECIMALS: TIntegerField;
    QrPARAMSFT_QTY_ROUNDING: TIntegerField;
    QrPARAMSFT_CUR_DECIMALS: TIntegerField;
    QrPARAMSFT_CUR_ROUNDING: TIntegerField;
    CbDVT: TwwDBLookupCombo;
    QrPARAMSFOLDER_IMAGE_WEB: TWideStringField;
    QrPARAMSFB_SOGIO_REMINDER: TFloatField;
    QrPARAMSDEFAULT_DVT: TIntegerField;
    QrPARAMSHEADER4: TWideStringField;
    QrPARAMSHEADER5: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure SelectFolder(Sender: TwwDataInspector; Item: TwwInspectorItem);
    procedure QrPARAMSAfterPost(DataSet: TDataSet);
    procedure QrPARAMSFOLDER_IMPORTChange(Sender: TField);
    procedure FormCreate(Sender: TObject);
    procedure CmdAdminExecute(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: string; var Accept: Boolean);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
  private
  	mCanEdit, mChanged, mSysChanged: Boolean;
  public
  	function Execute(r: WORD): Integer;
  end;

var
	FrmParams2 : TFrmParams2;

implementation

uses
	MainData, isDb, Rights, ExCommon, isLib, isFile, isMsg, isStr,
    isCommon;

{$R *.DFM}

const
    FORM_CODE: String = 'SYS_CONFIG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmParams2.Execute;
begin
	mChanged := False;
    mSysChanged := False;

	mCanEdit := rCanEdit(r);
    Inspect.ReadOnly := not mCanEdit;
    ShowModal;

	Result := 0;
    if mChanged then
	    if mSysChanged then
    		Result := 2
	    else
    		Result := 1
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.FormShow(Sender: TObject);
var
	i: Integer;
    s: String;
    b: Boolean;
    ls: TStrings;
    it: TwwInspectorItem;
begin
	with DataMain do
    begin
		OpenDataSets([QrPARAMS, QrDMKHO, QrPTNHAP, QrPTXUAT, QrDM_DVT,
        	QrLYDO_THU, QrLYDO_CHI, QrPTTT]);

        with QrPARAMS do
        begin
        	if IsEmpty then
	        begin
        		Append;
                for i := 0 to FieldCount - 1 do
                	if Fields[i].DataType = ftBoolean then
                    	Fields[i].Value := False;
            end;
        end;
	end;

    SetDisplayFormat(QrParams, sysCurFmt);
    SetDictionary(QrParams, FORM_CODE);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CmdSaveExecute(Sender: TObject);
begin
	QrPARAMS.CheckBrowseMode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CmdCancelExecute(Sender: TObject);
begin
    QrPARAMS.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrPARAMS, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrPARAMS, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.SelectFolder(Sender: TwwDataInspector;
  Item: TwwInspectorItem);
var
	s: String;
begin
	s := ExpandUNCFileName(isGetDir('Chọn thư mục', ''));
    if s = '' then
    	Exit;
    with QrParams do
    begin
    	if State in [dsBrowse] then
        	Edit;
		Item.Field.AsString := s;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.QrPARAMSAfterPost(DataSet: TDataSet);
begin
	mChanged := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_FOLDERIMPORT_CFM = 'Đây phải là thư mục cục bộ của máy chủ cơ sở dữ liệu.';

procedure TFrmParams2.QrPARAMSFOLDER_IMPORTChange(Sender: TField);
begin
	Msg(RS_FOLDERIMPORT_CFM, RS_NOTE_CAP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: string; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmParams2.CmdAdminExecute(Sender: TObject);
begin
	if CmdAdmin.Tag = 0 then
    	if FixLogon then
        begin
        	CmdAdmin.Tag := 1;
            with Inspect do
            begin
                with GetItemByFieldName('IS_CENTRAL') do
                begin
                	ReadOnly := False;
                    TabStop := True;
                end;
                with GetItemByFieldName('DEFAULT_MAKHO') do
                begin
                	ReadOnly := False;
                    TabStop := True;
                end;
            end;
        end;
end;
end.
