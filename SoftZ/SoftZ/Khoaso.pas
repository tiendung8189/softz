﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Khoaso;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, ActnList, SysUtils,
  Buttons, Db, ADODB, Grids, StdCtrls;

type
  TFrmKhoaso = class(TForm)
    ActionList: TActionList;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdCancel: TAction;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    BitBtn3: TBitBtn;
    Inspect: TwwDataInspector;
    QrPARAMS: TADOQuery;
    DsPARAMS: TDataSource;
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  	mCanEdit : Boolean;
  public
  	procedure Execute (r : WORD);
  end;

var
  FrmKhoaso: TFrmKhoaso;

implementation

{$R *.DFM}

uses
	Rights, isDb, Excommon, isLib, MainData, isMsg;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.Execute (r : WORD);
begin
	mCanEdit := rCanEdit(r);
    Inspect.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_STOCK_DATE_MIN = 'Chưa chốt tồn hoặc có phát sinh thay đổi dữ liệu trước %s. Không thể khóa sổ.';
    RS_BEGIN_DATE   = 'Ngày khóa sổ phải >= ngày bắt đầu chương trình %s.';
procedure TFrmKhoaso.CmdSaveExecute(Sender: TObject);
var
    d, dMin, dFB, dFBMin: TDateTime;
begin
    dMin := GetSysParam('STOCK_DATE_MIN');
    if dMin < 10 then
        dMin := GetSysParam('STOCK_DATE');

    d := QrPARAMS.FieldByName('KS_HH').AsDateTime;

    dFBMin := GetSysParam('FB_STOCK_DATE_MIN');
    if dFBMin < 10 then
        dFBMin := GetSysParam('FB_STOCK_DATE');

    dFB := QrPARAMS.FieldByName('FB_KS_HH').AsDateTime;

    if d > dmin then
    begin
        ErrMsg(Format(RS_STOCK_DATE_MIN, [DateToStr(dMin)]));
        Exit;
    end;

    if dFB > dFBMin then
    begin
        ErrMsg(Format(RS_STOCK_DATE_MIN, [DateToStr(dFBMin)]));
        Exit;
    end;

    if (d < sysBegDate) or (dFB < sysBegDate) then
    begin
        ErrMsg(Format(RS_BEGIN_DATE, [DateToStr(sysBegDate)]));
        Exit;
    end;

	QrPARAMS.CheckBrowseMode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.CmdCancelExecute(Sender: TObject);
begin
    QrPARAMS.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.FormShow(Sender: TObject);
begin
	OpenDataSets([QrPARAMS]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrPARAMS.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrPARAMS, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhoaso.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrPARAMS, True);
end;

end.
