﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDondh;

interface

uses
  Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, Grids, Wwdbigrd, Wwdbgrid,
  DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh, DbLookupComboboxEh2, Variants,
  kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmChonDondh = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsDONDH: TDataSource;
    QrDONDH: TADOQuery;
    QrDONDHNGAY: TDateTimeField;
    QrDONDHSCT: TWideStringField;
    QrDONDHMADT: TWideStringField;
    QrDONDHMAKHO: TWideStringField;
    QrDONDHNG_DATHANG: TWideStringField;
    QrDONDHTHANHTOAN: TFloatField;
    QrDONDHDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMNCC: TADOQuery;
    QrDONDHTENKHO: TWideStringField;
    QrDONDHTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrDONDHNGAY_GIAO: TDateTimeField;
    QrDONDHCHIETKHAU_HD: TFloatField;
    QrDONDHSOLUONG: TFloatField;
    QrDONDHKHOA: TGuidField;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    DsDMKHO: TDataSource;
    DsDMNCC: TDataSource;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL: String;
  public
  	function Execute(pFix: Boolean; pNcc, pKho: String): TGUID;
  end;

var
  FrmChonDondh: TFrmChonDondh;

implementation

uses
	isDb, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDondh.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    CbbDonVi.Enabled := not pFix;
    CbKhoHang.Enabled := not pFix;


	if ShowModal = mrOK then
		Result := TGuidField(QrDONDH.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrDONDH, QrDMNCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DONDH', GrBrowse);

    SetDisplayFormat(QrDONDH, sysCurFmt);
    SetShortDateFormat(QrDONDH);
    SetDisplayFormat(QrDONDH, ['NGAY'], DateTimeFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormShow(Sender: TObject);
begin
   	OpenDataSets([QrDONDH, QrDMNCC, QrDMKHO]);
	mSQL := QrDONDH.SQL.Text;
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
    // Smart focus
    if mNCC = '' then
		try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDONDH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.CmdRefreshExecute(Sender: TObject);
var
	s, sDonVi, sKhoHang: String;
begin
    sDonVi := EdMADV.Text;
    sKhoHang := EdMaKho.Text;

   	if  (mNCC <> sDonVi) or
	   	(mKHO <> sKhoHang) then
    begin

   	    mNCC := sDonVi;
        mKHO := sKhoHang;

        with QrDONDH do
        begin
			Close;
            s := mSQL;
            if mNCC <> '' then
            	s := s + 'and a.MADT=''' + mNCC + '''';

			if mKHO <> '' then
            	s := s + 'and a.MAKHO=''' + mKHO + '''';

            SQL.Text := s;
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mNCC;
    end;
end;

end.
