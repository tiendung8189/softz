﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Return;

interface

uses
  Windows, Messages, SysUtils, Classes, Graphics, Controls, Forms, Dialogs,
  StdCtrls, Buttons, ExtCtrls;

type
  TFrmReturn = class(TForm)
    EdMA: TEdit;
    EdSL: TEdit;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Image1: TImage;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReturnClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  	mUID: Integer;

    procedure ReturnItems;
    procedure ReturnItem;
    procedure AppendReturn(pMabh, pMabhBo: String; pSoluong, pTlck: Double);

    function  IsValidQty(pMabh: String; pSlTra: Double): Boolean;
  public
  	procedure Execute (uid: Integer);
  end;

var
  FrmReturn: TFrmReturn;

implementation

{$R *.DFM}

uses
	ExCommon, PosMain, isMsg, isLib, Variants;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.Execute;
begin
	mUID := uid;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.FormShow(Sender: TObject);
begin
	EdMA.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.CmdReturnClick(Sender: TObject);
var
    s: String;
begin
    s := Trim(EdMA.Text);

    // PTD: Khong cho tra hang hoa bo
    with FrmMain.QrDMVT do
        if Locate('MAVT', s, []) then
        begin
            if FieldByName('BO').AsBoolean then
                ReturnItems
            else
                ReturnItem;
        end
        else
        begin
            ErrMsg('MÆt hµng kh«ng hîp lÖ.');
            EdMA.SetFocus;
            Exit;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.ReturnItems;
var
	b: Boolean;
    x: Double;
    s: String;
    pMabhBo: String;
begin
    pMabhBo := Trim(EdMA.Text);

    with FrmMain do
        with QrCTBH do
        begin
            DisableControls;

		    // Validate phase 1 : Mat hang da ban
            if not Locate('MABH_BO', pMabhBo, []) then
		    begin
	            EnableControls;
    			Msg('MÆt hµng nµy ch­a b¸n.');
				Self.EdMA.SetFocus;
        		Exit;
		    end;

            // Validate phase 2 : So luong
            b := True;
            x := 0;
            try
                x := StrToFloat(Trim(EdSL.Text));
            except
                b := False;
            end;

            if (not b) or (x <= 0) then
            begin
	            EnableControls;
    			Msg('Sè l­îng kh«ng hîp lÖ.');
                Self.EdSL.SetFocus;
                Exit;
            end;

            OpenDmvt_Bo(pMabhBo);
            with QrDMVT_BO do
            begin
                First;
                while not Eof do
                begin
                    if not IsValidQty(FieldByName('MABH').AsString, x) then
                    begin
                        b := False;
                        Break;
                    end;
                    Next;
                end;
            end;

            if not b then
            begin
                EnableControls;            
                Msg('Sè l­îng kh«ng hîp lÖ.');
				Self.EdSL.SetFocus;
        		Exit;
            end;
            EnableControls;

		    // Append
            with QrDMVT_BO do
            begin
                First;

                while not Eof do
                begin
                    s := FieldByName('MABH').AsString;
                    if QrCTBH.Locate('MAVT;MABH_BO', VarArrayOf([s, pMabhBo]), []) then
                    begin
                        try
                            AppendReturn(s, pMabhBo, x, QrCTBH.FieldByName('TL_CK').AsFloat);
                        except
                            EnableControls;
                            Self.EdMA.Text := '';
                            Self.EdMA.SetFocus;
                            Exit;
                        end;
                        Inc(mStt);
                    end;
                    Next;
                end;
            end;
        end;

    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.ReturnItem;
var
	b: Boolean;
    x, xCk: Double;
    pMabh: String;
begin
    pMabh := Trim(EdMA.Text);

    with FrmMain do
        with QrCTBH do
        begin
            DisableControls;

		    // Validate phase 1 : Mat hang da ban
            if not Locate('MAVT', pMabh, []) then
		    begin
	            EnableControls;
    			Msg('MÆt hµng nµy ch­a b¸n.');
				Self.EdMA.SetFocus;
        		Exit;
		    end
            else
            	xCk := FieldByName('TL_CK').AsFloat;

            // Validate phase 2 : So luong
            b := True;
            x := 0;
            try
                x := StrToFloat(Trim(EdSL.Text));
            except
                b := False;
            end;

            if (not b) or (x <= 0) then
            begin
	            EnableControls;
    			Msg('Sè l­îng kh«ng hîp lÖ.');
                Self.EdSL.SetFocus;
                Exit;
            end;

		    // Validate phase 3 : So luong tra <= So luong ban
            if not IsValidQty(pMabh, x) then
		    begin
                EnableControls;
    			Msg('Sè l­îng kh«ng hîp lÖ.');
				Self.EdSL.SetFocus;
        		Exit;
		    end;
            EnableControls;

		    // Append
        	try
	            AppendReturn(pMabh, '', x, xCk);
            except
            	Self.EdMA.Text := '';
            	Self.EdMA.SetFocus;
                Exit;
            end;
			Inc(mStt);
        end;

    ModalResult := mrOK;
end;

(*==============================================================================
** Kiem tra so luong tra cua 1 mat hang
** So luong tra <= So luong ban
**------------------------------------------------------------------------------
*)
function TFrmReturn.IsValidQty(pMabh: String; pSlTra: Double): Boolean;
var
    xSL: Double;
begin
    with FrmMain.QrCTBH do
    begin
        First;
        xSL := 0;
        while not Eof do
        begin
            if Trim(FieldByName('MAVT').AsString) = pMabh then
                xSL := xSL + FieldByName('SOLUONG').AsFloat;
            Next;
        end;

        Result := pSlTra < xSL + 0.000001;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmReturn.AppendReturn(pMabh, pMabhBo: String; pSoluong, pTlck: Double);
begin
    with FrmMain do
        with QrCTBH do
        begin
	    	GoLast;

            Append;
            FieldByName('MAVT').AsString := pMabh;
            if pMabhBo <> '' then
                FieldByName('MABH_BO').AsString := pMabhBo;
            FieldByName('SOLUONG').AsFloat := -pSoluong;
            FieldByName('TL_CK').AsFloat := pTlCk;
            FieldByName('TRA_BY').AsInteger := mUID;
            FieldByName('TRA_DATE').AsDateTime := Now;
            Post;
        end;
end;

end.
