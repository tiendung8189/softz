object FrmThuThungan: TFrmThuThungan
  Left = 155
  Top = 241
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Phi'#7871'u Thu Ti'#7873'n Nh'#226'n Vi'#234'n Thu Ng'#226'n'
  ClientHeight = 600
  ClientWidth = 850
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    850
    600)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 850
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 850
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 66
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 66
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 74
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 140
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 206
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 214
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 280
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 288
      Top = 0
      Cursor = 1
      Hint = 'In phi'#7871'u'
      Caption = 'In'
      DropdownMenu = PopPrint
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object SepChecked: TToolButton
      Left = 379
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 387
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton11: TToolButton
      Left = 453
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 461
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 850
    Height = 552
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    ExplicitLeft = -8
    ExplicitTop = 96
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 502
        Width = 842
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 842
        Height = 502
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GrBrowse: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 842
          Height = 453
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'IMG;ImageIndex;Original Size'
            'IMG2;ImageIndex;Original Size')
          Selected.Strings = (
            'IMG'#9'3'#9#9'F'
            'IMG2'#9'3'#9#9'F'
            'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
            'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
            'SOTIEN'#9'15'#9'S'#7889' ti'#7873'n'#9'F'
            'LK_TENTHUNGAN'#9'25'#9'Thu ng'#226'n'#9'F'
            'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
            'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho h'#224'ng'
            'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgAllowInsert]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopMaster
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnDblClick = GrBrowseDblClick
          OnEnter = CmdRefreshExecute
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
        end
        inline frDate: TfrNGAY
          Left = 0
          Top = 0
          Width = 842
          Height = 49
          Align = alTop
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 842
          inherited Panel1: TPanel
            Width = 842
            ParentColor = False
            ExplicitWidth = 842
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object PaMaster: TisPanel
        Left = 0
        Top = 0
        Width = 842
        Height = 523
        Align = alClient
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Thu ti'#7873'n thu ng'#226'n'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 191
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label1: TLabel
          Left = 77
          Top = 40
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object DBText1: TDBText
          Left = 6
          Top = 20
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CbNgay: TwwDBDateTimePicker
          Left = 112
          Top = 36
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsTC
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object GrDetail: TwwDBGrid2
          Left = 340
          Top = 139
          Width = 425
          Height = 272
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          Selected.Strings = (
            'CALC_STT'#9'3'#9'STT'#9'F'
            'MENHGIA'#9'17'#9'M'#7879'nh gi'#225#9'T'
            'SOLUONG'#9'15'#9'S'#7889' l'#432#7907'ng'#9'F'
            'SOTIEN'#9'19'#9'Th'#224'nh ti'#7873'n'#9'T')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = False
          ShowVertScrollBar = False
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          TabOrder = 8
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
        object EdSoPhieu: TDBEditEh
          Left = 340
          Top = 36
          Width = 185
          Height = 22
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          CharCase = ecUpperCase
          Color = 15794175
          ControlLabel.Width = 50
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' phi'#7871'u'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsTC
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object DBMemoEh1: TDBMemoEh
          Left = 112
          Top = 108
          Width = 653
          Height = 22
          ControlLabel.Width = 49
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'GhiChu'
          DataSource = DsTC
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 7
          Visible = True
          WantReturns = True
        end
        object CbbMaKho: TDbLookupComboboxEh2
          Left = 112
          Top = 60
          Width = 333
          Height = 22
          ControlLabel.Width = 55
          ControlLabel.Height = 16
          ControlLabel.Caption = #272'i'#7875'm b'#225'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsTC
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 331
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 412
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
        end
        object EdMaKho: TDBEditEh
          Left = 448
          Top = 60
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsTC
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object EdNumSoTien: TDBNumberEditEh
          Left = 614
          Top = 84
          Width = 151
          Height = 22
          TabStop = False
          ControlLabel.Width = 40
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' ti'#7873'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          Ctl3D = False
          DataField = 'SoTien'
          DataSource = DsTC
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 6
          Visible = True
        end
        object CbbThuNgan: TDbLookupComboboxEh2
          Left = 112
          Top = 84
          Width = 309
          Height = 22
          ControlLabel.Width = 54
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Thu ng'#226'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'NhanVienThuNgan'
          DataSource = DsTC
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'FullName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 307
            end
            item
              FieldName = 'UserName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 84
            end>
          DropDownBox.ListSource = DsTHUNGAN
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 412
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'UID'
          ListField = 'FullName'
          ListSource = DsTHUNGAN
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
        end
        object EdThuNgan: TDBEditEh
          Left = 424
          Top = 84
          Width = 101
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'LK_USERNAME'
          DataSource = DsTC
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 5
          Visible = True
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 708
    Top = 40
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 708
    ExplicitTop = 40
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 548
    Top = 348
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In phi'#7871'u'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTC
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MST'
      'LK_PTTT'
      'NGUOI')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 520
    Top = 348
  end
  object QrTC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTCBeforeOpen
    BeforeInsert = QrTCBeforeInsert
    AfterInsert = QrTCAfterInsert
    BeforeEdit = QrTCBeforeEdit
    BeforePost = QrTCBeforePost
    AfterPost = QrTCAfterPost
    AfterCancel = QrTCAfterCancel
    AfterScroll = QrTCAfterScroll
    OnCalcFields = QrTCCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'THUCHI'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 156
    Top = 212
    object QrTCIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrTCIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrTCCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrTCXOA: TWideStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrTCLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrTCNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrTCNGAYValidate
    end
    object QrTCSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrTCLK_LYDO: TWideStringField
      DisplayLabel = 'L'#253' do'
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrLYDO_THU
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'LyDo'
      Size = 50
      Lookup = True
    end
    object QrTCPTTT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldName = 'PTTT'
      Visible = False
    end
    object QrTCMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      FieldName = 'MAKHO'
      Visible = False
      OnChange = QrTCMAKHOChange
      FixedChar = True
      Size = 5
    end
    object QrTCLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrTCCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTCUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTCDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrTCCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrTCUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrTCDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrTCLK_USERNAME: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_USERNAME'
      LookupDataSet = QrTHUNGAN
      LookupKeyFields = 'UID'
      LookupResultField = 'USERNAME'
      KeyFields = 'NhanVienThuNgan'
      Size = 15
      Lookup = True
    end
    object QrTCLK_TENTHUNGAN: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENTHUNGAN'
      LookupDataSet = QrTHUNGAN
      LookupKeyFields = 'UID'
      LookupResultField = 'FULLNAME'
      KeyFields = 'NhanVienThuNgan'
      Size = 200
      Lookup = True
    end
    object QrTCKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTCLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrTCMALOC: TWideStringField
      FieldName = 'MALOC'
      Size = 5
    end
    object QrTCLK_TENLOC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENLOC'
      LookupDataSet = DataMain.QrLOC
      LookupKeyFields = 'LOC'
      LookupResultField = 'TEN'
      KeyFields = 'MALOC'
      Size = 200
      Lookup = True
    end
    object QrTCSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrTCMaCP: TWideStringField
      FieldName = 'MaCP'
    end
    object QrTCMADT: TWideStringField
      FieldName = 'MADT'
      Size = 15
    end
    object QrTCMATK: TWideStringField
      FieldName = 'MATK'
      Size = 50
    end
    object QrTCMATK2: TWideStringField
      FieldName = 'MATK2'
      Size = 50
    end
    object QrTCNhanVienThuNgan: TIntegerField
      FieldName = 'NhanVienThuNgan'
      OnChange = QrTCNhanVienThuNganChange
    end
    object QrTCSoTienTraHang: TFloatField
      FieldName = 'SoTienTraHang'
    end
    object QrTCSoGiaoDichNganHang: TWideStringField
      FieldName = 'SoGiaoDichNganHang'
      Size = 50
    end
    object QrTCGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrTCSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrTCSoDu: TFloatField
      FieldName = 'SoDu'
    end
    object QrTCNguoi: TWideStringField
      FieldName = 'Nguoi'
      Size = 50
    end
    object QrTCSoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrTCSOTIENChange
    end
    object QrTCThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrTCLyDo: TWideStringField
      FieldName = 'LyDo'
      Size = 70
    end
  end
  object DsTC: TDataSource
    DataSet = QrTC
    Left = 156
    Top = 240
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 207
    Top = 324
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 239
    Top = 324
  end
  object QrTHUNGAN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_NV_THUNGAN'
      'order by UID')
    Left = 668
    Top = 236
  end
  object ChonThungan: TwwSearchDialog
    Selected.Strings = (
      'USERNAME'#9'15'#9'T'#234'n '#273#259'ng nh'#7853'p'#9'F'
      'FULLNAME'#9'40'#9'T'#234'n '#273#7847'y '#273#7911#9'F')
    GridTitleAlignment = taCenter
    GridColor = clWhite
    Options = [opShowOKCancel, opShowStatusBar]
    GridOptions = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgPerfectRowFit]
    ShadowSearchTable = QrTHUNGAN2
    Caption = 'Ch'#7885'n'
    MaxWidth = 0
    MaxHeight = 209
    CharCase = ecNormal
    UseTFields = False
    Left = 240
    Top = 356
  end
  object QrTHUNGAN2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_NV_THUNGAN'
      ' where'#9'isnull(DISABLED, 0) = 0'
      'order by USERNAME')
    Left = 700
    Top = 236
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'THUCHI_MENHGIA'
      ' where '#9'KHOA =:KHOA'
      'order by '#9'MENHGIA desc')
    Left = 192
    Top = 212
    object QrCTCALC_STT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CALC_STT'
      Calculated = True
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTMENHGIA: TIntegerField
      FieldName = 'MENHGIA'
    end
    object QrCTSOLUONG: TIntegerField
      FieldName = 'SOLUONG'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
      OnChange = QrCTSOTIENChange
    end
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 192
    Top = 240
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrTC
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SOTIEN')
    DetailFields.Strings = (
      'SOTIEN')
    Left = 328
    Top = 368
  end
  object PopPrint: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 472
    Top = 264
    object ItemsTN: TMenuItem
      Tag = 1
      Action = CmdPrint
    end
    object ItemsTX: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = '-'
      Hint = 'In phi'#7871'u'
      OnClick = CmdPrintExecute
    end
    object ItemBangke: TMenuItem
      Caption = 'B'#7843'ng k'#234
      OnClick = ItemBangkeClick
    end
  end
  object DsTHUNGAN: TDataSource
    DataSet = QrTHUNGAN
    Left = 664
    Top = 272
  end
end
