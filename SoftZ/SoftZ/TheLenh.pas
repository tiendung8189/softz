﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TheLenh;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Mask, ADODB, DBCtrls, CR208U, ExtCtrls, ComCtrls;

type
  TFrmThelenh = class(TForm)
    EdMA: TEdit;
    UPDATE_THELENH: TADOCommand;
    StatusBar: TStatusBar;
    procedure FormShow(Sender: TObject);
    procedure EdMAKeyPress(Sender: TObject; var Key: Char);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure EdMAEnter(Sender: TObject);
  private
    mType: Integer;
    mPass, mMakho: String;
    mPort, mBaud, mWaitTime: Integer;

    mCardReader: TCr208UThread;

    function PassWDValidate(pPass: String): Boolean;
    procedure SetPassWD(pPass: String);
    procedure ReturnReadCard(pResult: Boolean; pData: String);
    procedure SetComment(const s: string);
  public
  	function Execute(pType: Integer; pMakho: String): Boolean;
  end;

var
  FrmThelenh: TFrmThelenh;

implementation

uses
	MainData, isLib, isEnCoding, isMsg, ClipBrd, IniFiles, SysUtils;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.SetComment(const s: string);
begin
	StatusBar.Panels[0].Text := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThelenh.Execute;
var
    i: Integer;
begin
    TMyForm(Self).Init1;
    mType := pType;
    mMakho := pMakho;

    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
    begin
        mPort := ReadInteger('CARD_READER', 'Port', -1);
        mBaud := ReadInteger('CARD_READER', 'Baud', 19200);
        mWaitTime := ReadInteger('CARD_READER', 'WaitTime', 5000000);
        Free;
    end;

    if mPort > 0  then
    begin
        mCardReader := TCr208UThread.Create(mPort, mBaud, mWaitTime);
        if mCardReader.ErrorCode <> 0 then
            // Auto scan port
            for i := 1 to 10 do
            begin
                mCardReader := TCr208UThread.Create(i, mBaud, mWaitTime);
                if mCardReader.ErrorCode = 0 then
                begin
                    mPort := i;
                    with TIniFile.Create(ChangeFileExt(Application.ExeName, '.ini')) do
                    begin
                        WriteInteger('CARD_READER', 'Port', mPort);
                        WriteInteger('CARD_READER', 'Baud', mBaud);
                        WriteInteger('CARD_READER', 'WaitTime', mWaitTime);
                        Free;
                    end;
                    Break;
                end;
            end;

        if mCardReader.ErrorCode = 0 then
        begin
            if mType = 0 then
            begin
                EdMA.MaxLength := 1;
                //EdMA.Top := -(EdMA.Height+20);
            end;
            Caption := '# ' + Caption;
            mCardReader.ReturnReadCard := ReturnReadCard;
        end;
    end;

    Result := ShowModal = mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.FormShow(Sender: TObject);
begin
    Clipboard.Clear;
    EdMA.Text := '';

    if mPort > 0 then
        mCardReader.StartReadCard;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.EdMAEnter(Sender: TObject);
begin
    EdMA.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.EdMAKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    begin
        if mType = 1 then
            SetPassWD(EdMA.Text)
        else
            PassWDValidate(EdMA.Text);
        EdMa.SelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (mPort > 0) and not mCardReader.Finished then
        mCardReader.Terminate;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThelenh.PassWDValidate(pPass: String): Boolean;
begin
    mPass := DataMain.GetPassCode(mMakho);
    Result := mPass = isEncodeMD5(pPass);
    if Result then
        ModalResult := mrOk
    else
    begin
		Beep;
        SetComment('Mã thẻ không hợp lệ.');
	end;
	Clipboard.Clear;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.ReturnReadCard(pResult: Boolean; pData: String);
begin
    if pResult then
    begin
        EdMA.Text := pData;
        if mType = 1 then
        begin
            SetPassWD(pData);
            if EdMA.Tag = 1 then
            begin
                mCardReader := TCr208UThread.Create(mPort, mBaud, mWaitTime);
                mCardReader.ReturnReadCard := ReturnReadCard;
                mCardReader.StartReadCard;
                // Con thieu 1 truong hop co y scan sai ma lan 2.
            end;
        end
        else
            if not PassWDValidate(pData) then
            begin
                mCardReader := TCr208UThread.Create(mPort, mBaud, mWaitTime);
                mCardReader.ReturnReadCard := ReturnReadCard;
                mCardReader.StartReadCard;
            end;
    end
    else if pData <> '' then
        SetComment(pData);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThelenh.SetPassWD;
begin
    if pPass = '' then
        Exit;

    with EdMA do
    begin
        if Tag = 0 then
        begin
            SetComment('Nhập lại mật khẩu.');
            mPass := pPass;
        end
        else if pPass <> mPass then
        begin
        	Beep;
			SetComment('Mật khẩu lặp lại không đúng.');
            mPass := '';
        end
        else	// Set PASS
        with UPDATE_THELENH do
        begin
            Prepared := True;
            Parameters[0].Value := isEncodeMD5(mPass);
            Parameters[1].Value := mMakho;
            Execute;
            ModalResult := mrOk;
        end;

        Clear;
        Tag := Tag xor 1;
    end;
end;

end.
