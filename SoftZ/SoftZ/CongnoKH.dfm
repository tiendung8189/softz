object FrmCongnoKH: TFrmCongnoKH
  Left = 270
  Top = 195
  BorderIcons = [biSystemMenu]
  Caption = 'T'#7893'ng H'#7907'p C'#244'ng N'#7907' Kh'#225'ch H'#224'ng'
  ClientHeight = 574
  ClientWidth = 1085
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 1085
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 72
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    ExplicitWidth = 1211
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Caption = 'L'#224'm m'#7899'i'
      ImageIndex = 30
      OnClick = CmdReloadExecute
    end
    object ToolButton2: TToolButton
      Left = 72
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton5: TToolButton
      Left = 80
      Top = 0
      Cursor = 1
      Action = CmdPrint3
      ImageIndex = 32
    end
    object ToolButton4: TToolButton
      Left = 152
      Top = 0
      Cursor = 1
      Action = CmdPrint2
      ImageIndex = 32
    end
    object ToolButton7: TToolButton
      Left = 224
      Top = 0
      Cursor = 1
      Action = CmdPrint4
      ImageIndex = 32
    end
    object ToolButton3: TToolButton
      Left = 296
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 32
    end
    object ToolButton6: TToolButton
      Left = 368
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 376
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 553
    Width = 1085
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitTop = 607
    ExplicitWidth = 1211
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 170
    Width = 1085
    Height = 383
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    ControlType.Strings = (
      'MARK;CheckBox;True;False')
    Selected.Strings = (
      'MADT'#9'10'#9'M'#227' kho'#9'F'
      'TENDT'#9'50'#9'T'#234'n kho'#9'F'
      'DAUKY'#9'10'#9'Ch'#7885'n'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
    Align = alClient
    DataSource = DsList
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab]
    Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    PopupMenu = popCheck
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = False
    UseTFields = False
    OnEnter = CmdRefeshExecute
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    ExplicitWidth = 1211
    ExplicitHeight = 437
  end
  inline frameTuden1: TframeTuden
    Left = 0
    Top = 36
    Width = 1085
    Height = 49
    Align = alTop
    Color = 16119285
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 3
    ExplicitTop = 36
    ExplicitWidth = 1211
    inherited PaTUDEN: TPanel
      Width = 1085
      ExplicitWidth = 1211
    end
  end
  inline frameDS_KH1: TframeDS_KH
    Left = 0
    Top = 85
    Width = 1085
    Height = 85
    Align = alTop
    Color = 16119285
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentBackground = False
    ParentColor = False
    ParentFont = False
    TabOrder = 4
    ExplicitTop = 85
    ExplicitWidth = 1211
    ExplicitHeight = 85
    inherited PaDS_KH: TPanel
      Width = 1085
      ExplicitWidth = 1211
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdUpdate: TAction
      Caption = 'Danh s'#225'ch kho'
      Hint = 'C'#7853'p nh'#7853't danh s'#225'ch kho'
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefesh: TAction
      OnExecute = CmdRefeshExecute
    end
    object CmdReload: TAction
      Caption = 'C'#7853'p nh'#7853't'
      OnExecute = CmdReloadExecute
    end
    object CmdPrint: TAction
      Caption = 'T'#7893'ng h'#7907'p'
      OnExecute = CmdPrintExecute
    end
    object CmdPrint2: TAction
      Caption = 'Phi'#7871'u'
      OnExecute = CmdPrint2Execute
    end
    object CmdPrint3: TAction
      Caption = 'Phi'#7871'u - MH'
      OnExecute = CmdPrint3Execute
    end
    object CmdPrint4: TAction
      Caption = 'Kh'#225'ch h'#224'ng'
      OnExecute = CmdPrint4Execute
    end
  end
  object DsList: TDataSource
    DataSet = QrList
    Left = 148
    Top = 224
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 276
    Top = 168
  end
  object Filter: TwwFilterDialog2
    DataSource = DsList
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 168
  end
  object popCheck: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 244
    Top = 168
  end
  object QrList: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    ProcedureName = 'spCONGNO_DOITAC;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@pTU'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pDEN'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@pDS_MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 2000
        Value = Null
      end>
    Left = 104
    Top = 225
  end
end
