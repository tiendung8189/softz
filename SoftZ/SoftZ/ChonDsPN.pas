﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsPN;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls,
  wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwDialog, wwfltdlg,
  wwFltDlg2;

type
  TFrmChonDsPN = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPhieu: TDataSource;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    SpBt1: TSpeedButton;
    SpBt2: TSpeedButton;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CbMAKHExit(Sender: TObject);
    procedure CbMAKHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure SpBt1Click(Sender: TObject);
  private
	mMADT: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pMADT: String): Boolean;
  end;

var
  FrmChonDsPN: TFrmChonDsPN;

implementation

uses
	isDb, ExCommon, isLib, Chi2, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsPN.Execute;
begin
	mMADT := pMADT;
    DsPhieu.DataSet := FrmChi2.QrNX2;

    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DS_PHIEU_PN', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
    GrBrowse.SetFocus;
    CmdRefresh.Execute;
    SpBt1.OnClick(SpBt1);

    SetDisplayFormat(FrmChi2.QrNX2, sysCurFmt);
    SetShortDateFormat(FrmChi2.QrNX2);
    SetDisplayFormat(FrmChi2.QrNX2, ['NGAY'], DateTimeFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.SpBt1Click(Sender: TObject);
var
    bm: TBytes;
begin
    with FrmChi2.QrNX2 do
    begin
        bm := Bookmark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SELECTED').AsBoolean := (Sender as TSpeedButton).Tag = 0;
            Next;
        end;
        Bookmark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPhieu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.CmdRefreshExecute(Sender: TObject);
var
    s: String;
    Tungay, Denngay: TDateTime;
begin
    Tungay  := Date;
    Denngay := Date;

//   	if (mTungay <> Tungay)  or (mDenngay <> Denngay)   then
	begin
        mTungay := Tungay;
        mDenngay := Denngay;

        with FrmChi2.QrNX2 do
        begin
			Close;

            Parameters[1].Value := mMADT;
            Parameters[2].Value := mTungay;
            Parameters[3].Value := mDenngay;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.CbMAKHExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.CbMAKHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsPN.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

end.
