﻿object FrmDmvtTTSach: TFrmDmvtTTSach
  Left = 246
  Top = 182
  BorderStyle = bsDialog
  Caption = 'Danh M'#7909'c H'#224'ng H'#243'a - Th'#244'ng Tin S'#225'ch'
  ClientHeight = 210
  ClientWidth = 328
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 0
    Top = 35
    Width = 328
    Height = 175
    DisableThemes = False
    Align = alClient
    CaptionColor = 13360356
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    DataSource = FrmDmvt.DsDMVT
    Items = <
      item
        DataSource = FrmDmvt.DsDMVT
        DataField = 'LK_TENNXB'
        Caption = 'Nh'#224' xu'#7845't b'#7843'n'
        Alignment = taRightJustify
        CustomControl = CbNXB
        WordWrap = False
      end
      item
        DataSource = FrmDmvt.DsDMVT
        DataField = 'LK_TENTG'
        Caption = 'T'#225'c gi'#7843
        Alignment = taRightJustify
        CustomControl = CbSize
        WordWrap = False
      end>
    CaptionWidth = 155
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    PaintOptions.BackgroundOptions = [coFillDataCells]
    CaptionFont.Charset = DEFAULT_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
    object CbSize: TwwDBLookupCombo
      Left = 147
      Top = 142
      Width = 167
      Height = 18
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F'
        'MA_HOTRO'#9'12'#9'MA_HOTRO'#9'F')
      DataField = 'LK_TENTG'
      DataSource = FrmDmvt.DsDMVT
      LookupTable = DataMain.QrDMTACGIA
      LookupField = 'MA_HOTRO'
      Options = [loColLines]
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 0
      Visible = False
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
    object CbNXB: TwwDBLookupCombo
      Left = 92
      Top = 142
      Width = 167
      Height = 18
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F'
        'MA_HOTRO'#9'12'#9'MA_HOTRO'#9'F')
      DataField = 'LK_TENNXB'
      DataSource = FrmDmvt.DsDMVT
      LookupTable = DataMain.QrDMNXB
      LookupField = 'MA_HOTRO'
      Options = [loColLines]
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 1
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 328
    Height = 35
    Align = alTop
    TabOrder = 1
  end
  object RzDBNavigator1: TRzDBNavigator
    Left = 188
    Top = 4
    Width = 132
    Height = 25
    Cursor = 1
    DataSource = FrmDmvt.DsDMVT
    VisibleButtons = [nbEdit, nbPost, nbCancel]
    ImageIndexes.Edit = 28
    ImageIndexes.Post = 1
    ImageIndexes.Cancel = 2
    Images = DataMain.ImageNavi
    BorderOuter = fsNone
    TabOrder = 2
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 44
    Top = 100
  end
end
