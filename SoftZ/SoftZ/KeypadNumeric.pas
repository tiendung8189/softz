﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit KeypadNumeric;

interface

uses
  SysUtils, Classes, Controls, Forms, StdCtrls, ExtCtrls, RzButton,
  AdvGlassButton, AdvSmoothButton, Buttons, Mask, AdvEdit, RzEdit, DBCtrlsEh,
  wwdbedit, DB, kbmMemTable, AdvSmoothTouchKeyBoard, Vcl.ActnList;

type
  TFrmKeypadNumeric = class(TForm)
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    tbTemp: TkbmMemTable;
    tbTempF1: TFloatField;
    DataSource1: TDataSource;
    EdMA: TwwDBEdit;
    AdvSmoothTouchKeyBoard1: TAdvSmoothTouchKeyBoard;
    ActionList1: TActionList;
    CmdClose: TAction;
    BtClose: TRzBitBtn;
    BtReturn: TRzBitBtn;
    CmdReturn: TAction;
    procedure FormShow(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReturnExecute(Sender: TObject);
  private
    mSoluong: Double;
  public
  	function Execute(var f: Double): Boolean;
  end;

var
  FrmKeypadNumeric: TFrmKeypadNumeric;

implementation

uses
	isLib, isDB, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKeypadNumeric.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKeypadNumeric.CmdReturnExecute(Sender: TObject);
begin
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmKeypadNumeric.Execute(var f: Double): Boolean;
begin
    if f > 1 then
        mSoluong := f
    else
        mSoluong := 0;

	Result := ShowModal = mrOk;
    if Result then
    begin
        tbTemp.CheckBrowseMode;
        mSoluong := tbTemp.FieldByName('F1').AsFloat;

        if mSoluong >= 1 then
            f := mSoluong
        else
            f := 1;
    end;

    tbTemp.Cancel;
    tbTemp.Close;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKeypadNumeric.FormShow(Sender: TObject);
begin
	with tbTemp do
    begin
        TFloatField(tbTempF1).DisplayFormat := sysIntFmt;

        Open;
        Append;

        FieldByName('F1').AsFloat := mSoluong;
    end;

    EdMA.SetFocus;
    EdMA.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKeypadNumeric.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKeypadNumeric.FormKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        CmdReturn.Execute;
end;

end.
