﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit CayNganhNhom;

interface

uses
  Classes, Controls, Forms,
  Db, ADODB, fcdbtreeview, Buttons, StdCtrls;

type
  TFrmCayNganhNhom = class(TForm)
    Tree: TfcDBTreeView;
    QrNHOM: TADOQuery;
    QrNHOMMA: TWideStringField;
    QrNHOMTENNHOM: TWideStringField;
    QrNHOMMANGANH: TWideStringField;
    QrNHOMMANHOM: TWideStringField;
    QrNGANH: TADOQuery;
    QrNGANHTENNGANH: TWideStringField;
    QrNGANHMANGANH: TWideStringField;
    DsNHOM: TDataSource;
    DsNGANH: TDataSource;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    QrNHOM2: TADOQuery;
    QrNHOM3: TADOQuery;
    QrNHOM4: TADOQuery;
    DsNHOM2: TDataSource;
    DsNHOM3: TDataSource;
    DsNHOM4: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure TreeDblClick(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode; Button: TMouseButton; Shift: TShiftState; X,
      Y: Integer);
    procedure FormCreate(Sender: TObject);
    procedure TntFormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdReturnClick(Sender: TObject);
    procedure TreeCalcNodeAttributes(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode);
  private
  public
  	function Execute(var sNganh, sNhom: String;
            sNhom2: String= ''; sNhom3: String= ''; sNhom4: String = ''): Boolean;
  end;

var
  FrmCayNganhNhom: TFrmCayNganhNhom;

implementation

uses
    isDb, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmCayNganhNhom.Execute;
begin
	OpenDataSets([QrNGANH, QrNHOM, QrNHOM2, QrNHOM3, QrNHOM4]);

    	(*
        ** Nhay toi node tuong ung
        *)
    QrNGANH.Locate('MANGANH', sNganh, []);
    QrNHOM.Locate('MANHOM', sNhom, []);
    QrNHOM2.Locate('MANHOM2', sNhom2, []);
    QrNHOM3.Locate('MANHOM3', sNhom3, []);
    QrNHOM4.Locate('MANHOM4', sNhom4, []);

    if sNhom4 <> '' then
		Tree.MakeActiveDataSet(QrNHOM4, False)
    else if sNhom3 <> '' then
		Tree.MakeActiveDataSet(QrNHOM3, False)
    else if sNhom2 <> '' then
		Tree.MakeActiveDataSet(QrNHOM2, False)
    else
    	Tree.MakeActiveDataSet(QrNHOM, False);
        (*
        *)

	Result := ShowModal = mrOK;
    if Result then
    begin
	    sNganh := QrNGANH.FieldByName('MANGANH').AsString;
        sNhom  := QrNHOM.FieldByName('MANHOM').AsString;

        if Tree.ActiveNode.Level > 1 then
	        sNhom2  := QrNHOM2.FieldByName('MANHOM2').AsString
        else
        	sNhom2 := '';

        if Tree.ActiveNode.Level > 2 then
	        sNhom3  := QrNHOM3.FieldByName('MANHOM3').AsString
        else
        	sNhom3 := '';

        if Tree.ActiveNode.Level > 3 then
	        sNhom4  := QrNHOM4.FieldByName('MANHOM4').AsString
        else
        	sNhom4 := '';
	end;
	CloseDataSets([QrNGANH, QrNHOM, QrNHOM2, QrNHOM3, QrNHOM4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCayNganhNhom.FormShow(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCayNganhNhom.TreeDblClick(TreeView: TfcDBCustomTreeView;
  Node: TfcDBTreeNode; Button: TMouseButton; Shift: TShiftState; X,
  Y: Integer);
begin
	CmdReturnClick(Nil);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCayNganhNhom.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCayNganhNhom.TntFormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)      
procedure TFrmCayNganhNhom.CmdReturnClick(Sender: TObject);
begin
    if Tree.ActiveNode <> Nil then
    	if Tree.ActiveNode.Level > 0 then
        begin
			ModalResult := mrOK;
            Exit;
        end;
    Tree.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmCayNganhNhom.TreeCalcNodeAttributes(
  TreeView: TfcDBCustomTreeView; Node: TfcDBTreeNode);
begin
	with Node do
        ImageIndex := Level
end;

end.

