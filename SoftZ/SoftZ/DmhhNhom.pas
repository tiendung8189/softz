﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmhhNhom;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2, ExtCtrls,
  ADODb, Db, Menus, AdvMenus,
  AppEvnts, Wwdbgrid,
  fcdbtreeview, RzSplit, Wwdbigrd, RzPanel, ToolWin;

type
  TFrmDmhhNhom = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    ToolButton12: TToolButton;
    QrNhom: TADOQuery;
    DsNhom: TDataSource;
    QrNganh: TADOQuery;
    DsNganh: TDataSource;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    Tm1: TMenuItem;
    QrNganhMANGANH: TWideStringField;
    QrNganhTENNGANH: TWideStringField;
    QrNhomMANHOM: TWideStringField;
    QrNhomMANGANH: TWideStringField;
    QrNhomMA: TWideStringField;
    QrNhomTENNHOM: TWideStringField;
    QrNhom2: TADOQuery;
    DsNhom2: TDataSource;
    QrNhom3: TADOQuery;
    DsNhom3: TDataSource;
    QrNhom2MANHOM: TWideStringField;
    QrNhom2MA: TWideStringField;
    QrNhom3MA: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    GrNganh: TwwDBGrid2;
    TabSheet2: TTabSheet;
    GrNhom: TwwDBGrid2;
    TntTabSheet1: TTabSheet;
    GrNhom2: TwwDBGrid2;
    TntTabSheet2: TTabSheet;
    GrNhom3: TwwDBGrid2;
    TreeView: TfcDBTreeView;
    QrNhom4: TADOQuery;
    StringField3: TWideStringField;
    DsNhom4: TDataSource;
    TntTabSheet3: TTabSheet;
    GrNhom4: TwwDBGrid2;
    QrNganhGHICHU: TWideStringField;
    QrNganhCREATE_BY: TIntegerField;
    QrNganhUPDATE_BY: TIntegerField;
    QrNganhCREATE_DATE: TDateTimeField;
    QrNganhUPDATE_DATE: TDateTimeField;
    QrNhomGHICHU: TWideStringField;
    QrNhomCREATE_BY: TIntegerField;
    QrNhomUPDATE_BY: TIntegerField;
    QrNhomCREATE_DATE: TDateTimeField;
    QrNhomUPDATE_DATE: TDateTimeField;
    QrNhom2MANHOM2: TWideStringField;
    QrNhom2TENNHOM2: TWideStringField;
    QrNhom2GHICHU: TWideStringField;
    QrNhom2CREATE_BY: TIntegerField;
    QrNhom2UPDATE_BY: TIntegerField;
    QrNhom2CREATE_DATE: TDateTimeField;
    QrNhom2UPDATE_DATE: TDateTimeField;
    QrNhom3MANHOM3: TWideStringField;
    QrNhom3MANHOM2: TWideStringField;
    QrNhom3TENNHOM3: TWideStringField;
    QrNhom3GHICHU: TWideStringField;
    QrNhom3CREATE_BY: TIntegerField;
    QrNhom3UPDATE_BY: TIntegerField;
    QrNhom3CREATE_DATE: TDateTimeField;
    QrNhom3UPDATE_DATE: TDateTimeField;
    QrNhom4MANHOM4: TWideStringField;
    QrNhom4MANHOM3: TWideStringField;
    QrNhom4TENNHOM4: TWideStringField;
    QrNhom4GHICHU: TWideStringField;
    QrNhom4CREATE_BY: TIntegerField;
    QrNhom4UPDATE_BY: TIntegerField;
    QrNhom4CREATE_DATE: TDateTimeField;
    QrNhom4UPDATE_DATE: TDateTimeField;
    QrNhomPLU: TBooleanField;
    QrNhom2MANGANH: TWideStringField;
    QrNhom3MANGANH: TWideStringField;
    QrNhom3MANHOM: TWideStringField;
    QrNhom4MANHOM: TWideStringField;
    QrNhom4MANHOM2: TWideStringField;
    QrNhom4MANGANH: TWideStringField;
    QrNhom2PLU: TBooleanField;
    CmdAudit: TAction;
    QrNhomNL: TBooleanField;
    QrNhomVL: TBooleanField;
    QrNhomCB: TBooleanField;
    QrNhomTL_HOAHONG: TFloatField;
    CmdExportDataGrid: TAction;
    N1: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    QrNganhNHOM: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrNhomCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrNhomBeforePost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrNganhBeforePost(DataSet: TDataSet);
    procedure PgMainChange(Sender: TObject);
    procedure QrNganhBeforeInsert(DataSet: TDataSet);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CbManganhNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrNhomAfterInsert(DataSet: TDataSet);
    procedure QrNganhBeforeDelete(DataSet: TDataSet);
    procedure QrNhom2BeforePost(DataSet: TDataSet);
    procedure QrNhom2AfterInsert(DataSet: TDataSet);
    procedure QrNhom3BeforePost(DataSet: TDataSet);
    procedure QrNhom3AfterInsert(DataSet: TDataSet);
    procedure QrNhom4AfterInsert(DataSet: TDataSet);
    procedure QrNhom4BeforePost(DataSet: TDataSet);
    procedure TreeViewCalcNodeAttributes(TreeView: TfcDBCustomTreeView;
      Node: TfcDBTreeNode);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrNganhAfterInsert(DataSet: TDataSet);
  private
    mCanEdit: Boolean;
    mGrid: TwwDBGrid2;
    mQuery: TADOQuery;
    mDs: TDataSource;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmhhNhom: TFrmDmhhNhom;

implementation

uses
	ExCommon, MainData, isDb, isMsg, RepEngine, Rights, isLib, isCommon, OfficeData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.Execute;
begin
	mCanEdit := rCanEdit(r);

    ShowModal;
end;

    (*
    **  Forms
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE1: String   = 'DM_HH_NGANH';
    FORM_CODE2: String   = 'DM_HH_NHOM';
    FORM_CODE3: String   = 'DM_HH_NHOM_2';
    FORM_CODE4: String   = 'DM_HH_NHOM_3';
    FORM_CODE5: String   = 'DM_HH_NHOM_4';

    TABLE_NAME = 'DM_NGANH';
    TABLE_NAME2 = 'DM_NHOM';
    TABLE_NAME3 = 'DM_NHOM2';
    TABLE_NAME4 = 'DM_NHOM3';
    TABLE_NAME5 = 'DM_NHOM4';

    REPORT_NAME = 'DM_NGANHNHOM';

procedure TFrmDmhhNhom.FormCreate(Sender: TObject);
var
	n: Integer;
begin
	TMyForm(Self).Init1;
    mTrigger := False;

    AddAllFields(QrNganh, TABLE_NAME, 0);
    AddAllFields(QrNhom, TABLE_NAME2, 0);
    AddAllFields(QrNhom2, TABLE_NAME3, 0);
    AddAllFields(QrNhom3, TABLE_NAME4, 0);
    AddAllFields(QrNhom4, TABLE_NAME5, 0);

    // May cap?
    n := FlexConfigInteger(FORM_CODE1, 'Depth');
    with PgMain do
	    while n < PageCount do
        begin
        	Pages[n].TabVisible := False;
            Inc(n);
        end;

    // Nganh
    n := FlexConfigInteger(FORM_CODE1, 'Code Length');
	with QrNganh.FieldByName('MANGANH') do
	    if n > 0 then
        begin
            DisplayWidth := n;
            Size := n;
        end;

    // Nhom
    n := FlexConfigInteger(FORM_CODE2, 'Code Length');
	with QrNhom.FieldByName('MA') do
	    if n > 0 then
        begin
            DisplayWidth := n;
            Size := n;
        end;

    // Nhom 2
    n := FlexConfigInteger(FORM_CODE3, 'Code Length');
	with QrNhom2.FieldByName('MA') do
	    if n > 0 then
        begin
            DisplayWidth := n;
            Size := n;
        end;

    // Nhom 3
    n := FlexConfigInteger(FORM_CODE4, 'Code Length');
	with QrNhom3.FieldByName('MA') do
	    if n > 0 then
        begin
            DisplayWidth := n;
            Size := n;
        end;

    // Nhom 4
    n := FlexConfigInteger(FORM_CODE5, 'Code Length');
	with QrNhom4.FieldByName('MA') do
	    if n > 0 then
        begin
            DisplayWidth := n;
            Size := n;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.FormShow(Sender: TObject);
begin
	GrNganh.ReadOnly    := not mCanEdit;
	GrNhom.ReadOnly     := not mCanEdit;
	GrNhom2.ReadOnly    := not mCanEdit;
	GrNhom3.ReadOnly    := not mCanEdit;
	GrNhom4.ReadOnly    := not mCanEdit;

    SetDisplayFormat(QrNhom, ['TL_HOAHONG'], sysPerFmt);

    SetDictionary([QrNganh, QrNhom, QrNhom2, QrNhom3, QrNhom4],
    	[FORM_CODE1, FORM_CODE2, FORM_CODE3, FORM_CODE4, FORM_CODE5], []);

    SetCustomGrid(
        [FORM_CODE1, FORM_CODE2, FORM_CODE3, FORM_CODE4, FORM_CODE5],
        [GrNganh, GrNhom, GrNhom2, GrNhom3, GrNhom4]);

    OpenDataSets([QrNganh, QrNhom, QrNhom2, QrNhom3, QrNhom4]);
    PgMain.OnChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(mQuery, True)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrNhom4, QrNhom3, QrNhom2, QrNhom, QrNganh]);
    finally
    end;
	Action := caFree;
end;

    (*
    ** Page cotrol
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.PgMainChange(Sender: TObject);

begin
	case PgMain.ActivePageIndex of
    0:
    	begin
        	mQuery := QrNganh;
            mDs := DsNganh;
            mGrid  := GrNganh;
        end;
    1:
    	begin
            mQuery := QrNhom;
            mDs := DsNhom;
            mGrid  := GrNhom;
        end;
    2:
    	begin
            mQuery := QrNhom2;
            mDs := DsNhom2;
            mGrid  := GrNhom2;
        end;
    3:
    	begin
            mQuery := QrNhom3;
            mDs := DsNhom3;
            mGrid  := GrNhom3;
        end;
    4:
    	begin
            mQuery := QrNhom4;
            mDs := DsNhom4;
            mGrid  := GrNhom4;
        end;
    end;

	HideAudit;
    mGrid.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

	(*
    ** Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdNewExecute(Sender: TObject);
begin
	mQuery.Append;
    mGrid.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdSaveExecute(Sender: TObject);
begin
	mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdCancelExecute(Sender: TObject);
begin
	mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdDelExecute(Sender: TObject);
begin
	mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    case PgMain.ActivePageIndex of
        0: DataOffice.ExportDataGrid(GrNganh);
        1: DataOffice.ExportDataGrid(GrNhom);
        2: DataOffice.ExportDataGrid(GrNhom2);
        3: DataOffice.ExportDataGrid(GrNhom3);
        4: DataOffice.ExportDataGrid(GrNhom4);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, REPORT_NAME, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name + IntToStr(PgMain.ActivePageIndex), mGrid.DataSource);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, mQuery, mCanEdit);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.OnDbError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Nganh
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNganhAfterInsert(DataSet: TDataSet);
begin
    with QrNganh do
    begin
        FieldByName('LOAI').AsString := 'HH';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNganhBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNganhBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNganhBeforePost(DataSet: TDataSet);
begin
	with QrNganh do
    begin
	   	if BlankConfirm(QrNganh, ['MANGANH', 'LOAI']) then
    	   	Abort;

	   	if LengthConfirm(QrNganh, ['MANGANH']) then
    	    Abort;

	   	if BlankConfirm(QrNganh, ['TENNGANH']) then
    	   	Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhomBeforePost(DataSet: TDataSet);
begin
	with QrNhom do
    begin
	    if BlankConfirm(QrNhom, ['MA']) then
    	    Abort;

	    if LengthConfirm(QrNhom, ['MA']) then
    	    Abort;

	    if BlankConfirm(QrNhom, ['TENNHOM']) then
    	    Abort;

        FieldByName('MANHOM').AsString  := FieldByName('MANGANH').AsString +
        	FieldByName('MA').AsString;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhomAfterInsert(DataSet: TDataSet);
begin
    with QrNhom do
    begin
        FieldByName('MANGANH').AsString := QrNGANH.FieldByName('MANGANH').AsString;
        FieldByName('PLU').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom2BeforePost(DataSet: TDataSet);
begin
	with QrNhom2 do
    begin
	    if BlankConfirm(QrNhom2, ['MA']) then
    	    Abort;

	    if LengthConfirm(QrNhom2, ['MA']) then
    	    Abort;

	    if BlankConfirm(QrNhom2, ['TENNHOM2']) then
    	    Abort;

        FieldByName('MANHOM2').AsString := FieldByName('MANHOM').AsString +
        	FieldByName('MA').AsString;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom2AfterInsert(DataSet: TDataSet);
begin
    with QrNhom2 do
    begin
        FieldByName('MANHOM').AsString := QrNHOM.FieldByName('MANHOM').AsString;
        FieldByName('PLU').AsBoolean := False;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom3BeforePost(DataSet: TDataSet);
begin
	with QrNhom3 do
    begin
	    if BlankConfirm(QrNhom3, ['MA']) then
    	    Abort;

	    if LengthConfirm(QrNhom3, ['MA']) then
    	    Abort;

	    if BlankConfirm(QrNhom3, ['TENNHOM3']) then
    	    Abort;

        FieldByName('MANHOM3').AsString := FieldByName('MANHOM2').AsString +
        	FieldByName('MA').AsString;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom3AfterInsert(DataSet: TDataSet);
begin
    with QrNhom3 do
        FieldByName('MANHOM2').AsString := QrNhom2.FieldByName('MANHOM2').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom4BeforePost(DataSet: TDataSet);
begin
	with QrNhom4 do
    begin
	    if BlankConfirm(QrNhom4, ['MA']) then
    	    Abort;

	    if LengthConfirm(QrNhom4, ['MA']) then
    	    Abort;

	    if BlankConfirm(QrNhom4, ['TENNHOM4']) then
    	    Abort;

        FieldByName('MANHOM4').AsString := FieldByName('MANHOM3').AsString +
        	FieldByName('MA').AsString;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.QrNhom4AfterInsert(DataSet: TDataSet);
begin
    with QrNhom4 do
        FieldByName('MANHOM3').AsString := QrNhom3.FieldByName('MANHOM3').AsString;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CbManganhNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	if mQuery = Nil then
    	Exit;
    Status.SimpleText := RecordCount(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.GrNhomCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MANGANH') or (Field.FullName = 'MA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.TreeViewCalcNodeAttributes(
  TreeView: TfcDBCustomTreeView; Node: TfcDBTreeNode);
begin
	with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmhhNhom.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, mDs);
end;

end.
