program SoftZ;



uses
  Forms,
  Main in 'Main.pas' {FrmMain},
  MainData in 'MainData.pas' {DataMain: TDataModule},
  TonkhoDK in 'TonkhoDK.pas' {FrmTonkhoDK},
  Params in 'Params.pas' {FrmParams},
  GmsRep in 'GmsRep.pas' {FrmGmsRep},
  ExCommon in 'ExCommon.pas',
  Dmkhac3 in 'Dmkhac3.pas' {FrmDmkhac3},
  Dmvt in 'Dmvt.pas' {FrmDmvt},
  Banle in 'Banle.pas' {FrmBanle},
  Soluong in 'Soluong.pas' {FrmSoluong},
  Xuat in 'Xuat.pas' {FrmXuat},
  DondhNCC in 'DondhNCC.pas' {FrmDondhNCC},
  Dmkhac in 'Dmkhac.pas' {FrmDmkhac},
  Khoaso in 'Khoaso.pas' {FrmKhoaso},
  ChonNgay in 'ChonNgay.pas' {FrmChonNgay},
  Kiemke in 'Kiemke.pas' {FrmKiemke},
  ChuyenKho in 'ChuyenKho.pas' {FrmChuyenKho},
  Dmkho in 'Dmkho.pas' {FrmDmkho},
  Inlabel in 'Inlabel.pas' {FrmInlabel},
  ChonMavt in 'ChonMavt.pas' {FrmChonMavt},
  Scan in 'Scan.pas' {FrmScan},
  Tuden in 'Tuden.pas' {FrmTuden},
  Dmkh in 'Dmkh.pas' {FrmDmkh},
  Dmncc in 'Dmncc.pas' {FrmDmncc},
  CayNganhNhom in 'CayNganhNhom.pas' {FrmCayNganhNhom},
  BkgrOption in 'BkgrOption.pas' {FrmBkgrOption},
  NXKhac in 'NXKhac.pas' {FrmNXKhac},
  Mamoi in 'Mamoi.pas' {FrmMamoi},
  DmVIP in 'DmVIP.pas' {FrmDmVIP},
  ExVIP in 'ExVIP.pas' {FrmExVIP},
  Tygia in 'Tygia.pas' {FrmTygia},
  Dmsize in 'Dmsize.pas' {FrmDmsize},
  ChonDsma in 'ChonDsma.pas' {FrmChonDsma},
  ChonPhieunhap in 'ChonPhieunhap.pas' {FrmChonPhieunhap},
  Dmquaytn in 'Dmquaytn.pas' {FrmDmquaytn},
  ChonDsNCC in 'ChonDsNCC.pas' {FrmChonDsNCC},
  ChonDsKH in 'ChonDsKH.pas' {FrmChonDsKH},
  Dmdl in 'Dmdl.pas' {FrmDmdl},
  ChonKh in 'ChonKh.pas' {FrmChonKh},
  ChonDondh in 'ChonDondh.pas' {FrmChonDondh},
  frameNgay in 'frameNgay.pas' {frNGAY: TFrame},
  frameNavi in 'frameNavi.pas' {frNavi: TFrame},
  DondhKH in 'DondhKH.pas' {FrmDondhKH},
  Lienhe in 'Lienhe.pas' {FrmLienhe: TForm},
  DmhhNhom in 'DmhhNhom.pas' {FrmDmhhNhom: TForm},
  Thu in 'Thu.pas' {FrmThu: TForm},
  Chi in 'Chi.pas' {FrmChi: TForm},
  DieuchinhXuat in 'DieuchinhXuat.pas' {FrmDieuchinhXuat: TForm},
  DmvtBo in 'DmvtBo.pas' {FrmDmvtBo: TForm},
  Nhaptrabl in 'Nhaptrabl.pas' {FrmNhaptrabl: TForm},
  HoadonGTGT in 'HoadonGTGT.pas' {FrmHoadonGTGT: TForm},
  HoadonGTGTGetBill in 'HoadonGTGTGetBill.pas' {FrmHoadonGTGTGetBill: TForm},
  Dmmau in 'Dmmau.pas' {FrmDmmau: TForm},
  DieuchinhNhap in 'DieuchinhNhap.pas' {FrmDieuchinhNhap: TForm},
  NhapTra in 'NhapTra.pas' {FrmNhapTra: TForm},
  ChonPhieuXuat in 'ChonPhieuXuat.pas' {FrmChonPhieuXuat: TForm},
  XuatTra in 'XuatTra.pas' {FrmXuatTra: TForm},
  Ketchuyentien in 'Ketchuyentien.pas' {FrmKetchuyentien: TForm},
  ChonDsHH in 'ChonDsHH.pas' {FrmChonDsHH: TForm},
  ChonDsNhom in 'ChonDsNhom.pas' {FrmChonDsNhom: TForm},
  TimBarcode in 'TimBarcode.pas' {FrmTimBarcode: TForm},
  ChonDsKho in 'ChonDsKho.pas' {FrmChonDsKho: TForm},
  ThuThungan in 'ThuThungan.pas' {FrmThuThungan: TForm},
  Chikhac in 'Chikhac.pas' {FrmChikhac: TForm},
  ChiThungan in 'ChiThungan.pas' {FrmChiThungan: TForm},
  Thukhac in 'Thukhac.pas' {FrmThukhac: TForm},
  ChonPhieuKM in 'ChonPhieuKM.pas' {FrmChonPhieuKM: TForm},
  Khuyenmai in 'Khuyenmai.pas' {FrmKhuyenmai: TForm},
  Khuyenmai2 in 'Khuyenmai2.pas' {FrmKhuyenmai2: TForm},
  frameKho in 'frameKho.pas' {frKHO: TFrame},
  Tonkho in 'Tonkho.pas' {FrmTonkho},
  crCommon in 'CrFrames\crCommon.pas' {CrFrame: TFrame},
  crCT_TH in 'CrFrames\crCT_TH.pas' {frameCT_TH: TFrame},
  crDS_KH in 'CrFrames\crDS_KH.pas' {frameDS_KH: TFrame},
  crDS_KHO in 'CrFrames\crDS_KHO.pas' {frameDS_KHO: TFrame},
  crDS_NCC in 'CrFrames\crDS_NCC.pas' {frameDS_NCC: TFrame},
  crDS_NHOMVT in 'CrFrames\crDS_NHOMVT.pas' {FrameDS_NHOMVT: TFrame},
  crKho in 'CrFrames\crKho.pas' {frameCrKho: TFrame},
  crMAHH in 'CrFrames\crMAHH.pas' {FrameMAHH: TFrame},
  crMAKH in 'CrFrames\crMAKH.pas' {frameMAKH: TFrame},
  crMAVIP in 'CrFrames\crMAVIP.pas' {frameMAVIP: TFrame},
  crMAVT in 'CrFrames\crMAVT.pas' {frameMAVT: TFrame},
  crNAM in 'CrFrames\crNAM.pas' {frameNAM: TFrame},
  crNGANHNHOM in 'CrFrames\crNGANHNHOM.pas' {frameNGANHNHOM: TFrame},
  crNgay in 'CrFrames\crNgay.pas' {frameCrNgay: TFrame},
  crQUY in 'CrFrames\crQUY.pas' {frameQUY: TFrame},
  crSILE in 'CrFrames\crSILE.pas' {frameSILE: TFrame},
  crTHANGNAM in 'CrFrames\crTHANGNAM.pas' {frameTHANGNAM: TFrame},
  crTHOIGIAN in 'CrFrames\crTHOIGIAN.pas' {frameTHOIGIAN: TFrame},
  crTon in 'CrFrames\crTon.pas' {frameTon: TFrame},
  crTOP in 'CrFrames\crTOP.pas' {frameTOP: TFrame},
  crTuden in 'CrFrames\crTuden.pas' {frameTuden: TFrame},
  FlexRep in 'FlexRep.pas' {FrmFlexRep},
  FlexRep3 in 'FlexRep3.pas' {FrmFlexRep3},
  DmKhuyenmai in 'DmKhuyenmai.pas' {FrmDmKhuyenmai},
  Khuyenmai3 in 'Khuyenmai3.pas' {FrmKhuyenmai3},
  frameScanCode in 'frameScanCode.pas' {frScanCode: TFrame},
  Nhaptrabl2 in 'Nhaptrabl2.pas' {FrmNhaptrabl2},
  frameScanCode2 in 'frameScanCode2.pas' {frScanCode2: TFrame},
  Sapthutu in 'Sapthutu.pas' {FrmSapthutu},
  ChiHT in 'ChiHT.pas' {FrmChiHT},
  ThuHT in 'ThuHT.pas' {FrmThuHT},
  DmLoaiVipCT in 'DmLoaiVipCT.pas' {FrmDmLoaiVipCT},
  ChonDondh3 in 'ChonDondh3.pas' {FrmChonDondh3},
  TheodoiGia in 'TheodoiGia.pas' {FrmTheodoiGia},
  DmTK in 'DmTK.pas' {FrmDmTK},
  Thu2 in 'Thu2.pas' {FrmThu2},
  Chi2 in 'Chi2.pas' {FrmChi2},
  ChonPhieuNX in 'ChonPhieuNX.pas' {FrmChonPhieuNX},
  ReceiptDesc in 'ReceiptDesc.pas' {FrmReceiptDesc},
  ExcelData in 'ExcelData.pas' {DataExcel: TDataModule},
  ChonDsPN in 'ChonDsPN.pas' {FrmChonDsPN},
  ChonDsPX in 'ChonDsPX.pas' {FrmChonDsPX},
  GuidEx in 'GuidEx.pas',
  exThread in 'exThread.pas',
  CardReader in 'CardReader.pas' {FrmCardReader},
  CR208U in 'CR208U.pas',
  TheLenh in 'TheLenh.pas' {FrmThelenh},
  HoadonGTGTCT in 'HoadonGTGTCT.pas' {FrmHoadonGTGTCT},
  OfficeData in 'OfficeData.pas' {DataOffice: TDataModule},
  exPrintBill in 'exPrintBill.pas',
  ChonDsLydoNK in 'ChonDsLydoNK.pas' {FrmChonDsLydoNK},
  ChonDsLydoXK in 'ChonDsLydoXK.pas' {FrmChonDsLydoXK},
  ChonDsUser in 'ChonDsUser.pas' {FrmChonDsUser},
  crDS_LYDO_NK in 'crFrames\crDS_LYDO_NK.pas' {frameDS_LYDO_NK: TFrame},
  crDS_LYDO_XK in 'crFrames\crDS_LYDO_XK.pas' {frameDS_LYDO_XK: TFrame},
  crDS_USER in 'crFrames\crDS_USER.pas' {frameDS_USER: TFrame},
  TimBarcode2 in 'TimBarcode2.pas' {FrmTimBarcode2},
  DmKho2 in 'DmKho2.pas' {FrmDmKho2},
  RepEngine in 'RepEngine.pas' {FrmRep},
  crUID in 'crFrames\crUID.pas' {frameUID: TFrame},
  crSAMS_FUNC in '..\..\Softz.Admin\crFrames\crSAMS_FUNC.pas' {frameSAMS_FUNC: TFrame},
  crSAMS_GROUP in '..\..\Softz.Admin\crFrames\crSAMS_GROUP.pas' {frameSAMS_GROUP: TFrame},
  crSAMS_REP in '..\..\Softz.Admin\crFrames\crSAMS_REP.pas' {frameSAMS_REP: TFrame},
  crSAMS_USER in '..\..\Softz.Admin\crFrames\crSAMS_USER.pas' {frameSAMS_USER: TFrame},
  AddList in '..\..\Softz.Admin\AddList.pas' {FrmAddList},
  AdminData in '..\..\Softz.Admin\AdminData.pas' {Data: TDataModule},
  ExAdminCommon in '..\..\Softz.Admin\ExAdminCommon.pas',
  ListUser in '..\..\Softz.Admin\ListUser.pas' {FrmListUser},
  Users in '..\..\Softz.Admin\Users.pas' {FrmUser},
  ListGroup in '..\..\Softz.Admin\ListGroup.pas' {FrmListGroup},
  Groups in '..\..\Softz.Admin\Groups.pas' {FrmGroup},
  FunctionAccess in '..\..\Softz.Admin\FunctionAccess.pas' {FrmFuncAccess},
  ReportAccess in '..\..\Softz.Admin\ReportAccess.pas' {FrmRepAccess},
  FlexEdit in '..\..\Softz.Admin\FlexEdit.pas' {FrmFlexEdit},
  CustomizeGrid in '..\..\Softz.Admin\CustomizeGrid.pas' {FrmCustomizeGrid},
  Dictionary in '..\..\Softz.Admin\Dictionary.pas' {FrmDictionary},
  GridOption in '..\..\Softz.Admin\GridOption.pas' {FrmGridOption},
  FunctionAdmin in '..\..\Softz.Admin\FunctionAdmin.pas' {FrmFuncAdmin},
  RepConfig in '..\..\Softz.Admin\RepConfig.pas' {FrmRepConfig},
  DmKhNcc in 'DmKhNcc.pas' {FrmDmKhNcc},
  SetLicense in 'SetLicense.pas' {FrmSetLicense},
  DmLoaiVip in 'DmLoaiVip.pas' {FrmDmLoaiVip},
  DmvtCT in 'DmvtCT.pas' {FrmDmvtCT},
  DmvtPB in 'DmvtPB.pas' {FrmDmvtPB},
  ChonDsNhomCk in 'ChonDsNhomCk.pas' {FrmChonDsNhomCk},
  ChonDsNhomNCC in 'ChonDsNhomNCC.pas' {FrmChonDsNhomNCC},
  crMANCC in 'CrFrames\crMANCC.pas' {frameMANCC: TFrame},
  Nhap in 'Nhap.pas' {FrmNhap},
  RepEncode in '..\..\Softz.Admin\RepEncode.pas' {FrmRepEncode},
  ImportExcel in 'ImportExcel.pas' {FrmImportExcel},
  ImportExcelRev in 'ImportExcelRev.pas' {FrmImportExcelRev},
  ChonDondh2 in 'ChonDondh2.pas' {FrmChonDondh2},
  Tienthue in 'Tienthue.pas' {FrmTienthue},
  frameLoc in 'frameLoc.pas' {frLoc: TFrame},
  EximOption in '..\..\Softz.Admin\EximOption.pas' {FrmEximOption},
  CongnoKH in 'CongnoKH.pas' {FrmCongnoKH},
  CongnoNCC in 'CongnoNCC.pas' {FrmCongnoNCC},
  crDS_LOC in 'crFrames\crDS_LOC.pas' {frameDS_LOC: TFrame},
  ChonDsLoc in 'ChonDsLoc.pas' {FrmChonDsLoc},
  ChonDsHHKM in 'ChonDsHHKM.pas' {FrmChonDsHHKM},
  crDS_NV in 'crFrames\crDS_NV.pas' {frameDS_NV: TFrame},
  ChonDsNV in 'ChonDsNV.pas' {FrmChonDsNV},
  DmNganhang in 'DmNganhang.pas' {FrmDmNganhang},
  crSOTHANG in 'crFrames\crSOTHANG.pas' {frameSOTHANG: TFrame},
  Params2 in 'Params2.pas' {FrmParams2},
  crLOC in 'crFrames\crLOC.pas' {frameCrLOC: TFrame},
  crCHIETKHAU in 'crFrames\crCHIETKHAU.pas' {frameCHIETKHAU: TFrame},
  crSONGAY in 'crFrames\crSONGAY.pas' {frameSONGAY: TFrame},
  DmvtThuoctinh in 'DmvtThuoctinh.pas' {FrmDmvtThuoctinh},
  DmvtTTSach in 'DmvtTTSach.pas' {FrmDmvtTTSach},
  DmHotro in 'DmHotro.pas' {FrmDmHotro},
  InlabelChungtu in 'InlabelChungtu.pas' {FrmInlabelChungtu},
  PhieuQuatang in 'PhieuQuatang.pas' {FrmPhieuQuatang},
  crBANLE_TT in 'crFrames\crBANLE_TT.pas' {frameBANLE_TT: TFrame},
  CheckTonkho in 'CheckTonkho.pas' {FrmCheckTonkho},
  FastReport in 'FastReport.pas' {FrmFastReport: TDataModule},
  POM_DmChiphi in 'POM_DmChiphi.pas' {FrmPOM_DmChiphi},
  POM_DmhhNhom in 'POM_DmhhNhom.pas' {FrmPOM_DmhhNhom},
  POM_DmHesoDD in 'POM_DmHesoDD.pas' {FrmPOM_DmHesoDD},
  POM_DmHesoDT in 'POM_DmHesoDT.pas' {FrmPOM_DmHesoDT},
  POM_DmvtNVL in 'POM_DmvtNVL.pas' {FrmPOM_DmvtNVL},
  POM_DmvtBTP in 'POM_DmvtBTP.pas' {FrmPOM_DmvtBTP},
  POM_DmvtBTP_Dinhmuc in 'POM_DmvtBTP_Dinhmuc.pas' {FrmPOM_DmvtBTP_Dinhmuc},
  TudenTon in 'TudenTon.pas' {FrmTudenTon};

{$R *.RES}

begin
  Application.Initialize;
  Application.Title := 'SoftZ Solutions';
  Application.CreateForm(TFrmMain, FrmMain);
  Application.CreateForm(TDataMain, DataMain);
  Application.CreateForm(TFrmSoluong, FrmSoluong);
  Application.CreateForm(TFrmChonNgay, FrmChonNgay);
  Application.CreateForm(TFrmTuden, FrmTuden);
  Application.CreateForm(TFrmMamoi, FrmMamoi);
  Application.CreateForm(TFrmChonDsma, FrmChonDsma);
  Application.CreateForm(TFrmChonDsNCC, FrmChonDsNCC);
  Application.CreateForm(TFrmChonDsKH, FrmChonDsKH);
  Application.CreateForm(TFrmChonDsHH, FrmChonDsHH);
  Application.CreateForm(TFrmChonDsNhom, FrmChonDsNhom);
  Application.CreateForm(TFrmChonDsKho, FrmChonDsKho);
  Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
  Application.CreateForm(TDataExcel, DataExcel);
  Application.CreateForm(TDataOffice, DataOffice);
  Application.CreateForm(TFrmChonDsLydoNK, FrmChonDsLydoNK);
  Application.CreateForm(TFrmChonDsLydoXK, FrmChonDsLydoXK);
  Application.CreateForm(TFrmChonDsUser, FrmChonDsUser);
  Application.CreateForm(TFrmRep, FrmRep);
  Application.CreateForm(TDataAdmin, DataAdmin);
  Application.CreateForm(TFrmSetLicense, FrmSetLicense);
  Application.CreateForm(TFrmChonDsNhomCk, FrmChonDsNhomCk);
  Application.CreateForm(TFrmChonDsNhomNCC, FrmChonDsNhomNCC);
  Application.CreateForm(TFrmImportExcel, FrmImportExcel);
  Application.CreateForm(TFrmImportExcelRev, FrmImportExcelRev);
  Application.CreateForm(TFrmChonDsLoc, FrmChonDsLoc);
  Application.CreateForm(TFrmChonDsNV, FrmChonDsNV);
  Application.CreateForm(TFrmFastReport, FrmFastReport);
  if not DataMain.Logon then
  begin
      Application.Terminate;
      Exit;
  end;

  Application.Run;
end.
