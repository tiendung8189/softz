﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmkhac3;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, Grids, StdCtrls, Db,
  Wwdbgrid2, ADODb, wwdblook,
  AppEvnts, Menus, AdvMenus, DBCtrls, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmkhac3 = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    QrDmkhac: TADOQuery;
    DsDmkhac: TDataSource;
    CmdSearch: TAction;
    QrDmkhacX: TADOQuery;
    DsDmkhacX: TDataSource;
    Panel1: TPanel;
    CbKhac: TwwDBLookupCombo;
    Label1: TLabel;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    QrDmkhacLOAI: TWideStringField;
    QrDmkhacMA: TWideStringField;
    QrDmkhacDGIAI: TWideStringField;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    TntDBMemo1: TDBMemo;
    CmdAdmin: TAction;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDmkhacBeforePost(DataSet: TDataSet);
    procedure QrDmkhacBeforeDelete(DataSet: TDataSet);
    procedure QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrDmkhacBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbKhacNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrDmkhacAfterInsert(DataSet: TDataSet);
    procedure CmdAdminExecute(Sender: TObject);
    procedure TntDBMemo1Exit(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
  private
  	mCanEdit, fixCode: Boolean;

    procedure OpenQueries;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmkhac3: TFrmDmkhac3;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib;

{$R *.DFM}

const
	FORM_CODE = 'DM_KHAC2';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1('_IDI_BOOKS');
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDmkhac, FORM_CODE);
    fixCode := SetCodeLength(FORM_CODE, QrDmkhac.FieldByName('MA'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdNewExecute(Sender: TObject);
begin
	QrDmKhac.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdSaveExecute(Sender: TObject);
begin
	QrDmKhac.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdCancelExecute(Sender: TObject);
begin
	QrDmKhac.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdDelExecute(Sender: TObject);
begin
	QrDmKhac.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDmKhac, QrDmKhacX]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.OpenQueries;
begin
	OpenDataSets([QrDmKhacX]);
    CbKhac.LookupValue := QrDmKhacX.FieldByName('LOAI').AsString;

	with QrDmKhac do
    begin
    	Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
	OpenQueries;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkhac, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmKhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.QrDmkhacAfterInsert(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
        FieldByName('LOAI').AsString := QrDmkhacX.FieldByName('LOAI').AsString;
        FieldByName('MA').Clear;
        FieldByName('DGIAI').Clear;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.QrDmkhacBeforePost(DataSet: TDataSet);
begin
	with QrDmKhac do
    begin
        if fixCode then
    		if LengthConfirm(QrDmKhac, ['MA']) then
        		Abort;
		if BlankConfirm(QrDmKhac, ['DGIAI']) then
    		Abort;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.QrDmkhacBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsDmkhac);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.QrDmkhacBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if (Field.FullName = 'MA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDMKHAC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CbKhacNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.CmdAdminExecute(Sender: TObject);
begin
	if DsDmkhacx.AutoEdit then
    	Exit;
    DsDmkhacx.AutoEdit := FixLogon;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.TntDBMemo1Exit(Sender: TObject);
begin
	QrDmkhacx.CheckBrowseMode
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac3.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

end.
