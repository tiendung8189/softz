object FrmChuyenDvt: TFrmChuyenDvt
  Left = 104
  Top = 195
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Chuy'#7875'n '#272#7893'i '#272#417'n V'#7883' T'#237'nh'
  ClientHeight = 573
  ClientWidth = 853
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    853
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 853
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 853
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 66
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 66
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 74
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 140
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton11: TToolButton
      Left = 206
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 214
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 280
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 288
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object SepChecked: TToolButton
      Left = 354
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 362
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton2: TToolButton
      Left = 428
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 436
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 853
    Height = 533
    Cursor = 1
    ActivePage = TabSheet2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 845
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 845
        Height = 434
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'18'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
          'LK_TENKHO'#9'25'#9'T'#234'n'#9'F'#9'Kho h'#224'ng'
          'Tu_ThanhTien'#9'12'#9'Tr'#7883' gi'#225' xu'#7845't'#9'F'
          'ThanhTien'#9'12'#9'Tr'#7883' gi'#225' nh'#7853'p'#9'F'
          'GhiChu'#9'40'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 845
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 845
        inherited Panel1: TPanel
          Width = 845
          ParentColor = False
          ExplicitWidth = 845
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 845
        Height = 116
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 287
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 44
          Top = 62
          Width = 61
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i giao'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 293
          Top = 62
          Width = 65
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#432#7901'i nh'#7853'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object EdSCT: TwwDBEdit
          Left = 344
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdNGUOIGIAO: TwwDBEdit
          Left = 112
          Top = 58
          Width = 165
          Height = 22
          Ctl3D = False
          DataField = 'NguoiGiao'
          DataSource = DsNX
          ParentCtl3D = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 365
          Top = 58
          Width = 165
          Height = 22
          Ctl3D = False
          DataField = 'NguoiNhan'
          DataSource = DsNX
          ParentCtl3D = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdMaKhoXuat: TDBEditEh
          Left = 452
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'TU_MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object CbKhoXuat: TDbLookupComboboxEh2
          Left = 112
          Top = 34
          Width = 338
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 335
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
          OnCloseUp = CbKhoXuatCloseUp
          OnDropDown = CbKhoXuatDropDown
        end
        object DBMemoEh1: TDBMemoEh
          Left = 112
          Top = 82
          Width = 725
          Height = 22
          ControlLabel.Width = 49
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'GhiChu'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 6
          Visible = True
          WantReturns = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 292
        Width = 845
        Height = 212
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: H'#224'ng nh'#7853'p'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        ExplicitHeight = 351
        object GrDetail: TwwDBGrid2
          Tag = 1
          Left = 0
          Top = 16
          Width = 845
          Height = 196
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'MAVT'#9'14'#9'M'#227' barcode'#9'F'#9'H'#224'ng ho'#225
            'LK_MaNoiBo'#9'14'#9'M'#227' n'#7897'i b'#7897#9'T'#9'H'#224'ng ho'#225
            'LK_Tenvt'#9'35'#9'T'#234'n h'#224'ng'#9'T'#9'H'#224'ng ho'#225
            'LK_Dvt'#9'8'#9#272'VT'#9'T'#9'H'#224'ng ho'#225
            'DonGiaThamKhao'#9'11'#9'Danh m'#7909'c'#9'T'#9'Gi'#225' v'#7889'n'
            'DonGia'#9'11'#9'Nh'#7853'p h'#224'ng'#9'F'#9#272#417'n gi'#225
            'SoLuong'#9'6'#9'S'#7889' l'#432#7907'ng'#9'F'
            'ThanhTien'#9'13'#9'Nh'#7853'p h'#224'ng'#9'T'#9'Tr'#7883' gi'#225' v'#7889'n'
            'GhiChu'#9'40'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
          ExplicitHeight = 335
        end
      end
      object PaXuat: TisPanel
        Left = 0
        Top = 116
        Width = 845
        Height = 176
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        HeaderCaption = ' :: H'#224'ng xu'#7845't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrXuat: TwwDBGrid2
          Tag = 2
          Left = 0
          Top = 16
          Width = 845
          Height = 160
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'MAVT'#9'14'#9'M'#227' barcode'#9'F'#9'H'#224'ng ho'#225
            'LK_MaNoiBo'#9'14'#9'M'#227' n'#7897'i b'#7897#9'T'#9'H'#224'ng ho'#225
            'LK_Tenvt'#9'35'#9'T'#234'n h'#224'ng'#9'T'#9'H'#224'ng ho'#225
            'LK_Dvt'#9'8'#9#272'VT'#9'T'#9'H'#224'ng ho'#225
            'DonGiaThamKhao'#9'11'#9'Danh m'#7909'c'#9'T'#9'Gi'#225' v'#7889'n'
            'DonGia'#9'11'#9'Xu'#7845't h'#224'ng'#9'F'#9#272#417'n gi'#225
            'SoLuong'#9'7'#9'S'#7889' l'#432#7907'ng'#9'F'
            'ThanhTien'#9'13'#9'Xu'#7845't h'#224'ng'#9'T'#9'Tr'#7883' gi'#225' v'#7889'n'
            'GhiChu'#9'40'#9'Ghi ch'#250#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTu_CT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowFooter, dgShowCellHint]
          ParentFont = False
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrXuatUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 711
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 711
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 134
    Top = 312
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = '  K'#7871't th'#250'c  '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Caption = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'Ch'#7885'n theo m'#7863't h'#224'ng...'
      ImageIndex = 17
      OnExecute = CmdFilterComExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
      Visible = False
      OnExecute = CmdChecktonExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      OnExecute = CmdPrintExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'TU_MAKHO'
      'MAKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 106
    Top = 312
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    BeforeInsert = QrNXBeforeInsert
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DIEUKHO'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'LOC = :LOC')
    Left = 554
    Top = 374
    object QrNXXOA: TWideStringField
      Alignment = taCenter
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      DisplayWidth = 10
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXTU_MAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho xu'#7845't'
      DisplayWidth = 5
      FieldName = 'TU_MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TU_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho xu'#7845't'
      FieldKind = fkLookup
      FieldName = 'LK_TU_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'TU_MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'Kho h'#224'ng'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      DisplayWidth = 47
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 10
      FieldName = 'MADT'
      Visible = False
      FixedChar = True
      Size = 15
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      DisplayWidth = 15
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrNXNguoiGiao: TWideStringField
      FieldName = 'NguoiGiao'
      Size = 30
    end
    object QrNXNguoiNhan: TWideStringField
      FieldName = 'NguoiNhan'
      Size = 30
    end
    object QrNXThanhTien: TFloatField
      FieldName = 'ThanhTien'
    end
    object QrNXThanhTienSi: TFloatField
      FieldName = 'ThanhTienSi'
    end
    object QrNXThanhTienLe: TFloatField
      FieldName = 'ThanhTienLe'
    end
    object QrNXGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrNXThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrNXSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrNXTu_SoLuong: TFloatField
      FieldName = 'Tu_SoLuong'
    end
    object QrNXTu_ThanhTien: TFloatField
      FieldName = 'Tu_ThanhTien'
    end
    object QrNXTu_ThanhTienSi: TFloatField
      FieldName = 'Tu_ThanhTienSi'
    end
    object QrNXTu_ThanhTienLe: TFloatField
      FieldName = 'Tu_ThanhTienLe'
    end
    object QrNXTu_ThanhToan: TFloatField
      FieldName = 'Tu_ThanhToan'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DIEUKHO_CT'
      ' where'#9'isnull(NhapXuat, '#39#39') = '#39'N'#39' and KHOA =:KHOA'
      'order by STT')
    Left = 582
    Top = 374
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      FixedChar = True
      Size = 15
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTLK_BO: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_BO'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'BO'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTCALC_DonGiaThamKhaoHop: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_DonGiaThamKhaoHop'
      Calculated = True
    end
    object QrCTDonGiaThamKhao: TFloatField
      FieldName = 'DonGiaThamKhao'
    end
    object QrCTDonGiaHop: TFloatField
      FieldName = 'DonGiaHop'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSoLuongHop: TFloatField
      FieldName = 'SoLuongHop'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSoLuongLe: TFloatField
      FieldName = 'SoLuongLe'
    end
    object QrCTThanhTien: TFloatField
      FieldName = 'ThanhTien'
      OnChange = QrCTThanhTienChange
    end
    object QrCTDonGiaSi: TFloatField
      FieldName = 'DonGiaSi'
    end
    object QrCTThanhTienSi: TFloatField
      FieldName = 'ThanhTienSi'
    end
    object QrCTDonGiaLe: TFloatField
      FieldName = 'DonGiaLe'
    end
    object QrCTThanhTienLe: TFloatField
      FieldName = 'ThanhTienLe'
    end
    object QrCTHanSuDung: TDateTimeField
      FieldName = 'HanSuDung'
    end
    object QrCTDonGia: TFloatField
      FieldName = 'DonGia'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSoLuong: TFloatField
      FieldName = 'SoLuong'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTLK_Dvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Dvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'Dvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DvtHop: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DvtHop'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DvtHop'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_Tenvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tenvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTGhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrCTLK_MaNoiBo: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNoiBo'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNoiBo'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTThanhToan: TFloatField
      FieldName = 'ThanhToan'
      OnChange = QrCTThanhToanChange
    end
    object QrCTNhapXuat: TWideStringField
      FieldName = 'NhapXuat'
      Size = 5
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 554
    Top = 402
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 582
    Top = 402
  end
  object PopIn: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 232
    Top = 304
    object heogibn1: TMenuItem
      Caption = 'Theo gi'#225' b'#225'n'
      OnClick = CmdPrintExecute
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Phiuvnchuynnib1: TMenuItem
      Tag = 1
      Caption = 'Theo gi'#225' b'#225'n l'#7867
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      OnClick = CmdPrintExecute
    end
    object N2: TMenuItem
      Caption = '-'
      Visible = False
    end
    object Phiucginhp1: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'Theo gi'#225' v'#7889'n'
      Hint = 'In phi'#7871'u'
      Visible = False
      OnClick = CmdPrintExecute
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object Khngngi1: TMenuItem
      Tag = 3
      Caption = 'Kh'#244'ng '#273#417'n gi'#225
      OnClick = CmdPrintExecute
    end
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 324
    Top = 468
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Lctheomthng1: TMenuItem
      Action = CmdFilterCom
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 324
    Top = 304
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SoLuong'
      'ThanhTienSi'
      'ThanhTienLe'
      'ThanhTien'
      'ThanhToan')
    DetailFields.Strings = (
      'SoLuong'
      'ThanhTienSi'
      'ThanhTienLe'
      'ThanhTien'
      'ThanhToan')
    Left = 356
    Top = 304
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 176
    Top = 304
    object Kimtraslngtnkho1: TMenuItem
      Action = CmdCheckton
    end
  end
  object spCHUNGTU_Select_Full: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spCHUNGTU_Select_Full;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 678
    Top = 340
  end
  object QrTu_CT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrTu_CTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrTu_CTAfterEdit
    BeforePost = QrTu_CTBeforePost
    AfterCancel = QrTu_CTAfterCancel
    BeforeDelete = QrTu_CTBeforeDelete
    AfterDelete = QrTu_CTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'DIEUKHO_CT'
      ' where'#9'isnull(NhapXuat, '#39#39') = '#39'X'#39' and KHOA =:KHOA'
      'order by STT')
    Left = 630
    Top = 278
    object IntegerField1: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object WideStringField1: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = WideStringField1Change
      FixedChar = True
      Size = 15
    end
    object IntegerField2: TIntegerField
      FieldName = 'QD1'
    end
    object IntegerField3: TIntegerField
      FieldName = 'STT'
    end
    object BooleanField1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object BooleanField2: TBooleanField
      FieldKind = fkLookup
      FieldName = 'LK_BO'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'BO'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object GuidField1: TGuidField
      FieldName = 'KHOACT'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object GuidField2: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object WideStringField2: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object FloatField1: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_DonGiaThamKhaoHop'
      Calculated = True
    end
    object FloatField2: TFloatField
      FieldName = 'DonGiaThamKhao'
    end
    object FloatField3: TFloatField
      FieldName = 'DonGiaHop'
      OnChange = FloatField3Change
    end
    object FloatField4: TFloatField
      FieldName = 'SoLuongHop'
      OnChange = FloatField12Change
      OnValidate = QrCTSOLUONGValidate
    end
    object FloatField5: TFloatField
      FieldName = 'SoLuongLe'
    end
    object FloatField6: TFloatField
      FieldName = 'ThanhTien'
      OnChange = FloatField6Change
    end
    object FloatField7: TFloatField
      FieldName = 'DonGiaSi'
    end
    object FloatField8: TFloatField
      FieldName = 'ThanhTienSi'
    end
    object FloatField9: TFloatField
      FieldName = 'DonGiaLe'
    end
    object FloatField10: TFloatField
      FieldName = 'ThanhTienLe'
    end
    object DateTimeField1: TDateTimeField
      FieldName = 'HanSuDung'
    end
    object FloatField11: TFloatField
      FieldName = 'DonGia'
      OnChange = FloatField3Change
    end
    object FloatField12: TFloatField
      FieldName = 'SoLuong'
      OnChange = FloatField12Change
      OnValidate = QrCTSOLUONGValidate
    end
    object WideStringField3: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Dvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'Dvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object WideStringField4: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DvtHop'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DvtHop'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object WideStringField5: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tenvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object WideStringField6: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object WideStringField7: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNoiBo'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNoiBo'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object FloatField13: TFloatField
      FieldName = 'ThanhToan'
      OnChange = FloatField13Change
    end
    object QrTu_CTNhapXuat: TWideStringField
      FieldName = 'NhapXuat'
      Size = 5
    end
  end
  object DsTu_CT: TDataSource
    DataSet = QrTu_CT
    Left = 630
    Top = 306
  end
  object vlTotal_Tu: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrTu_CT
    MasterFields.Strings = (
      'Tu_SoLuong'
      'Tu_ThanhTienSi'
      'Tu_ThanhTienLe'
      'Tu_ThanhTien'
      'Tu_ThanhToan')
    DetailFields.Strings = (
      'SoLuong'
      'ThanhTienSi'
      'ThanhTienLe'
      'ThanhTien'
      'ThanhToan')
    Left = 580
    Top = 280
  end
end
