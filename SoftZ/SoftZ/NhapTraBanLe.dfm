object FrmNhapTraBanLe: TFrmNhapTraBanLe
  Left = 308
  Top = 68
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Nh'#7853'p Tr'#7843' B'#225'n L'#7867' - H'#243'a '#272#417'n'
  ClientHeight = 573
  ClientWidth = 1104
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1104
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 1104
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1104
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolMain'
    Color = 16119285
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentColor = False
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn2: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
    end
    object SepChecked: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object SepBarcode: TToolButton
      Left = 356
      Top = 0
      Width = 8
      Caption = 'SepBarcode'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 364
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 1104
    Height = 533
    Cursor = 1
    ActivePage = TabSheet2
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 483
        Width = 1096
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1096
        Height = 434
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Thanh to'#225'n'#9'F'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frNGAY1: TfrNGAY
        Left = 0
        Top = 0
        Width = 1096
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        ExplicitWidth = 1096
        inherited Panel1: TPanel
          Width = 1096
          ExplicitWidth = 1096
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaTotal: TPanel
        Left = 0
        Top = 457
        Width = 1096
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        object PaThanhtoan: TPanel
          Left = 977
          Top = 2
          Width = 117
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          DesignSize = (
            117
            43)
          object EdTriGiaTT: TDBNumberEditEh
            Left = 2
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 100
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhToan'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
        end
        object PaSotien1: TPanel
          Left = 521
          Top = 2
          Width = 214
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            214
            43)
          object DBNumberEditEh3: TDBNumberEditEh
            Left = 2
            Top = 17
            Width = 101
            Height = 22
            TabStop = False
            ControlLabel.Width = 44
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Ti'#7873'n C.K'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTienCK'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBNumberEditEh4: TDBNumberEditEh
            Left = 105
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 81
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' sau C.K '
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhTienSauCK'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaSotien: TPanel
          Left = 2
          Top = 2
          Width = 519
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            519
            43)
          object DBNumberEditEh1: TDBNumberEditEh
            Left = 319
            Top = 17
            Width = 89
            Height = 22
            TabStop = False
            ControlLabel.Width = 79
            ControlLabel.Height = 13
            ControlLabel.Caption = 'T'#7893'ng s'#7889' l'#432#7907'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoLuong'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBNumberEditEh2: TDBNumberEditEh
            Left = 410
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 66
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhTien'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaThue: TPanel
          Left = 735
          Top = 2
          Width = 242
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          DesignSize = (
            242
            43)
          object DBNumberEditEh7: TDBNumberEditEh
            Left = 4
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 98
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' tr'#432#7899'c thu'#7871
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhToanChuaThue'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object EdTienVAT: TDBNumberEditEh
            Left = 115
            Top = 17
            Width = 127
            Height = 22
            TabStop = False
            ControlLabel.Width = 53
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Ti'#7873'n thu'#7871
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTienThue'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <
              item
                Action = CmdThue
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 1096
        Height = 115
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object Label7: TLabel
          Left = 549
          Top = 38
          Width = 102
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y phi'#7871'u b'#225'n l'#7867
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object CbKhoHang: TDbLookupComboboxEh2
          Left = 112
          Top = 34
          Width = 338
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 2
          Visible = True
        end
        object EdMaKho: TDBEditEh
          Left = 452
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 3
          Visible = True
        end
        object CbbThuNgan: TDbLookupComboboxEh2
          Left = 112
          Top = 58
          Width = 165
          Height = 22
          ControlLabel.Width = 74
          ControlLabel.Height = 16
          ControlLabel.Caption = 'N.V thu ng'#226'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'NhanVienThuNgan'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'FullName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'UserName'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsUser
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'UID'
          ListField = 'FullName'
          ListSource = DataMain.DsUser
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 4
          Visible = True
        end
        object EdSoPhieu: TDBEditEh
          Left = 344
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15794175
          ControlLabel.Width = 50
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' phi'#7871'u'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object DBMemoEh1: TDBMemoEh
          Left = 112
          Top = 82
          Width = 417
          Height = 22
          ControlLabel.Width = 49
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'GhiChu'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 5
          Visible = True
          WantReturns = True
        end
        object wwDBDateTimePicker1: TwwDBDateTimePicker
          Left = 656
          Top = 34
          Width = 165
          Height = 22
          TabStop = False
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'Tahoma'
          CalendarAttributes.Font.Style = []
          Color = clBtnFace
          DataField = 'NgayPhieuGoc'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.FocusBorders = [efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlue
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ReadOnly = True
          ShowButton = False
          TabOrder = 6
        end
        object EdSctPhieuGoc: TDBEditEh
          Left = 656
          Top = 10
          Width = 165
          Height = 22
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabel.Width = 70
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Phi'#7871'u b'#225'n l'#7867
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'SctPhieuGoc'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <
            item
              Action = CmdInsertPhieuGoc
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Style = ebsGlyphEh
            end
            item
              Action = CmdRemovePhieuGoc
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 1
              Images.HotIndex = 1
              Images.PressedIndex = 1
              Images.DisabledIndex = 1
              Style = ebsGlyphEh
            end
            item
              Action = CmdUpdateDetail
              Images.NormalImages = DataMain.ImageEditButton
              Images.HotImages = DataMain.ImageEditButton
              Images.PressedImages = DataMain.ImageEditButton
              Images.DisabledImages = DataMain.ImageEditButton
              Images.NormalIndex = 3
              Images.HotIndex = 3
              Images.PressedIndex = 3
              Images.DisabledIndex = 3
              Style = ebsGlyphEh
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 115
        Width = 1096
        Height = 342
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 1096
          Height = 326
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False'
            'LK_TENTHUE;CustomEdit;CbLOAITHUE;F')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'SOLUONG'#9'10'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
            'DONGIA'#9'10'#9'SKU'#9'F'#9#272#417'n gi'#225
            'LK_TENTHUE'#9'25'#9'Lo'#7841'i'#9'T'#9'Thu'#7871' VAT'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
        object CbLOAITHUE: TwwDBLookupCombo
          Left = 444
          Top = 120
          Width = 117
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENLT'#9'0'#9'TENLT'#9'F')
          DataField = 'LOAITHUE'
          DataSource = DsCT
          LookupTable = DataMain.QrDMLOAITHUE
          LookupField = 'MALT'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMAKHONotInList
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 962
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 962
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 354
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Hint = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'Export ra Excel t'#7915' l'#432#7899'i d'#7919' li'#7879'u'
      OnExecute = CmdExportDataGridExecute
    end
    object CmdRecalc: TAction
      Caption = 'CmdRecalc'
      OnExecute = CmdRecalcExecute
    end
    object CmdThue: TAction
      Caption = ' '
      OnExecute = CmdThueExecute
    end
    object CmdInsertPhieuGoc: TAction
      Caption = 'Ch'#7885'n phi'#7871'u b'#225'n l'#7867
      Hint = 'Ch'#7885'n phi'#7871'u b'#225'n l'#7867
      OnExecute = CmdInsertPhieuGocExecute
    end
    object CmdRemovePhieuGoc: TAction
      Caption = 'X'#243'a phi'#7871'u b'#225'n l'#7867
      Hint = 'X'#243'a phi'#7871'u b'#225'n l'#7867
      OnExecute = CmdRemovePhieuGocExecute
    end
    object CmdUpdateDetail: TAction
      Caption = 'C'#7853'p nh'#7853't chi ti'#7871't m'#7863't h'#224'ng'
      Hint = 'C'#7853'p nh'#7853't chi ti'#7871't m'#7863't h'#224'ng'
      OnExecute = CmdUpdateDetailExecute
    end
    object CmdUpdateQty: TAction
      Caption = 'C'#7853'p nh'#7853't s'#7889' l'#432#7907'ng tr'#7843' h'#224'ng'
      Hint = 'C'#7853'p nh'#7853't s'#7889' l'#432#7907'ng tr'#7843' h'#224'ng'
      OnExecute = CmdUpdateQtyExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 124
    Top = 354
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterPost = QrNXAfterPost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'TRAHANG'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 724
    Top = 372
    object QrNXXOA: TWideStringField
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrNXSoTienCK: TFloatField
      FieldName = 'SoTienCK'
    end
    object QrNXThanhTienSauCK: TFloatField
      FieldName = 'ThanhTienSauCK'
    end
    object QrNXLoaiThue: TWideStringField
      FieldName = 'LoaiThue'
    end
    object QrNXThueSuat: TFloatField
      FieldName = 'ThueSuat'
    end
    object QrNXThanhToanChuaCL: TFloatField
      FieldName = 'ThanhToanChuaCL'
    end
    object QrNXSoTienCL: TFloatField
      FieldName = 'SoTienCL'
    end
    object QrNXThanhToanChuaThue: TFloatField
      FieldName = 'ThanhToanChuaThue'
    end
    object QrNXSoTienThue: TFloatField
      FieldName = 'SoTienThue'
    end
    object QrNXSoTienThue_5: TFloatField
      FieldName = 'SoTienThue_5'
    end
    object QrNXSoTienThue_10: TFloatField
      FieldName = 'SoTienThue_10'
    end
    object QrNXSoTienThue_Khac: TFloatField
      FieldName = 'SoTienThue_Khac'
    end
    object QrNXGhiChu: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrNXHinhThuc_Gia: TWideStringField
      FieldName = 'HinhThuc_Gia'
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXThanhTien: TFloatField
      FieldName = 'ThanhTien'
    end
    object QrNXThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrNXSoTienThueChuaRound: TFloatField
      FieldName = 'SoTienThueChuaRound'
      OnChange = QrNXSoTienThueChuaRoundChange
    end
    object QrNXThanhToanTinhThue: TFloatField
      FieldName = 'ThanhToanTinhThue'
    end
    object QrNXSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrNXMaQuay: TWideStringField
      FieldName = 'MaQuay'
    end
    object QrNXCa: TWideStringField
      FieldName = 'Ca'
    end
    object QrNXKhoaPhieuGoc: TGuidField
      FieldName = 'KhoaPhieuGoc'
      FixedChar = True
      Size = 38
    end
    object QrNXSctPhieuGoc: TWideStringField
      FieldName = 'SctPhieuGoc'
      Size = 50
    end
    object QrNXNgayPhieuGoc: TDateTimeField
      FieldName = 'NgayPhieuGoc'
    end
    object QrNXNhanVienThuNgan: TIntegerField
      FieldName = 'NhanVienThuNgan'
    end
    object QrNXMaVIP: TWideStringField
      FieldName = 'MaVIP'
    end
    object QrNXLK_UserNameNhanVienThuNgan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_UserNameNhanVienThuNgan'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'UserName'
      KeyFields = 'NhanVienThuNgan'
      Lookup = True
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
    end
    object QrNXLK_TenNhanVienThuNgan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenNhanVienThuNgan'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'UID'
      LookupResultField = 'FullName'
      KeyFields = 'NhanVienThuNgan'
      Size = 200
      Lookup = True
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from TRAHANG_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 756
    Top = 372
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      FixedChar = True
      Size = 15
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTTyLeCK: TFloatField
      FieldName = 'TyLeCK'
      OnChange = QrCTThanhTienChange
    end
    object QrCTSoTienCK: TFloatField
      FieldName = 'SoTienCK'
      OnChange = QrCTThanhTienChange
    end
    object QrCTThanhTienSauCK: TFloatField
      FieldName = 'ThanhTienSauCK'
      OnChange = QrCTThanhTienSauCKChange
    end
    object QrCTThanhToanChuaCL: TFloatField
      FieldName = 'ThanhToanChuaCL'
    end
    object QrCTSoTienCL: TFloatField
      FieldName = 'SoTienCL'
      OnChange = QrCTThanhTienChange
    end
    object QrCTThanhToanChuaThue: TFloatField
      FieldName = 'ThanhToanChuaThue'
    end
    object QrCTThueSuat: TFloatField
      FieldName = 'ThueSuat'
      OnChange = QrCTThanhTienSauCKChange
    end
    object QrCTSoTienThue: TFloatField
      FieldName = 'SoTienThue'
    end
    object QrCTSoTienThue_5: TFloatField
      FieldName = 'SoTienThue_5'
    end
    object QrCTSoTienThue_10: TFloatField
      FieldName = 'SoTienThue_10'
    end
    object QrCTSoTienThue_Khac: TFloatField
      FieldName = 'SoTienThue_Khac'
    end
    object QrCTThanhToan: TFloatField
      FieldName = 'ThanhToan'
      OnChange = QrCTThanhToanChange
    end
    object QrCTMaBo: TWideStringField
      FieldName = 'MaBo'
      Size = 15
    end
    object QrCTDonGia: TFloatField
      FieldName = 'DonGia'
    end
    object QrCTSoLuong: TFloatField
      FieldName = 'SoLuong'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTThanhTien: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      FieldName = 'ThanhTien'
      OnChange = QrCTThanhTienChange
    end
    object QrCTLoaiThue: TWideStringField
      FieldName = 'LoaiThue'
    end
    object QrCTLK_TenThue2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenThue'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'TENLT'
      KeyFields = 'LoaiThue'
      Lookup = True
    end
    object QrCTGhiChu: TWideStringField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrCTDonGiaChuaThue: TFloatField
      FieldName = 'DonGiaChuaThue'
    end
    object QrCTSoTienThueChuaRound: TFloatField
      FieldName = 'SoTienThueChuaRound'
    end
    object QrCTThanhToanTinhThue: TFloatField
      FieldName = 'ThanhToanTinhThue'
      OnChange = QrCTThanhToanTinhThueChange
    end
    object QrCTDonGiaTinhThue: TFloatField
      FieldName = 'DonGiaTinhThue'
    end
    object QrCTTenVT: TWideStringField
      FieldName = 'TenVT'
      Size = 200
    end
    object QrCTDvt: TWideStringField
      FieldName = 'Dvt'
    end
    object QrCTKhoactPhieuGoc: TGuidField
      FieldName = 'KhoactPhieuGoc'
      FixedChar = True
      Size = 38
    end
    object QrCTSoLuongPhieuGoc: TFloatField
      FieldName = 'SoLuongPhieuGoc'
    end
    object QrCTDonGiaCK: TFloatField
      FieldName = 'DonGiaCK'
    end
    object QrCTMaNoiBo: TWideStringField
      FieldName = 'MaNoiBo'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 724
    Top = 424
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 756
    Top = 424
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 290
    Top = 312
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object ExportraExceltlidliu2: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 354
    Top = 312
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 382
    Top = 428
    object MenuItem1: TMenuItem
      Action = CmdInsertPhieuGoc
    end
    object Cpnhtslngtrhng1: TMenuItem
      Action = CmdUpdateQty
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ExportraExceltlidliu1: TMenuItem
      Tag = 1
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SoLuong'
      'ThanhTien'
      'SoTienCK'
      'ThanhTienSauCK'
      'ThanhToanChuaCL'
      'SoTienCL'
      'ThanhToanChuaThue'
      'SoTienThueChuaRound'
      'SoTienThue_5'
      'SoTienThue_10'
      'SoTienThue_Khac'
      'ThanhToan')
    DetailFields.Strings = (
      'SoLuong'
      'ThanhTien'
      'SoTienCK'
      'ThanhTienSauCK'
      'ThanhToanChuaCL'
      'SoTienCL'
      'ThanhToanChuaThue'
      'SoTienThueChuaRound'
      'SoTienThue_5'
      'SoTienThue_10'
      'SoTienThue_Khac'
      'ThanhToan')
    Left = 418
    Top = 312
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 176
    Top = 408
    object MenuItem3: TMenuItem
      Tag = 1
      Caption = 'H'#224'ng h'#243'a'
      ImageIndex = 42
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Tag = 2
      AutoHotkeys = maManual
      AutoLineReduction = maManual
      Caption = 'Kh'#225'ch h'#224'ng'
    end
  end
  object spBANLE_Select_Full: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spBANLE_Select_Full;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 280
    Top = 399
  end
end
