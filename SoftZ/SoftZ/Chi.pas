﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Chi;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook, System.Variants,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2, AdvMenus,
  AppEvnts, isPanel, wwfltdlg, frameNgay, frameNavi, wwDialog, Grids, Wwdbigrd,
  Wwdbgrid, ToolWin, Buttons, AdvEdit, DBAdvEd, DBCtrlsEh, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmChi = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCMADT: TWideStringField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    QrTCLK_TENDT: TWideStringField;
    QrTCLK_LYDO: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    Bevel1: TBevel;
    PaMaster: TisPanel;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    QrTCLCT: TWideStringField;
    CmdAudit: TAction;
    DBText1: TDBText;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrTCMATK: TWideStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    PaTK: TPanel;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    QrTCMALOC: TWideStringField;
    QrTCLK_TENLOC: TWideStringField;
    PaMALOC: TPanel;
    PaMADT: TPanel;
    N2: TMenuItem;
    ItemObsolete: TMenuItem;
    QrTCSCT2: TWideStringField;
    QrTCGhiChu: TWideMemoField;
    DBMemoEh1: TDBMemoEh;
    DBEditEh4: TDBEditEh;
    EdNumSoTien: TDBNumberEditEh;
    QrTCNguoi: TWideStringField;
    QrTCSoTien: TFloatField;
    EdSoPhieu: TDBEditEh;
    CbHinhThuc: TDbLookupComboboxEh2;
    CbbLyDo: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    CbbMaLoc: TDbLookupComboboxEh2;
    EdMaLoc: TDBEditEh;
    CbTenTK: TDbLookupComboboxEh2;
    EdSTK: TDBEditEh;
    EdChinhanh: TDBEditEh;
    EdNganhang: TDBEditEh;
    CmdXemCongNo: TAction;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMaDT: TDBEditEh;
    QrTCThanhToan: TFloatField;
    QrTCLyDo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure BtnInClick(Sender: TObject);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure CmdXemCongNoExecute(Sender: TObject);
    procedure QrTCMATKChange(Sender: TField);
  private
  	mMakho, mLCT, mPTTT: String;
	mCanEdit, mFuncTK, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;
  public
	procedure Execute (r : WORD);
  end;

var
  FrmChi: TFrmChi;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isLib, isCommon,
  CongnoNCC;

{$R *.DFM}

const
    FORM_CODE: String = 'PHIEU_CHI';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsTC.AutoEdit := mCanEdit;

    mLCT := 'CHI';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    mFuncTK := sysFuncTK;
	mMakho := RegReadString(Name, 'Makho', sysDefKho);
    mPTTT := RegReadString(Name, 'Pttt', sysPTTT);

    mObsolete := False;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.FormShow(Sender: TObject);
begin
	with DataMain do
		OpenDataSets([QrDM_KH_NCC, QrPTTT, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);

    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsDrc then
    begin
        PaMALOC.Visible := False;
        grRemoveFields(GrBrowse, 'MALOC;LK_TENLOC', ';');
    end;

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
        grRemoveFields(GrBrowse, 'MATK;LK_TENTK;LK_NGANHANG', ';');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Pttt'], [mMakho, mPTTT]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
   	    CbNgay.SetFocus
	else
    	GrBrowse.SetFocus;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    with DataMain do
    begin
		QrDMKHO.Requery;
        QrPTTT.Requery;
        QrDM_KH_NCC.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdNewExecute(Sender: TObject);
begin
	if not (QrTC.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
			QrTC.Cancel;

	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
        exSearch(Name, DsTC);
end;

procedure TFrmChi.CmdXemCongNoExecute(Sender: TObject);
begin
    if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrTC do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdClearFilterExecute(Sender: TObject);
begin
    with  filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdPrint.Enabled := not bEmpty;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := d;
		FieldByName('LCT').AsString    := mLCT;
        FieldByName('MAKHO').AsString  := mMakho;
        FieldByName('LOC').AsString    := sysLoc;
        FieldByName('MALOC').AsString  := sysLoc;
        FieldByName('PTTT').AsString   := mPTTT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
		Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCBeforePost(DataSet: TDataSet);
var
    pPTTT: string;
begin
	with QrTC do
    begin


        if BlankConfirm(QrTC, ['NGAY', 'PTTT']) then
	    	Abort;

        if BlankConfirm(QrTC, ['MALOC']) then
        begin
            CbbMaLoc.SetFocus;
            Abort;
        end;

        if BlankConfirm(QrTC, ['MADT']) then
        begin
            CbbNhaCungCap.SetFocus;
            Abort;
        end;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

        if BlankConfirm(QrTC, [ 'SoTien']) then
	    	Abort;

        if FieldByName('PTTT').AsString = '02' then
            pPTTT := 'CHICK'
        else if FieldByName('PTTT').AsString = '01' then
            pPTTT := 'CHITM'
        else
            pPTTT := '';

	    exValidClosing(FieldByName('NGAY').AsDateTime, 2);
        SetNull(QrTC, ['MAKHO']);
    end;
	DataMain.AllocSCT(mLCT, QrTC);
    DataMain.AllocSCT2(pPTTT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCMATKChange(Sender: TField);
begin
    EdChinhanh.Text := EdChinhanh.Field.AsString;
    EdNganhang.Text := EdNganhang.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCNGAYValidate(Sender: TField);
begin
    with QrTC do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('ThanhToan').AsFloat := FieldByName('SoTien').AsFloat
end;

(*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.QrTCAfterPost(DataSet: TDataSet);
begin
	with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
        mPTTT := FieldByName('PTTT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

end.
