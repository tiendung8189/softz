﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Scan;

interface

uses
  SysUtils, Classes, Controls, Forms, Dialogs, ExtDlgs, Menus, ActnList, Db,
  ADODB, ExtCtrls, AdvMenus, ImgList, fcImager, rImageZoom, rDBComponents;

type
  TFrmScan = class(TForm)
    Action: TActionList;
    CmdAssign: TAction;
    CmdClear: TAction;
    OpenDlg: TOpenPictureDialog;
    CmdGray: TAction;
    ImageList1: TImageList;
    QrHINH: TADOQuery;
    DsHINH: TDataSource;
    Gnhnh1: TMenuItem;
    Xahnh1: TMenuItem;
    N1: TMenuItem;
    PopStretch: TMenuItem;
    PaPic: TPanel;
    CmdExport: TAction;
    Export1: TMenuItem;
    N2: TMenuItem;
    SaveDlg: TSaveDialog;
    PopPic: TAdvPopupMenu;
    rDBImage1: TrDBImage;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdAssignExecute(Sender: TObject);
    procedure CmdClearExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdExportExecute(Sender: TObject);
    procedure PopPicPopup(Sender: TObject);
    procedure PopStretchClick(Sender: TObject);
  private
  	isPopStretch: Boolean;
  public
  	procedure Execute(master: TDataSource; ma: String = 'MAVT');
  end;

var
  FrmScan: TFrmScan;

implementation

{$R *.DFM}

uses
	jpeg, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.Execute;
begin
    with QrHINH do
    begin
    	SQL.Text := Format(
			'select MAVT, HINH, EXT' +
			'  from DM_VT_HINH' +
			' where %s = :%s', [ma, ma]);

    	DataSource := master;
		Open;
    end;

    Show;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdAssignExecute(Sender: TObject);
var
    ext: String;
begin
   	FormStyle := fsNormal;
	if not OpenDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
        Exit;
	end;

    ext := UpperCase(ExtractFileExt(OpenDlg.FileName));
//    if ext = '.JPEG' then
//    	ext := '.JPG';

	with QrHINH do
    begin
    	if IsEmpty then
        	Append
		else
	        Edit;

		TBlobField(FieldByName('HINH')).LoadFromFile(OpenDlg.FileName);
        FieldByName('EXT').AsString := ext;
        Post;
	end;

   	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdClearExecute(Sender: TObject);
begin
	QrHINH.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
	if GetClass('TJPEGImage') = nil then
    	RegisterClass(TJpegImage);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	b: Boolean;
begin
	b := QrHINH.IsEmpty;
	CmdClear.Enabled := not b;
	CmdExport.Enabled := not b;
    PopStretch.Enabled := not b;
    CmdAssign.Enabled := not (QrHINH.DataSource.DataSet.State in [dsInsert]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    QrHINH.Close;
    finally
    end;
    FrmScan := Nil;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.CmdExportExecute(Sender: TObject);
begin
    FormStyle := fsNormal;
	if not SaveDlg.Execute then
    begin
    	FormStyle := fsStayOnTop;
    	Exit;
    end;

    with QrHINH do
    begin
	    if FieldByName('HINH').IsNull then
        begin
	    	FormStyle := fsStayOnTop;
    		Exit;
        end;

	    TBlobField(FieldByName('HINH')).SaveToFile(
        	ChangeFileExt(SaveDlg.FileName, FieldByName('EXT').AsString));
    end;
	FormStyle := fsStayOnTop;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.PopPicPopup(Sender: TObject);
begin
    PopStretch.Checked := isPopStretch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmScan.PopStretchClick(Sender: TObject);
begin
    isPopStretch := not isPopStretch;
    rDBImage1.Stretch := isPopStretch;
end;
end.
