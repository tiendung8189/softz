﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoadonGTGT;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls, Grids,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, isDb,
  frameNavi,
  isPanel, frameKho, wwDialog, Mask, Wwdbigrd, Wwdbgrid, ToolWin, wwdbedit, System.Variants;

type
  TFrmHoadonGTGT = class(TForm)
    ToolMain: TToolBar;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    ActionList: TActionList;
    CmdPrint: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    QrHD: TADOQuery;
    DsHD: TDataSource;
    DsCT: TDataSource;
    CmdSearch: TAction;
    GrDetail: TwwDBGrid2;
    CmdDetail: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    PopupMenu1: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    Bevel1: TBevel;
    frNavi: TfrNavi;
    QrHDNGAY: TDateTimeField;
    QrHDTENCTY: TWideStringField;
    QrHDMST: TWideStringField;
    QrHDDCHI: TWideStringField;
    QrHDDTHOAI: TWideStringField;
    QrHDFAX: TWideStringField;
    QrHDGHICHU: TWideStringField;
    QrHDCREATE_BY: TIntegerField;
    QrHDCREATE_DATE: TDateTimeField;
    QrHDUPDATE_BY: TIntegerField;
    QrHDUPDATE_DATE: TDateTimeField;
    TntLabel16: TLabel;
    CbNgay: TwwDBDateTimePicker;
    TntLabel10: TLabel;
    EdTENCTY: TwwDBEdit;
    TntLabel11: TLabel;
    EdMST: TwwDBEdit;
    TntLabel12: TLabel;
    EdDCHI: TwwDBEdit;
    TntLabel13: TLabel;
    EdDTHOAI: TwwDBEdit;
    TntLabel14: TLabel;
    DBEdit7: TwwDBEdit;
    TntLabel15: TLabel;
    DBEdit8: TwwDBEdit;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    QrHDXOA: TWideStringField;
    QrHDDELETE_BY: TIntegerField;
    QrHDDELETE_DATE: TDateTimeField;
    QrHDIMG: TIntegerField;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdIns: TAction;
    PopPrint: TAdvPopupMenu;
    TntLabel1: TLabel;
    CbHTTT: TwwDBLookupCombo;
    QrHDPT_THANHTOAN: TWideStringField;
    QrDmQuay: TADOQuery;
    QrHDSCT: TWideStringField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton8: TToolButton;
    QrCT: TADOQuery;
    QrCTDS_NHOMTHUE: TWideStringField;
    DBEdit1: TwwDBEdit;
    QrHDMST1: TWideStringField;
    PD1: TisPanel;
    CmdGetBill: TAction;
    PopDetail: TAdvPopupMenu;
    LydanhschBilltheongy1: TMenuItem;
    GrNHOMTHUE: TwwDBGrid2;
    QrNHOMTHUE: TADOQuery;
    DsNHOMTHUE: TDataSource;
    QrNHOMTHUEDGIAI: TWideStringField;
    QrNHOMTHUESOHDON: TWideStringField;
    QrNHOMTHUESERI: TWideStringField;
    PopNhomthue: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdUpdateGroupTax: TAction;
    GET_BILL_INFO: TADOCommand;
    QrCTNGAY: TDateTimeField;
    QrCTSOTIEN: TFloatField;
    QrCTTHUNGAN: TIntegerField;
    QrCTSCT: TWideStringField;
    QrCTTHUNGAN_DGIAI: TWideStringField;
    QrDmKho: TADOQuery;
    DsKho: TDataSource;
    QrDmQuayQUAY: TWideStringField;
    QrDmQuayMAKHO: TWideStringField;
    QrDmQuayTENMAY: TWideStringField;
    QrDmQuayGHICHU: TWideStringField;
    QrDmKhoMAKHO: TWideStringField;
    QrDmKhoTENKHO: TWideStringField;
    QrDmKhoDIACHI: TWideStringField;
    DS_NHOMTHUE: TADOCommand;
    rctip1: TMenuItem;
    GTGT101: TMenuItem;
    GTGT102: TMenuItem;
    Khngchuthu1: TMenuItem;
    N2: TMenuItem;
    N3: TMenuItem;
    N4: TMenuItem;
    CmdAudit: TAction;
    DBText2: TDBText;
    QrNHOMTHUENHOMTHUE: TWideStringField;
    CmdLisrRefesh: TAction;
    QrHDMAKHO: TWideStringField;
    frKHO1: TfrKHO;
    QrHDKHOA: TGuidField;
    QrCTKHOA: TGuidField;
    QrCTKHOA_CT: TGuidField;
    QrNHOMTHUEKHOACT: TGuidField;
    QrNHOMTHUEKHOA: TGuidField;
    CAPNHAT_HDGTGT_HH: TADOCommand;
    CmdChitietHH: TAction;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    QrHDLOC: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrHDBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDetailExecute(Sender: TObject);
    procedure QrHDCalcFields(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrHDAfterScroll(DataSet: TDataSet);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrHDBeforePost(DataSet: TDataSet);
    procedure QrHDAfterInsert(DataSet: TDataSet);
    procedure TntFormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure CbHTTTNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdDelExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrDetailCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdGetBillExecute(Sender: TObject);
    procedure CmdUpdateGroupTaxExecute(Sender: TObject);
    procedure PD1PullDown(Sender: TObject);
    procedure QrCTKHOA_CTChange(Sender: TField);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdLisrRefeshExecute(Sender: TObject);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure CmdChitietHHExecute(Sender: TObject);
    procedure QrHDBeforeInsert(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
  private
	mCanEdit, mChangeCT: Boolean;
   	fTungay, fDenngay: TDateTime;
    fKho: String;
    fSQL, fStr, mLCT: String;
    defTencty, defMst, defMst1, defDchi, defDthoai, defFax, defHTTT, defGhichu: String;

  public
    function  LayDsNhomthue(pKHOA: TGUID; pNgay: TDateTime; var pDgiai: String): String;
    function  CapnhatHH(pKHOA: TGUID): Boolean;
	procedure Execute(r: WORD);
  end;

var
  FrmHoadonGTGT: TFrmHoadonGTGT;

implementation

uses
	isMsg, ExCommon, MainData, isStr, RepEngine, Rights, isLib, HoadonGTGTGetBill,
    GuidEx, HoadonGTGTCT, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HOADON_GTGT';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsHD.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    mLCT := 'GTGT';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrHD;

    // Initial
    fStr := '';
    fSQL := QrHD.SQL.Text;
  	mTrigger := False;
    mChangeCT := False;

    // Load panel collapse status
    PD1.Collapsed := RegReadBool(Name, PD1.Name);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.FormShow(Sender: TObject);
begin
	OpenDataSets([QrDmKho, QrDmQuay, DataMain.QrPTTT, DataMain.QrDMLOAITHUE, DataMain.QrDMKHO]);
    QrDmKho.Locate('MAKHO', sysDefKho, []);

    frKHO1.Init2(Date - sysLateDay, Date, CmdRefresh);

    SetShortDateFormat(QrHD);
    SetDisplayFormat(QrHD, ['NGAY'], DateTimeFmt);
    SetDisplayFormat(QrCT, sysCurFmt);
    SetShortDateFormat(QrCT);

    // Customize
	SetCustomGrid([FORM_CODE, FORM_CODE + '_NHOMTHUE',FORM_CODE + '_CT'],
        [GrBrowse, GrNHOMTHUE, GrDetail]);
    SetDictionary([QrHD, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.TntFormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrHD, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    // Load panel collapse status
    RegWrite(Name, PD1.Name, PD1.Collapsed);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdRefreshExecute(Sender: TObject);
var
	s, sKho : String;
begin
    if not VarIsNull(frKHO1.cbbKho.Value) then
        sKho := frKHO1.cbbKho.Value
    else
        sKho := '';

   	if (frKHO1.edFrom.Date <> fTungay) or
	   (frKHO1.edTo.Date   <> fDenngay) or
       (sKho <> fKho) then
    begin
		fTungay  := frKHO1.EdFrom.Date;
        fDenngay := frKHO1.EdTo.Date;
        fKho := sKho;

		Screen.Cursor := crSQLWait;
		with QrHD do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
            if fKho <> '' then
            	SQL.Add(' and MAKHO =''' + fKho + '''');

            SQL.Add('order by NGAY desc, SCT desc');
                
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdLisrRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdNewExecute(Sender: TObject);
begin
	QrHD.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
    QrNHOMTHUE.CheckBrowseMode;
	QrHD.Post;
    QrCT.UpdateBatch;
    QrNHOMTHUE.UpdateBatch;

    if mChangeCT then
        if not CapnhatHH(TGuidField(QrHD.FieldByName('KHOA')).AsGuid) then
            ErrMsg('Cập nhật chi tiết mặt hàng cho HĐ GTGT có lỗi, vui lòng kiểm tra lại.');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdCancelExecute(Sender: TObject);
begin
    QrNHOMTHUE.CancelBatch;
	QrCT.CancelBatch;
	QrHD.Cancel;

    mChangeCT := False;

    if QrHD.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdChitietHHExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmHoadonGTGTCT, FrmHoadonGTGTCT);
    with QrHD do
        FrmHoadonGTGTCT.Execute(
            mCanEdit,
        	TGuidField(FieldByName('KHOA')).AsGuid,
            FieldByName('SCT').AsString,
            FieldByName('TENCTY').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdDelExecute(Sender: TObject);
begin
    if QrHD.FieldByName('DELETE_DATE').AsFloat = 0 then
        if not YesNo('Hóa đơn khi bị xóa sẽ không được phục hồi. Tiếp tục?', 1) then
            Abort;

	exValidClosing(QrHD.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrHD);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else if ActiveControl = GrNHOMTHUE then
    	GrDetail.SetFocus
    else
        GrNHOMTHUE.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsHD)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdPrintExecute(Sender: TObject);
const
    mReportList: array[0..3] of String = (
        FORM_CODE + '_00',
        FORM_CODE + '_05',
        FORM_CODE + '_10',
        FORM_CODE + '_TT');
begin
	CmdSave.Execute;
	ShowReport(Caption, mReportList[(Sender as TComponent).Tag], [sysLogonUID, TGuidEx.ToStringEx(QrHD.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
    n: Integer;
begin
	with QrHD do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty)
        and (n = 1) and (QrHD.FieldByName('DELETE_DATE').AsFloat = 0);
    CmdDel.Caption := GetMarkCaption(QrHD, RS_PHUCHOI);
    CmdDetail.Enabled := not bEmpty;

    CmdPrint.Enabled  := not (bEmpty) and (n = 1);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse and (n = 1);
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdSwitch.Enabled := n = 1;

    CmdGetBill.Enabled := mCanEdit;
    CmdUpdateGroupTax.Enabled := mCanEdit;

    CmdChitietHH.Enabled := (n = 1) and not bEmpty
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;

        with QrNHOMTHUE do
        begin
            Close;
            Open;
        end;

        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrHD);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.GrBrowseDblClick(Sender: TObject);
begin
    if QrHD.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

	(*
    ** Master events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDBeforeInsert(DataSet: TDataSet);
begin
    with QrHD do
    begin
        defTencty := FieldByName('TENCTY').AsString;
        defMst := FieldByName('MST').AsString;
        defMst1 := FieldByName('MST1').AsString;
        defDchi := FieldByName('DCHI').AsString;
        defDthoai := FieldByName('DTHOAI').AsString;
        defFax := FieldByName('FAX').AsString;
        defHTTT := FieldByName('PT_THANHTOAN').AsString;
        defGhichu := FieldByName('GHICHU').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDBeforeOpen(DataSet: TDataSet);
begin
	with QrHD do
    begin
		Parameters[0].Value := fTungay;
		Parameters[1].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

	(*
    ** Detail events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTBeforeOpen(DataSet: TDataSet);
begin
    with DataSet As TADOQuery do
    	Parameters[0].Value := QrHD.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;
    SetEditState(QrHD);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTAfterDelete(DataSet: TDataSet);
begin
    mChangeCT := True;
end;

procedure TFrmHoadonGTGT.QrCTAfterEdit(DataSet: TDataSet);
begin
    SetEditState(QrHD);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTAfterInsert(DataSet: TDataSet);
begin
    SetEditState(QrHD);
    mChangeCT := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTCalcFields(DataSet: TDataSet);
var
    s: String;
begin
    with QrCT do
        if not IsEmpty then
            FieldByName('DS_NHOMTHUE').AsString :=
                LayDsNhomthue(TGuidField(FieldByName('KHOA_CT')).AsGuid,
                    QrHD.FieldByName('NGAY').AsDateTime, s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdDetailExecute(Sender: TObject);
begin
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrHD, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDBeforePost(DataSet: TDataSet);
begin
    if BlankConfirm(QrHD, ['NGAY', 'TENCTY']) then
        Abort;
    DataMain.AllocSCT(mLCT, QrHD);
    SetAudit(QrHD);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrHDAfterInsert(DataSet: TDataSet);
var
    sKho: string;
begin
    if not VarIsNull(frKHO1.cbbKho.Value) then
        sKho := frKHO1.cbbKho.Value
    else
        sKho := '';

    with QrHD do
    begin
        TGuidEx.NewGuidDate(FieldByName('KHOA'));
        FieldByName('NGAY').AsDateTime := Now;
        FieldByName('MAKHO').AsString := sKho;
        FieldByName('LOC').AsString           := sysLoc;

        FieldByName('TENCTY').AsString := defTencty;
        FieldByName('MST').AsString := defMst;
        FieldByName('MST1').AsString := defMst1;
        FieldByName('DCHI').AsString := defDchi;
        FieldByName('DTHOAI').AsString := defDthoai;
        FieldByName('FAX').AsString := defFax;
        FieldByName('PT_THANHTOAN').AsString := defHTTT;
        FieldByName('GHICHU').AsString := defGhichu;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdGetBillExecute(Sender: TObject);
var
    mList: TStrings;
    i : Integer;
    mKhoa, mKhoact: TGUID;
begin
    mList := TStringList.Create;
    Application.CreateForm(TFrmHoadonGTGTGetBill, FrmHoadonGTGTGetBill);
    if FrmHoadonGTGTGetBill.Get(mList) then
    begin
        if not YesNo('Thêm danh sách bill đã được chọn vào chi tiết. Tiếp tục?') then
            Exit;

        Refresh;
        Wait(PROCESSING);
        with QrCT do
        begin
            mKhoa := TGuidField(QrHD.FieldByName('KHOA')).AsGuid;
            DisableControls;

            for i := 0 to mList.Count - 1 do
            begin
                mKhoact :=  TGuidEx.FromString(mList.Strings[i]);

                if not Locate('KHOA_CT', TGuidEx.ToString(mKhoact), []) then
                begin
                    Append;
                    TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
                    TGuidField(FieldByName('KHOA_CT')).AsGuid := mKhoact;
                    Post;
                end;
            end;

            EnableControls;
        end;

        ClearWait;
    end;
    mList.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdUpdateGroupTaxExecute(Sender: TObject);
var
    s, dgiai: String;
    lsThue, ls: TStrings;
    i: Integer;
    bm: TBytes;
    mKhoa: TGUID;
begin
    Wait(PROCESSING);

    lsThue := TStringList.Create;
    ls := TStringList.Create;

    // Get List
    with QrCT do
    begin
        DisableControls;
        bm := BookMark;
        First;
        while not Eof do
        begin
	    	isStrBreak(
                LayDsNhomthue(TGuidField(FieldByName('KHOA_CT')).AsGuid, QrHD.FieldByName('NGAY').AsDateTime, dgiai),
                 ',', ls);
        	lsThue.AddStrings(ls);
            Next;
        end;
        BookMark := bm;
        EnableControls;
    end;
    ls.Free;

    // Clean up
    EmptyDataSet(QrNHOMTHUE);

    mKhoa := TGuidField(QrHD.FieldByName('KHOA')).AsGuid;
    for i := 0 to lsThue.Count - 1 do
    begin
        s := Trim(lsThue[i]);
		if s <> '' then
            with QrNHOMTHUE do
                if not Locate('NHOMTHUE', s, []) then
                begin
                    Append;
                    TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
                    TGuidEx.NewGuidDate(FieldByName('KHOACT'));
                    FieldByName('NHOMTHUE').AsString  := s;
                    FieldByName('DGIAI').AsString := dgiai;
                    Post;
                end;
    end;

    lsThue.Free;
    ClearWait;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoadonGTGT.CapnhatHH(pKHOA: TGUID): Boolean;
begin
    with CAPNHAT_HDGTGT_HH do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKHOA);
        Execute;
        Result := Parameters[0].Value = 0;                
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CbHTTTNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.BtnInClick(Sender: TObject);
begin
    BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmHoadonGTGT.LayDsNhomthue(pKHOA: TGUID; pNgay: TDateTime; var pDgiai: String): String;
begin
    with DS_NHOMTHUE do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(pKHOA);
        Parameters[2].Value := pNgay;
        Execute;
        Result := Parameters[3].Value;
        pDgiai := Parameters[4].Value;
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.GrDetailCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if not Highlight and (Field.FullName = 'DS_NHOMTHUE') then
        with AFont do
        begin
            Style := [fsBold];
            Color := clPurple;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.PD1PullDown(Sender: TObject);
begin
    GrNHOMTHUE.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.QrCTKHOA_CTChange(Sender: TField);
var
    mNgay: TDateTime;
    mSotien: Double;
    mSCT: String;
    mThungan: Integer;
begin
    with GET_BILL_INFO do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrCT.FieldByName('KHOA_CT'));
        Execute;

        mNgay    := Parameters[2].Value;
        mSct     := Parameters[3].Value;
        mSotien  := Parameters[4].Value;
        mThungan := Parameters[5].Value;
    end;

    // update bill infor
    with QrCT do
    begin
        FieldByName('NGAY').AsDateTime  := mNgay;
        FieldByName('SCT').AsString     := mSct;
        FieldByName('SOTIEN').AsFloat   := mSotien;

        if mThungan <> 0 then
            FieldByName('THUNGAN').AsInteger := mThungan
        else
            FieldByName('THUNGAN').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsHD);
end;


end.
