﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtDanhSachImport;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, Vcl.Buttons, Vcl.Graphics,
  wwDialog, wwfltdlg, wwFltDlg2;

type
  TFrmDmvtDanhSachImport = class(TForm)
    GrList: TwwDBGrid2;
    DsList: TDataSource;
    QrList: TADOQuery;
    ActionList: TActionList;
    CmdRegister: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    Panel3: TPanel;
    BtnRegister: TBitBtn;
    BitBtn2: TBitBtn;
    spIMP_DM_HH_InsertData: TADOCommand;
    spIMP_DM_HH_DeleteList: TADOCommand;
    Filter: TwwFilterDialog2;
    CmdDel: TAction;
    Xadng1: TMenuItem;
    CmdDelRecordError: TAction;
    Xaccdngangli1: TMenuItem;
    CHECK_RECORD_ERROR: TADOCommand;
    QrListMAVT: TWideStringField;
    QrListTENVT: TWideStringField;
    QrListDVT: TWideStringField;
    QrListQD1: TIntegerField;
    QrListDvtHop: TWideStringField;
    QrListVAT_RA: TFloatField;
    QrListGiaNhapChuaThue: TFloatField;
    QrListGiaNhap: TFloatField;
    QrListGiaBan: TFloatField;
    QrListTyLeLai: TFloatField;
    QrListSheet: TWideStringField;
    QrListErrCode: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdRegisterExecute(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure CmdDelRecordErrorExecute(Sender: TObject);
    procedure QrListAfterDelete(DataSet: TDataSet);
  private
    mTransNo, countError: Integer;
    mCanEdit, mDelete, isFunctionApproved: Boolean;
    function DeleteAllRecord(pTransNo: Integer; pIsError: Boolean = false): Boolean;
    function CheckRecordError: Boolean;
  public
  	function Execute(r: WORD; pTransNo: Integer) : Boolean;
  end;

var
  FrmDmvtDanhSachImport: TFrmDmvtDanhSachImport;

const
    TABLE_NAME = 'IMP_DM_HH';
    FORM_CODE = 'IMP_DM_HH';
implementation

uses
	Rights, isDb, ExCommon, isLib, GuidEx, isCommon, isMsg, MainData, HrData;


{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
    RS_CONFIRM_DELETED_ERRORS	    = 'Xóa các dòng đang lỗi. Tiếp tục?';
    RS_INVAILD_RECORD_ERRORS	    = 'Dữ liệu còn lỗi. Không thể thực hiện được.';

function TFrmDmvtDanhSachImport.Execute;
begin
    mCanEdit := rCanEdit(r);
   	mTransNo := pTransNo;
    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
    if QrList.RecordCount > 0  then
    begin
        DeleteAllRecord(mTransNo);
    end;

    CloseDataSets([QrList]);
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    AddAllFields(QrList, TABLE_NAME);

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.FormShow(Sender: TObject);
begin

    with HrDataMain do
        OpenDataSets([QrDMNV]);

	OpenDataSets([QrList]);

    SetDisplayFormat(QrList, sysCurFmt);
    SetShortDateFormat(QrList, ShortDateFormat);
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE, Filter);
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.QrListAfterDelete(DataSet: TDataSet);
begin
    CheckRecordError;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.QrListBeforeOpen(DataSet: TDataSet);
begin
    with DataSet as TADOQuery do
        Parameters[0].Value := mTransNo;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdDelRecordErrorExecute(
  Sender: TObject);
begin
    if not YesNo(RS_CONFIRM_DELETED_ERRORS, 1) then
    	Exit;

    DeleteConfirm(False);
    DeleteAllRecord(mTransNo, True);
    DeleteConfirm(True);
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdRefreshExecute(Sender: TObject);
begin
    QrList.Requery;
    CheckRecordError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdRegisterExecute(Sender: TObject);
var
    s: string;
begin
    try
       	Wait('Đang xử lý...');
        with spIMP_DM_HH_InsertData do
        begin
            Prepared := True;
            Parameters[1].Value := sysLogonUID;
            Parameters[2].Value := mTransNo;
            Parameters[4].Value := 1;
            try
                Execute;
            except
            end;

            if Parameters[0].Value <> 0 then
            begin
                mTransNo := 0;
                s := Parameters[3].Value;
                ErrMsg(s);
            end
            else
            begin
                ClearWait;
                MsgDone;
                ModalResult := mrOk;
            end;
        end;
    finally
		ClearWait;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.CmdSearchExecute(Sender: TObject);
begin
    exSearch(Name, DsList);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bIsError: Boolean;
begin
    with QrList do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    bIsError := countError > 0;
    CmdDel.Enabled := bBrowse and (not bEmpty);
    CmdDelRecordError.Enabled := bIsError;
    CmdRegister.Enabled := not bIsError and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtDanhSachImport.GrListCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
     if Highlight then
        Exit;

    if QrList.FieldByName('ErrCode').AsString <> '' then
    begin
	    AFont.Color := clRed;
        Exit;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvtDanhSachImport.DeleteAllRecord;
begin
    with spIMP_DM_HH_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvtDanhSachImport.CheckRecordError;
begin
    with CHECK_RECORD_ERROR do
    begin
        Prepared := True;
        Parameters[0].Value := mTransNo;
        countError := Execute.RecordCount;
    end;
    Result := True;
end;


end.
