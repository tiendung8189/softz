﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExVIP;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls, Buttons;

type
  TFrmExVIP = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Edit1: TEdit;
    Edit2: TEdit;
    procedure CmdReturnClick(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
  private
  public
    function Execute(var m1, m2: Integer): Boolean;
  end;

var
  FrmExVIP: TFrmExVIP;

implementation

{$R *.DFM}

uses
	isMsg, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmExVIP.Execute;
begin
	Result := ShowModal = mrOK;
    if Result then
    begin
		m1 := StrToInt(Edit1.Text);
		m2 := StrToInt(Edit2.Text);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_VIPCODE = 'Mã không hợp lệ.';
    
procedure TFrmExVIP.CmdReturnClick(Sender: TObject);
begin
	try
		StrToInt(Edit1.Text);
		StrToInt(Edit2.Text);
        ModalResult := mrOK
    except
		Edit1.SetFocus;
    	ErrMsg(RS_INVALID_VIPCODE);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExVIP.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmExVIP.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
end;

end.
