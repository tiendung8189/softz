﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThongtinCN2;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, wwdblook, Mask, wwdbedit,
  ActnList, DB;

type
  TFrmThongtinCN2 = class(TForm)
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label30: TLabel;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdTENDT: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MyActionList: TActionList;
    CmdOK: TAction;
    Label15: TLabel;
    wwDBEdit4: TwwDBEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn2: TBitBtn;
    DsBH: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdOKExecute(Sender: TObject);
  private
    mPTTT: String;
  public
  	function Execute (pPTTT : String) : Boolean;
  end;

var
  FrmThongtinCN2: TFrmThongtinCN2;

implementation

uses
	isLib, MainData, isDb, isMsg, CongnoBanle;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN2.CmdOKExecute(Sender: TObject);
begin
    CmdReturn.SetFocus;
    with FrmCongnoBanle.QrBH do
    begin
        SetDefButton(1);
        if BlankConfirm(FrmCongnoBanle.QrBH, ['CN_TENDV', 'CN_DIACHI', 'CN_LIENHE', 'CN_DTHOAI']) then
            Abort;

        if mPTTT = '03' then
        begin
            if BlankConfirm(FrmCongnoBanle.QrBH, ['CN_MATK']) then
                Abort;
        end;
        SetDefButton(0);
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN2.Execute;
begin
    mPTTT := pPTTT;
	Result := ShowModal = mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN2.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN2.FormShow(Sender: TObject);
var
    b: Boolean;
begin
    TMyForm(Self).Init;
    b := mPTTT = '03';
    Label11.Visible := b;
    DsBH.DataSet := FrmCongnoBanle.QrBH;
    if not b then
        Self.Height := Self.Height - GroupBox3.Height;
    GroupBox3.Visible := b;
end;

end.
