﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit frameKho;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, wwdbdatetimepicker, ExtCtrls, wwdblook, DB, ActnList, DBGridEh,
  DBCtrlsEh, Vcl.Mask, DBLookupEh, DbLookupComboboxEh2;

type
  TfrKHO = class(TFrame)
    Panel1: TPanel;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    cbbKho: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure cbbKhoDropDown(Sender: TObject);
    procedure cbbKhoExit(Sender: TObject);
    procedure cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
  private
  	FAction: TAction;
  public
  	procedure Init; overload;
  	procedure Init2(d1, d2: TDateTime; ac: TAction); overload;
  end;

implementation

uses
	ExCommon, isLib, isCommon, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.Init;
begin
    Init2(Date - sysLateDay, Date, NIL);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.Init2(d1, d2: TDateTime; ac: TAction);
begin
	EdFrom.Date := d1;
	EdTo.Date := d2;
    FAction := ac;
    cbbKho.Value := sysDefKho;
    EdMaKho.Text := sysDefKho;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
begin
    if Assigned(FAction) then
    if Accept then
        FAction.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.cbbKhoDropDown(Sender: TObject);
begin
    if (not sysIsDrc) or (not sysIsCentral) then
    begin
         cbbKho.DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
         cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.cbbKhoExit(Sender: TObject);
begin
     EdMaKho.Text := cbbKho.Value;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TfrKHO.CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
