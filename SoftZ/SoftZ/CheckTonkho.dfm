object FrmCheckTonkho: TFrmCheckTonkho
  Left = 176
  Top = 114
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Th'#244'ng Tin T'#7891'n Kho M'#7863't H'#224'ng'
  ClientHeight = 322
  ClientWidth = 1012
  Color = clBtnFace
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 1012
    Height = 65
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    ExplicitWidth = 793
    object LbDONVI: TLabel
      Left = 53
      Top = 17
      Width = 67
      Height = 16
      Alignment = taRightJustify
      Caption = 'M'#227' barcode'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label31: TLabel
      Left = 275
      Top = 17
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#234'n h'#224'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbMA: TLabel
      Left = 129
      Top = 17
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbTEN: TLabel
      Left = 338
      Top = 17
      Width = 40
      Height = 16
      Caption = 'HOTEN'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 79
      Top = 35
      Width = 41
      Height = 16
      Alignment = taRightJustify
      Caption = 'M'#227' kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 283
      Top = 35
      Width = 46
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#234'n kho'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object LbMa2: TLabel
      Left = 129
      Top = 35
      Width = 38
      Height = 16
      Caption = 'MASO'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object LbTen2: TLabel
      Left = 338
      Top = 35
      Width = 40
      Height = 16
      Caption = 'HOTEN'
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 65
    Width = 1012
    Height = 236
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MAVT'#9'15'#9'M'#227' h'#224'ng'#9'F'
      'TENVT'#9'35'#9'T'#234'n h'#224'ng'#9'F'
      'DVT'#9'7'#9#272'vt'#9'F'
      'SOLUONG'#9'12'#9'Tr'#432#7899'c t'#7841'o phi'#7871'u'#9'F'#9'S'#7889' l'#432#7907'ng t'#7891'n'
      'LG_CUOI'#9'12'#9'Hi'#7879'n t'#7841'i'#9'F'#9'S'#7889' l'#432#7907'ng t'#7891'n')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = CHUNGTU_CT
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 2
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
    PaintOptions.AlternatingRowColor = 16772294
    ExplicitWidth = 793
  end
  object Status: TStatusBar
    Left = 0
    Top = 301
    Width = 1012
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
    ExplicitWidth = 793
  end
  object ActionList1: TActionList
    Left = 248
    Top = 160
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c '
      ShortCut = 32856
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object CHECK_TONKHO: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    ProcedureName = 'spCHECK_TONKHO;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 15
        Value = Null
      end
      item
        Name = '@MAKHO'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 5
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 368
    Top = 158
  end
  object CHUNGTU_CT: TDataSource
    DataSet = CHECK_TONKHO
    Left = 396
    Top = 158
  end
end
