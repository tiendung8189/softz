object FrmThu2: TFrmThu2
  Left = 90
  Top = 37
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Phi'#7871'u Thu Theo Ch'#7913'ng T'#7915' Xu'#7845't '
  ClientHeight = 600
  ClientWidth = 885
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    885
    600)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 38
    Width = 885
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 885
    Height = 38
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object SepChecked: TToolButton
      Left = 324
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 332
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object ToolButton11: TToolButton
      Left = 392
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 400
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 40
    Width = 885
    Height = 560
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 510
        Width = 877
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object Panel3: TPanel
        Left = 0
        Top = 0
        Width = 877
        Height = 510
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object GrBrowse: TwwDBGrid2
          Left = 0
          Top = 49
          Width = 877
          Height = 461
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'IMG;ImageIndex;Original Size'
            'IMG2;ImageIndex;Original Size')
          Selected.Strings = (
            'IMG'#9'3'#9#9'F'
            'IMG2'#9'3'#9#9'F'
            'NGAY'#9'18'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
            'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
            'SOTIEN'#9'12'#9'Thanh to'#225'n'#9'F'
            'CALC_SOTIEN'#9'15'#9'T'#7893'ng c'#7897'ng'#9'F'
            'MADT'#9'10'#9'M'#227#9'F'#9#272#417'n v'#7883' n'#7897'p'
            'LK_TENDT'#9'40'#9'T'#234'n'#9'F'#9#272#417'n v'#7883' n'#7897'p'
            'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng'
            'LK_TENKHO'#9'30'#9'T'#234'n'#9'F'#9'Kho h'#224'ng'
            'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mDisableDialog]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 0
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsTC
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgAllowInsert]
          Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopMaster
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = True
          UseTFields = False
          OnCalcCellColors = GrBrowseCalcCellColors
          OnDblClick = GrBrowseDblClick
          OnEnter = CmdRefreshExecute
          ImageList = DataMain.ImageMark
          TitleImageList = DataMain.ImageSort
          PadColumnStyle = pcsPadHeader
        end
        inline frDate: TfrNGAY
          Left = 0
          Top = 0
          Width = 877
          Height = 49
          Align = alTop
          Color = 16119285
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentBackground = False
          ParentColor = False
          ParentFont = False
          TabOrder = 0
          ExplicitWidth = 877
          inherited Panel1: TPanel
            Width = 877
            ParentColor = False
            ExplicitWidth = 877
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 877
        Height = 210
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        object Panel1: TPanel
          Left = 2
          Top = 2
          Width = 873
          Height = 33
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 0
          object DBText1: TDBText
            Left = 6
            Top = 4
            Width = 21
            Height = 17
            DataField = 'XOA'
            DataSource = DsTC
            Font.Charset = ANSI_CHARSET
            Font.Color = clRed
            Font.Height = -16
            Font.Name = 'Arial'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label1: TLabel
            Left = 77
            Top = 14
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object CbNgay: TwwDBDateTimePicker
            Left = 112
            Top = 10
            Width = 165
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            DataField = 'NGAY'
            DataSource = DsTC
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
            ShowButton = True
            TabOrder = 0
          end
          object EdSoPhieu: TDBEditEh
            Left = 340
            Top = 10
            Width = 185
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            CharCase = ecUpperCase
            Color = 15794175
            ControlLabel.Width = 50
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' phi'#7871'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'SCT'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object CbHinhThuc: TDbLookupComboboxEh2
            Left = 673
            Top = 10
            Width = 181
            Height = 22
            ControlLabel.Width = 54
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'PTTT'
            DataSource = DsTC
            DropDownBox.Columns = <
              item
                FieldName = 'DGIAI'
              end>
            DropDownBox.ListSource = DataMain.DsPTTT
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA'
            ListField = 'DGIAI'
            ListSource = DataMain.DsPTTT
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 2
            Visible = True
          end
        end
        object Panel2: TPanel
          Left = 2
          Top = 131
          Width = 873
          Height = 77
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 4
          object DBEditEh4: TDBEditEh
            Left = 112
            Top = 1
            Width = 165
            Height = 22
            BevelKind = bkFlat
            BorderStyle = bsNone
            ControlLabel.Width = 58
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ng'#432#7901'i n'#7897'p'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'Nguoi'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBMemoEh1: TDBMemoEh
            Left = 112
            Top = 25
            Width = 413
            Height = 46
            ControlLabel.Width = 49
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AutoSize = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Ctl3D = False
            DataField = 'GhiChu'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            ParentCtl3D = False
            ShowHint = True
            TabOrder = 5
            Visible = True
            WantReturns = True
          end
          object EdNumSoTien: TDBNumberEditEh
            Left = 404
            Top = 1
            Width = 121
            Height = 22
            TabStop = False
            ControlLabel.Width = 65
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#7893'ng s'#7889' d'#432
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoDu'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object EdSOTIEN: TDBNumberEditEh
            Left = 673
            Top = 1
            Width = 121
            Height = 22
            TabStop = False
            ControlLabel.Width = 95
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#7893'ng c'#7897'ng phi'#7871'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTien'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object EhThanhToan: TDBNumberEditEh
            Left = 673
            Top = 49
            Width = 121
            Height = 22
            TabStop = False
            ControlLabel.Width = 65
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Thanh to'#225'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhToan'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 4
            Visible = True
          end
          object EdSoTienTraHang: TDBNumberEditEh
            Left = 673
            Top = 25
            Width = 121
            Height = 22
            TabStop = False
            ControlLabel.Width = 61
            ControlLabel.Height = 16
            ControlLabel.Caption = '- Tr'#7843' h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clBlue
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTienTraHang'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            Visible = True
          end
        end
        object PaTK: TPanel
          Left = 2
          Top = 83
          Width = 873
          Height = 48
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 3
          object CbTenTK: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 237
            Height = 22
            ControlLabel.Width = 56
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#224'i kho'#7843'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MATK'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TENTK'
                Width = 236
              end
              item
                FieldName = 'MATK'
                Width = 155
              end>
            DropDownBox.ListSource = DataMain.DsDMTK_NB
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 413
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MATK'
            ListField = 'TENTK'
            ListSource = DataMain.DsDMTK_NB
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdSTK: TDBEditEh
            Left = 352
            Top = 1
            Width = 173
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MATK'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object EdNganhang: TDBEditEh
            Left = 112
            Top = 25
            Width = 237
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Width = 61
            ControlLabel.Height = 16
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_NGANHANG'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object EdChinhanh: TDBEditEh
            Left = 352
            Top = 25
            Width = 173
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'LK_CHINHANH'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 3
            Visible = True
          end
        end
        object PaMALOC: TPanel
          Left = 2
          Top = 35
          Width = 873
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 1
          object EdMaLoc: TDBEditEh
            Left = 448
            Top = 1
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MALOC'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object CbbMaLoc: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 333
            Height = 22
            ControlLabel.Width = 51
            ControlLabel.Height = 16
            ControlLabel.Caption = #272#7883'a '#273'i'#7875'm'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MALOC'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TEN'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 331
              end
              item
                FieldName = 'LOC'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 60
              end>
            DropDownBox.ListSource = DataMain.DsLOC
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 412
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'LOC'
            ListField = 'TEN'
            ListSource = DataMain.DsLOC
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object DBEditEh1: TDBEditEh
            Left = 673
            Top = 1
            Width = 181
            Height = 22
            TabStop = False
            BevelKind = bkFlat
            BorderStyle = bsNone
            CharCase = ecUpperCase
            Color = clBtnFace
            ControlLabel.Width = 136
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' phi'#7871'u theo h'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'SCT2'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
        end
        object PaMADT: TPanel
          Left = 2
          Top = 59
          Width = 873
          Height = 24
          Align = alTop
          BevelOuter = bvNone
          TabOrder = 2
          object CbbKhachHang: TDbLookupComboboxEh2
            Left = 112
            Top = 1
            Width = 333
            Height = 22
            ControlLabel.Width = 61
            ControlLabel.Height = 16
            ControlLabel.Caption = #272#417'n v'#7883' n'#7897'p'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MADT'
            DataSource = DsTC
            DropDownBox.AutoFitColWidths = False
            DropDownBox.Columns = <
              item
                FieldName = 'TENDT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 331
              end
              item
                FieldName = 'MADT'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 60
              end>
            DropDownBox.ListSource = DataMain.DsDMKH
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 412
            EmptyDataInfo.Color = clInfoBk
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MADT'
            ListField = 'TENDT'
            ListSource = DataMain.DsDMKH
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 0
            Visible = True
          end
          object EdMaDT: TDBEditEh
            Left = 448
            Top = 1
            Width = 77
            Height = 22
            TabStop = False
            Alignment = taLeftJustify
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'MADT'
            DataSource = DsTC
            DynProps = <>
            EditButtons = <
              item
                Action = CmdXemCongNo
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
      object PgDetail: TPageControl
        Left = 0
        Top = 210
        Width = 877
        Height = 321
        ActivePage = TabSheet4
        Align = alClient
        TabOrder = 0
        object TabSheet3: TTabSheet
          Caption = 'Phi'#7871'u xu'#7845't h'#224'ng'
          object GrDetail: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 869
            Height = 290
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            Selected.Strings = (
              'CALC_STT'#9'3'#9'STT'#9'T'
              'LK_NGAY'#9'18'#9'Ng'#224'y'#9'T'#9'Ch'#7913'ng t'#7915
              'LK_SCT'#9'17'#9'S'#7889#9'T'#9'Ch'#7913'ng t'#7915
              'LK_HOADON_SO'#9'12'#9'S'#7889#9'T'#9'H'#243'a '#273#417'n'
              'LK_HOADON_SERI'#9'8'#9'Seri'#9'T'#9'H'#243'a '#273#417'n'
              'LK_HOADON_NGAY'#9'10'#9'Ng'#224'y'#9'T'#9'H'#243'a '#273#417'n'
              'SODU'#9'13'#9'S'#7889' d'#432#9'T'#9'S'#7889' ti'#7873'n'
              'SOTIEN'#9'13'#9'Thanh to'#225'n'#9'F'#9'S'#7889' ti'#7873'n'
              'CALC_CONLAI'#9'13'#9'C'#242'n l'#7841'i'#9'T'#9'S'#7889' ti'#7873'n'
              'LK_DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'T')
            MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly, mDisableDialog]
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 1
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            Align = alClient
            DataSource = DsCT
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgWordWrap, dgShowFooter, dgShowCellHint]
            ParentFont = False
            PopupMenu = PopNX
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 2
            TitleButtons = False
            UseTFields = False
            OnUpdateFooter = GrDetailUpdateFooter
            FooterColor = 13360356
            FooterCellColor = 13360356
            PadColumnStyle = pcsPadHeader
          end
        end
        object TabSheet4: TTabSheet
          Caption = 'Phi'#7871'u nh'#7853'p tr'#7843
          ImageIndex = 1
          object GrNhaptra: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 869
            Height = 290
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            Selected.Strings = (
              'CALC_STT'#9'3'#9'STT'#9'F'
              'LK_NGAY'#9'18'#9'Ng'#224'y'#9'F'
              'LK_SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
              'LK_THANHTOAN'#9'13'#9'S'#7889' ti'#7873'n'#9'F'
              'LK_DGIAI'#9'60'#9'Di'#7877'n gi'#7843'i'#9'F')
            MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly, mDisableDialog]
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 1
            ShowHorzScrollBar = True
            EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
            Align = alClient
            DataSource = DsCT2
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowDelete]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
            ParentFont = False
            PopupMenu = PopNT
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 1
            TitleButtons = False
            UseTFields = False
            FooterColor = 13360356
            FooterCellColor = 13360356
            PadColumnStyle = pcsPadHeader
          end
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 743
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 743
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 532
    Top = 444
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 5
      ShortCut = 116
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = ' K'#7871't th'#250'c '
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdSwitch: TAction
      Caption = 'Fast Switch'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdChonCT2: TAction
      Category = 'CHITIET'
      Caption = 'Ch'#7885'n phi'#7871'u nh'#7853'p tr'#7843
      OnExecute = CmdChonCT2Execute
    end
    object CmdChonCT: TAction
      Category = 'CHITIET'
      Caption = 'Ch'#7885'n phi'#7871'u xu'#7845't'
      OnExecute = CmdChonCTExecute
    end
    object CmdTotal: TAction
      Hint = 'T'#237'nh t'#7893'ng s'#7889' ti'#7873'n thanh to'#225'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdEmpty: TAction
      Category = 'CHITIET'
      Caption = 'X'#243'a chi ti'#7871't'
      Hint = 'X'#243'a chi ti'#7871't'
      OnExecute = CmdEmptyExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdXemCongNo: TAction
      Caption = 'CmdXemCongNo'
      OnExecute = CmdXemCongNoExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsTC
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MST'
      'LK_PTTT'
      'NGUOI')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 504
    Top = 444
  end
  object QrTC: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrTCBeforeOpen
    BeforeInsert = QrTCBeforeInsert
    AfterInsert = QrTCAfterInsert
    BeforeEdit = QrTCBeforeEdit
    BeforePost = QrTCBeforePost
    AfterPost = QrTCAfterPost
    AfterCancel = QrTCAfterCancel
    AfterScroll = QrTCAfterScroll
    OnCalcFields = QrTCCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'THUCHI'
      ' where '#9'LCT = :LCT'
      '   and '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      '   and'#9'LOC = :LOC')
    Left = 588
    Top = 444
    object QrTCIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrTCXOA: TWideStringField
      Alignment = taCenter
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrTCIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrTCCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrTCLK_PTTT: TWideStringField
      DisplayLabel = 'H'#236'nh th'#7913'c'
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = DataMain.QrPTTT
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'PTTT'
      Size = 100
      Lookup = True
    end
    object QrTCLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Visible = False
      Size = 50
      Lookup = True
    end
    object QrTCLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n '#273#417'n v'#7883
      DisplayWidth = 30
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDM_KH_NCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 50
      Lookup = True
    end
    object QrTCCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrTCUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrTCDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrTCCALC_SOTIEN: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_SOTIEN'
      Calculated = True
    end
    object QrTCCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrTCUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrTCDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrTCCALC_CONLAI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_CONLAI'
      Calculated = True
    end
    object QrTCLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrTCNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrTCSCT: TWideStringField
      FieldName = 'SCT'
      Size = 50
    end
    object QrTCMAKHO: TWideStringField
      FieldName = 'MAKHO'
      Size = 5
    end
    object QrTCPTTT: TWideStringField
      FieldName = 'PTTT'
      OnChange = QrTCPTTTChange
    end
    object QrTCMADT: TWideStringField
      DisplayLabel = #272#417'n v'#7883' n'#7897'p'
      FieldName = 'MADT'
      OnChange = QrTCMADTChange
      OnValidate = QrTCMADTValidate
      Size = 15
    end
    object QrTCKHOA: TGuidField
      FieldName = 'KHOA'
      Visible = False
      FixedChar = True
      Size = 38
    end
    object QrTCLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrTCMATK: TWideStringField
      DisplayLabel = 'T'#224'i kho'#7843'n'
      FieldName = 'MATK'
      OnChange = QrTCMATKChange
      Size = 50
    end
    object QrTCLK_TENTK: TWideStringField
      DisplayLabel = 'Ch'#7911' t'#224'i kho'#7843'n'
      FieldKind = fkLookup
      FieldName = 'LK_TENTK'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENTK'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_NGANHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_NGANHANG'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCLK_CHINHANH: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_CHINHANH'
      LookupDataSet = DataMain.QrDMTK
      LookupKeyFields = 'MATK'
      LookupResultField = 'TENNH_CN'
      KeyFields = 'MATK'
      Size = 200
      Lookup = True
    end
    object QrTCMALOC: TWideStringField
      FieldName = 'MALOC'
      Size = 5
    end
    object QrTCLK_TENLOC: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TENLOC'
      LookupDataSet = DataMain.QrLOC
      LookupKeyFields = 'LOC'
      LookupResultField = 'TEN'
      KeyFields = 'MALOC'
      Size = 200
      Lookup = True
    end
    object QrTCSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrTCMaCP: TWideStringField
      FieldName = 'MaCP'
    end
    object QrTCMATK2: TWideStringField
      FieldName = 'MATK2'
      Size = 50
    end
    object QrTCNhanVienThuNgan: TIntegerField
      FieldName = 'NhanVienThuNgan'
    end
    object QrTCSoTienTraHang: TFloatField
      FieldName = 'SoTienTraHang'
      OnChange = QrTCSOTIENChange
    end
    object QrTCSoGiaoDichNganHang: TWideStringField
      FieldName = 'SoGiaoDichNganHang'
      Size = 50
    end
    object QrTCGhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrTCSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrTCSoDu: TFloatField
      FieldName = 'SoDu'
    end
    object QrTCSoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrTCSOTIENChange
    end
    object QrTCThanhToan: TFloatField
      FieldName = 'ThanhToan'
      OnChange = QrTCSOTIENChange
    end
    object QrTCNguoi: TWideStringField
      FieldName = 'Nguoi'
      Size = 50
    end
    object QrTCLyDo: TWideStringField
      FieldName = 'LyDo'
      Size = 70
    end
  end
  object DsTC: TDataSource
    DataSet = QrTC
    Left = 588
    Top = 472
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 89
    Top = 380
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 55
    Top = 380
  end
  object QrCHITIET: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCHITIETBeforeOpen
    BeforeInsert = QrCHITIETBeforeInsert
    AfterInsert = QrCHITIETAfterEdit
    BeforeEdit = QrCHITIETBeforeEdit
    AfterEdit = QrCHITIETAfterEdit
    BeforePost = QrCHITIETBeforePost
    AfterCancel = QrCHITIETAfterCancel
    BeforeDelete = QrCHITIETBeforeDelete
    AfterDelete = QrCHITIETAfterDelete
    OnCalcFields = QrCHITIETCalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'THUCHI_CT'
      ' where '#9'KHOA =:KHOA'
      'order by '#9'NgayThanhToan')
    Left = 616
    Top = 444
    object QrCHITIETCALC_STT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CALC_STT'
      Calculated = True
    end
    object QrCHITIETLK_PTTT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PTTT'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'TEN_PTTT'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIETLK_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_NGAY'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_SCT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_SCT'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'SCT'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_THANHTOAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_THANHTOAN'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'ThanhToan'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETCALC_CONLAI: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_CONLAI'
      Calculated = True
    end
    object QrCHITIETLK_HOADON_SO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SO'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SO'
      KeyFields = 'KHOANX'
      Size = 10
      Lookup = True
    end
    object QrCHITIETLK_HOADON_SERI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SERI'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SERI'
      KeyFields = 'KHOANX'
      Size = 10
      Lookup = True
    end
    object QrCHITIETLK_HOADON_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_NGAY'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETLK_DGIAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DGIAI'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'GhiChu'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIETSCT2: TWideStringField
      FieldName = 'SCT2'
    end
    object QrCHITIETLK_TC_SOTIEN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_TC_SOTIEN'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'TC_SOTIEN'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIETTHANHTOAN_NX: TFloatField
      FieldName = 'THANHTOAN_NX'
    end
    object QrCHITIETTC_SOTIEN_NX: TFloatField
      FieldName = 'TC_SOTIEN_NX'
    end
    object QrCHITIETLK_PHIEUGIAOHANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_PHIEUGIAOHANG'
      LookupDataSet = QrNX
      LookupKeyFields = 'KHOA'
      LookupResultField = 'PHIEUGIAOHANG'
      KeyFields = 'KHOANX'
      Size = 0
      Lookup = True
    end
    object QrCHITIETKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCHITIETKHOANX: TGuidField
      FieldName = 'KHOANX'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETGhiChu: TWideStringField
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrCHITIETSoDu: TFloatField
      FieldName = 'SoDu'
      OnChange = QrCHITIETSODUChange
    end
    object QrCHITIETSoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrCHITIETSOTIENChange
      OnValidate = QrCHITIETSOTIENValidate
    end
    object QrCHITIETLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrCHITIETNgayThanhToan: TDateTimeField
      FieldName = 'NgayThanhToan'
    end
    object QrCHITIETKhoaCTNX: TGuidField
      FieldName = 'KhoaCTNX'
      FixedChar = True
      Size = 38
    end
    object QrCHITIETTC_XONG: TBooleanField
      FieldName = 'TC_XONG'
    end
  end
  object DsCT: TDataSource
    DataSet = QrCHITIET
    Left = 616
    Top = 472
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  a.*, b.DGIAI "TEN_PTTT"'
      '  from'
      '    ('
      'select  b.*'
      '  from  THUCHI_CT a, CHUNGTU b'
      ' where  a.KHOA =:KHOA'
      '   and  a.KHOANX = b.KHOA'
      'union all'
      'select  *'
      '  from  T_CHUNGTU'
      ' where  (LCT in ('#39'XBAN'#39', '#39'NTRA'#39',  '#39'XDC'#39', '#39'TCNO'#39'))'
      '   and'#9'MADT = :MADT'
      '   and  isnull(TC_XONG, 0) = 0'
      '    ) a left join DM_PT_THANHTOAN b on a.PTTT = b.MA'
      'order by a.KHOA')
    Left = 364
    Top = 396
  end
  object PopNT: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 121
    Top = 380
    object Chnphiunhptr1: TMenuItem
      Action = CmdChonCT2
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmpty
    end
  end
  object QrCHITIET2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCHITIET2BeforeOpen
    BeforeInsert = QrCHITIET2BeforeInsert
    AfterInsert = QrCHITIET2AfterEdit
    AfterEdit = QrCHITIET2AfterEdit
    BeforePost = QrCHITIETBeforePost
    AfterCancel = QrCHITIET2AfterCancel
    BeforeDelete = QrCHITIET2BeforeDelete
    AfterDelete = QrCHITIET2AfterDelete
    OnCalcFields = QrCHITIET2CalcFields
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'THUCHI_TRAHANG'
      ' where '#9'KHOA =:KHOA'
      'order by '#9'KHOACT')
    Left = 644
    Top = 444
    object QrCHITIET2CALC_STT: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'CALC_STT'
      Calculated = True
    end
    object QrCHITIET2LK_NGAY: TDateTimeField
      DisplayWidth = 18
      FieldKind = fkLookup
      FieldName = 'LK_NGAY'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_SCT: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_SCT'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'SCT'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_THANHTOAN: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_THANHTOAN'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'ThanhToan'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2LK_DGIAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DGIAI'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'GhiChu'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_SERI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SERI'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SERI'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_SO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_SO'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_SO'
      KeyFields = 'KHOANX'
      Size = 200
      Lookup = True
    end
    object QrCHITIET2LK_HOADON_NGAY: TDateTimeField
      FieldKind = fkLookup
      FieldName = 'LK_HOADON_NGAY'
      LookupDataSet = QrTRAHANG
      LookupKeyFields = 'KHOA'
      LookupResultField = 'HOADON_NGAY'
      KeyFields = 'KHOANX'
      Lookup = True
    end
    object QrCHITIET2KHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2LOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCHITIET2KHOANX: TGuidField
      FieldName = 'KHOANX'
      OnChange = QrCHITIET2KHOANXChange
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2KhoaCTNX: TGuidField
      FieldName = 'KhoaCTNX'
      FixedChar = True
      Size = 38
    end
    object QrCHITIET2LCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrCHITIET2SoTien: TFloatField
      FieldName = 'SoTien'
      OnChange = QrCHITIET2SOTIENChange
    end
  end
  object DsCT2: TDataSource
    DataSet = QrCHITIET2
    Left = 644
    Top = 472
  end
  object QrTRAHANG2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  *'
      '  from  T_CHUNGTU'
      ' where  LCT = '#39'NTRA'#39
      '   and  MADT = :MADT'
      '   and  isnull(TC_XONG, 0) = 0'
      'order by NGAY, SCT')
    Left = 456
    Top = 396
    object QrTRAHANG2NGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrTRAHANG2SCT: TWideStringField
      FieldName = 'SCT'
      Size = 50
    end
    object QrTRAHANG2KHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrTRAHANG2HOADON_SERI: TWideStringField
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrTRAHANG2HOADON_SO: TWideStringField
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrTRAHANG2HOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
    object QrTRAHANG2GhiChu: TWideMemoField
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrTRAHANG2ThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
  end
  object QrTRAHANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select  x.*'
      '  from'
      '    ('
      '    select  b.*'
      '      from  THUCHI_TRAHANG a, CHUNGTU b'
      '     where  a.KHOA =:KHOA'
      '       and  a.KHOANX = b.KHOA'
      '    union all'
      '    select  *'
      '      from  T_CHUNGTU'
      '     where  LCT = '#39'NTRA'#39
      '       and  MADT = :MADT'
      '       and  isnull(TC_XONG, 0) = 0'
      '    ) x'
      'order by x.KHOA')
    Left = 428
    Top = 396
  end
  object PhieuCT2: TwwSearchDialog
    Selected.Strings = (
      'NGAY'#9'18'#9'Ng'#224'y'#9'F'
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
      'THANHTOAN'#9'12'#9'S'#7889' ti'#7873'n'#9'F'
      'DGIAI'#9'40'#9'Di'#7877'n gi'#7843'i'#9'F')
    GridTitleAlignment = taCenter
    GridColor = clWhite
    Options = [opShowOKCancel, opShowStatusBar]
    GridOptions = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgPerfectRowFit]
    ShadowSearchTable = QrTRAHANG2
    Caption = 'Ch'#7885'n'
    MaxWidth = 0
    MaxHeight = 209
    CharCase = ecNormal
    UseTFields = False
    OnInitDialog = PhieuCT2InitDialog
    Left = 56
    Top = 412
  end
  object QrNX2: TADOStoredProc
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    AfterOpen = QrNX2AfterOpen
    ProcedureName = 'TC_CHUNGTU_XUAT;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = 0
      end
      item
        Name = '@MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 20
        Value = Null
      end
      item
        Name = '@NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end>
    Left = 392
    Top = 396
  end
  object PopNX: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 153
    Top = 380
    object MenuItem1: TMenuItem
      Action = CmdChonCT
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Xachitit2: TMenuItem
      Action = CmdEmpty
    end
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrTC
    DetailDataSet = QrCHITIET
    MasterFields.Strings = (
      'SoDu'
      'SoTien')
    DetailFields.Strings = (
      'SoDu'
      'SoTien')
    Left = 144
    Top = 424
  end
  object vlTotal2: TisTotal
    MasterDataSet = QrTC
    DetailDataSet = QrCHITIET2
    MasterFields.Strings = (
      'SoTienTraHang')
    DetailFields.Strings = (
      'SoTien')
    Left = 184
    Top = 424
  end
end
