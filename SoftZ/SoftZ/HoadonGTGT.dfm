object FrmHoadonGTGT: TFrmHoadonGTGT
  Left = 157
  Top = 97
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#243'a '#208#417'n Gi'#225' Tr'#7883' Gia T'#259'ng'
  ClientHeight = 573
  ClientWidth = 796
  Color = 16119285
  Constraints.MinHeight = 600
  Constraints.MinWidth = 800
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = TntFormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    796
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Label27: TLabel
    Left = 112
    Top = 14
    Width = 62
    Height = 16
    Alignment = taRightJustify
    Caption = 'Chi'#213't kh'#202'u'
  end
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 796
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 796
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 60
    Caption = 'ToolMain'
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton5: TToolButton
      Left = 60
      Top = 0
      Width = 8
      Caption = 'ToolButton5'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 68
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton3: TToolButton
      Left = 128
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton6: TToolButton
      Left = 188
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 196
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton8: TToolButton
      Left = 256
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnIn: TToolButton
      Left = 264
      Top = 0
      Cursor = 1
      Hint = 'In phi'#213'u'
      Caption = 'In'
      DropdownMenu = PopPrint
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object ToolButton10: TToolButton
      Left = 339
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 347
      Top = 0
      Cursor = 1
      Action = CmdChitietHH
      ImageIndex = 8
    end
    object ToolButton11: TToolButton
      Left = 407
      Top = 0
      Width = 8
      Caption = 'ToolButton11'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 415
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 796
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 2
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 788
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 77
        Width = 788
        Height = 398
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9
          'TENCTY'#9'30'#9'T'#234'n c'#244'ng ty'#9'F'
          'MST'#9'15'#9'M'#227' s'#7889' thu'#7871#9'F'
          'DCHI'#9'35'#9#272#7883'a ch'#7881#9'F'
          'DTHOAI'#9'18'#9#272'i'#7879'n tho'#7841'i'#9'F'
          'FAX'#9'18'#9'Fax'#9'F'
          'GHICHU'#9'35'#9'Di'#7877'n gi'#7843'i'#9'F')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsHD
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frKHO1: TfrKHO
        Left = 0
        Top = 0
        Width = 788
        Height = 77
        Align = alTop
        AutoSize = True
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 2
        ExplicitWidth = 788
        inherited Panel1: TPanel
          Width = 788
          ParentColor = False
          ExplicitWidth = 788
          inherited Label65: TLabel
            Left = 84
            Width = 47
            Height = 16
            ExplicitLeft = 84
            ExplicitWidth = 47
            ExplicitHeight = 16
          end
          inherited Label66: TLabel
            Left = 284
            Width = 54
            Height = 16
            ExplicitLeft = 284
            ExplicitWidth = 54
            ExplicitHeight = 16
          end
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = 'Chi ti'#7871't'
      object PaInfo: TPanel
        Left = 0
        Top = 0
        Width = 788
        Height = 138
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        TabStop = True
        object TntLabel16: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel10: TLabel
          Left = 38
          Top = 38
          Width = 67
          Height = 16
          Caption = 'T'#234'n c'#244'ng ty'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel11: TLabel
          Left = 470
          Top = 38
          Width = 63
          Height = 16
          Alignment = taRightJustify
          Caption = 'M'#227' s'#7889' thu'#7871
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel12: TLabel
          Left = 66
          Top = 62
          Width = 39
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7883'a ch'#7881
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel13: TLabel
          Left = 47
          Top = 86
          Width = 58
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7879'n tho'#7841'i'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel14: TLabel
          Left = 321
          Top = 86
          Width = 20
          Height = 16
          Alignment = taRightJustify
          Caption = 'Fax'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel15: TLabel
          Left = 63
          Top = 110
          Width = 42
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ghi ch'#250
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 259
          Top = 14
          Width = 50
          Height = 16
          Alignment = taRightJustify
          Caption = 'S'#7889' phi'#7871'u'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object TntLabel1: TLabel
          Left = 528
          Top = 86
          Width = 81
          Height = 16
          Alignment = taRightJustify
          Caption = 'HT thanh to'#225'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object CbNgay: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsHD
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object EdTENCTY: TwwDBEdit
          Left = 112
          Top = 34
          Width = 325
          Height = 22
          Ctl3D = False
          DataField = 'TENCTY'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdMST: TwwDBEdit
          Left = 540
          Top = 34
          Width = 165
          Height = 22
          Ctl3D = False
          DataField = 'MST'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 3
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdDCHI: TwwDBEdit
          Left = 112
          Top = 58
          Width = 657
          Height = 22
          Ctl3D = False
          DataField = 'DCHI'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdDTHOAI: TwwDBEdit
          Left = 112
          Top = 82
          Width = 153
          Height = 22
          Ctl3D = False
          DataField = 'DTHOAI'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit7: TwwDBEdit
          Left = 348
          Top = 82
          Width = 153
          Height = 22
          Ctl3D = False
          DataField = 'FAX'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit8: TwwDBEdit
          Left = 112
          Top = 106
          Width = 657
          Height = 22
          Ctl3D = False
          DataField = 'GHICHU'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdSCT: TwwDBEdit
          Left = 316
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbHTTT: TwwDBLookupCombo
          Left = 616
          Top = 82
          Width = 153
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'DGIAI'#9'0'#9'DGIAI'#9'F')
          DataField = 'PT_THANHTOAN'
          DataSource = DsHD
          LookupTable = DataMain.QrPTTT
          LookupField = 'MA'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 8
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbHTTTNotInList
        end
        object DBEdit1: TwwDBEdit
          Left = 708
          Top = 34
          Width = 61
          Height = 22
          Ctl3D = False
          DataField = 'MST1'
          DataSource = DsHD
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 4
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
      object GrDetail: TwwDBGrid2
        Left = 0
        Top = 264
        Width = 788
        Height = 232
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        Selected.Strings = (
          'NGAY'#9'14'#9'Ng'#224'y'#9'T'
          'SCT'#9'20'#9'S'#7889' phi'#7871'u'#9'T'
          'SOTIEN'#9'16'#9'Th'#224'nh ti'#7873'n'#9'T'
          'THUNGAN_DGIAI'#9'34'#9'Thu ng'#226'n'#9'T'
          'DS_NHOMTHUE'#9'20'#9'Danh s'#225'ch nh'#243'm thu'#7871#9'T')
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsCT
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgEnterToTab, dgAllowDelete]
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopDetail
        TabOrder = 2
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
        OnCalcCellColors = GrDetailCalcCellColors
        FooterColor = 13360356
      end
      object PD1: TisPanel
        Left = 0
        Top = 138
        Width = 788
        Height = 126
        Align = alTop
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        TabStop = True
        HeaderCaption = ' .: Danh s'#225'ch nh'#243'm thu'#7871
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 58
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        OnPullDown = PD1PullDown
        object GrNHOMTHUE: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 788
          Height = 110
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          PictureMasks.Strings = (
            'SOHDON'#9'*!'#9'T'#9'T'
            'SERI'#9'*!'#9'T'#9'T')
          Selected.Strings = (
            'NHOMTHUE'#9'10'#9'Lo'#7841'i thu'#7871#9'T'
            'DGIAI'#9'61'#9'Di'#7877'n gi'#7843'i'#9'F'
            'SOHDON'#9'20'#9'S'#7889' h'#243'a '#273#417'n'#9'F'
            'SERI'#9'13'#9'Seri'#9'F')
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          Align = alClient
          DataSource = DsNHOMTHUE
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopNhomthue
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 1
          TitleButtons = False
          UseTFields = False
          FooterColor = 13360356
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 650
    Top = 46
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 1
    ExplicitLeft = 650
    ExplicitTop = 46
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 398
    Top = 316
    object CmdPrint: TAction
      OnExecute = CmdPrintExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSwitch: TAction
      Hint = 'Chuy'#7875'n tab nhanh'
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdDetail: TAction
      Hint = 'Danh s'#225'ch <-> Chi ti'#7871't'
      ShortCut = 16418
      OnExecute = CmdDetailExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdIns: TAction
      Caption = 'CmdIns'
    end
    object CmdGetBill: TAction
      Caption = 'L'#7845'y danh s'#225'ch bill theo ng'#224'y'
      OnExecute = CmdGetBillExecute
    end
    object CmdUpdateGroupTax: TAction
      Caption = 'C'#7853'p nh'#7853't danh s'#225'ch nh'#243'm thu'#7871
      OnExecute = CmdUpdateGroupTaxExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdLisrRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdLisrRefeshExecute
    end
    object CmdChitietHH: TAction
      Caption = 'DS h'#224'ng'
      OnExecute = CmdChitietHHExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsHD
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'MANS'
      'TENNS'
      'MAKH'
      'TENKH'
      'SOLUONG'
      'SOTIEN'
      'NG_GIAO'
      'NG_NHAN'
      'GHICHU')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 396
    Top = 352
  end
  object QrHD: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrHDBeforeOpen
    BeforeInsert = QrHDBeforeInsert
    AfterInsert = QrHDAfterInsert
    BeforePost = QrHDBeforePost
    AfterScroll = QrHDAfterScroll
    OnCalcFields = QrHDCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'frDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'toDate'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'HOADON_GTGT'
      ' where  '#9'NGAY >= :frDate'
      '   and '#9'NGAY < :toDate + 1'
      ''
      '')
    Left = 206
    Top = 428
    object QrHDIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Calculated = True
    end
    object QrHDXOA: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Size = 1
      Calculated = True
    end
    object QrHDNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      FieldName = 'NGAY'
    end
    object QrHDSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrHDTENCTY: TWideStringField
      DisplayLabel = 'T'#234'n c'#244'ng ty'
      FieldName = 'TENCTY'
      Size = 200
    end
    object QrHDMST: TWideStringField
      FieldName = 'MST'
      Size = 15
    end
    object QrHDMST1: TWideStringField
      FieldName = 'MST1'
      Size = 5
    end
    object QrHDDCHI: TWideStringField
      FieldName = 'DCHI'
      Size = 200
    end
    object QrHDPT_THANHTOAN: TWideStringField
      FieldName = 'PT_THANHTOAN'
      Size = 15
    end
    object QrHDDTHOAI: TWideStringField
      FieldName = 'DTHOAI'
      Size = 200
    end
    object QrHDFAX: TWideStringField
      FieldName = 'FAX'
      Size = 200
    end
    object QrHDGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrHDCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrHDCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrHDUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrHDUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrHDDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
    end
    object QrHDDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
    end
    object QrHDMAKHO: TWideStringField
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrHDKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrHDLOC: TWideStringField
      FieldName = 'LOC'
      Size = 2
    end
  end
  object DsHD: TDataSource
    DataSet = QrHD
    Left = 206
    Top = 456
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 234
    Top = 456
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 292
    Top = 304
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Hinttc1: TMenuItem
      Action = CmdClearFilter
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 293
    Top = 334
  end
  object PopPrint: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 384
    Top = 256
    object rctip1: TMenuItem
      Caption = 'GTGT 0%'
      OnClick = CmdPrintExecute
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object GTGT101: TMenuItem
      Tag = 1
      Caption = 'GTGT 5%'
      OnClick = CmdPrintExecute
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object GTGT102: TMenuItem
      Tag = 2
      Caption = 'GTGT 10%'
      OnClick = CmdPrintExecute
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Khngchuthu1: TMenuItem
      Tag = 3
      Caption = 'Kh'#244'ng ch'#7883'u thu'#7871
      OnClick = CmdPrintExecute
    end
  end
  object QrDmQuay: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsKho
    Parameters = <
      item
        Name = 'MAKHO'
        Attributes = [paNullable]
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_QUAYTN'
      ' where '#9'MAKHO = :MAKHO'
      ' ')
    Left = 368
    Top = 428
    object QrDmQuayQUAY: TWideStringField
      FieldName = 'QUAY'
      Size = 2
    end
    object QrDmQuayMAKHO: TWideStringField
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDmQuayTENMAY: TWideStringField
      FieldName = 'TENMAY'
      Size = 50
    end
    object QrDmQuayGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterInsert
    AfterEdit = QrCTAfterEdit
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'HOADON_GTGT_CT'
      ' where  '#9'KHOA =:KHOA ')
    Left = 234
    Top = 428
    object QrCTDS_NHOMTHUE: TWideStringField
      FieldKind = fkCalculated
      FieldName = 'DS_NHOMTHUE'
      Size = 50
      Calculated = True
    end
    object QrCTNGAY: TDateTimeField
      FieldName = 'NGAY'
    end
    object QrCTSOTIEN: TFloatField
      FieldName = 'SOTIEN'
    end
    object QrCTTHUNGAN: TIntegerField
      FieldName = 'THUNGAN'
    end
    object QrCTSCT: TWideStringField
      FieldName = 'SCT'
    end
    object QrCTTHUNGAN_DGIAI: TWideStringField
      FieldKind = fkLookup
      FieldName = 'THUNGAN_DGIAI'
      LookupDataSet = DataMain.QrUSER
      LookupKeyFields = 'USERNAME'
      LookupResultField = 'FULLNAME'
      KeyFields = 'THUNGAN'
      Size = 50
      Lookup = True
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTKHOA_CT: TGuidField
      FieldName = 'KHOA_CT'
      OnChange = QrCTKHOA_CTChange
      FixedChar = True
      Size = 38
    end
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 412
    Top = 256
    object LydanhschBilltheongy1: TMenuItem
      Action = CmdGetBill
    end
  end
  object QrNHOMTHUE: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    AfterInsert = QrCTAfterEdit
    AfterEdit = QrCTAfterEdit
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'HOADON_GTGT_NHOMTHUE'
      ' where  '#9'KHOA =:KHOA ')
    Left = 276
    Top = 428
    object QrNHOMTHUEDGIAI: TWideStringField
      FieldName = 'DGIAI'
      Size = 200
    end
    object QrNHOMTHUESOHDON: TWideStringField
      FieldName = 'SOHDON'
      Size = 15
    end
    object QrNHOMTHUESERI: TWideStringField
      FieldName = 'SERI'
      Size = 10
    end
    object QrNHOMTHUENHOMTHUE: TWideStringField
      FieldName = 'NHOMTHUE'
      Size = 15
    end
    object QrNHOMTHUEKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrNHOMTHUEKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
  end
  object DsNHOMTHUE: TDataSource
    DataSet = QrNHOMTHUE
    Left = 276
    Top = 456
  end
  object PopNhomthue: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 442
    Top = 256
    object MenuItem1: TMenuItem
      Action = CmdUpdateGroupTax
    end
  end
  object GET_BILL_INFO: TADOCommand
    CommandText = 'HOADONGTGT_GETBILL;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Direction = pdInputOutput
        Value = Null
      end
      item
        Name = '@SCT'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 20
        Value = Null
      end
      item
        Name = '@SOTIEN'
        Attributes = [paNullable]
        DataType = ftFloat
        Direction = pdInputOutput
        Precision = 15
        Value = Null
      end
      item
        Name = '@CREATE_BY'
        Attributes = [paNullable]
        DataType = ftInteger
        Direction = pdInputOutput
        Precision = 10
        Value = Null
      end>
    Left = 480
    Top = 428
  end
  object QrDmKho: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'DM_KHO'
      'order by'#9'MAKHO'
      '')
    Left = 340
    Top = 428
    object QrDmKhoMAKHO: TWideStringField
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrDmKhoTENKHO: TWideStringField
      FieldName = 'TENKHO'
      Size = 100
    end
    object QrDmKhoDIACHI: TWideStringField
      FieldName = 'DIACHI'
      Size = 100
    end
  end
  object DsKho: TDataSource
    DataSet = QrDmKho
    Left = 340
    Top = 456
  end
  object DS_NHOMTHUE: TADOCommand
    CommandText = 'HOADONGTGT_DS_NHOMTHUE;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end
      item
        Name = '@NGAY'
        Attributes = [paNullable]
        DataType = ftDateTime
        Value = Null
      end
      item
        Name = '@DS_NHOMTHUE'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 50
        Value = Null
      end
      item
        Name = '@DGIAI'
        Attributes = [paNullable]
        DataType = ftWideString
        Direction = pdInputOutput
        Size = 200
        Value = Null
      end>
    Left = 449
    Top = 428
  end
  object CAPNHAT_HDGTGT_HH: TADOCommand
    CommandText = 'CAPNHAT_HDGTGT_HH;1'
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 600
    Top = 428
  end
end
