object FrmMain: TFrmMain
  Left = 162
  Top = 129
  Caption = 'ErpZ Solutions'
  ClientHeight = 575
  ClientWidth = 1173
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnActivate = FormActivate
  OnClose = FormClose
  OnResize = TntFormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object CoolBar1: TCoolBar
    Left = 0
    Top = 0
    Width = 1173
    Height = 115
    AutoSize = True
    Bands = <
      item
        Control = ToolBar1
        ImageIndex = -1
        MinHeight = 21
        Width = 1171
      end
      item
        Control = ToolBar2
        ImageIndex = -1
        MinHeight = 92
        Width = 1171
      end>
    EdgeBorders = []
    object ToolBar1: TToolBar
      Left = 11
      Top = 0
      Width = 1162
      Height = 21
      AutoSize = True
      ButtonHeight = 21
      ButtonWidth = 51
      Caption = 'ToolBar1'
      Font.Charset = ANSI_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ShowCaptions = True
      TabOrder = 0
      object ToolButton3: TToolButton
        Left = 0
        Top = 0
        Caption = 'H'#7879' th'#7889'ng'
        DropdownMenu = PopHethong
        Grouped = True
      end
    end
    object ToolBar2: TToolBar
      Left = 11
      Top = 23
      Width = 1162
      Height = 92
      AutoSize = True
      ButtonHeight = 46
      ButtonWidth = 111
      Caption = 'ToolBar2'
      Color = 16119285
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Images = ImgLarge
      List = True
      ParentColor = False
      ParentFont = False
      ShowCaptions = True
      TabOrder = 1
      object BtKinhdoanh: TToolButton
        Left = 0
        Top = 0
        Cursor = 1
        Hint = 
          'Qu'#7843'n l'#253' h'#224'ng h'#243'a|Nh'#243'm c'#225'c ch'#7913'c n'#259'ng nh'#7853'p, xu'#7845't, v'#7853'n chuy'#7875'n h'#224'ng ' +
          'h'#243'a...'
        Caption = 'Kinh doanh'
        DropdownMenu = PopKinhdoanh
        ImageIndex = 0
        ParentShowHint = False
        ShowHint = True
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object BtSanxuat: TToolButton
        Left = 136
        Top = 0
        Cursor = 1
        Hint = 'Qu'#7843'n l'#253' s'#7843'n xu'#7845't|Nh'#243'm ch'#7913'c n'#259'ng quy tr'#236'nh s'#7843'n xu'#7845't, gi'#225' th'#224'nh...'
        Caption = 'S'#7843'n xu'#7845't'
        DropdownMenu = PopSanxuat
        ImageIndex = 19
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object ToolButton1: TToolButton
        Left = 272
        Top = 0
        Cursor = 1
        Hint = 'Qu'#7843'n l'#253' nh'#224' h'#224'ng'
        Caption = 'Nh'#224' h'#224'ng'
        DropdownMenu = PopFnB
        ImageIndex = 21
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object BtNhansu: TToolButton
        Left = 408
        Top = 0
        Cursor = 1
        Hint = 'Qu'#7843'n l'#253' nh'#226'n s'#7921'|Nh'#243'm c'#225'c ch'#7913'c n'#259'ng ch'#7845'm c'#244'ng, t'#237'nh l'#432#417'ng...'
        Caption = 'Nh'#226'n s'#7921
        DropdownMenu = PopNhansu
        ImageIndex = 15
        ParentShowHint = False
        Wrap = True
        ShowHint = True
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object BtKetoan: TToolButton
        Left = 0
        Top = 46
        Cursor = 1
        Hint = 
          'Qu'#7843'n l'#253' k'#7871' to'#225'n|Nh'#243'm c'#225'c ch'#7913'c n'#259'ng v'#7873' thu ti'#7873'n, chi c'#244'ng n'#7907', chi' +
          ' ph'#237' kh'#225'c...'
        Caption = 'K'#7871' to'#225'n'
        DropdownMenu = PopKetoan
        ImageIndex = 18
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object BtDanhmuc: TToolButton
        Left = 136
        Top = 46
        Cursor = 1
        Hint = 
          'C'#225'c danh m'#7909'c|C'#7853'p nh'#7853't danh m'#7909'c h'#224'ng h'#243'a, kh'#225'ch h'#224'ng, nh'#224' cung c'#7845 +
          'p...'
        Caption = 'Danh m'#7909'c'
        DropdownMenu = PopDanhmuc
        ImageIndex = 13
        ParentShowHint = False
        ShowHint = True
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object BtInBarcode: TToolButton
        Left = 272
        Top = 46
        Cursor = 1
        Hint = 'In m'#227' v'#7841'ch'
        Caption = 'M'#227' v'#7841'ch'
        ImageIndex = 11
        OnClick = CmdInlabelExecute
      end
      object BtBaocao: TToolButton
        Left = 383
        Top = 46
        Cursor = 1
        Hint = 'Danh s'#225'ch b'#225'o c'#225'o, th'#7889'ng k'#234
        Caption = 'B'#225'o c'#225'o'
        DropdownMenu = PopBaocao
        ImageIndex = 3
        Style = tbsDropDown
        OnClick = CmdForceDropDownExecute
      end
      object ToolButton2: TToolButton
        Left = 519
        Top = 46
        Cursor = 1
        Hint = '|K'#7871't th'#250'c ch'#432#417'ng tr'#236'nh'
        Action = CmdQuit
        ImageIndex = 4
        ParentShowHint = False
        ShowHint = True
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 115
    Width = 1173
    Height = 437
    Align = alClient
    BevelOuter = bvLowered
    TabOrder = 1
    object RzSizePanel1: TRzSizePanel
      Tag = 99
      Left = 1
      Top = 1
      Width = 315
      Height = 435
      Color = 16119285
      GradientColorStart = 16119285
      GradientColorStop = 16119285
      HotSpotHighlight = 11855600
      HotSpotVisible = True
      SizeBarWidth = 7
      TabOrder = 0
      object GbFunc: TRzGroupBar
        Tag = 99
        Left = 0
        Top = 0
        Width = 307
        Height = 435
        GradientColorStart = 16119285
        GradientColorStop = 16119285
        LargeImages = ImgLarge
        SmallImages = DataMain.ImageSmall
        GroupBorderSize = 8
        UniqueItemSelection = True
        Align = alClient
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentColor = False
        ParentFont = False
        PopupMenu = PopFunc
        TabOrder = 0
        object GrHanghoa: TRzGroup
          Tag = 99
          CaptionColor = clHighlightText
          CaptionColorDefault = False
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 0
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdDondhKH
              ImageIndex = 62
            end
            item
              Action = CmdDdhNCC
            end
            item
              Caption = '-'
            end
            item
              Action = CmdNhap
              ImageIndex = 7
            end
            item
              Action = CmdXuattra
            end
            item
              Caption = '-'
            end
            item
              Action = CmdXuat
              ImageIndex = 6
            end
            item
              Action = CmdNhaptra
            end
            item
              Caption = '-'
            end
            item
              Action = CmdNhapkhac
              ImageIndex = 63
            end
            item
              Action = CmdXuatkhac
            end
            item
              Caption = '-'
            end
            item
              Action = CmdChuyenDvt
            end
            item
              Action = CmdChuyenkho
              ImageIndex = 15
            end
            item
              Action = CmdKiemke
            end
            item
              Caption = '-'
            end
            item
              Action = CmdNhapDC
            end
            item
              Action = CmdXuatDC
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBanle
            end
            item
              Action = CmdNhaptrabl
            end
            item
              Action = CmdNhaptrabl2
            end
            item
              Caption = '-'
            end
            item
              Action = CmdKetCa
            end
            item
              Caption = '-'
            end
            item
              Action = CmdKhuyenmai
            end
            item
              Action = CmdDmvtBo
            end
            item
              Action = CmdKhuyenmai3
            end
            item
              Caption = '-'
            end
            item
              Action = CmdPhieuQuatang
            end
            item
              Caption = '-'
            end
            item
              Action = CmdTonkho
            end
            item
              Action = CmdTMDT
            end
            item
              Action = CmdBcKD
              Tag = 10
            end
            item
              Caption = '-'
            end>
          Opened = False
          OpenedHeight = 688
          DividerVisible = False
          SmallImages = DataMain.ImageSmall
          Caption = 'Kinh doanh'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrGiathanh: TRzGroup
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 19
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Caption = '-'
            end
            item
              Action = CmdGiathanh
            end
            item
              Action = CmdQuytrinhSX
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBcSX
            end>
          Opened = False
          OpenedHeight = 108
          DividerVisible = False
          Caption = 'S'#7843'n xu'#7845't'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrFB: TRzGroup
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 21
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdDondhNCCFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdNhapFB
            end
            item
              Action = CmdXuattraFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdNhapkhacFB
            end
            item
              Action = CmdXuatkhacFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDieukhoFB
            end
            item
              Action = CmdKiemkeFB
            end
            item
              Action = CmdCheBienFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdKhuyenmaiChietkhauFB
            end
            item
              Action = CmdKhuyemaiComboFB
            end
            item
              Action = CmdKhuyenmaidachieuFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdHoaDonBanLeFB
            end
            item
              Action = CmdNhapTraBanLeFB
            end
            item
              Action = CmdTonkhoFB
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBaocaoFB
            end>
          Opened = False
          OpenedHeight = 448
          DividerVisible = False
          Caption = 'Nh'#224' h'#224'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrCongno: TRzGroup
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 15
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdProfile
            end
            item
              Caption = '-'
            end
            item
              Action = CmdQuatrinh
            end
            item
              Action = CmdHrHopDongLaoDong
            end
            item
              Action = CmdKhenthuong
            end
            item
              Action = CmdThietbi
            end
            item
              Action = CmdCongtac
            end
            item
              Caption = '-'
            end
            item
              Action = CmdVangmat
            end
            item
              Action = CmdTangCa
            end
            item
              Action = CmdMangThai
            end
            item
              Action = CmdThaisan
            end
            item
              Action = CmdThoiviec
            end
            item
              Caption = '-'
            end
            item
              Action = CmdHrTuyendung
            end
            item
              Action = CmdDaotao
            end
            item
              Caption = '-'
            end
            item
              Action = CmdPhepnam
            end
            item
              Action = CmdNghiBu
            end
            item
              Caption = '-'
            end
            item
              Action = CmdDmCalamviec
            end
            item
              Action = CmdDmNgayle
            end
            item
              Action = CmdLichlamviec
            end
            item
              Action = CmdDmMCC
            end
            item
              Action = CmdVantay
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBangcong
            end
            item
              Action = CmdTinhluong
              Caption = 'B'#7843'ng t'#237'nh l'#432#417'ng'
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBcNS
            end
            item
              Action = CmdBcNSCong
            end>
          Opened = False
          OpenedHeight = 648
          DividerVisible = False
          Caption = 'Nh'#226'n s'#7921
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrBanle: TRzGroup
          CaptionFont.Charset = DEFAULT_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 18
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdThu
            end
            item
              Action = CmdThu2
            end
            item
              Action = CmdThuHT
            end
            item
              Action = CmdThuThungan
            end
            item
              Action = CmdThukhac
            end
            item
              Caption = '-'
            end
            item
              Action = CmdChi
            end
            item
              Action = CmdChi2
            end
            item
              Action = CmdChiHT
            end
            item
              Action = CmdChiThungan
            end
            item
              Action = CmdChikhac
            end
            item
              Caption = '-'
            end
            item
              Action = CmdKetchuyen
            end
            item
              Action = CmdHoadonGTGT
            end
            item
              Caption = '-'
            end
            item
              Action = CmdCongnoKH
            end
            item
              Action = CmdCongnoNCC
            end
            item
              Caption = '-'
            end
            item
              Action = CmdBcKT
            end>
          Opened = False
          OpenedHeight = 408
          DividerVisible = False
          Caption = 'K'#7871' to'#225'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrHethong: TRzGroup
          Tag = 99
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 8
          CaptionStyle = csLarge
          Color = clRed
          ColorDefault = False
          Items = <
            item
              Action = CmdConfig
            end
            item
              Action = CmdCounter
            end
            item
              Caption = '-'
            end
            item
              Action = CmdKhoaso
            end
            item
              Caption = '-'
            end
            item
              Action = CmdSetpass
            end>
          Opened = False
          OpenedHeight = 148
          DividerVisible = False
          Caption = 'H'#7879' th'#7889'ng'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
        object GrPhanQuyen: TRzGroup
          Tag = 99
          CaptionColorStop = 16119285
          CaptionFont.Charset = ANSI_CHARSET
          CaptionFont.Color = clHighlight
          CaptionFont.Height = -11
          CaptionFont.Name = 'Tahoma'
          CaptionFont.Style = [fsBold]
          CaptionImageIndex = 8
          CaptionStyle = csLarge
          Color = clBtnFace
          ColorDefault = False
          Items = <
            item
              Action = CmdListUser
            end
            item
              Action = CmdListGroup
            end
            item
              Caption = '-'
            end
            item
              Action = CmdFunctionRight
            end
            item
              Action = CmdReportRight
              ImageIndex = 60
            end
            item
              Action = CmdDataRight
            end>
          Opened = False
          OpenedHeight = 128
          DividerVisible = False
          Caption = 'Ph'#226'n Quy'#7873'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentColor = False
          ParentFont = False
        end
      end
    end
    object PaBkGr: TPanel
      Tag = 99
      Left = 316
      Top = 1
      Width = 856
      Height = 435
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
      object BkGr: TfcImager
        Left = 0
        Top = 285
        Width = 856
        Height = 150
        Align = alClient
        AutoSize = False
        BitmapOptions.AlphaBlend.Amount = 0
        BitmapOptions.AlphaBlend.Transparent = False
        BitmapOptions.Color = clNone
        BitmapOptions.Contrast = 0
        BitmapOptions.Embossed = False
        BitmapOptions.TintColor = clNone
        BitmapOptions.GaussianBlur = 0
        BitmapOptions.GrayScale = False
        BitmapOptions.HorizontallyFlipped = False
        BitmapOptions.Inverted = False
        BitmapOptions.Lightness = 0
        BitmapOptions.Rotation.CenterX = -1
        BitmapOptions.Rotation.CenterY = -1
        BitmapOptions.Rotation.Angle = 0
        BitmapOptions.Saturation = -1
        BitmapOptions.Sharpen = 0
        BitmapOptions.Sponge = 0
        BitmapOptions.VerticallyFlipped = False
        BitmapOptions.Wave.XDiv = 0
        BitmapOptions.Wave.YDiv = 0
        BitmapOptions.Wave.Ratio = 0
        BitmapOptions.Wave.Wrap = False
        DrawStyle = dsProportionalCenter
        PreProcess = True
        SmoothStretching = False
        Transparent = True
        TransparentColor = clNone
        Visible = False
        TabOrder = 0
        ExplicitLeft = -2
        ExplicitTop = 291
        ExplicitWidth = 802
      end
      object PaDesc: TPanel
        Tag = 99
        Left = 0
        Top = 0
        Width = 856
        Height = 285
        Align = alTop
        BevelOuter = bvNone
        TabOrder = 0
        object LbDesc2: TLabel
          Left = 0
          Top = 178
          Width = 856
          Height = 33
          Align = alTop
          Alignment = taCenter
          Caption = 'DESC2'
          Font.Charset = ANSI_CHARSET
          Font.Color = 13360356
          Font.Height = -27
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          WordWrap = True
          ExplicitWidth = 89
        end
        object Bevel3: TBevel
          Left = 0
          Top = 164
          Width = 856
          Height = 14
          Align = alTop
          Shape = bsSpacer
          ExplicitWidth = 554
        end
        object LbDesc1: TLabel
          Left = 0
          Top = 80
          Width = 856
          Height = 84
          Align = alTop
          Alignment = taCenter
          AutoSize = False
          Caption = 'DESC1'
          Font.Charset = ANSI_CHARSET
          Font.Color = 13360356
          Font.Height = -32
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Transparent = True
          ExplicitWidth = 554
        end
        object Bevel2: TBevel
          Left = 0
          Top = 0
          Width = 856
          Height = 80
          Align = alTop
          Shape = bsSpacer
          ExplicitWidth = 554
        end
      end
    end
  end
  object Status: TfcStatusBar
    Left = 0
    Top = 552
    Width = 1173
    Height = 23
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageSmall
    Panels = <
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel0'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '15'
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Hint = 'Double click '#273#7875' ch'#7881'nh ng'#224'y, gi'#7901
        Name = 'Panel1'
        Style = psDateTime
        Tag = 0
        Text = '5/26/2024 9:27 AM'
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '197'
        OnDblClick = CmdSetDateTimeExecute
      end
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Hint = 'Double click '#273#7875' ch'#7881'nh ng'#224'y, gi'#7901
        Name = 'Panel2'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '15'
        OnDblClick = CmdSetDateTimeExecute
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = -1
        Name = 'Panel3'
        Style = psHintContainerOnly
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '24'
      end
      item
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Name = 'Panel4'
        Tag = 0
        TextOptions.Alignment = taCenter
        TextOptions.VAlignment = vaVCenter
        Width = '280'
      end
      item
        Bevel = pbNone
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ImageIndex = -1
        Name = 'Panel5'
        Style = psGlyph
        Tag = 0
        TextOptions.Alignment = taLeftJustify
        TextOptions.VAlignment = vaVCenter
        Width = '24'
        OnClick = CmdReconnectExecute
      end>
    ParentFont = False
    ParentShowHint = False
    ShowHint = True
    SimplePanel = False
    StatusBarText.CapsLock = 'Caps'
    StatusBarText.Overwrite = 'Overwrite'
    StatusBarText.NumLock = 'Num'
    StatusBarText.ScrollLock = 'Scroll'
  end
  object MyActionList: TActionList
    Images = DataMain.ImageSmall
    OnExecute = MyActionListExecute
    Left = 380
    Top = 276
    object CmdNhap: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Phi'#7871'u nh'#7853'p'
      Hint = '@SZ_NHAP'
      ShortCut = 113
      OnExecute = CmdNhapExecute
    end
    object CmdThu: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u thu ti'#7873'n kh'#225'ch h'#224'ng'
      Hint = '@ACC_THU'
      OnExecute = CmdThuExecute
    end
    object CmdThuHT: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u thu h'#7895' tr'#7907
      Hint = '@ACC_THU_PHIEUHT'
      OnExecute = CmdThuHTExecute
    end
    object CmdThuThungan: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u thu ti'#7873'n nh'#226'n vi'#234'n thu ng'#226'n'
      Hint = '@ACC_THU_THUNGAN'
      OnExecute = CmdThuThunganExecute
    end
    object CmdDmHotroKT: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' k'#7871' to'#225'n'
      Hint = '@ACC_DM_HOTRO'
      OnExecute = CmdDmHotroKTExecute
    end
    object CmdDmVangmat: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'L'#253' do v'#7855'ng m'#7863't'
      Hint = '@HR_DM_VANGMAT'
      OnExecute = CmdDmVangmatExecute
    end
    object CmdQuit: TAction
      Category = 'HETHONG'
      Caption = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdQuitExecute
    end
    object CmdNhapDC: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = #272'i'#7873'u ch'#7881'nh phi'#7871'u nh'#7853'p'
      Hint = '@SZ_NHAP_DC'
      OnExecute = CmdNhapDCExecute
    end
    object CmdXuat: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Phi'#7871'u xu'#7845't'
      Hint = '@SZ_XUAT'
      ShortCut = 114
      OnExecute = CmdXuatExecute
    end
    object CmdXuatDC: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = #272'i'#7873'u ch'#7881'nh phi'#7871'u xu'#7845't'
      Hint = '@SZ_XUAT_DC'
      OnExecute = CmdXuatDCExecute
    end
    object CmdSetpass: TAction
      Category = 'HETHONG'
      Caption = #208#7893'i m'#7853't kh'#7849'u'
      ImageIndex = 9
      OnExecute = CmdSetpassExecute
    end
    object CmdAbout: TAction
      Caption = 'Gi'#7899'i thi'#7879'u...'
      OnExecute = CmdAboutExecute
    end
    object CmdParams: TAction
      Category = 'HETHONG'
      Caption = 'Config'
      Hint = '@SZ_TSO'
      ImageIndex = 8
      OnExecute = CmdParamsExecute
    end
    object CmdTonkhoDK: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'T'#7891'n kho '#273#7847'u k'#7923
      Hint = '@SZ_TONKHO_DK'
      OnExecute = CmdTonkhoDKExecute
    end
    object CmdDmnhom: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Ng'#224'nh nh'#243'm h'#224'ng h'#243'a'
      Hint = '@SZ_DM_NGANH'
      ImageIndex = 14
      OnExecute = CmdDmnhomExecute
    end
    object CmdDmvt: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'H'#224'ng h'#243'a'
      Hint = '@SZ_DM_MAVT'
      ImageIndex = 42
      OnExecute = CmdDmvtExecute
    end
    object CmdDmvtBo: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Khuy'#7871'n m'#227'i - H'#224'ng h'#243'a b'#7897
      Hint = '@SZ_KHUYENMAI_HANGHOABO'
      OnExecute = CmdDmvtBoExecute
    end
    object CmdDmncc: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Nh'#224' cung c'#7845'p'
      Hint = '@SZ_DM_NCC'
      ImageIndex = 51
      OnExecute = CmdDmnccExecute
    end
    object CmdDmkhac: TAction
      Category = 'DANHMUC'
      Caption = 'C'#225'c quy '#432#7899'c'
      Hint = '@SZ_PUB_DM_QUYUOC'
      OnExecute = CmdDmkhacExecute
    end
    object CmdKhoaso: TAction
      Category = 'HETHONG'
      Caption = 'Kh'#243'a s'#7893
      Hint = '@SZ_KHOASO'
      ImageIndex = 48
      OnExecute = CmdKhoasoExecute
    end
    object CmdDmkh: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Kh'#225'ch h'#224'ng'
      Hint = '@SZ_DM_KHACHHANG'
      OnExecute = CmdDmkhExecute
    end
    object CmdChi: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u chi ti'#7873'n nh'#224' cung c'#7845'p'
      Hint = '@ACC_CHI'
      OnExecute = CmdChiExecute
    end
    object CmdBc: TAction
      Caption = 'B'#225'o c'#225'o'
      Hint = 'X'#7917' l'#253', xem in b'#225'o c'#225'o, th'#7889'ng k'#234'...'
      OnExecute = CmdBcExecute
    end
    object CmdBanle: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'H'#243'a '#273#417'n b'#225'n l'#7867
      Hint = '@SZ_SUABILL'
      ImageIndex = 56
      OnExecute = CmdBanleExecute
    end
    object CmdNhapkhac: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Nh'#7853'p kh'#225'c'
      Hint = '@SZ_NHAPKHAC'
      OnExecute = CmdNhapkhacExecute
    end
    object CmdXuatkhac: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Xu'#7845't kh'#225'c'
      Hint = '@SZ_XUATKHAC'
      OnExecute = CmdXuatkhacExecute
    end
    object CmdDmkhac3: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c kh'#225'c'
      Hint = '@SZ_PUB_DM_KHAC'
      ImageIndex = 32
      OnExecute = CmdDmkhac3Execute
    end
    object CmdForceDropDown: TAction
      OnExecute = CmdForceDropDownExecute
    end
    object CmdNhaptra: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Nh'#7853'p tr'#7843
      Hint = '@SZ_NHAPTRA'
      OnExecute = CmdNhaptraExecute
    end
    object CmdXuattra: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Xu'#7845't tr'#7843
      Hint = '@SZ_XUATTRA'
      OnExecute = CmdXuattraExecute
    end
    object CmdChuyenkho: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Phi'#7871'u '#273'i'#7873'u kho'
      Hint = '@SZ_CKHO'
      ShortCut = 115
      OnExecute = CmdChuyenkhoExecute
    end
    object CmdDmkho: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c kho'
      Hint = '@SZ_PUB_DM_KHO'
      ImageIndex = 40
      OnExecute = CmdDmkhoExecute
    end
    object CmdInlabel: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'In m'#227' v'#7841'ch, tem nh'#227'n'
      Hint = '@SZ_STAMP'
      ImageIndex = 10
      OnExecute = CmdInlabelExecute
    end
    object CmdChiHT: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u chi h'#7895' tr'#7907
      Hint = '@ACC_CHI_PHIEUHT'
      OnExecute = CmdChiHTExecute
    end
    object CmdHelp: TAction
      Caption = 'H'#432#7899'ng d'#7851'n s'#7917' d'#7909'ng'
      ShortCut = 112
      OnExecute = CmdHelpExecute
    end
    object CmdSetBkgr: TAction
      Category = 'HINHNEN'
      Caption = 'G'#7855'n h'#236'nh n'#7873'n'
      ImageIndex = 2
      OnExecute = CmdSetBkgrExecute
    end
    object CmdClearBkgr: TAction
      Category = 'HINHNEN'
      Caption = 'X'#243'a h'#236'nh n'#7873'n'
      OnExecute = CmdClearBkgrExecute
    end
    object CmdBkgrOption: TAction
      Category = 'HINHNEN'
      Caption = 'Canh ch'#7881'nh'
      ImageIndex = 8
      OnExecute = CmdBkgrOptionExecute
    end
    object CmdKiemke: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Ki'#7875'm k'#234' kho'
      Hint = '@SZ_KIEMKE'
      OnExecute = CmdKiemkeExecute
    end
    object CmdSetDateTime: TAction
      Hint = 'Ch'#7881'nh l'#7841'i ng'#224'y, gi'#7901
      OnExecute = CmdSetDateTimeExecute
    end
    object CmdDmVIP: TAction
      Category = 'DANHMUC'
      Caption = 'Kh'#225'ch h'#224'ng th'#226'n thi'#7871't'
      Hint = '@SZ_PUB_DM_VIP'
      ImageIndex = 43
      OnExecute = CmdDmVIPExecute
    end
    object CmdDmmau: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c m'#224'u'
      Hint = '@SZ_PUB_DM_MAU'
      ImageIndex = 41
      OnExecute = CmdDmmauExecute
    end
    object CmdDmsize: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c size'
      Hint = '@SZ_PUB_DM_SIZE'
      OnExecute = CmdDmsizeExecute
    end
    object CmdDdhNCC: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = #208#417'n '#273#7863't h'#224'ng nh'#224' cung c'#7845'p'
      Hint = '@SZ_DDH_NCC'
      OnExecute = CmdDdhNCCExecute
    end
    object CmdCounter: TAction
      Category = 'HETHONG'
      Caption = 'Qu'#7847'y thu ng'#226'n'
      Hint = '@SZ_QUAYTN'
      ImageIndex = 56
      OnExecute = CmdCounterExecute
    end
    object CmdLock: TAction
      Caption = 'Kh'#243'a ch'#432#417'ng tr'#236'nh'
      ShortCut = 49228
      OnExecute = CmdLockExecute
    end
    object CmdFaq: TAction
      Caption = 'C'#225'c h'#7887'i '#273#225'p th'#432#7901'ng g'#7863'p'
      OnExecute = CmdFaqExecute
    end
    object CmdMyCustomer: TAction
      Caption = 'Danh s'#225'ch kh'#225'ch h'#224'ng'
      OnExecute = CmdMyCustomerExecute
    end
    object CmdDmdialy: TAction
      Category = 'DANHMUC'
      Caption = 'V'#7883' tr'#237' '#273#7883'a l'#253
      Hint = '@SZ_PUB_DM_DIALY'
      ImageIndex = 35
      OnExecute = CmdDmdialyExecute
    end
    object CmdTygia: TAction
      Category = 'DANHMUC'
      Caption = 'Ngo'#7841'i t'#7879'/ T'#7927' gi'#225
      Hint = '@SZ_PUB_DM_TYGIA'
      ImageIndex = 45
      OnExecute = CmdTygiaExecute
    end
    object CmdDondhKH: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = #208#417'n '#273#7863't h'#224'ng kh'#225'ch h'#224'ng'
      Hint = '@SZ_DDH_KH'
      OnExecute = CmdDondhKHExecute
    end
    object CmdNhaptrabl: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Nh'#7853'p tr'#7843' b'#225'n l'#7867' - h'#243'a '#273#417'n'
      Hint = '@SZ_NHAPTRA_BL'
      OnExecute = CmdNhaptrablExecute
    end
    object CmdHoadonGTGT: TAction
      Category = 'KETOAN'
      Caption = 'H'#243'a '#273#417'n gi'#225' tr'#7883' gia t'#259'ng'
      Hint = '@ACC_HOADON_GTGT'
      OnExecute = CmdHoadonGTGTExecute
    end
    object CmdChiThungan: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u chi ti'#7873'n nh'#226'n vi'#234'n thu ng'#226'n'
      Hint = '@ACC_CHI_THUNGAN'
      OnExecute = CmdChiThunganExecute
    end
    object CmdChikhac: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u chi kh'#225'c'
      Hint = '@ACC_CHI_KHAC'
      OnExecute = CmdChikhacExecute
    end
    object CmdKetchuyen: TAction
      Category = 'KETOAN'
      Caption = 'K'#7871't chuy'#7875'n ti'#7873'n'
      Hint = '@ACC_KETCHUYEN'
      OnExecute = CmdKetchuyenExecute
    end
    object CmdThukhac: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u thu kh'#225'c'
      Hint = '@ACC_THU_KHAC'
      OnExecute = CmdThukhacExecute
    end
    object CmdKhuyenmai: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Khuy'#7871'n m'#227'i - Chi'#7871't kh'#7845'u, '#273#7891'ng gi'#225
      Hint = '@SZ_KHUYENMAI'
      ImageIndex = 18
      OnExecute = CmdKhuyenmaiExecute
    end
    object CmdReconnect: TAction
      Category = 'HETHONG'
      Caption = 'K'#7871't n'#7889'i c'#417' s'#7903' d'#7919' li'#7879'u'
      ImageIndex = 57
      OnExecute = CmdReconnectExecute
    end
    object CmdTonkho: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'T'#7891'n kho'
      Hint = '@SZ_TONKHO'
      OnExecute = CmdTonkhoExecute
    end
    object CmdExport: TAction
      Caption = 'Xu'#7845't Excel'
      Hint = 'B'#225'o c'#225'o '#273#7897'ng, xu'#7845't ra Excel'
      OnExecute = CmdExportExecute
    end
    object CmdKhuyenmai3: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Khuy'#7871'n m'#227'i - '#272'a chi'#7873'u'
      Hint = '@SZ_KHUYENMAI_DACHIEU'
      OnExecute = CmdKhuyenmai3Execute
    end
    object CmdNhaptrabl2: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Nh'#7853'p tr'#7843' b'#225'n l'#7867
      Hint = '@SZ_NHAPTRA_BL2'
      OnExecute = CmdNhaptrabl2Execute
    end
    object CmdLoaiVipCT: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Chi'#7871't kh'#7845'u th'#7867' VIP'
      Hint = '@SZ_DM_LOAIVIP_CT'
      OnExecute = CmdLoaiVipCTExecute
    end
    object CmdDmTaikhoan: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
      Hint = '@ACC_DM_TAIKHOAN_NH'
      OnExecute = CmdDmTaikhoanExecute
    end
    object CmdThu2: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u thu ti'#7873'n kh'#225'ch h'#224'ng - Theo phi'#7871'u'
      Hint = '@ACC_THU_PHIEU'
      OnExecute = CmdThu2Execute
    end
    object CmdChi2: TAction
      Category = 'KETOAN'
      Caption = 'Phi'#7871'u chi ti'#7873'n nh'#224' cung c'#7845'p - Theo phi'#7871'u'
      Hint = '@ACC_CHI_PHIEU'
      OnExecute = CmdChi2Execute
    end
    object CmdListUser: TAction
      Category = 'PHANQUYEN'
      Caption = 'Danh s'#225'ch t'#224'i kho'#7843'n'
      OnExecute = CmdListUserExecute
    end
    object CmdListGroup: TAction
      Category = 'PHANQUYEN'
      Caption = 'Danh s'#225'ch nh'#243'm t'#224'i kho'#7843'n'
      OnExecute = CmdListGroupExecute
    end
    object CmdFunctionRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n ch'#7913'c n'#259'ng'
      ImageIndex = 43
      OnExecute = CmdFunctionRightExecute
    end
    object CmdReportRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n b'#225'o c'#225'o'
      OnExecute = CmdReportRightExecute
    end
    object CmdFlexsible: TAction
      Category = 'SOFTZ'
      Caption = 'Flexsible settings'
      ImageIndex = 4
    end
    object CmdCustomGrid: TAction
      Category = 'SOFTZ'
      Caption = 'Customize grids'
    end
    object CmdDictionary: TAction
      Category = 'SOFTZ'
      Caption = 'Dictionary'
    end
    object CmdFuncAdmin: TAction
      Category = 'SOFTZ'
      Caption = 'Danh s'#225'ch ch'#7913'c n'#259'ng'
    end
    object CmdRepAdmin: TAction
      Category = 'SOFTZ'
      Caption = 'Danh s'#225'ch b'#225'o c'#225'o'
    end
    object CmdAdmin: TAction
      Category = 'SOFTZ'
      ShortCut = 16507
    end
    object CmdDmKhNcc: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Kh'#225'ch h'#224'ng/ Nh'#224' cung c'#7845'p'
      Hint = '@SZ_DM_KH_NCC'
      OnExecute = CmdDmKhNccExecute
    end
    object CmdLoaiVIP: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i th'#7867' VIP'
      Hint = '@SZ_PUB_DM_LOAIVIP'
      OnExecute = CmdLoaiVIPExecute
    end
    object CmdLoaiVip2: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Ph'#226'n c'#7845'p kh'#225'ch h'#224'ng'
      Hint = '@SZ_DM_LOAIVIP_KH'
      OnExecute = CmdLoaiVip2Execute
    end
    object CmdLoaiVipCt2: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Chi'#7871't kh'#7845'u theo ph'#226'n c'#7845'p kh'#225'ch h'#224'ng'
      Hint = '@SZ_DM_LOAIVIP_KH_CT'
      OnExecute = CmdLoaiVipCt2Execute
    end
    object CmdReportEncrypt: TAction
      Category = 'SOFTZ'
      Caption = 'M'#227' h'#243'a reports'
    end
    object CmdExOption: TAction
      Category = 'SOFTZ'
      Caption = 'Xu'#7845't ra file '#8211' Export setting'
    end
    object CmdImOption: TAction
      Category = 'SOFTZ'
      Caption = 'L'#7845'y t'#7915' file '#8211' Import setting'
    end
    object CmdConfig: TAction
      Category = 'HETHONG'
      Caption = 'C'#7845'u h'#236'nh h'#7879' th'#7889'ng'
      Hint = '@SZ_TSO'
      ImageIndex = 8
      OnExecute = CmdConfigExecute
    end
    object CmdDmNganhang: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'Ng'#226'n h'#224'ng/ Chi nh'#225'nh'
      Hint = '@SZ_PUB_DM_NGANHANG'
      OnExecute = CmdDmNganhangExecute
    end
    object CmdCongnoKH: TAction
      Category = 'KETOAN'
      Caption = 'C'#244'ng n'#7907' Kh'#225'ch h'#224'ng'
      Hint = '@ACC_CONGNO_KH'
      OnExecute = CmdCongnoKHExecute
    end
    object CmdCongnoNCC: TAction
      Category = 'KETOAN'
      Caption = 'C'#244'ng n'#7907' Nh'#224' cung c'#7845'p'
      Hint = '@ACC_CONGNO_NCC'
      OnExecute = CmdCongnoNCCExecute
    end
    object CmdDmHotro: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' kinh doanh'
      Hint = '@SZ_DM_HOTRO'
      OnExecute = CmdDmHotroExecute
    end
    object CmdPhieuQuatang: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Th'#7867' qu'#224' t'#7863'ng'
      Hint = '@SZ_PHIEU_QUATANG'
      OnExecute = CmdPhieuQuatangExecute
    end
    object CmdDmChiphiPOM: TAction
      Category = 'GIATHANH'
      Caption = 'Danh s'#225'ch chi ph'#237' s'#7843'n xu'#7845't'
      Hint = '@POM_DM_CHIPHI'
      OnExecute = CmdDmChiphiPOMExecute
    end
    object CmdDmhhNhom_POM: TAction
      Category = 'GIATHANH'
      Caption = 'Nh'#243'm NL/ BTP/ TP'
      Hint = '@POM_DM_NGANH'
      OnExecute = CmdDmhhNhom_POMExecute
    end
    object CmdDmHesoDodam: TAction
      Category = 'GIATHANH'
      Caption = 'H'#7879' s'#7889' '#273#7897' '#273#7841'm'
      Hint = '@POM_DM_HESO_DODAM'
      OnExecute = CmdDmHesoDodamExecute
    end
    object CmdDmHesoDungtich: TAction
      Category = 'GIATHANH'
      Caption = 'H'#7879' s'#7889' dung t'#237'ch'
      Hint = '@POM_DM_HESO_DUNGTICH'
      OnExecute = CmdDmHesoDungtichExecute
    end
    object CmdDmTP: TAction
      Category = 'GIATHANH'
      Caption = 'Th'#224'nh ph'#7849'm'
      Hint = '@POM_DM_TP'
      OnExecute = CmdDmTPExecute
    end
    object CmdDmNVL: TAction
      Category = 'GIATHANH'
      Caption = 'Nguy'#234'n li'#7879'u'
      Hint = '@POM_DM_NVL'
      OnExecute = CmdDmNVLExecute
    end
    object CmdDmBTP: TAction
      Category = 'GIATHANH'
      Caption = 'B'#225'n th'#224'nh ph'#7849'm'
      Hint = '@POM_DM_BTP'
      OnExecute = CmdDmBTPExecute
    end
    object CmdDmPhongban: TAction
      Category = 'DANHMUC'
      Caption = 'T'#7893' ch'#7913'c ph'#242'ng ban'
      Hint = '@SZ_PUB_DM_PHONGBAN'
      OnExecute = CmdDmPhongbanExecute
    end
    object CmdDmChucvu: TAction
      Category = 'DANHMUC'
      Caption = 'Ch'#7913'c danh/ Ch'#7913'c v'#7909
      Hint = '@SZ_PUB_DM_CHUCVU'
      OnExecute = CmdDmChucvuExecute
    end
    object CmdDmNhomlv: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Nh'#243'm l'#224'm vi'#7879'c'
      Hint = '@HR_DM_NHOMLV'
      OnExecute = CmdDmNhomlvExecute
    end
    object CmdDmPhucap: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Lo'#7841'i ph'#7909' c'#7845'p'
      Hint = '@HR_DM_PHUCAP'
      OnExecute = CmdDmPhucapExecute
    end
    object CmdDmQuatrinh: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Lo'#7841'i qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      Hint = '@HR_DM_QUATRINH_LV'
      OnExecute = CmdDmQuatrinhExecute
    end
    object CmdDmHopdong: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Lo'#7841'i h'#7907'p '#273#7891'ng lao '#273#7897'ng'
      Hint = '@HR_DM_HOPDONG'
      OnExecute = CmdDmHopdongExecute
    end
    object CmdDmDKKhambenh: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'N'#417'i kh'#225'm ch'#7919'a b'#7879'nh'
      Hint = '@HR_DM_DK_KHAMBENH'
      OnExecute = CmdDmDKKhambenhExecute
    end
    object CmdDmHotroSX: TAction
      Category = 'GIATHANH'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' s'#7843'n xu'#7845't'
      Hint = '@SZ_HOTRO'
      OnExecute = CmdDmHotroSXExecute
    end
    object CmdDmHotroNS: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' nh'#226'n s'#7921
      Hint = '@HR_DM_HOTRO'
      OnExecute = CmdDmHotroNSExecute
    end
    object CmdhethongTaikhoanKT: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'H'#7879' th'#7889'ng t'#224'i kho'#7843'n k'#7871' to'#225'n'
      Hint = '@ACC_DM_TAIKHOAN_TK'
      OnExecute = CmdhethongTaikhoanKTExecute
    end
    object CmdDmChiphiKT: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'Danh s'#225'ch chi ph'#237' k'#7871' to'#225'n'
      Hint = '@ACC_DM_CHIPHI'
      OnExecute = CmdDmChiphiKTExecute
    end
    object CmdBcSX: TAction
      Category = 'GIATHANH'
      Caption = 'B'#225'o c'#225'o s'#7843'n xu'#7845't'
      Hint = '@POM_REP'
      ImageIndex = 64
      OnExecute = CmdBcSXExecute
    end
    object CmdBcKD: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'B'#225'o c'#225'o kinh doanh'
      Hint = '@SZ_REP'
      ImageIndex = 64
      OnExecute = CmdBcKDExecute
    end
    object CmdBcNS: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'B'#225'o c'#225'o nh'#226'n s'#7921
      Hint = '@HR_NHANSU_REP'
      ImageIndex = 64
      OnExecute = CmdBcNSExecute
    end
    object CmdBcKT: TAction
      Category = 'KETOAN'
      Caption = 'B'#225'o c'#225'o k'#7871' to'#225'n'
      Hint = '@ACC_KETOAN_REP'
      ImageIndex = 64
      OnExecute = CmdBcKTExecute
    end
    object CmdDmnv: TAction
      Category = 'NHANSU'
      Caption = 'Danh m'#7909'c nh'#226'n vi'#234'n'
      Hint = '@HR_NHANVIEN'
      OnExecute = CmdDmnvExecute
    end
    object CmdProfile: TAction
      Category = 'NHANSU'
      Caption = 'H'#7891' s'#417' nh'#226'n vi'#234'n'
      Hint = '@HR_PROFILE'
      OnExecute = CmdProfileExecute
    end
    object CmdQuatrinh: TAction
      Category = 'NHANSU'
      Caption = 'Qu'#225' tr'#236'nh l'#224'm vi'#7879'c'
      Hint = '@HR_QUATRINH_LV'
      OnExecute = CmdQuatrinhExecute
    end
    object CmdKhenthuong: TAction
      Category = 'NHANSU'
      Caption = 'Khen th'#432#7903'ng - K'#7927' lu'#7853't'
      Hint = '@HR_KHENTHUONG_KL'
      OnExecute = CmdKhenthuongExecute
    end
    object CmdCongtac: TAction
      Category = 'NHANSU'
      Caption = #272'i c'#244'ng t'#225'c'
      Hint = '@HR_CONGTAC'
      OnExecute = CmdCongtacExecute
    end
    object CmdGiadinh: TAction
      Category = 'NHANSU'
      Caption = 'Quan h'#7879' gia '#273#236'nh'
      Enabled = False
      Hint = '@HR_GIADINH'
      OnExecute = CmdGiadinhExecute
    end
    object CmdThietbi: TAction
      Category = 'NHANSU'
      Caption = 'Thi'#7871't b'#7883' - T'#224'i s'#7843'n'
      Hint = '@HR_THIETBI_DUNGCU'
      OnExecute = CmdThietbiExecute
    end
    object CmdDaotao: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#224'o t'#7841'o'
      Hint = '@HR_DAOTAO'
      OnExecute = CmdDaotaoExecute
    end
    object CmdThoiviec: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' th'#244'i vi'#7879'c'
      Enabled = False
      Hint = '@HR_NGHIVIEC'
      OnExecute = CmdThoiviecExecute
    end
    object CmdTuyendungHoso: TAction
      Category = 'NHANSU'
      Caption = 'H'#7891' s'#417' '#7913'ng vi'#234'n'
      Enabled = False
      Hint = '@HR_TUYENDUNG_HOSO'
      OnExecute = CmdTuyendungHosoExecute
    end
    object CmdTuyendungKetqua: TAction
      Category = 'NHANSU'
      Caption = 'K'#7871't qu'#7843' ph'#7887'ng v'#7845'n'
      Enabled = False
      Hint = '@HR_TUYENDUNG_KETQUA'
      OnExecute = CmdTuyendungKetquaExecute
    end
    object CmdTMDT: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Li'#234'n k'#7871't s'#224'n th'#432#417'ng mai '#273'i'#7879'n t'#7917
      Hint = '@SZ_LIENKET_TMDT'
    end
    object CmdDmCalamviec: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'Ca l'#224'm vi'#7879'c'
      Hint = '@HR_CALAMVIEC'
      OnExecute = CmdDmCalamviecExecute
    end
    object CmdDmNgayle: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'C'#225'c ng'#224'y ngh'#7881' trong n'#259'm'
      Hint = '@HR_NGAYLE'
      OnExecute = CmdDmNgayleExecute
    end
    object CmdBangcong: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'B'#7843'ng ch'#7845'm c'#244'ng'
      Hint = '@HR_BANGCONG'
      OnExecute = CmdBangcongExecute
    end
    object CmdVangmat: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' v'#7855'ng m'#7863't'
      Hint = '@HR_VANGMAT'
      OnExecute = CmdVangmatExecute
    end
    object CmdPhepnam: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'Qu'#7843'n l'#253' ph'#233'p n'#259'm'
      Hint = '@HR_PHEPNAM'
      OnExecute = CmdPhepnamExecute
    end
    object CmdLichlamviec: TAction
      Category = 'NHANSU_DANGKY'
      Caption = 'L'#7883'ch l'#224'm vi'#7879'c'
      Hint = '@HR_LICHLAMVIEC'
      OnExecute = CmdLichlamviecExecute
    end
    object CmdThaisan: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' thai s'#7843'n'
      Hint = '@HR_THAISAN'
      OnExecute = CmdThaisanExecute
    end
    object CmdDmMCC: TAction
      Category = 'NHANSU'
      Caption = 'M'#225'y ch'#7845'm c'#244'ng'
      Hint = '@HR_MAYCC'
      OnExecute = CmdDmMCCExecute
    end
    object CmdVantay: TAction
      Category = 'NHANSU'
      Caption = 'Danh s'#225'ch m'#7851'u d'#7919' li'#7879'u ch'#7845'm c'#244'ng'
      Hint = '@HR_VANTAY'
      OnExecute = CmdVantayExecute
    end
    object CmdCongtho: TAction
      Category = 'NHANSU'
      Caption = 'D'#7919' li'#7879'u c'#244'ng th'#244
      Hint = '@TB_DULIEU_CONGTHO'
      OnExecute = CmdCongthoExecute
    end
    object CmdLaydulieucong: TAction
      Category = 'NHANSU'
      Caption = '.: L'#7845'y d'#7919' li'#7879'u t'#7915' m'#225'y ch'#7845'm c'#244'ng'
      Hint = '@TB_DULIEU_CONG'
      OnExecute = CmdLaydulieucongExecute
    end
    object CmdBangluong: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'B'#7843'ng t'#237'nh l'#432#417'ng'
      Hint = '@HR_BANGLUONG'
      OnExecute = CmdBangluongExecute
    end
    object CmdGiathanh: TAction
      Category = 'GIATHANH'
      Caption = 'Gi'#225' th'#224'nh'
      Hint = '@POM_GIATHANH'
      OnExecute = CmdGiathanhExecute
    end
    object CmdBcNSTuyendung: TAction
      Category = 'NHANSU'
      Caption = 'B'#225'o c'#225'o tuy'#7875'n d'#7909'ng'
      Hint = '@HR_TUYENDUNG_REP'
      ImageIndex = 64
      OnExecute = CmdBcNSTuyendungExecute
    end
    object CmdBcNSCong: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'B'#225'o c'#225'o c'#244'ng l'#432#417'ng'
      Hint = '@HR_CHAMCONG_REP'
      ImageIndex = 64
      OnExecute = CmdBcNSCongExecute
    end
    object CmdDmKetoan: TAction
      Category = 'KETOAN_DANHMUC'
      Caption = 'Danh m'#7909'c k'#7871' to'#225'n'
      Hint = '@ACC_DANHMUC'
      OnExecute = CmdDmKetoanExecute
    end
    object CmdDmNhansu: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Danh m'#7909'c nh'#226'n s'#7921
      Hint = '@HR_DANHMUC'
      OnExecute = CmdDmNhansuExecute
    end
    object CmdDmSanxuat: TAction
      Category = 'GIATHANH'
      Caption = 'Danh m'#7909'c s'#7843'n xu'#7845't'
      Hint = '@POM_DANHMUC'
      OnExecute = CmdDmSanxuatExecute
    end
    object CmdDmKinhdoanh: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Danh m'#7909'c kinh doanh'
      Hint = '@SZ_DANHMUC'
      OnExecute = CmdDmKinhdoanhExecute
    end
    object CmdQuytrinhSX: TAction
      Category = 'GIATHANH'
      Caption = 'Quy tr'#236'nh s'#7843'n xu'#7845't'
      Hint = '@POM_QUYTRINH'
      OnExecute = CmdQuytrinhSXExecute
    end
    object CmdHrLichsu: TAction
      Category = 'NHANSU'
      Caption = 'L'#7883'ch s'#7917
      Hint = '@HR_XULY'
      OnExecute = CmdHrLichsuExecute
    end
    object CmdHrThietbi: TAction
      Category = 'NHANSU'
      Caption = 'M'#225'y ch'#7845'm c'#244'ng'
      Hint = '@TB_THIETBI'
      OnExecute = CmdHrThietbiExecute
    end
    object CmdHrTuyendung: TAction
      Category = 'NHANSU'
      Caption = 'Tuy'#7875'n d'#7909'ng'
      Enabled = False
      Hint = '@HR_TUYENDUNG'
      OnExecute = CmdHrTuyendungExecute
    end
    object CmdUnlockChungtu: TAction
      Category = 'HETHONG'
      Caption = 'Unlock ch'#7913'ng t'#7915' b'#7883' treo'
      Hint = '@SZ_UNLOCK'
      OnExecute = CmdUnlockChungtuExecute
    end
    object CmdTbMaychamcong: TAction
      Category = 'NHANSU'
      Caption = 'M'#225'y ch'#7845'm c'#244'ng'
      Hint = '@TB_THIETBI'
      OnExecute = CmdTbMaychamcongExecute
    end
    object CmdDONDATHANG: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = #272#417'n '#273#7863't h'#224'ng'
      Hint = '@SZ_DONDATHANG'
      OnExecute = CmdDONDATHANGExecute
    end
    object CmdNHAPXUATKHO: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Nh'#7853'p - Xu'#7845't H'#224'ng h'#243'a'
      Hint = '@SZ_NHAPXUATKHO'
      OnExecute = CmdNHAPXUATKHOExecute
    end
    object CmdBANLEMENU: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'B'#225'n l'#7867
      Hint = '@SZ_BANLE'
      OnExecute = CmdBANLEMENUExecute
    end
    object CmdGrThu: TAction
      Category = 'KETOAN'
      Caption = 'Thu'
      Hint = '@ACC_CONGNO_THU'
      OnExecute = CmdGrThuExecute
    end
    object CmdGrChi: TAction
      Category = 'KETOAN'
      Caption = 'Chi'
      Hint = '@ACC_CONGNO_CHI'
      OnExecute = CmdGrChiExecute
    end
    object CmdDanhmuc: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c chung'
      Hint = '@SZ_PUB_DANHMUC'
      OnExecute = CmdDanhmucExecute
    end
    object CmdDmTaisan: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Danh m'#7909'c t'#224'i s'#7843'n'
      Hint = '@ASS_DANHMUC'
      OnExecute = CmdDmTaisanExecute
    end
    object CmdDmvtTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'T'#224'i s'#7843'n'
      Hint = '@ASS_DMVT'
      OnExecute = CmdDmvtTSExecute
    end
    object CmdDmnhomTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Nh'#243'm t'#224'i s'#7843'n'
      Hint = '@ASS_DM_NGANH'
      OnExecute = CmdDmnhomTSExecute
    end
    object CmdDmnccTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'NCC/ NSX/ '#272#7889'i t'#225'c BH, BT'
      Hint = '@ASS_DM_NCC'
      OnExecute = CmdDmnccTSExecute
    end
    object CmdDmPhongbanTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Ph'#242'ng ban/ B'#7897' ph'#7853'n'
      Hint = '@TS_DM_PHONGBAN'
      OnExecute = CmdDmPhongbanTSExecute
    end
    object CmdDmnvTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Nh'#226'n vi'#234'n'
      Hint = '@TS_DM_NHANVIEN'
      OnExecute = CmdDmnvTSExecute
    end
    object CmdDmmauTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'M'#224'u'
      Hint = '@TS_DM_MAU'
      OnExecute = CmdDmmauTSExecute
    end
    object CmdDmSizeTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Size'
      Hint = '@TS_DM_SIZE'
      OnExecute = CmdDmSizeTSExecute
    end
    object CmdDmHotroTS: TAction
      Category = 'TAISAN_DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' t'#224'i s'#7843'n'
      Hint = '@ASS_DM_HOTRO'
      OnExecute = CmdDmHotroTSExecute
    end
    object CmdDanhmucNhomNganhFB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Ng'#224'nh Nh'#243'm H'#224'ng'
      Hint = '@FB_DM_NGANH'
      OnExecute = CmdDanhmucNhomNganhFBExecute
    end
    object CmdDanhmucFB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Danh m'#7909'c nh'#224' h'#224'ng'
      Hint = '@FB_DANHMUC'
      OnExecute = CmdDanhmucFBExecute
    end
    object CmdDanhmuchotroFB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Danh m'#7909'c h'#7895' tr'#7907' nh'#224' h'#224'ng'
      Hint = '@FB_DM_HOTRO'
      OnExecute = CmdDanhmuchotroFBExecute
    end
    object CmdDanhmucBAN: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Khu v'#7921'c (T'#7847'ng)/ B'#224'n'
      Hint = '@FB_DM_BAN'
      OnExecute = CmdDanhmucBANExecute
    end
    object CmdDanhmucChiPhiFB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Danh m'#7909'c chi ph'#237
      Hint = '@FB_DM_CHIPHI'
      OnExecute = CmdDanhmucChiPhiFBExecute
    end
    object CmdDmNPL_FB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Danh m'#7909'c nguy'#234'n ph'#7909' li'#7879'u'
      Hint = '@FB_DM_NPL'
      OnExecute = CmdDmNPL_FBExecute
    end
    object CmdDmhh_FB: TAction
      Category = 'FB_DANHMUC'
      Caption = 'Danh m'#7909'c th'#7921'c '#273#417'n'
      Hint = '@FB_DM_HH'
      OnExecute = CmdDmhh_FBExecute
    end
    object CmdNhapFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Phi'#7871'u nh'#7853'p'
      Hint = '@FB_PHIEU_NHAP'
      OnExecute = CmdNhapFBExecute
    end
    object CmdNhapkhacFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Nh'#7853'p kh'#225'c'
      Hint = '@FB_NHAPKHAC'
      OnExecute = CmdNhapkhacFBExecute
    end
    object CmdXuatkhacFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Xu'#7845't kh'#225'c'
      Hint = '@FB_XUATKHAC'
      OnExecute = CmdXuatkhacFBExecute
    end
    object CmdKiemkeFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Ki'#7875'm k'#234
      Hint = '@FB_KIEMKE'
      OnExecute = CmdKiemkeFBExecute
    end
    object CmdDieukhoFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = #272'i'#7873'u kho'
      Hint = '@FB_DIEUKHO'
      OnExecute = CmdDieukhoFBExecute
    end
    object CmdKhuyenmaiFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Chi'#7871't kh'#7845'u, khuy'#7871'n m'#227'i nh'#224' h'#224'ng'
      Hint = '@FB_KHUYENMAI'
      OnExecute = CmdKhuyenmaiFBExecute
    end
    object CmdKhuyemaiComboFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Khuy'#7871'n m'#227'i - Combo'
      Hint = '@FB_KHUYENMAI_COMBO'
      OnExecute = CmdKhuyemaiComboFBExecute
    end
    object CmdKhuyenmaiChietkhauFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Chi'#7871't kh'#7845'u - '#272#7891'ng gi'#225
      Hint = '@FB_KHUYENMAI_CHIETKHAU'
      OnExecute = CmdKhuyenmaiChietkhauFBExecute
    end
    object CmdKhuyenmaidachieuFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Khuy'#7871'n m'#227'i - '#272'a chi'#7873'u'
      Hint = '@FB_KHUYENMAI_DACHIEU'
      OnExecute = CmdKhuyenmaidachieuFBExecute
    end
    object CmdBaocaoFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'B'#225'o c'#225'o nh'#224' h'#224'ng'
      Hint = '@FB_REP'
      ImageIndex = 64
      OnExecute = CmdBaocaoFBExecute
    end
    object CmdNhapxuatkhoFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Nh'#7853'p xu'#7845't h'#224'ng h'#243'a'
      Hint = '@FB_NHAPXUATKHO'
      OnExecute = CmdNhapxuatkhoFBExecute
    end
    object CmdDondathangFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = #272#417'n '#273#7863't h'#224'ng'
      Hint = '@FB_DONDATHANG'
      OnExecute = CmdDondathangFBExecute
    end
    object CmdDondhNCCFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = #272#417'n '#273#7863't h'#224'ng nh'#224' cung c'#7845'p'
      Hint = '@FB_DONDH_NCC'
      OnExecute = CmdDondhNCCFBExecute
    end
    object CmdXuattraFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Xu'#7845't tr'#7843
      Hint = '@FB_XUATTRA'
      OnExecute = CmdXuattraFBExecute
    end
    object CmdDmchiphi: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c chi ph'#237
      Hint = '@SZ_PUB_DM_CHIPHI'
      OnExecute = CmdDmchiphiExecute
    end
    object CmdTonkhoFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'T'#7891'n kho'
      Hint = '@FB_TONKHO'
      OnExecute = CmdTonkhoFBExecute
    end
    object CmdCheBienFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Ch'#7871' bi'#7871'n'
      Hint = '@FB_CHEBIEN'
      OnExecute = CmdCheBienFBExecute
    end
    object CmdLoaiVipCTFB: TAction
      Category = 'KINHDOANH_DANHMUC'
      Caption = 'Chi'#7871't kh'#7845'u th'#7867' VIP'
      Hint = '@FB_DM_LOAIVIP_CT'
      OnExecute = CmdLoaiVipCTFBExecute
    end
    object CmdHoaDonBanLeFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'H'#243'a '#273#417'n'
      Hint = '@FB_HOADON_BANLE'
      OnExecute = CmdHoaDonBanLeFBExecute
    end
    object CmdNhapTraBanLeFB: TAction
      Category = 'FB_NHAPXUAT'
      Caption = 'Nh'#7853'p tr'#7843
      Hint = '@FB_NHAPTRA_BANLE'
      OnExecute = CmdNhapTraBanLeFBExecute
    end
    object CmdDmBacLuong: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'B'#7853'c l'#432#417'ng'
      Hint = '@HR_DM_BACLUONG'
      OnExecute = CmdDmBacLuongExecute
    end
    object CmdDmDiaDiemLamViec: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'N'#417'i l'#224'm vi'#7879'c'
      Hint = '@HR_DM_NOI_LAMVIEC'
      OnExecute = CmdDmDiaDiemLamViecExecute
    end
    object CmdDmNguonTuyenDung: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Ngu'#7891'n tuy'#7875'n d'#7909'ng'
      Hint = '@HR_DM_NGUON_TUYENDUNG'
      OnExecute = CmdDmNguonTuyenDungExecute
    end
    object CmdDmThietBiTaiSanLaoDong: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'Thi'#7871't b'#7883' t'#224'i s'#7843'n lao '#273#7897'ng'
      Hint = '@HR_DM_THIETBI_TAISAN_LAODONG'
      OnExecute = CmdDmThietBiTaiSanLaoDongExecute
    end
    object CmdDmCanhBao: TAction
      Category = 'DANHMUC'
      Caption = 'Lo'#7841'i nh'#7855'c vi'#7879'c'
      Hint = '@SZ_PUB_DM_NHACVIEC'
      OnExecute = CmdDmCanhBaoExecute
    end
    object CmdDmBieuMau: TAction
      Category = 'DANHMUC'
      Caption = 'Danh m'#7909'c bi'#7875'u m'#7851'u'
      Hint = '@SZ_PUB_DM_BIEUMAU'
      OnExecute = CmdDmBieuMauExecute
    end
    object CmdHrHopDongLaoDong: TAction
      Category = 'NHANSU'
      Caption = 'H'#7907'p '#273#7891'ng lao '#273#7897'ng'
      Hint = '@HR_HOPDONG_LD'
      OnExecute = CmdHrHopDongLaoDongExecute
    end
    object CmdDangky: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' - Y'#234'u c'#7847'u'
      Hint = '@HR_DANGKY'
      OnExecute = CmdDangkyExecute
    end
    object CmdChamcong: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'H'#7895' tr'#7907' ch'#7845'm c'#244'ng'
      Hint = '@HR_CHAMCONG'
      OnExecute = CmdChamcongExecute
    end
    object CmdTinhluong: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'T'#237'nh l'#432#417'ng'
      Hint = '@HR_TINHLUONG'
      OnExecute = CmdTinhluongExecute
    end
    object CmdTamung: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'T'#7841'm '#7913'ng l'#432#417'ng'
      Hint = '@HR_TAMUNG'
    end
    object CmdDmLyDoKTKL: TAction
      Category = 'NHANSU_DANHMUC'
      Caption = 'L'#253' do Khen th'#432#7903'ng k'#7927' lu'#7853't'
      Hint = '@HR_DM_LYDO_KTKL'
      OnExecute = CmdDmLyDoKTKLExecute
    end
    object CmdDataRight: TAction
      Category = 'PHANQUYEN'
      Caption = 'Ph'#226'n quy'#7873'n d'#7919' li'#7879'u'
      OnExecute = CmdDataRightExecute
    end
    object CmdTangCa: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' t'#259'ng ca'
      Hint = '@HR_TANGCA'
      OnExecute = CmdTangCaExecute
    end
    object CmdMangThai: TAction
      Category = 'NHANSU_DANGKY'
      Caption = #272#259'ng k'#253' mang thai'
      Hint = '@HR_MANGTHAI'
      OnExecute = CmdMangThaiExecute
    end
    object CmdNghiBu: TAction
      Category = 'NHANSU_CONGLUONG'
      Caption = 'Qu'#7843'n l'#253' ngh'#7881' b'#249
      Hint = '@HR_NGHIBU'
      OnExecute = CmdNghiBuExecute
    end
    object CmdDsKhachHang: TAction
      Category = 'DANHMUC'
      Caption = 'Danh s'#225'ch kh'#225'ch h'#224'ng'
      Hint = '@SZ_PUB_DM_KHACHHANG'
      OnExecute = CmdDsKhachHangExecute
    end
    object CmdKetCa: TAction
      Category = 'KINHDOANH_BANLE'
      Caption = 'Danh s'#225'ch K'#7871't ca'
      Hint = '@SZ_KETCA'
      OnExecute = CmdKetCaExecute
    end
    object CmdChuyenDvt: TAction
      Category = 'KINHDOANH_NHAPXUAT'
      Caption = 'Chuy'#7875'n '#273#7893'i '#273#417'n v'#7883' t'#237'nh'
      Hint = '@SZ_CHUYENDOI_DVT'
      OnExecute = CmdChuyenDvtExecute
    end
  end
  object ImgLarge: TImageList
    Height = 32
    Width = 32
    Left = 380
    Top = 308
    Bitmap = {
      494C010116001900280020002000FFFFFFFFFF10FFFFFFFFFFFFFFFF424D3600
      000000000000360000002800000080000000C000000001002000000000000080
      0100000000000000000000000000000000000000000000000000B3B3B300ABAB
      AB00D9D9D9000000000000000000000000000000000000000000000000000000
      0000DDDDDD00D7D7D700D3D3D300D0D0D000D1D1D100D5D5D500DADADA000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000096969600ABABAB00A9A9
      A90082828200C6C6C6000000000000000000DDDDDD00C9C9C900B4B4B4009F9F
      9F008C8C8C007C7C7B00706F6F00686868006C6B6B0076767500848484009797
      9700ABABAB00C0C0C000D5D5D500000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FFD0D0F300C7C7F200CBCBF700CCCCF800CACDF900B9D6FA00B6D7FB00BDDE
      FB00BDDFFB00BDDFFB00BDDFFB008DD8FB00BADEFB00BDDFFB00BDDFFB00BCDF
      FB008DD8FB00BDDFFB00AADCFB00A0DAFB00000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A8A8A800C2C2C2000000
      0000A2A2A2008A8A8A00C3C3C30000000000D0D0D000B8B8B8009E9E9D007C6C
      650077513F00864D2E0097502800A1552800A25629009A542A008C5333007F5A
      460087787000ACABA900C6C6C600DEDEDE000000000000000000000000000000
      000000000000000000000000000000000000000000FFE2E2F8007979E1004848
      D8004D4DE7005050EF005050EF005055ED00349BF5001E93F7001E93F7001E93
      F7001E93F7001E93F7001D95F70004C2FE001E93F7001E93F7001E93F7001A9A
      F80008BAFD001E93F70010ACFB0011A9FB002495F500B7DCFB00000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B8B8B800BFBFBF00D3D3
      D3008D8D8D00B5B5B50085858500C4C4C40000000000A18A800097523200A75B
      2A00B1682E00B66F3100B8733200B9743200B9743200BF804400C2865000BE7F
      4B00B6744500A86D4A00B19E8E00000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF5353D8004848D9004F4F
      ED005050EF005050EF005050EF005B79EB001C8AE7001E91F4001E93F7001E93
      F7001E93F7001E93F7001A9AF80009B8FD001E93F7001E93F7001E93F7001B98
      F80014A5FA001E93F7000BB5FC0017A0F9001E93F7006EB8F800000000FF0000
      00FFE5F6ED00C3ECD800D5F1E300000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000B3B3B3007D7D7D006565
      6500A0A0A00027272700A4A4A4007873720082524000B4765300C28A5D00C791
      6200C48B5600B9763900B7713200C0834C00C48B5900BC7C4300B6713200B671
      3100B6703200B5703600AD632E00B6795200E8D8CA0000000000000000000000
      0000000000000000000000000000000000008686E3004848D7004D4DE5005050
      EF005050EF005050EF005050EF005259EB00338EE1001B85DF001C89E50011A7
      F6001E92F6001E93F7001E93F7001D94F7001E93F7001E93F7001E93F7001E93
      F7001E93F7001E93F7001C97F8001E93F7001E93F70062B7E8006FD3A2004FCA
      8D0047C9890047C989007CD7AA00000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B7B7B700CBCB
      CB00454746002527260023232300A8A8A800715B4C0093572800AE672F00AD64
      2F00B06B3D00BE866300CEA48A00C3907100A75C3000A6592C00A65A2C00A85D
      2D00AA602D00AE652F00B4703B00AD632D00A3592F00D4B19F00000000000000
      0000000000000000000000000000000000005B5BD9004848D7004F4FEB005050
      EF005050EF005050EF005050EF005050EF004F56ED005989E9002E99E70008B6
      F8001B85E0001C8AE8001D90F1001E93F7001E93F7001E93F7001E93F7001E93
      F7001E93F7001E93F7001E93F7001E93F7001E93F70059BCCA0047C7880069D2
      9E0095DEB900BEEBD500000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000BABA
      BA009999990040434100272828002E2E2E00A6A5A500644533008A4824009F51
      2A00A8613D00B67A5C00A1542C00A55B3500BB826600B77C5F00A0522C009E4F
      2A009D4E29009E4F2900A3573000AA623600A85D2B009F512700CBA392000000
      0000000000000000000000000000000000006060DA004848D7004F4FEB005050
      EF005050EF005050EF005050EF005050EF005050EF005050EF005050EF004F70
      EB0060A0E700288ADE001B85DF001B87E2001D8CEC001E91F400179FF9001E93
      F7001E93F7001E93F7001E93F7001E93F7001E93F70051C1AB0047C9890057CC
      930079D6A7009AE0BD00BAE9D200E2F5EB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000BBB1AE00A1A1A10042454300282A29003A3A3A00A19E9D0063453700A474
      5A00C18E7300B5785500B6795700B67A5700B67A5800B06F4B00BD876A00AC67
      44009F512B009D4E2A009B4B2800A45B3C009D4D2700A15328009A4A2500D8BA
      AF00000000000000000000000000000000009A9AE8004848D7004D4DE5005050
      EF005050EF005050EF005050EF005050EF005050EF005050EF005050EF004F4F
      ED00D5D2E800E4E4E400A2C7E50054A1E4001B84DD001A87E00004C0FD001D8E
      EF001E93F6001E93F7001E93F7001E93F7001E92F50044C58B0050CA8E007CD7
      AA005FCE980046C7870047C9890054CB90000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EEE5
      E30094442D00B2938600A9A9A900444645002A2C2B0047474700989492006648
      370096542B00A85F3100A9603100A9603100A95F3100B2704800B97E5B00C89A
      8000BD856800A1532C009E4F2A00A65F3F009A492800994726009B4A2600994B
      2D00EFE4E000000000000000000000000000E3E3F8005B5BD9004949D9005F74
      CE0040996F005585AB005160D8004E928C004E9C7F005050EF005050EF008988
      EE00EDE9E600EDE9E600EDE9E600EDE9E600D3DBE40073BAE90018ADF0001B85
      DF001B86E1001C8BE9001D90F2001E92F6003A9FF00055CB910047C9890068D1
      9D00DCF3E700000000FFD6F1E400D3F1E2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000AF74
      6500A1573C00A0553500AA837200AFAFAF00464948002B2D2C00555555008B85
      8200603A1F009D5C2F00AD663400AD663400AD653400AB633300AA613200A85F
      3100B06E4700CBA08700B4765700A55B3800A359390098462600964325009643
      2400B2786600000000000000000000000000000000FFE5DFD8007876D7005D81
      B100108C37000E8C36000F8B37000E8C36003A9A6300606EDA00AAA8ED00ECE8
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600B7D1
      E50069ABE5002388DE001B85DF001B85DF0095C0E500E1E5DF0092DAB50048C8
      890049C88900B0E6CB00000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E8D8D400984A
      3500974526009B4A28009F522B00AB816900B5B5B4004A4C4B002C2E2D006767
      67007C756F00653E2000A3643200B16C3700B16B3600AF693500AE673400AC64
      3300AA603200AC653B00BA816100B5785800B77D62009B4A2800974425009440
      23008F3B2000E8D7D4000000000000000000000000FFEFE5CB00D2C58E0058A9
      67002E9A4F0049A663004DAD6F00369D5800399F590090C49F00EDE9E600EDE9
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9
      E600EDE9E600E0E2E400ABCBE700B9D2E700EDE9E600EDE9E600EDE9E600D0E2
      D70075D4A5009EE1C000000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BE8F8500923E
      2400984626009E502A00A4582E00A95F3100B58D7200BAB9B8004E504F002D30
      2F00797A7A006D635B006B442200A86A3500B4713900B36F3800B16C3700AF69
      3500AD653400AA613200AC653A00B5775500B4775800B27458009C4C2C009541
      2400913C2100BA8677000000000000000000000000FFEFE5CB00E0C89100ECD4
      9A00F9DFA200DCDCBB0080D3AA00DDDAD700EDE9E600EDE9E600EDE9E600EDE9
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9
      E600F7F6F500000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000009A4F3E00943F
      23009B4B2800A2552D00A85E3100AD663400C4906800B2835B00C0BDBB005355
      540030333100888888005F5449006D462300A56A3500B3723900B4713900B26E
      3700B06A3600AD653400AA603200AE694100B87E5F009F502B00AF6E52009C4E
      3100923D2200954530000000000000000000000000FFEFE5CB00E0C89100ECD4
      9A00F9DFA200E6DDCA00D5D1CF00E0DCD900EDE9E600EDE9E600EDE9E600EDE9
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9
      E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9E600EDE9
      E600F5F3F200000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000EBDEDC00903B25009643
      25009E502A00A65B2F00AC643300C0895E00B8784100B9783C00B7865700C4C0
      BC005A5C5B00363837009393930054493E0051351A00734B25008B592C009B62
      3100A4653300AB663400AC643300B06C4200B77A5800A55B36009C4C2900AA66
      4B0098472C008F382000E9D9D50000000000000000FFEFE5CB00E0C89100ECD4
      9A00F9DFA200E5DDCB00D6D2CF00DFDCDA00EBE8E500EBE8E500EBE8E500EBE9
      E500EBE8E500EBE8E500EBE8E500EAE7E500EBE8E600EBE8E600EBE8E600EBE8
      E500EBE8E500EBE8E500EBE8E500EBE8E500EBE8E500EAE7E400EBE8E500EBE8
      E500F4F2F100000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DABFBA008F3921009847
      2600A1542C00A9603100B06A3600C6926600BA793C00BD7E3F00C0824100BC8A
      5300C8C3BC00676867003E3F3F00A1A1A1005D5752003E322800342313003D27
      13004D3018006F4422009E5E3000B97E5700A75C3000AD6A47009D4E2A009947
      2700A7624A00903A2100D5B7AF0000000000000000FFEFE5CB00E0C89100ECD4
      9A00F9DFA200F9DFA2005DD3EE0002C5FF0002C5FF0061D1AA004ACC90005FE5
      CD004ED19D00728FD9004F4FEE00312CC5004847E5004644E100322DC7004C4C
      EA006A74DF000E8C36000E8C36000E8C36000E8C360049C5980050D3A1004BC8
      8B00000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CDA9A200944229009A4A
      2800A4582E00AC643300BF855700BE814800BE803F00C2854200C5894300C78C
      4500C2905200D0C9BF00ADADAD00B7B7B700636462007E7E7D007E7E7D005D5B
      58004A443F00392B2000643C1E00B17A5600A75D3000A55B3200A45B38009A48
      2700964428009E533E00CAA4990000000000000000FFD8C49900AF8B4200B993
      4500C39B4900C39B49009CA4730017C7F8002EA9830054CB900048CB8D004BCE
      940048C889005F60EC003631CC004A49E7005050EF005050EF004D4CEB003530
      CB004D4CE8001A8A49000E8C36000E8C3600148E3C004AC78C0050D4A40047C9
      8900BEEAD400000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C59A92009D513A009C4C
      2800A65B2F00AF693500C6916300BD7E3F00C3864200C78C4500CA904600CC93
      4800CC944800C6945300D0CCC600DCDCDC005E616000BBBFBD00686867009495
      95007D7E7D008A8A8A004A3A2E008D614200A75E3000A4582E00A45A36009A49
      2700964326009D513B00C89F940000000000000000FFC4AA7200895C04009161
      0400996604009966040099660400799042000E8C360061C7900059CB920056C7
      8C0040A174005F63EA005A5BE6005E60EA005E61EC005E61EE005E61EE00595C
      EA005E61EC00398879000E8C36002E9C590039C7D3005FD0A10047C9890047C9
      8900CFEFDF00000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000C7A098009F543E009C4D
      2900A75D3000B16C3700C8946300C1844100C78C4500CC934800CF974A00D099
      4B00D19A4B00D0994A00B69C76009E9E9D009B9D9B008084820099999900B7B9
      B800A1A3A200BCBCBC0083828100744B2F00A45D3000A4582E00A1542E00AF70
      5500A35B4200923D2500D1B0A80000000000000000FFC4AA7200895C04009161
      04009966040099660400996604007C8833000E8C36000E8C36000E8C36000E8C
      36001F9E4F00199746001E9D4D000E8C360047A6B8001E93F7001E93F7001E93
      F70062BBA9001E934300168F3C002FBECA0002C5FF0002C5FF00D1F1E9000000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3B4AE00913C2500AA65
      4700A85F3100BE845500C68F5800C68B4600CC934800D0994A00D39E4C00D5A0
      4D00D5A14E00D49F4D00C0965400B1B2B200D5D5D500A6A6A500D3D5D400CACC
      CB009B9A9700C8BEB300ACACAC008F6C5300A75F3100A4582E00A9624000CCA2
      9200AC6A54008E372000DEC8C10000000000000000FFC4AA7200895C04009161
      04009966040099660400996604009866040052994F001695420025A457000E8C
      36001F9E4F003DBE7B0024A457000E8C36002B9460004B95F200329BF4004DB8
      DE004BCE950048C6870040CFEE0002C5FF0002C5FF003BD0FD00000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000E7D8D5008C331E00A65E
      4100BC836000CB9C7700C78F5600CC965200D19B4E00D5A04E00D8A55000D9A7
      5000D9A75100D8A55000D09E4C00AAA59C00C5C6C600E2E3E200DFE0E000C0BB
      B200B3804C00BB7D4400BC957500A2775800A9603100A3572E009E4F2A00C698
      8500B57A68008D351F00EEE3E10000000000000000FFC4AA7200895C04009161
      0400996604009966040099660400996604007E8E3E00108F3A003FC07D003FC1
      7E00179644000E8C36000E8C360033985A004E69BC004847E50065BFA9004FD2
      9E005FE5CB004BCD930056CD9F007EDFFC007DDFFD00000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000913E3000B072
      5B00C7987C00D4AC8C00CC986200D2A05F00D7A65B00DAAA5700DDAC5300DEAE
      5300DDAD5300DCAA5200D7A55000B09F8200DCDDDC00EDEEED00ECEDED00816D
      550084582C00B0855D00B06D3800AE673400A85E3100A2562D009D4D2900A45B
      4000BC887900964A35000000000000000000000000FFC4AA7200895C04009161
      0400996604009966040099660400996604009271120029964A000E8C3700118F
      3A000E8C36000E8C36001A903F00525EDC004E4EED003733CE005483C60047C9
      89004DD09B004BCE940047C98900E2F5EB00000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000BB898200BA85
      7500C08D7200D6B19500D6AD8200D6A86B00DCB06800E0B56400E2B65E00E2B4
      5700E1B35600DFAF5400DBAA5200B6996700E5E6E600F3F3F300F9F9F9005C52
      4800392818006D4C2F00A6663300AC643300A65C3000A0532C009B4A28009541
      24009E543F00C8A097000000000000000000000000FFC4AA7200895C04009161
      04009966040099660400996604009966040099660400D5C89F00C1E0CA000000
      00FF7CBF910051AA6D00B4DAC1009191F3004B4AE8003A36D2005050EF0095D6
      C1004CC98C0047C9890047C98900000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E7D7D600AE73
      6600C2917C00D1A98D00D4AB8200E1C09400E2BB7A00E6BF7100E8C16B00E7BD
      6000E4B85800E1B35600DDAD5300CEA25800CBCBCA00EDEDED00F5F5F500CBCA
      C9008D857C0089603D00AD693500AA613200A4582E009E4F2A0098462600923D
      22008D392400ECDEDB000000000000000000000000FFECE1C500DAC18900E7CC
      9000F3D79800F3D79800F3D79800F3D79800F3D79800F9F2DF00000000FF0000
      00FF000000FF000000FF000000FF000000FF9191F3004543E0008585F2000000
      00FF000000FFCDEFDE00DEF4E900000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000B782
      7B00C3958600D1A99300D1A68100D9B17F00E7C89600EED39800EECD7C00ECC8
      6F00E7BC5B00E2B55700DDAD5300D8A54F00C2A37400D5D4D300E8E9E800EAEB
      EA00E9E9E900CDBAAB00AD663400A75C3000A1532C009B4A2800944124009442
      2B00B57F7400000000000000000000000000000000FFF1E6CF00E0C89100EBD3
      9900F9DFA200F9DFA200F9DFA200F9DFA200F9DFA200FCF5E700000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF9E9EF400DEDEFA000000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFE6
      E500B0776F00CFA89900CFA58900D7AF8700E0BC8600EBCB8700F4DC9C00F3DA
      9800ECC97B00E1B35600DBAA5200D5A14E00CF984A00C5935400CBB19400C7A4
      8400BE8C6100AF693600A9603100A3562D009C4D29009A4A2C009F543F009648
      3600EFE5E300000000000000000000000000000000FF000000FFDFC99600E2CB
      9300F8DEA100F9DFA200F9DFA200F9DFA200F8E2AF00000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D8BCB900C0928900CFA79500D2AA8D00DAB48B00E3C18B00EDCE8A00EDCD
      8300E9C88100E4BE7900DFB67300DAAE6F00D4A46700CF9E6600C9966400B774
      3B00B16D3900B06B3F00B16F4B00AD6A4B00A76146009D50390089311C00D3B4
      AF0000000000000000000000000000000000000000FF000000FF000000FFF0E4
      C600E5CD7E00ECD38200ECD38200FBF3E000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000CEAAA700C79D9400CEA49300D3AB8F00D9B28C00DEBA8900E1BC
      8400E0B97C00D8AA6700CD944800C78C4500CC986000CB9A6C00C1885900BD83
      5900B7795200AD6844009F522F0097442500913B210088311C00C49991000000
      000000000000000000000000000000000000000000FF000000FF000000FFF7F1
      DB00E1C14800E1C14800E1C14800000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D7BBB800C2958E00CCA39400D0A79000D3AA8B00D5AB
      8500D5AA7E00D2A47600C58C5300C8956300BE845300B16B3600AB633300A65B
      2F00A0532B009A4A2800954124008F38200089311E00C9A29B00000000000000
      000000000000000000000000000000000000000000FF000000FF000000FFBA9F
      6900AC873800B38C3A00B38C3A00D0BA8F00000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000EDE2E100C0938F00C99E9400CA9F8E00CB9E
      8800CA9C8000CEA38600D1A98E00B4744B00A75D3000A3572E009F512B009B4A
      280096422400903A21008B321D0095473900E0CBC60000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF946C
      1E00926104009966040099660400B3904800000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DDC6C300C0938E00CCA5
      9D00CCA59A00C5988900BB867200B67D6700A2593E0094402300913B21008D35
      1F00892F1C00903E3100C69D9800000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF966C
      1E00926104009966040099660400B48F4900000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E6D5
      D400CCA8A400BB8A8500AE766D00A7696000A36258009D584C00A05B5100B783
      7C00DBC2BE000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FFE4DC
      CA00DCCEB300E0D2B700E0D2B700EEE6D900000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
      00FF000000FF000000FF000000FF000000FF0000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000C9C9C900ABABAB00909090007A7A7A007676760086868600A0A0A000BEBE
      BE00D9D9D90000000000CACACA00A4A4A40085858500818181009F9F9F00C9C9
      C900000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CCE8F80084C6EA0081BFE100519BC6004D92BC004580
      A70041779D0039658800345C7E006380990097ABBC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D1D1D1008E8075006E51
      380067411D00693A0D006D360400703600006B35030067370B00623B18005B41
      28004D4137002D6F7D001492AD0007B4DA0000CAF60009B7DC001B98B4003779
      870083838300D6D6D60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000060B7
      E50060BAEA007EC4E8007EC6E90081D5F9005DB7E50050BAF30037ADEF002A96
      D40042A2D600328ABB000B5D8E000F5D8D00297FAF002B7DAE00337FAE00407A
      A500A6C2D6000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D8D5D500887461006C441E00713700007137
      0000713700007137000071370000FFFFFF0071370000713700006F360000593E
      1600207F88000BCEF80020D4FB0038D9FE004CDEFF0043DBFE002BD6FC0012CE
      F8001E8CA40060707400D6D6D600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D9DADC00D3D5D800D3D5D700D8D9DC000000
      00000000000000000000000000000000000000000000000000009DCFEC005BB6
      E9005DB5E5007BCFF3007DD0F20081D8FB005EB6E40053B4EC0039A2E2002C8B
      C600439BCE003386B7000B5A89000F5D8D002981B1003489BA003391C6002F86
      BA00357AAA00E7EEF300CFDEE700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D8D8D800000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D0D1D100778C9600C0C1C200000000000000000000000000000000000000
      00000000000000000000AFA39800714B27007137000071370000713700007137
      0000713700007137000071370000FFFFFF0071370000713700006A340000267F
      86000BCCF80020D4FC0033D9FD003DDBFD0044DCFE0047DDFE0045DDFF0034D7
      FD0014CEF8001F8AA30083838300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D0D0
      D000C4C4C400AAAAAD009C9DA20075787F0061646A0067686900707071007F7F
      8200B7B8BA00000000000000000000000000000000000000000052ADE0005AB7
      EA005FB5E400AADCF600B5E1F500AEEDFE0094E6FC007BDFFC007EE3FF0078DF
      FF0075DDFF0060C0E9006BC3EA0080D4FA008BD9FD0080BDE0004BA4D5003192
      C9003798CE00246BAB00155694000E477D00185387001A507E0013456F002E62
      890033638A00A4BDD00000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000959595009D9D9D0085858500D4D4D40000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C7C8
      C8002C7FA00000B0E50016A1CC00C8CACD000000000000000000000000000000
      0000000000009D8976006F3D0F0071370000FFFFFF0095694000713700007137
      0000713700007137000071370000FFFFFF00713700007137000045604D0005C5
      F20012D0FA001FD5FC002AD7FD0032D9FD0038DAFE003ADBFD0038DAFE0032D8
      FD0024D4FB000AC5F10037748300C9C9C9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D9D9D900C0C0
      C000AEAEAF008C8D91007B7F84005D6169006F717400797979007F7F7F00C1C1
      C100B7B7B800000000000000000000000000000000000000000052ADE00065B8
      E7008ED2F40089E7FA0089EFFF0084E5FD007CDCFA0066C1EB0060B6E100519B
      C6004D92BC004580A70041779D0039658800345C7E00427598004F93BC00419B
      D0003292CA002571B300145C9D000D4C85001A598F001C5C90000B4A7B002073
      A6002A7CAD003174A200407AA500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DBDBDB00A6A6A600000000009F9F9F0092929200DADADA00000000000000
      0000000000000000000000000000000000000000000000000000C1C1C2002581
      A40000B4E5000CD1F20007CAF1007EA2B2000000000000000000000000000000
      00009B846F006F380500713700007137000095694000FFFFFF00713700007137
      00007137000071370000713700007137000071370000713700002886900005C7
      F4000FD1FC0017D3FC001FD5FC0027D6FD002CD8FD002DD8FD002CD7FD0027D6
      FD0020D4FC000FC9F5001D8EAA009F9F9F000000000000000000000000000000
      000000000000CCCCCC00C1C1C100AAAAAA00A0A0A000919191008D8D8D008585
      85008282820085858500878787008E8E8E0091919100999999009C9C9D009395
      970087898D006D737C005F6674009B9B9B0094949400E6E6E600D5D5D500D7D7
      D700C8C8C800CECECE000000000000000000000000000000000052ADE000AFFB
      FF00B2F7FD005BB7E7005BB5E60079C2E8007EC7EA0081D6F9005DB8E60050BA
      F30037AEEF002A97D40042A2D600328ABB000B5E8E000F5D8D00297FAF002B7C
      AE00327EAE003572A100316DA1003370A9003676AD003877A9002F6A99001F71
      A5003487B8003691C6003293C900377EB0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E3E0DF00A3A2A200C6C4C4008E8E8E009797970090908F00DFDDDD000000
      00000000000000000000000000000000000000000000B8BABA001E84AA0000B4
      E5001AD5F40010D2F3002497BE00DBE1E400000000000000000000000000AD9A
      88006F380500713700007137000071370000713700009569400071370000773C
      06007C400C00804410008346140085481600834614008044100014A3C40003CA
      F70008CFFC00043D4900043D4900043D4900043D4900043D4900043D4900043D
      490015D3FC000DCCF8000CA4CA0081818100000000000000000000000000CECE
      CE00B8B8B80094949400858585003085AF001595CB0005ACE50003B5EB0000C6
      F80000CBFA0000C8F90003BBEE0005A9E1000F98CD002D79A100526F81006D6F
      7400696E75005D6576008D909800A6A6A600C5C5C500D4D4D400E1E1E100DEDE
      DE00D6D6D600BEBEBE00000000000000000000000000000000004AA4D70086F1
      FC006BCCEE005BB7EA005CB4E4007ACFF3007DD0F20081D8FB005FB6E30053B3
      EB003AA1E0002C8AC500439ACE003386B6000B5989000F5D8D002981B1003489
      BA003392C6002E87BC00357AAA003684BA00398BC3004091C80056A8DB008ECE
      F10085C1E4003790C5003192C900377EB0000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B9B2AD00A6A6A600868786001B1C1B007A7A7A008C8A8900E0DB
      D80000000000000000000000000000000000D4CFCD001A7FAC0000B8E7002AD9
      F50018D2F2002C95B90000000000000000000000000000000000D0C6BD007341
      13007137000071370000713700007137000071370000763B06007E420E008548
      160085481600854816008548160085481600854816008548160008B0DD0002CD
      FA0015D3FB002C5C66003B68710048717A004F777F0048717A003B6871002C5C
      66001DD4FC0008CFFB0005ADDB00858585000000000000000000B8B8B8000D5E
      B7000062CD000088DC000090E00000A1E70000AAEB0000B8F20000BFF40000C8
      F90000CBFA0000CAFA0000C6F80000B7EF0000AEEB000093DC000084CF000062
      B1000053A000BBC0C500C6C6C600ECECEC00D3D3D300D4D4D400D1D1D100D8D8
      D800D0D0D000B8B8B80000000000000000000000000000000000000000005DAC
      D80052ADE00059B7E9005FB4E300ADDDF600B2E2F500A1E9FC0098EAFE0073CE
      EC0070CAE90074D8F80075DDFF006CD5FC0064C8F40075CFF80086D8FD0085C1
      E4004EA7D8003192C8003797CE000E487E00185388001A507E000F4570002C61
      890033638A0046A0D20054BCF200377EB00000000000C1B9B200AFACA700A7A4
      A200A4A2A000A4A2A000A5A2A000A5A2A000A5A2A000A5A2A000A5A2A000A5A2
      A000A5A2A000A09E9C0081818100999A9A003D403E001D1E1D00828282007D7B
      7A00A09E9C00A7A4A200A6A19F00999693004E74810000C1EB003AE3F9001CC7
      ED001A83A9000000000000000000000000000000000000000000876240007137
      000071370000713700007137000071370000793E090082451300854816008548
      1600854816008548160085481600854816008548160085481600189BBC001ECE
      F60040DAFC0055DEFC005EE0FC0064E1FD0068E2FD0066E1FD0060E1FD0058DF
      FC0042DBFC001ECEF6000C97BF00A6A6A60000000000CECECE00597DA700005D
      CA000062CD000083D7000087D80037AAE20055B8E60074C3E80079C4E7008FCD
      EA009CD1EC0095D3EE0083D2EF0082D6F3006CCFF2003DB0E500158ED3000061
      B1000050A000D3D3D300CECECE00DADADA00AEAEAE00A7A7A700B0B0B000D4D4
      D400CECECE00BCBCBC0000000000000000000000000000000000000000000000
      000052ADE00065B7E70084CDF40074D2F50068C3EB0055A3D000519BC600498A
      B3004580A7003D6E9300396588003D6A8A00477C9B0056A4C80063C2E80057C4
      F7005FCAFE0045A5DC003291C9000D4C85001A598F001C5C90000B4A7C002074
      A7002A7FB0003173A1003778A7003982B4000000000078533800A76C4700C386
      6200000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000004A4A4A00A0A1A0003F424100222323008989
      89002626260000000000001019000062900000B6E50000DCF80019DEF8000972
      9C004B58540000000000000000000000000000000000C4B4A60071370000FFFF
      FF00956940007137000071370000793E09008347140085481600854816008548
      160085481600854816008548160085481600854816008548160035808B0053D2
      F1006EE3FD006DE3FD006CE3FD006BE2FD006BE2FD006BE2FD006CE3FD006DE3
      FD006EE3FD0053D2F100197A9800CBCBCB0000000000C5C5C5000053C0003D7C
      C1004D89C2000B9DD40004B3E10001CCEF0001D4F30000E0F90000E3FA0000E2
      FA0001DCF50001CBEF0003BAE5000999D2001287C4001C88C0001E8DBF002384
      B20092AFC300DADADA00F6F6F600CFCFCF00D8D8D800D2D2D200E9E9E900CACA
      CA00C7C7C700CFCFD0000000000000000000000000000000000000000000A9D6
      EF0058B3E50064B5E2007BC3E80085CCE80080D7FA0058C0F30050BAF3004CB3
      EB002A97D4001B7BB200328ABC002C7FAE000F5E8E002074A700297DAF003075
      A300397AA7007AE2FF0060CAFC003370A8003676AD003B7AAD00417CA8002B7B
      AC003487B7003691C6003293C900377EB000000000007F553500A4745400B68A
      6F001A120E001A120E001B130E001B130E001B130E001C140E001C140E001C14
      0F001D140F001D150F001E1611001F18130057514C00A6A6A50042454400292A
      2A0088888800221F1C0011506B0000C1EE0000BEFB0006ADD800274D57002A2D
      2B004D635E00000000000000000000000000000000008A633F00713700009569
      4000FFFFFF0095694000763B06008245130085481600FFFFFF00A07049008548
      16008548160085481600854816008548160085481600854816005C6452003DBD
      E20077E1FA0079E5FD0077E5FD0077E5FD0077E5FD0077E5FD0077E5FD0079E5
      FD0077E1F9003CBDE200316270000000000000000000D2D2D200166CC8000384
      D70000A3E30000B5EA0000BEED0000CFF30000D6F60000E0F90000E3FA0000E2
      FA0000DEF80000CEF30000C4EF0000ACE600009EE0000180C700056EB4001E67
      9C00CBCFD200E6E6E600F7F7F700ECECEC00DBDBDB00B6B6B600D9D9D900DBDB
      DB00B9B9B900E2E2E200000000000000000000000000000000000000000057B3
      E50057B4E80071C5EE007BD0F40083D2EE0081D7FA005BB8EA0053B2EA004DAA
      E0002C89C4001C71A6003385B5002C7DAC000F5D8D002077AA003489BA00358F
      C4002D86BC0061B2D20060CAFC003580B5003786BE004DA4D70075CEF6008FD2
      F4008CC6E8003993C8003191C800377EB0000000000074492A00825A3A00835A
      3900794E30006B3C20006D3D20006F3F21007141220074422300764423007846
      2400794724007A4824007C4B2900855A3C00764F330074594600A8A7A5004648
      4700303231006F6D6B00ACA7A3007BCFDE0046C4EE00287A920061796E005B6E
      64005A473600000000000000000000000000E2DBD40070360000713700007137
      000071370000713700007E420E008548160085481600A0704900FFFFFF00A070
      490085481600854816008548160085481600854816008548160085481600477B
      7F004FC6E8007BE1F90085E8FD0083E7FD0082E7FD0083E7FD0085E8FD007BE1
      F9004EC6E8002D6B75004D413700D9D9D900C2D8F1000068CF00006DD200007A
      D90000A2E3003FC8F00061D4F3007ED6F10083D4EF0093D3ED009CD1EC008CCB
      E90079CAE80075CBEA005FC5E80037B1E200149AD8000074C0000063B00095A5
      B500C4C4C400FBFBFB00B8B8B8009E9E9E009F9F9F00D9D9D900B2B2B200D0D0
      D000A8A8A80000000000000000000000000000000000000000000000000055BC
      F3005AB6E90075C0E8007FC3E7007CB3D4006EA9CC005196BB005193B6004D8C
      AE00569EBF0061BADE0062C2E8006ACAF5006FCCF6008FD2F4008CC6E8003993
      C8003191C800377EB0002269A5000E487E001A558A0024628F00135680002B62
      8A0034668D00449ACC0053BEF500377EB00000000000764B2C007D522E007E52
      2E007F532E007B4D29006936150065301000683311006A3512006D3713006F38
      1300713A1300723B1400733B1400743F19007C4F2F006B432600694C3800A6A4
      A2003F403F00AEAEAE00BCBBBA0050524F004B829000386B650048615A00463A
      2F0054271000000000000000000000000000BDA7940071370000713700007137
      000071370000773C060085481600854816008548160085481600A0704900FFFF
      FF00A0704900854816008548160085481600854816008548160085481600794F
      260044777B0035B6DC0059CFEE0074DFF80083E7FD0074DFF80059CFEE0035B6
      DC002E6972005A3A12005B412800BEBEBE001D71CF000068CF001079D6007EC8
      EF0096D9F20063C6E70047B9E10029A0D2002598CD001F8EC6001E8CC5001B81
      BE001B80BE00218BC500278DC6004598CB005C9DCC004B8BBD002C70AC00B1B1
      B100B9B9B900EBEBEB00A6A6A600E2E2E200FFFFFF00FFFFFF00FFFFFF00B4B4
      B400B4B4B4000000000000000000000000000000000000000000000000005ABC
      EF005FBBED0064B8E50064B2DF0053AADD003F9BD6003388BE003F8EBF00357A
      A6001E58800027567A002F5C7F004D8CAE005AACD00063CEF90057C4F7005ABE
      F40047A8DE00377EB000145C9D000D4D85001A598F00266F9F000F5E8E002075
      A7002E81B2002E76A5003778A7003982B4000000000081573200885D3600885D
      3600875D3600875C3500855B35007246240061311200663311006C3712007039
      1300723B1400743D1400753D1500753D1500754019007D51310069442900513D
      2F00C7C7C600BDBDBC005E5F5E00767D7D003255570040544C00513520005626
      0D0058270F000000000000000000000000009B77570071370000713700007137
      0000713700007C400C0085481600854917008E552600956034009B6A4100B691
      7300FFFFFF00BD9B8000AC836100AE856400AC836100AA7F5C00A67A5500A172
      4C009B6A4100677264003A7E8A001B8DAE000C99C700198BAD002B717C004853
      41006A3400006F360000623B1800A0A0A0001D7ED6005FD1F2002CB6E1001483
      C300068ED20000A3E30000ABE60000BEED0000C7F00000D6F60000DCF80000E3
      FA0000E4FB0000DEF80000D7F60000C0EB0000ACE000007EBE000066A9009898
      9800ADADAD00C2C2C200C0C0C000CCCCCC00BABABA00C9C9C900CECECE00B0B0
      B000D2D2D200000000000000000000000000DDEEF7005BB6E9005DB5E5007BCC
      F1007DCEF1007FDAFD005DB8E70052B7EF0039A8E8002C90CC00439ED2003388
      B9000B5C8C000F5E8E002881B2003489B9003390C400317CAE003A7FAD0062CE
      FB007CE5FF003982B4003F7EB5003C7EB8003777AF0065AED7005090B8003180
      AF003487B7003691C6003293C900377EB00000000000885A3300916237009263
      370094643800976739009B693A00A06C3B00A06A39008E542600864D2100834C
      2100814B20007E491F007A471E0076431D00703F1A006939170062463200C3C0
      BD00BEBBB90098949100575958005C5D5C00707474003E281A004E230B004D21
      0A00552B14000000000000000000000000008355290071370000713700007137
      0000713700008B5526009B694000A6795400AF876600AF876600AF876600AF87
      6600C1A18800FFFFFF00C1A18800AF876600AF876600AF876600AF876600AF87
      6600AF876600AF876600AF876600A67954009B6940008B552600713700007137
      0000713700007137000067370B008686860031C5F30025ACDD001386C6000088
      DA000090DD0000A2E30000ABE60000BEED0000C7F00000D6F60000DCF80000E3
      FA0000E4FB0000DEF80000D7F60000C0EB0000ACE000007EBE000065A9008C8C
      8C00C8C8C800C4C4C400D9D9D900E7E7E700DCDCDC00EAEAEA00CECECE00B7B7
      B700F3F3F3000000000000000000000000005BB0E00058B5E8005DB8E7007BCF
      F3007ECDEF008AD1F20071B6DF0079C2EC0073B9E50061AAD90065B3E00057A6
      D3002A729E0019618F00297DAD003489B9003492C6003193C900318ABF0091E9
      FF00A4FCFF00469DD000337AAD00337BB0004DA7D70067C9F50072CDF6008FD2
      F40092CBEB003992C7003191C800377EB000000000007B58360085613B008963
      3B008A643C008A643B0089643B0086623B0085613A00825F39006C4726005D36
      18006639150079441A00884E1E009254210093552100774F2F00C7C4C100C8C3
      BF007B5B420086644B00B8B1AC00717271009D9D9D0071706F00645C57005843
      35005A372200CDCCCC000000000000000000763F0D007137000071370000824F
      1E0092653B00AE866500B0896800B0896800B0896800B0896800B0896800B089
      6800A6878600C2A28900FFFFFF00C2A28900A6878600A6878600A6878600A687
      8600A6878600A6878600A6878600A6878600A6878600AE86650092653B00824F
      1E0071370000713700006B35030076767600CCF2FB00007BD400007CD6000F8E
      DB004FB2E40086C7E70077BEE2003AA0D2002C98CD00218DC6001F8AC5001B81
      BE001B81BE001F8AC500218DC6002B96CB003698CB005B9AC400568BB6007979
      7900E0E0E000CBCBCB00F0F0F000E6E6E600E1E1E100C4C4C400A3A3A3000000
      00000000000000000000000000000000000052ADE0005CB4E60078C0EA00A1EC
      FE0095F0FF0089EBFF0088EAFF0082E5FF007EE3FF0078DFFF0075DDFF0070D2
      F2006FD2F4006DD3F70061CBF70078CFF9006BCCFD003898CF003495CC0050B1
      E500429AD200266EAE00155796000E487F003386B6002A77A400135681002862
      8B0032688F00449ACC0051B9F000377EB000000000008E60360096683B009568
      3B0096683B0098693C00996A3C009C6C3C009F6E3D00A3703E00A7744000A66E
      3A00945A2A008651240078492100653C1C00543B2900C7C4C100BEBAB6006943
      2900855E43008963480068473000B9B3AF00D7D7D700707371007C7E7C009293
      92007576760074747400C8C8C8000000000070370100FFFFFF00FFFFFF00FFFF
      FF00A5805D00B18B6B00B18B6B00B18B6B00B18B6B00B18B6B00B18B6B00B18B
      6B003E3DE8003E3DE800DCCABB00FFFFFF00C5A890003E3DE8003E3DE8003E3D
      E8003E3DE8003E3DE8003E3DE8003E3DE8003E3DE8003E3DE800A5805D00FFFF
      FF00FFFFFF00FFFFFF00703600007A7A7A00000000000078D4001086D7007DBF
      E3004AA5D5001C85C200128AC60004B3E10003BDE60001D4F30001DAF50000E3
      FA0000E4FB0001DCF50001D5F30003B8E30004A7D9000F74B1001467A4007373
      7300E2E2E200C8C8C800EFEFEF00E2E2E200DBDBDB00A0A0A000B8B8B8000000
      00000000000000000000000000000000000052ADE00079CFF40093EEFC009FED
      FA0090EAFA0089EBFF0088EAFF008EE4F80082DCF40079D7F30070CFEE0078D8
      F30065C9F00071DBFF007AE1FF0068D2FA0057C7F90057BFF50042A1D800338F
      D0002A81C9002571B300145C9C000D4C8500328ABC002C7FAE000F5E8E002075
      A8002E81B2002E74A4003573A1003982B40000000000875E370091673C009268
      3C0093683C0094693C0095693C00966A3C00976A3C00996B3C009A6C3C009B6C
      3C009A6A3B00885226007E461A00724F3200CBC8C500C8BFB8007A502D007C5B
      4100755843005B3F2C005F422E00614C3E00A2A2A100A5A7A5006D6E6D00BEBF
      BF00A1A3A200B9B9B9007B7B7B000000000077410F00986E4600A6826000A682
      6000A6826000B28C6C00B38D6E00B38D6E00B38D6E00B38D6E00B38D6E00B38D
      6E00A98C8A00A98C8A00C6AA9200FFFFFF00C6AA9200A98C8A00A98C8A00A98C
      8A00A98C8A00A98C8A00A98C8A00A98C8A00A98C8A00B28C6C00A6826000A682
      6000A6826000986E46006D36040090909000000000002AB2E600228ACB00006D
      D2000074D60000A2E30000ABE60000BEED0000C7F00000D6F60000DCF80000E3
      FA0000E4FB0000DEF80000D7F60000C3EE0000B6E9000095D7000084CB007373
      7300CDCDCD00B7B7B700D8D8D800C7C7C7009F9F9F00C7C7C800000000000000
      0000000000000000000000000000000000004EA8DB008AF6FF0095F6FF00AAF3
      FD0092E8F800B9F9FF00A2F2FF0081E1FA0071D0EE0088E9FF0081E1F9006CD2
      F7006DD2F60079DEFC0070D8FB0060CDFA0068D4FE009CFBFF0063CEFF005D9D
      D0005092C8004689BE004585B9004692CA0071C3EE0071B9E1005393BA00317F
      AF003486B6003691C5003393C900377EB000000000008B623900956B3E00966B
      3E00976C3E00986C3E00986D3E009A6E3F009B6E3F009C6F3F009D6F3F009E70
      3F009E703E008C6337006D554100C7C5C300BEB5AD0071442200855A3A008C5F
      3E0082492000874A1D008353300070513A00A6A5A200D5D5D500CACBCA00D1D2
      D10086848200AAAAA9009495950000000000885A30009D754F00A8846200A884
      6200A8846200B18C6C00B48E7000B48E7000B48E7000B48E7000B48E7000B48E
      7000B48E7000B48E7000C7AB9300FFFFFF00C7AB9300B48E7000B48E7000B48E
      7000B48E7000B48E7000B48E7000B48E7000B48E7000B18C6C00A8846200A884
      6200A88462009D754F00693A0D00ABABAB0000000000249EDC000065CD00006D
      D2000074D60000A2E30017B4E90064D8F6006CDAF50086D9F20083D5EF009ED4
      ED009CD1EC0079CAE80079C8E8005EC2E60054BDE5001397D6000083CC007073
      7500ACACAC00C4C4C400A1A1A100A1A1A100B6B6B60000000000000000000000
      0000000000000000000000000000000000007FBDE00061C1E5008AF4FF008AF1
      FF00A6F5FF00B3F7FF0098EFFF0084E7FF007EE3FF006ECEEE0073D3F2007FE4
      FF007AE0FF006ED9FF0063D1FE006DD2FF0099E4FF007EDDFF0050B1E50048A8
      DA00429CCF003680B400337AAD0059B0D80064C7EF0067CBF5006BCAF40089D0
      F4008DCAEB003992C7003191C800377EB000000000008F663B00986F4000996F
      4000996F4000986F4000956D4000916C4000946F4300987144009A7344009B74
      45008B673D00AAA19700C1C1C000B6B1AA006E492B00795235007E5436006F3C
      19006D3712006A351200683312007345270093887F00D4D4D400E8E8E800BEB5
      AF00764C3200000000000000000000000000A785670095694000A9856400A985
      6400A9856400AF8B6B00B5907100B5907100B5907100B5907100B5907100B590
      7100B5907100B5907100C8AC9500FFFFFF00C8AC9500B5907100B5907100B590
      7100B5907100B5907100B5907100B5907100B5907100AF8B6B00A9856400A985
      6400A98564009569400067411D00C9C9C900000000000060CB001173D200A2E9
      FC006DDCF7002DB9E20025A5D5001B86C200128AC60004B3E10003BDE60001D4
      F30001DAF50000E3FA0000E4FB0001DCF50001D5F30003B9E40004ABDC002283
      C0004883BC00B0B9C100C3C3C300DDDEE2000000000000000000000000000000
      00000000000000000000000000000000000000000000E7F1F8008BC3E30048A4
      D70055B5E2006ACEF30079DCFA007FE3FE0081E5FF006CCDEF0077DEFF007EE8
      FF007EE7FB0085E3FF0081DDFF0052B5EA004CA9DD00469AD400449AD7003183
      C6002B7CC1003B90CA002A8CC8001C75AA003386B7002877A500145782002864
      8E0032668E004295C50051B9F000377EB000000000008E673C009475490081A2
      860091906B00A77C4800AE804A00B4834B00B2804800B1804800B07F4700B17F
      4700A98B6800F3F3F300D9D8D5009C764B00BB905E00AC805500885327007945
      1E006C3D1A0061351600572C11005C30140076635600E9EAE900F5F5F500A093
      8C005A362300000000000000000000000000CDBBAA008A5A2D00AA886700AA88
      6700AA886700AE8B6B00B6927400B6927400B6927400B6927400B6927400B692
      7400B6927400B6927400C9AD9700FFFFFF00C9AD9700B6927400B6927400B692
      7400B6927400B6927400B6927400B6927400B6927400AE8B6B00AA886700AA88
      6700AA8867008A5A2D006B4F360000000000000000001D7ED60089E0F90036D4
      F4002BBAE3001480C3000681CF0000A3E30000ABE60000BEED0000C7F00000D6
      F60000DCF80000E3FA0000E4FB0000DEF80000D7F60000C3EE0000B7EA0000A0
      E1000096DD000D83CC000A73BD00C6D0E4000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D0E6
      F200A2CEE8006AB5DE0049A7DB004AAFE5004BB1E7004CB3EA004CB4EA004EB4
      EA004EB2E70047A7E00044A2DC00429FDE0049A7E5004CA6DB0048A9E5003B9B
      DC0043A6E4004CB3EA002A95D3001B7AB100328ABB002C7FAE000F5E8E002075
      A8002E82B3003079A9003575A3003A84B70000000000A2754300B0976C009FDC
      CC00A3BBA300A4794500A2774400A3784500A4784500A4784500A5784500A679
      4500A1764300BCA89000A9886200AB835400AB825300A1754300996A3D007E48
      1F007A4017008043180085471B0079421B00714E3600D4D4D300F1F1F100DBDA
      DA00847C77009D9C9C000000000000000000000000007C471500A8856300AB89
      6900AB896900AB896900B3907100B7937600B7937600B7937600B7937600B793
      7600B7937600B7937600C9AE9800FFFFFF00C9AE9800B7937600B7937600B793
      7600B7937600B7937600B7937600B7937600B3907100AB896900AB896900AB89
      6900A88563007D48160085796C0000000000000000003ED1F50029CFF8001593
      D7000065CD00006BD0000070D1000095D90014A3DD0054BFE6005CC2E60079C8
      E8007BCCE9009ED4ED00A0D6EE0086DAF2008BDEF5006FDDF70064D6F50017AB
      E6000096DF000083D800007DD600007CD5009ECEE80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000054BAF0005AB8EA0072C6EE007CCAEE00A8D0E600AFDEF600B3DC
      F40098D5F40090D9FA0091D9FA007DD2FA0075CBF70071B9E1005D9CC2003884
      B2003486B6003691C5003393C900377EB00000000000996F4000A0794600A9A0
      79009D7C4D00997545009875440098754400997544009B7544009D774500A178
      4500A3794500A77D4A00B1895800AD855400A5794600A3764300A17442009E71
      420082522C006833110065311000632F0F006D3C1E00876C5B00CCC9C700E3E4
      E300DAD6D200C3C3C100000000000000000000000000A37F5F00966C4300C0A6
      8E00FFFFFF00C1A89000AF8D6E00B6937600B8957800B8957800B8957800B895
      7800B8957800B8957800CAAF9900FFFFFF00CAAF9900B8957800B8957800B895
      7800B8957800B8957800B8957800B6937600AF8D6E00C1A89000FFFFFF00C1A8
      9000966C43006C3B0C00C6C6C6000000000000000000CCF2FB0031CEF6000261
      C700005CC5000C68C5003B87CC005F9CCD005596CA002F8BC5002385C1001C80
      BE001D85C1001F8FC6002093C90027A0D2002CABD9003DC2E70051CBEC0098E4
      F900A3E6FB0056B6E9001087DA00007BD50021B5E50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000055BCF20059B6E9008DCAEC00B4E6FB00ACECFB009CECFF0085E7
      FF007BDBF8006ECBEC006BC7E90064BFE40063C4EA005EC8F5006BCAF40083CF
      F40091CFF1003E98CD003090C800377EB000000000009A744300AC834B00B286
      4C00B7894E00BA8A4E00BB8B4F00BC8C4F00BC8C5000BB8C5000B98B5000B589
      4F00B1864E00AD854F00AD885500A5804E009E79480099754600957042009870
      4100997040008D623A0069371700612D0F0061301400693C2200653A21006037
      21005C2E170000000000000000000000000000000000DFD3C90083502000FBFA
      F800C2A99100AE8C6D00AE8C6D00B2907200B8967800B9967A00B9967A00B996
      7A00B9967A00B9967A00CBB19B00FFFFFF00CBB19B00B9967A00B9967A00B996
      7A00B9967A00B9967A00B8967800B2907200AE8C6D00AE8C6D00C2A99200FFFF
      FF00835020007E5D3F00000000000000000000000000C7D1E50094A9CE000377
      CB000484D70000ABE60000B5EA0000C7F00000CFF30000DCF80000E0F90000E4
      FB0000E2FA0000D7F60000CEF30000B8EB0000ADE7000098DF00018ED7001480
      C2002097CD0034D6F40039DDFA0087E1F9001CBCEC0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000064BEF00087DAF50069C2EE0065BCEB007ABFE30081CAEE005BB5
      E70052B4EB004DAEE5002C90CC001C76AB003386B7002877A600135783002A66
      910030678F004194C40051B9F000377EB00000000000A77A4400AF947200B3B2
      B000B89F7E00AB814900AB814900AC814900AC814900AC814900AC814900AE81
      4900AF824A00B2844A00B4854B00B6864B00B7874C00B9874C00B9884E00B284
      4C00A97E4A00A1784700977145006A452700522B1100522E1700502E1A004A29
      16004C2C1B000000000000000000000000000000000000000000A3815F009367
      3D00AF8E6F00AF8E6F00AF8E6F00AF8E6F00B3927400B8967A00BA987B00BA98
      7B00BA987B00BA987B00CCB29C00FFFFFF00CCB29C00BA987B00BA987B00BA98
      7B00BA987B00B8967A00B3927400AF8E6F00AF8E6F00AF8E6F00AF8E6F009367
      3D006F3F1300D3D0CE000000000000000000000000006C99CB000A74CB000074
      D600007AD90000ABE60000B5EA0000C7F00000CFF30000DCF80000E0F90000E4
      FB0000E2FA0000D2F30000CAF00000B8EB0000ADE7000096DF00008CDB000070
      D2000276CD002CC3EB0033DBFA004FD7F9002FCCF50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005BB6E70058B3E60067B5E1007AC5EA0084CFEB0080D9FC0058C0
      F40050BAF3004CB3EA002B95D2001B7AB100328ABB002C7FAE000F5E8E002076
      A8002E83B400307CAC003474A3003A84B70000000000A1774300BEA88A00D3D2
      D000C7B19300AE844A00AE844A00AE844A00AE844A00AF834A00AF834A00AF83
      4A00AE834A00AE824A00AD814900AC804900AB7F4800A97D4700A77B4700A67A
      4600A67A4500A7794500A7794500A879450098643700783B1400753711007435
      1000773A18000000000000000000000000000000000000000000000000008B5F
      35009C744E00AF8F7000AF8F7000AF8F7000AF8F7000B2917300B7957800BB98
      7C00BB987C00BB987C00CCB29D00FFFFFF00CCB29D00BB987C00BB987C00BB98
      7C00B7957800B2917300AF8F7000AF8F7000AF8F7000AF8F70009C744E007848
      1B00AD9F9300000000000000000000000000000000000063CD000068CF0068B6
      EA009DD8F500C6F3FD00AAEFFC007FEAFC0067E8FB0052C4E6002DA3D20021A5
      D40031A8D6004BB2D90058C2E30091E9FA00B2F1FD00CBF1FD00A5DEF700157F
      D700006DD000029BE20011A0E00031CFF600CCF2FB0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000055BAF0005AB8EA0072C6EE007CC9ED00AED3E700B4E1F700BAE1
      F7009ADAF70091D9FA0091D9FA007DD2FA0077CEF70071B9E10067A5CA003E89
      B6003485B5003691C5003393C900377EB00000000000A47A4400AE9065007368
      570093764D00B1874C00B1874C00B1874C00B1864C00B1864B00B1864B00B186
      4B00B1854B00B0844B00AF844A00AE834A00AD814900AB804900A97E4800A87C
      4700A57A4600A3784500A07644009E7443009B7142008E633800623214005322
      0A0057291200000000000000000000000000000000000000000000000000DCCD
      C20085542600A07A5500B0907200B0907200B0907200C4AC9500B0907200B493
      7500B7957900B9987B00BA997D00FFFFFF00BA997D00B9987B00B7957900B493
      7500B0907200C4AC9500B0907200B0907200B0907200A07A55007C4B1D009C88
      750000000000000000000000000000000000000000001274D20078C0ED0082E6
      FC0057E2FB0039E2FB0039E2FB0039E2FB003AE2FB0046F6FD0048FBFF003EE7
      F6003DE3F30025ADD8001C99CE0031CEEF0039E1FA0042E2FB005BE3FC00B6EF
      FD007EC4EE00009EE6002ACDF600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000055BCF20059B5E80094CFEF00B7E8FC00A4EBFB0099EDFF0085E7
      FF0082E5FF007BE1FF0078DFFF006FD7FC0069CFF60064CAF30067C9F40083D0
      F60096D4F5003E97CC003090C700377EB00000000000A77D4500B48A4C00B48A
      4C00B48A4D00B48A4C00B48A4C00B4894D00B4894D00B4894D00B4894D00B388
      4C00B3884C00B2874C00B1864B00B0854B00AF844A00AE824A00AC814900AA7F
      4800A87D4700A67B4700A3794500A17744009E7443009B724200976D3F007144
      2200572812000000000000000000000000000000000000000000000000000000
      0000D3C3B400855426009E765000B2927400C5AD9600FFFFFF00B2927400B292
      7400B2927400B2927400B2927400B2927400B2927400B2927400B2927400B292
      7400B2927400F6F2EF00C5AD9600B29274009E7650007F4D1F009B836E000000
      000000000000000000000000000000000000000000004FD7F90032D9FA0033DB
      FA0033DBFA0033DBFA0033DBFA002CC8EE001DA1D5001A99CF001EA3D60041EC
      F70020A7D60036D6EE0048FAFF002ECDF10033DAF90033DBFA0033DBFA0033DB
      FA0032D9FA0034CAF50000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000064BEEF008FDEF40095F3FE00A6F5FF0090E6F70083E1F600A9F2
      FD00A1EFFC0085E6FF0085E7FF0088E9FF0087E9FF0065CBF2005DC9F80071DB
      FF007AE0FF0052C3F90057C5FC00377EB00000000000B9996E00B0884F00AF86
      4D00B0874E00B1885000B2895100B38A5200B48B5300B48B5400B58C5500B58C
      5600B58D5700B58D5700B48C5800B38B5700B28A5700B1885600AF875400AD84
      5300AA825100A77F4F00A57D4E00A27A4B009E7749009B74470098714400966F
      44009E7F65000000000000000000000000000000000000000000000000000000
      000000000000D9CCBF008C60360095694000F7F4F000C6AE9800B3947600B394
      7600B3947600B3947600B3947600FFFFFF00B3947600B3947600B3947600B394
      7600B3947600BB9F8500F3EDE8009569400079461700A7948200000000000000
      0000000000000000000000000000000000000000000029CFF80021BDED002DD1
      F60030D7F90030D7F90030D7F9003AE1F60042EEF90036D5ED0039DCF0003ADE
      F10038DAF0003EE7F80031D2F2002FD5F80030D7F90030D7F90030D7F90022BD
      ED0021BDED0040D1F60000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000005FBEE7009EF9FF00ABF3FC0096EDFA0090EDFD00B6F8FF0082DD
      F3007ADAF5007CDBF6007BDBF50086E8FF0071D7F9007CDFFB007ADEFC005ECB
      FA005BC9FA0064D1FD0085ECFF003B87B9000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A5836300845223009A714A00AF8F7000B394
      7700B3947700B3947700B3947700FFFFFF00B3947700B3947700B3947700B394
      7700AF8F70009A714A00845223007C4D2100C8BBAF0000000000000000000000
      000000000000000000000000000000000000000000000000000075DFF90020BD
      EF0013A1E1000A8ED700109ADD0020B9EB002CCEF20043F2FC003BE4F7003CE7
      F80033D6F30022BFEF0021BDEE0016A8E400109ADD00098ED70013A1E1002ACD
      F60075DFF9000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000051A6D5004CA7D80086EDFC008AF1FF00B8F9FF00A6F3FF008DEB
      FF007EDDF7007CE2FF0078DDFC0069CCF1006FD7FA0067D4FF0064D2FF0093E0
      FF00C7EFFF0098E4FF005DC0F200DBEBF5000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000DCD1C5009A734F007F4A18008F61
      35009C734D00A7836100AF8E6F00FFFFFF00AF8E6F00A78361009C734D008F61
      35007F4A180089623D00C0B09F00000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098E6
      FA0066DBF80026C8F5001FBAEE0013A0E1000C93DA000C93DA000889D6000584
      D3000889D6000C93DA000C93DA0018ABE6001FBAEE0039D1F60066DBF8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000008BC3E3004CA7D8005DBDE5007DE1FA0089EBFF0086E8
      FF0086E8FF0076D5F2006CCDEF0076E0FF0066D3FB0095F4FF00B2FCFF00ABEA
      FF0089D7FC004CA9DD008BC3E300000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CDBB
      AA00A7856700885A300077410F0070370100763F0D00835529009B775700BDA7
      9400E2DBD4000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0E6F200A2CE
      E800A2CEE80074B7DE0074B7DE0074B7DE00A2CEE800ADD4EB00D0E6F2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D9D9D900BABABA00B0B0B000AFAF
      AF00B4B4B400BFBFBF00D7D7D700000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000D3DED10087A98500487C
      4600306B2D00195A1600306B2D004B7D480087A98500D5DFD300000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000D7D7D700D3D3D300D7D7D700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000007D949E0042849F003188AA002F88AC002D86
      AA002F7FA00036748E004E6A7600727A7E00A6A6A600D5D5D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000DEE6DE00608D5E0017641300127A0E000F85
      0B000F870A000F890A000F870A000F850B00127A0E0017641300608D5E00DEE6
      DE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCDCDC00B6B6B6009595950080808000777777008080800095959500B6B6
      B600DCDCDC000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D1D3D5004F92AF0044B9E90055CAFA0054C6FA0056C6FB0058CE
      FE0054CAFA004DC5F60044BDEF0034A0CD00307694004C6872008C8C8C00CECE
      CE00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BBCEBA0024642100127D0D001591100048C5430074E7
      6F008BF687008CF788008BF6870074E76F0048C5430015911000127D0D002464
      2100BDCFBD00000000000000000000000000000000000000000000000000AAA5
      A000B6986800C39E6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E
      6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E
      6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E6500C39E6500B698
      6800A9A4A0000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D4D4
      D400B6987B00BC824B00D7863B00EB8E3600D5843800AD733C00836548006B6B
      6B009C9C9C00D4D4D40000000000000000000000000000000000E2CBC300C798
      8B00B5776300AC654900B06A4500B06A4500AC644800AA614C00B06C5800BF88
      7800AE7E6E00458AA70059CBFB0062CFFA0053A6E6003462C8002252C3004498
      E0005ACAF50059C7F50059C8F50057C9F80054CAFB0042B6E6003081A4005364
      6B00A7A7A7000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BACDB800186114000F860B003ABE35007FEF7B0076DA720095C8
      9200AFBDAE00BABCBA00AFBDAE0095C8920076DA72007FEF7B0038BD34000F86
      0B0018611400BFD0BE000000000000000000000000000000000000000000B597
      6700E2B16600DAB67E00DCB06900D8B68000DEB57400DBB77F00E2B16600E2B1
      6600E2B16600DDB47300DCB37400DAB47900DBB37400D9B37800DBB67C00E2B1
      6600E2B16600E2B16600DBB67D00DCB37400DAB98700DEB67700E2B16600E2B1
      6600B5976700000000000000000000000000000000000000000000000000DADA
      DA00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECECE00CECE
      CE00CECECE00CECECE00CECECE00CECECE00CECECE00CBCBCB00B7AEA700BB89
      5700F19F4F00F9BC7900FCD6A200FFE5BC00FCD5A100F9BC7900F2A14F009C69
      3800696159009C9C9C00DCDCDC000000000000000000AC664C00AC5D2400BF7E
      2D00CA923500D19B3B00D19C3C00D19B3B00CF983700CD933100C7892900BA75
      20005D65560057C8F70069CFF80065D0F800467DD4005065C7002642BA003066
      CB0061CEF7005DC7F4005CC8F4005BC7F4005AC7F5005AC9F80056C9FB003AA8
      D60038677A009393930000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DFE7DF00246421000F880B004DD148006ADF6500A4BFA300D3D3D300EBEB
      EB00F8F8F80045454500F8F8F800EBEBEB00D3D3D300A4C0A3006BDF66004DD1
      48000F880B0024642100DFE7DF0000000000000000000000000000000000C19B
      5F00E0AE6000E6BF8100EACC9C00E7C18400EBCC9D00E0AE6000E0AE6000E0AE
      6000E0AE6000E4C49200E3C79A00E8C69000E8C68F00E5C59100E2C08B00E0AE
      6000E0AE6000E0AE6000DAB47900E8C89500E0C49800E7C89700DBB77D00E0AE
      6000C19B5F000000000000000000000000000000000000000000DADADA009393
      93006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F
      6F006F6F6F006F6F6F006F6F6F006F6F6F006F6F6F0068686800A5724100F6A6
      5500FDC88700FFD8A300FFDEAD00FFE0B100FFDEAD00FFD9A300FDC88600F6A7
      55009C6938006B6B6B00B6B6B60000000000C7988B00BD792700CD953300D19C
      3C00D4A24400D6A64800D7A74A00D6A54900D4A24400D19C3C00CD953300C98D
      280055A1B8007CD7FD006ACDF6006CD1F80054A9E7004978D1004573CF0050A3
      E40066CEF60063CAF60062C9F50061CBF6006BD0F9007CD2F7006ECFF7005ECD
      FB004FC3F4004166770000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000608C5E00117E0D0038C4330058D45300BAC5BA00F2F2F20045454500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0045454500F2F2F200BAC5BA0058D4
      530038C43300117E0D00618E5F0000000000000000000000000000000000BF99
      5B00DEAB5A00E3B77200E8C48B00E3B77200E8C48B00DEAB5A00DEAB5A00DEAB
      5A00DEAB5A00E2B77100E3BC7E00E5BE7E00E5BE7E00DFBC8400D9B06F00DEAB
      5A00DEAB5A00DEAB5A00E1BC8200DDB57300DDBE8C00E2BF8700E0BC8100DEAB
      5A00BF995B00000000000000000000000000000000000000000008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D700528F9400F29D4700FDBF
      7600FFCC8B00FFD19600FFD69E00461E0000FFD69E00FFD19600FFCC8C00FDBE
      7600F29C4600836548009595950000000000B5776000CB913000D29C3C00D6A4
      4700D9AB5000DBB05600DCB15800DBB05600D9AC5000D5A54700D29C3D00CC93
      310064B5D20087DAFB006FD0F90070D1F90071D4FA006AD0FA0069CDF6006BCF
      F70068CBF50068CCF6006ACEF80050B6E00065A7C20080C0DB006CC5E90070CE
      F50063D1FE003890B40000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000D5DF
      D3001764140011A10B0056DA5100ACC6AB00FAFAFA00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FAFAFA00ABC5
      AA0056DA510011A00B0017641400D5E1D500000000000000000000000000BF9A
      5D00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DFB4
      6D00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE
      5F00DFB46D00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE5F00DEAE
      5F00BF9A5D00000000000000000000000000000000000000000008A0D70029D3
      FE002AD4FE002AD4FF002BD3FF002BD4FF002BD4FF002AD4FF002AD3FE0029D3
      FF0027D3FE0026D3FF0024D2FE0021D2FE001FD1FE009D9E7600F9AC5B00FFC2
      7A00FFC68100FFCA8800FFCD8D00461E0000FFCD8D00FFCA8800FFC68100FFC2
      7A00F9AC5B00AD733C0080808000D7D7D700B26F5500CF983700D4A14400D9AB
      4F00DDB35A00E0B86100E1BA6400E0B96100DDB35A00D9AC4F00D4A24400CF98
      37009FD1E100BDEDFE00AFD7E700AD9B9700A6807500A2B1BA0099DEFA008EDA
      FA0081D4F80079D3F9006AD2FD00467D9300D1D2D20000000000B6D3DF0071C8
      ED0077D9FF00409AC00000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000085A7
      8300117C0E003ACB350056BD5300F2F2F20045454500FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0045454500F1F1
      F10055BD51003ACB3500117C0E0087A98500000000000000000000000000BE98
      5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900E0C1
      8E00DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB
      5900E0C18E00DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB5900DCAB
      5900BE985900000000000000000000000000000000000000000008A0D7002DD5
      FF002FD5FE002FD5FF0030D5FE0030D5FE0030D4FE002FD4FE002ED5FF002ED4
      FE002CD4FE002AD3FE0028D3FF0025D3FE0024D3FE00CF965000FCB86B00FFC1
      7B00FFC98A00FFD09800FFD5A000461E0000FFD39D00FFCD9100FFC78500FFC2
      7A00FCB86B00D584380077777700D3D3D300AD664C00D09B3B00D6A64900DCB0
      5600E0B86100E3C06A00E5C26E00E3C06A00E0B86100DBB05600D6A64900D19B
      3B00B6D6DB00D0F8FF00BEB7B700C6856F00B66C5200B4786400B9E2F300B6E9
      FE00B2E5FB00ABE3FC009BE4FF00578498008C8C8C00CACACA008BA3AD0073CF
      F50074D9FF006A9BAE0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000467A
      45000C94070030C62B00A5D1A300FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A5D1A30032C72C000C940700487C4600000000000000000000000000BB91
      4D00D79F4300D79F4300D79F4300D79F4300D79F4300D79F4300D79F4300DCB9
      8000D79F4300D79F4300D79F4300D79F4300D79F4300D79F4300D79F4300D79F
      4300DCB98000D79F4300D79F4300D79F4300D79F4300D79F4300D79F4300D79F
      4300BB914D00000000000000000000000000000000000000000008A0D70034D5
      FE0033D5FF0035D6FE0036D6FF0036D6FE0036D6FE0035D6FE0034D5FF0032D6
      FF0031D5FF002FD5FE002DD4FE002AD3FE0028D3FF00ED933B00FFC68200FFD2
      9C007F644F00917967009E897900A59283009E897900917967007F644F00FFCC
      8E00FFC37D00EB8E360080808000D7D7D700A6594100D09A3C00D7A74B00DCB2
      5700E1BA6400E5C26E00E8C87500E5C26E00E1BB6300DDB15800D7A74B00D19C
      3C00AEC5BC00D3F5FF00C3D4DC00BF968800AF857800455767007F9DA800B5E0
      F300BFEBFF00B9E9FF00B6E8FD009EDAF20073A0B3006693A50061B6D8006CD0
      FB0065A1BA000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002D69
      2A0009A303001ABB1400DBEBDA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00577A55001F5E1C006E936C00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DBEBDA001BBB150009A203002E6A2C00000000000000000000000000BA92
      4E00D8A65000DEB67200D8A54F00D7A24900D7A24900D7A24900D7A24900DDBC
      8400D7A24900D7A24900D7A24900D7A24900D7A24900D7A24900D7A24900D7A2
      4900DDBC8400D7A24900D7A24900D7A24900D7A24900D7A24900D7A24900D7A2
      4900BA924E00000000000000000000000000000000000000000008A0D70041D8
      FF0039D7FF003AD6FE003BD6FF003BD7FE003BD7FF003AD7FF0039D7FF0037D7
      FF0036D6FE0033D5FE0031D5FF002ED4FE002BD3FE00D1995300FDCD9500FFDE
      B700FFDEB600FFDDB500FFDDB4009E897900FFDBAE00FFD9AB00FFD7A700FFD4
      A200FCC48400D7863B009595950000000000A75D4600C5893600D6A64900DBB0
      5500E0B96100E4C06900E5C26E00E3C06A00E0B96100DCB05600D6A54900D09B
      3B00B8A26500CAEBFA00D4F3FF00C8E7F500C6E1ED00043B6100113853005C77
      7A0080B3AC00AADDE600BAE8FD00B8E9FE00B8EEFF00B5EEFF00B1EDFF0068A6
      C100AAAAAB00000000000000000000000000CBCBCB0094949400939393009292
      9200929292009191910090909000909090008F8F8F008E8E8E008E8E8E00195B
      160012B20C0011B60B00EFF2EF0045454500FFFFFF00FFFFFF00FFFFFF00FFFF
      FF001E5D1B003DB639001B5C1800FFFFFF00FFFFFF00FFFFFF00FFFFFF004545
      4500EFF2EF0011B60B0012B10C001A5A1700000000000000000000000000C0AD
      8C00E8E6E300E9E6E200E4D1B100D59E4100D59E4100D59E4100D59E4100DCB9
      7F00D59E4100D59E4100D59E4100D59E4100D59E4100D59E4100D59E4100D59E
      4100DCB97F00D59E4100D59E4100D59E4100D59E4100D59E4100D59E4100D59E
      4100B98F4900000000000000000000000000000000000000000008A0D70054DC
      FE003FD7FE003FD8FE0040D8FE0040D8FF0040D8FE003FD8FE003ED7FE003DD7
      FF003AD7FE0037D7FE0035D5FF0033D5FE002FD5FE00A3A77F00FBC78F00FFE3
      C100FFE3C100FFE2C000FFE2BE00947D6C00FFDFB900FFDDB500FFDBB000FFD9
      AB00FBBD7C00BC824B00B6B6B60000000000BA7F6D00B36B2C00D4A24400D9AB
      4F00DDB35A00E0B96100E1BA6400E0B86200DDB35A00D9AC5000D4A24400CF98
      3700C88E2D00B0B9A100D4F1FC00D6F4FF00CEF0FE003A6D900008426A000A35
      500016412500458C6200B4E5EF00C3ECFF00C3EBFD00C3ECFC00BDEDFF0085B9
      CD006D737400C6C6C600000000000000000097979700FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF002F6A
      2C001CB3160006B00000DBEBDA00FFFFFF00FFFFFF00FFFFFF00FFFFFF00AAAA
      AA002E512C001D5C1A002D522B00BABABA00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00DBEBDA0006B000001CB31600245F2100000000000000000000000000B28D
      4E00C58F3100CA9D4F00E3E2E000D7AF6A00D2993900D2993900D2993900DAB6
      7A00D2993900D2993900D2993900D2993900DCB36E00EFE2CB00DCB46E00D299
      3900DAB67A00D2993900D2993900D2993900D2993900D2993900D2993900D299
      3900B78C4400000000000000000000000000000000000000000008A0D70069E0
      FE0056DDFF0044D9FE0045D9FF0045D9FF0045D9FF0044D9FE0043D9FF0041D8
      FF003FD8FE003CD7FE003AD7FF0036D6FE0033D6FF006DBDBC00F4AE6600FEDF
      BB00FFE7CA00FFE7C900FFE6C800947D6C00FFE3C200FFE2BE00FFDFB900FED5
      A600F3A55A00B6987B00DCDCDC0000000000DABBB1009C442200CD953A00D6A4
      4700D9AB5000DBB05500DCB25800DBB05500D9AC5000D5A54700D8A75400D49F
      4A00D0984000BF842300B1C4B700D7F2FD00D9F3FE0093BBD0000C4D77000D4D
      790008375B0029613C00B8E6EA00BEECFF0083DCFB0076D9FA00A7E4FC00BDEE
      FF008ABFD4005D6F7500000000000000000099999900FFFFFF00E4E4E400E5E5
      E500E6E6E600E8E8E800A4A4A400EAEAEA00ECECEC00EDEDED00EEEEEE004579
      420036BB310006B00000A5D1A300FFFFFF00FFFFFF00FFFFFF00A9A9A9004545
      45009B9B9B00FFFFFF00AAAAAA0046464600B8B8B800FFFFFF00FFFFFF00FFFF
      FF00A4D0A30006B0000036BB310030642D00000000000000000000000000B385
      3600CB8C1F00CB8C1F00CFB58700DDD5C800D0983800D0973500D6B68000DECF
      B700CF953000CF953000CF953000D6AA5F00EBEAE900DFD2BB00E7DAC500CF95
      3000D8B47500CF953000CF953000CF953000CF953000CF953000CF953000CF95
      3000B5893E00000000000000000000000000000000000000000008A0D70074E3
      FF0072E2FF0059DDFE004ADAFE004ADBFE0049DAFF0049DAFE0048D9FE0046D9
      FF0043D9FF0041D8FE003ED7FE003AD7FE0037D6FE0033D5FE0098AD8E00F9BD
      8100FEE3C400FFEBD300FFEAD100FFE9CF00FFE7CC00FFE5C800FEDBB300F8B6
      7400BC895800D4D4D400000000000000000000000000A4573D00A8572600D19C
      3C00D4A24400D6A64900D7A74B00D6A64900D4A24400DCAC6700D3A17300FDDE
      CF00FDDECF00CE914C00BA7A1800AFB7A100D6F1FC00D9F2FC002E6E9700125B
      8800054B7C0048605D00CCE8F50087DFFC0043CFF90015C3F8004FD0F900C3EC
      FD00C4F3FF009BD3E90000000000000000009B9B9B00FFFFFF00E4E4E400E5E5
      E500E6E6E600E7E7E700A4A4A400EAEAEA00EBEBEB00EDEDED00EEEEEE0081A3
      800041AE3D000AB2040050B94C00F2F2F20045454500FFFFFF00454545009F9F
      9F00FFFFFF00FFFFFF00FFFFFF00ACACAC0046464600B8B8B80096969600F1F1
      F10050B94C000AB2040041AE3D004E704C00000000000000000000000000B182
      2F00C8871500C8871500C6881D00DBD2C400DBC8A800DDD5C600D7D0C300DFDE
      DC00CD9B4300C8871500C8871500D9C39F00D8CAB100C88F2B00E3E0DD00D0A0
      4D00D4AB6500C9891900C9891900C9891900C9891900C9891900C9891900C989
      1900B2833200000000000000000000000000000000000000000008A0D70081E5
      FE0081E6FF0076E3FE005DDEFE004EDBFF004EDBFF004EDBFF004DDBFF004ADA
      FF0048DAFE0045D9FE0041D9FF003ED8FF003BD7FF0037D6FE0043CEEC0098AD
      8E00F4B27000FCD2A700FEE4C800FFECD600FEE1C100FBCE9E00F4AD6600A773
      4300BAB1A90000000000000000000000000000000000E0C6BF00973C2000AF63
      2800CF973700D09B3B00D19C3C00D19B3B00CF984C00FDDECF00FDDECF005F87
      B8005F79A400FDDECF00CF924100B96F0400B19F6C00BCD9DE00CDECF80087A0
      AF009AA5AC00575A5C008999A00088DEFB004FD2F9003FCEF9005CD4FA00CAEE
      FE00CBF2FF00B1E4F9000000000000000000A2A2A200A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A4008C97
      8B002675220047D0420008B00200ACC6AB00FAFAFA00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00ACACAC0045454500FAFAFA00A8C4
      A70008B0020048D143002574210075807400000000000000000000000000AF7F
      2900C5810B00C5810B00C5810B00C7964100DDD6CA00CAAA7300C0811100D7C5
      A700DBC7A500C98B1D00CC953300E0DEDC00CCA45D00C8881800D1B98F00DACA
      AD00D6B27000CC912900CC912900CC912900CC912900CC912900CC912900CC91
      2900B3873A00000000000000000000000000000000000000000008A0D7008CE7
      FF008BE7FF008CE8FF007FE5FF0065DFFE0054DCFE0052DCFF0050DBFF004EDB
      FF004CDAFF0049DAFE0046D9FF0041D8FF003ED8FE0039D7FE0036D6FE0031D5
      FF006BBDBC00A2A77F00D0985300ED933B00CF9650009B9E7600538B8D006D6D
      6D00CECECE000000000000000000000000000000000000000000D3AFA500973C
      2000A8572300C88B2C00CB912F00CD924500FDDECF00769DCA005186C100457A
      B8004479B60069719000EBD6D000B86C0200AA5A1300D1B9B100D8EFF800BEDE
      EA00BCBCBB007776760059606400BBD6E000A7E8FD009FE5FD00C4EEFF00D2F8
      FF00BAECFD00A6B9C1000000000000000000A0A0A000FFFFFF00E4E4E400E5E5
      E500E5E5E500E6E6E600A4A4A400E6E6E600E7E7E700E8E8E800E9E9E900E9E9
      E9004672440057C6530025C0200024B41F00BAC5BA00F2F2F20045454500FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF0096969600F2F2F200BAC5BA0024B4
      1F0027C1210057C65300639061008B8B8B00000000000000000000000000AD7B
      2400C27C0100C27C0100C27C0100C27C0100BD790100C27C0100C27C0100CDA7
      6400E7E6E400CB943200D6B57C00E1DCD500C1800B00C8891900C7943800E1DF
      DD00D7B57900CC922B00CC922B00CC922B00CC922B00CC922B00CC922B00CC92
      2B00B3883B00000000000000000000000000000000000000000008A0D70096EA
      FF0096E9FF0096E9FF0094E9FE008AE7FF006FE2FF0057DCFE0054DCFE0053DC
      FF0050DBFF004DDAFE0049DAFF0045D9FE0041D8FF003DD7FF0038D6FE0033D6
      FE002ED4FF002AD4FE0026D3FF0021D2FE001CD1FF0018D0FE000A99CC007474
      7400D1D1D100000000000000000000000000000000000000000000000000E0C6
      BF00A15139009A412000B1652100FDDECF00699CD3005181BE005F8DC60073A0
      D30079A5D7007AA7D8007AA7D90091848E00ECDED80000000000000000000000
      0000B6B9BB00A5A3A30052565700859FA900BFE2F100C4E6F600BDE3F400AFC9
      D400CCD2D500000000000000000000000000A2A2A200FFFFFF00E5E5E500E5E5
      E500E5E5E500E5E5E500A4A4A400E6E6E600E6E6E600E6E6E600E6E6E600E7E7
      E700919A91002669230072E36E0025C01F0025B32000A4BDA300D3D3D300EBEB
      EB00F8F8F80045454500F8F8F800EBEBEB00D3D3D300A2BDA10025B3200025C0
      1F0071E26D0026692300E0E9E0008D8D8D00000000000000000000000000AC79
      2400BF780000BF780000BF780000BF780000BF780000BF780000BF780000CCA0
      5500E2D7C500DBBF8F00E6DECF00D4B88700BF780000C6871A00C88B2200D9C7
      A900DCC7A200CC943200CC943200CC943200CC943200CC943200CC943200CC94
      3200B3894100000000000000000000000000000000000000000008A0D700A0EC
      FF00A1ECFF009FECFF009EEBFF009BEAFF0097EAFF007DE5FE0063DFFF0057DC
      FF0053DCFF0050DBFF004CDBFF0048D9FF0043D9FE003FD8FF003AD7FF0035D6
      FF0030D5FF002CD4FE0027D3FE0023D2FE001ED1FE001AD1FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      000000000000D6B4AA00B47461006A96C8005282BE006895CC007AA5D8007CAA
      DA007FACDC0081AEDD0082AFDE0081AEDE00D5E3F20000000000000000000000
      0000F0F0EF00B4B6B9008672620067626000CBCBCB0000000000000000000000
      000000000000000000000000000000000000A4A4A400FFFFFF00E8E8E800E9E9
      E900EAEAEA00ECECEC00A4A4A400ECECEC00EEEEEE00EFEFEF00EFEFEF00EFEF
      EF00A4A4A400B0C3AF00236F20007AE875004AD445000DB2070053B94F0093BC
      9100AFBDAE00BABCBA00AFBDAE0092BD900053B94F000DB207004AD445007AE8
      7500236F2000A9BBA900FFFFFF008F8F8F00000000000000000000000000AA78
      2500BD750000BD750000BD750000BD750000BD750000BD750000BD750000CC9F
      5600CEA46000F1F0EE00F0F0EF00C2892C00BD750000C2811500C98D2C00CB9F
      5600E8E7E500E6DFD300D4AB6700CD963C00CD963C00CD963D00D3AD6D00CD98
      4000B38B4800000000000000000000000000000000000000000008A0D700AAEE
      FF00AAEEFF00AAEDFE00A8EDFF00A5ECFF00A1ECFE009DEBFF008DE8FF0075E2
      FF005DDDFE0052DCFF004EDBFE004ADAFE0046D9FE0041D9FE003CD7FF0037D6
      FE0032D6FF002ED4FE0029D3FF0024D3FE001FD2FE001BD0FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      00000000000000000000E0E3E7005683BC006795CC007AA6D80080ABDC0084B1
      DF0087B4E2008AB6E4008AB7E40089B7E300AAC9E90000000000000000000000
      000000000000CAAB9200DC813C008D470F006D6C690000000000000000000000
      000000000000000000000000000000000000A7A7A700A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A4007C8F7B00236520005CC9570089F4840054D94F0029C3
      240015B80F0006B0000015B80F0029C3240054D94F0089F484005CC957002365
      20007D907C00A4A4A400A4A4A40091919100000000000000000000000000A976
      2600BA720000BA720000BA720000BA720000BA720000BA720000BA720000CA9D
      5600BA750900EFEAE200E5D7C000BA710000BA720000BD770900CA903600CA90
      3600CBA97400D1B38300EAE3D900CC964100CA903600D8B78300EAE8E700E4D8
      C600B6935B00000000000000000000000000000000000000000008A0D700B4F0
      FF00B3EFFF00B2EFFF00B1EFFF00AEEEFF00A9EEFE00A5ECFF009FEBFF0098EA
      FF0086E6FF0071E2FF005DDEFF004CDBFF0047DAFF0043D8FF003DD7FF0039D6
      FE0033D6FE002FD5FE002AD3FF0025D2FF0020D2FE001CD0FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000000000000049607A00729FD20079A6D7007FABDC0085B2E0008AB7
      E4008FBBE80092BEEA0093C0EB0092BEEA009BC3E90000000000000000000000
      000000000000EBDED300DB945E00C15E12006D432100B1B1B100000000000000
      000000000000000000000000000000000000A9A9A900FFFFFF00F2F2F200F2F2
      F200F2F2F200F2F2F200A4A4A400F1F1F100F1F1F100F1F1F100F1F1F100F1F1
      F100A4A4A400F1F1F100F0F0F000D2DCD2005D8A5B002677220052BD4E007AE8
      75008CF688009AFE96008CF688007AE8750052BD4E00257422005B885900C8D1
      C800DEDEDE00DEDEDE00FFFFFF0094949400000000000000000000000000A874
      2700B86E0000B86E0000B86E0000B86E0000B86E0000B86E0000B86E0000C99B
      5600B86E0000BD842F00BC802700B86E0000B86E0000B86E0000C88F3900CB94
      4000D5B27E00C9934100E4D8C500E6D2B600D3A76400EFEDEA00D2B48700D1B6
      8E00C4B6A200000000000000000000000000000000000000000008A0D70099E4
      F900BCF1FF00BAF1FF00B9F1FF00B6F0FF00B2EFFF00ACEEFF00A6EDFF009FEB
      FF009CEBFF0099EAFF0092E9FF007FE5FF006AE1FF0053DCFF003ED7FF003AD7
      FE0035D6FF002FD4FE002BD4FF0025D2FF0021D2FE001CD1FF000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000000000007D8E9F002640600077A3D5007DA9DA0084B0DF008BB7E40091BE
      E90096C3EE009AC7F0009BC8F10099C6F00095C0EB0000000000000000000000
      00000000000000000000A54D0800F6B78700A14C070079797900000000000000
      000000000000000000000000000000000000ABABAB00FFFFFF00F3F3F300F3F3
      F300F3F3F300F2F2F200A4A4A400F2F2F200F1F1F100F1F1F100F1F1F100F1F1
      F100A4A4A400F0F0F000F0F0F000F0F0F000F0F0F000C8D4C80082A38000386B
      3500306B2D001C5D1900306B2D00487C460081A380008B978A00EFEFEF00EEEE
      EE00EDEDED00E7E7E700FFFFFF0096969600000000000000000000000000A673
      2800B66B0000B66B0000B66B0000B66B0000B66B0000B66B0000B66B0000C899
      5600B66B0000B66B0000B66B0000B66B0000B66B0000B66B0000C3852B00CC97
      4B00D6B48500CC974B00CDA16200F4F2EF00F0E6D900E7DDCD00CB974B00CC97
      4A00B08E5C00000000000000000000000000000000000000000082CEE8000BA2
      D7007AD5F200C1F2FF00C1F3FF00BEF2FF00B9F1FF00B3EFFF00ADEEFF00A9ED
      FF00A5EDFF00A4EDFE00A3EDFE00A2ECFF00A1ECFE009FEBFE008DE8FE0079E4
      FE0065DFFE0052DBFE003FD8FE0030D5FF0024D3FF001DD1FE000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      0000919EAF00274162003553770079A5D70080ACDD0087B4E2008FBCE80096C3
      ED009DC9F200A0CEF500A2CFF600A0CDF5009DC9F200DEE8F200000000000000
      00000000000000000000E1CBB900C4661C00C6712E0062452C00ADADAD000000
      000000000000000000000000000000000000ABABAB00FFFFFF00F5F5F500F5F5
      F500F4F4F400F3F3F300A4A4A400F3F3F300F3F3F300F2F2F200F2F2F200F1F1
      F100A4A4A400F1F1F100F0F0F000F0F0F000F0F0F000F0F0F000EFEFEF00A4A4
      A400EFEFEF00EEEEEE00EFEFEF00EEEEEE00EEEEEE00A4A4A400EEEEEE00EEEE
      EE00EDEDED00EDEDED00FFFFFF0098989800000000000000000000000000A571
      2A00B3670000B3670000B3670000B3670000B3670000B3670000B3670000C697
      5600B3670000B3670000B3670000B3670000B3670000B3670000BB761800CD9B
      5500D6B78B00CD9B5500CD9B5500DBC19C00FAF9F700CFA76F00CD9B5500CD9B
      5500B48F5B0000000000000000000000000000000000000000000000000009A9
      DB0010A9DF0052C3EA00AFEBFC00C4F3FF00BFF2FF00B9F1FF00B5F0FF00B1EF
      FF00B0EEFF00AFEEFF00AEEFFF00AFEFFF00AFEEFF00B0EFFF00B0EFFF00AEEF
      FF00AAEEFF00A3ECFF0099EAFE008AE7FF0079E4FE0067E0FF000A99CC007474
      7400D1D1D1000000000000000000000000000000000000000000000000000000
      000061769000274466006088B7007AA6D80082AEDD008AB6E40092BEEA0099C6
      F000A0CEF500A6D3F900A8D5FB00A5D2F900A0CEF50095AECF00000000000000
      0000000000000000000000000000C79D7C00E3975D00A3480000747372000000
      000000000000000000000000000000000000ABABAB00A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A4009A9A9A00000000000000000000000000A46F
      2B00B1640000B1640000B1640000B1640000B1640000B1640000B1640000C595
      5600B1640000B1640000B1640000B1640000B1640000B1640000B2660200CE9C
      5B00D7B99100CF9F5F00CF9F5F00CD9D5E00C99E6600CF9F5F00CF9F5F00CF9F
      5F00B591620000000000000000000000000000000000000000000000000009AB
      DD0053D7FF0031C3FA0018AFEB002AB4E50098E3F800B3EDFE00ABEBFC00B9F1
      FF00B7F0FF00B7F1FF00B8F1FF00BAF1FF00BCF1FF00BEF2FF00C0F2FF00BFF2
      FF00BDF1FF00B6F0FF00ACEEFF009EECFF008CE8FE0079E4FE000B9BCE008B8B
      8B00DCDCDC00000000000000000000000000000000000000000000000000A2AF
      C0002A486D0028466B0074A1D3007AA7D80082AFDE008BB7E40093C0EB009BC8
      F000A2CFF600A8D5FB00ACDAFE00A8D5FB00A2CFF700698DB900000000000000
      000000000000000000000000000000000000B1530B00EAA7740076441D009898
      980000000000000000000000000000000000ABABAB00FFFFFF00FBFBFB00FBFB
      FB00FAFAFA00F9F9F900A4A4A400F8F8F800F8F8F800F7F7F700F7F7F700F6F6
      F600A4A4A400F5F5F500F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200A4A4
      A400F1F1F100F0F0F000F0F0F000F0F0F000F0F0F000A4A4A400EEEEEE00EEEE
      EE00EEEEEE00EEEEEE00FFFFFF009D9D9D00000000000000000000000000A26E
      2C00AE610000AE610000AE610000AE610000AE610000AE610000AE610000C393
      5600AE610000AE610000AE610000AE610000AE610000AE610000AE610000C68F
      4800D9BC9800D1A36900D1A36900D1A36900D1A36900D1A36900D1A36900D1A3
      6900B694690000000000000000000000000000000000000000000000000009AC
      DD0076E1FF0068DBFF004CCFFF0038C3FF0023B4FA0013A9EF000EA8EA000BA5
      E10009A2D90008A0D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0
      D70008A0D70008A0D70008A0D70008A0D70008A0D70008A0D7000C9BCE00BABA
      BA0000000000000000000000000000000000000000000000000000000000A0AF
      C1002E4D7400395676005D84B300547BA70082AEDE008AB6E30092BEEA009AC7
      F000A0CEF500A6D3F900A8D6FB00A6D2F900A0CEF5004E87BC00E4EAF2000000
      000000000000000000000000000000000000C0906B00D77F3B00A5520F00594D
      4300BBBBBB00000000000000000000000000ABABAB00FFFFFF00FFFFFF00FEFE
      FE00FEFEFE00FDFDFD00A4A4A400FCFCFC00FBFBFB00FBFBFB00FAFAFA00F9F9
      F900A4A4A400F8F8F800F8F8F800F7F7F700F7F7F700F6F6F600F5F5F500A4A4
      A400F4F4F400F4F4F400F3F3F300F3F3F300F2F2F200A4A4A400F1F1F100F0F0
      F000F0F0F000EFEFEF00FFFFFF009F9F9F000000000000000000000000009C71
      3C00AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000C290
      5600AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000AC5D0000BB7A
      2D00DABF9E00D3A87300D3A87300D3A87300D3A87300D3A87300D3A87300D3A8
      7300B094710000000000000000000000000000000000000000000000000009AC
      DD0093E9FF008FE8FF0081E5FF0070DEFF005FD8FF004ACEFF003AC8FE0030C3
      FF002AC3FD0024C4FD001EC2FD0018BEFD0012BDFF000CBBFD0008B8FD0005BA
      FE0003BBFE0003BBFE0003BCFE0003BEFE0004C3FE0008A8DB00A1A1A1000000
      0000000000000000000000000000000000000000000000000000000000006981
      A0002D4F780043607F002D4E76004569950080ACDC0087B4E2008FBCE80096C3
      ED009CC9F100A0CEF500A2CFF600A0CDF5009CC9F100446D9B00CBD7E7000000
      00000000000000000000000000000000000000000000B7622000D57C38008341
      0C0089898900000000000000000000000000ABABAB00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00A4A4A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A4A4
      A400FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A4A4A400FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00A1A1A1000000000000000000000000007E73
      6600A3784500A06B2F00A06B2F00A06B2F00A06B2F00A06B2F00A06B2F00AB85
      5A00A06B2F00A06B2F00A06B2F00A06B2F00A06B2F00A06B2F00A06B2F00A472
      3900BCA48900B8997500B8997500B8997500B8997500B8997500B8997500B89D
      7E00847A6F0000000000000000000000000000000000000000000000000009AC
      DD00A8EEFF00A5ECFF009FEBFF0097EAFF008CE8FE007EE4FF006ADEFF0008A6
      DA0021A4CF002DABD50030AED70030AFD70030B0D70030B0D70030B1D70030B1
      D70030B1D70030B1D70030B1D70030B1D90030B1D90030B2D900000000000000
      00000000000000000000000000000000000000000000000000000000000098AA
      BF0030537F003B5978002F517D003B5F8B007CA8D90084B0E0008AB7E40091BD
      E90096C2ED009AC6EF009BC7F10099C6F0007AA7D6003A679800C6D2E4000000
      00000000000000000000000000000000000000000000DFC8B600DE844000C665
      18006A5F5900000000000000000000000000A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400A4A4A400A4A4A4000000000000000000000000008F8D
      8C00C9C8C700EAEAE900F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2
      F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2
      F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100F2F2F100EDECEB00CECD
      CB00939190000000000000000000000000000000000000000000000000001BAF
      DC0062D1F100B7F0FF00B0EFFF00A7EDFF009DEBFF008CE6FF0034C1EA0057A7
      BF00DBDBDB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00003257860032568400315683004170A3007FACDB0077A3D300729DCD006790
      C100729DCC0090BCE80099C2E80082AEDC004276A9003F6DA900D7E0ED000000
      0000000000000000000000000000000000000000000000000000CEAD9000DC75
      2900844718008A8A8A000000000000000000AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800000000000000000000000000D1D0
      D000A6A5A400C7C7C600CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCD
      CC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCD
      CC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00C9C9C800A9A9
      A800D3D2D2000000000000000000000000000000000000000000000000000000
      000044BCE10009ACDD0009ACDD0009ACDD0009ACDD0009ACDD0070BFD8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000056769E00345B8B00648BB7005F91C8005F91C80036649700285D96003760
      9400396298003B669D004A76AD0081A8CD0081A8CD00527BB200000000000000
      000000000000000000000000000000000000000000000000000000000000D194
      6600C0601700625347000000000000000000AD451800F0B26C00DF842300E492
      3400E6973500F7C48000AD451800F8C98200F0B14700F3B84E00F5BF5800FCD1
      8800AD451800FDD48800FCCF5800FDD25800FFD65800FDD25800FED68800AD45
      1800FCD38800F6C35800F4BC4E00F2B54700F9CA8200AD451800F8C67F00E89C
      3500E6973400E2892300F2B66E00AD4518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A1B4CB00335883006C9DD2005F91C80036649700234779003E699800578A
      C00075A5D9008AB3DF0096BAE10089B2DD003F6EAB00C3D1E400000000000000
      000000000000000000000000000000000000000000000000000000000000F1E7
      DF00F08330007C5233000000000000000000AD451800FFDAA700EAAB6C00EBAE
      6C00ECB16C00FFDAA700AD451800FFDAA700F1BC6C00F2BE6C00F3C06C00FFDA
      A700AD451800FFDAA700F7C86C00F7C96C00F7C96C00F7C96C00FFDAA700AD45
      1800FFDAA700F5C36C00F3C06C00F2BE6C00FFDAA700AD451800FFDAA700EDB3
      6C00ECB16C00EBAE6C00FFDAA700AD4518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000728FB400335C8B0036649700234779003E6998004F7EB3005E92
      CC006EA2DB006EA2DB003E6CA8003F6EAB00A5BBD80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EDCDB500B68B6A000000000000000000AD451800FCDAAE00F6DABE00F6DA
      BE00F6DBBE00FDDAAE00AD451800FDDBAE00F8DEBE00F8DEBE00F8DFBE00FDDC
      AE00AD451800FDDCAE00F9E1BE00F9E1BE00F9E1BE00F9E1BE00FDDCAE00AD45
      1800FDDCAE00F8E0BE00F8DFBE00F8DEBE00FDDBAE00AD451800FDDBAE00F7DB
      BE00F6DBBE00F6DABE00FCDAAE00AD4518000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000C8D4E300234779003E6998003C67A0003C69A3003D6A
      A4003E6BA6003F6DA9006589BB00C9D6E7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000D1998100AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800AD451800AD451800AD451800AD451800AD45
      1800AD451800AD451800AD451800D29B84000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F1CEA700EEC2
      8E00F7E7D600F5DCBC00F0C18900EFCDA7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CAE7F90095CEF20071BCEB005CB3EA006CB9EB008ECA
      F100C2E3F8000000000000000000000000000000000000000000000000000000
      00000000000000000000DFE0E000DDDDDE00E6E6E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E3E4E400E6E6E70000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000E99C4000EEA3
      4700DB7E1100E89D4400F0A34400E1871E00E3A15400F7E7D500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CCE6F7005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E900BDDFF500000000000000000000000000000000000000
      0000BEBEBE006E69690097867300A391770082756A0076767800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000848484007B736D00978774008F8070006B676700C0C0C0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000E9AC6400E69E4A00E9A85D00ED9A3600ED90
      2100EE922600ED8D1D00EB830A00EF942900EF973000F4D1A700E6AD6B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C2E2
      F6005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E900BBDEF4000000000000000000000000000000
      00008E878600C8985F00E2B65800E9C56400D8A65100AE8D7100BDBDBE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000CACACB00A7907E00D5A45800E9C46500E5BD5F00C8995E0089817D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000F2D4B100EF9C3800EC881300ED8E1E00EB7F0200EB7F
      0200EB7F0200EB7F0200EB7F0200EB7F0200EF9B3900E9B06E00D7750400F4DD
      C200000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000060B3
      E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E90061B3E700000000000000000000000000D7D7
      D900AD7E5F00C7752700BB9A6F00B6A78F00D0985500BE702D009E908A000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A0999800BC743900CA8A4000B7A38100C6A87900CE823100AC785300D4D4
      D500000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000F7E4CF00E9B37300EE932800EB7F0200EB7F0200EB7F0200EB7F
      0200EB7F0200EB7F0200EB7F0200EB800400EE9A3600F4D5B200E7AF6F000000
      00000000000000000000000000000000000096CCE70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000A4A2
      A400B6785400C67F5700B3AFB200AFB2BC00C19F8D00BB591F00A08272000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000A18E8400C06C3900BA927C00B0B2BC00B0AEB300C97E4F00B16233009F9D
      9D00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000ECBF8C00EC9B3A00ED993600EB810700EB800500EB800500EC8A1800EE91
      2400EE8F2200EB830C00EB972D00EBB96900ECC17900EDC47F00F6E2C000D97E
      1500F0D0AA0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000B2B1
      B200AF836B00DDA88F00D3C2BD00DAD6D600E0BEB000C68866009D887D00C3C3
      C300CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00CDCDCC00C5C6
      C6009F8F8800C4896800D6B0A100D6D2D200D6CBC700E0AA9100B1795900ACAA
      AA000000000000000000000000000000000000000000FDE9D100FDE5C9000000
      0000FDE6CB000000000000000000FDE2C20000000000FDE9D100FDEEDD000000
      0000FDE2C20000000000FDECD900FDEAD50000000000FDE2C200000000000000
      0000FDE7CE0000000000FDE4C6000000000000000000FDE4C700000000000000
      000000000000FDE2C200FDE9D200000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7AA6300EE932900EB800500EB800500EB800500F4BD7E00ECBB8200DE85
      1D00DF933900E79C4200EDBA6B00ECC07600EDC37C00F1D09900F2D2A900D777
      0800E8B3780000000000000000000000000051ABD70051ABD70051ABD700A4DD
      FD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DDFD00A4DD
      FD00A4DDFD0051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E900000000000000000000000000D5D6
      D600AE9C9400B88F7800ECC9B700EECEBE00E0BAA600AD8A7800A7A4A3007675
      7300747472007474720074747200747472007474720074747200777573007676
      7400A9A6A500A3806D00DFB8A300EECFC100F0CFBF00CBA48E00B1948700CFCE
      CF000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDCA8E00FDDCB600FB88020000000000FCA74200FCBD7200FDE9
      D200FF8B000000000000FCB56200FCAF520000000000FF8B0000FDE2C200FDC5
      8200FD9F320000000000FB921200FDD3A200FDD19E00FB931600000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EEC59500EF973100EB800500EB800500F4BD7E00E1963F00D9801A00F1D4
      B20000000000F1D0AA00EDAC5700ECC17900EDC47F00F6E2C100D8790C00E099
      48000000000000000000000000000000000051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      0000B5B3B300BDACA400A07F6D00AB877500A3887B009F999C00A4A4A700D8D8
      D80000000000000000000000000000000000000000000000000000000000C9C9
      CB00A7A7A900BDB7B8009E817200AC857100AD867200AF897C0084A9B8000000
      00000000000000000000000000000000000000000000FCA74200FB951E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFCB
      A200E8A04C00ED8F2000EB800500EE922800EBB67A00D6770A00F6E5D3000000
      00000000000000000000E79B3E00EDC37C00EEC58200F2D4A200FAECD700E199
      4700F5E1CB0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      000000000000BEBEBF00CACACC00AAAAAC0096949100998A6F00AC9C7C00827A
      7100C1C1C2000000000000000000000000000000000000000000828184009B8C
      7900AC9B7A00BCAE9C00D8D6D900BBB8B900A2A4A300579EA20000C5E900B6C8
      CD000000000000000000000000000000000000000000FCA74200FB951E000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E596
      3800EC850E00EB800500EB800500F2AE5F00DE8D2F00E3A45D00000000000000
      00000000000000000000EBA64F00EDC47F00EEC78500EFC98B00F6E1BF00DF8D
      2F00E9B9800000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005DB3
      EB005CB2E9005CB2E9005BB1E90059B1E80057B0E80056B0E80057B0E80059B0
      E8005BB1E9005CB2E9005CB2E9005CB2E9000000000000000000000000000000
      00000000000000000000857F7A0084797300B6804800DDAA4E00E2B75700D39A
      4F009F8C7C00E2E1E000000000000000000000000000B5B3AF00B9906700DAA2
      4A00E1B85900D69339008A8B7C0019C4EF000BD5E70000ECFD0000E0FF007AAE
      C3000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FDD6A900FDDBB30000000000FDD09A00FDD09A00FDD09A00000000000000
      0000FDE4C800FDDBB30000000000FDD9AE00FDD09A00FDDAB10000000000FDD0
      9A00000000000000000000000000FDECD800FDD09A00FDE4C700FDE8D000FDD7
      AB00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E4A0
      5100EE9B3900EB800500EB800500F2B06300DD882700E9B98000000000000000
      000000000000EBB97F00F0B96D00EEC68200EFC88800F7E1C500E9B37400D87C
      1200F3DBBF0000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700000000005CB3
      EB0054AFE80050ADE60055AEE60073BDEB0089C6ED0095CDF0008AC8ED0075BE
      EB0056B0E60050ACE60054AEE8005BB1E80000000000000000007C493C00883C
      13008E42170094491A0098481400A14A1200AB4B0B00B25A1200BB6B1A00C57D
      1F00D1952E00D1A83C00D3B03E00D3B13E00D0AB3C00D3A53700CA8B2600BF78
      2000B9651400AF5109003A89A70004D1FF000EE8FF0012E9FF0012DBFF0049A5
      CD000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EB963200EB800500EB800500EDA24200E4923200E5AB6700000000000000
      0000F1D3B200E9A04700EEC37D00EEC78500F1D09900EBB87D00D7750400F7EA
      DA000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD7000000000059B2
      EA00B6DCF400F5FBFD00F4FAFC00D8ECFA00C3E1F500B6DCF400C3E1F500D8EC
      FA00F5FBFC00F8FCFE00BADFF4005AB0E80000000000000000006F220A00A461
      3E00AE6E4900B26E4400B36C3E00B4683700B4643100B8692C00BF732A00C681
      2900D0942A00DBAB3100E1B73500E1B93600DFB43600D5A32E00C88B2700C375
      1600AF5F16003F7AAC0015B4FF0023CCFF0025E1FF0018E5FF0016CDF9002DA9
      E4000000000000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E7A9
      6200EE962F00EB800500E9932300E7B45B00EFAD5500DD872100EFCBA100E9B0
      6D00EBA34A00EEBF7600EEC68200EFC88800F0CB8E00F6DBB800D7790E00F8ED
      DF000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70000000000F3FB
      FE0096CDF00055AFE80050ADE60052AEE60053AEE80054AFE80053AEE80052AE
      E60050ACE60055AFE60095CDF000ECF5FC00000000000000000070260B00B37E
      6200BB896B00BD866400BF855F00C0825A00C17F5500C5835200CA8C5100D096
      4F00D5A14D00DBAE4A00DEB74400DDB53C00D9AD3300D09A2B00CE871A00AF6F
      27003B70C000259EFF0036B6FF0036CEFF0020E1FF00458684004970760017BB
      FD00CCCED40000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F6E0C700EE9F
      4100EE902300EA860F00E7B05400E8B55E00E9B76300EEB56300EEB15E00EFB8
      6A00EDC17900EDC48000EEC78600F7E5C600F9EAD200E39C4A00D5750600F5E3
      CE000000000000000000000000000000000051ABD70051ABD70051ABD700A4DD
      FD00FFFFFF00FFFFFF00FFFFFF00A4DDFD00A4DDFD00FFFFFF00FFFFFF00FFFF
      FF00A4DDFD0051ABD70051ABD70051ABD70051ABD70051ABD7000000000067BA
      EC0054AFE8005BB1E8005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005BB1E90055AFE80062B4E90000000000F2F0EF0071250600B988
      6F00C0907400C18E6E00C38C6900C48A6400C5865F00C7895B00CC905A00D199
      5900D6A35A00DBAE5B00DFB75A00DFB85600D9AE4A00D99B2D00AD7D3B003774
      D300378FFF0048A4FF0047BDFF0029C8FF00527975009126000065422F0028BC
      FF008DA5BC0000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000EFCC
      A400E6A04D00EBA84A00E8B45B00E8B76100E9B96700EABC6C00EBBE7200ECC1
      7800EDC37D00EEC68300F4DAAF00E49D4B00D5770A00E8B67C00EBC090000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D700FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD700FFFFFF00FFFFFF00FFFF
      FF0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70000000000B1DA
      F4005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2E9005CB2
      E9005CB2E9005CB2E9005CB2E900ABD6F20000000000EFEDEB0071250200C094
      7E00C59A8000C6977A00C7967500C8937100C98F6B00CA906700CE956500D29C
      6300D6A46200DAAC6100DCB16000DCB15C00E1AB4C00B49769005690E6005AA1
      FF005D9EFF0053ADFF0031A9F3006563590097320000883002007329080037A1
      E9004E8FC90000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EBBB8500EDB15A00E8B55E00E9B86400EABA6A00EBBD6F00ECC07500EDC2
      7B00EFCA8C00F1D09900F3D8AC00E7AD6800E3A2590000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF00A4DDFD0051ABD70051ABD700A4DDFD00FFFFFF00A4DD
      FD0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD7009FD0E7000000
      000000000000BEE1F70095CEF1007AC0ED0067B6E8005BB0E70067B6E8007AC0
      EC0095CDF100BDE0F700000000000000000000000000EDEAE800772A0800C7A1
      8D00CAA38D00CBA18700CC9F8200CD9D7D00CD997800CD977300D09B7000D3A0
      6E00D6A56C00D9AA6A00DAAD6700E1AD5900B29E80006AA1F10082B8FF0089B8
      FF007AB5FF004994E700775341009E3800008E390800843105007C1F00004578
      A7002E8EED0000000000000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000000000000000000000000000099C8EE008FC3EC00DCEBF80097D1
      F100BC925800F1C28400F4D9B500EDC47F00EABC6C00EBBE7200ECC17800F3D7
      A700EFC49100F0C99900EFC18B00D7770800E3A45D0000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD700FFFFFF0051ABD70051ABD70051ABD70051ABD700FFFFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70053AA
      D50096CBE500D7EBF40000000000000000000000000000000000000000000000
      000000000000D8EBF500000000000000000000000000EBE7E4007B301000CEAD
      9D00CFAD9900CFAA9300D0A88E00D1A68A00D1A38400D19F7F00D2A07B00D5A4
      7800D7A77600D9AA7300DFAB6600B0A395007FB1F8009DC7FF00A1C6FF008EC1
      FF006A94D900A06B5100B15B2200984614008A360500822E04007D1F0000524D
      5F003398FF00E0E7F300000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000000000000D2E5F7000000000055C5F10059CEF50048B8EC0064DB
      FE004EBAE900B5AE8600D89E5800F1D29E00F5DDB700ECBE8900F4DBB200F6DF
      BA00DA821B00E0974400F1D4B200EABC87000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E9E3E00081381900D5BA
      AD00D4B6A600D4B4A000D5B29B00D5AF9600D5AC9100D5A88C00D5A68700D6A8
      8300D8AA7F00DDAA7400AFA8A80095C0FD00B5D5FF00B7D4FF009ECAFF008099
      CC00B2785400BB723F00AF6D4300A7633A0094471C007E2A01007A210000612E
      24004C9EFF00A1B9DE00000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000096C8EE005BD0F90052C4F20059CDFE004FBEFF0049B6FF0043AD
      FF0049B5FF0087CDFF007CB0DC00C0C1BC00E6A75D00D7750400EFCDA700E4A8
      6200D7750400ECC3940000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E7DFDC0086402200DDC7
      BC00DAC0B200D9BDAC00D9BBA800D9B8A300D9B59D00D9B19800D8AE9200D9AD
      8D00DBAC8400B2AFBA00ABCFFF00CAE1FF00CAE0FF00AAD0FF00949FBF00C388
      5E00C3825500B97C5400B3744D00AB6C4600A36340008E462400782301006F2A
      1200568EE100598AD500000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000000000
      00000000000081C4EC0047B4FF003EA7FF003CA4FF003AA1FF0039A0FF0039A0
      FF0039A2FF00A3D5FF0067AEE800DBEBF80000000000F5E1CB00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E6DCD7008E4B2E00E6D6
      CF00E0CBC100DFC9BB00DFC6B700DEC3B100DEC0AC00DDBCA700DEB89F00DCB2
      9500B9BACB00C2DDFF00DDEBFF00DAEBFF00B4D5FF00A7A7B900CF976D00CA91
      6B00C38C6800BD856100B77D5A00AF755300A66A4B009D614600904B2D007C33
      1B006E8AB9005A9FFC00000000000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA9460000000000000000000000000000000000C2DD
      F5004EBEF10054C6FB003DAAFF0040B3FF0061C8FF0055D1FF004ECCFE004CCA
      FE0062D9FE006FDEFE0089E4FE0099C6EC00C8DEF40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E4D8D2008C472C00E6D7
      D300E3D0C700E2CCC100E1CABC00E1C7B700E0C4B200E0C1AC00D9BDAD00BDC9
      E400D1ECFF00E6F5FF00DEF1FF00B8D3FC00B6AAB000D5A17B00CF9D7C00CA98
      7800C6937200C08C6B00BA846400B27C5E00AA715600A0664D009D624B00934F
      2E0099A2B4006FB4FF00B1BACA000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000000000000098C7
      EE0055C6FC003AA3FF003DACFF009FD8FE0079B5EA00429ADF0071BCEB0066DD
      FB0068DDFE0068DDFE00A0E9FE004D9ADF00BDD8F20000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000E6D8D10076210100914B
      230093513000985837009D5C3600A25F3600A6603400AD612E00746D84005F84
      C400718DC100708EC1006B8CBD00926F5B00B6652000AC602500A85B2200A356
      20009E501D00984A1B0091451800893D15007F331000813611008F4214008B50
      3300DEDFE2006CA8FB00819DC6000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000000000000087BD
      EC0053C3FE003AA3FF008CCDFF00549FE30074B0E600000000000000000053C1
      EF0068DDFE0068DDFE00AFEDFE0081B9EA000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000EBE5E30084340000A459
      0C009B6741009F756100A2745800A5735300A6714E00A66D4900AA6F4500B075
      3E00B27D3D00B7863C00BC8D3A00BE8F3700B5863400AC742600A15E1700964B
      0D008C3E0800863807007E330500752B0200651B0000823D0900BC711A00A38F
      7C00000000007099D80079A5E8000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000008FC2ED0055C4
      FB003FA6FE003AA3FF00A1D1F8003089DA0000000000000000000000000058C3
      F00068DDFE006EDFFE0081E4FE0095C5EE00A7CCEE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD700000000000000000000000000000000007E4225009F50
      0D009D6D4E00A6816F00A77E6500A97D6100AA7B5B00AB775500AD765000B07B
      4D00B4824C00B7884A00B98C4700B88B4200B3823B00AC753200A36427009750
      19008C400D00853A09007E340800742C0500661E00007E380800AE671D00789A
      A300000000008CA1C30060A2FE000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA946000000000000000000000000009CC8EE0059CA
      F400379BFF003AA3FF0097C9F300509BE0000000000000000000C0DCF3006ECC
      F30072E0FE009FEBFE00A9D2F2004495DD00CFE2F50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000947A73009743
      0700A1735900AE8B7A00AE877000B0856B00B1836500B2805F00B27C5A00B57F
      5600B8845300BA885100BB8A4D00BA874700B5804000AF743700A6652D009D56
      2300934A1A008A3E0E007F330700752A0400681E00007C2F0100A16B360050B2
      DF0000000000AD9F98009F835E000000000000000000FCA74200FB951E000000
      0000FB9B2600FDA63E00FDDCB600FF8B0000FF8B0000FF8B0000FDE2C2000000
      0000FDBF7600FDA63E0000000000FD9F3200FF8B0000FAA43A0000000000FF8B
      0000FDE9D2000000000000000000FDD19E00FF8B0000FCBD7200FDC88A00FB9D
      2A00FDE5CA00FF8B0000FCA94600000000000000000000000000000000005EC0
      F000379BFF0041ABFE0072DFFD003C94DE0000000000D0E6F70068BDEC0075E0
      FC007FE5FE00BCF2FE004498E100BDD8F2000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000C9C5C5008934
      0300B3846C00C4A08F00C39B8400C5997E00C6977800C6947200C68F6C00C78E
      6600C9916200CB945E00CB945900C9915300C5894C00BF7D4200B66F3800AD61
      2E00A55726009B4E1E008E3E10007E2C030073210000842A00007070650052CD
      FF00D9DDE000B17E4C00B79666000000000000000000FDC98B00FCBF75000000
      0000FCC17A00FDC88900FDE9D200FCB56200FCB56200FCB56200FDECD9000000
      0000FDD7AB00FDC8890000000000FDC48100FCB56200FDC6860000000000FCB5
      6200000000000000000000000000FDE2C300FCB56200FDD6A800FDDDB700FCC2
      7C00FDEEDE00FCB56200FDCA8D000000000000000000000000008EC7EE0055C4
      FD00399EFF0061D4FE0068DEFE0063DAFA0056BEEE006BCBF30077E4FD0083E7
      FE00A3EEFF00C7F5FF004198E10092C0EB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700FFFF
      FF00FFFFFF00FFFFFF00FFFFFF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700FFFFFF0051ABD70051AB
      D70051ABD70051ABD70000000000000000000000000000000000000000007F42
      2500BE917D00CCAC9D00CBA59100CCA48B00CDA18500CD9E7F00CD997800CC95
      7200CD956C00CE966700CD956200CB915B00C7895400C17F4A00B9724100B168
      3800A95F3000A055270094481E0083340F0076230100822700008B91950057C8
      FD0089A3BA00C47D2F00BDB2A400000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A5CFF00060BB
      EC005FD2FE0068DDFE0068DDFE0069DDFE0071E0FE007CE4FE0086E8FE00A6EF
      FF009CCCF1006AAEE600579FE100000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000FDFDFD00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00A5E0FE0051AB
      D70051ABD70051ABD7000000000000000000000000000000000000000000987D
      7400C2907B00CEAD9F00CCA69300CDA58E00CFA38800CFA08200CF9D7C00CE98
      7500CE956F00CE936A00CD926400CA8E5E00C6875600C17D4E00BA744600B46D
      3F00AC643700A2592E00964B2400883C1A0081300E00772A0A000000000062B1
      EF00769BC500CE904200DEDCDA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098C7
      EE0071DFFC0076E0FE006BDEFE0075E1FE007FE5FE008EEAFE00BCF3FF00BBF4
      FF007DB8EA00A0C8ED0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BDDFF00051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF00FFFFFF00FFFFFF00A4DDFD0051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700A4DDFD00FFFFFF00A5E0
      FE0051ABD70051ABD7000000000000000000000000000000000000000000CFCB
      C90085442A0087492E008D4D2E0093543200995932009D5D3300A25F3300A45F
      3300A5603200A7603200A7613200A7613100A55F3000A35C2E009F5A2D009C57
      2B0095522A008E4C2600864423007E3C2000813C20008B6858000000000072A1
      CE00C6BDBC00C49A640000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000C1DC
      F60078BDEC00B3DAF50088E6FE00B0EFFE00A2EEFE00C9F0FD0063ACE7008BC1
      EB002C87D900C8DEF40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000098CE
      E80051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD700C5F4FF00FFFFFF00FFFFFF00C1F1FF0051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700A4DDFD00FFFF
      FF00A5E0FE0051ABD70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000A1B0
      C300D3BCAB00B69F840000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000A1CAEE00CBEFFD0055A0E300BCE2F7008FC7F0002985D9000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DCEDF60077BEE10051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD7007FC9EE00FFFFFF00FFFFFF00E0FFFF0051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD700A4DD
      FD00FFFFFF00A5E0FE0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000DEDE
      E200A5A4AB00C4C2C40000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000009EC9ED00A0C8ED0000000000AACEEF00D3E6F6000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BBDEEF0051ABD70051ABD70051ABD70051ABD70051AB
      D70051ABD70051ABD70051ABD70051ABD70051ABD700D7FEFF00FFFFFF00F9FF
      FF0051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051ABD70051AB
      D700A4DDFD00FFFFFF0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000F4F7FB000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000008CB6C7005E9CB40074A2B5000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D5E3E900ABC9D500C5CFD400CECECE0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000077A9BD006BAEC5009BE2EF005A97B000CACA
      CA000000000000000000000000000000000000000000E9F0F300B5CFDA008CB6
      C700639CB3005D9CB60063A5C1005C95AC00829AA200ABB3B600000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E9F0F30077A9BD0068AFC8007FCDE3009CE3EF005A97B000C0C0
      C000CDCDCD0000000000C9DCE40096BCCC006DA2B8005C9BB40060A2BC0066AA
      C7006AB1CF006BB3D1006BB3D10069ABC40076B8CC005D97AE00D0D0D0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000DEE9EE00639CB3006CB7D00076C9E10080CFE4009DE3F0005A97B000839A
      A4005C82910050869C005FA0BA0064A7C30069B0CD006BB4D2006BB4D2006CB5
      D3005AB0B00018A33000199E330078C1DB009CE2EF005A97B000D1D1D1000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BAA59600624A3200604830006048300060483000604830006048
      3000604830006048300060483000604830006048300060483000604830006048
      300060483000604830000000000000000000000000000000000000000000CEE4
      EB0067A5BB006FBDD40078CCE30077CBE20080D0E5009EE4F0005A97B0004D81
      970051879E005A97B0006BB4D2006BB5D3006CB6D5006CB7D6006CB7D7005BB3
      B4001EAC3C0026BE4C00109F210079C4DF009CE3EF005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000BAA59600DEB8A200E2B59900E6B18D00E7AF8800E9AC8300E9AC
      8300EBAA7F00EDA77B00EEA47500EEA47500F1A27000F1A06C00F39E6900F49C
      6500F49C65006048300000000000000000000000000000000000000000006DB9
      CE0076C9DF0079D0E60079CEE40078CDE40081D2E6009FE4F0005A97B000558E
      A600558FA8005B9AB4006CB7D6006CB8D8006DB9D9006DBADA005CB6B80021AF
      41002BC3570026BE4C00109F21007AC6E2009DE3F0005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000004B5698002E51BA002764DA002778
      EE002A8AF1002A8AF1002A85EC002D74D600335BB50044508E00000000000000
      00000000000000000000000000000000000000000000A09C9500A69C8F000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000B4C0BC00ACBCB600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000037714B0037714B00356F
      4900356F4900316A4500316A45002F6742002C643F0029603D00275E3900255A
      360022583400205531001D512E001D512E00194D2B00FAE6D900FAE5D800FAE4
      D600FAE3D400FAE2D300F9E0D200F9E0CF00F9DECD00F9DDCC00F9DCCA00F9DB
      C800F49C650060483000000000000000000000000000000000000000000070BF
      D3007BD3E8007AD1E70079D0E60079CEE50082D3E700A0E5F1005A97B0005792
      AB005794AD005D9EBA006DBADA006DBBDC006EBCDD005DB8BB0022B2460030C8
      60002BC3570026BE4C00109F21007BC9E5009EE4F0005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000454CB1002441F200295DFA002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0031A9FE0030A3FE00378EFE003978F80060BC60005AA0
      4D007F827500AEA89D00A6B5B50097C3CD0093CFD70073CEE20083C0CF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00006DA595003BB992003BB992005FA690000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000039744E006AA3790068A2
      780065A17600619E73005C9D6F00589A6B00529868004D956300489260004290
      5C003E8E5900388C55003389520033895200194D2B00FBE8DC00FAE7DA00FAE6
      D900FAE5D800FAE4D600FAE2D300FAE1D300FAE0D100F9DFCF00F9DECD00F9DC
      CA00F39E690060483000000000000000000000000000000000000000000073C5
      D8007BD4E9007BD3E8007AD2E7007AD0E60083D5E800A1E6F1005A97B0005997
      B1005998B3005FA2BF006EBDDE006EBEE0005DBABE0024B4480032CB660030C8
      60002BC3570026BE4C00109F210052B79800A0E5F0005A97B000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000599662002737FE002849FE00295DFA002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0031A9FE00369EFF00378EFE003978F8006BD0640062FA
      4F004AC648004ABFDD0051D6F1005EE3F60063EBFB0066F0FD0070C2D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000006DA6
      94003BBB93003BBB93003BBB93003BBB93005FA7920000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000003D7851006EA67C00EBF3
      EB00E9F1E900E6F0E600E4EFE400E2EEE300E0ECE000DEECDE00DDEADD00DBEA
      DB00DAE8D900D8E7D800D7E7D70033895200194D2B00B68A6A00B68A6A00B68A
      6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00B68A6A00FADF
      CE00F39E690060483000000000000000000000000000000000000000000075C8
      DB007CD6EA007BD5E9007BD3E8007AD2E70084D7E900A3E7F1005A97B0005A9B
      B6005B9DB80060A6C4006FC0E2005EBCC00025B64A0033CB670032CB660030C8
      60002BC3570026BE4C0020B8400013A627001C9E2E0040947D00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000032A24200273AFC002849FE00295DFA002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0031A9FE00369EFF00378EFE003F80F6006AD45D0064FF
      500053EC3D0054D4E3005EE6FE0063EBFB0063EBFB0066F0FD0070C2D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000006DA795003BBD
      95003BBD95003BBD95003BBD95003BBD95003BBD95005FA89200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000427D520071A98100EEF4
      ED00EBF3EC00E9F1E900E6F0E600E4EFE400E2EEE300E0ECE000DEECDE00DDEA
      DD00DBE9DB00D9E9DA00D8E7D800388C55001D512E00FCF0E900D4AA8C00FEF8
      F500B68A6A00FCEDE300FCEBE100D1A68700FEF8F500B68A6A00FAE6D900FAE0
      D100F1A06C0060483000000000000000000000000000000000000000000076CA
      DD007DD8EB007CD6EA007CD5E9007BD4E90085D8EB00A4E7F2005A97B0005C9F
      BC005DA1BE0062AAC9005EBEC20026B84C0033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000898110062B565000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000002FA73C00283CF5002849FE00295DFA002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0031A9FE00369EFF00378EFE003F80F60069D5580062FA
      4F0053EC3D0054D4E3005EE6FE0063EBFB0066F0FD0066F0FD0070C2D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000006DA997003BBF96003BBF
      96003BBF96003BBF96003BBF96003BBF96003BBF96003BBF96005FA992000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000046815A0077AD8500F0F6
      EF00EDF4EE00EBF3EC00E9F2E900E6F0E600E4EFE40081AD88002B512F002B51
      2F002B512F002B512F00D9E9DA003E8E590020553100FDF3EE00D5AB8D00FEF8
      F500B68A6A00FCF0E900FCEFE700D1A68700FEF8F500B68A6A00FCEADF00FAE2
      D300F1A2700060483000000000000000000000000000000000000000000076CA
      DD007DD9EC007DD8EB007DD7EB007CD6EA0086DAEC00A5E8F2005A97B0005EA3
      C1005FA5C30054ACAD0026B94E0033CB670033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA315000495070060B3
      6200000000000000000000000000000000000000000000000000000000000000
      0000000000002FAC3E00283FEE002849FE00295DFA002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0031A9FE00369EFF00378EFE00478AEB0069D5580062FA
      4F0053EC3D0057D9F1005EE6FE005EE6FE0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000006EA997003BC197003BC197003BC1
      97003BC197003BC197003BC197003BC1970045C49C003DC198003BC197005FAB
      9300000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000004B865E007DB38A00F3F8
      F30051A356002B512F002B512F002B512F002B512F00237927006CB475004798
      5C004A9D4B0026613900DBEADC0042905C0022583400FDF3EE00D6AC8F00FEF8
      F500B68A6A00FCF0E900FCEFE700D3A98A00FEF8F500B68A6A00FCEADF00FAE4
      D600EEA4750060483000000000000000000000000000000000000000000076CA
      DD007EDBED007EDAED007DD9EC007DD8EB0082DAEC00A2E7F2005C9BB4005C9C
      B6005FA6C40031BB530066D88D0033CB670033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000191
      0200AED7AF000000000000000000000000000000000000000000000000000000
      0000000000002FB141002941E5002849FE002756FA002458F1002361EA002675
      E8002A85EC002986E800297FE8002970EB002F75F4004685E60069D5580062FA
      4F0053EC3D0057D9F1005EE6FE0063EBFB0063EBFB0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000006EA997003BC399003BC399003BC399003BC3
      99003BC399003BC399003BC399003BC39900DCF4ED0089DBC2003BC399003BC3
      990060AB94000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000538E670086B99300F4F9
      F500F2F7F20051A3560064B271004FA65600237927007EC1850047985C004A9D
      4B002661390081AD8800DDEADD0048926000255C3700FEF7F300D7AF9200FEF8
      F500B68A6A00D5AB8E00D4AA8C00D3A98A00FEF8F500B68A6A00FCEFE700FAE5
      D800EEA4750060483000000000000000000000000000000000000000000076CA
      DD007FDDEE007FDCEE007EDBED007EDAED007EDAEC007DD9EC0083D8E9008BCB
      DA005B98B2004ED07300E9FCEC0059D5840033CB670033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000099
      0000209521000000000000000000000000000000000000000000000000000000
      0000000000002FB141002338D500223BE9002654EF002A70F7002C85FF002E96
      FE0030A3FE0031A9FE0030A3FE00359BF9003786F100579896005ACA46005DEF
      460052E9400057D9F1005EE6FE0063EBFB0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000007DAB9E003BC59A003BC59A003BC59A003BC59A003BC5
      9A003BC59A003BC59A003BC59A003BC59A00F6FCFA0086DBC1003BC59A003BC5
      9A003BC59A0060AD950000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000057926A0086B99300F7FA
      F700F4F9F500F3F8F30051A356002E83310086C18C0047985C004A9D4B002661
      390060A0610060A06100DFECE0004D95630028603B00FEF7F300D9B19400FEF8
      F500B68A6A00FDF4EE00FDF3ED00D5AB8D00FEF8F500B68A6A00FCEEE400FBE8
      DC00EDA77B0060483000000000000000000000000000000000000000000076CA
      DD0080DFEF007FDEEF007FDDEE007FDCEE007EDBED007EDBED0089DFEF00AAEB
      F4005A97B00051B9A80070DF9000E9FCEC0059D5830033CB670032CB660030C8
      60002BC3570026BE4C0020B8400019B2330012AB24000AA31500029B05000099
      000001900100BEDEBF0000000000000000000000000000000000000000000000
      00000000000033BD4700318F5400307389002F6DCD002B71FE002C85FF002E96
      FE0030A3FE0031A9FE0035A9FB0046A4B20068BB710070E15F0070FE5E005DEF
      46004BD43C005EE6FE005EE6FE005EE6FE0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000040BE97003BC69B003BC69B003BC69B003BC69B0043C8
      9F0070D5B60051CCA6003BC69B0040C89E00FFFFFF0078D8BA003BC69B003BC6
      9B003BC69B003BC69B0060AD9500000000000000000000000000000000000000
      000000000000000000000000000000000000000000005D986F008FC19B00F9FC
      F900F7FAF700F4F9F500348C370098CB9D005CA9660052A35300337042000066
      0000E6EFE600E2EEE300E0ECE000529868002C633E00FEFAF800D9B29600D9B1
      9400B68A6A00FDF5F000FDF5F000D6AC8F00FEF8F500B68A6A00FDF3ED00FBE8
      DC00EBAA7F0060483000000000000000000000000000000000000000000076CA
      DD0081E0F10080DFF00080DFF00080DEEF007FDEEF007FDDEE008AE0F000ACEC
      F5005A97B00066B6D90053BFB00070DF9000E8FCEB0059D5830032CB660030C8
      60002BC3570026BE4C0054CA64002CBA420012AB24000AA31500029B05000099
      0000009501007FBF7F0000000000000000000000000000000000000000000000
      00000000000034D35E003FDA680049CF69004DBB5A004AA55C003C959100369B
      CD0034A6F20041A9B30068BB71008BE5800090FD830081FF700070FE5E0062FA
      4F0050E2490057DEFE005EE6FE0063EBFB0063EBFB0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000065AD96003BC89C003BC89C003BC89C0043CAA000DFF6
      EF00FFFFFF00F9FDFC006BD5B4004FCEA600FFFFFF0069D5B3003BC89C006ED6
      B60086DDC20047CBA2003BC89C0060AE95000000000000000000000000000000
      00000000000000000000000000000000000000000000619E730094C5A000FBFC
      FB00F9FCF900429446009ACBA4006CB475005CA96600427D520051A35600348C
      370000660000E6F0E600E4EFE400589A6B002F674200FEFAF800FEFAF800FEFA
      F800FEFAF800FEF9F600FEF8F500D7AE9100D6AD9000B68A6A00FDF3ED00FBEA
      DF00E9AC830060483000000000000000000000000000000000000000000076CA
      DD0081E2F20081E1F10081E1F10080E0F00080E0F00080DFF0008BE2F100AEED
      F5005A97B00068BADD0068BBDF0057C6B8006FDF9000E5FCE90057D4810030C8
      60002BC3570026BE4C003BAF4400A5F3AE007ADA82000AA31500029B05000099
      0000009900004FA74F0000000000000000000000000000000000000000000000
      0000000000006FD777003FD9670050E1730061E67F0073EB8A007DE38C0075CB
      75006EBF690094E69000A6FEA0009EFF940090FD830081FF700070FE5E005FF7
      4A0053DDA6005EE6FE005EE6FE005EE6FE0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000055B496003BCA9E003BCA9E0070D8B800FFFF
      FF0052D0A900D2F3E900B4EBDA005ED3AF00FFFFFF005BD3AE0090E1C800FEFF
      FF00F5FCFA00DFF6EF003FCBA0003BCA9E0062AF970000000000000000000000
      0000000000000000000000000000000000000000000069A47A009ACBA400FCFD
      FC0045974900A7D5B10074BA7B0074BA7B004F8D55005681590064B2710051A3
      5600348C370000660000E6F0E6005C9D6F00316A4500FEFAF800FEFAF800FEFA
      F800DBB49800FEFAF800FDF7F300FDF7F300FDF7F300FDF7F300FDF7F300FCEF
      E700E6B18D0060483000000000000000000000000000000000000000000076CA
      DD0082E4F30082E3F20081E2F20081E2F20081E1F10081E1F1008CE4F200AFEE
      F6005A97B00069BDE2006ABEE4006FC9EF005CD0C4006DDF8F00E0FCE50054D2
      7B002BC3570026BE4C000F991E0040C5720082F1A100A2EDA6000FA111000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      00000000000000000000000000006FD975005CDD710072E8850085F0950094F6
      9E00A0FBA400A0FBA400A6FEA0009EFF94008EF97D0073EF69005BE0810052D9
      D30057DEFE005EE6FE005EE6FE0066F0FD0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000055B497003BCC9F0053D2AB00FAFE
      FD00D0F3E800F8FDFC0090E2C8006DD9B700FFFFFF004DD1A800CBF1E500B4EC
      DA0061D6B200FFFFFF005BD4AF003BCC9F003BCC9F0062B09700000000000000
      000000000000000000000000000000000000000000006EA98000A1D0AC00FFFE
      FE0069AF6E0067AA6B0064A5680059975B00F6F9F600F3F8F300568159005681
      59005681590056815900E9F2E900619E7300336D4700DEBA9F00DEB99E00DDB7
      9C00DCB69B00DBB49800DAB39700D9B29500D9B19400D8AF9300FDF6F100FCF0
      E900E4B4950060483000000000000000000000000000000000000000000076CA
      DD0083E5F40082E5F40082E4F30082E4F30082E3F30082E3F2008DE6F300B1EF
      F6005A97B0006BC0E5006BC2E70070CBF20075D3FD005CD1C50069DF8D00D7FB
      DD004ECE710026BE4C00109F21005A97B0007EDD9E0063E1850066CC67000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000002FB5D00044C2AD0050C8
      A60080CE9A00ACE5A300B2EAA50090DBB30042C5D8004AD1F6004FD6FF0057DE
      FE0057DEFE005EE6FE005EE6FE005EE6FE0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000055B698003BCEA0006DDA
      B800B4ECDA0090E3C9003DCFA1007CDEBF00FFFFFF003FCFA2009AE6CE00FAFE
      FC00E9F9F400E8F9F40041D0A3003BCEA0003BCEA0003BCEA00079AE9E000000
      0000000000000000000000000000000000000000000075B08600A4D2AF00FFFE
      FE00FDFEFD00FDFEFD00FBFCFB00F9FCF900F8FBF800F6F9F600F3F8F300F2F7
      F200F0F6EF00EDF5ED00EBF3EB0065A17600356F4900FEFAF800FEFAF800FEFA
      F800DDB89C00FEFAF800FEF8F500FEFAF800FEF8F500D9B19400FEF8F500FDF1
      EA00E4B4950060483000000000000000000000000000000000000000000076CA
      DD0083E7F50083E6F50083E6F40083E5F40083E5F40082E5F4008EE7F400B3F0
      F7005A97B0006CC3E9006CC4EB0071CDF50075D4FE0075D5FF005CD1C60064DF
      8A00C9FBD00046CA6600109F21005A97B0000000000066D78B007DDF87000099
      0000009900003F9F3F0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000027AEFA0032B5FD0069C7
      FA00D4D5D500EEEDEA00FFFBF400FFFBF100ACE0F20046CAF9004FD6FF0057DE
      FE0057DEFE0057DEFE005EE6FE005EE6FE0066F0FD0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000055B899003BD0
      A2003BD0A2003BD0A2003BD0A2008AE3C800F4FCFA003BD0A2003CD0A20081E1
      C30099E6CE0050D5AC003BD0A2003BD0A2003BD0A2003BD0A2004EBC99000000
      0000000000000000000000000000000000000000000079B58A00A7D5B100FFFE
      FE00FFFEFE00FFFEFE00FDFEFD00FBFCFB00FAFCFA00F8FBF800F6F9F600F4F9
      F500F2F7F200EFF6EF00EEF4ED0068A2780037714B00FEFAF800FEFAF800FEFA
      F800DEBA9F00FEFAF800FEF8F500FEF8F500FEF8F500DAB39700FDF7F300FDF2
      EC00E2B5990060483000000000000000000000000000000000000000000076CA
      DD0084E9F60084E8F60084E7F50084E7F50083E7F50083E7F5008FE9F500B5F1
      F7005A97B0006DC6EC006EC7EE0072CEF70075D5FF0075D5FF0075D5FF005CD1
      C6005BDE8500B7FBC00025A732005A97B00000000000BDEDCD0070DB81000099
      00000098000060AF600000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000051A5D50051A5D500CDCF
      CF00B4B3B200C2C1BE00C5C5C200C2C1BE00C2C1BE006AB5D5004FD6FF004FD6
      FF0057DEFE005EE6FE0057DEFE005EE6FE0064EEFE0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000055B8
      99003BD2A3003BD2A3003BD2A3009AE8CF00E6F9F3003BD2A3003BD2A3003BD2
      A3003BD2A3003BD2A3003BD2A3003BD2A3003BD2A3003BD2A3004EBC9A000000
      000000000000000000000000000000000000000000007DB98F00A7D5B100A4D2
      AF00A1D0AC009DCCA8009ACBA40094C5A0008FC19B0086B9930086B993007DB3
      8A0079AF870074AB820071A981006EA67C0037714B00E2C0A600E1BEA400E1BE
      A400E0BCA200DFBAA000DEB99E00DDB89C00DCB69B00DBB49800FDF7F300FDF3
      ED00E1BA9E0060483000000000000000000000000000000000000000000076CA
      DD0085EAF70085EAF70084E9F70084E9F60084E9F60084E8F60091EBF600B7F2
      F8005A97B0006FC8EF006FC9F10072D0F80075D5FF0075D5FF0075D5FF0075D5
      FF005CD1C6004DDA7B004EBC6C005A97B00000000000CBF0D8005DCF6B000099
      0000009201008EC78E0000000000000000000000000000000000000000000000
      000000000000000000000000000000000000E3E4E000EEEDEA00F1F0ED00F6F5
      F100D4D5D500EEEDEA00FFFBF400FFFBF400FFFBF400F4F7EF0065CCF30050D6
      FA0057DEFE0063D5F500CCE7E50076D8EF005EE6FE0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000055BA9A003BD4A4003BD4A4009AE9D000CDF4E8003BD4A4003BD4A4003BD4
      A4003BD4A4003BD4A4003BD4A4003BD4A4003BD4A4003BD4A40050BD9A000000
      000000000000000000000000000000000000000000007DB98F0079B58A0075B0
      860070AC81006BA67D0065A17600609B73005A956D00538E67004F8A62004B86
      5E0046815A00427D52003D78510039744E0037714B00FFFEFE00FEFAF800FFFE
      FE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFE00FDF5F000FDF3
      EE00E1BA9E0060483000000000000000000000000000000000000000000076CA
      DD0086ECF80085EBF80085EBF80085EAF70085EAF70085EAF70092ECF700B9F4
      F9005A97B0006FCAF20070CCF30072D0F90075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0086DCFD00B7F3F8005A97B0000000000098E3B20051C355000099
      0000018C02000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EAE7E200FEFEFE00FEFEFE00FEFE
      FE00D2D1D000F4F7EF00FFFBF400FFFBF400FFFBF100FEF7E800FFFBF100A9DF
      E7007ABDD500F1F2E300FFF5E000B6E0E6005EE6FE0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000055BC9B003BD6A6003CD6A60044D8AA003BD6A6003BD6A6003BD6
      A6003BD6A6003BD6A60055BB9C0081B2A30042CAA0003BD6A60054BC9B000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D0BDAF00FFFEFE00FEFAF800FFFEFE00FFFEFD00FFFEFD00FFFD
      FC00FFFCFB00FFFCFB00FFFEFE00FFFCFB00816B57007A634E00725B45006A53
      3C006048300060483000000000000000000000000000000000000000000076CA
      DD0086EDF90086EDF90086ECF90086ECF80086ECF80085ECF80093EEF800BBF5
      F9005A97B00070CCF40070CDF50073D1FA0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0086DDFE00B9F4F9005A97B000000000006CCB79001BA71D000097
      010070B971000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EEE9E200FEFEFE00FEFEFE00FFFB
      F400D4D5D500F4F7EF00FFFBF400FFFBF100FFFBF100FFFBF100FFF5E000FFFB
      F100BFBCB000FFF5E000FFF5E000F9F2DD005EE3F60066F0FD006AC3D8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000055BD9C003BD8A7003BD8A7003BD8A7003BD8A7003BD8
      A7003BD8A7003BD8A700DEE0DF000000000093B5AB003BD8A70055BC9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000A7522900000000000000000000000000000000000000
      000000000000D0BDAF00FFFEFE00EFD2BD00EFD2BD00EDD0BB00EDCFB900FFFE
      FD00FFFDFC00FFFCFB00FFFEFE00FFFCFB00C1AE9F00FBE8DC00F5DED000EED4
      C3006048300000000000000000000000000000000000000000000000000076CA
      DD0087EEFA0087EEFA0086EEFA0086EEF90086EDF90086EDF90094EFF900BDF6
      FA005A97B00071CEF70072CEF70074D3FC0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0087DDFE00BBF5FA005A97B0000000000009A11300039A050030A0
      3300000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0E9E200FEFEFE00FEFEFE00FEFE
      FE00D2D1D000F5F3EF00FFFBF400FFFBF100FFFBF100FEF7E800F5F3EF00FEF7
      E800BFBCB000FFF5E000FEF7E800FFF5E00076D8EF0066F0FD006EC3D7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000055BE9D003BDAA8003BDAA8003BDAA8003BDA
      A8003BDAA8003BDAA800A5BCB600000000006BB59D003BDAA80058BC9C000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E68F6300A7522900000000000000000000000000000000000000
      000000000000D1BFB000FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFD00FFFE
      FD00FFFDFC00FFFCFB00FFFEFE00FFFCFB00C2B0A000FBE8DC00F5DED000654E
      36000000000000000000000000000000000000000000000000000000000076CA
      DD0087F0FB0087F0FB0087EFFB0087EFFA0087EFFA0087EEFA0095F0FA00BFF7
      FB005A97B00072D0F90074D2FC0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0087DDFE00BEF7FA005597A60033AB380009A11300219A2600CEE7
      D000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000F0E9E200FEFEFE00FEFEFE00FFFB
      F400D4D5D500FAF8F300FFFBF400FFFBF100FFFBF100FFFBF100FEF7E800FEF7
      E800BFBEB400FEF7E800FFF5E000FFF5E000A9DFE70066F0FD006AC3D8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000055BF9E003BDBA9003BDBA9003BDB
      A9003BDBA9003BDBA9003BDBA9003DD4A3003BDBA9003BDBA9005CBA9D000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000E7976F00E89A7100BD6A4100A752290095482300954823008C4421000000
      000000000000D1BFB000FFFEFE00FFFEFE00FFFEFE00FFFEFE00FFFEFD00FFFD
      FC00FFFDFC00FFFCFB00FEFAF800FFFCFB00C4B2A300FBE8DC006C553E000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0088F1FC0088F1FC0088F1FB0088F0FB0087F0FB0087F0FB0096F2FB00C1F8
      FB005A97B00075D4FE0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0088DEFE00C0F8FB002F9A5C000D9E190044A94800000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000EEE9E200FEFEFE00FEFEFE00FEFE
      FE00D2D1D000FFFBF400FFFBF400FFFBF100FFFBF100FEF7E800FEF7E800FEF7
      E800BFBEB400FFF5E000FFF5E000FFF5E000DFEBE00059E3F8006AC3D8000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000055BE9D003BDBA9003BDB
      A9003BDBA9003BDBA9003BDBA9003BDBA9003BDBA9003BDBA90094B6AC000000
      000000000000000000000000000000000000000000000000000000000000E89C
      7600F9C2A600F5AE8800EC9F7600DB8C6300D87D4F00BD6A4100954823000000
      000000000000D0BDAF00CFBDAF00CFBDAF00CCB9AB00CBB8AA00CAB7A900CAB7
      A900C9B6A700C9B6A700C9B6A700C6B4A400C4B2A300836E5900000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0088F2FD0088F2FC0088F2FC0088F2FC0088F1FC0088F1FC00A2F5FC00C3FA
      FC005B98B10075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF0088DEFE00C2F9FC005A9EAF00A1D3A40000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F0E9E200E9E1D500E8DF
      D200D9D1C500E9E1D500E9E1D500E9E1D500EEE6D700EEE6D700EEE6D700EEE6
      D700BFBCB000FFF5E000FCF2DA00FCF2DA00FCF2DA00A3DDE5009FCDD3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000A6BDB60095B6
      AC0095B6AC0095B6AC0095B6AC0095B6AC0097B7AC00B9C6C200000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000EAA68400F9C2A600BD6A4100E79A7300E68F6300D87D4F00E27239000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0089F3FD0089F3FD0089F3FD0089F3FD009FF6FD00B6F9FD00C5FBFD009CDA
      E4005EA0B90075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5FF0075D5
      FF0075D5FF009DE8FE00C4FBFD0063A5C1000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000E89C7600B7633A00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD0089F4FE008DF5FE00A1F7FE00BFFBFD00C2F9FB009EE3ED007FCEDF006EBF
      D90078D1F7008ADFFF008ADFFF009EE9FE009EE9FE00B2F2FE00B2F2FE00C6FC
      FD00C6FCFD00C6FCFD00C0F7FA0066ABC8000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E89C7600000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD00AAF9FE00C6FDFE00BFF7FA009AE0EB007BCDDF0095DDE900B9F4FA00C9FD
      FE00C9FDFE00C9FDFE00C9FDFE00B1EBF300B1EBF3009AD8E8009AD8E80088CA
      DF0082C6DC0071B8D40072B6D200C5E0EB000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000000000000000000076CA
      DD00BBF5F90096DEEA0075C9DC0089D1E300A2E0EC009BD9E8009BD9E80083C6
      DC0083C6DC006BB3D1006BB3D1008EC4DB008EC4DB00B4D7E600B4D7E600D9EA
      F100D9EAF1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000008DD0
      E10074C6DB0070BDD60073B7D3008EC4DB0097C9DD00B4D7E600BCDCE900D9EA
      F100D9EAF1000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000D8D7D700D3D3D300D1D1D100D2D2D200D4D4D400DCDC
      DC00000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000C6C6C600AAAAAA00DFDFDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000414141002D2D2D00424242006767
      6700656565006565650065656500656565006565650065656500656565006565
      65006969690055555500323232002B2B2B00C8C8C80000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000000000000000000000E0DE
      DD00C8C5C500A8A8A80095979700919193009090920091919300939393009A9B
      9B00B5B5B500D8D8D80000000000000000000000000000000000000000000000
      00000000000000000000DBDBDB00D6D6D600D2D2D200D0D0D000D0D0D000D2D2
      D200D6D6D600DBDBDB0000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000CBCBCB00ADAD
      AD00A0A0A000ABABAB00C5C5C500E0E0E000E0E0E000CFCFCF0000000000D5D5
      D500CDCDCD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000003030300017171700303030005A5A
      5A00575757005757570057575700575757005757570057575700575757005757
      57005D5D5D00454545001E1E1E0019191900CACACA0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B6B7BB0087B7
      E4005BB1F4003EAEF7002FAAF8002AA7F90025A8F9002AA8F80033ACF8003FAF
      F60052ACF2007FAEDE00A6AAB00000000000000000000000000000000000D0D0
      D000C1C1C1009395A500AD968A00F3B36C00F9BD7500FBC17D00FBC47F00FAC1
      8000F3BD7B00E3B47F00C6B4A300C2C2C200D0D0D00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000D1D1D100B3B3B300A2A2A200AFAFAF00CFCFCF00F4F4F400F8F8
      F800F4F4F400F0F0F000EBEBEB00E6E6E600E1E1E100C0C0C000CDCBCB00E8E8
      E700CFCECD000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000005F5F5F0061616100767676008989
      890095959500989898009A9A9A009C9C9C009C9C9C009C9C9C009B9B9B009898
      9800949494008A8A8A007272720053535300C0C0C00000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A9C5E00056C4FC0031BB
      FF0056D1FF004FC6FF0074E7FF0060D4FF006DE2FF0075E3FF0059CDFF006AE1
      FF0049C7FF0045CDFF0053C9FB00B6CBE00000000000D2D2D200B9B9B9009DA1
      AB004C74B7006CBBF6005EB4F60085A8D800FEF3DB00FEF5DF00FEF5DF00FEF3
      D900FEECCC00FED9AA00FDC78600DCA87300ACA8A300BABABA00D4D4D4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000B5B5B500A1A1
      A000BABABA00E4E4E400F5F5F500F9F9F900FCFCFC00FEFEFE00FCFCFC00F9F9
      F900F5F5F500F0F0F000ECECEC00E6E6E500DDD3C900E3DDD800EAE9E800EAE9
      E900C9C8C700D5D5D500CACACA00000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000007B7A7A00B8B5B600B0B3B800C3C5
      C600CDCCCC00D9D9D900E5E5E500F0F0F000F1F1F100E6E6E600D8D8D800CBCB
      CB00BDBEBE00989CA0008C8A8C0073707000B5B5B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000000000001E9EFD0056D0FF004FC0
      FF0071DEFF0062D0FF008DF6FF0072DEFF0084EAFF008AF1FF0066D4FF007FE6
      FF0053BFFF0051C2FF0052CAFF003DADFD00D6D6D600B2B2B2008993A500437B
      C5008DD2FB00B8F6FF00C7F6FF001C74E500F3E9C800FEF1C700FEF1C800FEF2
      CB00FEF4D000FEF5D900FEEED100FED6A700FDB16200BB9C7E00B2B2B200D7D7
      D700000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000DFDFDF00DEDEDE00E6E6
      E600EBEBEB00F0F0F000F4F4F400F8F8F800FCFCFC00FEFEFE00FCFCFC00F3EB
      E300E6CEB600DDB48A00D59A5F00D0853900D17E2A00DBC3AB00DEDDDC00E0DF
      DE00E7E6E500EAE9E800D1D0D000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000059676E00759EA300BE975F007967
      560036393B00525252006969690080808000848484006F6F6F00585858003B3D
      3E004E4340009E76400063766A003C5E6A00B7B6B50000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000128DFD0072DEFF0078E5
      FF008AF2FF008FF8FF0094FAFF0094FBFF0097FBFF0096FAFF008EF5FF0090F7
      FF007EE9FF0074DEFF005ECFFF0027A1FF00C8C8C800A0A0A0003A6EBD0099DA
      FD00A5F0FF007FE8FF00A8F0FF0087D0FC0084A5CF00FEEBBE00FEECBF00FEEC
      C000FEEABB00FEE7B800FEE9BC00FEEFCD00FEDEB500FCAD5E00BA977800C0C0
      C000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000F2F2F200E1E1E100E5E5
      E500EAEAEA00EFEFEF00EFEAE400E9D4BE00E2BB9400DAA26900D1883F00CF7D
      2900D07D2800D07E2800D17E2900D17E2900D07E2900D6BFA700D5D4D300D2D1
      D000E3E2E100EAEAE900D8D7D700B6B6B600B4B4B400D7D7D700000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000646E740087A0AD00B98E74007664
      5D002D3031004B4B4B00636363007A7B7B007F7F7F006A6A6A00515152003335
      3500483F3F008C6048005B616400425A6600BFBFBF0000000000000000000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00C9C9C900C3C3C300B5B5B500A7A7A70094949400848484007B7B7B007D7D
      7D008989890099999900AAAAAA00B8B8B800BFBFBF001998F8007DE2F60063C4
      E40059B4D80053AFD40052ACD2004FABD3004DADD5004EB1DD0052BBEF0057C7
      FB005DC9FD005FD1FF006CD9FF003AAEFD00D1D1D1005B83BD007BC4FA009FE9
      FF0074DFFF0074DFFF0075DFFF00B9ECFF00207BE800E9D9B800FEE5B500FEE5
      B500FEE4B200FEE1AD00FEDEA900FEDBA500FEE6BB00FEDEB700FB994700A797
      8A00B4B4B400BBBBBB00BABABA00BCBCBC00BFBFBF00C0C0C000C1C1C100C1C1
      C100C2C2C200C3C3C300C5C5C500CFCFCF0000000000F5F4F300DED9D400D9BB
      9D00D5A26E00D08A4400CE7C2900CF7C2800CF7C2700D17E2900D17E2900D07E
      2900D1802A00D1802A00D1802900D2812A00D2812A00D1BAA200DAD8D800E2E1
      E000E4E3E200DCDBDA00E6E5E500EAE9E800EAE9E800B3B3B300D5D5D5000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E3E3E300A0A0A1008E888500C8C0BC00B6B5B500B5B2
      AF00ABA6A200B5B0AC00C0BCB800CCC7C300CCC8C400C0BCB800B3AFAB00A6A1
      9D00A09C98008A8A890087817E0088807B0081818200C2C3C300000000000000
      000000000000000000000000000000000000000000000000000000000000CCCC
      CC00C9C9C900C3C3C300B6B6B600A7A7A7000058DC000058DE000058DE000058
      DE000058DB008191A500ABABAB00B8B8B800B9B9B9002092DB0067B8C300488E
      A6003D7C960038779200357691003474920034769400347B9B003A8DB60047A8
      D90051BDF4005ECDFC0079E3FF0047B9FF00CED7E4002F85EB00A8E3FF006ED8
      FF0068D5FF0069D7FF0067D5FF008DE0FF008ACBFB00809DC800FEDCA600FEDC
      A700FEDBA400FED9A100FED69D00FED69A00FED69B00FEE4BB00FDD1A400BF7B
      47007E7E7E008181810081818100838383008383830082828200868686008787
      87008787870087878700818181009F9F9F0000000000F6F3ED00E3B98600D284
      3400CF7C2700CF7C2800D07D2800D07E2800D17E2900D17F2900D2812A00D281
      2A00D2812A00D3822B00D3832C00D3832C00D4842C00D9C2A900D4D3D200DEDD
      DC00E1E0E000D6D5D500E1E0DF00EBEAE900EBEAE900DFDFDF00EDEDED000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B2B2BA0031327500BBB4AA00D6CCC100D3C9BF00D5C9
      C000D7CBC300D6C9C100D5C7BF00D4C5BE00D4C5BE00D6C7C000D8CAC200DBCC
      C400DBCEC400DDD0C500E1D4C800D1C6B5004D4C830056577500000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE005DA0F3008EBFF700529F
      F600045CE0000058DE000000000000000000ADADAD00ACB6BD00B9BBBB00B2B2
      B200ADADAD00A6A5A500AAAAAA00ACACAC00A6A5A500B2B3B300A6A5A500AABB
      BC006EA1A4008EEEF50077DDFF0097D7FD0084ADEB007ABFF9008ADBFF0059CA
      FF005BCDFF005CCDFF005ACBFF005FCCFF00A3DEFE00287EEA00E8C79A00FED2
      9500FED19300FED09100FECD8E00FECE8E00FECE8F00FED49A00FEE5C200F9A6
      68008C6B550066666600636363005E5E5E005B5B5B0057575700525252004E4E
      4E004C4C4C0048484800464646004D4D4D0000000000F9F2E800E4B88300D284
      3500D07C2800D07E2800D17F2900D17F2900D2812A00D3822B00D2822B00D484
      2C00D5852E00D4862E00D5872F00D5872F00D5872F00DBC4AC00D6D6D500D7D7
      D600E1E1E000E1E0DF00E6E5E400D0CFCE00E5E4E300DFDFDF00F4F3F2000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E0E0E00063616400ADACB400C4D1E600B8CBDF00B5CE
      DF00B2D1E000B0D4E000ADD7E100AEDAE300B0DDE500A9D9E300A6D5E200A3D1
      E100A0CAE0009DC2DE009DBDE200A6ADC000706D6E00ABABAC00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE0092C0F7000669E900408C
      EE0060A9F8000058DE000259DD00BCBCBC00B5B5B500A6A5A500D2D2D200A6A5
      A500E4E4E400A6A5A500E1E1E100E8E7E700A6A5A500DDDDDD00A6A5A500BEBD
      BD00B7B8B80087A9BB00AFDBF80000000000186EE40095D4FE005EC6FF004EC1
      FF0050C4FF0050C3FF004FC1FF004DBFFF007CD0FF0076BAF900859BC000FDC5
      8100FEC48000FEC37E00FEC47E00FEC47E00FEC68000FEC88600FEDEB100FCCE
      A400F5A37400F1F1F100EAEAEA00E6E6E600E3E3E300E2E2E200E1E1E100E0E0
      E000E0E0E000E0E0E000DDDDDD004B4B4B0000000000F9F1E600E2AE7200D385
      3500D17E2900D17F2900D2812A00D2812B00D3832C00D5862E00D5872F00D588
      2F00D78A3100D78A3200D78B3200D98C3400D88C3400D5BEA600DBDAD900E7E7
      E600E5E5E400DBDAD900E7E6E500DAD8D800E2E1E000D6B18900F4ECE3000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000092908D0094A7C6008E92A0009490840099A1
      8D009DB19300A1C19800A3C99B008BC4960075C19A0095C6980089B996007CA8
      93006F948E00647D8600375B89003770BE00A09D9900E1E1E100000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000058DE008FBFF800096CEA000061
      E5005199F1004292F2000058DE00A8A8A900D5D5D500A6A5A500EBEBEB00A6A5
      A500EFEEEE00B8B7B700F2F1F100FFFFFF00BCBCBC00F7F7F700B1B1B100C7C7
      C700D5D5D500A6A5A50000000000000000002B7DEA009BD7FF003FB5FF0043B7
      FF0044B9FF0045BAFF0044B7FF0040B6FF0043B4FF009ACBEE0084A1CA00DBB6
      9100FEB66B00FEB86C00FEB86A00FEB96C00FEBA6F00FEC07800FECC8E00FEDE
      BB00F27C4400D8C8BF00D2D2D200D1D1D100CDCDCD00C8C8C800BEBEBE00B1B1
      B100A2A2A20095959500E5E5E5005959590000000000F9F1E600E2AE7200D486
      3600D17F2900D2812B00D3822C00D4852E00D6873000D6893100D78B3200D88D
      3400D98E3500DA903600DA913700DA913800DB923800D4BEA600D5D4D300E3E2
      E200DFDEDE00DAD9D800E4E3E200E4E3E200ECEBEA00D9A05D00F4E8DA000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009F9793008593AC009A694800D9A87600E0B5
      7900EBC58000F3D58700FBDD8C00D1C26600AFB25800FBD16900EFC86900E5B4
      6100D9A05B00CC8F5600A35F3300546A9100ADA6A500E0DFDF00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000059DF009AC6F900026BEB000064
      E7000462E40081BDFB00146BE400A4A4A500EDEDED00A6A5A500FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00EEEEEE00ECEC
      EC00DDDDDD00A6A5A50000000000000000003887ED0093D1FF0031ABFF0037AE
      FF0039AFFF0038B0FF0037AEFF0033AAFF0036A6FA00C6CCD100D1D2D2009EA6
      B900FDAA5900FEAC5800FEAC5800FEAF5C00FEB26100FEB66900FEBD7400FEE3
      C000F17F5300F7DCCF00F9F9F900F9F9F900F9F9F900F8F8F800F8F8F800F7F7
      F700FBFBFB0093939300E0E0E0006969690000000000F9F1E600E2AE7200D488
      3700D2812B00D4842D00D5872F00D6893100D78B3300D98E3500D98F3600DB92
      3900DC933A00DC943B00DD963D00DE983E00DE983E00E6D1B800F0F0EF00EEEE
      ED00E1E0E000E7E7E600E7E7E600D1D0D000E9E8E700DAA25F00F5E8DA000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000E8EAED0079A7B7007598B6008F522C00BA7C3E00C790
      4600D4A54C00DEB85300E8C75800CBBC5600B1B25900E9C34400DFB94400D19E
      3900BE802A00A9611C008B3A0800446B980061BEDD00ADC1CA00000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000093BCF300025BDF009FCAFA00006FEF000067
      EA000062E6003787ED0068ADF800A7A8AA00F7F7F700EBEBEB00E5E5E500E1E1
      E100E1E1E100DFDFDF00DBDBDB00DBDBDB00D9D9D900D9D9D900DDDDDD00DFDF
      DF00E7E7E700BFBFBF000000000000000000408EEE0084CAFF0026A1FF002AA4
      FF002CA5FF002BA5FF0029A3FF0026A1FF0060A4DC00E6E6E600ECECEC00AFB3
      BB00EDAE7000FD9F4200FDA14600FDA34A00FDA64E00FDAC5800FDB36300FEDF
      BA00F0815E00F3CDBF00F3F3F300F2F2F200F0F0F000EEEEEE00EDEDED00ECEC
      EC00FBFBFB0099999900DADADA007777770000000000F9F1E600E3AF7300D68A
      3800D4852E00D5872F00D78A3200D98D3500DA903700DB933900DD953C00DE98
      3F00DF9A4000E09C4200E09C4300E19E4400E19E4400DDC9B000E2E1E100E3E3
      E200E3E3E200E4E3E200E5E4E400DBDBDA00E3E2E100DCA76300F5E8DA000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000B1DBF3002ADCFF003F99CA00883A0D00A6581200BA76
      2400CB913500D8A74600E4BA5600CBB15500BAB46300F1CF6E00E5C26C00DBAF
      6300CD985600B7783F0094410F002A619B0036DFFF007DC0D500000000000000
      00000000000000000000000000000000000000000000000000000000000065A1
      F0000C66E200015BDF00005ADE00005ADF000E66E40097C7FB000074F300006E
      EE000067E9000263E60085BFFC00AEAFAF00E5E5E500EBEBEB00E5E5E500E1E1
      E100DDDDDD00DBDBDB00D9D9D900D7D7D700D3D3D300D5D5D500D7D7D700DFDF
      DF00F1F1F100CBCBCB0000000000000000003785EB0087C8FF001B99FF001D9B
      FF001F9CFF001E9CFF001D9AFF002298F900BEC2C700E9E9E900E8E8E800D1D1
      D100D2B89F00FB973700FD963400FD983900FD9D3E00FDA34800FDAA5300FEDA
      B100EB755C00F0C8BC00F2F2F200F0F0F000EFEFEF00EDEDED00ECECEC00EBEB
      EB00FAFAFA0095959500D6D6D6007D7D7D0000000000F9F1E600E3B07500D78D
      3B00D6883000D88C3400DA8F3600DB923A00DD963C00DE983F00DF9A4100E09C
      4300E29F4600E2A14700E2A14800E3A34A00E3A34A00DBC7AF00E0DFDF00DDDC
      DC00E8E7E700EAEAE900E6E5E500E7E6E500EFEEEE00DFAB6600F6E9DA000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000BEDDF3006EDCFF00648CBF0082534500B0866B00BB96
      7400C6A77D00CDB48300D5BF8800B6AA6B00A5AD6E00E0D09300D6C39100CFB6
      8D00C6A98A00BE9F8A00A073620037609D007DDAFF0097BDD300000000000000
      00000000000000000000000000000000000000000000A6CBF7000162E6000461
      E400307CE60071A5ED0095BDF200A4C7F400A3CAF700067DF700017AF7000176
      F4000171EF00026CEC00046AEA00A7B1BD00B8B7B700E0E0E000FFFFFF00FFFF
      FF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFFFF00FFFF
      FF00F1F1F100D5DBE20000000000000000002978E80086C6FF001191FF001292
      FF001393FF001292FF001291FF006998C300DFDFDF00E0E0E000DBDBDB00E3E3
      E300B3B2B000E7A36400FD8F2800FD912B00FD953200FD9B3C00FDA24700FED4
      A800E8635600F0CEC600F1F1F100EFEFEF00EEEEEE00ECECEC00EAEAEA00E9E9
      E900F9F9F9008F8F8F00D2D2D2007F7F7F0000000000F9F1E700E4B27700D990
      3E00DA8D3600DA903800DC933B00DE973E00DE994000E09C4300E2A04700E2A2
      4900E4A44C00E5A64E00E5A85000E5A85000E6AA5100DAC7AE00D8D7D700E0DF
      DF00EAE9E900E7E7E600E8E7E700E3E2E200E6E6E500E1B16C00F6EADB000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000CEDDF00082B8FA003481F6004686ED004B8CF3005192
      EE0058A1EB005EB5EC0062CCEE0060D8EB005EDAEB0064D4F10060BFEE005BA9
      ED005497EE004C8DF2004A88EF002B7DF3007EB3F800ADBACD00000000000000
      000000000000000000000000000000000000AACFF900016AEA002278E9009ABF
      F00078B1F20056A8FB003296FA001C8BFA000680F9000280FB000280FA00037E
      F900047CF7000479F4000478F3000979F1005A93CF008AA6C300BFC3C700C4C4
      C500C4C4C500C8C9C900C9C9C900C9C9C900C8C9C900C3C4C500B9BCC000B4C0
      CC004988D6007CB3F30000000000000000001367E20079BBFC002297FF000B8C
      FF000B8DFF000B8CFF00198DF400B9BBBE00E1E1E100D3D3D300D1D1D100DEDE
      DE00CCCCCC00C6AF9B00FC892100FD8C2300FD8F2900FD943100FDA65200FBBF
      9700E1474800EEDBD600EEEEEE00EDEDED00EBEBEB00E9E9E900E8E8E800E5E5
      E500F7F7F7008D8D8D00D0D0D0007C7C7C0000000000F9F2E700E5B47900DB93
      4100DA903800DD953D00DF994100E09C4500E2A04900E3A34B00E4A54D00E5A8
      5000E6AA5200E8AD5600E9AE5800E8AE5800E9AF5900E1CEB600DADAD900DEDD
      DD00E8E8E800D9D9D800E7E6E600E1E0E000E7E6E600E3B57200F6EBDC000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000008699B8004D95FF0073ADFF006DAAFF006FAB
      FF0075B2FE007ABCFC007DC6FC0080CEFE0081CFFE007EC9FC007CC0FC0078B6
      FD0072ADFF006FAAFF0073ADFF004D95FF00819DCB00DEDEDE00000000000000
      0000000000000000000000000000000000000672ED00257EEC009AC0EF00004D
      C1000056CB000181FC000281FC000283FD000384FD000384FD000585FC000685
      FC000684FB000783FB000884FB000985FB00026BEB00005BE000005AE000026A
      EB000B8BFE000A8BFE000989FD000886FB000681FA00047BF6000275F3007EBC
      FB004F9FF7002E8AF200000000000000000083ADED0064A7F5004AA9FF000689
      FF000689FF000889FF006F94B700D3D3D300D7D7D700CBCBCB00CBCBCB00D1D1
      D100D9D9D900AAA8A600E3A67000FD881D00FD8B2200FD902B00FEB56E00F39C
      8300E26B6A00ECECEC00EBEBEB00EAEAEA00E7E7E700E6E6E600E4E4E400E1E1
      E100F4F4F40089898900CFCFCF007B7B7B0000000000F9F2E700E6B67B00DD96
      4400DC943C00DE984000E19D4600E3A14A00E4A54E00E6AA5400E7AD5800E8AF
      5900EAB05B00EAB25D00EAB25E00EBB46100EBB46100DBC8B100E0E0DF00E2E1
      E100E6E6E600E0DFDF00ECECEB00EDECEB00EFEEED00E5B97A00F6ECDD000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B1ADA9005593DE002598ED0044C2F5005ECC
      F70078CDF70092D2F700ACDBF900C8E6FC00D7EEFD00C2E9FC00A4E5FC0086E9
      FD006AE4FD0052CAF70038A7EF00227DDF00B1B5BE00DDDDDC00000000000000
      0000000000000000000000000000000000000274F000A6CDF800014DC100004C
      C0001187F8001E93FF002295FF002899FF000F8DFF000D8CFF001992FF0038A2
      FF0044A8FF001379EF000165E7000165E7000061E5009FFFFF009FFFFF00005F
      E4000164E7000F79F00046AAFF0046A9FE0038A1FD002F99FB001988F700449E
      F90095CAFD000575F0000000000000000000D9E6F8002877E70073B9FD000C8B
      FF000387FF001989EF00B1B2B300DADADA00C4C4C400C4C4C400C4C4C400C2C2
      C200D4D4D400C4C4C400BDA69300FB8F2D00FD891F00FD953400FCBC8B00E257
      5B00E7B7B300E8E8E800E7E7E700E6E6E600E4E4E400E2E2E200E0E0E000DFDF
      DF00EFEFEF0086868600CECECE007979790000000000F9F2E800E7B77D00DE99
      4700DF984100E19D4500E2A14A00E5A65000E7AB5500E8AE5900EAB25F00EBB5
      6300EBB66700ECB86800ECB86700ECB86800EEBA6A00E7D5BE00EAEAEA00F1F0
      F000E7E6E600E0DFDF00ECEBEB00DFDEDE00E7E7E600E6BE8000F7ECDE000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B1ACA900ABBDCE000991E20023C6F1004DD6
      F50071D6F50094DBF600B4E2F800D0EBFB00DCF3FD00CCF4FD00AFF5FD008FFE
      FF006DF9FD004BD7F500149FE90070A5D200CAC2BC00DBDBDD00000000000000
      0000000000000000000000000000000000000378F100C6DFFB001365D200238F
      F7002E9CFF00359FFF003AA1FF0044A8FF0046A9FF004CACFF0051AFFF0052B0
      FF004BABFE000C75ED00A6FFFF00A6FFFF00A5FFFF00A5FFFF00A5FFFF00A0FF
      FF0095F1FD002186ED00248AF50057B5FF005BB5FF0058B3FF0054AFFE003E9E
      FA00D2E9FF001180F4005EAAF700000000000000000084ADED00579DF20051AB
      FF000185FF007E94AD00CACACA00CACACA00BCBCBC00BDBDBD00BDBDBD00BCBC
      BC00C1C1C100D2D2D200A4A3A300E1A77200FD881C00FEB06800EF8A7500E06D
      6C00E6E6E600E5E5E500E4E4E400E3E3E300E1E1E100DFDFDF00DDDDDD00DADA
      DA00E9E9E90085858500CCCCCC007777770000000000F9F2E800E8B97F00E09D
      4B00DF9B4300E3A14A00E5A54F00E6AA5500E9AF5A00EAB36000ECB66600EDBA
      6B00EEBC7000F0BF7400F1C07600F1C07400F1C17400EFDEC800F6F5F500F6F5
      F500EAE9E900E0DFDE00E8E7E700DBDADA00EDECEC00E8C38800F7EDDF000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000AEAEAE00E8E1DD0071BBE30043C9F10065D8
      F4007DD6F30092D7F400A3D9F400B0DBF500B4E3F700AEE9F800A1EEFA008EF8
      FC0079F4FA005ED6F30049B0E500BFC6CC00BDBBBA00DBDBDB00000000000000
      000000000000000000000000000000000000057CF5003690F200BEDCFA009CCF
      FF002E9BFF0037A1FF003EA4FF00004CC0004BADFF004EAFFF0050B1FF0052B3
      FF0053B4FF001B88F6000473F0000473F0000470EF00B0FFFF00B0FFFF00278E
      F2009DF8FE0088E6FC000977F10056B7FF0057B6FF0059B6FF005AB5FF0055B1
      FF0090CAFE0089C5FD001787F6000000000000000000000000003266B5006AAC
      F700509FEA00A9A9AC00D3D3D300B4B4B400B6B6B600B6B6B600B7B7B700B6B6
      B600B4B4B400C9C9C900BCBCBC00AD9E9200F8AD6700F3977900DE5A5A00E4E2
      E200E4E4E400E2E2E200E2E2E200E0E0E000DFDFDF00DDDDDD00DADADA00D7D7
      D700E3E3E30083838300CBCBCB007777770000000000F9F3E800E8BB8100E2A0
      4E00E29F4800E4A44E00E7A95400EAAF5B00EBB56100EEB96900F0BD6F00F1C0
      7400F2C27800F3C47C00F3C77F00F4C88100F3C67D00E6D5C000E8E8E700EBEB
      EB00F0EFEF00F4F3F300EEEDED00DEDDDD00E8E7E700E9C78E00F7EEE0000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000B1B1B100ECECEB00E4E5E400D7E2E600DAE5
      E700DDE6E900E0E7EA00E1E8EB00E2E8EB00E2E8EB00E1E8EA00DEE7E900DBE6
      E600D6E3E300D0DCDF00D4D8DA00DAD8D700BDBEBE00DBDBDB00000000000000
      000000000000000000000000000000000000AFD6FB001084F5000C82F6006FB2
      F600A5D4FE0036A2FF003EA6FF0044ABFF004BB0FF004DB2FF0050B5FF0050B6
      FF0051B7FF0050B7FF0033A2FC00198AF700087BF400C0FFFF00C0FFFF002D97
      F700A5FBFF00A2FDFF000478F40053B9FF0056B9FF0059B9FF0058B7FF0055B4
      FF005AB4FF00D4EBFF000D84F80000000000000000000000000065738800045B
      DF00849BB700CCCCCD00D1D1D100B7B7B700B1B1B100B2B2B200B2B2B200B1B1
      B100B7B7B700C8C8C800D8D8D800A09E9C00EDBFB000DF625D00DDBBB600DBDB
      DB00DBDBDB00DBDBDB00DADADA00D9D9D900D7D7D700D4D4D400D1D1D100CECE
      CE00DBDBDB0083838300CACACA007676760000000000F9F3E900E9BC8300E3A3
      5100E5A44E00E8A95400EAAF5B00ECB56100EFB96800F0BD6F00F1C17500F2C3
      7A00F3C57E00F3C88200F3C98400F3CA8600F3CB8700E2D2BD00DADADA00E1E1
      E100F0F0F000F6F6F500F0F0EF00E2E2E100F1F0F000ECCB9500F8EFE2000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000ACACAC00EAEAEA00E7E6E600EAE7E700EBE8
      E800ECEAE900ECEAEA00ECEBEA00ECEBEA00EBEAEA00EBE9E900E9E7E700E7E4
      E400E5E2E200E2DFDE00DEDCDB00DADADA00BDBDBD00DBDBDB00000000000000
      000000000000000000000000000000000000000000000000000054AAF9001489
      F800B2D6F9006ABBFF003BA7FF0042ACFF0048B1FF004BB5FF004DB7FF004FB9
      FF004FBAFF002A9EFC00299AFA00A5DFFD00D0FAFF00D0FFFF00D0FFFF00A7FF
      FF00A5FFFF009DF9FF000983F90051BDFF0055BCFF0058BDFF0058BAFF0055B8
      FF004FB2FF00D0EAFF001089F90000000000000000000000000078787800B0B9
      C6004C73AF00A2A4A700BABABA00CBCBCB00D6D6D600D8D8D800D8D8D800D9D9
      D900D0D0D000C2C2C200ACACAC00B1837700E46A6600DECDCA00DDDDDD00DCDC
      DC00DCDCDC00DCDCDC00DBDBDB00DADADA00D9D9D900D7D7D700D4D4D400D2D2
      D200E0E0E00081818100C7C7C7007676760000000000FAF3E900EABE8500E5A6
      5500E7A75100E9AD5800EBB25F00EDB76500EFBB6C00F0BF7200F0C27800F2C5
      7D00F4C98300F3CA8600F5CD8A00F5CF8D00F5CF8E00E1D2BD00E6E6E600DAD9
      D900E9E9E800E7E7E700F1F1F100F5F5F500F5F5F500EECE9C00F8EFE3000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A9A9A900EAEAEA00E7E7E700E8E8E800E9E9
      E900E9E9E900EAEAEA00EAEAEA00EAEAEA00E9E9E900E8E8E800E7E7E700E5E5
      E500E2E2E200E0E0E000DCDCDC00D7D7D700B8B8B800DADADA00000000000000
      0000000000000000000000000000000000000000000000000000000000002493
      F9005EABF40070BDFD00015FE3001F8AF50047B4FF004BB8FF004CBAFF004EBD
      FF004FBEFF002CA1FC00E6F9FF00F7FFFF00D8F7FF00E4FFFF00E4FFFF00A9FF
      FF0097F4FF00299EFC0029A0FD0054C2FF0056C2FF0057C0FF0058BFFF0055BC
      FF0050B6FF00D6EEFF00118DFB0000000000000000000000000077777700C1C1
      C100808080009AB4DC0092A0B600A9A9A900A0A0A000A4A4A400A4A4A400A1A1
      A100A7A7A700B0B0B000D1A89D00E0A39300E0DEDE00E0E0E000E0E0E000E0E0
      E000E0E0E000E0E0E000E0E0E000DDDDDD00DDDDDD00DCDCDC00DADADA00D8D8
      D800EAEAEA0081818100C5C5C5007676760000000000FAF3E900EABE8600E6A8
      5700E7A85300E8AD5900EAB36000EEB96900EFBD7000F0C17600F2C67D00F3C9
      8300F5CC8800F6CF8D00F6D09000F7D29200F7D39400E0D1BD00E2E2E200E9E9
      E900E8E8E800E4E4E300F1F1F000F4F4F400F6F6F500EFD3A200F8F0E4000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A6A6A600EBEBEB00E6E6E600E9E9E900EAEA
      EA00EAEAEA00EAEAEA00EAEAEA00EAEAEA00E9E9E900E8E8E800E7E7E700E5E5
      E500E3E3E300E0E0E000DADADA00D4D4D400B3B3B300DADADA00000000000000
      00000000000000000000000000000000000000000000000000000000000081C2
      FB00289AFB00196FE5000158DE000262E4001087F90048BAFF0050C0FF0053C4
      FF0055C6FF003AAFFD00F3FFFF00E3F6FF0043B0FE00F7FFFF00F7FFFF0028A0
      FE00259FFD0033ABFE0058C9FF0058C9FF005AC8FF005CC7FF005CC5FF0059C2
      FF0078CCFF00C9E9FF001891FB0000000000000000000000000077777700BEBE
      BE0080808000EBEBEB00D7D7D700D9D9D900D8D8D800CFCFCF00D1D1D100D4D4
      D400D9D9D900DEDEDE00E0E0E000E1E1E100E0E0E000E1E1E100E0E0E000E1E1
      E100E0E0E000E0E0E000E1E1E100E1E1E100DFDFDF00DFDFDF00DEDEDE00DBDB
      DB00F0F0F00080808000C3C3C3007575750000000000FAF3EA00EBBF8700E6A9
      5800E7AA5500EAB05D00EBB56400EEBB6C00F1C07400F1C37A00F2C78100F5CC
      8800F5CE8D00F6D19100F8D49600F7D59800F8D69A00E6D8C400DFDFDF00F7F7
      F600EEEEED00D9D9D800EBEBEB00DFDFDF00F7F7F600F0D7A800F9F0E5000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000A2A2A200EEEEEE00E7E7E700E9E9E900ECEC
      EC00ECECEC00ECECEC00ECECEC00ECECEC00EBEBEB00EAEAEA00E9E9E900E7E7
      E700E4E4E400DEDEDE00D7D7D700D3D3D300AFAFAF00D9D9D900000000000000
      0000000000000000000000000000000000000000000000000000000000002098
      FA001984F2000E62E1000C69E5001D87F400309EFD0033A2FF0056C6FF005DCC
      FF0060CEFE003EB3FE00E9FAFF00EFFBFF0084D0FE00FCFFFF00FCFFFF0036AC
      FE0033AAFF0035ACFF0062D1FF0061D0FF0061CFFF0062CFFF0063CEFF0061CB
      FF00C2EAFF007DC8FE0076BEFC0000000000000000000000000077777700BBBB
      BB0080808000F0F0F000DDDDDD00DEDEDE00DFDFDF00E1E1E100E1E1E100E2E2
      E200E2E2E200E2E2E200E4E4E400E4E4E400E4E4E400E4E4E400E3E3E300E4E4
      E400E4E4E400E4E4E400E4E4E400E3E3E300E1E1E100E1E1E100E0E0E000E0E0
      E000F4F4F40080808000C1C1C1007575750000000000FAF3EA00EBC08900E7AB
      5B00E7AB5700EAB15F00EDB86800EFBC6F00F1C17600F2C67E00F4CA8500F5CE
      8B00F7D19100F7D39500F8D69A00F9D89E00F9D89E00E1D4C100E7E7E700DCDC
      DC00E7E7E700E9E9E900ECECEC00DEDEDD00EAEAEA00F2DAAD00F9F1E6000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000009C9C9C00EBEBEB00E2E2E200E3E3E300E4E4
      E400E6E6E600E7E7E700E8E8E800E8E8E800E6E6E600E5E5E500E3E3E300DFDF
      DF00DBDBDB00D5D5D500CFCFCF00CDCDCD00AAAAAA00D7D7D700000000000000
      000000000000000000000000000000000000000000000000000000000000259C
      FC002788F000318FF2004AA9FB009AD1FE00E4F3FF006FCFFE0065D4FF0068D6
      FF006AD8FF003EB5FF00A3DFFF00E2F7FF00FBFFFF00FEFFFF00FEFFFF00FCFF
      FF00F5FFFF004DB8FF0060D1FF006BD8FF006AD8FF006BD7FF006CD7FF008EE0
      FF00D4F1FF00269BFB000000000000000000000000000000000076767600B7B7
      B70080808000F2F2F200E1E1E100E3E3E300E4E4E400E4E4E400E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5
      E500E5E5E500E5E5E500E5E5E500E5E5E500E5E5E500E4E4E400E4E4E400E4E4
      E400F7F7F70080808000C0C0C0007575750000000000FAF3EA00EBC08A00E7AC
      5C00E8AD5A00EBB36100EDB86900F0BE7200F2C37900F3C78100F5CD8800F6CF
      8E00F7D39300F8D69900F8D89D00F9DAA100FADBA300E0D3C000E2E2E200EEEE
      EE00F0F0F000F8F8F800F3F3F200E5E5E500EFEEEE00F2DBB200F9F1E6000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000090909000A2A2A200A8A8A800B2B2B200BBBB
      BB00C2C2C200C7C7C700CACACA00CCCCCC00CBCBCB00CACACA00C6C6C600C1C1
      C100B9B9B900AEAEAE00A3A3A3009C9C9C008787870000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002BA0
      FC00A2D4FF00DAEFFF00EAF6FF00B2DFFF005DC0FD00DFF6FF0099E7FF0074E1
      FF0076E3FF0066D5FF0041B8FF0042B7FF0049BCFF00FEFEFF00F9FDFF0047BB
      FF0046BAFF003FB6FF0077E3FF0074E0FF0074E0FF0075E1FF00A2EBFF00E2F8
      FF0042ADFF00AED9FD000000000000000000000000000000000076767600B3B3
      B30080808000F3F3F300E5E5E500E6E6E600E6E6E600E7E7E700E7E7E700E7E7
      E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E7E7E700E8E8E800E8E8
      E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E8E8E800E7E7E700E7E7
      E700F8F8F80080808000BFBFBF007575750000000000FAF3EA00ECC18A00E7AD
      5E00E8AD5A00ECB46300EDB96B00F0BF7300F3C47B00F3C88200F5CD8900F7D1
      9000F8D49500F8D79B00FADAA000F9DBA400F4D7A600EADFD200EBEBEB00EAEA
      E900E1E1E100DCDCDC00E9E9E800E8E8E800EFEFEF00F3DEB500F9F2E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000A5A5A500D9D9D900DEDEDE00E0E0
      E000E2E2E200E2E2E200E3E3E300E2E2E200E2E2E200E1E1E100DFDFDF00DCDC
      DC00D9D9D900D3D3D300C9C9C900C0C0C000AFAFAF0000000000000000000000
      0000000000000000000000000000000000000000000000000000000000002AA3
      FC0032AAFF0038AFFE003DB3FE0040B5FE0048B6FC0058C0FF00D5F4FF00E6FB
      FF00ABF2FF0082ECFF0081EBFF0082ECFF0042B8FF0049BCFF0047BBFF0044BA
      FF008BF3FF0086EFFF0081EBFF007EE7FF00ABEFFF00E8FBFF00D0F3FF0043B1
      FF009CD3FD00000000000000000000000000000000000000000076767600AFAF
      AF0080808000FAFAFA00F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5F500F5F5
      F500FAFAFA0080808000BEBEBE007575750000000000FAF3EA00ECC18B00E8AD
      5F00E9AE5C00EBB46300EFBA6C00F0BF7300F2C47B00F5CA8300F5CE8D00F2CE
      9600F4D8AE00F4E1C400F2E6D500E6E0D900E5E5E400E5E5E500E7E7E700E3E3
      E300E6E6E600EAEAEA00F2F2F200EDEDEC00F3F3F200F4DEB700F9F2E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      000000000000000000000000000000000000BBBBBB00E3E3E300EDEDED00EBEB
      EB00ECECEC00EDEDED00EDEDED00EDEDED00EDEDED00ECECEC00EAEAEA00E8E8
      E800E5E5E500E3E3E300E8E8E800BDBDBD000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000A3DBFD0048BBFF0074CD
      FF00D2F5FF00EDFDFF00C6F8FF00ACF5FF00A1F5FF0092F7FF0093FAFF0099FC
      FF00A8FBFF00B1F7FF00C7F7FF00EDFCFF00C9F1FF0069C7FF0041B0FD00B9E0
      FD0000000000000000000000000000000000000000000000000076767600ADAD
      AD008A8A8A008A8A8A008B8B8B008D8D8D008F8F8F0091919100939393009494
      94009696960098989800999999009C9C9C009E9E9E00A1A1A100A2A2A200A4A4
      A400A4A4A400A4A4A400A4A4A400A5A5A500A4A4A400A5A5A500A5A5A500A5A5
      A500A5A5A500A5A5A500BEBEBE007575750000000000FAF3EA00ECC18B00E8AD
      5F00E9AE5C00ECB56800EABB7700EECA9600F2D9B600F1E3CF00E7DFD700E6E6
      E500E7E7E700E3E3E300E4E4E400E8E8E800ECECEC00EEEDED00ECEBEB00F0F0
      F000EDEDED00EDEDED00EDEDED00E6E6E600F1F1F100F5DFB700F9F2E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000BABABA00F5F5F500F3F3
      F300F3F3F300F5F5F500F5F5F500F6F6F600F6F6F600F5F5F500F3F3F300F1F1
      F100EEEEEE00ECECEC00EAEAEA00C7C7C7000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000D0EBFD0054BE
      FD0049BCFF006BCAFF00B2EAFF00D6F7FF00E6FBFF00FCFFFF00FFFFFF00FBFF
      FF00E7FCFF00D4F6FF00ADE8FF0062C6FF0042B7FF0075C6FD00000000000000
      000000000000000000000000000000000000000000000000000075757500AEAE
      AE008585850085858500878787008A8A8A008D8D8D008F8F8F00929292009393
      930096969600999999009A9A9A009D9D9D009E9E9E00A0A0A000A2A2A200A3A3
      A300A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4A400A4A4
      A400A4A4A400A4A4A400BFBFBF007676760000000000F7EFE500E5B78000E8C5
      9A00F0DBC200EDE4DA00F3F1F000F4F4F400F1F1F100EAEAEA00EFEFEF00F0F0
      F000EEEEEE00EBEBEB00E8E8E800E7E7E700E4E4E300E2E2E200E0E0DF00DFDE
      DE00DCDBDB00E0E0E000DCDBDB00E5E4E400F5F4F400F5E0B700F9F2E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      00000000000000000000000000000000000000000000CBCBCB00DEDEDE00F3F3
      F300F1F1F100F3F3F300F5F5F500F7F7F700F7F7F700F5F5F500F2F2F200EFEF
      EF00ECECEC00F2F2F200B5B5B500000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000D6EDFD0094D5FD0059BFFD0045B9FF0045B9FF0045B9FF0045B9FF0046BA
      FF0047BAFF0046BAFF0076CAFD00ACDDFD000000000000000000000000000000
      000000000000000000000000000000000000000000000000000085858500A1A1
      A100AEAEAE00B1B1B100B4B4B400B9B9B900BEBEBE00C4C4C400C8C8C800CCCC
      CC00CFCFCF00D2D2D200D4D4D400D5D5D500D7D7D700D7D7D7008C8C8C00DBDB
      DB00F0F0F000F0F0F000FB922700FFEF8A00FFEF8A00118BFE006BB7FF006BB7
      FF00118BFE000D5FDC00B5B5B5008C8C8C0000000000F4EBE200CA722700D1AA
      8900D5B49700DCBB9C00DDBD9E00DEBFA000DFC1A100E4C7A600E8CCAC00E9CE
      AE00EBD1B100ECD4B400EDD7B700EEDABA00EEDCBE00F0DEC200F1E1C500F2E2
      C800F2E4CB00F3E5CD00F4E6CF00F4E7D200F4E8D400F6DEB000F9F1E7000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000F1F1F100F7F7
      F700F7F7F700F7F7F700F8F8F800F9F9F900F9F9F900F8F8F800F8F8F800F7F7
      F700F6F6F600F7F7F70000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000DEDEDE008A8A
      8A00828282008383830085858500878787008B8B8B008E8E8E00929292009595
      9500989898009A9A9A009B9B9B009D9D9D009F9F9F009F9F9F00898989008888
      88008888880088888800FC7F1100FF780000FF7800000083FE000084FF000084
      FF000083FE000B5DDA009B9B9B00E1E1E1000000000000000000EACFB700DDAD
      7F00DCA67800DAA37100DAA47000DBA67200DDA87400DFAB7600E0AF7800E2B2
      7B00E3B57F00E5B88200E6BC8600E8C08A00E9C28E00EAC59300ECC89700ECCB
      9C00EDCC9F00EECEA100EFD0A400EFD1A700EFD2A800F0D3AB00000000000000
      000000000000000000000000000000000000424D3E000000000000003E000000
      2800000080000000C00000000100010000000000000C00000000000000000000
      000000000000000000000000FFFFFF00C7F01FFFFFFFFFFF0000000000000000
      830001FFF00000FF0000000000000000910000FF8000003F0000000000000000
      808001FF8000003100000000000000008000007F000000010000000000000000
      C000003F000000030000000000000000E000001F000000000000000000000000
      F000000F000000000000000000000000E0000007000000040000000000000000
      E0000007800000030000000000000000C0000003800000030000000000000000
      C0000003800000070000000000000000C0000003800000070000000000000000
      80000001800000070000000000000000800000018000000F0000000000000000
      8000000180000007000000000000000080000001800000070000000000000000
      800000018000001F0000000000000000800000018000003F0000000000000000
      800000018000007F0000000000000000C0000003800000FF0000000000000000
      C0000003801001FF0000000000000000C0000003803F19FF0000000000000000
      E0000007803F9FFF0000000000000000E0000007C07FFFFF0000000000000000
      F000000FE0FFFFFF0000000000000000F800001FE1FFFFFF0000000000000000
      FC00003FE0FFFFFF0000000000000000FE00007FE0FFFFFF0000000000000000
      FF8001FFE0FFFFFF0000000000000000FFE007FFE0FFFFFF0000000000000000
      FFFFFFFFFFFFFFFF0000000000000000FFF0040FFFFFFFFFFC007FFFFFFFFFFF
      FF800003FFFFFFFFE00007FFFFFFFFFFFE000001FFFFFE1FC00001FFFFFDFFF1
      FC000001FFFFE007C0000003FFF87FE0F8000000FFFFC007C0000001FFF23FC0
      F0000000F8000003C0000000FFF01F80E0000000E0000003C0000000FFF80F03
      C0000000C0000003E000000080000007C000000080000003F000000080000007
      8000000080000003E0000000800000078000000180000003E000000080000007
      0000000000000007E0000000800000070000000000000007E000000080000007
      0000000000000007000000008000000700000000000000070000000080000003
      000000000000001F0000000080000001000000008000001F0000000080000001
      000000008000003F0000000080000001000000008000007F0000000080000007
      00000000800000FF800000008000000700000001800000FFE000000080000003
      800000018000007FFFF8000080000003800000018000007FFFF8000080000007
      800000038000007FFFF8000080000007C00000038000007FFFF8000080000007
      E00000078000007FFFF8000080000007E000000F800001FFFFF8000080000007
      F000001F800003FFFFF8000080000007F800003F800003FFFFF80000FFFFFFFF
      FE00007FC00007FFFFF80000FFFFFFFFFF0001FFE0001FFFFFFC0001FFFFFFFF
      FFE007FFFFFFFFFFFFFFC01FFFFFFFFFFFFFFFFFFFFFFFFFFFFF01FFFFFF803F
      FFFFFFFFFFFFFE3FFFFE003FFFFE000FFFFFFFFFFFFFF007FFF8000FFFFC0007
      E0000007FFFFE003C0000007FFF80003E0000007E000000180000003FFF00001
      E0000007C000000100000003FFF00001E0000007C000000100000003FFE00000
      E0000007C000000000000043FFE00000E0000007C000000000000003FFE00000
      E0000007C000000000000007FFE00000E0000007C00000010000000700000000
      E0000007C00000010000000300000000E0000007C00000010000000300000000
      E0000007C00000038000000300000000E0000007C00000078000000300000000
      E0000007C0000007C000000300000000E0000007C0000007E000700700000000
      E0000007C0000007F800707F00000000E0000007C0000007FC00787F00000000
      E0000007C0000007FC00783F00000000E0000007C0000007F8007C3F00000000
      E0000007C0000007F0003C1F00000000E0000007E0000007F0003E1F00000000
      E0000007E0000007E0003F0F00000000E0000007E000000FE0001F0700000000
      E0000007E000001FE0001F8700000000E0000007E000003FE0001F8700000000
      E0000007E007FFFFF0001FC300000000E0000007F01FFFFFF0003FE300000000
      FFFFFFFFFFFFFFFFF0003FE300000000FFFFFFFFFFFFFFFFF8007FF300000000
      FFFFFFFFFFFFFFFFFC00FFFF00000000FFFFC0FFFFFFFC07FC7FFE7FFFFFFFFF
      FFFFC03FFFFFF001F03FF81FFFFFFFFFFFFE001FFFFFE000F01FF01FFFFFFFFF
      FFFC000FFFFFE000E01FF00FFFFFFFFFFFF8001F00002000E01FF00FFFFFFFFF
      FFF0000700002000E000000F9694B5B9FFF0000700002000E000000F90848431
      FFF0080F00002000F00FE01F9FFFFFF1FFE01C0700002000F807C00F9FFFFFF1
      FFE03C0700002000FC03800F92322E01FFE0380700002000C000000F90122601
      FFF0300F00002000C000000F90122601FFE0000F00002000C000000790122601
      FFC0000F000020008000000790122601FFE0001F000020008000000790122601
      FFF0007F000018038000000790122601FF00007F000003FB8000000390122601
      FD0000FF000000038000000390122601F80003FF000000038000000390122601
      F800BFFF000000038000000390122601E0007FFF000000038000000190122601
      E0007FFF000000038000000190122601E060FFFF000000038000000990122601
      C0E07FFF00000003C000000990122601C0C07FFF00000003C000000990122601
      E080FFFF00000003C000000190122E01C000FFFF00000003E0000001FFFFFFFF
      C001FFFF00000003E0000021FFFFFFFFE003FFFF80000003E0000023FFFFFFFF
      E003FFFFE0000003FFFFFFE3FFFFFFFFF81FFFFFF0000003FFFFFFE3FFFFFFFF
      FC9FFFFFFC000003FFFFFFF7FFFFFFFFFF1FF87FFFFFFFFFFFFFFFFFFFFFFFFF
      FE0F803FFFFFFFFFFFFFFFFFFFFFFFFFF804001FFFFFFFFFFFFFFFFFFFFFFFFF
      F000001FFFFFFFFFFFFFFFFFFFF80003E000003FFFFFFFFFFFFFFFFFFFF80003
      E000003FFF003F9FFFF9FFFF80000003E000003FFC00001FFFF0FFFF80000003
      E000003FF800001FFFE07FFF80000003E000003FF800001FFFC03FFF80000003
      E000001FF800001FFF801FFF80000003E000000FF800001FFF000FFF80000003
      E0000007F800001FFE0007FF80000003E0000007F800001FFC0003FF80000003
      E0000003F800001FFC0001FF80000003E0000003F800001FFC0000FF80000003
      E0000003F800001FFE00007F80000003E0000003FE00001FFF00003F80000003
      E0000003FF80001FFF80001F80000003E0000083FF80001FFFC0001F80000003
      E0000083FF80001FFFE0001F80000003E0000083FF00001FFFF0001F80000003
      E0000087FF00001FFFF8001FFFF80003E0000087FF00001FFFFC011FFDF80007
      E000008FFF00001FFFFE011FF9F8000FE000000FFF00001FFFFF001FF018001F
      E000003FFF00001FFFFF801FE018003FE000007FFF80001FFFFFC03FF01FFFFF
      E00000FFFFFFFFFFFFFFFFFFF9FFFFFFE00000FFFFFFFFFFFFFFFFFFFDFFFFFF
      E00000FFFFFFFFFFFFFFFFFFFFFFFFFFE00007FFFFFFFFFFFFFFFFFFFFFFFFFF
      E007FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFC0FFFFFFFFFFFFE3FFF
      FF00007FFFFFE003FC03FFFFFFC027FFFF00007FFFFFC001E0007FFFF80007FF
      FF00007FFFFF800080001FFFC00001FFFF00007FFFFF800000000FFF800001FF
      FF00007FFFFF800000000FFF8000003FFF00007FE0000000000000008000001F
      FC00003FE0000000000000008000001FFC00003FFF030000000000008000001F
      FC00003FFF000001000000008000001FFE00003FFF000003000000008000001F
      FE00003FFF000003000000008000001FFC00003FFE000003000000008000001F
      FC00003FE0000003000000008000001FFC00003F80000003000000008000001F
      FC00003F00000003000000008000001FFE00003F00000003000000008000001F
      FE00003F00000003000000008000001FFE00003F00000001800000008000001F
      FE00003F00000001C00000008000001FFE00003F00000001C00000008000001F
      FE00003FC0000001C00000008000001FFE00003FE0000001C00000008000001F
      FE00003FE0000001C00000008000001FFE00003FE0000001C00000008000001F
      FE00003FE0000003C00000008000001FFE00007FE0000003C00000008000001F
      FF00007FE0000007C00000008000001FFF0000FFFF80000FC00000008000001F
      FF8000FFFFC0003FC00000008000001FFF8001FFFFF000FFC00000008000001F
      FFC003FFFFFFFFFFC0000000C000003F00000000000000000000000000000000
      000000000000}
  end
  object PicDlg: TOpenPictureDialog
    Filter = 
      'All (*.png;*.jpg;*.jpeg;*.gif;*.tif;*.tiff;*.gif;*.png;*.jpg;*.j' +
      'peg;*.bmp;*.tif;*.tiff;*.ico;*.emf;*.wmf)|*.png;*.jpg;*.jpeg;*.g' +
      'if;*.tif;*.tiff;*.gif;*.png;*.jpg;*.jpeg;*.bmp;*.tif;*.tiff;*.ic' +
      'o;*.emf;*.wmf|PNG graphics from DevExpress (*.png)|*.png|JPEG gr' +
      'aphics from DevExpress (*.jpg)|*.jpg|JPEG graphics from DevExpre' +
      'ss (*.jpeg)|*.jpeg|GIF graphics from DevExpress (*.gif)|*.gif|TI' +
      'FF graphics from DevExpress (*.tif)|*.tif|TIFF graphics from Dev' +
      'Express (*.tiff)|*.tiff|GIF Image (*.gif)|*.gif|Portable Network' +
      ' Graphics (*.png)|*.png|JPEG Image File (*.jpg)|*.jpg|JPEG Image' +
      ' File (*.jpeg)|*.jpeg|Bitmaps (*.bmp)|*.bmp|TIFF Images (*.tif)|' +
      '*.tif|TIFF Images (*.tiff)|*.tiff|Icons (*.ico)|*.ico|Enhanced M' +
      'etafiles (*.emf)|*.emf|Metafiles (*.wmf)|*.wmf'
    Left = 364
    Top = 484
  end
  object PopBkGr: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopBkGrPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 444
    Top = 268
    object Gnhnhnn1: TMenuItem
      Action = CmdSetBkgr
    end
    object Xahnhnn1: TMenuItem
      Action = CmdClearBkgr
    end
    object N30: TMenuItem
      Caption = '-'
    end
    object Canhchnh2: TMenuItem
      Action = CmdBkgrOption
    end
  end
  object PopFunc: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopFuncPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 444
    Top = 300
    object ItemCat: TMenuItem
      Caption = 'D'#7841'ng th'#7875' lo'#7841'i'
      OnClick = ItemCatClick
    end
    object Outlook2: TMenuItem
      Tag = 1
      Caption = 'D'#7841'ng Outlook'
      OnClick = ItemCatClick
    end
    object TaskList2: TMenuItem
      Tag = 2
      Caption = 'D'#7841'ng danh s'#225'ch'
      OnClick = ItemCatClick
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ItemCloseAll: TMenuItem
      Caption = #272#243'ng t'#7845't c'#7843' th'#7875' lo'#7841'i'
      OnClick = ItemCloseAllClick
    end
  end
  object PopKinhdoanh: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 364
    Top = 364
    object nthng1: TMenuItem
      Action = CmdDONDATHANG
      object nthngkhchhng1: TMenuItem
        Action = CmdDondhKH
      end
      object nthng2: TMenuItem
        Action = CmdDdhNCC
      end
    end
    object NhpXutTn1: TMenuItem
      Action = CmdNHAPXUATKHO
      Caption = 'Nh'#7853'p xu'#7845't H'#224'ng h'#243'a'
      object Phiunhp2: TMenuItem
        Action = CmdNhap
        ImageIndex = 7
      end
      object Xuttr2: TMenuItem
        Action = CmdXuattra
      end
      object N4: TMenuItem
        Caption = '-'
      end
      object Phiuxut2: TMenuItem
        Action = CmdXuat
        ImageIndex = 6
      end
      object Nhptr2: TMenuItem
        Action = CmdNhaptra
      end
      object N20: TMenuItem
        Caption = '-'
      end
      object Nhpkhc2: TMenuItem
        Action = CmdNhapkhac
      end
      object Xutkhc2: TMenuItem
        Action = CmdXuatkhac
      end
      object N25: TMenuItem
        Caption = '-'
      end
      object Chuninvtnh1: TMenuItem
        Action = CmdChuyenDvt
      end
      object Vnchuynnib1: TMenuItem
        Action = CmdChuyenkho
      end
      object Kimkkho2: TMenuItem
        Action = CmdKiemke
        ImageIndex = 15
      end
      object N13: TMenuItem
        Caption = '-'
      end
      object iuchnhphiunhp1: TMenuItem
        Action = CmdNhapDC
      end
      object iuchnhphiuxut1: TMenuItem
        Action = CmdXuatDC
      end
    end
    object Bnl1: TMenuItem
      Action = CmdBANLEMENU
      object Hanbnl1: TMenuItem
        Action = CmdBanle
      end
      object Nhptrbnlhan1: TMenuItem
        Action = CmdNhaptrabl
      end
      object Nhptrbnl1: TMenuItem
        Action = CmdNhaptrabl2
      end
      object N14: TMenuItem
        Caption = '-'
      end
      object Chitkhukhuynmi2: TMenuItem
        Action = CmdKhuyenmai
      end
      object Khuynmitheohnghab2: TMenuItem
        Action = CmdDmvtBo
      end
      object Khuynmitheohanhngha2: TMenuItem
        Action = CmdKhuyenmai3
      end
      object N18: TMenuItem
        Caption = '-'
      end
      object hqutng1: TMenuItem
        Action = CmdPhieuQuatang
      end
    end
    object nkho1: TMenuItem
      Action = CmdTonkho
    end
    object Linktsnthngmaiint1: TMenuItem
      Action = CmdTMDT
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Boco1: TMenuItem
      Action = CmdBcKD
    end
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 676
    Top = 364
    object Danhmcchun1: TMenuItem
      Action = CmdDanhmuc
      object Danhmckhohng1: TMenuItem
        Action = CmdDmkho
      end
      object Danhmcmu2: TMenuItem
        Action = CmdDmmau
      end
      object Danhmcsize2: TMenuItem
        Action = CmdDmsize
      end
      object LoithVIP1: TMenuItem
        Action = CmdLoaiVIP
      end
      object Khchhngthnthit1: TMenuItem
        Action = CmdDmVIP
      end
      object Ngnhng1: TMenuItem
        Action = CmdDmNganhang
      end
      object N10: TMenuItem
        Caption = '-'
      end
      object Danhmcchiph1: TMenuItem
        Action = CmdDmchiphi
      end
      object N15: TMenuItem
        Caption = '-'
      end
      object Tgi1: TMenuItem
        Action = CmdTygia
      end
      object Vtral1: TMenuItem
        Action = CmdDmdialy
      end
      object chcphngban1: TMenuItem
        Action = CmdDmPhongban
      end
      object ChcdanhChcv1: TMenuItem
        Action = CmdDmChucvu
      end
      object Cnhbonhn1: TMenuItem
        Action = CmdDmCanhBao
      end
      object Danhmcbiumu1: TMenuItem
        Action = CmdDmBieuMau
      end
      object N29: TMenuItem
        Caption = '-'
      end
      object Ccquyc2: TMenuItem
        Action = CmdDmkhac
      end
      object Danhmckhc2: TMenuItem
        Action = CmdDmkhac3
      end
    end
    object Danhmckinhdoanh1: TMenuItem
      Action = CmdDmKinhdoanh
      object Danhmchngha2: TMenuItem
        Action = CmdDmvt
      end
      object Nhmhngha1: TMenuItem
        Action = CmdDmnhom
      end
      object N32: TMenuItem
        Caption = '-'
      end
      object LoithVIPS1: TMenuItem
        Action = CmdLoaiVip2
      end
      object ChitkhutheoloithVIPS1: TMenuItem
        Action = CmdLoaiVipCt2
      end
      object Danhschnhcungcp1: TMenuItem
        Action = CmdDmKhNcc
      end
      object Khchhng1: TMenuItem
        Action = CmdDmkh
      end
      object Nhcungcp2: TMenuItem
        Action = CmdDmncc
      end
      object N33: TMenuItem
        Caption = '-'
      end
      object ChitkhutheoloithVIP1: TMenuItem
        Action = CmdLoaiVipCT
      end
      object N34: TMenuItem
        Caption = '-'
      end
      object Danhmchtr1: TMenuItem
        Action = CmdDmHotro
      end
    end
    object Danhmcsnxut1: TMenuItem
      Action = CmdDmSanxuat
      Visible = False
      object NhmNVLBTPTP1: TMenuItem
        Action = CmdDmhhNhom_POM
      end
      object Nguynvtliu1: TMenuItem
        Action = CmdDmNVL
      end
      object Bnthnhphm1: TMenuItem
        Action = CmdDmBTP
      end
      object hnhphm1: TMenuItem
        Action = CmdDmTP
      end
      object N6: TMenuItem
        Caption = '-'
      end
      object Hsm1: TMenuItem
        Action = CmdDmHesoDodam
      end
      object Hsdungtch1: TMenuItem
        Action = CmdDmHesoDungtich
      end
      object N8: TMenuItem
        Caption = '-'
      end
      object Danhschchiph1: TMenuItem
        Action = CmdDmChiphiPOM
      end
      object Danhmchtr2: TMenuItem
        Action = CmdDmHotroSX
      end
    end
    object CmdDanhmucNhomNganhFB1: TMenuItem
      Action = CmdDanhmucFB
      object CmdDanhmucNhomNganhFB2: TMenuItem
        Action = CmdDanhmucNhomNganhFB
      end
      object Danhmcbn2: TMenuItem
        Action = CmdDanhmucBAN
      end
      object N39: TMenuItem
        Caption = '-'
      end
      object Nguynphliu1: TMenuItem
        Action = CmdDmNPL_FB
      end
      object hcn1: TMenuItem
        Action = CmdDmhh_FB
      end
      object N38: TMenuItem
        Caption = '-'
      end
      object ChitkhutheoloithVIPFB1: TMenuItem
        Action = CmdLoaiVipCTFB
      end
      object CmdDanhmucChiPhiFB1: TMenuItem
        Action = CmdDanhmucChiPhiFB
        Visible = False
      end
      object DanhmchtrFB1: TMenuItem
        Action = CmdDanhmuchotroFB
      end
    end
    object Danhmcnhns1: TMenuItem
      Action = CmdDmNhansu
      object Loiqutrnhlmvic1: TMenuItem
        Action = CmdDmQuatrinh
      end
      object Nhmlmvic1: TMenuItem
        Action = CmdDmNhomlv
      end
      object Bclng1: TMenuItem
        Action = CmdDmBacLuong
      end
      object Loiphcp1: TMenuItem
        Action = CmdDmPhucap
      end
      object N48: TMenuItem
        Caption = '-'
      end
      object Loihpnglaong1: TMenuItem
        Action = CmdDmHopdong
      end
      object LdoKhenthngKlut1: TMenuItem
        Action = CmdDmLyDoKTKL
      end
      object Ldovngmt1: TMenuItem
        Action = CmdDmVangmat
      end
      object aimlmvic1: TMenuItem
        Action = CmdDmDiaDiemLamViec
      end
      object Ningkkhmbnh1: TMenuItem
        Action = CmdDmDKKhambenh
      end
      object Nguntuyndng1: TMenuItem
        Action = CmdDmNguonTuyenDung
      end
      object isnlaong1: TMenuItem
        Action = CmdDmThietBiTaiSanLaoDong
      end
      object N35: TMenuItem
        Caption = '-'
      end
      object Danhmchtr3: TMenuItem
        Action = CmdDmHotroNS
      end
    end
    object Danhmckton1: TMenuItem
      Action = CmdDmKetoan
      object Danhmctikhon1: TMenuItem
        Action = CmdDmTaikhoan
      end
      object HthngtikhonKton1: TMenuItem
        Action = CmdhethongTaikhoanKT
      end
      object N36: TMenuItem
        Caption = '-'
      end
      object Danhmchtr4: TMenuItem
        Action = CmdDmHotroKT
      end
    end
    object Danhmctisn1: TMenuItem
      Action = CmdDmTaisan
      Visible = False
      object Danhmctisn2: TMenuItem
        Action = CmdDmvtTS
        Hint = '@ASS_DM_VT'
      end
      object Nhmtisn1: TMenuItem
        Action = CmdDmnhomTS
      end
      object N27: TMenuItem
        Caption = '-'
      end
      object Nhcungcp1: TMenuItem
        Action = CmdDmnccTS
      end
      object N28: TMenuItem
        Caption = '-'
      end
      object PhngbanBphn1: TMenuItem
        Action = CmdDmPhongbanTS
        Hint = '@SZ_PUB_DM_PHONGBAN'
      end
      object Nhnvin1: TMenuItem
        Action = CmdDmnvTS
        Hint = '@SZ_PUB_DM_NHANVIEN'
      end
      object N37: TMenuItem
        Caption = '-'
      end
      object Mu1: TMenuItem
        Action = CmdDmmauTS
        Hint = '@SZ_PUB_DM_MAU'
      end
      object Size1: TMenuItem
        Action = CmdDmSizeTS
        Hint = '@SZ_PUB_DM_SIZE'
      end
      object Htr1: TMenuItem
        Action = CmdDmHotroTS
      end
    end
  end
  object PopNhansu: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 508
    Top = 364
    object uyndng1: TMenuItem
      Action = CmdHrTuyendung
      Visible = False
      object Hsngvin1: TMenuItem
        Action = CmdTuyendungHoso
      end
      object Ktquphngvn1: TMenuItem
        Action = CmdTuyendungKetqua
      end
      object Bocotuyndng1: TMenuItem
        Action = CmdBcNSTuyendung
      end
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Boconhns1: TMenuItem
      Action = CmdDmnv
      Visible = False
    end
    object Hsnhnvin1: TMenuItem
      Action = CmdProfile
    end
    object N46: TMenuItem
      Caption = '-'
    end
    object Lchs1: TMenuItem
      Action = CmdHrLichsu
      object Qutrnhlmvic1: TMenuItem
        Action = CmdQuatrinh
      end
      object Hpnglaong1: TMenuItem
        Action = CmdHrHopDongLaoDong
      end
      object KhenthngKlut1: TMenuItem
        Action = CmdKhenthuong
      end
      object Gitisnthitb1: TMenuItem
        Action = CmdThietbi
      end
      object incngoi1: TMenuItem
        Action = CmdCongtac
      end
    end
    object Nghvic1: TMenuItem
      Action = CmdDangky
      object ngkvngmt1: TMenuItem
        Action = CmdVangmat
      end
      object ngktngca2: TMenuItem
        Action = CmdTangCa
      end
      object ngkmangthai1: TMenuItem
        Action = CmdMangThai
      end
      object ngkthaisn1: TMenuItem
        Action = CmdThaisan
      end
      object N45: TMenuItem
        Caption = '-'
      end
      object Nghvic2: TMenuItem
        Action = CmdThoiviec
      end
    end
    object uyndng2: TMenuItem
      Action = CmdHrTuyendung
    end
    object oto1: TMenuItem
      Action = CmdDaotao
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object Qunlphpnm1: TMenuItem
      Action = CmdPhepnam
    end
    object Qunlnghb1: TMenuItem
      Action = CmdNghiBu
    end
    object Chmcng1: TMenuItem
      Action = CmdChamcong
      object Calmvic1: TMenuItem
        Action = CmdDmCalamviec
      end
      object Ngyltt1: TMenuItem
        Action = CmdDmNgayle
      end
      object Lchlmvic1: TMenuItem
        Action = CmdLichlamviec
      end
      object Mychmcng1: TMenuItem
        Caption = '-'
        OnClick = CmdTbMaychamcongExecute
      end
      object Mychmcng2: TMenuItem
        Action = CmdDmMCC
      end
      object CpmthLyvntay1: TMenuItem
        Action = CmdVantay
      end
      object N5: TMenuItem
        Caption = '-'
        Visible = False
      end
    end
    object N47: TMenuItem
      Caption = '-'
    end
    object Bngchmcng1: TMenuItem
      Action = CmdBangcong
    end
    object Bngtnhlng1: TMenuItem
      Action = CmdBangluong
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object Boconhns2: TMenuItem
      Action = CmdBcNS
    end
    object Bocochmcng2: TMenuItem
      Action = CmdBcNSCong
    end
  end
  object PopDulieu: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 364
    Top = 428
    object Tnkhouk1: TMenuItem
      Action = CmdTonkhoDK
    end
    object N26: TMenuItem
      Caption = '-'
    end
    object XutExcel1: TMenuItem
      Action = CmdExport
    end
    object N16: TMenuItem
      Caption = '-'
    end
    object Ktthc2: TMenuItem
      Action = CmdQuit
      ImageIndex = 1
    end
  end
  object PopHethong: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 396
    Top = 428
    object Cuhnhhthng1: TMenuItem
      Action = CmdConfig
    end
    object MkhaPhiuangsa1: TMenuItem
      Action = CmdUnlockChungtu
    end
    object N12: TMenuItem
      Caption = '-'
    end
    object Inmvch2: TMenuItem
      Action = CmdInlabel
    end
    object Danhschquythungn2: TMenuItem
      Action = CmdCounter
    end
    object N17: TMenuItem
      Caption = '-'
    end
    object Khas1: TMenuItem
      Action = CmdKhoaso
    end
    object N22: TMenuItem
      Caption = '-'
    end
    object imtkhu2: TMenuItem
      Action = CmdSetpass
    end
  end
  object PopTrogiup: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 428
    Top = 428
    object Hngdnsdng2: TMenuItem
      Action = CmdHelp
      ImageIndex = 0
    end
    object N21: TMenuItem
      Caption = '-'
    end
    object Cchipthnggp1: TMenuItem
      Action = CmdFaq
    end
    object Danhschkhchhng1: TMenuItem
      Action = CmdMyCustomer
    end
    object N31: TMenuItem
      Caption = '-'
    end
    object Giithiu2: TMenuItem
      Action = CmdAbout
      ImageIndex = 13
    end
  end
  object PopSanxuat: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 436
    Top = 364
    object Githnh1: TMenuItem
      Action = CmdGiathanh
    end
    object Quytrnhsnxut1: TMenuItem
      Action = CmdQuytrinhSX
    end
    object N19: TMenuItem
      Caption = '-'
    end
    object Bocosnxut1: TMenuItem
      Action = CmdBcSX
    end
  end
  object PopKetoan: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 588
    Top = 364
    object hu1: TMenuItem
      Action = CmdGrThu
      object MenuItem1: TMenuItem
        Action = CmdThu
      end
      object MenuItem2: TMenuItem
        Action = CmdThu2
      end
      object MenuItem3: TMenuItem
        Action = CmdThuHT
      end
      object MenuItem4: TMenuItem
        Action = CmdThuThungan
      end
      object MenuItem5: TMenuItem
        Action = CmdThukhac
      end
    end
    object Chi1: TMenuItem
      Action = CmdGrChi
      object MenuItem7: TMenuItem
        Action = CmdChi
      end
      object MenuItem8: TMenuItem
        Action = CmdChi2
      end
      object MenuItem9: TMenuItem
        Action = CmdChiHT
      end
      object MenuItem10: TMenuItem
        Action = CmdChiThungan
      end
      object MenuItem11: TMenuItem
        Action = CmdChikhac
      end
    end
    object MenuItem12: TMenuItem
      Caption = '-'
    end
    object MenuItem13: TMenuItem
      Action = CmdKetchuyen
    end
    object Hangitrgiatng1: TMenuItem
      Action = CmdHoadonGTGT
    end
    object MenuItem14: TMenuItem
      Caption = '-'
    end
    object MenuItem15: TMenuItem
      Action = CmdCongnoKH
    end
    object MenuItem16: TMenuItem
      Action = CmdCongnoNCC
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Bocokton1: TMenuItem
      Action = CmdBcKT
    end
  end
  object PopBaocao: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 772
    Top = 364
    object Bocokinhdoanh1: TMenuItem
      Action = CmdBcKD
    end
    object Bocosnxut2: TMenuItem
      Action = CmdBcSX
    end
    object N23: TMenuItem
      Caption = '-'
    end
    object Boconhns3: TMenuItem
      Action = CmdBcNS
    end
    object Bocotuyndng2: TMenuItem
      Action = CmdBcNSTuyendung
    end
    object Bocochmcng1: TMenuItem
      Action = CmdBcNSCong
    end
    object N24: TMenuItem
      Caption = '-'
    end
    object BocoFB1: TMenuItem
      Action = CmdBaocaoFB
    end
    object N44: TMenuItem
      Caption = '-'
    end
    object Bocokton2: TMenuItem
      Action = CmdBcKT
    end
  end
  object PopFnB: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 844
    Top = 364
    object MenuItem6: TMenuItem
      Action = CmdDondhNCCFB
    end
    object NhpXutKhoFB1: TMenuItem
      Action = CmdNhapxuatkhoFB
      object PhiunhpFB1: TMenuItem
        Action = CmdNhapFB
      end
      object XuttrFB1: TMenuItem
        Action = CmdXuattraFB
      end
      object N42: TMenuItem
        Caption = '-'
      end
      object NhpkhcFB2: TMenuItem
        Action = CmdNhapkhacFB
      end
      object XutkhcFB1: TMenuItem
        Action = CmdXuatkhacFB
      end
      object N43: TMenuItem
        Caption = '-'
      end
      object iukhoFB1: TMenuItem
        Action = CmdDieukhoFB
      end
    end
    object KimkFB3: TMenuItem
      Action = CmdKiemkeFB
    end
    object ChbinFB1: TMenuItem
      Action = CmdCheBienFB
    end
    object HanbnlFB1: TMenuItem
      Action = CmdHoaDonBanLeFB
    end
    object NhptrbnlFB1: TMenuItem
      Action = CmdNhapTraBanLeFB
    end
    object CmdTonkhoFB1: TMenuItem
      Action = CmdTonkhoFB
    end
    object N40: TMenuItem
      Caption = '-'
    end
    object NhpkhcFB1: TMenuItem
      Action = CmdKhuyenmaiFB
      Caption = 'Khuy'#7871'n m'#227'i - Chi'#7871't kh'#7845'u'
      object Khuynmitheochitkhunggi1: TMenuItem
        Action = CmdKhuyenmaiChietkhauFB
      end
      object KhuynmitheoCombo1: TMenuItem
        Action = CmdKhuyemaiComboFB
      end
      object Khuynmitheoslng1: TMenuItem
        Action = CmdKhuyenmaidachieuFB
      end
    end
    object N41: TMenuItem
      Caption = '-'
    end
    object KimkFB1: TMenuItem
      Action = CmdBaocaoFB
    end
  end
end
