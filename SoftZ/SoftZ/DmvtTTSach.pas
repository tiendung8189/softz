﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtTTSach;

interface

uses
  Classes, Controls, Forms,
  wwDataInspector, DBCtrls, ActnList, Db, ExtCtrls, Grids, StdCtrls, wwdblook,
  wwSpeedButton, wwDBNavigator, wwclearpanel, RzPanel, RzDBNav;

type
  TFrmDmvtTTSach = class(TForm)
    Inspect: TwwDataInspector;
    Action: TActionList;
    CbNXB: TwwDBLookupCombo;
    CbSize: TwwDBLookupCombo;
    Panel1: TPanel;
    RzDBNavigator1: TRzDBNavigator;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
  private
  public
  	procedure Execute(pStr: String);
  end;

var
  FrmDmvtTTSach: TFrmDmvtTTSach;

implementation

uses
	ExCommon, isLib, isDb, isMsg, Dmvt, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtTTSach.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtTTSach.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtTTSach.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtTTSach.Execute;
var
	n, h: Integer;
begin
	n := exConfigInspector(Inspect, pStr);
    if n < 12 then
    	with Inspect do
        begin
            h := Height;
            Height := Trunc(n * (h - 4) / 12.0) + 4;
            ApplySettings;
            Self.Height := Self.Height + Height - h;
        end;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtTTSach.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1
end;

end.
