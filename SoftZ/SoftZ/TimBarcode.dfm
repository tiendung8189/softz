﻿object FrmTimBarcode: TFrmTimBarcode
  Left = 408
  Top = 428
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'T'#236'm M'#227' v'#7841'ch'
  ClientHeight = 66
  ClientWidth = 268
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = TntFormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = TntFormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 251
    Height = 49
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label1: TLabel
      Left = 19
      Top = 16
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'M'#227' v'#7841'ch'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdBarcode: TEdit
      Left = 75
      Top = 12
      Width = 155
      Height = 24
      CharCase = ecUpperCase
      Color = 15794175
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
  object QrBarcode: TADOQuery
    Connection = DataMain.Conn
    Filtered = True
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MABH'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'MAVT'
      '  from'#9'DM_HH_CT'
      ' where'#9'MABH = :MABH')
    Left = 4
    Top = 6
  end
end
