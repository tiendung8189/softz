object FrmXuatTra: TFrmXuatTra
  Left = 308
  Top = 68
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Xu'#7845't Tr'#7843
  ClientHeight = 573
  ClientWidth = 1104
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    1104
    573)
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 0
    Top = 46
    Width = 1104
    Height = 2
    Align = alTop
    Shape = bsSpacer
    ExplicitTop = 39
    ExplicitWidth = 792
  end
  object ToolMain: TToolBar
    Left = 0
    Top = 0
    Width = 1104
    Height = 46
    AutoSize = True
    ButtonHeight = 44
    ButtonWidth = 69
    Caption = 'ToolMain'
    Color = 16119285
    DisabledImages = DataMain.ImageNavi
    EdgeBorders = [ebBottom]
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentColor = False
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object BtnThem: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
    end
    object ToolButton8: TToolButton
      Left = 69
      Top = 0
      Width = 8
      Caption = 'ToolButton8'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton3: TToolButton
      Left = 77
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton1: TToolButton
      Left = 146
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton2: TToolButton
      Left = 215
      Top = 0
      Width = 8
      Caption = 'ToolButton2'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object BtnXoa: TToolButton
      Left = 223
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 33
    end
    object ToolButton9: TToolButton
      Left = 292
      Top = 0
      Width = 8
      Caption = 'ToolButton9'
      ImageIndex = 8
      Style = tbsSeparator
    end
    object BtnIn2: TToolButton
      Left = 300
      Top = 0
      Cursor = 1
      Hint = 'In phi'#7871'u'
      Caption = 'In'
      DropdownMenu = PopIn
      ImageIndex = 4
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object SepChecked: TToolButton
      Left = 394
      Top = 0
      Width = 8
      Caption = 'SepChecked'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton6: TToolButton
      Left = 402
      Top = 0
      Cursor = 1
      Action = CmdChecked
      ImageIndex = 29
    end
    object SepBarcode: TToolButton
      Left = 471
      Top = 0
      Width = 8
      Caption = 'SepBarcode'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object btnDmhh: TToolButton
      Left = 479
      Top = 0
      Cursor = 1
      Hint = 'Danh m'#7909'c'
      Caption = 'Danh m'#7909'c'
      DropdownMenu = PopDanhmuc
      ImageIndex = 8
      ParentShowHint = False
      ShowHint = True
      Style = tbsDropDown
      OnClick = BtnInClick
    end
    object btn2: TToolButton
      Left = 573
      Top = 0
      Width = 8
      Caption = 'btn2'
      ImageIndex = 7
      Style = tbsSeparator
    end
    object ToolButton7: TToolButton
      Left = 581
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object PgMain: TPageControl
    Left = 0
    Top = 48
    Width = 1104
    Height = 525
    Cursor = 1
    ActivePage = TabSheet1
    Align = alClient
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Tahoma'
    Font.Style = []
    HotTrack = True
    ParentFont = False
    TabOrder = 1
    TabStop = False
    OnChange = PgMainChange
    OnChanging = PgMainChanging
    object TabSheet2: TTabSheet
      Caption = ' Danh s'#225'ch '
      ImageIndex = 1
      object Status: TStatusBar
        Left = 0
        Top = 475
        Width = 1096
        Height = 21
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Panels = <
          item
            Width = 650
          end
          item
            Width = 50
          end>
        SimplePanel = True
        UseSystemFont = False
      end
      object GrBrowse: TwwDBGrid2
        Left = 0
        Top = 49
        Width = 1096
        Height = 426
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size'
          'IMG2;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'3'#9#9'F'
          'IMG2'#9'3'#9#9'F'
          'NGAY'#9'10'#9'Ng'#224'y'#9'F'#9'Ch'#7913'ng t'#7915
          'SCT'#9'18'#9'S'#7889#9'F'#9'Ch'#7913'ng t'#7915
          'THANHTOAN'#9'15'#9'Thanh to'#225'n'#9'F'
          'MADT'#9'10'#9'M'#227#9'F'#9'Nh'#224' cung c'#7845'p'
          'MAKHO'#9'5'#9'M'#227#9'F'#9'Kho h'#224'ng')
        MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = True
        Align = alClient
        DataSource = DsNX
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        KeyOptions = [dgAllowInsert]
        Options = [dgTitles, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
        ParentFont = False
        PopupMenu = PopMaster
        TabOrder = 1
        TitleAlignment = taCenter
        TitleFont.Charset = ANSI_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 2
        TitleButtons = True
        UseTFields = False
        OnDblClick = GrBrowseDblClick
        OnEnter = CmdRefreshExecute
        ImageList = DataMain.ImageMark
        TitleImageList = DataMain.ImageSort
        PadColumnStyle = pcsPadHeader
      end
      inline frDate: TfrNGAY
        Left = 0
        Top = 0
        Width = 1096
        Height = 49
        Align = alTop
        Color = 16119285
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentColor = False
        ParentFont = False
        TabOrder = 0
        ExplicitWidth = 1096
        inherited Panel1: TPanel
          Width = 1096
          ExplicitWidth = 1096
        end
      end
    end
    object TabSheet1: TTabSheet
      Caption = ' Chi ti'#7871't '
      object PaTotal: TPanel
        Left = 0
        Top = 449
        Width = 1096
        Height = 47
        Hint = 'G'#245' F9 '#273#7875' t'#237'nh l'#7841'i t'#7893'ng s'#7889' ti'#7873'n'
        Align = alBottom
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        object ImgTotal: TImage
          Left = 12
          Top = 8
          Width = 32
          Height = 32
          AutoSize = True
          Transparent = True
        end
        object PaThanhtoan: TPanel
          Left = 977
          Top = 2
          Width = 117
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 2
          DesignSize = (
            117
            43)
          object EdTriGiaTT: TDBNumberEditEh
            Left = 2
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 100
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' thanh to'#225'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhToan'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
        end
        object PaSotien1: TPanel
          Left = 248
          Top = 2
          Width = 214
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            214
            43)
          object DBNumberEditEh3: TDBNumberEditEh
            Left = 2
            Top = 17
            Width = 101
            Height = 22
            TabStop = False
            ControlLabel.Width = 65
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Ti'#7873'n C.K MH'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTienCKMH'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBNumberEditEh4: TDBNumberEditEh
            Left = 105
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 99
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' sau C.K MH'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhTienSauCKMH'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaSotien: TPanel
          Left = 2
          Top = 2
          Width = 246
          Height = 43
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 0
          DesignSize = (
            246
            43)
          object DBNumberEditEh1: TDBNumberEditEh
            Left = 46
            Top = 17
            Width = 89
            Height = 22
            TabStop = False
            ControlLabel.Width = 79
            ControlLabel.Height = 13
            ControlLabel.Caption = 'T'#7893'ng s'#7889' l'#432#7907'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoLuong'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBNumberEditEh2: TDBNumberEditEh
            Left = 137
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 66
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' h'#224'ng'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhTien'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
        object PaSotien2: TPanel
          Left = 462
          Top = 2
          Width = 275
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 3
          DesignSize = (
            275
            43)
          object EdTLCK: TDBNumberEditEh
            Left = 4
            Top = 17
            Width = 54
            Height = 22
            ControlLabel.Width = 53
            ControlLabel.Height = 13
            ControlLabel.Caption = '% C.K H'#272
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'TyLeCKHD'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBNumberEditEh6: TDBNumberEditEh
            Left = 60
            Top = 17
            Width = 101
            Height = 22
            ControlLabel.Width = 64
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Ti'#7873'n C.K H'#272
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clWhite
            Ctl3D = False
            DataField = 'SoTienCKHD'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
          object DBNumberEditEh5: TDBNumberEditEh
            Left = 164
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 98
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' sau C.K H'#272
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhTienSauCKHD'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
        end
        object PaThue: TPanel
          Left = 737
          Top = 2
          Width = 240
          Height = 43
          Align = alRight
          BevelOuter = bvNone
          TabOrder = 4
          DesignSize = (
            240
            43)
          object DBNumberEditEh7: TDBNumberEditEh
            Left = 2
            Top = 17
            Width = 109
            Height = 22
            TabStop = False
            ControlLabel.Width = 98
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Tr'#7883' gi'#225' tr'#432#7899'c thu'#7871
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'ThanhToanChuaThue'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object EdTienVAT: TDBNumberEditEh
            Left = 113
            Top = 17
            Width = 127
            Height = 22
            TabStop = False
            ControlLabel.Width = 53
            ControlLabel.Height = 13
            ControlLabel.Caption = 'Ti'#7873'n thu'#7871
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = 8404992
            ControlLabel.Font.Height = -11
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = [fsBold]
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            Anchors = [akTop, akRight]
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = clBtnFace
            Ctl3D = False
            DataField = 'SoTienThue'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <
              item
                Action = CmdThue
                DefaultAction = False
                Style = ebsEllipsisEh
                Width = 20
                DrawBackTime = edbtWhenHotEh
              end>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clNavy
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            ReadOnly = True
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
      object PaHoadon: TisPanel
        Left = 0
        Top = 115
        Width = 1096
        Height = 55
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        HeaderCaption = ' .: H'#243'a '#273#417'n'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object PaHoadon2: TPanel
          Left = 2
          Top = 18
          Width = 1092
          Height = 35
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          object TntLabel3: TLabel
            Left = 399
            Top = 11
            Width = 28
            Height = 16
            Alignment = taRightJustify
            Caption = 'Ng'#224'y'
          end
          object wwDBDateTimePicker1: TwwDBDateTimePicker
            Left = 434
            Top = 7
            Width = 93
            Height = 22
            BorderStyle = bsNone
            CalendarAttributes.Font.Charset = DEFAULT_CHARSET
            CalendarAttributes.Font.Color = clWindowText
            CalendarAttributes.Font.Height = -11
            CalendarAttributes.Font.Name = 'MS Sans Serif'
            CalendarAttributes.Font.Style = []
            Color = 15794175
            DataField = 'HOADON_NGAY'
            DataSource = DsNX
            Epoch = 1950
            ButtonEffects.Transparent = True
            ButtonEffects.Flat = True
            Frame.Enabled = True
            Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            ShowButton = True
            TabOrder = 3
          end
          object CbGia: TDbLookupComboboxEh2
            Left = 654
            Top = 7
            Width = 181
            Height = 22
            ControlLabel.Width = 54
            ControlLabel.Height = 16
            ControlLabel.Caption = 'H'#236'nh th'#7913'c'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'HinhThuc_Gia'
            DataSource = DsNX
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
              end>
            DropDownBox.ListSource = DataMain.DsHINHTHUC_GIA
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = DataMain.DsHINHTHUC_GIA
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 4
            Visible = True
          end
          object EdSOHD: TDBEditEh
            Left = 110
            Top = 7
            Width = 75
            Height = 22
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15794175
            ControlLabel.Width = 65
            ControlLabel.Height = 16
            ControlLabel.Caption = 'S'#7889' h'#243'a '#273#417'n'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'HOADON_SO'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 0
            Visible = True
          end
          object DBEditEh4: TDBEditEh
            Left = 329
            Top = 7
            Width = 65
            Height = 22
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15794175
            ControlLabel.Width = 41
            ControlLabel.Height = 16
            ControlLabel.Caption = 'K'#253' hi'#7879'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'HOADON_SERI'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 2
            Visible = True
          end
          object DBEditEh6: TDBEditEh
            Left = 222
            Top = 7
            Width = 53
            Height = 22
            BevelKind = bkFlat
            BorderStyle = bsNone
            Color = 15794175
            ControlLabel.Width = 24
            ControlLabel.Height = 16
            ControlLabel.Caption = 'M'#7851'u'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            Ctl3D = False
            DataField = 'HoaDon_Mau'
            DataSource = DsNX
            DynProps = <>
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentCtl3D = False
            ParentFont = False
            ShowHint = True
            TabOrder = 1
            Visible = True
          end
        end
      end
      object PaChitiet: TisPanel
        Left = 0
        Top = 170
        Width = 1096
        Height = 279
        Align = alClient
        BevelOuter = bvNone
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        HeaderCaption = ' :: Chi ti'#7871't'
        HeaderColor = clHighlight
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object GrDetail: TwwDBGrid2
          Left = 0
          Top = 16
          Width = 1096
          Height = 263
          DittoAttributes.ShortCutDittoField = 16397
          DittoAttributes.Options = [wwdoSkipReadOnlyFields]
          ControlType.Strings = (
            'B1;CheckBox;True;False'
            'LK_TENTHUE;CustomEdit;CbLOAITHUE;F'
            'LK_TenThue;CustomEdit;CbLOAITHUE;F')
          Selected.Strings = (
            'RSTT'#9'3'#9'STT'#9'F'
            'B1'#9'7'#9'In tem'#9'F'
            'MAVT'#9'14'#9'M'#227' h'#224'ng'#9'F'
            'SOLUONG'#9'10'#9'SKU'#9'F'#9'S'#7889' l'#432#7907'ng'
            'DONGIA'#9'10'#9'SKU'#9'F'#9#272#417'n gi'#225
            'LK_TENTHUE'#9'25'#9'Lo'#7841'i'#9'T'#9'Thu'#7871' VAT'
            'GHICHU'#9'40'#9'Ghi ch'#250#9'F')
          MemoAttributes = [mSizeable, mWordWrap, mGridShow]
          IniAttributes.Delimiter = ';;'
          TitleColor = 13360356
          FixedCols = 1
          ShowHorzScrollBar = True
          EditControlOptions = [ecoCheckboxSingleClick, ecoSearchOwnerForm]
          Align = alClient
          DataSource = DsCT
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
          Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgWordWrap, dgShowCellHint]
          ParentFont = False
          PopupMenu = PopDetail
          TabOrder = 1
          TitleAlignment = taCenter
          TitleFont.Charset = ANSI_CHARSET
          TitleFont.Color = 8404992
          TitleFont.Height = -11
          TitleFont.Name = 'Tahoma'
          TitleFont.Style = [fsBold]
          TitleLines = 2
          TitleButtons = False
          UseTFields = False
          OnUpdateFooter = GrDetailUpdateFooter
          FooterColor = 13360356
          FooterCellColor = 13360356
          PadColumnStyle = pcsPadHeader
        end
        object CbLOAITHUE: TwwDBLookupCombo
          Left = 444
          Top = 120
          Width = 117
          Height = 22
          Ctl3D = False
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'TENLT'#9'0'#9'TENLT'#9'F')
          DataField = 'LOAITHUE'
          DataSource = DsCT
          LookupTable = DataMain.QrDMLOAITHUE
          LookupField = 'MALT'
          Options = [loColLines]
          Style = csDropDownList
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
          OnNotInList = CbMAKHONotInList
        end
      end
      object PaMaster: TPanel
        Left = 0
        Top = 0
        Width = 1096
        Height = 115
        Align = alTop
        BevelInner = bvRaised
        BevelOuter = bvLowered
        Color = 16119285
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 3
        TabStop = True
        object DBText2: TDBText
          Left = 6
          Top = 4
          Width = 21
          Height = 17
          DataField = 'XOA'
          DataSource = DsNX
          Font.Charset = ANSI_CHARSET
          Font.Color = clRed
          Font.Height = -16
          Font.Name = 'Arial'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label1: TLabel
          Left = 77
          Top = 14
          Width = 28
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ng'#224'y'
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 112
          Top = 10
          Width = 165
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'NGAY'
          DataSource = DsNX
          Epoch = 1950
          ButtonEffects.Transparent = True
          ButtonEffects.Flat = True
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 0
        end
        object CbLyDo: TDbLookupComboboxEh2
          Left = 656
          Top = 34
          Width = 181
          Height = 22
          ControlLabel.Width = 30
          ControlLabel.Height = 16
          ControlLabel.Caption = 'L'#253' do'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'LYDO'
          DataSource = DsNX
          DropDownBox.Columns = <
            item
              FieldName = 'DGIAI'
            end>
          DropDownBox.ListSource = DataMain.DsPTXUATTRA
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MA'
          ListField = 'DGIAI'
          ListSource = DataMain.DsPTXUATTRA
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 5
          Visible = True
        end
        object CbKhoHang: TDbLookupComboboxEh2
          Left = 112
          Top = 34
          Width = 338
          Height = 22
          ControlLabel.Width = 53
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Kho h'#224'ng'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MAKHO'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MAKHO'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMKHO
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MAKHO'
          ListField = 'TENKHO'
          ListSource = DataMain.DsDMKHO
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 3
          Visible = True
          OnCloseUp = CbKhoHangCloseUp
          OnDropDown = CbKhoHangDropDown
        end
        object EdMaKho: TDBEditEh
          Left = 452
          Top = 34
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MAKHO'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 4
          Visible = True
        end
        object CbbNhaCungCap: TDbLookupComboboxEh2
          Left = 112
          Top = 58
          Width = 338
          Height = 22
          ControlLabel.Width = 77
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Nh'#224' cung c'#7845'p'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AlwaysShowBorder = True
          AutoSize = False
          BevelKind = bkFlat
          Ctl3D = False
          ParentCtl3D = False
          BorderStyle = bsNone
          DynProps = <>
          DataField = 'MADT'
          DataSource = DsNX
          DropDownBox.AutoFitColWidths = False
          DropDownBox.Columns = <
            item
              FieldName = 'TENDT'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'T'#234'n'
              Width = 336
            end
            item
              FieldName = 'MADT'
              Font.Charset = DEFAULT_CHARSET
              Font.Color = clWindowText
              Font.Height = -12
              Font.Name = 'Tahoma'
              Font.Style = []
              SpecCell.Font.Charset = DEFAULT_CHARSET
              SpecCell.Font.Color = clWindowText
              SpecCell.Font.Height = -12
              SpecCell.Font.Name = 'Tahoma'
              SpecCell.Font.Style = []
              Title.Alignment = taCenter
              Title.Caption = 'M'#227
              Width = 60
            end>
          DropDownBox.ListSource = DataMain.DsDMNCC
          DropDownBox.ListSourceAutoFilter = True
          DropDownBox.ListSourceAutoFilterType = lsftContainsEh
          DropDownBox.ListSourceAutoFilterAllColumns = True
          DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
          DropDownBox.AutoDrop = True
          DropDownBox.Rows = 15
          DropDownBox.Sizable = True
          DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
          DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
          DropDownBox.SpecRow.Font.Color = clWindowText
          DropDownBox.SpecRow.Font.Height = -12
          DropDownBox.SpecRow.Font.Name = 'Tahoma'
          DropDownBox.SpecRow.Font.Style = []
          DropDownBox.Width = 417
          EmptyDataInfo.Color = clInfoBk
          EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
          EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
          EmptyDataInfo.Font.Color = clSilver
          EmptyDataInfo.Font.Height = -13
          EmptyDataInfo.Font.Name = 'Tahoma'
          EmptyDataInfo.Font.Style = [fsItalic]
          EmptyDataInfo.ParentFont = False
          EmptyDataInfo.Alignment = taLeftJustify
          EditButton.DefaultAction = True
          EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
          EditButton.Style = ebsAltDropDownEh
          EditButton.Width = 20
          EditButton.DrawBackTime = edbtWhenHotEh
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          KeyField = 'MADT'
          ListField = 'TENDT'
          ListSource = DataMain.DsDMNCC
          ParentFont = False
          ShowHint = True
          Style = csDropDownEh
          TabOrder = 6
          Visible = True
        end
        object EdMADT: TDBEditEh
          Left = 452
          Top = 58
          Width = 77
          Height = 22
          TabStop = False
          Alignment = taLeftJustify
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = clBtnFace
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'MADT'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <
            item
              Action = CmdXemCongNo
              DefaultAction = False
              Style = ebsEllipsisEh
              Width = 20
              DrawBackTime = edbtWhenHotEh
            end>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Flat = True
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 7
          Visible = True
        end
        object EdSoPhieu: TDBEditEh
          Left = 344
          Top = 10
          Width = 185
          Height = 22
          TabStop = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Color = 15794175
          ControlLabel.Width = 50
          ControlLabel.Height = 16
          ControlLabel.Caption = 'S'#7889' phi'#7871'u'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'SCT'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          ReadOnly = True
          ShowHint = True
          TabOrder = 1
          Visible = True
        end
        object EdPhieuGiaoHangSo: TDBEditEh
          Left = 656
          Top = 10
          Width = 181
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 108
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Phi'#7871'u giao h'#224'ng s'#7889
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'PHIEUGIAOHANG'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 2
          Visible = True
        end
        object DBEditEh1: TDBEditEh
          Left = 656
          Top = 58
          Width = 181
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 61
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#432#7901'i giao'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'NguoiGiao'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 8
          Visible = True
        end
        object DBEditEh2: TDBEditEh
          Left = 656
          Top = 82
          Width = 181
          Height = 22
          BevelKind = bkFlat
          BorderStyle = bsNone
          ControlLabel.Width = 65
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Ng'#432#7901'i nh'#7853'n'
          ControlLabel.Font.Charset = DEFAULT_CHARSET
          ControlLabel.Font.Color = clWindowText
          ControlLabel.Font.Height = -13
          ControlLabel.Font.Name = 'Tahoma'
          ControlLabel.Font.Style = []
          ControlLabel.ParentFont = False
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          Ctl3D = False
          DataField = 'NguoiNhan'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentCtl3D = False
          ParentFont = False
          ShowHint = True
          TabOrder = 10
          Visible = True
        end
        object DBMemoEh1: TDBMemoEh
          Left = 112
          Top = 82
          Width = 417
          Height = 22
          ControlLabel.Width = 49
          ControlLabel.Height = 16
          ControlLabel.Caption = 'Di'#7877'n gi'#7843'i'
          ControlLabel.Visible = True
          ControlLabelLocation.Spacing = 5
          ControlLabelLocation.Position = lpLeftCenterEh
          AutoSize = False
          BevelKind = bkFlat
          BorderStyle = bsNone
          Ctl3D = False
          DataField = 'GhiChu'
          DataSource = DsNX
          DynProps = <>
          EditButtons = <>
          ParentCtl3D = False
          ShowHint = True
          TabOrder = 9
          Visible = True
          WantReturns = True
        end
      end
    end
  end
  inline frNavi: TfrNavi
    Left = 962
    Top = 39
    Width = 141
    Height = 21
    Anchors = [akTop, akRight]
    AutoSize = True
    Color = 16119285
    ParentBackground = False
    ParentColor = False
    TabOrder = 2
    ExplicitLeft = 962
    ExplicitTop = 39
  end
  object Action: TActionList
    OnUpdate = ActionUpdate
    Left = 152
    Top = 354
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ImageIndex = 0
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In phi'#7871'u'
      ImageIndex = 4
      OnExecute = CmdPrintExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ImageIndex = 2
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 6
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdRefresh: TAction
      OnExecute = CmdRefreshExecute
    end
    object CmdSwitch: TAction
      Hint = 'Di chuy'#7875'n con tr'#7887
      ShortCut = 117
      OnExecute = CmdSwitchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a / ph'#7909'c h'#7891'i m'#7851'u tin'
      ShortCut = 119
      OnExecute = CmdDelExecute
    end
    object CmdTotal: TAction
      Hint = 'C'#7897'ng l'#7841'i h'#243'a '#273#417'n'
      ShortCut = 120
      OnExecute = CmdTotalExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReRead: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch ch'#7913'ng t'#7915
      ShortCut = 16466
      OnExecute = CmdReReadExecute
    end
    object CmdFilterCom: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c theo m'#7863't h'#224'ng...'
      OnExecute = CmdFilterComExecute
    end
    object CmdEmptyDetail: TAction
      Category = 'DETAIL'
      Caption = 'X'#243'a chi ti'#7871't ch'#7913'ng t'#7915
      OnExecute = CmdEmptyDetailExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdListRefesh: TAction
      Caption = 'T'#7843'i l'#7841'i danh m'#7909'c'
      ShortCut = 16461
      OnExecute = CmdListRefeshExecute
    end
    object CmdDmvt: TAction
      Caption = 'H'#224'ng h'#243'a'
      ImageIndex = 42
      OnExecute = CmdDmvtExecute
    end
    object CmdSapthutu: TAction
      Category = 'DETAIL'
      Caption = 'S'#7855'p l'#7841'i th'#7913' t'#7921' m'#7863't h'#224'ng'
      OnExecute = CmdSapthutuExecute
    end
    object CmdThamkhaoGia: TAction
      Category = 'DETAIL'
      Caption = 'Tham kh'#7843'o gi'#225
      OnExecute = CmdThamkhaoGiaExecute
    end
    object CmdDmncc: TAction
      Caption = 'Nh'#224' cung c'#7845'p'
      OnExecute = CmdDmnccExecute
    end
    object CmdChecked: TAction
      Caption = 'Kh'#243'a'
      OnExecute = CmdCheckedExecute
    end
    object CmdFromNhap: TAction
      Category = 'DETAIL'
      Caption = 'L'#7845'y t'#7915' phi'#7871'u nh'#7853'p'
      OnExecute = CmdFromNhapExecute
    end
    object CmdCheckton: TAction
      Category = 'DETAIL'
      Caption = 'Ki'#7875'm tra s'#7889' l'#432#7907'ng t'#7891'n kho'
      ShortCut = 16468
      OnExecute = CmdChecktonExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'Export ra Excel t'#7915' l'#432#7899'i d'#7919' li'#7879'u'
      OnExecute = CmdExportDataGridExecute
    end
    object CmdXemCongNo: TAction
      Caption = ' '
      Hint = 'Xem c'#244'ng n'#7907' nh'#224' cung c'#7845'p'
      OnExecute = CmdXemCongNoExecute
    end
    object CmdRecalc: TAction
      Caption = 'CmdRecalc'
      OnExecute = CmdRecalcExecute
    end
    object CmdThue: TAction
      Caption = ' '
      OnExecute = CmdThueExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsNX
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchStart
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'NGAY'
      'SCT'
      'SO_HDON'
      'MADT'
      'THANHTOAN')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 124
    Top = 354
  end
  object QrNX: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeOpen = QrNXBeforeOpen
    AfterInsert = QrNXAfterInsert
    BeforeEdit = QrNXBeforeEdit
    BeforePost = QrNXBeforePost
    AfterPost = QrNXAfterPost
    AfterCancel = QrNXAfterCancel
    AfterScroll = QrNXAfterScroll
    OnCalcFields = QrNXCalcFields
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LCT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 5
        Value = Null
      end
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'LOC'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'*'
      '  from'#9'CHUNGTU'
      ' where'#9'LCT = :LCT'
      '   and'#9'NGAY >= :NGAYD'
      '   and'#9'NGAY <  :NGAYC + 1'
      '   and'#9'MAKHO in (select MAKHO from DM_KHO where LOC = :LOC)')
    Left = 724
    Top = 372
    object QrNXXOA: TWideStringField
      DisplayLabel = 'H'#7911'y'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'XOA'
      Visible = False
      Size = 1
      Calculated = True
    end
    object QrNXIMG: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG'
      Visible = False
      Calculated = True
    end
    object QrNXIMG2: TIntegerField
      FieldKind = fkCalculated
      FieldName = 'IMG2'
      Visible = False
      Calculated = True
    end
    object QrNXNGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y'
      DisplayWidth = 10
      FieldName = 'NGAY'
      Visible = False
      OnValidate = QrNXNGAYValidate
    end
    object QrNXSCT: TWideStringField
      DisplayLabel = 'S'#7889' phi'#7871'u'
      DisplayWidth = 50
      FieldName = 'SCT'
      Size = 50
    end
    object QrNXMAKHO: TWideStringField
      DisplayLabel = 'M'#227' kho'
      DisplayWidth = 5
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 5
    end
    object QrNXLK_TENKHO: TWideStringField
      DisplayLabel = 'T'#234'n kho'
      FieldKind = fkLookup
      FieldName = 'LK_TENKHO'
      LookupDataSet = DataMain.QrDMKHO
      LookupKeyFields = 'MAKHO'
      LookupResultField = 'TENKHO'
      KeyFields = 'MAKHO'
      Size = 50
      Lookup = True
    end
    object QrNXMADT: TWideStringField
      DisplayLabel = 'M'#227' NCC'
      DisplayWidth = 15
      FieldName = 'MADT'
      OnChange = QrNXMADTChange
      FixedChar = True
      Size = 15
    end
    object QrNXLK_TENDT: TWideStringField
      DisplayLabel = 'T'#234'n NCC'
      DisplayWidth = 50
      FieldKind = fkLookup
      FieldName = 'LK_TENDT'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'TENDT'
      KeyFields = 'MADT'
      Size = 100
      Lookup = True
    end
    object QrNXTU_MAKHO: TWideStringField
      DisplayWidth = 2
      FieldName = 'TU_MAKHO'
      Visible = False
      FixedChar = True
      Size = 2
    end
    object QrNXCREATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrNXUPDATE_DATE: TDateTimeField
      DisplayWidth = 18
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrNXDELETE_DATE: TDateTimeField
      FieldName = 'DELETE_DATE'
      Visible = False
    end
    object QrNXCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrNXUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrNXDELETE_BY: TIntegerField
      FieldName = 'DELETE_BY'
      Visible = False
    end
    object QrNXPTTT: TWideStringField
      FieldName = 'PTTT'
      Visible = False
      FixedChar = True
    end
    object QrNXTC_SOTIEN: TFloatField
      FieldName = 'TC_SOTIEN'
      Visible = False
    end
    object QrNXHOADON_SO: TWideStringField
      DisplayLabel = 'S'#7889' ch'#7913'ng t'#7915
      FieldName = 'HOADON_SO'
      Size = 10
    end
    object QrNXHOADON_SERI: TWideStringField
      DisplayLabel = 'Seri'
      FieldName = 'HOADON_SERI'
      Size = 10
    end
    object QrNXHOADON_NGAY: TDateTimeField
      FieldName = 'HOADON_NGAY'
    end
    object QrNXLCT: TWideStringField
      FieldName = 'LCT'
    end
    object QrNXSCT2: TWideStringField
      FieldName = 'SCT2'
      Size = 50
    end
    object QrNXPHIEUGIAOHANG: TWideStringField
      FieldName = 'PHIEUGIAOHANG'
      Size = 30
    end
    object QrNXKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrNXKHOA2: TGuidField
      FieldName = 'KHOA2'
      FixedChar = True
      Size = 38
    end
    object QrNXLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrNXLYDO: TWideStringField
      FieldName = 'LYDO'
    end
    object QrNXLK_LYDO: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LYDO'
      LookupDataSet = DataMain.QrPTNHAP
      LookupKeyFields = 'MA'
      LookupResultField = 'DGIAI'
      KeyFields = 'LYDO'
      Size = 200
      Lookup = True
    end
    object QrNXCHECKED: TBooleanField
      FieldName = 'CHECKED'
      Visible = False
    end
    object QrNXNguoiGiao: TWideStringField
      FieldName = 'NguoiGiao'
      Size = 30
    end
    object QrNXNguoiNhan: TWideStringField
      FieldName = 'NguoiNhan'
      Size = 30
    end
    object QrNXTyLeCKHD: TFloatField
      FieldName = 'TyLeCKHD'
      OnChange = QrNXTyLeCKHDChange
    end
    object QrNXSoLuongHop: TFloatField
      FieldName = 'SoLuongHop'
    end
    object QrNXSoTienCKMH: TFloatField
      FieldName = 'SoTienCKMH'
    end
    object QrNXThanhTienSauCKMH: TFloatField
      FieldName = 'ThanhTienSauCKMH'
      OnChange = QrNXTyLeCKHDChange
    end
    object QrNXSoTienCKHD: TFloatField
      FieldName = 'SoTienCKHD'
      OnChange = QrNXTyLeCKHDChange
    end
    object QrNXThanhTienSauCKHD: TFloatField
      FieldName = 'ThanhTienSauCKHD'
    end
    object QrNXLoaiThue: TWideStringField
      FieldName = 'LoaiThue'
    end
    object QrNXThueSuat: TFloatField
      FieldName = 'ThueSuat'
    end
    object QrNXThanhToanChuaCL: TFloatField
      FieldName = 'ThanhToanChuaCL'
    end
    object QrNXSoTienCL: TFloatField
      FieldName = 'SoTienCL'
    end
    object QrNXThanhToanChuaThue: TFloatField
      FieldName = 'ThanhToanChuaThue'
    end
    object QrNXSoTienThue: TFloatField
      FieldName = 'SoTienThue'
    end
    object QrNXSoTienThue_5: TFloatField
      FieldName = 'SoTienThue_5'
    end
    object QrNXSoTienThue_10: TFloatField
      FieldName = 'SoTienThue_10'
    end
    object QrNXSoTienThue_Khac: TFloatField
      FieldName = 'SoTienThue_Khac'
    end
    object QrNXThanhTienSi: TFloatField
      FieldName = 'ThanhTienSi'
    end
    object QrNXThanhTienLe: TFloatField
      FieldName = 'ThanhTienLe'
    end
    object QrNXGhiChu: TWideMemoField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'GhiChu'
      BlobType = ftWideMemo
    end
    object QrNXHanThanhToan: TIntegerField
      FieldName = 'HanThanhToan'
    end
    object QrNXHinhThuc_Gia: TWideStringField
      FieldName = 'HinhThuc_Gia'
      OnChange = QrNXHINHTHUC_GIAChange
    end
    object QrNXCALC_NgayThanhToan: TDateTimeField
      FieldKind = fkCalculated
      FieldName = 'CALC_NgayThanhToan'
      Calculated = True
    end
    object QrNXLK_HanThanhToan: TIntegerField
      FieldKind = fkLookup
      FieldName = 'LK_HanThanhToan'
      LookupDataSet = DataMain.QrDMNCC
      LookupKeyFields = 'MADT'
      LookupResultField = 'HAN_TTOAN'
      KeyFields = 'MADT'
      Lookup = True
    end
    object QrNXThanhTien: TFloatField
      FieldName = 'ThanhTien'
    end
    object QrNXThanhToan: TFloatField
      FieldName = 'ThanhToan'
    end
    object QrNXSoTienThueChuaRound: TFloatField
      FieldName = 'SoTienThueChuaRound'
      OnChange = QrNXSoTienThueChuaRoundChange
    end
    object QrNXThanhToanTinhThue: TFloatField
      FieldName = 'ThanhToanTinhThue'
    end
    object QrNXSoLuong: TFloatField
      FieldName = 'SoLuong'
    end
    object QrNXHoaDon_Mau: TWideStringField
      FieldName = 'HoaDon_Mau'
    end
  end
  object QrCT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    BeforeOpen = QrCTBeforeOpen
    BeforeInsert = QrCTBeforeInsert
    AfterInsert = QrCTAfterEdit
    BeforeEdit = QrCTBeforeEdit
    AfterEdit = QrCTAfterEdit
    BeforePost = QrCTBeforePost
    AfterCancel = QrCTAfterCancel
    BeforeDelete = QrCTBeforeDelete
    AfterDelete = QrCTAfterDelete
    OnCalcFields = QrCTCalcFields
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'KHOA'
        DataType = ftGuid
        NumericScale = 255
        Precision = 255
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from CHUNGTU_CT'
      ' where KHOA= :KHOA'
      'order by STT')
    Left = 756
    Top = 372
    object QrCTRSTT: TIntegerField
      DisplayLabel = 'STT'
      DisplayWidth = 4
      FieldKind = fkCalculated
      FieldName = 'RSTT'
      Calculated = True
    end
    object QrCTSTT: TIntegerField
      FieldName = 'STT'
    end
    object QrCTKHOA: TGuidField
      FieldName = 'KHOA'
      FixedChar = True
      Size = 38
    end
    object QrCTMAVT: TWideStringField
      DisplayLabel = 'M'#227' h'#224'ng'
      DisplayWidth = 13
      FieldName = 'MAVT'
      OnChange = QrCTMAVTChange
      OnValidate = QrCTMAVTValidate
      FixedChar = True
      Size = 15
    end
    object QrCTKHOACT: TGuidField
      FieldName = 'KHOACT'
      FixedChar = True
      Size = 38
    end
    object QrCTQD1: TIntegerField
      FieldName = 'QD1'
    end
    object QrCTLK_VAT_RA: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_RA'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_RA'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTLK_VAT_VAO: TFloatField
      FieldKind = fkLookup
      FieldName = 'LK_VAT_VAO'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'VAT_VAO'
      KeyFields = 'LOAITHUE'
      Lookup = True
    end
    object QrCTB1: TBooleanField
      DisplayLabel = 'Tem'
      FieldName = 'B1'
    end
    object QrCTLOC: TWideStringField
      FieldName = 'LOC'
      Size = 5
    end
    object QrCTLK_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TINHTRANG'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TINHTRANG'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_Tenvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Tenvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'TENVT'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_Dvt: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_Dvt'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'Dvt'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTLK_DvtHop: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_DvtHop'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'DvtHop'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTSoLuongHop: TFloatField
      FieldName = 'SoLuongHop'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTSoLuongLe: TFloatField
      FieldName = 'SoLuongLe'
      OnChange = QrCTSOLUONGChange
    end
    object QrCTDonGiaThamKhao: TFloatField
      FieldName = 'DonGiaThamKhao'
    end
    object QrCTDonGiaHop: TFloatField
      FieldName = 'DonGiaHop'
      OnChange = QrCTDONGIAChange
    end
    object QrCTTyLeCKMH: TFloatField
      FieldName = 'TyLeCKMH'
      OnChange = QrCTThanhTienChange
    end
    object QrCTSoTienCKMH: TFloatField
      FieldName = 'SoTienCKMH'
      OnChange = QrCTThanhTienChange
    end
    object QrCTThanhTienSauCKMH: TFloatField
      FieldName = 'ThanhTienSauCKMH'
      OnChange = QrCTThanhTienSauCKMHChange
    end
    object QrCTTyLeCKHD: TFloatField
      FieldName = 'TyLeCKHD'
      OnChange = QrCTThanhTienSauCKMHChange
    end
    object QrCTSoTienCKHD: TFloatField
      FieldName = 'SoTienCKHD'
    end
    object QrCTThanhTienSauCKHD: TFloatField
      FieldName = 'ThanhTienSauCKHD'
      OnChange = QrCTThanhTienSauCKHDChange
    end
    object QrCTThanhToanChuaCL: TFloatField
      FieldName = 'ThanhToanChuaCL'
    end
    object QrCTSoTienCL: TFloatField
      FieldName = 'SoTienCL'
      OnChange = QrCTThanhTienChange
    end
    object QrCTThanhToanChuaThue: TFloatField
      FieldName = 'ThanhToanChuaThue'
    end
    object QrCTThueSuat: TFloatField
      FieldName = 'ThueSuat'
      OnChange = QrCTThanhTienSauCKHDChange
    end
    object QrCTSoTienThue: TFloatField
      FieldName = 'SoTienThue'
    end
    object QrCTSoTienThue_5: TFloatField
      FieldName = 'SoTienThue_5'
    end
    object QrCTSoTienThue_10: TFloatField
      FieldName = 'SoTienThue_10'
    end
    object QrCTSoTienThue_Khac: TFloatField
      FieldName = 'SoTienThue_Khac'
    end
    object QrCTThanhToan: TFloatField
      FieldName = 'ThanhToan'
      OnChange = QrCTThanhToanChange
    end
    object QrCTDonGiaSi: TFloatField
      FieldName = 'DonGiaSi'
    end
    object QrCTThanhTienSi: TFloatField
      FieldName = 'ThanhTienSi'
    end
    object QrCTDonGiaLe: TFloatField
      FieldName = 'DonGiaLe'
    end
    object QrCTThanhTienLe: TFloatField
      FieldName = 'ThanhTienLe'
    end
    object QrCTTyLeLai: TFloatField
      FieldName = 'TyLeLai'
    end
    object QrCTTyLeLaiSi: TFloatField
      FieldName = 'TyLeLaiSi'
    end
    object QrCTHanSuDung: TDateTimeField
      FieldName = 'HanSuDung'
    end
    object QrCTMaBo: TWideStringField
      FieldName = 'MaBo'
      Size = 15
    end
    object QrCTLK_LOAITHUE: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_LOAITHUE'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'LOAITHUE'
      KeyFields = 'MAVT'
      Lookup = True
    end
    object QrCTCALC_DonGiaThamKhaoHop: TFloatField
      FieldKind = fkCalculated
      FieldName = 'CALC_DonGiaThamKhaoHop'
      Calculated = True
    end
    object QrCTGiaVon: TFloatField
      FieldName = 'GiaVon'
    end
    object QrCTDonGia: TFloatField
      FieldName = 'DonGia'
      OnChange = QrCTDONGIAChange
    end
    object QrCTSoLuong: TFloatField
      FieldName = 'SoLuong'
      OnChange = QrCTSOLUONGChange
      OnValidate = QrCTSOLUONGValidate
    end
    object QrCTThanhTien: TFloatField
      DisplayLabel = 'Th'#224'nh ti'#7873'n'
      FieldName = 'ThanhTien'
      OnChange = QrCTThanhTienChange
    end
    object QrCTLoaiThue: TWideStringField
      FieldName = 'LoaiThue'
      OnChange = QrCTLOAITHUEChange
    end
    object QrCTLK_TenThue2: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenThue'
      LookupDataSet = DataMain.QrDMLOAITHUE
      LookupKeyFields = 'MALT'
      LookupResultField = 'TENLT'
      KeyFields = 'LoaiThue'
      Lookup = True
    end
    object QrCTGhiChu: TWideStringField
      DisplayLabel = 'Di'#7877'n gi'#7843'i'
      FieldName = 'GhiChu'
      Size = 200
    end
    object QrCTDonGiaCKMH: TFloatField
      FieldName = 'DonGiaCKMH'
      OnChange = QrCTThanhTienChange
    end
    object QrCTDonGiaCKHD: TFloatField
      FieldName = 'DonGiaCKHD'
    end
    object QrCTDonGiaChuaThue: TFloatField
      FieldName = 'DonGiaChuaThue'
    end
    object QrCTSoTienThueChuaRound: TFloatField
      FieldName = 'SoTienThueChuaRound'
    end
    object QrCTThanhToanTinhThue: TFloatField
      FieldName = 'ThanhToanTinhThue'
      OnChange = QrCTThanhToanTinhThueChange
    end
    object QrCTDonGiaTinhThue: TFloatField
      FieldName = 'DonGiaTinhThue'
    end
    object QrCTDonGiaSiChuaThue: TFloatField
      FieldName = 'DonGiaSiChuaThue'
    end
    object QrCTLK_MaNoiBo: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_MaNoiBo'
      LookupDataSet = DataMain.QrDMVT
      LookupKeyFields = 'MAVT'
      LookupResultField = 'MaNoiBo'
      KeyFields = 'MAVT'
      Size = 200
      Lookup = True
    end
    object QrCTDonGiaLeChuaThue: TFloatField
      FieldName = 'DonGiaLeChuaThue'
    end
  end
  object DsNX: TDataSource
    DataSet = QrNX
    Left = 724
    Top = 424
  end
  object DsCT: TDataSource
    DataSet = QrCT
    Left = 756
    Top = 424
  end
  object PopMaster: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopMasterPopup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 290
    Top = 312
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object CmdClearFilter1: TMenuItem
      Action = CmdClearFilter
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object Chntheomthng1: TMenuItem
      Action = CmdFilterCom
      ImageIndex = 17
    end
    object N6: TMenuItem
      Caption = '-'
    end
    object ItemObsolete: TMenuItem
      Caption = 'Hi'#7875'n th'#7883' c'#225'c phi'#7871'u '#273#227' x'#243'a'
      OnClick = ItemObsoleteClick
    end
    object N9: TMenuItem
      Caption = '-'
    end
    object ExportraExceltlidliu2: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 354
    Top = 312
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 382
    Top = 428
    object Lytphiunhp1: TMenuItem
      Action = CmdFromNhap
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object ExportraExceltlidliu1: TMenuItem
      Tag = 1
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
    object N8: TMenuItem
      Caption = '-'
    end
    object Xachitit1: TMenuItem
      Action = CmdEmptyDetail
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object Splithtmthng1: TMenuItem
      Action = CmdSapthutu
    end
    object N5: TMenuItem
      Caption = '-'
    end
    object hamkhoginhp1: TMenuItem
      Action = CmdThamkhaoGia
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object Kimtraslngtnkho1: TMenuItem
      Action = CmdCheckton
    end
  end
  object CHECK_NCC: TADOCommand
    CommandText = 
      'select 1 from DM_VT where isnull(KHAC_NCC, 0) = 1 or (MAVT=:MAVT' +
      ' and MADT=:MADT)'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 548
    Top = 340
  end
  object vlTotal1: TisTotal
    MasterDataSet = QrNX
    DetailDataSet = QrCT
    MasterFields.Strings = (
      'SoLuong'
      'ThanhTien'
      'SoTienCKMH'
      'ThanhTienSauCKMH'
      'ThanhTienSauCKHD'
      'ThanhToanTinhThue'
      'ThanhToanChuaCL'
      'SoTienCL'
      'ThanhToanChuaThue'
      'SoTienThueChuaRound'
      'SoTienThue_5'
      'SoTienThue_10'
      'SoTienThue_Khac'
      'ThanhTienSi'
      'ThanhTienLe'
      'ThanhToan')
    DetailFields.Strings = (
      'SoLuong'
      'ThanhTien'
      'SoTienCKMH'
      'ThanhTienSauCKMH'
      'ThanhTienSauCKHD'
      'ThanhToanTinhThue'
      'ThanhToanChuaCL'
      'SoTienCL'
      'ThanhToanChuaThue'
      'SoTienThueChuaRound'
      'SoTienThue_5'
      'SoTienThue_10'
      'SoTienThue_Khac'
      'ThanhTienSi'
      'ThanhTienLe'
      'ThanhToan')
    Left = 418
    Top = 312
  end
  object PopDanhmuc: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 176
    Top = 408
    object MenuItem3: TMenuItem
      Tag = 1
      Action = CmdDmvt
    end
    object MenuItem4: TMenuItem
      Caption = '-'
    end
    object MenuItem5: TMenuItem
      Tag = 2
      Action = CmdDmncc
      AutoHotkeys = maManual
      AutoLineReduction = maManual
    end
  end
  object spCHUNGTU_Select_Full: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    ProcedureName = 'spCHUNGTU_Select_Full;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@KHOA'
        Attributes = [paNullable]
        DataType = ftGuid
        Value = Null
      end>
    Left = 614
    Top = 364
  end
  object PopIn: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 224
    Top = 360
    object heogibn1: TMenuItem
      Caption = 'In phi'#7871'u'
      OnClick = CmdPrintExecute
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object Khngngi1: TMenuItem
      Tag = 1
      Caption = 'Kh'#244'ng '#273#417'n gi'#225
      OnClick = CmdPrintExecute
    end
  end
end
