object DataExcel: TDataExcel
  OldCreateOrder = False
  Height = 293
  Width = 377
  object SP_PROCESS: TADOCommand
    CommandTimeout = 0
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <>
    Left = 216
    Top = 52
  end
  object SP_EXPORT: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    Parameters = <>
    Left = 144
    Top = 104
  end
  object QrIMPORT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <>
    Left = 220
    Top = 100
  end
  object QrCheck: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'Select 1 from DM_VT where MAVT = :MAVT')
    Left = 220
    Top = 156
  end
  object QrImport2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltBatchOptimistic
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from DUMMY_IMPORT'
      'where 1 = 0')
    Left = 140
    Top = 156
  end
  object XLSReadWrite: TXLSReadWriteII4
    Version = xvExcelUnknown
    Sheets = <
      item
        Name = 'Sheet1'
        DefaultColWidth = 8
        DefaultRowHeight = 255
        PrintSettings.Copies = 0
        PrintSettings.FooterMargin = 0.500000000000000000
        PrintSettings.FooterMarginCm = 1.270000000000000000
        PrintSettings.HeaderMargin = 0.500000000000000000
        PrintSettings.HeaderMarginCm = 1.270000000000000000
        PrintSettings.MarginBottom = 1.000000000000000000
        PrintSettings.MarginLeft = 0.750000000000000000
        PrintSettings.MarginRight = 0.750000000000000000
        PrintSettings.MarginTop = 1.000000000000000000
        PrintSettings.MarginBottomCm = 2.540000000000000000
        PrintSettings.MarginLeftCm = 1.905000000000000000
        PrintSettings.MarginRightCm = 1.905000000000000000
        PrintSettings.MarginTopCm = 2.540000000000000000
        PrintSettings.Options = [psoPortrait]
        PrintSettings.PaperSize = psLegal
        PrintSettings.ScalingFactor = 100
        PrintSettings.StartingPage = 1
        PrintSettings.HorizPagebreaks = <>
        PrintSettings.VertPagebreaks = <>
        PrintSettings.Resolution = 600
        PrintSettings.FitWidth = 1
        PrintSettings.FitHeight = 1
        Options = [soGridlines, soRowColHeadings, soShowZeros]
        WorkspaceOptions = [woShowAutoBreaks, woRowSumsBelow, woColSumsRight, woOutlineSymbols]
        SheetProtection = [spEditObjects, spEditScenarios, spEditCellFormatting, spEditColumnFormatting, spEditRowFormatting, spInsertColumns, spInsertRows, spInsertHyperlinks, spDeleteColumns, spDeleteRows, spSelectLockedCells, spSortCellRange, spEditAutoFileters, spEditPivotTables, spSelectUnlockedCells]
        Zoom = 0
        ZoomPreview = 0
        RecalcFormulas = True
        Hidden = hsVisible
        Validations = <>
        DrawingObjects.Texts = <>
        DrawingObjects.Notes = <>
        DrawingObjects.Basics = <>
        DrawingObjects.AutoShapes = <>
        DrawingObjects.Pictures = <>
        ControlsObjects.ListBoxes = <>
        ControlsObjects.ComboBoxes = <>
        ControlsObjects.Buttons = <>
        ControlsObjects.CheckBoxes = <>
        ControlsObjects.RadioButtons = <>
        Hyperlinks = <>
        ConditionalFormats = <>
      end>
    Workbook.Left = 100
    Workbook.Top = 100
    Workbook.Width = 10000
    Workbook.Height = 7000
    Workbook.SelectedTab = 0
    Workbook.Options = [woHScroll, woVScroll, woTabs]
    OptionsDialog.SaveExtLinkVal = False
    OptionsDialog.CalcCount = 100
    OptionsDialog.CalcMode = cmAutomatic
    OptionsDialog.Delta = 0.001000000000000000
    OptionsDialog.ShowObjects = soShowAll
    OptionsDialog.Iteration = False
    OptionsDialog.PrecisionAsDisplayed = True
    OptionsDialog.R1C1Mode = False
    OptionsDialog.RecalcBeforeSave = False
    OptionsDialog.Uncalced = False
    OptionsDialog.SaveRecalc = True
    BookProtected = False
    Backup = False
    RefreshAll = False
    StrTRUE = 'TRUE'
    StrFALSE = 'FALSE'
    ShowFormulas = False
    IsMac = False
    PreserveMacros = True
    ComponentVersion = '4.00.56a'
    MSOPictures = <>
    RecomendReadOnly = False
    TempFileMode = tfmOnDisk
    Left = 300
    Top = 36
  end
  object ExToExcel: TSMExportToExcel
    AnimatedStatus = False
    DataFormats.DateOrder = doDMY
    DataFormats.DateSeparator = '/'
    DataFormats.TimeSeparator = ':'
    DataFormats.FourDigitYear = True
    DataFormats.LeadingZerosInDate = True
    DataFormats.ThousandSeparator = ','
    DataFormats.DecimalSeparator = '.'
    DataFormats.CurrencyString = '$'
    DataFormats.BooleanTrue = 'True'
    DataFormats.BooleanFalse = 'False'
    DataFormats.UseRegionalSettings = True
    KeyGenerator = 'SMExport 4.99'
    Options = [soWaitCursor, soDisableControls, soUseFieldNameAsCaption, soColorsFonts]
    TitleStatus = 'Exporting...'
    ExportIfEmpty = False
    OnProgress = ExToExcelProgress
    Columns = <>
    Bands = <>
    ColumnSource = csDataSet
    FileName = 'SMExport.XLS'
    AddTitle = True
    CharacterSet = csANSI_WINDOWS
    RowsPerFile = 5000
    DetailSources = <>
    OnGetCellParams = ExToExcelGetCellParams
    AutoFitColumns = True
    ExportStyle.Style = esNormal
    ExportStyle.OddColor = clBlack
    ExportStyle.EvenColor = clBlack
    Left = 300
    Top = 100
  end
  object ExToCSV: TSMExportToText
    AnimatedStatus = False
    DataFormats.DateOrder = doDMY
    DataFormats.DateSeparator = '/'
    DataFormats.TimeSeparator = ':'
    DataFormats.FourDigitYear = True
    DataFormats.LeadingZerosInDate = True
    DataFormats.ThousandSeparator = ','
    DataFormats.DecimalSeparator = '.'
    DataFormats.CurrencyString = '$'
    DataFormats.BooleanTrue = 'True'
    DataFormats.BooleanFalse = 'False'
    DataFormats.UseRegionalSettings = False
    KeyGenerator = 'SMExport 4.99'
    Options = [soWaitCursor, soDisableControls, soUseFieldNameAsCaption]
    TitleStatus = 'Exporting...'
    OnProgress = ExToExcelProgress
    Columns = <>
    Bands = <>
    DataSet = SP_EXPORT
    ColumnSource = csDataSet
    FileName = 'C:\Program Files\CodeGear\RAD Studio\6.0\bin\SMExport.TXT'
    AddTitle = False
    CharacterSet = csANSI_WINDOWS
    DetailSources = <>
    OnGetCellParams = ExToExcelGetCellParams
    TextQualifying = tqStringOnly
    RecordSeparator = #13#10
    Fixed = False
    Left = 304
    Top = 160
  end
  object QrCheck2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'MANGANH'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'Select 1 from DM_VT where MAVT = :MAVT'
      'and MANGANH = :MANGANH')
    Left = 220
    Top = 212
  end
  object QrCheck3: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <
      item
        Name = 'MAVT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end
      item
        Name = 'MANGANH'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end
      item
        Name = 'MADT'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    SQL.Strings = (
      'Select 1 from DM_VT where MAVT = :MAVT'
      'and MANGANH = :MANGANH'
      'and MADT = :MADT')
    Left = 284
    Top = 212
  end
end
