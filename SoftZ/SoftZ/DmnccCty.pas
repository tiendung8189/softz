﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmnccCty;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Mask, ExtCtrls,
  RzPanel, Grids, Wwdbigrd, Wwdbgrid, ToolWin, wwdbedit, Wwkeycb, Buttons,
  AdvEdit, wwcheckbox, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmDmnccCty = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMCongTy: TADOQuery;
    DsDmCongTy: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    ToolButton12: TToolButton;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    QrDMCongTyPLOAI: TWideStringField;
    QrDMCongTyDCHI: TWideStringField;
    QrDMCongTyDTHOAI: TWideStringField;
    QrDMCongTyFAX: TWideStringField;
    QrDMCongTyMST: TWideStringField;
    QrDMCongTyTENTK: TWideStringField;
    QrDMCongTyMATK: TWideStringField;
    QrDMCongTySODU: TFloatField;
    QrDMCongTyGHICHU: TWideMemoField;
    QrDMCongTyCREATE_BY: TIntegerField;
    QrDMCongTyUPDATE_BY: TIntegerField;
    QrDMCongTyCREATE_DATE: TDateTimeField;
    QrDMCongTyUPDATE_DATE: TDateTimeField;
    QrDMCongTyHAN_TTOAN: TIntegerField;
    QrDMCongTyLK_PLOAI: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    EdTen: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    EdMa: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    PD2: TisPanel;
    Label1: TLabel;
    DBEdit4: TwwDBEdit;
    PD5: TisPanel;
    DBMemo1: TDBMemo;
    PD4: TisPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit6: TwwDBEdit;
    DBEdit7: TwwDBEdit;
    DBEdit8: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    QrDMCongTyLOAI: TIntegerField;
    QrDMCongTyMADT: TWideStringField;
    QrDMCongTyTENDT: TWideStringField;
    QrDMCongTyMAQG: TWideStringField;
    QrDMCongTyMATINH: TWideStringField;
    QrDMCongTyMAHUYEN: TWideStringField;
    QrQuocgia: TADOQuery;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrTinh: TADOQuery;
    QrTinhMATINH: TWideStringField;
    QrTinhMAQG: TWideStringField;
    QrTinhMA: TWideStringField;
    QrTinhTEN: TWideStringField;
    QrHuyen: TADOQuery;
    QrHuyenMAHUYEN: TWideStringField;
    QrHuyenMATINH: TWideStringField;
    QrHuyenMA: TWideStringField;
    QrHuyenTEN: TWideStringField;
    QrDMCongTySODU_NGAY: TDateTimeField;
    QrDMCongTySODU_LIMIT: TFloatField;
    TntLabel3: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    PD3: TisPanel;
    TntLabel4: TLabel;
    DBEdit10: TwwDBEdit;
    TntLabel5: TLabel;
    DBEdit11: TwwDBEdit;
    Panel1: TPanel;
    PaVIP: TPanel;
    PaNV: TPanel;
    QrDMCongTyMANV: TWideStringField;
    QrDMCongTyLOAI_VIP: TWideStringField;
    PaChuquan: TPanel;
    QrDMCongTyMADT2: TWideStringField;
    QrDMCongTyLK_TENDT2: TWideStringField;
    QrDMCongTyEMAIL: TWideStringField;
    Label17: TLabel;
    wwDBEdit1: TwwDBEdit;
    CmdRefesh: TAction;
    QrDMCongTyNGUNG_LIENLAC: TBooleanField;
    N3: TMenuItem;
    ItemLienlac: TMenuItem;
    PaList: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    EdSearch: TAdvEdit;
    CHECK_DTHOAI: TADOCommand;
    SpeedButton2: TSpeedButton;
    CmdDmtk: TAction;
    QrDMCongTyLK_TENTK: TWideStringField;
    QrDMCongTyLK_NGANHANG: TWideStringField;
    QrDMCongTyLK_CHINHANH: TWideStringField;
    CmdExportDataGrid: TAction;
    N1: TMenuItem;
    N4: TMenuItem;
    wwCheckBox1: TwwCheckBox;
    QrDMCongTyIdx: TAutoIncField;
    cbbMAQG: TDbLookupComboboxEh2;
    EdMAQG: TDBEditEh;
    EdMATINH: TDBEditEh;
    cbbTINH: TDbLookupComboboxEh2;
    cbbMAHUYEN: TDbLookupComboboxEh2;
    EdMAHUYEN: TDBEditEh;
    QrDMCongTyLK_MATINHNGAN: TWideStringField;
    QrDMCongTyLK_MAHUYENNGAN: TWideStringField;
    CbLOAI_VIP: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    EdMANV: TDBEditEh;
    CbMANV: TDbLookupComboboxEh2;
    CbMADT: TDbLookupComboboxEh2;
    EdMADT2: TDBEditEh;
    DsQuocgia: TDataSource;
    DsTinh: TDataSource;
    DsHuyen: TDataSource;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMCongTyBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDMCongTyBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMCongTyBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbPhanLoaiNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMCongTyAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdContactExecute(Sender: TObject);
    procedure QrDMCongTyAfterInsert(DataSet: TDataSet);
    procedure QrDMCongTyMAQGChange(Sender: TField);
    procedure QrDMCongTyMATINHChange(Sender: TField);
    procedure CbTentinhBeforeDropDown(Sender: TObject);
    procedure CbTenHuyenBeforeDropDown(Sender: TObject);
    procedure QrDMCongTyMADTChange(Sender: TField);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ItemLienlacClick(Sender: TObject);
    procedure QrDMCongTyDTHOAIValidate(Sender: TField);
    procedure CmdDmtkExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure cbbTINHDropDown(Sender: TObject);
    procedure cbbMAHUYENDropDown(Sender: TObject);
    procedure QrDMCongTyMAHUYENChange(Sender: TField);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mClose, mObsolete: Boolean;
    mCodeLength, mFilter: Integer;
    mSQL, mPrefix, mSearch: String;
  public
  	function Execute(r: WORD; pClose: Boolean = True): Boolean;
  end;

var

  FrmDmnccCty: TFrmDmnccCty;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, isLib, Lienhe, isCommon, DmTK, OfficeData, HrData;

{$R *.DFM}

const
	FORM_CODE = 'DM_NCC';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmnccCty.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsDmCongTy.AutoEdit := mCanEdit;

    mRet := False;
    mClose := pClose;
    ShowModal;
    Result := mRet;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    mTrigger := False;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMCongTy, FORM_CODE, Filter);

    PaDetail.VertScrollBar.Position := 0;

    PD2.Collapsed := RegReadBool(Name, 'PD2');
    PD3.Collapsed := RegReadBool('PD3');
  	PD4.Collapsed := RegRead('PD4', True);
    PD5.Collapsed := RegRead('PD5', True);

    PD2.Visible := False;
    PD3.Visible := False;
    PD4.Visible := False;

    mAutoCode := FlexConfigBool(FORM_CODE, 'AutoCode');
    fixCode := FlexConfigBool(FORM_CODE, 'Fixed');
    mCodeLength := FlexConfigInteger(FORM_CODE, 'CodeLength', 0);
    mObsolete     := RegReadBool('Obsolete');
    mPrefix := FlexConfigString(FORM_CODE, 'Prefix', '');
    mFilter := 9;
    mSearch := '';
    mSQL := QrDMCongTy.SQL.Text;

//    if mCodeLength > 0 then
//        with QrDanhmuc.FieldByName('MADT') do
//        begin
//            DisplayWidth := mCodeLength;
//            Size := mCodeLength;
//        end
//	else
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.FormShow(Sender: TObject);
var
    bVIP, bNV: Boolean;
begin
    EdMa.ReadOnly := mAutoCode;
    PD2.Enabled := GetRights('SZ_DM_KH_NCC_CONGNO', False) <> R_DENY;
    bVIP := GetFuncState('SZ_DM_LOAIVIP_KH');
    bNV := GetFuncState('SZ_PUB_DM_NHANVIEN');


    if not bNV then
    begin
        PaNV.Visible := False;
        PD1.Height := PD1.Height - PaNV.Height;
    end;

    PaVIP.Visible := False;
    PD1.Height := PD1.Height - PaVIP.Height;

    PaChuquan.Visible := False;
    PD1.Height := PD1.Height - PaChuquan.Height;


    with DataMain do
        OpenDataSets([QrLOAI_KH_NCC, QrLOAI_VIP_SI, QrDM_KH_NCC, QrDMTK, QrTINH, QrHUYEN]);

	OpenDataSets([QrQuocgia, QrTinh, QrHuyen, HrDataMain.QrDMNV_DANGLAMVIEC]);
    SetDisplayFormat(QrDMCongTy, sysCurFmt);

    CmdRefesh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.ItemLienlacClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDMCongTy, QrQuocgia, QrTinh, QrHuyen]);
    finally
    end;

	//Save state panel
    RegWrite(Name, ['PD2', 'PD3', 'PD4', 'PD5', 'Obsolete'],
        [PD2.Collapsed, PD3.Collapsed, PD4.Collapsed, PD5.Collapsed, mObsolete]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMCongTy, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdNewExecute(Sender: TObject);
begin
    QrDMCongTy.Append;
	if not mAutoCode then
        EdMa.SetFocus
    else
        EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdSaveExecute(Sender: TObject);
begin
    QrDMCongTy.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdCancelExecute(Sender: TObject);
begin
    QrDMCongTy.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdDelExecute(Sender: TObject);
begin
    QrDMCongTy.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdDmtkExecute(Sender: TObject);
var
    r: WORD;
    pMatk, pMadt: String;
begin
    with QrDMCongTy do
    begin
        pMadt := FieldByName('MADT').AsString;

        Application.CreateForm(TFrmDmTK, FrmDmTK);
        if FrmDmTK.Execute(R_FULL, pMatk, 'DT', pMadt, '', False) then
        begin
            DataMain.QrDMTK.Requery();

            Edit;
            FieldByName('MATK').AsString := pMatk;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdRefeshExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;

    mSearch := DataMain.StripToneMark(EdSearch.Text);
	with QrDMCongTy do
    begin
    	Close;
        if mSearch <> '' then
            sSQL := sSQL + ' and (dbo.fnStripToneMark(TENDT) like ''%'+ mSearch + '%'' or dbo.fnStripToneMark(DTHOAI) like ''%' + mSearch + '%'')';

        if not mObsolete then
            sSQL := sSQL + ' and (isnull(NGUNG_LIENLAC, 0) = 0)';

        SQL.Text := sSQL;
        SQL.Add(' order by 	TENDT');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDmCongTy)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty: Boolean;
begin
    exActionUpdate(ActionList, QrDMCongTy, Filter, mCanEdit);

	with QrDMCongTy do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

    CmdContact.Enabled := bBrowse and (not bEmpty);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDmCongTy);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.PopupMenu1Popup(Sender: TObject);
begin
    ItemLienlac.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyBeforePost(DataSet: TDataSet);
begin
	with QrDMCongTy do
    begin
        if State in [dsInsert] then
        begin
            if not mAutoCode then
            begin
                if BlankConfirm(QrDMCongTy, ['MADT']) then
                    Abort;
            end
            else
                FieldByName('MADT').AsString :=
                    DataMain.AllocMADT(mCodeLength, 1, mPrefix);
        end;

        if fixCode then
            if LengthConfirm(QrDMCongTy, ['MADT']) then
                Abort;

        if BlankConfirm(QrDMCongTy, ['TENDT']) then
        	Abort;

	    SetNull(QrDMCongTy, ['MAQG', 'MATINH', 'MAHUYEN', 'DTHOAI']);
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DTHOAI_EXISTS = 'Số điện thoại đã được sử dụng.';
procedure TFrmDmnccCty.QrDMCongTyDTHOAIValidate(Sender: TField);
var
	s: String;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
    if s = '' then
        Exit;

    with CHECK_DTHOAI do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrDMCongTy.FieldByName('MADT').AsString;
            if Execute.RecordCount > 0 then
            begin
                ErrMsg(RS_DTHOAI_EXISTS);
                Abort;
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMCongTy, Filter);
end;

procedure TFrmDmnccCty.cbbMAHUYENDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrTinh.FieldByName('MATINH').AsString;
    QrHuyen.Filter := 'MATINH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.cbbTINHDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrQuocgia.FieldByName('MA').AsString;
    QrTinh.Filter := 'MAQG=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CbPhanLoaiNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CmdContactExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmLienhe, FrmLienhe);
    with QrDMCongTy do
        FrmLienhe.Execute(
            mCanEdit,
        	FieldByName('Idx').AsInteger,
            FieldByName('MADT').AsString,
            FieldByName('TENDT').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyAfterInsert(DataSet: TDataSet);
begin
    with QrDMCongTy do
    begin
        FieldByName('LOAI').AsInteger := 1;
        FieldByName('NGUNG_LIENLAC').AsBoolean := False;
        FieldByName('MAQG').AsString := '084';
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyMADTChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

procedure TFrmDmnccCty.QrDMCongTyMAHUYENChange(Sender: TField);
begin
    with QrDMCongTy do
    begin
        EdMAHUYEN.Text := EdMAHUYEN.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyMAQGChange(Sender: TField);
begin
    with QrDMCongTy do
    begin
        FieldByName('MATINH').Clear;
        FieldByName('MAHUYEN').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.QrDMCongTyMATINHChange(Sender: TField);
begin
    with QrDMCongTy do
    begin
        FieldByName('MAHUYEN').Clear;
        EdMATINH.Text := EdMATINH.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CbTentinhBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDMCongTy.FieldByName('MAQG').AsString;
    with QrTinh do
    begin
        Filter := Format('MAQG=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmnccCty.CbTenHuyenBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDMCongTy.FieldByName('MATINH').AsString;
    with QrHuyen do
    begin
        Filter := Format('MATINH=''%s''', [s]);
    end;
end;

end.
