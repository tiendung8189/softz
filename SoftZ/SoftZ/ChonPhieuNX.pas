﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieuNX;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit,
  AdvEdit, DBAdvEd, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2, Variants,
  kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmChonPhieuNX = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNX: TDataSource;
    QrPHIEUNX: TADOQuery;
    QrPHIEUNXNGAY: TDateTimeField;
    QrPHIEUNXSCT: TWideStringField;
    QrPHIEUNXMADT: TWideStringField;
    QrPHIEUNXMAKHO: TWideStringField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrPHIEUNXTENKHO: TWideStringField;
    QrPHIEUNXTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNXHOADON_SO: TWideStringField;
    QrPHIEUNXHOADON_NGAY: TDateTimeField;
    QrPHIEUNXPHIEUGIAOHANG: TWideStringField;
    QrPHIEUNXKHOA: TGuidField;
    QrPHIEUNXNguoiGiao: TWideStringField;
    QrPHIEUNXNguoiNhan: TWideStringField;
    QrPHIEUNXThueSuat: TFloatField;
    QrPHIEUNXHanThanhToan: TIntegerField;
    QrPHIEUNXThanhToan: TFloatField;
    QrPHIEUNXGhiChu: TWideMemoField;
    QrPHIEUNXSoLuong: TFloatField;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    DsDMKHO: TDataSource;
    DsDM_KH_NCC: TDataSource;
    DsDummyEh: TDataSource;
    TbDummyEh: TMemTableEh;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
    procedure QrPHIEUNXBeforeOpen(DataSet: TDataSet);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mMaDV, mKHO, mSQL, mLCT: String;
    mTungay, mDenngay: TDateTime;
    mLoai: Integer; //0: Khach hang; 1: NCC
  public
  	function Execute(pFix: Boolean; pMaDV, pKho: String;
    	pLCT: String = 'NMUA'; pLoai: Integer = 1): TGUID;
  end;

var
  FrmChonPhieuNX: TFrmChonPhieuNX;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieuNX.Execute;
begin
	mMaDV := pMaDV;
    mKho := pKho;
    CbbDonVi.Enabled := not pFix;
    mLCT := pLCT;
    mLoai := pLoai;

	if ShowModal = mrOK then
          Result := TGuidField(QrPHIEUNX.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNX, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_PHIEUNHAP', GrBrowse);

    SetDisplayFormat(QrPHIEUNX, sysCurFmt);
    SetDisplayFormat(QrPHIEUNX, ['NGAY'], DateTimeFmt);

    mSQL := QrPHIEUNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormShow(Sender: TObject);
begin
   	OpenDataSets([QrPHIEUNX, QrDM_KH_NCC, QrDMKHO]);

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Smart focus
    if mMaDV = '' then
    	try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mMaDV := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.CmdRefreshExecute(Sender: TObject);
var
	s, sDonVi, sKhoHang: String;
    dTuNgay, dDenNgay: TDateTime;
begin
    sDonVi :=  EdMADV.Text;
    sKhoHang := EdMaKho.Text;
    dTuNgay :=  EdTungay.Date;
    dDenNgay :=  EdDenngay.Date;


   	if (mTungay <> dTuNgay)  or (mDenngay <> dDenNgay) or
       (mMaDV <> sDonVi)        or (mKHO <> sKhoHang)         then
	begin
   	    mMaDV := sDonVi;
        mKHO := sKhoHang;
        mTungay := dTuNgay;
        mDenngay := dDenNgay;

        with QrPHIEUNX do
        begin
			Close;

            s := mSQL;
            if mMaDV <> '' then
                s := s + ' and a.MADT=''' + mMaDV + '''';

			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';

            s := s + ' order by a.NGAY, a.SCT';

            SQL.Text := s;
			Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
begin
	QrDM_KH_NCC.Parameters[0].Value := mLoai;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuNX.QrPHIEUNXBeforeOpen(DataSet: TDataSet);
begin
	with QrPHIEUNX do
    begin
    	Parameters[0].Value := mLCT;
        Parameters[1].Value := mTungay;
        Parameters[2].Value := mDenngay;
    end;
end;

procedure TFrmChonPhieuNX.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mMaDV;
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
