﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DondhClose;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, Vcl.Buttons, Vcl.Graphics,
  wwDialog, wwfltdlg, wwFltDlg2, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2, System.Variants, MemTableDataEh, MemTableEh, kbmMemTable;

type
  TFrmDondhClose = class(TForm)
    ActionList: TActionList;
    Panel3: TPanel;
    BtnRegister: TBitBtn;
    BitBtn2: TBitBtn;
    Filter: TwwFilterDialog2;
    CmdOk: TAction;
    DsDummyEh: TDataSource;
    CbbLyDo: TDbLookupComboboxEh2;
    TbDummy: TkbmMemTable;
    TbDummyLyDo: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure CmdOkExecute(Sender: TObject);
  private
    mLydo: String;
  public
  	function Execute(var pLyDo: string) : Boolean;
  end;

var
  FrmDondhClose: TFrmDondhClose;
implementation

uses
	Rights, isDb, ExCommon, isLib, GuidEx, isCommon, isMsg, MainData;


{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDondhClose.Execute;
begin
    Result := ShowModal = mrOK;
    with TbDummy do
    begin
        if Result then
            pLyDo := FieldByName('LyDo').AsString;

        Cancel;
    end;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.FormShow(Sender: TObject);
begin
    with TbDummy do
    begin
        if Active then
            Close;

        Open;
    	Append;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.CmdOkExecute(Sender: TObject);
begin
    with TbDummy do
    begin
        if BlankConfirm(TbDummy, ['LyDo']) then
            Abort;

        if not YesNo('Đơn hàng chưa hoàn tất, thực hiện "Đóng đơn hàng". Tiếp tục?') then
            Abort;
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bIsError: Boolean;
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDondhClose.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.
