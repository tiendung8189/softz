object FrmDmTKKeToan: TFrmDmTKKeToan
  Left = 413
  Top = 271
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'H'#7879' Th'#7889'ng T'#224'i Kho'#7843'n K'#7871' To'#225'n'
  ClientHeight = 743
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton10: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 722
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 343
    Top = 36
    Width = 449
    Height = 686
    Align = alRight
    HotSpotHighlight = 11855600
    HotSpotVisible = True
    LockBar = True
    SizeBarWidth = 7
    TabOrder = 2
    object PaDetail: TScrollBox
      Left = 8
      Top = 0
      Width = 441
      Height = 686
      Align = alLeft
      TabOrder = 0
      object PD1: TisPanel
        Left = 0
        Top = 0
        Width = 437
        Height = 175
        Align = alTop
        Caption = '`'
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Panel1: TPanel
          Left = 1
          Top = 17
          Width = 435
          Height = 157
          Align = alClient
          BevelOuter = bvNone
          TabOrder = 1
          DesignSize = (
            435
            157)
          object Label12: TLabel
            Left = 10
            Top = 58
            Width = 79
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#234'n ti'#7871'ng anh'
            FocusControl = DBEdit1
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label2: TLabel
            Left = 16
            Top = 10
            Width = 73
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#224'i kho'#7843'n s'#7889
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object Label6: TLabel
            Left = 11
            Top = 34
            Width = 78
            Height = 16
            Alignment = taRightJustify
            Caption = 'T'#234'n t'#224'i kho'#7843'n'
            Font.Charset = ANSI_CHARSET
            Font.Color = clBlack
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object DBEdit1: TwwDBEdit
            Left = 96
            Top = 54
            Width = 309
            Height = 22
            Ctl3D = False
            DataField = 'TENTK_TA'
            DataSource = DsDanhmuc
            ParentCtl3D = False
            TabOrder = 3
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdMa: TwwDBEdit
            Tag = 1
            Left = 96
            Top = 6
            Width = 185
            Height = 22
            CharCase = ecUpperCase
            Color = 15794175
            Ctl3D = False
            DataField = 'MATK'
            DataSource = DsDanhmuc
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clPurple
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 0
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object EdTen: TwwDBEdit
            Tag = 1
            Left = 96
            Top = 30
            Width = 309
            Height = 22
            Anchors = [akLeft, akTop, akRight]
            Color = 15794175
            Ctl3D = False
            DataField = 'TENTK'
            DataSource = DsDanhmuc
            Font.Charset = ANSI_CHARSET
            Font.Color = 8404992
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentCtl3D = False
            ParentFont = False
            TabOrder = 2
            UnboundDataType = wwDefault
            WantReturns = False
            WordWrap = False
          end
          object wwCheckBox1: TwwCheckBox
            Left = 287
            Top = 11
            Width = 118
            Height = 17
            TabStop = False
            DisableThemes = False
            AlwaysTransparent = False
            ValueChecked = 'True'
            ValueUnchecked = 'False'
            DisplayValueChecked = 'True'
            DisplayValueUnchecked = 'False'
            NullAndBlankState = cbUnchecked
            BiDiMode = bdRightToLeft
            Caption = 'Ng'#7915'ng theo d'#245'i'
            DataField = 'TINHTRANG'
            DataSource = DsDanhmuc
            ParentBiDiMode = False
            TabOrder = 1
          end
          object cbbMATK_CAP1: TDbLookupComboboxEh2
            Left = 96
            Top = 78
            Width = 309
            Height = 22
            ControlLabel.Width = 91
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#224'i kho'#7843'n c'#7845'p 1'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MATK_CAP1'
            DataSource = DsDanhmuc
            DropDownBox.Columns = <
              item
                AutoFitColWidth = False
                FieldName = 'TENTK'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 230
              end
              item
                AutoFitColWidth = False
                FieldName = 'MATK'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 70
              end>
            DropDownBox.ListSource = DsTaiKhoanCap1
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 310
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MATK'
            ListField = 'TENTK'
            ListSource = DsTaiKhoanCap1
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 4
            Visible = True
          end
          object cbbMATK_CAP2: TDbLookupComboboxEh2
            Left = 96
            Top = 102
            Width = 309
            Height = 22
            ControlLabel.Width = 91
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#224'i kho'#7843'n c'#7845'p 2'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'MATK_CAP2'
            DataSource = DsDanhmuc
            DropDownBox.Columns = <
              item
                AutoFitColWidth = False
                FieldName = 'TENTK'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
                Width = 230
              end
              item
                AutoFitColWidth = False
                FieldName = 'MATK'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'M'#227
                Width = 70
              end>
            DropDownBox.ListSource = DsTaiKhoanCap2
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            DropDownBox.Width = 310
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MATK'
            ListField = 'TENTK'
            ListSource = DsTaiKhoanCap2
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 5
            Visible = True
            OnDropDown = cbbMATK_CAP2DropDown
          end
          object cbbTINHCHAT: TDbLookupComboboxEh2
            Left = 96
            Top = 126
            Width = 309
            Height = 22
            ControlLabel.Width = 53
            ControlLabel.Height = 16
            ControlLabel.Caption = 'T'#237'nh ch'#7845't'
            ControlLabel.Font.Charset = DEFAULT_CHARSET
            ControlLabel.Font.Color = clWindowText
            ControlLabel.Font.Height = -13
            ControlLabel.Font.Name = 'Tahoma'
            ControlLabel.Font.Style = []
            ControlLabel.ParentFont = False
            ControlLabel.Visible = True
            ControlLabelLocation.Spacing = 5
            ControlLabelLocation.Position = lpLeftCenterEh
            AlwaysShowBorder = True
            AutoSize = False
            BevelKind = bkFlat
            Ctl3D = False
            ParentCtl3D = False
            BorderStyle = bsNone
            DynProps = <>
            DataField = 'TINHCHAT'
            DataSource = DsDanhmuc
            DropDownBox.Columns = <
              item
                FieldName = 'TEN_HOTRO'
                Font.Charset = DEFAULT_CHARSET
                Font.Color = clWindowText
                Font.Height = -12
                Font.Name = 'Tahoma'
                Font.Style = []
                SpecCell.Font.Charset = DEFAULT_CHARSET
                SpecCell.Font.Color = clWindowText
                SpecCell.Font.Height = -12
                SpecCell.Font.Name = 'Tahoma'
                SpecCell.Font.Style = []
                Title.Alignment = taCenter
                Title.Caption = 'T'#234'n'
              end>
            DropDownBox.ListSource = DsTinhChat
            DropDownBox.ListSourceAutoFilter = True
            DropDownBox.ListSourceAutoFilterType = lsftContainsEh
            DropDownBox.ListSourceAutoFilterAllColumns = True
            DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
            DropDownBox.AutoDrop = True
            DropDownBox.Rows = 15
            DropDownBox.Sizable = True
            DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
            DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
            DropDownBox.SpecRow.Font.Color = clWindowText
            DropDownBox.SpecRow.Font.Height = -12
            DropDownBox.SpecRow.Font.Name = 'Tahoma'
            DropDownBox.SpecRow.Font.Style = []
            EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
            EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
            EmptyDataInfo.Font.Color = clSilver
            EmptyDataInfo.Font.Height = -13
            EmptyDataInfo.Font.Name = 'Tahoma'
            EmptyDataInfo.Font.Style = [fsItalic]
            EmptyDataInfo.ParentFont = False
            EmptyDataInfo.Alignment = taLeftJustify
            EditButton.DefaultAction = True
            EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
            EditButton.Style = ebsAltDropDownEh
            EditButton.Width = 20
            EditButton.DrawBackTime = edbtWhenHotEh
            EditButtons = <>
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            Flat = True
            KeyField = 'MA_HOTRO'
            ListField = 'TEN_HOTRO'
            ListSource = DsTinhChat
            ParentFont = False
            ShowHint = True
            Style = csDropDownEh
            TabOrder = 6
            Visible = True
          end
        end
      end
      object PD5: TisPanel
        Left = 0
        Top = 599
        Width = 437
        Height = 83
        Align = alBottom
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = 10841658
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clWhite
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object DBMemo1: TDBMemo
          Left = 1
          Top = 17
          Width = 435
          Height = 65
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GHICHU'
          DataSource = DsDanhmuc
          TabOrder = 1
        end
      end
    end
  end
  object PaList: TPanel
    Left = 0
    Top = 36
    Width = 343
    Height = 686
    Align = alClient
    TabOrder = 3
    object GrList: TwwDBGrid2
      Left = 1
      Top = 61
      Width = 341
      Height = 624
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'MATK'#9'20'#9'T'#224'i kho'#7843'n s'#7889#9'F'
        'MATK_CAP1'#9'20'#9'C'#7845'p 1'#9'F'#9'Thu'#7897'c t'#224'i kho'#7843'n'
        'MATK_CAP2'#9'20'#9'C'#7845'p 2'#9'F'#9'Thu'#7897'c t'#224'i kho'#7843'n'
        'TENTK'#9'30'#9'T'#234'n t'#224'i kho'#7843'n'#9'F'
        'LK_TINHTRANG'#9'15'#9'T'#237'nh ch'#7845't'#9'F'
        'GHICHU'#9'25'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsDanhmuc
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 1
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnEnter = CmdRefeshExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
    object PaSearch: TPanel
      Left = 1
      Top = 1
      Width = 341
      Height = 60
      Align = alTop
      TabOrder = 0
      DesignSize = (
        341
        60)
      object Label18: TLabel
        Left = 9
        Top = 8
        Width = 114
        Height = 16
        Caption = '&Nh'#7853'p chu'#7895'i c'#7847'n t'#236'm:'
        FocusControl = EdSearch
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdSearch: TAdvEdit
        Left = 9
        Top = 30
        Width = 327
        Height = 24
        EmptyText = 'T'#234'n, '#273'i'#7879'n tho'#7841'i'
        EmptyTextStyle = [fsItalic]
        LabelFont.Charset = DEFAULT_CHARSET
        LabelFont.Color = clWindowText
        LabelFont.Height = -11
        LabelFont.Name = 'Tahoma'
        LabelFont.Style = []
        Lookup.Font.Charset = DEFAULT_CHARSET
        Lookup.Font.Color = clWindowText
        Lookup.Font.Height = -11
        Lookup.Font.Name = 'Arial'
        Lookup.Font.Style = []
        Lookup.Separator = ';'
        Anchors = [akLeft, akTop, akRight]
        AutoSelect = False
        Color = clWindow
        TabOrder = 0
        Visible = True
        OnKeyPress = EdSearchKeyPress
        Version = '3.5.0.1'
      end
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 64
    Top = 272
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Hint = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdContact: TAction
      Caption = 'Li'#234'n l'#7841'c'
      Hint = 'Danh s'#225'ch ng'#432#7901'i li'#234'n l'#7841'c'
    end
    object CmdRefesh: TAction
      OnExecute = CmdRefeshExecute
    end
    object CmdDmtk: TAction
      Caption = 'T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
      Hint = 'T'#224'i kho'#7843'n ng'#226'n h'#224'ng'
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByFilter
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MADT'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MADT'
      'TENDT'
      'MST'
      'DCHI'
      'DTHOAI'
      'FAX'
      'TENTK'
      'MATK'
      'TEN_NGANHANG'
      'DC_NGANHANG')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 148
    Top = 320
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDanhmucBeforeInsert
    BeforePost = QrDanhmucBeforePost
    AfterPost = QrDanhmucAfterPost
    BeforeDelete = QrDanhmucBeforeDelete
    AfterDelete = QrDanhmucAfterPost
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'ACC_DM_TAIKHOAN_KETOAN'
      'where    1 = 1')
    Left = 40
    Top = 112
    object QrDanhmucMATK: TWideStringField
      FieldName = 'MATK'
      OnChange = QrDanhmucMATKChange
      Size = 50
    end
    object QrDanhmucTENTK: TWideStringField
      FieldName = 'TENTK'
      Size = 200
    end
    object QrDanhmucTENTK_TA: TWideStringField
      FieldName = 'TENTK_TA'
      Size = 200
    end
    object QrDanhmucTINHCHAT: TWideStringField
      FieldName = 'TINHCHAT'
      Size = 70
    end
    object QrDanhmucMATK_CAP1: TWideStringField
      FieldName = 'MATK_CAP1'
      Size = 50
    end
    object QrDanhmucMATK_CAP2: TWideStringField
      FieldName = 'MATK_CAP2'
      Size = 50
    end
    object QrDanhmucTINHTRANG: TBooleanField
      FieldName = 'TINHTRANG'
    end
    object QrDanhmucGHICHU: TWideStringField
      FieldName = 'GHICHU'
      Size = 200
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrDanhmucLK_TINHTRANG: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TINHTRANG'
      LookupDataSet = QrTinhChat
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'TINHTRANG'
      Lookup = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 40
    Top = 140
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 272
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 64
    Top = 300
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Khngspxp2: TMenuItem
      Action = CmdClearFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object ItemLienlac: TMenuItem
      Caption = 'Ng'#7915'ng theo d'#245'i'
      OnClick = ItemLienlacClick
    end
    object N3: TMenuItem
      Caption = '-'
    end
    object N4: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object QrTaiKhoanCap1: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * '
      'from ACC_DM_TAIKHOAN_KETOAN'
      'where isnull(MATK, '#39#39') <> '#39#39
      '    and isnull(MATK_CAP1, '#39#39') = '#39#39)
    Left = 216
    Top = 112
  end
  object QrTaiKhoanCap2: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select * '
      'from ACC_DM_TAIKHOAN_KETOAN')
    Left = 244
    Top = 112
  end
  object QrTinhChat: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select  '#9'*'
      '  from  '#9'V_ACC_DM_TAIKHOAN_KETOAN_TINHCHAT'
      ''
      ' ')
    Left = 272
    Top = 112
  end
  object CHECK_DTHOAI: TADOCommand
    CommandText = 
      'select 1 from DM_KH_NCC where DTHOAI = :DTHOAI'#13#10'and MADT <> :MAD' +
      'T'
    CommandTimeout = 0
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'DTHOAI'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 100
        Value = Null
      end
      item
        Name = 'MADT'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 15
        Value = Null
      end>
    Left = 108
    Top = 196
  end
  object DsTinhChat: TDataSource
    DataSet = QrTinhChat
    Left = 272
    Top = 156
  end
  object DsTaiKhoanCap1: TDataSource
    DataSet = QrTaiKhoanCap1
    Left = 208
    Top = 156
  end
  object DsTaiKhoanCap2: TDataSource
    DataSet = QrTaiKhoanCap2
    Left = 240
    Top = 156
  end
end
