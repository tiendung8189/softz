﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Chikhac;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2, AdvMenus,
  AppEvnts, isPanel, wwfltdlg, System.Variants,
  frameNgay, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid, ToolWin,
  Vcl.Buttons, AdvEdit, DBAdvEd, DBCtrlsEh, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmChikhac = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Hinttc1: TMenuItem;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    PaMaster: TisPanel;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    CmdReRead: TAction;
    QrTCLCT: TWideStringField;
    QrTCLK_LYDO: TWideStringField;
    CmdAudit: TAction;
    DBText1: TDBText;
    QrTCPTTT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    CmdListRefesh: TAction;
    QrTCKHOA: TGuidField;
    QrTCLOC: TWideStringField;
    QrTCMATK: TWideStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    PaTK: TPanel;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepChecked: TToolButton;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    QrTCMALOC: TWideStringField;
    QrTCLK_TENLOC: TWideStringField;
    PaMALOC: TPanel;
    N2: TMenuItem;
    ItemObsolete: TMenuItem;
    QrTCMACP: TWideStringField;
    QrTCLK_TENCP: TWideStringField;
    QrTCLK_MACP_NHOM: TWideStringField;
    QrTCLK_TENCP_NHOM: TWideStringField;
    QrTCSCT2: TWideStringField;
    QrTCNguoi: TWideStringField;
    QrTCGhiChu: TWideMemoField;
    QrTCSoTien: TFloatField;
    QrTCThanhToan: TFloatField;
    CbHinhThuc: TDbLookupComboboxEh2;
    CbbLyDo: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    EdNhomChiPhi: TDBEditEh;
    CbbMaLoc: TDbLookupComboboxEh2;
    EdMaLoc: TDBEditEh;
    CbTenTK: TDbLookupComboboxEh2;
    EdSTK: TDBEditEh;
    EdChinhanh: TDBEditEh;
    EdNganhang: TDBEditEh;
    DBEditEh4: TDBEditEh;
    EdNumSoTien: TDBNumberEditEh;
    DBMemoEh1: TDBMemoEh;
    EdSoPhieu: TDBEditEh;
    DBEditEh2: TDBEditEh;
    CbMACP: TDbLookupComboboxEh2;
    QrTCLyDo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure CbMACPKeyPress(Sender: TObject; var Key: Char);
    procedure QrTCMATKChange(Sender: TField);
    procedure QrTCMACPChange(Sender: TField);
  private
	mCanEdit, mFuncTK, mObsolete : Boolean;
   	fTungay, fDenngay 	: TDateTime;
    fLoc, fSQL: String;
    mMakho, mLydo, mPTTT, mLCT: String;

  public
	procedure Execute (r: WORD);
  end;

var
  FrmChikhac: TFrmChikhac;

implementation

uses
	isMsg, isDb, ExCommon, MainData, RepEngine, Rights, GuidEx, isLib, isCommon;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_CHIKHAC';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_MSG = 'Đã có phiếu thu, không thể xóa / sửa được.';

procedure TFrmChikhac.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsTC.AutoEdit := mCanEdit;

    mLCT := 'PCK';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    mFuncTK := sysFuncTK;
    mPTTT := RegReadString(Name, 'Pttt', sysPTTT);
	mMakho := RegReadString(Name, 'Makho', sysDefKho);
	mLydo := RegReadString(Name, 'Lydo', GetSysParam('DEFAULT_LYDO_CHI'));

    mObsolete := False;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.FormShow(Sender: TObject);
begin
	// Open Database
    with DataMain do
    	OpenDataSets([QrDMKHO, QrLYDO_CK, QrPTTT, QrDMTK, QrDMTK_NB, QrNganhang,
            QrNganhangCN, QrFB_DM_CHIPHINHOM]);

	SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetCustomGrid(FORM_CODE, GrBrowse);
    SetDictionary(QrTC, FORM_CODE, Filter);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsDrc then
    begin
        PaMALOC.Visible := False;
        grRemoveFields(GrBrowse, 'MALOC;LK_TENLOC', ';');
    end;

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
        grRemoveFields(GrBrowse, 'MATK;LK_TENTK;LK_NGANHANG', ';');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho', 'Pttt', 'Lydo'], [mMakho, mPTTT, mLydo]);
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.PgMainChange(Sender: TObject);
begin
    SCreen.Cursor := crSQLWait;
	if PgMain.ActivePageIndex = 1 then
    begin
        try
   	    	CbNgay.SetFocus;
        except
        end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
    SCreen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');
            SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
    	QrDMKHO.Requery;
        QrLYDO_CHIPHI.Requery;
        QrPTTT.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdNewExecute(Sender: TObject);
begin
	if not (QrTC.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
			QrTC.Cancel;

	QrTC.Append;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdSaveExecute(Sender: TObject);
begin
	QrTC.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdDelExecute(Sender: TObject);
begin
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime, 2);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex =0 then
        exSearch(Name, DsTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdReReadExecute(Sender: TObject);
begin
    fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                            and exCheckLoc(QrTC, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime := d;
		FieldByName('LCT').AsString    := mLCT;
        FieldByName('MAKHO').AsString  := mMakho;
        FieldByName('MALOC').AsString  := sysLoc;
        FieldByName('LOC').AsString    := sysLoc;
        if mLydo <> '' then
            FieldByName('Lydo').AsString   := mLydo;
        FieldByName('PTTT').AsString   := mPTTT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
    if mTrigger then
        Exit;

    exIsChecked(QrTC);

    with QrTC do
    begin
		exValidClosing(FieldByName('NGAY').AsDateTime, 2);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCBeforePost(DataSet: TDataSet);
var
    pPTTT: string;
begin
    with QrTC do
    begin
		if BlankConfirm(QrTC, ['NGAY', 'PTTT', 'Lydo', 'MALOC']) then
    		Abort;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

        if BlankConfirm(QrTC, ['SoTien']) then
    		Abort;

        if FieldByName('PTTT').AsString = '02' then
            pPTTT := 'CHICK'
        else if FieldByName('PTTT').AsString = '01' then
            pPTTT := 'CHITM'
        else
            pPTTT := '';

        exValidClosing(FieldByName('NGAY').AsDateTime, 2);
        SetNull(QrTC, ['MAKHO', 'MACP']);
    end;
	DataMain.AllocSCT(mLCT, QrTC);
    DataMain.AllocSCT2(pPTTT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCMACPChange(Sender: TField);
begin
     EdNhomChiPhi.Text := EdNhomChiPhi.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCMATKChange(Sender: TField);
begin
    EdChinhanh.Text := EdChinhanh.Field.AsString;
    EdNganhang.Text := EdNganhang.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCNGAYValidate(Sender: TField);
begin
    with QrTC do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCSOTIENChange(Sender: TField);
begin
    with QrTC do
        FieldByName('ThanhToan').AsFloat := FieldByName('SoTien').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCAfterScroll(DataSet: TDataSet);
begin
   PgMainChange(PgMain);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CbMACPKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.QrTCAfterPost(DataSet: TDataSet);
begin
    with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
        mPTTT := FieldByName('PTTT').AsString;
        mLydo := FieldByName('Lydo').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChikhac.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

end.
