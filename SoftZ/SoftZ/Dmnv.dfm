object FrmDmnv: TFrmDmnv
  Left = 117
  Top = 93
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh S'#225'ch Nh'#226'n Vi'#234'n'
  ClientHeight = 573
  ClientWidth = 792
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 792
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton10: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 356
    Top = 36
    Width = 436
    Height = 516
    Align = alRight
    HotSpotHighlight = 11855600
    HotSpotVisible = True
    LockBar = True
    SizeBarWidth = 7
    TabOrder = 2
    object PaDetail: TScrollBox
      Left = 8
      Top = 0
      Width = 428
      Height = 516
      Align = alLeft
      TabOrder = 0
      object PD1: TisPanel
        Left = 0
        Top = 0
        Width = 424
        Height = 250
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 0
        HeaderCaption = ' :: Th'#244'ng tin'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = False
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label6: TLabel
          Left = 52
          Top = 54
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'H'#7885' t'#234'n'
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 50
          Top = 126
          Width = 39
          Height = 16
          Alignment = taRightJustify
          Caption = #272#7883'a ch'#7881
          FocusControl = DBEdit2
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label4: TLabel
          Left = 31
          Top = 150
          Width = 58
          Height = 16
          Alignment = taRightJustify
          Caption = #272'i'#7879'n tho'#7841'i'
          FocusControl = DBEdit3
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 58
          Top = 198
          Width = 31
          Height = 16
          Alignment = taRightJustify
          Caption = 'Email'
          FocusControl = DBEdit5
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label2: TLabel
          Left = 55
          Top = 30
          Width = 34
          Height = 16
          Alignment = taRightJustify
          Caption = 'M'#227' s'#7889
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label1: TLabel
          Left = 43
          Top = 102
          Width = 46
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ch'#7913'c v'#7909
          FocusControl = wwDBEdit1
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label5: TLabel
          Left = 52
          Top = 174
          Width = 37
          Height = 16
          Alignment = taRightJustify
          Caption = 'Mobile'
          FocusControl = wwDBEdit2
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label30: TLabel
          Left = 30
          Top = 76
          Width = 60
          Height = 16
          Alignment = taRightJustify
          Caption = 'Ph'#242'ng ban'
          Font.Charset = ANSI_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object EdTen: TwwDBEdit
          Tag = 1
          Left = 96
          Top = 50
          Width = 309
          Height = 22
          Color = 15794175
          Ctl3D = False
          DataField = 'TENNV'
          DataSource = DsDMNV
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 2
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit2: TwwDBEdit
          Left = 96
          Top = 122
          Width = 309
          Height = 22
          Ctl3D = False
          DataField = 'DCHI'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 6
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit3: TwwDBEdit
          Left = 96
          Top = 146
          Width = 309
          Height = 22
          Ctl3D = False
          DataField = 'DTHOAI'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 7
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object DBEdit5: TwwDBEdit
          Left = 96
          Top = 194
          Width = 309
          Height = 22
          Ctl3D = False
          DataField = 'EMAIL'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 9
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object EdMa: TwwDBEdit
          Tag = 1
          Left = 96
          Top = 26
          Width = 168
          Height = 22
          CharCase = ecUpperCase
          Color = 15794175
          Ctl3D = False
          DataField = 'MANV'
          DataSource = DsDMNV
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clPurple
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentCtl3D = False
          ParentFont = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit1: TwwDBEdit
          Left = 96
          Top = 98
          Width = 309
          Height = 22
          Ctl3D = False
          DataField = 'CHUCVU'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 5
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object wwDBEdit2: TwwDBEdit
          Left = 96
          Top = 170
          Width = 309
          Height = 22
          Ctl3D = False
          DataField = 'DIDONG'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 8
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
        object CbNGAY: TwwDBDateTimePicker
          Left = 116
          Top = 218
          Width = 101
          Height = 22
          BorderStyle = bsNone
          CalendarAttributes.Font.Charset = DEFAULT_CHARSET
          CalendarAttributes.Font.Color = clWindowText
          CalendarAttributes.Font.Height = -11
          CalendarAttributes.Font.Name = 'MS Sans Serif'
          CalendarAttributes.Font.Style = []
          DataField = 'THOIVIEC_NGAY'
          DataSource = DsDMNV
          Epoch = 1950
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          Font.Charset = ANSI_CHARSET
          Font.Color = 8404992
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          ShowButton = True
          TabOrder = 11
        end
        object wwDBLookupCombo11: TwwDBLookupCombo
          Left = 96
          Top = 74
          Width = 242
          Height = 22
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taLeftJustify
          Selected.Strings = (
            'TENPB'#9'25'#9'TENPB'#9'F'
            'MAPB'#9'5'#9'MAPB'#9#9)
          DataField = 'MAPB'
          DataSource = DsDMNV
          LookupTable = DataMain.QrDMPB
          LookupField = 'MAPB'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 3
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwDBLookupCombo12: TwwDBLookupCombo
          Left = 340
          Top = 74
          Width = 65
          Height = 22
          TabStop = False
          Ctl3D = False
          BorderStyle = bsNone
          DropDownAlignment = taRightJustify
          Selected.Strings = (
            'MAPB'#9'5'#9'MANXB'#9'F'
            'TENPB'#9'25'#9'TENNXB'#9#9)
          DataField = 'MAPB'
          DataSource = DsDMNV
          LookupTable = DataMain.QrDMPB
          LookupField = 'MAPB'
          Options = [loColLines]
          Style = csDropDownList
          Frame.Enabled = True
          Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
          ParentCtl3D = False
          TabOrder = 4
          AutoDropDown = True
          ShowButton = True
          UseTFields = False
          PreciseEditRegion = False
          AllowClearKey = True
          ShowMatchText = True
        end
        object wwCheckBox1: TwwCheckBox
          Left = 37
          Top = 221
          Width = 73
          Height = 17
          TabStop = False
          DisableThemes = False
          AlwaysTransparent = False
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          DisplayValueChecked = 'True'
          DisplayValueUnchecked = 'False'
          NullAndBlankState = cbUnchecked
          BiDiMode = bdRightToLeft
          Caption = 'Ngh'#7881' vi'#7879'c'
          DataField = 'THOIVIEC'
          DataSource = DsDMNV
          ParentBiDiMode = False
          TabOrder = 10
        end
        object CkbManager: TwwCheckBox
          Left = 304
          Top = 221
          Width = 101
          Height = 17
          DisableThemes = False
          AlwaysTransparent = False
          ValueChecked = 'True'
          ValueUnchecked = 'False'
          DisplayValueChecked = 'True'
          DisplayValueUnchecked = 'False'
          NullAndBlankState = cbUnchecked
          BiDiMode = bdRightToLeft
          Caption = 'Ch'#7911' c'#7917'a h'#224'ng'
          DataField = 'CHU_CUAHANG'
          DataSource = DsDMNV
          ParentBiDiMode = False
          TabOrder = 12
        end
      end
      object PD5: TisPanel
        Left = 0
        Top = 311
        Width = 424
        Height = 201
        Align = alClient
        Color = 16119285
        ParentBackground = False
        TabOrder = 2
        HeaderCaption = ' .: Ghi ch'#250
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object EdGhichu: TDBMemo
          Left = 1
          Top = 17
          Width = 422
          Height = 183
          Align = alClient
          BorderStyle = bsNone
          DataField = 'GHICHU'
          DataSource = DsDMNV
          TabOrder = 1
        end
      end
      object PD2: TisPanel
        Left = 0
        Top = 250
        Width = 424
        Height = 61
        Align = alTop
        Color = 16119285
        ParentBackground = False
        TabOrder = 1
        Visible = False
        HeaderCaption = ' .: L'#432#417'ng'
        HeaderColor = 16119285
        ImageSet = 4
        RealHeight = 0
        ShowButton = True
        HeaderBevelInner = bvNone
        HeaderBevelOuter = bvNone
        HeaderFont.Charset = ANSI_CHARSET
        HeaderFont.Color = clBlue
        HeaderFont.Height = -11
        HeaderFont.Name = 'Tahoma'
        HeaderFont.Style = [fsBold]
        object Label7: TLabel
          Left = 110
          Top = 31
          Width = 123
          Height = 16
          Alignment = taRightJustify
          Caption = '% L'#432#417'ng '#273#432#7907'c h'#432#7903'ng'
          FocusControl = wwDBEdit3
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 301
          Top = 31
          Width = 12
          Height = 16
          Caption = '%'
          FocusControl = wwDBEdit3
          Font.Charset = ANSI_CHARSET
          Font.Color = clBlack
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object wwDBEdit3: TwwDBEdit
          Left = 240
          Top = 28
          Width = 56
          Height = 22
          Ctl3D = False
          DataField = 'LUONG'
          DataSource = DsDMNV
          ParentCtl3D = False
          TabOrder = 1
          UnboundDataType = wwDefault
          WantReturns = False
          WordWrap = False
        end
      end
    end
  end
  object PaList: TPanel
    Left = 0
    Top = 36
    Width = 356
    Height = 516
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 3
    object GrList: TwwDBGrid2
      Left = 0
      Top = 0
      Width = 356
      Height = 516
      DittoAttributes.ShortCutDittoField = 16397
      DittoAttributes.Options = [wwdoSkipReadOnlyFields]
      Selected.Strings = (
        'MANV'#9'15'#9'M'#227' s'#7889#9'F'
        'TENNV'#9'30'#9'H'#7885' t'#234'n'#9'F'
        'DCHI'#9'30'#9#272#7883'a ch'#7881#9'F'
        'GHICHU'#9'30'#9'Ghi ch'#250#9'F')
      IniAttributes.Delimiter = ';;'
      TitleColor = 13360356
      FixedCols = 0
      ShowHorzScrollBar = True
      Align = alClient
      DataSource = DsDMNV
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      KeyOptions = [dgEnterToTab]
      Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      TitleAlignment = taCenter
      TitleFont.Charset = ANSI_CHARSET
      TitleFont.Color = 8404992
      TitleFont.Height = -11
      TitleFont.Name = 'Tahoma'
      TitleFont.Style = [fsBold]
      TitleLines = 1
      TitleButtons = True
      UseTFields = False
      OnDblClick = CmdCloseExecute
      TitleImageList = DataMain.ImageSort
      PadColumnStyle = pcsPadHeader
    end
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 64
    Top = 272
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin        '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ImageIndex = 39
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Hint = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdContact: TAction
      Caption = 'Li'#234'n l'#7841'c'
      Hint = 'Danh s'#225'ch ng'#432#7901'i li'#234'n l'#7841'c'
    end
    object CmdReload: TAction
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDMNV
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MANV'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MADT'
      'TENDT'
      'MST'
      'DCHI'
      'DTHOAI'
      'FAX'
      'TENTK'
      'SOTK'
      'TEN_NGANHANG'
      'DC_NGANHANG')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 116
    Top = 272
  end
  object QrDMNV: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeInsert = QrDMNVBeforeInsert
    AfterInsert = QrDMNVAfterInsert
    BeforePost = QrDMNVBeforePost
    AfterPost = QrDMNVAfterPost
    BeforeDelete = QrDMNVBeforeDelete
    OnDeleteError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from '#9'DM_NHANVIEN')
    Left = 40
    Top = 112
    object QrDMNVMANV: TWideStringField
      DisplayLabel = 'M'#227' s'#7889
      FieldName = 'MANV'
      OnChange = QrDMNVMANVChange
      Size = 15
    end
    object QrDMNVTENNV: TWideStringField
      DisplayLabel = 'H'#7885' t'#234'n'
      FieldName = 'TENNV'
      Size = 200
    end
    object QrDMNVDTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'DTHOAI'
      Size = 100
    end
    object QrDMNVDCHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881
      FieldName = 'DCHI'
      Size = 200
    end
    object QrDMNVEMAIL: TWideStringField
      DisplayLabel = 'Email'
      FieldName = 'EMAIL'
      Size = 100
    end
    object QrDMNVTHOIVIEC: TBooleanField
      FieldName = 'THOIVIEC'
    end
    object QrDMNVCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDMNVUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDMNVCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDMNVUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDMNVGHICHU: TWideMemoField
      FieldName = 'GHICHU'
      BlobType = ftWideMemo
    end
    object QrDMNVCHUCVU: TWideStringField
      FieldName = 'CHUCVU'
      Size = 100
    end
    object QrDMNVDIDONG: TWideStringField
      FieldName = 'DIDONG'
      Size = 100
    end
    object QrDMNVLUONG: TFloatField
      FieldName = 'LUONG'
    end
    object QrDMNVTHOIVIEC_NGAY: TDateTimeField
      DisplayLabel = 'Ng'#224'y ngh'#297' vi'#7879'c'
      FieldName = 'THOIVIEC_NGAY'
    end
    object QrDMNVCHU_CUAHANG: TBooleanField
      FieldName = 'CHU_CUAHANG'
    end
    object QrDMNVMAPB: TWideStringField
      DisplayLabel = 'Ph'#242'ng ban'
      FieldName = 'MAPB'
      Size = 5
    end
  end
  object DsDMNV: TDataSource
    DataSet = QrDMNV
    Left = 40
    Top = 140
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 92
    Top = 272
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    OnPopup = PopupMenu1Popup
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.5.4.3'
    Left = 144
    Top = 272
    object Tm1: TMenuItem
      Action = CmdSearch
    end
    object Lc1: TMenuItem
      Action = CmdFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object Khngspxp2: TMenuItem
      Action = CmdClearFilter
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object mnObsolete: TMenuItem
      Caption = 'Ngh'#7881' vi'#7879'c'
      Checked = True
      OnClick = mnObsoleteClick
    end
  end
  object ALLOC_MANV: TADOCommand
    CommandText = 'ALLOC_MANV;1'
    CommandType = cmdStoredProc
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@LEN'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@CODE'
        Attributes = [paNullable]
        DataType = ftString
        Direction = pdInputOutput
        Size = 15
        Value = Null
      end>
    Left = 80
    Top = 112
  end
end
