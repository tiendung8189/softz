﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit InlabelChungtu;

interface

uses
  SysUtils, Classes, Controls, Forms, ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  StdCtrls, ADODb, Db, Wwfltdlg2, Menus, wwdbdatetimepicker, AppEvnts, AdvMenus,
  wwfltdlg, wwDialog, Grids, Wwdbigrd, Wwdbgrid, ToolWin, Vcl.Buttons;

type
  TFrmInlabelChungtu = class(TForm)
    Action: TActionList;
    CmdClose: TAction;
    CmdExport: TAction;
    ToolBar1: TToolBar;
    btExport: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    DsDMVT: TDataSource;
    CmdSum: TAction;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    Status: TStatusBar;
    CmdRefresh: TAction;
    ToolButton1: TToolButton;
    CmdIntem: TAction;
    ToolButton2: TToolButton;
    QrCT: TADOStoredProc;
    PaDondh: TPanel;
    SpBt1: TSpeedButton;
    SpBt2: TSpeedButton;
    CmdMavach2: TAction;
    BtIn: TToolButton;
    ToolButton4: TToolButton;
    PopIn: TAdvPopupMenu;
    Mu35x25x31: TMenuItem;
    MenuItem3: TMenuItem;
    Mu35x15x31: TMenuItem;
    Mu35x15x41: TMenuItem;
    MenuItem5: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdExportExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSumExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure CmdIntemExecute(Sender: TObject);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure SpBt1Click(Sender: TObject);
    procedure CmdMavach2Execute(Sender: TObject);
    procedure BtInClick(Sender: TObject);
    procedure QrCTAfterOpen(DataSet: TDataSet);
  private
  	mExPath, mLCT: String;
  	mStamp: Integer;
    mKhoa: TGUID;
	mTrigger: Boolean;
    procedure SaveUncomplete;
    procedure LoadUncomplete;
  public
    procedure Excecute(khoa: TGUID; lct: String = 'NMUA');
  end;

var
  FrmInlabelChungtu: TFrmInlabelChungtu;

implementation

uses
	ExCommon, isDb, isMsg, isStr, MainData, isLib, isFile, isCommon, GuidEx;

{$R *.DFM}

const
	FORM_CODE = 'IN_LABEL_CHUNGTU';

(*==============================================================================
**------------------------------------------------------------------------------
*)

procedure TFrmInlabelChungtu.Excecute(khoa: TGUID; lct: String);
begin
    mKhoa := khoa;
    mLCT := lct;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.SaveUncomplete;
begin
    if not DirectoryExists(mExPath) then
        if not ForceDirectories(mExPath) then
        begin
            ErrMsg(RS_FILE_IO);
            Exit;
        end;

    QrCT.Filter := 'SOLUONG<>0';
//	TextExport(QrDMVT, ['MAVT', 'STAMP'], mExPath + 'Stamp.csv', TEncoding.UTF8);
    TextExport(QrCT, ['MAVT', 'SOLUONG'], mExPath + 'Stamp.csv');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.SpBt1Click(Sender: TObject);
var
    bm: TBytes;
begin
    with QrCT do
    begin
        bm := Bookmark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SELECTED').AsBoolean := (Sender as TSpeedButton).Tag = 0;
            Next;
        end;
        Bookmark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.LoadUncomplete;
var
    i: Integer;
	s, x: String;
    ls, fs: TStrings;
begin
	// Check existing
    s := mExPath + 'Stamp.csv';
	if not FileExists(s) then
    	Exit;

    // Proc.
    ls := TStringList.Create;
    ls.LoadFromFile(s);

    // Update stamp count
    fs := TStringList.Create;
    with QrCT do
    begin
    	DisableControls;

        for i := 0 to ls.Count - 1 do
        begin
        	isStrBreak(ls[i], ',', fs);
        	s := fs[0];
            x := fs[1];

            if Locate('MAVT', s, []) then
            begin
            	Edit;
                FieldByName('SOLUONG').AsInteger := StrToIntDef(x, 0);
                Post;
            end;
	    end;

        First;
    	EnableControls;
    end;
    ls.Free;
    fs.Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.QrCTAfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrCT, sysCurFmt);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.QrCTBeforeOpen(DataSet: TDataSet);
begin
    with QrCT do
    begin
        Parameters[1].Value := TGuidEx.ToString(mKhoa);
        Parameters[2].Value := mLCT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.FormShow(Sender: TObject);
begin
	mStamp := GetSysParam('STAMP_OF_PAGE');
    mExPath := IncludeTrailingPathDelimiter(GetSysParam('FOLDER_BARCODE'));

    Wait(PREPARING);
    with QrCT do
    begin
        if Active then
            Close;
    	Open;
//	    LoadUncomplete;
    end;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrCT, FORM_CODE, Filter);
    ClearWait;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	SaveUncomplete;
    try
        CloseDataSets([QrCT]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdExportExecute(Sender: TObject);
var
    ngay: TDateTime;
    s: String;
begin
	DataMain.LabelOpen;
    ngay := Date;

    with QrCT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'SELECTED=1';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MAVT').AsString,
                FieldByName('SOLUONG').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;

    if mExPath = '' then
        s := isGetSaveFileName('CSV')
    else
        s := mExPath + 'Label.csv';

	if s = '' then
    	Exit;

    DataMain.LabelClose(s,
    	GetSysParam('STAMP_CSV_HEADER'),
        GetSysParam('STAMP_OVERWRITE'));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdIntemExecute(Sender: TObject);
var
    ngay: TDateTime;
    s: String;
begin
	DataMain.LabelOpen;
	ngay := Date;

    with QrCT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'SELECTED>0';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MAVT').AsString,
                FieldByName('SOLUONG').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;


    DataMain.LabelClose2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdMavach2Execute(Sender: TObject);
var
    n: Integer;
    ngay: TDateTime;
    s, filename: String;
begin
	DataMain.LabelOpen;
    ngay := Date;

    with QrCT do
    begin
        CheckBrowseMode;
    	DisableControls;
        Filter := 'SELECTED=1';
        First;
        Screen.Cursor := crSQLWait;

        while not Eof do
        begin
        	DataMain.LabelAdd(
            	FieldByName('MAVT').AsString,
                FieldByName('SOLUONG').AsInteger,
                ngay);
            Next;
        end;

        Filter := '';
        First;
        CancelBatch;

        Screen.Cursor := crDefault;
    	EnableControls;
    end;

    n := (sender as TComponent).Tag;
    case n of
        1: filename := RP_LABEL_35x22x3;
        2: filename := RP_LABEL_35x15x3;
        3: filename := RP_LABEL_25x15x4;
        4: filename := RP_LABEL_20x10x2
    else
        filename := RP_LABEL_35x25x3
    end;

    DataMain.LabelCloseFR(filename);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdRefreshExecute(Sender: TObject);
begin
    with QrCT do
    begin
        if Active then
            Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_STAMPS = '%d tem';
    RS_STAMPS_PAGE = '%d tem = %d trang %d tem';

procedure TFrmInlabelChungtu.CmdSumExecute(Sender: TObject);
var
	n: Integer;
	bm: TBytes;
begin
	with QrCT do
    begin
    	CheckBrowseMode;
    	bm := BookMark;
        DisableControls;
        Filter := 'SELECTED<>0';
        First;

        n := 0;
        while not Eof do
        begin
			n := n + FieldByName('SOLUONG').AsInteger;
        	Next;
        end;

        Filter := '';
        First;
        BookMark := bm;
        EnableControls;
    end;

    if mStamp = 0 then
	    Status.Panels[2].Text := Format(RS_STAMPS, [n])
    else
	    Status.Panels[2].Text := Format(RS_STAMPS_PAGE,
        	[n, n div mStamp, n mod mStamp]);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.FormResize(Sender: TObject);
begin
	Status.Panels[0].Width := Width - 180 - 300;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.Panels[0].Text := exRecordCount(QrCT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmInlabelChungtu.BtInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

end.
