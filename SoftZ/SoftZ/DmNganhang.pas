﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmNganhang;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ExtCtrls, fcdbtreeview, Grids, ComCtrls,
  AppEvnts, Menus, AdvMenus, ActnList, Db, ADODB, wwDBGrid2,
  wwfltdlg, wwFltDlg2, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, RzPanel, ToolWin;

type
  TFrmDmNganhang = class(TForm)
    QrNganhang: TADOQuery;
    DsNganhang: TDataSource;
    ApplicationEvents1: TApplicationEvents;
    Panel2: TPanel;
    fcDBTreeView1: TfcDBTreeView;
    Bevel1: TBevel;
    QrChinhanh: TADOQuery;
    DsChinhanh: TDataSource;
    Status: TStatusBar;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdPrint: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    CmdFilter: TAction;
    CmdClearFilter: TAction;
    PopDetail: TAdvPopupMenu;
    Lctheomthng1: TMenuItem;
    Lcdliu1: TMenuItem;
    N1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    FilterQuocgia: TwwFilterDialog2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton9: TToolButton;
    ToolButton2: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    RzSizePanel1: TRzSizePanel;
    PgMain: TPageControl;
    TsQuocgia: TTabSheet;
    GrList1: TwwDBGrid2;
    TsTinh: TTabSheet;
    GrList2: TwwDBGrid2;
    CmdReload: TAction;
    FilterTinh: TwwFilterDialog2;
    QrNganhangCREATE_BY: TIntegerField;
    QrNganhangUPDATE_BY: TIntegerField;
    QrNganhangCREATE_DATE: TDateTimeField;
    QrNganhangUPDATE_DATE: TDateTimeField;
    QrChinhanhCREATE_BY: TIntegerField;
    QrChinhanhUPDATE_BY: TIntegerField;
    QrChinhanhCREATE_DATE: TDateTimeField;
    QrChinhanhUPDATE_DATE: TDateTimeField;
    CmdAudit: TAction;
    QrNganhangMANH: TWideStringField;
    QrNganhangTENNH: TWideStringField;
    QrChinhanhMANH_CN: TWideStringField;
    QrChinhanhMANH: TWideStringField;
    QrChinhanhTENNH_CN: TWideStringField;
    QrChinhanhMA: TWideStringField;
    CmdExportDataGrid: TAction;
    N2: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure FormShow(Sender: TObject);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure QrNganhangBeforeInsert(DataSet: TDataSet);
    procedure QrNganhangBeforePost(DataSet: TDataSet);
    procedure QrNganhangBeforeDelete(DataSet: TDataSet);
    procedure OnDBError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure GrList2CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure QrNganhangBeforeEdit(DataSet: TDataSet);
    procedure QrChinhanhAfterInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure GrList1CalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdExportDataGridExecute(Sender: TObject);
  private
    mCanEdit, fixCode1, fixCode2, fixCode3: Boolean;
    mQuery: TADOQuery;
    mDs: TDataSource;
    mFilter: TwwFilterDialog2;
  public
    procedure Execute(r: WORD);
  end;

var
  FrmDmNganhang: TFrmDmNganhang;

implementation

uses
    exCommon, isCommon, isMsg, isDb, MainData, Rights, isLib, RepEngine, OfficeData;

{$R *.DFM}

    (*
    **  Form
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.Execute(r: WORD);
begin
    mCanEdit := rCanEdit(r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
	FORM_CODE1: String = 'DM_NGANHANG';
	FORM_CODE2: String = 'DM_NGANHANG_CHINHANH';

    FIELD_CODE: String = 'MA';

procedure TFrmDmNganhang.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;

//    AddFields(QrNganhang, FORM_CODE1);
//    AddFields(QrChinhanh, FORM_CODE2);

    SetDictionary ([QrNganhang, QrChinhanh],
        [FORM_CODE1, FORM_CODE2],
        [FilterQuocgia, FilterTinh]);
    SetCustomGrid([FORM_CODE1, FORM_CODE2],
        [GrList1, GrList2]);

    fixCode1 := SetCodeLength(FORM_CODE1, QrNganhang.FieldByName('MANH'));
    fixCode2 := SetCodeLength(FORM_CODE2, QrChinhanh.FieldByName(FIELD_CODE));

    mQuery := QrNganhang;
    mDs := DsNganhang;
    mFilter := FilterQuocgia;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.FormShow(Sender: TObject);
begin
    OpenDataSets([QrNganhang, QrChinhanh]);
	GrList1.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrNganhang, QrChinhanh]);
    finally
    end;
    Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.FormCloseQuery(Sender: TObject;
  var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataset(mQuery, True);
end;

    (*
    **  PageTab
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
	    begin
    	    mQuery := QrNganhang;
            mDs := DsNganhang;
            mFilter := FilterQuocgia;
            GrList1.SetFocus;
	    end;
    1:
	    begin
    	    mQuery := QrChinhanh;
            mDs := DsChinhanh;
            mFilter := FilterTinh;
            GrList2.SetFocus;
	    end;
    end;

   	HideAudit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := CheckBrowseDataSet(mQuery, True)
end;

    (*
    **  Command
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdNewExecute(Sender: TObject);
begin
    mQuery.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdSaveExecute(Sender: TObject);
begin
    mQuery.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdCancelExecute(Sender: TObject);
begin
    mQuery.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdDelExecute(Sender: TObject);
begin
    mQuery.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdExportDataGridExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
        0: DataOffice.ExportDataGrid(GrList1);
        1: DataOffice.ExportDataGrid(GrList2);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdCloseExecute(Sender: TObject);
begin
    Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdSearchExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
        exSearch(Name + '0', DsNganhang);
    1:
        exSearch(Name + '1', DsChinhanh);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE1, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdFilterExecute(Sender: TObject);
begin
    mFilter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdClearFilterExecute(Sender: TObject);
begin
    with mFilter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdReloadExecute(Sender: TObject);
begin
    QrNganhang.Requery;
    QrChinhanh.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, mQuery, mFilter, mCanEdit);
end;

    (*
    **  Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.QrNganhangBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    case PgMain.ActivePageIndex of
    1:
        if QrNganhang.FieldByName('MANH').AsString = '' then
            Abort;
    2:
        if QrChinhanh.FieldByName('MA').AsString = '' then
            Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.QrNganhangBeforeEdit(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.QrNganhangBeforePost(DataSet: TDataSet);
var
    s: String;
begin
    case PgMain.ActivePageIndex of
    0:
    	with QrNganhang do
    	begin
            if BlankConfirm(QrNganhang, ['MANH']) then
                Abort;

            if fixCode1 then
		        if LengthConfirm(QrNganhang, ['MANH']) then
        	        Abort;

            if BlankConfirm(QrNganhang, ['TENNH']) then
                Abort;
        end;
    1:
    	with QrChinhanh do
    	begin
            if BlankConfirm(QrChinhanh, [FIELD_CODE]) then
                Abort;

            if fixCode2 then
		        if LengthConfirm(QrChinhanh, [FIELD_CODE]) then
        	        Abort;

            if BlankConfirm(QrChinhanh, ['TENNH_CN']) then
                Abort;

            s := QrNganhang.FieldByName('MANH').AsString;
			FieldByName('MANH').AsString := s;
            FieldByName('MANH_CN').AsString := s + '.' + FieldByName('MA').AsString;
        end;
    end;
    SetAudit(mQuery);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.QrNganhangBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
    	Abort;

    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.OnDBError(DataSet: TDataSet;
  E: EDatabaseError; var Action: TDataAction);
begin
    Action := DbeMsg;
end;

    (*
    **  Other
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(mQuery, mFilter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.GrList1CalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if SameText(Field.FullName, 'MANH') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.GrList2CalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if SameText(Field.FullName, 'MA') then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.QrChinhanhAfterInsert(DataSet: TDataSet);
begin
    DataSet.FieldByName('MA').Clear
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmNganhang.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, mDs);
end;

end.
