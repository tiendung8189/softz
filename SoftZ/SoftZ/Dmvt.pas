﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmvt;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Graphics,
  ComCtrls, ActnList, Wwdbigrd, Wwdbgrid2, ExtCtrls,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AppEvnts, AdvMenus, isPanel,
  wwfltdlg, RzSplit, fctreecombo, fcTreeView, wwDialog, Mask, RzPanel, fcCombo,
  Grids, Wwdbgrid, ToolWin, wwdbdatetimepicker, IniFiles, RzLaunch, Buttons,
  AdvEdit, DBAdvEd, wwcheckbox, rDBComponents, DBCtrlsEh, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2;

type
  TFrmDmvt = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVT: TADOQuery;
    DsDMVT: TDataSource;
    CmdRefresh: TAction;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVTMAVT: TWideStringField;
    QrDMVTMANHOM: TWideStringField;
    QrDMVTTENVT: TWideStringField;
    QrDMVTDVT: TWideStringField;
    QrDMVTQD1: TIntegerField;
    QrDMVTQUAYKE: TWideStringField;
    QrDMVTGHICHU: TWideMemoField;
    QrDMVTSTAMP: TIntegerField;
    QrDMVTCREATE_BY: TIntegerField;
    QrDMVTUPDATE_BY: TIntegerField;
    QrDMVTCREATE_DATE: TDateTimeField;
    QrDMVTUPDATE_DATE: TDateTimeField;
    QrDMVTUPDATE_NAME: TWideStringField;
    QrDM_QUAYKE: TADOQuery;
    ItmNCC: TMenuItem;
    ItmNHOM: TMenuItem;
    N1: TMenuItem;
    CmdPhoto: TAction;
    N2: TMenuItem;
    Xemhnh1: TMenuItem;
    QrDMVTTENTAT: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    N4: TMenuItem;
    QrDMVTTON_MIN: TFloatField;
    PopIn: TAdvPopupMenu;
    PopMain: TAdvPopupMenu;
    CmdReRead: TAction;
    CmdChangeGroup: TAction;
    N5: TMenuItem;
    ingnhnhm1: TMenuItem;
    Xl1: TMenuItem;
    QrDMVTMAMAU: TWideStringField;
    QrDMVTMASIZE: TWideStringField;
    DMVT_TINHLAI: TADOCommand;
    QrDMVTTINHTRANG: TWideStringField;
    QrDMVTPLU: TWideStringField;
    QrDMVTPLU_KEY: TIntegerField;
    ALLOC_PLU: TADOCommand;
    QrDMVTIS_PLU: TBooleanField;
    CmdExport: TAction;
    BtExport: TToolButton;
    ToolButton12: TToolButton;
    DMVT_EXPORT: TADOStoredProc;
    PaList: TPanel;
    GrList: TwwDBGrid2;
    PaNhom: TisPanel;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD3: TisPanel;
    PD1: TisPanel;
    PD2: TisPanel;
    PD5: TisPanel;
    EdGHICHU: TDBMemo;
    PD6: TisPanel;
    TntLabel4: TLabel;
    TntLabel5: TLabel;
    DBText1: TDBText;
    DBText2: TDBText;
    CbNhomhang: TfcTreeCombo;
    QrDMVTMANGANH: TWideStringField;
    QrDMVTMANHOM2: TWideStringField;
    QrDMVTMANHOM3: TWideStringField;
    QrDMVTMANHOM4: TWideStringField;
    CmdSwitch: TAction;
    QrDMVTBO: TBooleanField;
    QrDMVTLOAITHUE: TWideStringField;
    QrDMVTLK_VAT_VAO: TFloatField;
    QrDMVTLK_VAT_RA: TFloatField;
    LbTLLAI2: TLabel;
    CmdPLU: TAction;
    QrDMVTIS_PLU2: TBooleanField;
    CmdAudit: TAction;
    CmdTimBarcode: TAction;
    N3: TMenuItem;
    mtheobarcode1: TMenuItem;
    QrDMVTCAL_TONMIN_BOX: TFloatField;
    DMVT_EXPORT2: TADOStoredProc;
    QrDMVTCAL_TONMAX_BOX: TFloatField;
    QrDMVTTON_MAX: TFloatField;
    QrDMVTNAMXB: TIntegerField;
    QrDMVTTRONGLUONG: TFloatField;
    QrDMVTSOTRANG: TIntegerField;
    QrDMVTDAI: TFloatField;
    QrDMVTRONG: TFloatField;
    QrDMVTMATG: TWideStringField;
    QrDMVTMANXB: TWideStringField;
    QrDMVTLAN_TAIBAN: TIntegerField;
    QrDMVTCAO: TFloatField;
    QrDMVTKHAC_NCC: TBooleanField;
    CmdImport: TAction;
    btnImport: TToolButton;
    ToolButton13: TToolButton;
    CmdExport2: TAction;
    RzLauncher: TRzLauncher;
    PopEx: TAdvPopupMenu;
    Export1: TMenuItem;
    N7: TMenuItem;
    Xutdliuchothitbkimkho1: TMenuItem;
    CmdTimBarcode2: TAction;
    CmdDmdvt: TAction;
    CmdDmvtCt: TAction;
    ToolButton8: TToolButton;
    ToolButton14: TToolButton;
    ToolButton15: TToolButton;
    QrDMVTMADT: TWideStringField;
    QrDMVTLK_TENDT: TWideStringField;
    N6: TMenuItem;
    PopTinhtrang1: TMenuItem;
    PD7: TisPanel;
    Label31: TLabel;
    PD8: TisPanel;
    Label39: TLabel;
    QrDMVTVAT_VAO: TFloatField;
    QrDMVTVAT_RA: TFloatField;
    QrDMVTNL: TBooleanField;
    QrDMVTVL: TBooleanField;
    QrDMVTCB: TBooleanField;
    QrDMVTLK_TENNGANH: TWideStringField;
    QrDMVTLK_TENNHOM: TWideStringField;
    QrDMVTGIAVON: TFloatField;
    CmdEdit: TAction;
    ToolButton6: TToolButton;
    ToolButton16: TToolButton;
    PopTinhtrang: TMenuItem;
    PopTinhtrang2: TMenuItem;
    QrDMVTTENVT_KHONGDAU: TWideStringField;
    QrDMVTCAMUNG_NHOM: TWideStringField;
    LbThuoctinh: TLabel;
    LbTTSach: TLabel;
    QrDMVTLK_TENMAU: TWideStringField;
    QrDMVTLK_TENSIZE: TWideStringField;
    QrDMVTLK_TENNXB: TWideStringField;
    QrDMVTLK_TENTG: TWideStringField;
    CmdUpdateTinhtrang: TAction;
    N8: TMenuItem;
    CpnhtTnhtrngtfileexcel1: TMenuItem;
    Panel3: TPanel;
    Label5: TLabel;
    EdSearch: TAdvEdit;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    N10: TMenuItem;
    LbX: TLabel;
    LbQD: TLabel;
    PD0: TisPanel;
    QrNHOM: TADOQuery;
    QrNGANH: TADOQuery;
    QrDMVTTyLeLai: TFloatField;
    QrDMVTTyLeLaiSi: TFloatField;
    QrDMVTGiaBan: TFloatField;
    QrDMVTGiaNhap: TFloatField;
    QrDMVTGiaSi: TFloatField;
    ChbKHAC_NCC: TrDBCheckBox;
    EdMA: TDBEditEh;
    EdTen: TDBEditEh;
    DBEditEh1: TDBEditEh;
    CbbCamUng: TDbLookupComboboxEh2;
    EdMADT: TDBEditEh;
    CbMADT: TDbLookupComboboxEh2;
    CbQD1: TDBNumberEditEh;
    CbDVT: TDbLookupComboboxEh2;
    CbbDvtHop: TDbLookupComboboxEh2;
    cbbTINHTRANG: TDbLookupComboboxEh2;
    cbbQUAYKE: TDbLookupComboboxEh2;
    DsDM_QUAYKE: TDataSource;
    EdDVT1_2: TDBEditEh;
    EdDVT1_3: TDBEditEh;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    CbMANGANH: TDbLookupComboboxEh2;
    DBEditEh4: TDBEditEh;
    DsNGANH: TDataSource;
    DsNHOM: TDataSource;
    CbMANHOM: TDbLookupComboboxEh2;
    DBEditEh2: TDBEditEh;
    EdVAT_VAO: TDBNumberEditEh;
    CbLoaithue: TDbLookupComboboxEh2;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    EdGianhap: TDBNumberEditEh;
    EdGianhapVat: TDBNumberEditEh;
    DBNumberEditEh5: TDBNumberEditEh;
    EdGiasi: TDBNumberEditEh;
    EdGiaSiVat: TDBNumberEditEh;
    EdTL_LAI: TDBNumberEditEh;
    EdGIABANLE: TDBNumberEditEh;
    QrDMVTDvtHop: TWideStringField;
    QrDMVTGiaNhapChuaThue: TFloatField;
    QrDMVTGiaBanChuaThue: TFloatField;
    QrDMVTGiaSiChuaThue: TFloatField;
    QrDMVTTyLeCKNCC: TFloatField;
    QrDMVTCamUng_SoLan: TIntegerField;
    QrDMVTGiaBQ: TFloatField;
    PopImport: TAdvPopupMenu;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    CmdImportExcel: TAction;
    CmdSampleExcel: TAction;
    spIMP_DM_HH_DeleteList: TADOCommand;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVTAfterInsert(DataSet: TDataSet);
    procedure QrDMVTBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVTBeforeInsert(DataSet: TDataSet);
    procedure CmdPhotoExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDMVTAfterPost(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdChangeGroupExecute(Sender: TObject);
    procedure CbMANGANHNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrDMVTTENVTChange(Sender: TField);
    procedure QrDMVTGIANHAPChange(Sender: TField);
    procedure QrDMVTMAVTChange(Sender: TField);
    procedure CmdExportExecute(Sender: TObject);
    procedure ItmNCCClick(Sender: TObject);
    procedure CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomhangExit(Sender: TObject);
    procedure CbNhomhangKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure QrDMVTLOAITHUEChange(Sender: TField);
    procedure CmdPLUExecute(Sender: TObject);
    procedure CbNhomhangCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure BtnInClick(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdTimBarcodeExecute(Sender: TObject);
    procedure QrDMVTTON_MINChange(Sender: TField);
    procedure CmdImportExecute(Sender: TObject);
    procedure CmdExport2Execute(Sender: TObject);
    procedure CmdTimBarcode2Execute(Sender: TObject);
    procedure CmdDmdvtExecute(Sender: TObject);
    procedure CmdDmvtCtExecute(Sender: TObject);
    procedure QrDMVTMADTChange(Sender: TField);
    procedure PopTinhtrang1Click(Sender: TObject);
    procedure PopMainPopup(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrDMVTQD1Validate(Sender: TField);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDMVTDVTChange(Sender: TField);
    procedure LbThuoctinhClick(Sender: TObject);
    procedure LbTTSachClick(Sender: TObject);
    procedure CmdUpdateTinhtrangExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVTMAVTValidate(Sender: TField);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrDMVTMANGANHChange(Sender: TField);
    procedure CbMANHOMDropDown(Sender: TObject);
    procedure CmdImportExcelExecute(Sender: TObject);
    procedure CmdSampleExcelExecute(Sender: TObject);
    procedure btnImportClick(Sender: TObject);
    function DeleteAllRecordByTransNo(pTransNo: Integer; pIsError: Boolean = false): Boolean;
  private
  	mCanEdit, mEANConfirm, mPLU: Boolean;
    mGia: Double;
    mObsolete, defDvt : Integer;
    mRet, bUnitLevel, m1Ncc : Boolean;

    sCodeLen, defTinhtrang, defThue, defDvtText: String;
  	mSQL, mNganh, mNhom, mNhom2, mNhom3, mNhom4, mPrefix, mSearch: String;

    function  AllocPLU(nhom: String): String;
  public
  	function Execute(r: WORD; closeDs : Boolean = True) : Boolean;
  end;

var
  FrmDmvt: TFrmDmvt;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, Scan, Variants,
    CayNganhNhom, isBarcode, isLib, isFile, TimBarcode, ExcelData, isCommon,
    TimBarcode2, Dmkhac, DmvtCT, DmvtThuoctinh, DmvtTTSach, OfficeData, DmHotro,
    GuidEx, DmvtDanhSachImport;

{$R *.DFM}

const
	FORM_CODE = 'DM_HH';

(*==============================================================================
**------------------------------------------------------------------------------
*)
Function  TFrmDmvt.Execute;
begin
	mCanEdit := rCanEdit(r);
    mRet := False;
    ShowModal;

    if closeDs then
        CloseDataSets(DataMain.Conn)
    else
    begin
        CloseDataSets([QrDM_QUAYKE]);
        with DataMain do
        CloseDataSets([QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG]);
    end;
    Result := mRet;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvt.AllocPLU;
begin
    with ALLOC_PLU do
    begin
    	Prepared := True;
        Parameters.ParamByName('@MANHOM').Value := nhom;
        Execute;
        Result := Parameters.ParamValues['@PLU'];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormCreate(Sender: TObject);
var
	bLe, bSi, b1NccCheckbox, bImport, bExport: Boolean;
    x: Integer;
begin
    TMyForm(Self).Init2;

    with QrDMVT do
    begin
	    SetDisplayFormat(QrDMVT, sysCurFmt);
    	SetDisplayFormat(QrDMVT, ['TON_MIN', 'TON_MAX'], sysQtyFmt);
	    SetDisplayFormat(QrDMVT, ['TyLeLai', 'TyLeLaiSi', 'TyleCKNCC'], sysPerFmt);
        SetDisplayFormat(QrDMVT, ['VAT_VAO', 'VAT_RA'], sysTaxFmt);
    	SetShortDateFormat(QrDMVT);
    end;

    // Tuy bien luoi
	SetCustomGrid([FORM_CODE], [GrList]);
    SetDictionary(QrDMVT, FORM_CODE, Filter);

    // Don vi tinh quy doi
	bUnitLevel := FlexConfigInteger(FORM_CODE, 'Unit Level') <> 0;

    LbX.Visible := bUnitLevel;
    LbQD.Visible := bUnitLevel;
    CbQD1.Visible := bUnitLevel;
    CbbDvtHop.Visible := bUnitLevel;

    // Flex
	mPrefix := FlexConfigString(FORM_CODE, 'Barcode Prefix');
    mEANConfirm := FlexConfigBool(FORM_CODE,'EAN Confirm');
	sCodeLen := FlexConfigString(FORM_CODE, 'Code Length');
    mPLU := FlexConfigBool(FORM_CODE, 'PLU');
    bImport := FlexConfigBool(FORM_CODE, 'Import Button');
    bExport := FlexConfigBool(FORM_CODE, 'Export Button');

    CmdImport.Visible := bImport;
    btnImport.Visible := bImport;
    ToolButton15.Visible := bImport;

    CmdExport.Visible := bExport;
    BtExport.Visible := bExport;
    ToolButton13.Visible := bExport;

    m1Ncc := FlexConfigBool(FORM_CODE, '1 nha cung cap');
    b1NccCheckbox := FlexConfigBool(FORM_CODE, '1 nha cung cap Visible Checkbox');
    ChbKHAC_NCC.Visible := b1NccCheckbox;
    ItmNCC.Visible := m1Ncc;
    N1.Visible := m1Ncc;

    LbTTSach.Visible := FlexConfigBool(FORM_CODE, 'Thong tin sach');

    PD8.Visible := FlexConfigBool(FORM_CODE, 'Gia si');
    PD3.Visible := FlexConfigBool(FORM_CODE, 'Gia le');

    LbThuoctinh.Visible := FlexConfigBool(FORM_CODE, 'Thuoc tinh');

    CmdDmvtCt.Visible := FlexConfigBool(FORM_CODE, 'Chi tiet');
    ToolButton12.Visible := CmdDmvtCt.Visible;

    // Default unit
	defDvt := GetSysParam('DEFAULT_DVT');
    defThue := GetSysParam('DEFAULT_LOAITHUE');


    // Initial
    mTrigger := False;
    mGia := 0;
    mNhom := '@';
	mSQL := QrDMVT.SQL.Text;
   	FrmScan := Nil;

    CmdPLU.Visible := mPLU;

    mObsolete     := 0;

    // Tree Combo
    FlexGroupCombo(CbNhomhang);

    // Thue
    if not sysIsThue then
    begin
        PD2.Visible := False;

        EdGianhap.ControlLabel.Caption := 'Giá nhập';
        EdGianhapVat.Visible := False;

        EdGiasi.ControlLabel.Caption := 'Giá bán sỉ';
        EdGiaSiVat.Visible := False;

        EdGIABANLE.ControlLabel.Caption := 'Giá bán lẻ';

//        grRemoveFields(GrList, ['VAT_VAO', 'VAT_RA', 'GIANHAP'])
    end;

    // Panels
    PD2.Collapsed := RegReadBool('PD2');
    PD3.Collapsed := RegReadBool('PD3');
    PD5.Collapsed := RegReadBool('PD5');
    PD6.Collapsed := RegReadBool('PD6');
    PD7.Collapsed := RegReadBool('PD7');
    PD8.Collapsed := RegReadBool('PD8');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormShow(Sender: TObject);
begin
//	DsDMVT.AutoEdit := mCanEdit;
    DsDMVT.AutoEdit := False;
    PaDetail.VertScrollBar.Position := 0;

    // Open database
    QrDM_QUAYKE.Open;
    with DataMain do
    begin
    	OpenDataSets([QrDMNCC, QrDM_DVT, QrMAU, QrSIZE, QrDMVT_TINHTRANG,
            QrDMTACGIA, QrDMNXB, QrDMLOAITHUE, QrDM_CAMUNG]);
        defTinhtrang := QrDMVT_TINHTRANG.FieldByName('MA_HOTRO').AsString;
    end;
    OpenDataSets([QrNGANH, QrNHOM]);

    if (defDvt > 0) then
    begin
        with DataMain.QrDM_DVT do
        if Locate('MA', defDvt, []) then
        begin
             defDvtText := FieldByName('DGIAI').AsString;
        end;
    end;

    // Loading
	CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDMVT do
    begin
        if FieldByName('TINHTRANG').AsString <> '01' then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	if QrDMVT.Active then
		CanClose := CheckBrowseDataset(QrDMVT, True)
    else
		CanClose := True
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
	if FrmScan <> Nil then
    	FrmScan.Close;

    RegWrite(Name, ['PD2', 'PD3', 'PD5', 'PD6', 'PD7', 'PD8'],
    	[PD2.Collapsed, PD3.Collapsed, PD5.Collapsed, PD6.Collapsed,
            PD7.Collapsed, PD8.Collapsed]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdNewExecute(Sender: TObject);
begin
	QrDMVT.Append;
    PD0.Enabled := True;
    CbMANGANH.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSampleExcelExecute(Sender: TObject);
begin
    openImportFile(Handle, 'IMP_DM_HANGHOA_EXCEL');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSaveExecute(Sender: TObject);
begin
    QrDMVT.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdCancelExecute(Sender: TObject);
begin
	QrDMVT.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDelExecute(Sender: TObject);
begin
    if DataMain.BarcodeIsUsed(QrDMVT.FieldByName('MAVT').AsString, True) then
        Abort;
	QrDMVT.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDmdvtExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_PUB_DM_QUYUOC');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmHotro, FrmDmHotro);
    FrmDmHotro.Execute(r, 0, 'DVT');

    DataMain.QrDM_DVT.ReQuery();
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdDmvtCtExecute(Sender: TObject);
begin
    with QrDMVT do
    begin
        Application.CreateForm(TFrmDmvtCT, FrmDmvtCT);
        FrmDmvtCT.Execute(mCanEdit,
            FieldByName('MAVT').AsString, FieldByName('TENVT').AsString,
            FieldByName('MANHOM').AsString, mPrefix, FieldByName('VAT_RA').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ItmNCCClick(Sender: TObject);
var
    n: Integer;
    REP_NAME: String;
begin
    n := (Sender as TComponent).Tag;
    if n = 1 then
        REP_NAME := FORM_CODE + '_DT'
    else
        REP_NAME := FORM_CODE + '_NN';

	ShowReport(Caption, REP_NAME,
    	[sysLogonUID, mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.LbTTSachClick(Sender: TObject);
var
	p: TPoint;
    s: String;
begin
	// Field list
   	s := FlexConfigString(FORM_CODE, 'Thong tin sach Fields');
    if s = '' then
    	Exit;

	p.x := RzSizePanel1.Left;
    p.y := RzSizePanel1.Top + PD1.Height;
    p := ClientToScreen(p);

	Application.CreateForm(TFrmDmvtTTSach, FrmDmvtTTSach);
    with FrmDmvtTTSach do
    begin
    	Left := p.x - 2;
        Top  := p.y + 2;
    	Execute(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.LbThuoctinhClick(Sender: TObject);
var
	p: TPoint;
    s: String;
begin
	// Field list
   	s := FlexConfigString(FORM_CODE, 'Thuoc tinh Fields');
    if s = '' then
    	Exit;

	p.x := RzSizePanel1.Left;
    p.y := RzSizePanel1.Top + PD1.Height;
    p := ClientToScreen(p);

	Application.CreateForm(TFrmDmvtThuoctinh, FrmDmvtThuoctinh);
    with FrmDmvtThuoctinh do
    begin
    	Left := p.x - 2;
        Top  := p.y + 2;
    	Execute(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPrintExecute(Sender: TObject);
begin
   BtnIn.CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bHasRec, b, bInsert: Boolean;
begin
	with QrDMVT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bHasRec := not IsEmpty;
        bInsert := State in [dsInsert];
	end;

    GrList.Enabled := bBrowse;
    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdEdit.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and bHasRec;
    CmdReRead.Enabled := bBrowse;

    if FrmScan = Nil then
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec;
        CmdFilter.Enabled := True;
        CmdSearch.Enabled := True;
    end
    else
    begin
	    CmdPhoto.Enabled := mCanEdit and bHasRec and (not FrmScan.Visible);
        CmdFilter.Enabled := not FrmScan.Visible;
        CmdSearch.Enabled := not FrmScan.Visible;
    end;

    CmdSearch.Enabled := CmdSearch.Enabled and bBrowse;
    CmdFilter.Enabled := CmdFilter.Enabled and bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdChangeGroup.Enabled := mCanEdit and bBrowse and bHasRec;

    CmdDmvtCt.Enabled := bBrowse;

    PD0.Enabled := bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.PopMainPopup(Sender: TObject);
begin
    PopTinhtrang.Checked := mObsolete = 0;
    PopTinhtrang1.Checked := mObsolete = 1;
    PopTinhtrang2.Checked := mObsolete = 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.PopTinhtrang1Click(Sender: TObject);
begin
    mObsolete :=  (Sender as TComponent).Tag;
    mNganh := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdRefreshExecute(Sender: TObject);
var
    fLevel: Integer;
	sSql, sNganh, sNhom, sNhom2, sNhom3, sNhom4, s1 : String;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
    	with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
                sNganh := StringData;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;

   	if  (mNganh <> sNganh) or
        (mNhom  <> sNhom) or
        (mNhom2 <> sNhom2) or
        (mNhom3 <> sNhom3) or
        (mNhom4 <> sNhom4) or
        (EdSearch.Text <> mSearch) then
	begin
        mNganh := sNganh;
        mNhom  := sNhom;
        mNhom2 := sNhom2;
        mNhom3 := sNhom3;
        mNhom4 := sNhom4;
        mSearch := EdSearch.Text;

        with QrDMVT do
        begin
			Close;
            sSQL := mSQL;

            // Nganh, Nhom
            case fLevel of
            0:  // Nganh
                sSQL := sSQL + Format(' and MANGANH=''%s''', [mNganh]);
            1:  // Nhom
                sSQL := sSQL + Format(' and MANGANH=''%s'' and MANHOM=''%s''', [mNganh, mNhom]);
            2:  // Nhom 2
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s''', [mNganh, mNhom, mNhom2]);
            3:  // Nhom 3
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3]);
            4:  // Nhom 4
                sSQL := sSQL + Format(
                    ' and MANGANH=''%s'' and MANHOM=''%s'' and MANHOM2=''%s'' and MANHOM3=''%s'' and MANHOM4=''%s''',
                        [mNganh, mNhom, mNhom2, mNhom3, mNhom4]);
            end;

            case mObsolete of
            0:
                sSQL := sSQL + ' and TINHTRANG in (''01'')';
            1:
                sSQL := sSQL + ' and TINHTRANG in (''02'')';
            end;

            if mSearch <> '' then
            begin
                s1 := DataMain.StripToneMark(mSearch);
                sSQL := sSQL + ' and (' +
                            'dbo.fnStripToneMark([MAVT]) like ''%' + s1 + '%'''
                            + ' or dbo.fnStripToneMark([TENVT]) like ''%' + s1 + '%'''
                            + ')';
            end;

            sSQL := sSQL + ' order by	MANGANH, MANHOM';
            SQL.Text := sSQL;
            Open;
            First;
        end;

//        with Filter do
//        begin
//            RefreshOriginalSQL; //NTD
//            ApplyFilter;
//        end;
		GrList.SetFocus;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTAfterInsert(DataSet: TDataSet);
begin
	with QrDMVT do
    begin
    	// Set default
		FieldByName('MANGANH').AsString := mNganh;
		FieldByName('MANHOM').AsString  := mNhom;
		FieldByName('MANHOM2').AsString := mNhom2;
		FieldByName('MANHOM3').AsString := mNhom3;
		FieldByName('MANHOM4').AsString := mNhom4;
        FieldByName('LOAITHUE').AsString := defThue;
        FieldByName('QD1').AsInteger := 1;
        FieldByName('DVT').AsString := defDvtText;
		FieldByName('TINHTRANG').AsString := defTinhtrang;
		FieldByName('BO').AsBoolean := False;
        FieldByName('KHAC_NCC').AsBoolean := m1Ncc;
        FieldByName('NL').AsBoolean := False;
        FieldByName('VL').AsBoolean := False;
        FieldByName('CB').AsBoolean := False;
		FieldByName('MAVT').AsString := DataMain.AllocEAN(mPrefix, '');

        if FieldByName('IS_PLU').AsBoolean or
        	FieldByName('IS_PLU2').AsBoolean then
			FieldByName('PLU').AsString := AllocPLU(mNhom);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_LENCODE = 'Chiều dài mã không hợp lệ.';

procedure TFrmDmvt.QrDMVTBeforePost(DataSet: TDataSet);
var
	s: String;
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['MANGANH', 'MANHOM', 'TENVT', 'DVT', 'MADT', 'LOAITHUE']) then
			Abort;

        if FieldByName('DvtHop').AsString = '' then
        begin
           FieldByName('DvtHop').AsString := FieldByName('DVT').AsString ;
           FieldByName('QD1').Value := 1;
        end;

        if sCodeLen <> '' then
        begin
			s := IntToStr(Length(Trim(FieldByName('MAVT').AsString)));
    	    if Pos(';' + s + ';', ';' + sCodeLen) <= 0 then
        	begin
        		Msg(RS_INVALID_LENCODE);
	            Abort;
    	    end;
        end;

        if sysTinhlai = 1 then
        begin
            if (FieldByName('TyLeLai').AsFloat > 100) or (FieldByName('TyLeLaiSi').AsFloat > 100) then
            begin
                ErrMsg(RS_INVALID_INTEREST);
                Abort;
            end;
        end;

	    SetNull(DataSet, ['MAMAU' , 'MASIZE', 'MANHOM2', 'MANHOM3', 'MANHOM4']);
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTGIANHAPChange(Sender: TField);
var
	mGiavon, mGianhap, mLai, mGiaban, mGiabanvat, mGiasi, mGiasivat, mLaisi, mGianhapvat,
    mTL_CK_NCC: Double;
    bm: TBytes;
    mLoaithue: String;
begin
	if mTrigger then
    	Exit;

	with QrDmvt do
    begin
            mGiavon := FieldByName('GIAVON').AsFloat;
            mTL_CK_NCC := FieldByName('TyleCKNCC').AsFloat;
		mGianhap := FieldByName('GiaNhapChuaThue').AsFloat;
        mGianhapvat := FieldByName('GiaNhap').AsFloat;
        mLai := FieldByName('TyLeLai').AsFloat;
        mLaisi := FieldByName('TyLeLaiSi').AsFloat;
        mGiaban := FieldByName('GiaBanChuaThue').AsFloat;
        mGiabanvat := FieldByName('GiaBan').AsFloat;
        mGiasi := FieldByName('GiaSiChuaThue').AsFloat;
        mGiasivat := FieldByName('GiaSi').AsFloat;
        mLoaithue := FieldByName('LOAITHUE').AsString;
    end;

    with DMVT_TINHLAI do
    begin
    	Parameters.ParamByName('@LoaiThue').Value := mLoaithue;
        Parameters.ParamByName('@Sender').Value := Sender.FieldName;
    	Parameters.ParamByName('@GiaNhapChuaThue').Value := mGianhap;
        Parameters.ParamByName('@GiaNhap').Value := mGianhapvat;
    	Parameters.ParamByName('@TyLeLaiSi').Value := mLaisi;
    	Parameters.ParamByName('@GiaSiChuaThue').Value := mGiasi;
    	Parameters.ParamByName('@GiaSi').Value := mGiasivat;
    	Parameters.ParamByName('@TyLeLai').Value := mLai;
        Parameters.ParamByName('@GiaBanChuaThue').Value := mGiaban;
        Parameters.ParamByName('@GiaBan').Value := mGiabanvat;
        Parameters.ParamByName('@TyleCKNCC').Value := mTL_CK_NCC;
        Parameters.ParamByName('@GiaVon').Value := mGiavon;
        Execute;
    	mGianhap := Parameters.ParamValues['@GiaNhapChuaThue'];
        mGianhapvat := Parameters.ParamValues['@GiaNhap'];
    	mLaisi := Parameters.ParamValues['@TyLeLaiSi'];
        mGiasi := Parameters.ParamValues['@GiaSiChuaThue'];
    	mGiasivat := Parameters.ParamValues['@GiaSi'];
    	mLai := Parameters.ParamValues['@TyLeLai'];
        mGiaban := Parameters.ParamValues['@GiaBanChuaThue'];
        mGiabanvat := Parameters.ParamValues['@GiaBan'];
        mTL_CK_NCC := Parameters.ParamValues['@TyleCKNCC'];
        mGiavon := Parameters.ParamValues['@GiaVon'];
    end;

    mTrigger := True;
	with QrDmvt do
    begin
        FieldByName('GiaNhapChuaThue').AsFloat := mGianhap;
        FieldByName('GiaNhap').AsFloat := mGianhapvat;
        FieldByName('TyLeLai').AsFloat := mLai;
        FieldByName('TyLeLaiSi').AsFloat := mLaisi;
        FieldByName('GiaBanChuaThue').AsFloat := mGiaban;
        FieldByName('GiaBan').AsFloat := mGiabanvat;
        FieldByName('GiaSiChuaThue').AsFloat  := mGiasi;
        FieldByName('GiaSi').AsFloat  := mGiasivat;

        FieldByName('GIAVON').AsFloat := mGiavon;
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;



(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPhotoExecute(Sender: TObject);
begin
 	Application.CreateForm(TFrmScan, FrmScan);
	FrmScan.Execute (DsDMVT, 'MAVT');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NAME_DEF = 'Mặc định tên tắt?';
    
procedure TFrmDmvt.QrDMVTTENVTChange(Sender: TField);
var
	b: Boolean;
begin
	with QrDMVT do
    begin
		b := FieldByName('TENTAT').AsString = '';
    	if not b then
        	b := YesNo(RS_NAME_DEF);
		if b then
			FieldByName('TENTAT').AsString := isLeft(
            	FieldByName('TENVT').AsString, FieldByName('TENTAT').Size);

        FieldByName('TENVT_KHONGDAU').AsString := DataMain.StripToneMark(FieldByName('TENVT').AsString)
	end;
end;

procedure TFrmDmvt.QrDMVTTON_MINChange(Sender: TField);
begin
	with QrDMVT do
    begin
      	if FieldByName('TON_MAX').AsFloat < FieldByName('TON_MIN').AsFloat then
            FieldByName('TON_MAX').AsFloat := FieldByName('TON_MIN').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTLOAITHUEChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('LOAITHUE').AsString = 'TTT') or (FieldByName('LOAITHUE').AsString = 'KCT') then
        begin
            FieldByName('VAT_VAO').Clear;
            FieldByName('VAT_RA').Clear;
        end else
        begin
            FieldByName('VAT_VAO').AsFloat := FieldByName('LK_VAT_VAO').AsFloat;
            FieldByName('VAT_RA').AsFloat := FieldByName('LK_VAT_RA').AsFloat;
        end
    end;
    QrDMVTGIANHAPChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVT, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.btnImportClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTMADTChange(Sender: TField);
begin
    exDotMadt(DataMain.QrDMNCC, Sender);
//    EdTENDT.Text := EdTENDT.Field.AsString;
//    CbMADT.Text := Sender.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_EAN13_CFM = 'Mã không đúng chuẩn EAN13.'#13'Mã EAN13 đúng phải là: ';

procedure TFrmDmvt.QrDMVTMAVTChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;

	// Check EAN code
	if mEANConfirm then
    begin
    	mMa := Sender.AsString;
        n := Length(mMa);
        if n = 13 then
        begin
	        s := Copy(mMa, 1, n - 1);
            s := s + isEAN13Checksum(s);
            if mMa <> s then
                Msg(RS_EAN13_CFM + s);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTMAVTValidate(Sender: TField);
begin
    with QrDMVT do
    if FieldByName('MAVT').OldValue <> null then
        if DataMain.BarcodeIsUsed(FieldByName('MAVT').OldValue, True) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTQD1Validate(Sender: TField);
begin
    with QrDMVT do
    begin
        if State in [dsInsert] then
            Exit;

        if DataMain.BarcodeIsUsed(Sender.AsString) then
            Abort
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTAfterPost(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdClearFilterExecute(Sender: TObject);
begin
	with Filter do
    begin
    	FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdReReadExecute(Sender: TObject);
begin
	mNhom := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CHANGEGROUP_ERR = 'Lỗi trong khi đổi ngành nhóm.';

procedure TFrmDmvt.CmdChangeGroupExecute(Sender: TObject);
var
	b: Boolean;
    nganh, nhom, nhom2, nhom3, nhom4: String;
begin
	// Lay ma nhom moi
    with QrDMVT do
    begin
        nganh := FieldByName('MANGANH').AsString;
        nhom  := FieldByName('MANHOM').AsString;
        nhom2 := FieldByName('MANHOM2').AsString;
        nhom3 := FieldByName('MANHOM3').AsString;
        nhom4 := FieldByName('MANHOM4').AsString;
    end;
	Application.CreateForm(TFrmCayNganhNhom, FrmCayNganhNhom);
    b := FrmCayNganhNhom.Execute(nganh, nhom, nhom2, nhom3, nhom4);
    FrmCayNganhNhom.Free;

    if not b then
    	Exit;

    // Chuyen
    try
        with QrDMVT do
        begin
        	DisableControls;
            Edit;
            FieldByName('MANGANH').AsString  := nganh;
            FieldByName('MANHOM').AsString   := nhom;
            FieldByName('MANHOM2').AsString  := nhom2;
            FieldByName('MANHOM3').AsString  := nhom3;
            FieldByName('MANHOM4').AsString  := nhom4;
            FieldByName('PLU').AsString := AllocPLU(nhom);

            Post;
        	EnableControls;
        end;
        b := True;
	except
        with QrDMVT do
        begin
	    	Cancel;
	       	EnableControls;
        end;
        b := False;
    end;

    if b then
    begin
	    mNhom := '~';
    	CmdRefresh.Execute;
    end
    else
    	ErrMsg(RS_CHANGEGROUP_ERR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdEditExecute(Sender: TObject);
begin
    QrDMVT.Edit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdExport2Execute(Sender: TObject);
var
	s, sNganh, sNhom: String;
    n, fLevel: Integer;
begin
    with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            end;
        end;

    // Export to file
    s := sysTempPath + 'download.txt';

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	Prepared := True;
    	Open;
        TextExport(DMVT_EXPORT, s);
        Close;
    end;

	Cursor := crDefault;
	if not YesNo('Thiết bị phải đang ở tình trạng nhận danh mục. Tiếp tục?', 1) then
    	Exit;

	// Get comport
    with TIniFile.Create(sysAppPath + 'ConfigZ.ini') do
    begin
    	n := ReadInteger('Data Collector', 'ComPort', 1);
        Free;
    end;

    // Run extern command
    Screen.Cursor := crSqlWait;
	with RzLauncher do
    begin
        FileName := sysAppPath + '\System\Dn.exe';
        // [filename],[COM port],[Baud rate],[Interface],[Show dialog]
        Parameters := s + ',' + IntToStr(n) + ',1,2,0';
        Launch;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdExportExecute(Sender: TObject);
var
	s: String;
    sNganh, sNhom, sNhom2, sNhom3, sNhom4: String;
    fLevel: Integer;
begin
    if CbNhomhang.Text = '' then
    	fLevel := -1
    else
    begin
        with CbNhomhang.SelectedNode do
	    begin
            fLevel := Level;
    		case fLevel of
        	0:  // Nganh
            begin
                sNganh := StringData;
            end;
            1:  // Nhom
            begin
                sNhom   := StringData;
                sNganh  := Parent.StringData;
            end;
            2:  // Nhom 2
            begin
                sNhom2  := StringData;
                sNhom   := Parent.StringData;
                sNganh  := Parent.Parent.StringData;
            end;
            3:  // Nhom 3
            begin
                sNhom3  := StringData;
                sNhom2  := Parent.StringData;
                sNhom   := Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.StringData;
            end;
            4:  // Nhom 4
            begin
                sNhom4  := StringData;
                sNhom3  := Parent.StringData;
                sNhom2  := Parent.Parent.StringData;
                sNhom   := Parent.Parent.Parent.StringData;
                sNganh  := Parent.Parent.Parent.Parent.StringData;
            end;
            end;
		end;
    end;
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT2 do
    begin
        Parameters.ParamByName('@BO').Value := 0;  // Bo = 0
        Parameters.ParamByName('@NGANH').Value := sNganh;
        Parameters.ParamByName('@NHOM').Value := sNhom;
        Parameters.ParamByName('@NHOM2').Value := sNhom2;
        Parameters.ParamByName('@NHOM3').Value := sNhom3;
        Parameters.ParamByName('@NHOM4').Value := sNhom4;
        Parameters.ParamByName('@LEVEL').Value := fLevel;

    	ExecProc;
        Active := True;
//        TextExport(DMVT_EXPORT2, s, TEncoding.UTF8);
        TextExport(DMVT_EXPORT2, s);
        Active := False
    end;
	Cursor := crDefault;

    MsgDone();

        {
	s := vlGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	Cursor := crSQLWait;
	with DMVT_EXPORT do
    begin
    	ExecProc;
        Active := True;
        TextExport(DMVT_EXPORT, s);
        Active := False
    end;
	Cursor := crDefault;
    }
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)


procedure TFrmDmvt.CbMANGANHNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
    Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbMANHOMDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDMVT.FieldByName('MANGANH').AsString;
    with QrNHOM do
    begin
        Filter := Format('MANGANH=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangExit(Sender: TObject);
begin
	PaNhom.HeaderCaption := ' .: ' + exGetFlexDesc(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdSwitchExecute(Sender: TObject);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdPLUExecute(Sender: TObject);
begin
	with QrDMVT do
	begin
		if FieldByName('IS_PLU').AsBoolean or FieldByName('IS_PLU2').AsBoolean then
        else	// Khong phai PLU
        	Exit;

    	if IsEmpty or Eof or (State in [dsInsert]) then	// Safety
        	Exit;

		if State in [dsBrowse] then	// Force edit
        	Edit;

        // Alloc
		FieldByName('PLU').AsString := AllocPLU(FieldByName('MANHOM').AsString);
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CbNhomhangCalcNodeAttributes(
  TreeView: TfcCustomTreeView; Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdImportExcelExecute(Sender: TObject);
var
    sFile, mTableNameImport: String;
    b: Boolean;
    rs, mTransNo: Integer;
    r: WORD;
begin
	r := GetRights('SZ_DM_MAVT');
    mTableNameImport := 'IMP_DM_HH';
    mTransNo := DataMain.GetSeqValue(mTableNameImport);

	sFile := isGetOpenFileName('XLSX;XLS');
    if sFile = '' then
        Exit;

    if not DataExcel.ExcelImport('IMP_' + FORM_CODE, sFile, mTableNameImport,
        'spIMP_DM_HH_ImportListExcel;1', 'MAVT',
        [sysLogonUID, mPrefix, TGuidEx.ToString(DataMain.GetNewGuid), FORM_CODE + '_EXCEL', sysAppSource, mTransNo, '', 1], mTransNo, False) then
    begin
        //xóa dữ liệu excel đã tự động insert
        DeleteAllRecordByTransNo(mTransNo);
        mTransNo := 0;
    end;

    if mTransNo <> 0 then
    begin
        Application.CreateForm(TFrmDmvtDanhSachImport, FrmDmvtDanhSachImport);
        if not FrmDmvtDanhSachImport.Execute(r, mTransNo) then
            Exit;

        CmdReRead.Execute;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdImportExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
    rs: Integer;
begin
	sFile := isGetOpenFileName('XLS');
    if sFile = '' then
        Exit;

    rs := YesNoCancel(RS_IM_EXCEL_YESNO);
    if rs = mrCancel then
        Exit;
    b := rs = mrYes;

    DataExcel.ExcelImport('IMP_' + FORM_CODE, sFile, 'IMP_DM_HH',
        'spIMPORT_DM_HH;1', 'MAVT', [sysLogonUID, mPrefix, b], 0);

    CmdReRead.Execute;
    mRet := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdTimBarcode2Execute(Sender: TObject);
begin
    Application.CreateForm(TFrmTimBarcode2, FrmTimBarcode2);
    FrmTimBarcode2.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdTimBarcodeExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmTimBarcode, FrmTimBarcode);
    FrmTimBarcode.Execute(QrDMVT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.CmdUpdateTinhtrangExecute(Sender: TObject);
var
    sFile: String;
    b: Boolean;
begin
	sFile := isGetOpenFileName('XLSX;XLS');
    if sFile = '' then
        Exit;

    DataExcel.ExcelImport('IMP_' + FORM_CODE + '_UPDATE', sFile, 'IMP_DM_HH_UPDATE',
        'spUPDATE_DM_HH;1', 'MAVT', [sysLogonUID], 0);
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTDVTChange(Sender: TField);
begin
    with QrDMVT do
    begin
        if (FieldByName('DvtHop').AsString = '') or (VarIsNull(CbbDvtHop.Value))
            or (CbbDvtHop.Text = '') then
            FieldByName('DvtHop').AsString := FieldByName('DVT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvt.QrDMVTMANGANHChange(Sender: TField);
begin
     QrDMVT.FieldByName('MANHOM').AsString := '';
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmvt.DeleteAllRecordByTransNo;
begin
    with spIMP_DM_HH_DeleteList do
    begin
        Prepared := True;
        Parameters[1].Value := pTransNo;
        Parameters[2].Value := pIsError;
        Execute;
    end;
    Result := True;
end;

end.
