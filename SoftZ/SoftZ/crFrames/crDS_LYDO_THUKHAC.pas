﻿unit crDS_LYDO_THUKHAC;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_LYDO_THUKHAC = class(TCrFrame)
    PaDS_KHO: TPanel;
    Label7: TLabel;
    EdDS: TMemo;
    procedure Label7Click(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_LYDO_THUKHAC: TframeDS_LYDO_THUKHAC;

implementation

{$R *.dfm}

uses
    ChonDsLydoTHUKHAC;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_LYDO_THUKHAC.GetCode: Variant;
begin
    Result := EdDS.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LYDO_THUKHAC.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_LYDO_THUKHAC.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LYDO_THUKHAC.Init;
begin
  inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_LYDO_THUKHAC.Label7Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsLydoTHUKHAC.Get(s) then
    	Exit;

    with EdDS do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
