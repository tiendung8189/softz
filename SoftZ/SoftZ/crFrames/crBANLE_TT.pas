﻿unit crBANLE_TT;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeBANLE_TT = class(TCrFrame)
    PaSILE: TPanel;
    Rb1: TRadioButton;
    Rb2: TRadioButton;
    Rb0: TRadioButton;
  private
  protected
    procedure Init; override;

  public
    function GetCode: Variant; override;
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  frameBANLE_TT: TframeBANLE_TT;

implementation

{$R *.dfm}
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeBANLE_TT.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeBANLE_TT.GetCode: Variant;
begin
    if Rb1.Checked then
        Result := 1
    else if Rb2.Checked then
        Result := 2
    else
        Result := 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeBANLE_TT.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeBANLE_TT.Init;
begin
    inherited;
    Rb0.Checked:= True;
end;

end.
