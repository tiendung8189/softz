inherited frameLOAI_HD: TframeLOAI_HD
  Height = 61
  ExplicitHeight = 61
  object PaLHD: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 61
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label38: TLabel
      Left = 16
      Top = 4
      Width = 73
      Height = 16
      Alignment = taRightJustify
      Caption = 'Lo'#7841'i h'#243'a '#273#417'n'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object CbLHD: TwwDBLookupCombo
      Tag = 7
      Left = 16
      Top = 20
      Width = 101
      Height = 24
      Ctl3D = True
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'5'#9'M'#227#9'F'
        'DGIAI'#9'18'#9'DGIAI'#9'F')
      LookupTable = DataMain.QrLOAI_HD
      LookupField = 'MA'
      Options = [loColLines, loTitles]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbLHDChange
      OnExit = CbLHDChange
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
    object CbTENLHD: TwwDBLookupCombo
      Tag = 8
      Left = 120
      Top = 20
      Width = 261
      Height = 24
      Ctl3D = True
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'DGIAI'#9'18'#9'DGIAI'#9'F'
        'MA'#9'5'#9'M'#227#9'F')
      DataField = 'LOAIHD'
      LookupTable = DataMain.QrLOAI_HD
      LookupField = 'MA'
      Options = [loColLines, loTitles]
      Style = csDropDownList
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbLHDChange
      OnExit = CbLHDChange
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
  end
end
