inherited frameBANLE_TT: TframeBANLE_TT
  Height = 49
  ExplicitHeight = 49
  object PaSILE: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Rb1: TRadioButton
      Left = 35
      Top = 16
      Width = 105
      Height = 17
      Caption = 'Ch'#432'a ho'#224'n t'#7845't'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
    end
    object Rb2: TRadioButton
      Left = 157
      Top = 16
      Width = 103
      Height = 17
      Caption = #272#227' ho'#224'n t'#7845't'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
    end
    object Rb0: TRadioButton
      Left = 279
      Top = 16
      Width = 65
      Height = 17
      Caption = 'C'#7843' hai'
      Checked = True
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      TabStop = True
    end
  end
end
