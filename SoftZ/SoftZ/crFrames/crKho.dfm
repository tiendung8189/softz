inherited frameCrKho: TframeCrKho
  Height = 61
  ExplicitHeight = 61
  object PaKHO: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 61
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label27: TLabel
      Left = 16
      Top = 4
      Width = 21
      Height = 16
      Caption = 'Kho'
    end
    object CbMAKHO: TwwDBLookupCombo
      Tag = 1
      Left = 16
      Top = 24
      Width = 101
      Height = 24
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      CharCase = ecUpperCase
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'10'#9'MAKHO'#9'F'
        'TENKHO'#9'31'#9'TENKHO'#9'F')
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentFont = False
      TabOrder = 0
      AutoDropDown = True
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbMAKHOChange
      OnBeforeDropDown = CbMAKHOBeforeDropDown
      OnExit = CbMAKHOChange
    end
    object CbTENKHO: TwwDBLookupCombo
      Tag = 2
      Left = 120
      Top = 24
      Width = 261
      Height = 24
      TabStop = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      DropDownAlignment = taRightJustify
      Selected.Strings = (
        'TENKHO'#9'31'#9'TENKHO'#9'F'
        'MAKHO'#9'10'#9'MAKHO'#9'F')
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      Style = csDropDownList
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentFont = False
      TabOrder = 1
      AutoDropDown = True
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
      OnChange = CbMAKHOChange
      OnExit = CbMAKHOChange
    end
  end
end
