﻿unit crSONGAY;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls, Mask, wwdbedit;

type
  TframeSONGAY = class(TCrFrame)
    Pa1: TPanel;
    Label2: TLabel;
    EdTOP: TwwDBEdit;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameSONGAY: TframeSONGAY;

implementation

{$R *.dfm}

{ TframeSONGAY }

procedure TframeSONGAY.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := StrToIntDef(EdTOP.Text, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeSONGAY.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeSONGAY.Init;
begin
    inherited;
    EdTOP.Text := '0';
end;

end.
