inherited frameDS_LYDO_THUKHAC: TframeDS_LYDO_THUKHAC
  Height = 55
  ExplicitHeight = 55
  object PaDS_KHO: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 55
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label7: TLabel
      Tag = 2
      Left = 16
      Top = 4
      Width = 182
      Height = 13
      Cursor = 1
      Caption = '<< Danh s'#225'ch l'#253' do thu kh'#225'c >>'
      Font.Charset = ANSI_CHARSET
      Font.Color = 8404992
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      OnClick = Label7Click
    end
    object EdDS: TMemo
      Tag = 2
      Left = 16
      Top = 20
      Width = 365
      Height = 21
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      TabOrder = 0
    end
  end
end
