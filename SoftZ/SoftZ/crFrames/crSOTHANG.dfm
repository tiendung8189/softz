inherited frameSOTHANG: TframeSOTHANG
  Height = 42
  ExplicitHeight = 42
  object Pa1: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 41
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label2: TLabel
      Left = 119
      Top = 13
      Width = 51
      Height = 16
      Caption = 'S'#7889' th'#225'ng'
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object EdTOP: TwwDBEdit
      Left = 176
      Top = 8
      Width = 69
      Height = 24
      EditAlignment = eaRightAlignEditing
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Picture.PictureMask = '#*#'
      TabOrder = 0
      UnboundDataType = wwDefault
      WantReturns = False
      WordWrap = False
    end
  end
end
