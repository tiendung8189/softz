﻿unit crDS_KHO;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeDS_KHO = class(TCrFrame)
    PaDS_KHO: TPanel;
    Label7: TLabel;
    EdDS_KHO: TMemo;
    procedure Label7Click(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameDS_KHO: TframeDS_KHO;

implementation

{$R *.dfm}

uses
    ChonDsKho;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KHO.GetCode: Variant;
begin
    Result := EdDS_KHO.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KHO.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeDS_KHO.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KHO.Init;
begin
  inherited;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeDS_KHO.Label7Click(Sender: TObject);
var
    s: String;
begin
    if not FrmChonDsKho.Get(s) then
    	Exit;

    with EdDS_KHO do
    begin
		Text := s;
        SetFocus;
    end;
end;

end.
