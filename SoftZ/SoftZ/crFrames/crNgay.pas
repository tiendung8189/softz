﻿unit crNgay;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, wwdbdatetimepicker, ExtCtrls;

type
  TframeCrNgay = class(TCrFrame)
    PaNGAY: TPanel;
    Label11: TLabel;
    EdNgay: TwwDBDateTimePicker;
  private
  protected
    procedure Init; override;
  public
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;
    function GetCode: Variant; override;

  end;

var
  frameCrNgay: TframeCrNgay;

implementation

{$R *.dfm}

{ TframeCrNgay }

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrNgay.GetCode: Variant;
begin
    Result := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdNgay.DateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrNgay.GetCriteria;
var
    n: Integer;
begin
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeCrNgay.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeCrNgay.Init;
begin
    inherited;
    EdNgay.Date := Date;
end;

end.
