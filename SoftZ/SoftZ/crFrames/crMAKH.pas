﻿unit crMAKH;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeMAKH = class(TCrFrame)
    PaMADT: TPanel;
    Label36: TLabel;
    EdMaDT: TEdit;
    EdTenDT: TEdit;
    procedure EdMaDTExit(Sender: TObject);
  private
  protected
    procedure Init; override;
  public
    function GetCode: Variant; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;

  end;

var
  frameMAKH: TframeMAKH;

implementation
uses
    MainData, MasterData,ExCommon;
{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAKH.EdMaDTExit(Sender: TObject);
var
    s: String;
begin
    s := (Sender as TEdit).Text;

    exDotMakh(s);
    with DataMain.QrDMKH do
    begin
        if Locate('MADT', s, []) then
        begin
            EdMaDT.Text := s;
            EdTenDT.Text := FieldByName('TENDT').AsString;
        end
        else
        begin
            EdTenDT.Clear;
            EdMaDT.Clear;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeMAKH.GetCode: Variant;
begin
    Result := EdMaDT.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAKH.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeMAKH.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeMAKH.Init;
begin
    inherited;
    with DataMain.QrDMKH do
    if not Active then
        Open;
end;

end.
