﻿unit crDS_NHOMVT;

interface

uses
  SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TFrameDS_NHOMVT = class(TCrFrame)
    PaDS_NHOMVT: TPanel;
    LbNganhNhom: TLabel;
    EdDSNHOM: TMemo;
    procedure LbNganhNhomClick(Sender: TObject);

  private
  protected
    procedure Init; override;

  public
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  FrameDS_NHOMVT: TFrameDS_NHOMVT;

implementation

{$R *.dfm}
    uses
    ChonDsNhom;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrameDS_NHOMVT.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_TIT0 = 'Ngành hàng';
    RS_TIT1 = 'Nhóm hàng';
	RS_TIT3 = 'Nhóm hàng cấp 2';
    RS_TIT_CAP = '<< %s báo cáo >>';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameDS_NHOMVT.LbNganhNhomClick(Sender: TObject);
const
	TIT: array [0..2] of String = (
    	RS_TIT0,
        RS_TIT1,
        RS_TIT3
    );
var
    n: Integer;
    s: String;
begin
	n := EdDSNHOM.Tag;
    if not FrmChonDsNhom.Get(n, s) then
    	Exit;

    LbNganhNhom.Caption := Format(RS_TIT_CAP, [TIT[n]]);
    with EdDSNHOM do
    begin
    	Tag := n;
		Text := s;
        SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
 procedure TFrameDS_NHOMVT.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := EdDSNHOM.Tag;
    Inc(n);
    cr[n] := EdDSNHOM.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrameDS_NHOMVT.Init;
begin
  inherited;

end;

end.
