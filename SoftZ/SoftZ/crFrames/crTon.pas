﻿unit crTon;

interface

uses
  Windows, SysUtils, Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, wwdbdatetimepicker, ExtCtrls;

type
  TframeTon = class(TCrFrame)
    PaTON: TPanel;
    Label15: TLabel;
    Label16: TLabel;
    EdTonDen: TwwDBDateTimePicker;
    EdTonTu: TwwDBDateTimePicker;
    procedure EdTonDenExit(Sender: TObject);
  private
    FStockDate: TDateTime;
  protected
    procedure Init; override;
  public
    function GetStockDate: TDateTime;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
    function GetParamNo: Integer; override;
    function GetCode: Variant; override;
    function Action(Sender: TObject): Boolean; override;

  end;

var
  frameTon: TframeTon;

implementation

uses
    MainData, MasterData, ExCommon, isStr, isMsg, GmsRep, isLib;

{$R *.dfm}

{ TCrFrame1 }

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NO_CALCMONTH = 'Chưa chốt tồn kho tháng %s/%d';
    RS_CANT_CALC = 'Không được tính tồn kho trước ngày bắt đầu'#13'sử dụng chương trình %s ';
    RS_CALC_CFM  = 'Xác nhận tính tồn kho đến ngày %s?';

function TframeTon.Action(Sender: TObject): Boolean;
var
	d, dTon : TDateTime;
    dd, mm, yy: WORD;
begin
    Result := False;
	EdTonDenExit(Sender);

	d := EncodeDate(sysBegYear, sysBegMon, 1);
    if EdTonDen.Date < d then
    begin
    	Msg(Format(RS_CANT_CALC, [DateToStr(d)]));
    	Exit;
    end;

    if sysCloseHH > EdTonDen.Date then
    begin
    	EdTonDen.SetFocus;
    	Exit;
    end;

	if YesNo(Format(RS_CALC_CFM, [DateToStr(EdTonDen.Date)])) then
    begin
		DataMain.CalcStock(EdTonDen.Date);
        d := GetSysParam('STOCK_DATE');
        DecodeDate(d, yy, mm, dd);

		FStockDate := EncodeDate(yy, mm, dd);;
		FrmGmsRep.SetMustCalc(False);
    	EdTonDen.SetFocus;
    end
    else
    begin
    	EdTonDen.Date := FStockDate;
		EdTonDenExit(Sender);
    end;

    Result := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTon.EdTonDenExit(Sender: TObject);
var
	mDate, mBeg, mEnd: TDateTime;
    yy, mm, dd: Word;
    	(*
	    **
    	*)
    procedure Khoaso;
    begin
		Msg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
    end;
begin
    if EdTonDen.Date < sysBegDate then
    begin
		ErrMsg(Format('"Đến ngày" tối thiểu là "%s"', [DateToStr(sysBegDate)]));
        EdTonDen.SelectAll;
        EdTonDen.SetFocus;
    end
    else
    begin
        FrmGmsRep.SetMustCalc(False);
        // Have stock control rights
        if FrmGmsRep.IsStockControl then
        begin
            mDate := EdTonDen.Date;

            DecodeDate(IncMonth(mDate, 1), yy, mm, dd);
            mEnd := EncodeDate(yy, mm, 1) - 1;

            DecodeDate (FStockDate, yy, mm, dd);
            mBeg := EncodeDate(yy, mm, 1);

            // Thang moi
            if mDate >= IncMonth(mBeg, 1) then
            begin
                if FStockDate + 1 < IncMonth(mBeg, 1) then
                begin
                    Msg(Format(RS_NO_CALCMONTH, [isInt2Str(mm, 2), yy]));
                    mDate := FStockDate;
                end
                else
                begin
                    if sysCloseHH > mDate then	// Da khoa so
                        mDate := FStockDate
                    else
                        FrmGmsRep.SetMustCalc(True);
                end;
            end

            // Thang cu
            else if mDate < mBeg then
            begin
                if (Sender as TComponent).Tag = 1 then
                begin
                    if sysCloseHH > mDate then
                    begin
                        KhoaSo;
                        mDate := mEnd;
                    end
                    else
                        FrmGmsRep.SetMustCalc(True);
                end
                else
                begin
                    if mDate = mEnd then			// Cuoi thang is OK
                    else
                        if sysCloseHH > mDate then
                        begin
                            KhoaSo;
                            mDate := mEnd;
                        end
                        else
                            FrmGmsRep.SetMustCalc(True);
                end;
            end

            // Thang hien tai
            else
            begin
                if sysCloseHH > mDate then	// Da khoa so
                begin
                    KhoaSo;
                    mDate := FStockDate;
                end
                else
                    FrmGmsRep.SetMustCalc(mDate <> FStockDate);
            end;
        end

        // No stock control rights
        else
        begin
            // Min(rep date, stock date);
            if EdTonDen.Date > FStockDate then
                mDate := FStockDate
            else
                mDate := EdTonDen.Date;

            DecodeDate (FStockDate, yy, mm, dd);

            // Thang cu
            if mDate < EncodeDate(yy, mm, 1) then
            begin
                DecodeDate(IncMonth(mDate, 1), yy, mm, dd);
                mDate := EncodeDate(yy, mm, 1) - 1;
            end
            else
                mDate := FStockDate;
        end;

        // Adjust rep date
        DecodeDate(mDate, yy, mm, dd);
        if EdTonDen.Date <> mDate then
        begin
            EdTonDen.Date := mDate;
            if FrmGmsRep.IsStockControl then
            begin
                EdTonDen.SelectAll;
                EdTonDen.SetFocus;
            end;
        end;

        EdTonTu.Date := EncodeDate(yy, mm, 1);
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTon.GetCode: Variant;
begin
    Result := GetStockDate;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTon.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdTonTu.DateTime);
    inc(n);
    cr[n] := FormatDateTime('yyyy,mm,dd hh:mm:ss', EdTonDen.DateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTon.GetParamNo: Integer;
begin
    Result := 2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeTon.GetStockDate: TDateTime;
var
    yy, mm, dd: word;
    d: TDateTime;
begin
    d := GetSysParam('STOCK_DATE');
    if d = 0 then
        if (sysMon = sysBegMon) and (sysYear = sysBegYear) then
            d := Date
        else
            d := IncMonth(EncodeDate(sysBegYear, sysBegMon, 1)) - 1;

    DecodeDate(d, yy, mm, dd);
    Result := EncodeDate(yy, mm, dd);
    EdTonTu.Date := EncodeDate(yy, mm, 1);
    EdTonDen.DateTime := Result;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeTon.Init;
begin
    FStockDate := GetStockDate;
    EdTonTu.DisplayFormat := ShortDateFormat;
    EdTonDen.DisplayFormat := ShortDateFormat;
end;

end.
