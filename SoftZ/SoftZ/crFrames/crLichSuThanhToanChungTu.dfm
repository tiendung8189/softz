inherited frameLichSuThanhToanChungTu: TframeLichSuThanhToanChungTu
  Height = 74
  ExplicitHeight = 74
  object PaSILE: TPanel
    Left = 0
    Top = 0
    Width = 395
    Height = 74
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object GroupBox1: TGroupBox
      Left = 2
      Top = 2
      Width = 391
      Height = 70
      Align = alClient
      Caption = '   L'#7883'ch s'#7917' thanh to'#225'n ch'#7913'ng t'#7915'   '
      TabOrder = 0
      ExplicitLeft = 8
      ExplicitTop = 0
      ExplicitWidth = 241
      ExplicitHeight = 121
      object Rb1: TRadioButton
        Left = 17
        Top = 22
        Width = 142
        Height = 17
        Caption = 'Ch'#432'a thanh to'#225'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
      end
      object Rb2: TRadioButton
        Left = 165
        Top = 22
        Width = 98
        Height = 17
        Caption = 'M'#7897't ph'#7847'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 1
      end
      object RbALL: TRadioButton
        Left = 17
        Top = 45
        Width = 65
        Height = 17
        Caption = 'T'#7845't c'#7843
        Checked = True
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 3
        TabStop = True
      end
      object Rb3: TRadioButton
        Left = 288
        Top = 22
        Width = 98
        Height = 17
        Caption = 'Ho'#224'n t'#7845't'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 2
      end
    end
  end
end
