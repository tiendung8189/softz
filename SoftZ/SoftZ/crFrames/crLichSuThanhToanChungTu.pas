﻿unit crLichSuThanhToanChungTu;

interface

uses
  Variants, Classes, Controls, Forms,
  crCommon, StdCtrls, ExtCtrls;

type
  TframeLichSuThanhToanChungTu = class(TCrFrame)
    PaSILE: TPanel;
    Rb1: TRadioButton;
    Rb2: TRadioButton;
    RbALL: TRadioButton;
    GroupBox1: TGroupBox;
    Rb3: TRadioButton;
  private
  protected
    procedure Init; override;

  public
    function GetCode: Variant; override;
    function GetParamNo: Integer; override;
    procedure GetCriteria(var cr: array of Variant;
                            fieldList: array of String;
                            var valuesList: array of Variant); override;
  end;

var
  frameLichSuThanhToanChungTu: TframeLichSuThanhToanChungTu;

implementation

{$R *.dfm}
(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeLichSuThanhToanChungTu.GetParamNo: Integer;
begin
    Result := 1;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TframeLichSuThanhToanChungTu.GetCode: Variant;
begin
    if Rb1.Checked then
        Result := 'NEW'
    else if Rb2.Checked then
        Result := 'PART'
    else if Rb3.Checked then
        Result := 'DONE'
    else
        Result := 'ALL';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeLichSuThanhToanChungTu.GetCriteria;
var
    n: Integer;
begin
    inherited;
    n := Length(cr) - GetParamNo;
    cr[n] := GetCode;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TframeLichSuThanhToanChungTu.Init;
begin
    inherited;
    RbALL.Checked:= True;
end;

end.
