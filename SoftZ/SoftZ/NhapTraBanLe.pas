﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit NhapTraBanLe;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,Variants,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Messages, Windows,
  frameNgay, frameNavi, isDb, isPanel, wwDialog, Grids, Wwdbgrid, ToolWin,
  Wwdotdot, Wwdbcomb, Buttons, AdvEdit, DBAdvEd, wwcheckbox, DBGridEh,
  DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmNhapTraBanLe = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    GrBrowse: TwwDBGrid2;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    PaTotal: TPanel;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXDELETE_DATE: TDateTimeField;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaMaster: TPanel;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    CmdClearFilter1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    PopDetail: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    CmdEmptyDetail: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    Bevel1: TBevel;
    N3: TMenuItem;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    vlTotal1: TisTotal;
    CbLOAITHUE: TwwDBLookupCombo;
    DBText2: TDBText;
    Label1: TLabel;
    CbNGAY: TwwDBDateTimePicker;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    PopDanhmuc: TAdvPopupMenu;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    SepChecked: TToolButton;
    BtnIn2: TToolButton;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    SepBarcode: TToolButton;
    QrCTLOC: TWideStringField;
    PaThanhtoan: TPanel;
    PaSotien1: TPanel;
    PaSotien: TPanel;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    PaThue: TPanel;
    N8: TMenuItem;
    ExportraExceltlidliu1: TMenuItem;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    ExportraExceltlidliu2: TMenuItem;
    QrNXSoTienCK: TFloatField;
    QrNXLoaiThue: TWideStringField;
    QrNXThueSuat: TFloatField;
    QrNXThanhToanChuaCL: TFloatField;
    QrNXSoTienCL: TFloatField;
    QrNXThanhToanChuaThue: TFloatField;
    QrNXSoTienThue: TFloatField;
    QrNXSoTienThue_5: TFloatField;
    QrNXSoTienThue_10: TFloatField;
    QrNXSoTienThue_Khac: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrNXHinhThuc_Gia: TWideStringField;
    QrNXThanhTien: TFloatField;
    QrNXThanhToan: TFloatField;
    QrCTTyLeCK: TFloatField;
    QrCTSoTienCK: TFloatField;
    QrCTThanhTienSauCK: TFloatField;
    QrCTThanhToanChuaCL: TFloatField;
    QrCTSoTienCL: TFloatField;
    QrCTThanhToanChuaThue: TFloatField;
    QrCTThueSuat: TFloatField;
    QrCTSoTienThue: TFloatField;
    QrCTSoTienThue_5: TFloatField;
    QrCTSoTienThue_10: TFloatField;
    QrCTSoTienThue_Khac: TFloatField;
    QrCTThanhToan: TFloatField;
    QrCTMaBo: TWideStringField;
    QrCTDonGia: TFloatField;
    QrCTSoLuong: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTLoaiThue: TWideStringField;
    QrCTLK_TenThue2: TWideStringField;
    QrCTGhiChu: TWideStringField;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    CbbThuNgan: TDbLookupComboboxEh2;
    QrCTDonGiaChuaThue: TFloatField;
    QrCTSoTienThueChuaRound: TFloatField;
    QrNXSoTienThueChuaRound: TFloatField;
    QrCTThanhToanTinhThue: TFloatField;
    QrCTDonGiaTinhThue: TFloatField;
    CmdRecalc: TAction;
    QrNXThanhToanTinhThue: TFloatField;
    QrNXSoLuong: TFloatField;
    EdSoPhieu: TDBEditEh;
    EdTriGiaTT: TDBNumberEditEh;
    DBNumberEditEh1: TDBNumberEditEh;
    DBNumberEditEh2: TDBNumberEditEh;
    DBNumberEditEh3: TDBNumberEditEh;
    DBNumberEditEh4: TDBNumberEditEh;
    DBNumberEditEh7: TDBNumberEditEh;
    EdTienVAT: TDBNumberEditEh;
    CmdThue: TAction;
    DBMemoEh1: TDBMemoEh;
    QrNXMaQuay: TWideStringField;
    QrNXCa: TWideStringField;
    QrNXKhoaPhieuGoc: TGuidField;
    QrNXSctPhieuGoc: TWideStringField;
    QrNXNgayPhieuGoc: TDateTimeField;
    QrNXNhanVienThuNgan: TIntegerField;
    QrNXMaVIP: TWideStringField;
    QrNXThanhTienSauCK: TFloatField;
    QrCTTenVT: TWideStringField;
    QrCTDvt: TWideStringField;
    QrCTKhoactPhieuGoc: TGuidField;
    QrCTSoLuongPhieuGoc: TFloatField;
    QrCTDonGiaCK: TFloatField;
    Label7: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    EdSctPhieuGoc: TDBEditEh;
    QrNXLK_UserNameNhanVienThuNgan: TWideStringField;
    QrNXCHECKED: TBooleanField;
    CmdInsertPhieuGoc: TAction;
    CmdRemovePhieuGoc: TAction;
    CmdUpdateDetail: TAction;
    CmdUpdateQty: TAction;
    Cpnhtslngtrhng1: TMenuItem;
    frNGAY1: TfrNGAY;
    QrNXLK_TenNhanVienThuNgan: TWideStringField;
    spBANLE_Select_Full: TADOStoredProc;
    QrCTMaNoiBo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure QrNXAfterPost(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrCTThanhTienChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure QrNXHINHTHUC_GIAChange(Sender: TField);
    procedure BtnInClick(Sender: TObject);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure QrCTThanhTienSauCKChange(Sender: TField);
    procedure QrCTThanhToanChange(Sender: TField);
    procedure QrNXSoTienThueChuaRoundChange(Sender: TField);
    procedure QrCTThanhToanTinhThueChange(Sender: TField);
    procedure CmdRecalcExecute(Sender: TObject);
    procedure CmdThueExecute(Sender: TObject);
    procedure CmdInsertPhieuGocExecute(Sender: TObject);
    procedure CmdRemovePhieuGocExecute(Sender: TObject);
    procedure CmdUpdateDetailExecute(Sender: TObject);
    procedure CmdUpdateQtyExecute(Sender: TObject);

  private
	mLCT: String;
	mCanEdit, mObsolete: Boolean;
	mMakho, mHTGia: String;

    // List filter
   	fTungay, fDenngay: TDateTime;
    fLoc: String;
    fType: Integer;
    fSQL, fStr: String;

	    (*
    	** Functions
	    *)

    procedure escapeKey(pSleepTime: Variant);
  public
	procedure Execute(r: WORD);
  end;

var
  FrmNhapTraBanLe: TFrmNhapTraBanLe;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, GuidEx, isCommon,
    ChonDsma, isLib, exThread, isFile,
    Tienthue, MasterData, OfficeData, NhaptraBanleTimPhieu, FastReport;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_NHAPTRA_BL';

	(*
	** Form events
	*)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.Execute;
begin
    mLCT := 'NTBL';

    // Audit setting
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;

    // Done
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.FormCreate(Sender: TObject);
begin
    frNgay1.Init;
    frNavi.DataSet := QrNX;
	
    //Lay gia tri mac dinh cho HTGia
    mHTGia := sysGiaCoVAT;

    // Initial
    fLoc := sysLoc;
    mMakho := RegReadString(Name, 'Makho', sysDefKho);

    mObsolete := False;
  	mTrigger := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init2('_IDI_RECEIPT');

    Wait(PREPARING);
    InitFmtAndRoud(mLct);

	// Open database
	with DataMain do
    begin
		OpenDataSets([QrDMLOAITHUE, QrDMKHO, QrHINHTHUC_GIA, QrLOC]);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, ctCurFmt);
        SetDisplayFormat(QrNX, ['SoLuong'], ctQtyFmt);
	    SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['Ngay', 'NgayPhieuGoc'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, ctCurFmt);
    	SetDisplayFormat(QrCT, ['SoLuong', 'SoLuongPhieuGoc'], ctQtyFmt);
	    SetDisplayFormat(QrCT, ['TyLeCK'], sysPerFmt);
        SetDisplayFormat(QrCT, ['DonGia'], ctPriceFmt);
        SetDisplayFormat(QrCT, ['ThueSuat'], sysTaxFmt);
        SetDisplayFormat(QrCT, ['SoTienThue'], sysFloatFmtTwo);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

//    if not sysIsThue then
//    begin
//        PaThue.Visible := False;
//        grRemoveFields(GrBrowse, ['SoTienThue', 'HOADON_SO', 'HOADON_SERI', 'HOADON_NGAY']);
//        grRemoveFields(GrDetail, ['ThanhToanChuaThue', 'LK_TenThue', 'ThueSuat',
//                'SoTienThue', 'ThanhToan']);
//    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsBarcode then
    begin
        grRemoveFields(GrDetail, ['B1']);
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
	RegWrite(Name, ['Makho'], [mMakho]);
    try
	    CloseDataSets([QrCT, QrNX]);
    finally
    end;
    Action := caFree;
end;

	(*
	** Actions
	*)

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;

	with DataMain do
    begin
    	QrDMVT.Requery;
        QrPTNHAP.Requery;
        QrDMNCC.Requery;
        QrDMKHO.Requery;
    end;
            
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdRecalcExecute(Sender: TObject);
var
    bm: TBytes;
begin
    with QrCT do
    begin
        DisableControls;
        bm := BookMark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('ThanhToanTinhThue').AsFloat := FieldByName('ThanhToanTinhThue').AsFloat;
            Next;
        end;
        CheckBrowseMode;
        BookMark := bm;
        EnableControls;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
    if (frNgay1.edFrom.Date <> fTungay) or
       (frNgay1.edTo.Date   <> fDenngay) or
       (VarToStr(frNgay1.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frNgay1.EdFrom.Date;
        fDenngay := frNgay1.EdTo.Date;
        fLoc     := VarToStr(frNgay1.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from TRAHANG_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from TRAHANG_CT a, DM_VT_FULL b where a.KHOA = TRAHANG.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from TRAHANG_CT where KHOA = TRAHANG.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add(' order by NGAY desc, SCT desc');

    	    Open;
    	    if s <> '' then
	        	Sort := s;
        end;

        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdRemovePhieuGocExecute(Sender: TObject);
begin
    with QrNX do
    begin
        SetEditState(QrNX);
        FieldByName('KhoaPhieuGoc').Clear;
        FieldByName('SctPhieuGoc').Clear;
        FieldByName('NgayPhieuGoc').Clear;
    end;

    CmdUpdateDetail.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdNewExecute(Sender: TObject);
begin
	QrNX.Append;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;

    with QrCT do
    begin
        DisableControls;
        First;
        while not Eof do
            if FieldByName('SoLuong').AsFloat = 0 then
            begin
                DeleteConfirm(False);
                Delete;
                DeleteConfirm(True);
            end
            else
                Next;
        First;
        EnableControls;
    end;

    QrNX.Post;
    exSaveDetails(QrCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdDelExecute(Sender: TObject);
begin
    exCheckLoc(QrNX);
	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    try
        CbNgay.SetFocus;
    except
    end
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

procedure TFrmNhapTraBanLe.CmdInsertPhieuGocExecute(Sender: TObject);
var
    makho, maquay, sct, mavip: String;
    m, y, thungan: Integer;
    dd, mm, yy: WORD;
    khoa: TGUID;
    ngay: TDateTime;
begin
   	DecodeDate(Date, yy, mm, dd);
    m := mm;
    y := yy;
    with QrNX do
    begin
        makho := FieldByName('MaKho').AsString;
        maquay := FieldByName('MaQuay').AsString;
    end;

    Application.CreateForm(TFrmNhaptraBanleTimPhieu, FrmNhaptraBanleTimPhieu);
	if not FrmNhaptraBanleTimPhieu.Execute(y, m, makho, maquay, khoa, ngay, sct, mavip) then
        Exit;

    with QrNX do
    begin
        if sct <> '' then
        begin
            SetEditState(QrNX);
            TGuidField(FieldByName('KhoaPhieuGoc')).AsGuid := khoa;
            FieldByName('NgayPhieuGoc').AsDateTime := ngay;
            FieldByName('SctPhieuGoc').AsString := sct;
            if mavip <> '' then
                FieldByName('MaVIP').AsString := mavip
            else
                FieldByName('MaVIP').Clear;
        end;
    end;

    CmdUpdateDetail.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsNX)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdThueExecute(Sender: TObject);
var
    p: TPoint;
begin
    p.x := GrDetail.Left + GrDetail.Width;
    p.y := PaTotal.Top;
    p := ClientToScreen(p);

    Application.CreateForm(TFrmTienthue, FrmTienthue);
    with FrmTienthue do
    begin
    	Left := p.x - Width - 2;
        Top  := p.y - Height + 2;
    	Execute(DsNX);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdTotalExecute(Sender: TObject);
var
    candoi: Double;
begin
    candoi := 0;
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('ThueSuat').AsFloat <> 0 then
            begin
                Break;
            end;

            Next;
        end;

        if FieldByName('SoTienCL').AsFloat <> 0 then
        begin
            Edit;
            FieldByName('SoTienCL').AsFloat := 0;
            CheckBrowseMode;
        end;
    end;

    with QrNX do
    begin
        if (FieldByName('ThanhToan').AsFloat <> 0) then
        begin
            if FieldByName('HinhThuc_Gia').AsString = sysGiaCoVAT then
                candoi := FieldByName('ThanhToan').AsFloat
                            - FieldByName('SoTienThue').AsFloat
                            - FieldByName('ThanhToanChuaThue').AsFloat
            else
                candoi := FieldByName('ThanhToanChuaThue').AsFloat
                            + FieldByName('SoTienThue').AsFloat
                            - FieldByName('ThanhToan').AsFloat
        end;
    end;

    if candoi <> 0 then
    with QrCT do
    begin
        First;
        while not Eof do
        begin
            if FieldByName('ThueSuat').AsFloat <> 0 then
            begin
                Break;
            end;

            Next;
        end;

        Edit;
        FieldByName('SoTienCL').AsFloat := candoi;
        CheckBrowseMode;
    end;
	vlTotal1.Sum;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu bán lẻ có mặt hàng sai mã. Tiếp tục?';
procedure TFrmNhapTraBanLe.CmdUpdateDetailExecute(Sender: TObject);
var
    khoaPhieuGoc : TGUID;
    sctPhieuGoc: String;
begin
    khoaPhieuGoc := TGuidField(QrNX.FieldByName('KhoaPhieuGoc')).AsGuid;
    sctPhieuGoc := QrNX.FieldByName('SctPhieuGoc').AsString;

    // Clean up
    DeleteConfirm(False);
	EmptyDataset(QrCT);
    DeleteConfirm(True);

    if sctPhieuGoc <> '' then
    begin
        with spBANLE_Select_Full do
        begin
            Prepared := True;
            Parameters.ParamByName('@KHOA').Value := TGuidEx.ToString(khoaPhieuGoc);;
            ExecProc;

            // Co mat hang sai ma
            if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
                if not YesNo(RS_INVALID_CODEIMPORTED, 1) then
                    Exit;

            // Khong co mat hang nao
            Active := True;
            if IsEmpty then
            begin
                Active := False;
                Exit;
            end;

            Screen.Cursor := crSqlWait;
            QrCT.DisableControls;
            while not Eof do
            begin
                QrCT.Append;
                QrCT.FieldByName('MaVT').AsString     := FieldByName('MAVT').AsString;
                QrCT.FieldByName('MaNoiBo').AsString     := FieldByName('MaNoiBo').AsString;
                QrCT.FieldByName('TenVT').AsString     := FieldByName('TenVT').AsString;
                QrCT.FieldByName('Dvt').AsString     := FieldByName('Dvt').AsString;
                QrCT.FieldByName('SoLuongPhieuGoc').AsFloat   := FieldByName('SoLuong').AsFloat;
                QrCT.FieldByName('DonGia').AsFloat    := FieldByName('DonGia').AsFloat;
                QrCT.FieldByName('LoaiThue').AsString     := FieldByName('LoaiThue').AsString;
                QrCT.FieldByName('ThueSuat').AsFloat := FieldByName('ThueSuat').AsFloat;
                QrCT.FieldByName('TyLeCK').AsFloat     := FieldByName('TyLeCK').AsFloat;
                QrCT.Post;

                Next;
            end;
            Close;
            QrCT.EnableControls;
        end;
        QrCT.First;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdUpdateQtyExecute(Sender: TObject);
begin
    with QrCT do
    begin
        CheckBrowseMode;
        DisableControls;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SoLuong').AsFloat := FieldByName('SoLuongPhieuGoc').AsFloat;
            Post;
            Next;
        end;
        EnableControls;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.escapeKey(pSleepTime: Variant);
begin
    Application.ProcessMessages;
    Sleep(pSleepTime);
    GrDetail.Perform(WM_KEYDOWN, VK_ESCAPE, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail)
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

//    FrmFastReport.ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT, bPhieuGoc: Boolean;
    n: Integer;
begin
	// Master
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    CmdPrint.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
	CmdRefresh.Enabled := bBrowse;
    CmdFilter.Enabled := n = 0;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdTotal.Enabled  := n = 1;
    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

	// Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;

    CmdEmptyDetail.Enabled := not bEmptyCT;

    bPhieuGoc := QrNX.FieldByName('KhoaPhieuGoc').AsString <> '';
    CmdInsertPhieuGoc.Enabled := mCanEdit and (not bPhieuGoc);
    CmdRemovePhieuGoc.Enabled := mCanEdit and bPhieuGoc;
    CmdUpdateDetail.Enabled := mCanEdit;

    CmdUpdateQty.Enabled := mCanEdit and (not bEmptyCT);

end;

    (*
    **  Master DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    mTrigger := True;
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('Ngay').AsDateTime        := d;
		FieldByName('MaKho').AsString         := mMakho;
        FieldByName('LOC').AsString           := sysLoc;
        FieldByName('NhanVienThuNgan').AsInteger := sysLogonUID;
        FieldByName('HinhThuc_Gia').AsString  := Iif(mHTGia = '', '03', mHTGia);
    end;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
	    if BlankConfirm(QrNX, ['Ngay', 'MaKho', 'NhanVienThuNgan']) then
        	Abort;
		exValidClosing(FieldByName('Ngay').AsDateTime);
    end;

 	DataMain.AllocSCT(mLCT, QrNX);
	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXAfterPost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		mMakho := FieldByName('MaKho').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	with DataSet do
    begin
    	// Validate: khoa so
		exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CHANGE_PRICE_FORM = 'Hình thức giá đã thay đổi, phải tính lại giá cả.';
procedure TFrmNhapTraBanLe.QrNXHINHTHUC_GIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
	if mTrigger then
    	Exit;

	if QrCT.IsEmpty then
    	Exit;

    Msg(RS_INVALID_CHANGE_PRICE_FORM);
    CmdRecalc.Execute;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrNXSoTienThueChuaRoundChange(Sender: TField);
begin
    with QrNX do
    begin
        FieldByName('SoTienThue').AsFloat := exVNDRound(FieldByName('SoTienThueChuaRound').AsFloat, sysCurRound)
    end;
end;

(*
    ** End: Master DB
    *)

    (*
    **  Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_GIAVON_LONHON_GIABAN = 'Giá vốn nhập hàng > Giá bán lẻ. Xác nhận?';
procedure TFrmNhapTraBanLe.QrCTBeforePost(DataSet: TDataSet);
var
    mGiaban, mSL, mThanhtien: Double;
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
   		Abort;
    SetEditState(QrNX);
	vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTAfterDelete(DataSet: TDataSet);
begin
	vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if BlankConfirm(QrNX, ['HinhThuc_Gia']) then
        Abort;

	SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTCalcFields(DataSet: TDataSet);
begin
	with QrCT do
    begin
    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    soluong, dongia, thanhtien : Double;
begin
	with QrCT do
    begin
        soluong := FieldByName('SoLuong').AsFloat;
        dongia := FieldByName('DonGia').AsFloat;

        thanhtien := exVNDRound(soluong * dongia, ctCurRound);

        FieldByName('ThanhTien').AsFloat := thanhtien;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTThanhTienChange(Sender: TField);
var
    r: Extended;
    dongia, dongiaCK, thanhtien, ck, tlck, thanhtienSauCK: Double;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        dongia := FieldByName('DonGia').AsFloat;
        dongiaCK := FieldByName('DonGiaCK').AsFloat;
        tlck := FieldByName('TyLeCK').AsFloat;
        Ck := FieldByName('SoTienCK').AsFloat;
        thanhtien := FieldByName('ThanhTien').AsFloat;

        if Sender.FieldName = 'DonGiaCK' then
        begin
            tlck := (1 - SafeDiv(dongiaCK, dongia))*100;
            Ck := exVNDRound(thanhtien * tlck / 100.0, sysCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCK').AsFloat := tlck;
            FieldByName('SoTienCK').AsFloat := ck;
            mTrigger := pTrigger;
        end
        else if Sender.FieldName = 'SoTienCK' then
        begin
            tlck := Iif(Ck=0, 0, 100 * (SafeDiv(Ck, thanhtien)));
            dongiaCK := dongia * (1 - tlck/100);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('TyLeCK').AsFloat := tlck;
            FieldByName('DonGiaCK').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end else
        begin
            dongiaCK := dongia * (1 - tlck/100);
            Ck := exVNDRound(thanhtien * tlck / 100.0, sysCurRound);

            pTrigger := mTrigger;
            mTrigger := True;
            FieldByName('SoTienCK').AsFloat := ck;
            FieldByName('DonGiaCK').AsFloat := dongiaCK;
            mTrigger := pTrigger;
        end;

        thanhtienSauCK := thanhtien - ck;
        FieldByName('ThanhTienSauCK').AsFloat := thanhtienSauCK;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTThanhTienSauCKChange(Sender: TField);
begin
    with QrCT do
    begin
        FieldByName('DonGiaTinhThue').AsFloat := FieldByName('DonGiaCK').AsFloat;
        FieldByName('ThanhToanTinhThue').AsFloat := FieldByName('ThanhTienSauCK').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTThanhToanChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.QrCTThanhToanTinhThueChange(Sender: TField);
var
    mHinhthuc: String;
    donGiaTinhThue, dgChuaVAT, ts, sl,
    thanhToanTinhThue, thanhtoanChuaVATcl, cl, thanhtoanChuaVAT, thue, thanhtoan : Double;
    pTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    mHinhthuc := QrNX.FieldByName('HinhThuc_Gia').AsString;

    with QrCT do
    begin
        donGiaTinhThue := FieldByName('DonGiaTinhThue').AsFloat;
        ts := FieldByName('ThueSuat').AsFloat;
        sl := FieldByName('SoLuong').AsFloat;
        thanhToanTinhThue := FieldByName('ThanhToanTinhThue').AsFloat;
        cl := FieldByName('SoTienCL').AsFloat;

        if (mHinhthuc = sysGiaCoVAT) then
        begin
            thanhtoan := thanhToanTinhThue;

            thanhtoanChuaVATcl := exVNDRound((donGiaTinhThue /(1 + ts/100)) * sl, -2);
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl + cl, sysCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);
        end
        else
        begin
            thanhtoanChuaVATcl := thanhToanTinhThue;
            thanhtoanChuaVAT := exVNDRound(thanhtoanChuaVATcl, sysCurRound);

            dgChuaVAT := exVNDRound(Iif(sl=0,0, SafeDiv(thanhtoanChuaVAT, sl)), -2);
            thue := exVNDRound(thanhtoanChuaVATcl * ts/100, -2);

            thanhtoan := exVNDRound(thanhtoanChuaVATcl + thue + cl, sysCurRound);
        end;

        pTrigger := mTrigger;
        mTrigger := True;
        FieldByName('DonGiaChuaThue').AsFloat := dgChuaVAT;
        FieldByName('ThanhToanChuaCL').AsFloat := thanhtoanChuaVATcl ;
        FieldByName('SoTienCL').AsFloat := cl;
        FieldByName('ThanhToanChuaThue').AsFloat := thanhtoanChuaVAT;
        FieldByName('SoTienThueChuaRound').AsFloat := thue;

        if Ts = 5 then
        begin
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_5').AsFloat := Thue;
        end
        else if Ts = 10 then
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := Thue;
        end else
        begin
            FieldByName('SoTienThue_5').AsFloat := 0;
            FieldByName('SoTienThue_10').AsFloat := 0;
            FieldByName('SoTienThue_Khac').AsFloat := Thue;
        end;

        FieldByName('SoTienThue').AsFloat := Thue;
        FieldByName('ThanhToan').AsFloat := thanhtoan;
        mTrigger := pTrigger;
    end;
end;
	(*
    ** End: Detail DB
    *)

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrNX do
    begin
		ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SoLuong').AsFloat);
        ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('SoTienCK').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienCK').AsFloat);
        ColumnByName('ThanhTienSauCK').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSauCK').AsFloat);
        ColumnByName('ThanhToanChuaCL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToanChuaCL').AsFloat);
        ColumnByName('SoTienCL').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienCL').AsFloat);
        ColumnByName('ThanhToanChuaThue').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToanChuaThue').AsFloat);

		ColumnByName('SoTienThue').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('SoTienThue').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CbMAKHOBeforeDropDown(Sender: TObject);
begin
//    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
  NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrCT do
        begin
            Close;
            Open;
        end;

        Screen.Cursor := crDefault;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhapTraBanLe.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

end.
