object FrmChonKh: TFrmChonKh
  Left = 560
  Top = 435
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Ch'#7885'n Phi'#7871'u Xu'#7845't'
  ClientHeight = 386
  ClientWidth = 755
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object PaDondh: TPanel
    Left = 0
    Top = 0
    Width = 755
    Height = 105
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object Label65: TLabel
      Left = 147
      Top = 73
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 456
      Top = 73
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object EdTungay: TwwDBDateTimePicker
      Left = 200
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'TuNgay'
      DataSource = DsDummyEh
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 4
    end
    object EdDenngay: TwwDBDateTimePicker
      Left = 516
      Top = 68
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      DataField = 'DenNgay'
      DataSource = DsDummyEh
      Epoch = 1950
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 5
    end
    object CbbDonVi: TDbLookupComboboxEh2
      Left = 200
      Top = 12
      Width = 338
      Height = 22
      ControlLabel.Width = 56
      ControlLabel.Height = 16
      ControlLabel.Caption = #272#417'n v'#7883' tr'#7843
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      DynProps = <>
      DataField = 'Madt'
      DataSource = DsDummyEh
      DropDownBox.AutoFitColWidths = False
      DropDownBox.Columns = <
        item
          FieldName = 'TENDT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'T'#234'n'
          Width = 336
        end
        item
          FieldName = 'MADT'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'M'#227
          Width = 60
        end>
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.Width = 417
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      KeyField = 'MADT'
      ListField = 'TENDT'
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 0
      Visible = True
    end
    object EdMADV: TDBEditEh
      Left = 540
      Top = 11
      Width = 77
      Height = 22
      TabStop = False
      Alignment = taLeftJustify
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clBtnFace
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DataField = 'Madt'
      DataSource = DsDummyEh
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 1
      Visible = True
    end
    object EdMaKho: TDBEditEh
      Left = 540
      Top = 40
      Width = 77
      Height = 22
      TabStop = False
      Alignment = taLeftJustify
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clBtnFace
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DataField = 'MaKho'
      DataSource = DsDummyEh
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      Visible = True
    end
    object CbKhoHang: TDbLookupComboboxEh2
      Tag = 1
      Left = 200
      Top = 40
      Width = 338
      Height = 22
      ControlLabel.Width = 21
      ControlLabel.Height = 16
      ControlLabel.Caption = 'Kho'
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      DynProps = <>
      DataField = 'MaKho'
      DataSource = DsDummyEh
      DropDownBox.AutoFitColWidths = False
      DropDownBox.Columns = <
        item
          FieldName = 'TENKHO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'T'#234'n'
          Width = 336
        end
        item
          FieldName = 'MAKHO'
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          SpecCell.Font.Charset = DEFAULT_CHARSET
          SpecCell.Font.Color = clWindowText
          SpecCell.Font.Height = -12
          SpecCell.Font.Name = 'Tahoma'
          SpecCell.Font.Style = []
          Title.Alignment = taCenter
          Title.Caption = 'M'#227
          Width = 60
        end>
      DropDownBox.ListSource = DsDMKHO
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.Width = 417
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      Flat = True
      KeyField = 'MAKHO'
      ListField = 'TENKHO'
      ListSource = DsDMKHO
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 2
      Visible = True
    end
  end
  object GrBrowse: TwwDBGrid2
    Left = 0
    Top = 105
    Width = 755
    Height = 260
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'NGAY'#9'10'#9'Ng'#224'y'#9'F'
      'SCT'#9'18'#9'S'#7889' phi'#7871'u'#9'F'
      'MAKH'#9'15'#9'M'#227' '#273#417'n v'#7883#9'F'
      'TENKH'#9'40'#9'T'#234'n '#273#417'n v'#7883#9'F'
      'MAKHO'#9'15'#9'M'#227' kho'#9'F'
      'TENKHO'#9'40'#9'T'#234'n kho'#9'F'
      'DGIAI'#9'50'#9'Di'#7877'n gi'#7843'i'#9'F'
      'NG_GIAO'#9'30'#9'Ng'#432#7901'i giao'#9'F'
      'NG_NHAN'#9'30'#9'Ng'#432#7901'i nh'#7853'n'#9'F'
      'HAN_TTOAN'#9'15'#9'H'#7841'n thanh to'#225'n'#9'F')
    MemoAttributes = [mSizeable, mWordWrap, mGridShow, mDisableDialog]
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsKHACHHANG
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgWordWrap, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnDblClick = GrBrowseDblClick
    OnEnter = CmdRefreshExecute
    OnKeyPress = GrBrowseKeyPress
    ImageList = DataMain.ImageMark
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object Status: TStatusBar
    Left = 0
    Top = 365
    Width = 755
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object DsKHACHHANG: TDataSource
    DataSet = QrKHACHHANG
    Left = 172
    Top = 196
  end
  object QrKHACHHANG: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Parameters = <
      item
        Name = 'NGAYD'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end
      item
        Name = 'NGAYC'
        Attributes = [paNullable]
        DataType = ftDateTime
        NumericScale = 3
        Precision = 23
        Size = 16
        Value = Null
      end>
    SQL.Strings = (
      'select'#9'a.*, b.TENKHO, c.TENKH'
      '  from'#9'T_CHUNGTU a, DM_KHO b, DM_KH c'
      ' where'#9'a.MAKHO = b.MAKHO'
      '   and'#9'a.MAKH = c.MAKH'
      '   and'#9'a.LCT = '#39'XBAN'#39
      '   and'#9'a.NGAY >= :NGAYD'
      '   and'#9'a.NGAY <  :NGAYC + 1'
      'order by a.NGAY, a.SCT')
    Left = 144
    Top = 196
    object QrKHACHHANGSCT: TWideStringField
      DisplayLabel = 'S'#232' phi'#213'u'
      FieldName = 'SCT'
    end
    object QrKHACHHANGNGAY: TDateTimeField
      DisplayLabel = 'Ng'#181'y'
      FieldName = 'NGAY'
    end
    object QrKHACHHANGMAKH: TWideStringField
      DisplayLabel = 'M'#183' '#174#172'n v'#222
      FieldName = 'MAKH'
      Size = 15
    end
    object QrKHACHHANGTENKH: TWideStringField
      DisplayLabel = 'T'#170'n '#174#172'n v'#222
      DisplayWidth = 35
      FieldName = 'TENKH'
      Size = 200
    end
    object QrKHACHHANGTENKHO: TWideStringField
      DisplayLabel = 'T'#170'n kho'
      DisplayWidth = 35
      FieldName = 'TENKHO'
      Size = 100
    end
    object QrKHACHHANGMAKHO: TWideStringField
      DisplayLabel = 'M'#183' kho'
      DisplayWidth = 15
      FieldName = 'MAKHO'
      FixedChar = True
      Size = 2
    end
    object QrKHACHHANGKHOA: TIntegerField
      FieldName = 'KHOA'
      Visible = False
    end
    object QrKHACHHANGDGIAI: TWideMemoField
      DisplayLabel = 'Di'#212'n gi'#182'i'
      FieldName = 'DGIAI'
      BlobType = ftWideMemo
    end
    object QrKHACHHANGNG_GIAO: TWideStringField
      DisplayLabel = 'Ng'#173#234'i giao'
      FieldName = 'NG_GIAO'
      Size = 30
    end
    object QrKHACHHANGNG_NHAN: TWideStringField
      DisplayLabel = 'Ng'#173#234'i nh'#203'n'
      FieldName = 'NG_NHAN'
      Size = 30
    end
    object QrKHACHHANGHAN_TTOAN: TIntegerField
      DisplayLabel = 'H'#185'n thanh to'#184'n'
      DisplayWidth = 15
      FieldName = 'HAN_TTOAN'
    end
  end
  object ActionList1: TActionList
    Left = 144
    Top = 224
    object CmdChose: TAction
      Caption = 'CmdChose'
    end
    object CmdClose: TAction
      Caption = 'CmdClose'
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin    '
      ImageIndex = 31
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdRefresh: TAction
      Caption = 'CmdRefresh'
      OnExecute = CmdRefreshExecute
    end
  end
  object PopupMenu1: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 172
    Top = 224
    object Tm1: TMenuItem
      Action = CmdSearch
    end
  end
  object QrDMKHO: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKHO, TENKHO'
      '  from DM_KHO'
      'order by MAKHO'
      ' ')
    Left = 234
    Top = 196
  end
  object QrDMKH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MAKH, TENKH'
      '  from DM_KH'
      'order by TENKH'
      ' ')
    Left = 206
    Top = 196
  end
  object DsDMKHO: TDataSource
    DataSet = QrDMKHO
    Left = 244
    Top = 228
  end
  object DsDMKH: TDataSource
    DataSet = QrDMKH
    Left = 204
    Top = 228
  end
  object TbDummyEh: TMemTableEh
    Params = <>
    AfterInsert = TbDummyEhAfterInsert
    Left = 400
    Top = 168
    object MemTableData: TMemTableDataEh
      object DataStruct: TMTDataStructEh
        object MaKho: TMTStringDataFieldEh
          FieldName = 'MaKho'
          StringDataType = fdtStringEh
          DisplayWidth = 20
        end
        object Madt: TMTStringDataFieldEh
          FieldName = 'Madt'
          StringDataType = fdtStringEh
          DisplayWidth = 20
        end
        object TuNgay: TMTDateTimeDataFieldEh
          FieldName = 'TuNgay'
          DateTimeDataType = fdtDateEh
          DisplayWidth = 20
        end
        object DenNgay: TMTDateTimeDataFieldEh
          FieldName = 'DenNgay'
          DateTimeDataType = fdtDateEh
          DisplayWidth = 20
        end
      end
      object RecordsList: TRecordsListEh
      end
    end
  end
  object DsDummyEh: TDataSource
    DataSet = TbDummyEh
    Left = 407
    Top = 217
  end
end
