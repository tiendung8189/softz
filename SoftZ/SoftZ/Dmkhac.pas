﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmkhac;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls, Db,
  Wwdbgrid2, ADODb, wwdblook, AppEvnts,
  Menus, AdvMenus, DBCtrls, Grids, Wwdbigrd, Wwdbgrid, ToolWin;

type
  TFrmDmkhac = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    ToolButton12: TToolButton;
    QrDmkhac: TADOQuery;
    DsDmkhac: TDataSource;
    CmdSearch: TAction;
    QrDmkhacX: TADOQuery;
    DsDmkhacX: TDataSource;
    Panel1: TPanel;
    CbKhac: TwwDBLookupCombo;
    Label1: TLabel;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    QrDmkhacDGIAI: TWideStringField;
    QrDmkhacLOAI: TWideStringField;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    TntDBMemo1: TDBMemo;
    CmdAdmin: TAction;
    QrDmkhacCREATE_BY: TIntegerField;
    QrDmkhacUPDATE_BY: TIntegerField;
    QrDmkhacCREATE_DATE: TDateTimeField;
    QrDmkhacUPDATE_DATE: TDateTimeField;
    DMKHAC_EXISTS_DGIAI: TADOCommand;
    QrDmkhacMA: TIntegerField;
    QrLast: TADOQuery;
    WideStringField1: TWideStringField;
    WideStringField2: TWideStringField;
    IntegerField1: TIntegerField;
    IntegerField2: TIntegerField;
    DateTimeField1: TDateTimeField;
    DateTimeField2: TDateTimeField;
    IntegerField3: TIntegerField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDmkhacBeforePost(DataSet: TDataSet);
    procedure QrDmkhacBeforeDelete(DataSet: TDataSet);
    procedure QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrDmkhacBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbKhacNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure QrDmkhacAfterInsert(DataSet: TDataSet);
    procedure TntDBMemo1Exit(Sender: TObject);
    procedure CmdAdminExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDmkhacDGIAIValidate(Sender: TField);
  private
  	mCanEdit: Boolean;
    mDef: String;
    procedure OpenQueries;
    function  BarcodeIsUsed(pDGIAI: String): Boolean;
  public
  	procedure Execute (r : WORD; pDef: String = '');
  end;

var
  FrmDmkhac: TFrmDmkhac;

implementation

uses
	isDb, isMsg, ExCommon, Rights, isLib, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.Execute;
begin
	mCanEdit := rCanEdit (r);
    mDef := pDef;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1('_IDI_BOOKS');
    SetCustomGrid('DM_KHAC', GrList);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdNewExecute(Sender: TObject);
begin
	QrDmKhac.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdSaveExecute(Sender: TObject);
begin
	QrDmKhac.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdCancelExecute(Sender: TObject);
begin
	QrDmKhac.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdDelExecute(Sender: TObject);
begin
	QrDmKhac.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([QrDmKhac, QrDmKhacX]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.OpenQueries;
begin
	OpenDataSets([QrDmKhacX]);
    if mDef <> '' then
        CbKhac.LookupValue := mDef
    else
        CbKhac.LookupValue := QrDmKhacX.FieldByName('LOAI').AsString;
    
	with QrDmKhac do
    begin
    	Close;
    	Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
	OpenQueries;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkhac, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDmKhac, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.QrDmkhacAfterInsert(DataSet: TDataSet);
var
    MA: Integer;
begin
	with QrDmKhac do
    begin
        with QrLast do
        begin
            if Active then
                Close;

            Parameters[0].Value := CbKhac.LookupValue;
            Open;
            MA := (FieldByName('MA').AsInteger + 1);
        end;

        FieldByName('LOAI').AsString := QrDmkhacX.FieldByName('LOAI').AsString;
        FieldByName('MA').AsInteger := MA;
        FieldByName('DGIAI').Clear;
	end;        
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.QrDmkhacBeforePost(DataSet: TDataSet);
begin

    begin
        if BlankConfirm(QrDmKhac, ['DGIAI']) then
    		Abort;
    end;
    SetAudit(QrDmKhac);

end;

procedure TFrmDmkhac.QrDmkhacDGIAIValidate(Sender: TField);
begin
    with QrDmkhac do
    if Sender.AsString <> '' then
        if BarcodeIsUsed(Sender.AsString) then
            Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.QrDmkhacBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
        
	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.QrDmkhacPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDmkhac);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.QrDmkhacBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDMKHAC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CbKhacNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.TntDBMemo1Exit(Sender: TObject);
begin
	QrDmkhacx.CheckBrowseMode
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.CmdAdminExecute(Sender: TObject);
begin
	if DsDmkhacx.AutoEdit then
    	Exit;
    DsDmkhacx.AutoEdit := FixLogon;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_BARCODE_USED = 'Đơn vị tính không được trùng';
function TFrmDmkhac.BarcodeIsUsed(pDGIAI: String): Boolean;
begin
    with DMKHAC_EXISTS_DGIAI do
    begin
        Prepared := True;
        Parameters[1].Value := pDGIAI;

        Execute;
        Result := Parameters[0].Value <> 0;
    end;
    if Result then
        ErrMsg(RS_BARCODE_USED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkhac.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

end.
