﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsKH;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Db, ADODB, Buttons, Grids,
  Wwdbigrd, Wwdbgrid2, ActnList, Wwkeycb, Wwdbgrid;

type
  TFrmChonDsKH = class(TForm)
    QrDMKH: TADOQuery;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Action: TActionList;
    CmdIns: TAction;
    CmdDel: TAction;
    CmdClear: TAction;
    DsDMKH: TDataSource;
    Label1: TLabel;
    EdMaSearch: TwwIncrementalSearch;
    GrListSearch: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    GrList: TStringGrid;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CmdInsExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdClearExecute(Sender: TObject);
    procedure GrListSearchColumnMoved(Sender: TObject; FromIndex,
      ToIndex: Integer);
    procedure EdMaSearchKeyPress(Sender: TObject; var Key: Char);
    procedure EdMaSearchPerformCustomSearch(Sender: TObject;
      LookupTable: TDataSet; SearchField, SearchValue: string;
      PerformLookup: Boolean; var Found: Boolean);
  private

  public
  	function  Get(var pLst: String): Boolean;
  end;

var
  FrmChonDsKH: TFrmChonDsKH;

implementation

uses
	isDb, isLib, isStr;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DSKH', GrListSearch);
	OpenDataSets([QrDMKH]);
    EdMaSearch.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsKH.Get;
var
	i: Integer;
begin
    Result := ShowModal = mrOK;
    if Result then
    begin
        pLst := '';
        with GrList do
	        for i := 0 to RowCount - 1 do
    	    begin
        		if Cells[0, i] = '' then
            		Break;
                if pLst <> '' then
                    pLst := pLst + ',';
                pLst := pLst + '''' + Cells[0, i] + '''';
            end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.CmdInsExecute(Sender: TObject);
var
	i: Integer;
	s1, s2: String;
begin
    with QrDMKH do
    begin
        s1 := FieldByName('MADT').AsString;
        s2 := FieldByName('TENDT').AsString
    end;

    with GrList do
    begin
    	for i := 0 to RowCount - 1 do
        begin
        	if Cells[0, i] = '' then
	        begin
				Cells[0, i] := s1;
				Cells[1, i] := s2;
	        	Row := i;
                Break;
	        end;

        	if Cells[0, i] = s1 then
            begin
		        Row := i;
                Break;
        	end
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.CmdDelExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		if Cells[0, Row] = '' then
			Exit;

		for i := Row to RowCount - 2 do
        begin
        	Cells[0, i] := Cells[0, i + 1];
        	Cells[1, i] := Cells[1, i + 1];
        end;
		Cells[0, RowCount - 1] := '';
        Cells[1, RowCount - 1] := '';
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	s: String;
begin
    s := EdMaSearch.Text;
    CmdIns.Enabled := not QrDMKH.IsEmpty;
    CmdDel.Enabled := GrList.Cells[0, GrList.Row] <> '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.FormCreate(Sender: TObject);
begin
    with GrList do
    	ColWidths[1] := Width - ColWidths[0];
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDMKH]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.CmdClearExecute(Sender: TObject);
var
	i: Integer;
begin
	with GrList do
    begin
		for i := 0 to RowCount - 1 do
        begin
        	Cells[0, i] := '';
        	Cells[1, i] := '';
        end;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.GrListSearchColumnMoved(Sender: TObject; FromIndex,
  ToIndex: Integer);
var
    s: String;
begin
    s := GrListSearch.Columns[0].FieldName;
    EdMaSearch.SearchField := s;
    QrDMKH.Sort := s;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.EdMaSearchKeyPress(Sender: TObject; var Key: Char);
var
    s: String;
begin
    if (Key = #13) or (Key = #8) then
    begin
        s := (Sender as TwwIncrementalSearch).Text;

        with (Sender as TwwIncrementalSearch).DataSource.DataSet do
        begin
            if s = '' then
                Filter := '';
            Filtered := s <> '';
            First;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsKH.EdMaSearchPerformCustomSearch(Sender: TObject;
  LookupTable: TDataSet; SearchField, SearchValue: string;
  PerformLookup: Boolean; var Found: Boolean);
begin
    exPerformCustomSearch(LookupTable, SearchField, SearchValue);
end;

end.
