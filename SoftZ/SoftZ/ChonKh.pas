﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonKh;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh,
  DbLookupComboboxEh2, Variants, kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmChonKh = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsKHACHHANG: TDataSource;
    QrKHACHHANG: TADOQuery;
    QrKHACHHANGKHOA: TIntegerField;
    QrKHACHHANGNGAY: TDateTimeField;
    QrKHACHHANGSCT: TWideStringField;
    QrKHACHHANGMAKHO: TWideStringField;
    QrKHACHHANGDGIAI: TWideMemoField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMKH: TADOQuery;
    QrKHACHHANGTENKHO: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    QrKHACHHANGMAKH: TWideStringField;
    QrKHACHHANGNG_GIAO: TWideStringField;
    QrKHACHHANGNG_NHAN: TWideStringField;
    QrKHACHHANGTENKH: TWideStringField;
    QrKHACHHANGHAN_TTOAN: TIntegerField;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    DsDMKHO: TDataSource;
    DsDMKH: TDataSource;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mKH, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pFix: Boolean; pKH, pKho: String): Integer;
  end;

var
  FrmChonKh: TFrmChonKh;

implementation

uses
	isDb, ExCommon, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonKh.Execute;
begin
	mKH := pKH;
    mKho := pKho;
    CbbDonVi.Enabled := not pFix;

	if ShowModal = mrOK then
		Result := QrKHACHHANG.FieldByName('KHOA').AsInteger
    else
		Result := 0;

	CloseDataSets([QrKHACHHANG, QrDMKH, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_KHACHHANG', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormShow(Sender: TObject);
begin
   	OpenDataSets([QrKHACHHANG, QrDMKH, QrDMKHO]);
	mSQL := QrKHACHHANG.SQL.Text;
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Smart focus
    if mKH = '' then
    	try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mKH := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsKHACHHANG);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.CmdRefreshExecute(Sender: TObject);
var
    s, sDonVi, sKhoHang: String;
    Tungay, Denngay: TDateTime;
begin
    sDonVi := EdMADV.Text;
    sKhoHang := EdMaKho.Text;

    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mKH <> sDonVi)          or (mKHO <> sKhoHang)         then
	begin
   	    mKH := sDonVi;
        mKHO := sKhoHang;

        with QrKHACHHANG do
        begin
			Close;

            s := mSQL;
            if mKH <> '' then
            	s := s + 'and a.MAKH=''' + mKH + '''';
			if mKHO <> '' then
            	s := s + 'and a.MAKHO=''' + mKHO + '''';
            SQL.Text := s;

            Parameters[0].Value := mTungay;
            Parameters[1].Value := mDenngay;
            
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonKh.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mKH;
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

end.
