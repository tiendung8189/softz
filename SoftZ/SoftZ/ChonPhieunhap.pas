﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieunhap;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook, ComCtrls, wwdbdatetimepicker, Grids,
  Wwdbigrd, Wwdbgrid, Vcl.Mask, wwdbedit, AdvEdit, DBAdvEd, DBGridEh, DBCtrlsEh,
  DBLookupEh, DbLookupComboboxEh2, Variants, kbmMemTable, MemTableDataEh,
  MemTableEh;

type
  TFrmChonPhieunhap = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPHIEUNHAP: TDataSource;
    QrPHIEUNHAP: TADOQuery;
    QrPHIEUNHAPNGAY: TDateTimeField;
    QrPHIEUNHAPSCT: TWideStringField;
    QrPHIEUNHAPMADT: TWideStringField;
    QrPHIEUNHAPMAKHO: TWideStringField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMNCC: TADOQuery;
    QrPHIEUNHAPTENKHO: TWideStringField;
    QrPHIEUNHAPTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUNHAPHOADON_SO: TWideStringField;
    QrPHIEUNHAPHOADON_NGAY: TDateTimeField;
    QrPHIEUNHAPPHIEUGIAOHANG: TWideStringField;
    QrPHIEUNHAPKHOA: TGuidField;
    QrDMNCCMADT: TWideStringField;
    QrDMNCCTENDT: TWideStringField;
    QrPHIEUNHAPGhiChu: TWideMemoField;
    QrPHIEUNHAPNguoiGiao: TWideStringField;
    QrPHIEUNHAPNguoiNhan: TWideStringField;
    QrPHIEUNHAPThueSuat: TFloatField;
    QrPHIEUNHAPHanThanhToan: TIntegerField;
    QrPHIEUNHAPSoLuong: TFloatField;
    QrPHIEUNHAPThanhToan: TFloatField;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    DsDMKHO: TDataSource;
    DsDMNCC: TDataSource;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CbMANCCKeyPress(Sender: TObject; var Key: Char);
    procedure QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
    mDelete: Boolean;
  public
  	function Execute(pFix: Boolean; pNcc, pKho: String; bDelete: Boolean = False): TGUID;
  end;

var
  FrmChonPhieunhap: TFrmChonPhieunhap;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieunhap.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    mDelete := bDelete;


	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUNHAP.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;

    SetDisplayFormat(QrPHIEUNHAP, sysCurFmt);
    SetDisplayFormat(QrPHIEUNHAP, ['NGAY'], DateTimeFmt);

    SetCustomGrid('CHON_PHIEUNHAP', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormShow(Sender: TObject);
begin
	mSQL := QrPHIEUNHAP.SQL.Text;

	OpenDataSets([QrPHIEUNHAP, QrDMNCC, QrDMKHO]);
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
    // Smart focus
    if mNCC = '' then
    	try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;


    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPHIEUNHAP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CmdRefreshExecute(Sender: TObject);
var
	s, sDonVi, sKhoHang: String;
    Tungay, Denngay: TDateTime;
begin
    sDonVi := EdMADV.Text;
    sKhoHang := EdMaKho.Text;
    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if (mTungay <> Tungay)  or (mDenngay <> Denngay) or
       (mNCC <> sDonVi)        or (mKHO <> sKhoHang)         then
	begin
   	    mNCC := sDonVi;
        mKHO := sKhoHang;
        mTungay := Tungay;
        mDenngay := Denngay;

        with QrPHIEUNHAP do
        begin
			Close;

            s := mSQL;
            if mNCC <> '' then
            	s := s + ' and a.MADT=''' + mNCC + '''';
			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
            if not mDelete then
                s := s + ' and isnull(a.DELETE_BY, 0) = 0';

            SQL.Text := s;
            SQL.Add(' order by NGAY, SCT');
			Open;
        end;
    end;
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKHO;
        FieldByName('Madt').AsString := mNCC;
        FieldByName('TuNgay').AsDateTime := Date - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.CbMANCCKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieunhap.QrPHIEUNHAPBeforeOpen(DataSet: TDataSet);
begin
    with QrPHIEUNHAP do
    begin
    	Parameters[0].Value := mTungay;
        Parameters[1].Value := mDenngay;
    end;
end;

end.
