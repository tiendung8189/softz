﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BkgrOption;

interface

uses
  Classes, Graphics, Controls, Forms, StdCtrls, Buttons, fcColorCombo,
  Wwdbcomb, ExtCtrls, Mask, wwdbedit, Wwdotdot, fcCombo;

const
	BKGR_DRAW_STYLE			= 3;
    BKGR_TRANSPARENT        = False;
    BKGR_TRANSPARENT_COLOR	= clNone;

    BKGR_SHOWTEXT			= True;
    BKGR_TEXT_COLOR			= $00966464;
    BKGR_TEXT_HIGHLIGHT		= clWhite;
    BKGR_TEXT_SHADE			= clBtnShadow;
    BKGR_TEXT_STYLE			= 3;
    BKGR_COLOR              = clWhite;

type
  TFrmBkgrOption = class(TForm)
    GroupBox1: TGroupBox;
    Label1: TLabel;
    CmdOK: TBitBtn;
    BitBtn3: TBitBtn;
    CbFontColor: TfcColorCombo;
    CkShow: TCheckBox;
    GroupBox2: TGroupBox;
    CbStyle: TwwDBComboBox;
    Label5: TLabel;
    CbColor: TfcColorCombo;
    CmdDefault: TBitBtn;
    Panel1: TPanel;
    LbXemtruoc: TLabel;
    ChkChon: TCheckBox;
    CbBkgrColor: TfcColorCombo;
    Label6: TLabel;
    procedure FormShow(Sender: TObject);
    procedure CbFontColorChange(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CkShowClick(Sender: TObject);
    procedure CmdDefaultClick(Sender: TObject);
  private
  public
	function Execute: Boolean;
  end;

var
  FrmBkgrOption: TFrmBkgrOption;

implementation

uses
	isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmBkgrOption.Execute;
begin
	Result := ShowModal = mrOK;
    if Result then
	// Save configure
		RegWrite('Background',
        	[
            	'Transparent', 'Transparent Color', 'Draw Style',
            	'Text Color', 'Show Text', 'Background Color'
			],
            [
            	ChkChon.Checked, CbColor.SelectedColor, CbStyle.ItemIndex,
                CbFontColor.SelectedColor, CkShow.Checked, CbBkgrColor.SelectedColor
			]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;

	// Load configure
    ChkChon.Checked := RegReadBool('Background', 'Transparent');
	CbColor.SelectedColor := RegRead('Transparent Color', BKGR_TRANSPARENT_COLOR);
    CbStyle.ItemIndex := RegRead('Draw Style', BKGR_DRAW_STYLE);
	CbFontColor.SelectedColor := RegRead('Text Color', BKGR_TEXT_COLOR);
	CkShow.Checked := RegRead('Show Text', BKGR_SHOWTEXT);
	CbBkgrColor.SelectedColor := RegRead('Background Color', BKGR_COLOR);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.FormClose(Sender: TObject;
  var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.CbFontColorChange(Sender: TObject);
var
	n: Integer;
	colo: TColor;
begin
	n := (Sender as TComponent).Tag;
	if n < 0 then
    	colo := 0
    else
		colo := (Sender as TfcColorCombo).SelectedColor;

    case n of
    0:
        LbXemtruoc.Font.Color := colo;
    3:
        Panel1.Color := colo;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.CkShowClick(Sender: TObject);
begin
	LbXemtruoc.Visible := CkShow.Checked
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBkgrOption.CmdDefaultClick(Sender: TObject);
begin
    ChkChon.Checked := BKGR_TRANSPARENT;
    CbColor.SelectedColor := BKGR_TRANSPARENT_COLOR;
    CbStyle.ItemIndex := BKGR_DRAW_STYLE;

    CkShow.Checked := BKGR_SHOWTEXT;
    CbFontColor.SelectedColor := BKGR_TEXT_COLOR;
    CbFontColor.OnChange(CbFontColor);

    CbBkgrColor.SelectedColor := BKGR_COLOR;
    CbBkgrColor.OnChange(CbBkgrColor);
end;

end.
