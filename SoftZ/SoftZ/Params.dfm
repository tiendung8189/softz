object FrmParams: TFrmParams
  Left = 426
  Top = 218
  HelpContext = 1
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'C'#7845'u H'#236'nh'
  ClientHeight = 310
  ClientWidth = 560
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000000000000680500001600000028000000100000002000
    0000010008000000000040010000000000000000000000000000000000000000
    0000000080000080000000808000800000008000800080800000C0C0C000C0DC
    C000F0CAA60004040400080808000C0C0C0011111100161616001C1C1C002222
    220029292900555555004D4D4D004242420039393900807CFF005050FF009300
    D600FFECCC00C6D6EF00D6E7E70090A9AD000000330000006600000099000000
    CC00003300000033330000336600003399000033CC000033FF00006600000066
    330000666600006699000066CC000066FF000099000000993300009966000099
    99000099CC000099FF0000CC000000CC330000CC660000CC990000CCCC0000CC
    FF0000FF660000FF990000FFCC00330000003300330033006600330099003300
    CC003300FF00333300003333330033336600333399003333CC003333FF003366
    00003366330033666600336699003366CC003366FF0033990000339933003399
    6600339999003399CC003399FF0033CC000033CC330033CC660033CC990033CC
    CC0033CCFF0033FF330033FF660033FF990033FFCC0033FFFF00660000006600
    330066006600660099006600CC006600FF006633000066333300663366006633
    99006633CC006633FF00666600006666330066666600666699006666CC006699
    00006699330066996600669999006699CC006699FF0066CC000066CC330066CC
    990066CCCC0066CCFF0066FF000066FF330066FF990066FFCC00CC00FF00FF00
    CC009999000099339900990099009900CC009900000099333300990066009933
    CC009900FF00996600009966330099336600996699009966CC009933FF009999
    330099996600999999009999CC009999FF0099CC000099CC330066CC660099CC
    990099CCCC0099CCFF0099FF000099FF330099CC660099FF990099FFCC0099FF
    FF00CC00000099003300CC006600CC009900CC00CC0099330000CC333300CC33
    6600CC339900CC33CC00CC33FF00CC660000CC66330099666600CC669900CC66
    CC009966FF00CC990000CC993300CC996600CC999900CC99CC00CC99FF00CCCC
    0000CCCC3300CCCC6600CCCC9900CCCCCC00CCCCFF00CCFF0000CCFF330099FF
    6600CCFF9900CCFFCC00CCFFFF00CC003300FF006600FF009900CC330000FF33
    3300FF336600FF339900FF33CC00FF33FF00FF660000FF663300CC666600FF66
    9900FF66CC00CC66FF00FF990000FF993300FF996600FF999900FF99CC00FF99
    FF00FFCC0000FFCC3300FFCC6600FFCC9900FFCCCC00FFCCFF00FFFF3300CCFF
    6600FFFF9900FFFFCC006666FF0066FF660066FFFF00FF666600FF66FF00FFFF
    66002100A5005F5F5F00777777008686860096969600CBCBCB00B2B2B200D7D7
    D700DDDDDD00E3E3E300EAEAEA00F1F1F100F8F8F800F0FBFF00A4A0A0008080
    80000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF000A0A
    0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A0A012524240A0A0A29A0
    385E3838383138017A2C240A0A0A29A0E5385E38383831017A2C240A0A0A29A0
    38E5385E383838017A2C240A0A0A29A0A038E5385E3838017A2C240A0A0A29A0
    38A038E5385E38017A2C240A0A0A29A0A0A0A0A0A0A0A001A02C240A0A0A2929
    2929292929292901C37A240A0A0A0A29F6A0A0990A0A0A030101010A0A0A0A0A
    2929290A0A0A0A0A14090A0A0A0A0A0A0A0A0A0A0AEB150A14090A0A0A0A0A0A
    0A0A0A0AEB191514F809860A860A0A0A0A0A0A0AEBFFBCBCBCFFBCBC090A0A0A
    0A0A0A0AEBFF090AFFFF19190A0A0A0A0A0A0A0A0AEBEBF2EBEBEBEB0A0AFFC7
    0000000300000003000000030000000300000003000000030000000300000003
    000081870000C3C70000FE440000FC000000FC000000FC010000FE030000}
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Inspect: TwwDataInspector
    Left = 15
    Top = 8
    Width = 537
    Height = 251
    DisableThemes = False
    Ctl3D = True
    ParentCtl3D = False
    TabOrder = 0
    DataSource = DsPARAMS
    Items = <
      item
        DataSource = DsPARAMS
        DataField = 'HEADER1'
        Caption = ' T'#234'n c'#244'ng ty'
        ReadOnly = True
        TabStop = False
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        DataField = 'HEADER2'
        Caption = ' '#272#7883'a ch'#7881
        ReadOnly = True
        TabStop = False
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        DataField = 'HEADER3'
        Caption = ' '#272'i'#7879'n tho'#7841'i - Fax'
        ReadOnly = True
        TabStop = False
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        DataField = 'MST'
        Caption = ' M'#227' s'#7889' thu'#7871
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' '#272#7883'nh d'#7841'ng - Nh'#7853'p li'#7879'u'
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_CUR'
            Caption = 'S'#7889' ti'#7873'n (VND)'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_USD'
            Caption = 'S'#7889' ti'#7873'n (USD)'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_QTY'
            Caption = 'S'#7889' l'#432#7907'ng'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_DATE'
            Caption = 'Ng'#224'y'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_THOUSAND_SEP'
            Caption = 'D'#7845'u c'#225'ch ng'#224'n'
            Expanded = True
            PickList.Items.Strings = (
              ', (d'#7845'u ph'#7849'y)'#9','
              '. (d'#7845'u ch'#7845'm)'#9'.')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_GMS_DECIMAL_SEP'
            Caption = 'D'#7845'u c'#225'ch th'#7853'p ph'#226'n'
            PickList.Items.Strings = (
              '. (d'#7845'u ch'#7845'm)'#9'.'
              ', (d'#7845'u ph'#7849'y)'#9',')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end>
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' '#272#7883'nh d'#7841'ng - B'#225'n l'#7867
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'FMT_POS_CUR'
            Caption = 'S'#7889' ti'#7873'n'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_POS_QTY'
            Caption = 'S'#7889' l'#432#7907'ng'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_POS_DATE'
            Caption = 'Ng'#224'y'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_POS_THOUSAND_SEP'
            Caption = 'D'#7845'u c'#225'ch ng'#224'n'
            PickList.Items.Strings = (
              ', (d'#7845'u ph'#7849'y)'#9','
              '. (d'#7845'u ch'#7845'm)'#9'.')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FMT_POS_DECIMAL_SEP'
            Caption = 'D'#7845'u c'#225'ch th'#7853'p ph'#226'n'
            PickList.Items.Strings = (
              '. (d'#7845'u ch'#7845'm)'#9'.'
              ', (d'#7845'u ph'#7849'y)'#9',')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'ROUNDING_CUR'
            Caption = 'L'#224'm tr'#242'n gi'#225' b'#225'n l'#7867
            PickList.Items.Strings = (
              '0.0001'#9'-4'
              '0.001'#9'-3'
              '0.01'#9'-2'
              '0.1'#9'-1'
              '1'#9'0'
              '10'#9'1'
              '100'#9'2'
              '1,000'#9'3'
              '10,000'#9'4'
              '100,000'#9'5'
              '1,000,000'#9'6')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end>
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' '#272#7883'nh d'#7841'ng - B'#225'o c'#225'o'
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'FT_THOUSAND_SEPARATOR'
            Caption = 'D'#7845'u c'#225'ch ng'#224'n'
            PickList.Items.Strings = (
              ', (d'#7845'u ph'#7849'y)'#9','
              '. (d'#7845'u ch'#7845'm)'#9'.')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FT_DECIMAL_SEPARATOR'
            Caption = 'D'#7845'u c'#225'ch th'#7853'p ph'#226'n'
            PickList.Items.Strings = (
              '. (d'#7845'u ch'#7845'm)'#9'.'
              ', (d'#7845'u ph'#7849'y)'#9',')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FT_CUR_DECIMALS'
            Caption = #272#7883'nh d'#7841'ng s'#7889' ti'#7873'n'
            PickList.Items.Strings = (
              '1'#9'0'
              '1.0'#9'1'
              '1.00'#9'2'
              '1.000'#9'3'
              '1.0000'#9'4'
              '1.00000'#9'5'
              '1.000000'#9'6'
              '1.0000000'#9'7'
              '1.00000000'#9'8'
              '1.000000000'#9'9'
              '1.0000000000'#9'10')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FT_CUR_ROUNDING'
            Caption = 'L'#224'm tr'#242'n s'#7889' ti'#7873'n'
            PickList.Items.Strings = (
              '0.0001'#9'4'
              '0.001'#9'3'
              '0.01'#9'2'
              '0.1'#9'1'
              '1'#9'0'
              '10'#9'-1'
              '100'#9'-2'
              '1,000'#9'-3'
              '10,000'#9'-4'
              '100,000'#9'-5'
              '1,000,000'#9'-6')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FT_QTY_DECIMALS'
            Caption = #272#7883'nh d'#7841'ng s'#7889' l'#432#7907'ng'
            PickList.Items.Strings = (
              '1'#9'0'
              '1.0'#9'1'
              '1.00'#9'2'
              '1.000'#9'3'
              '1.0000'#9'4'
              '1.00000'#9'5'
              '1.000000'#9'6'
              '1.0000000'#9'7'
              '1.00000000'#9'8'
              '1.000000000'#9'9'
              '1.0000000000'#9'10')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'FT_QTY_ROUNDING'
            Caption = 'L'#224'm tr'#242'n s'#7889' l'#432#7907'ng'
            PickList.Items.Strings = (
              '0.0001'#9'4'
              '0.001'#9'3'
              '0.01'#9'2'
              '0.1'#9'1'
              '1'#9'0'
              '10'#9'-1'
              '100'#9'-2'
              '1,000'#9'-3'
              '10,000'#9'-4'
              '100,000'#9'-5'
              '1,000,000'#9'-6')
            PickList.MapList = True
            PickList.Style = csDropDownList
            WordWrap = False
          end>
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' M'#7863'c '#273#7883'nh'
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'IS_CENTRAL'
            Caption = 'Kho trung t'#226'm'
            ReadOnly = True
            PickList.Items.Strings = (
              'True'
              'False')
            PickList.DisplayAsCheckbox = True
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_MAKHO'
            Caption = 'M'#227' kho'
            CustomControl = CbMAKHO
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_DVT'
            Caption = #272#417'n v'#7883' t'#237'nh'
            CustomControl = CbDVT
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_PTNHAP'
            Caption = 'Ph'#432#417'ng th'#7913'c nh'#7853'p'
            CustomControl = CbPTNHAP
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_PTXUAT'
            Caption = 'Ph'#432#417'ng th'#7913'c xu'#7845't'
            CustomControl = CbPTXUAT
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_PTTT'
            Caption = 'Ph'#432#417'ng th'#7913'c thanh to'#225'n'
            CustomControl = CbPTTT
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_LYDO_THU'
            Caption = 'L'#253' do thu'
            CustomControl = CbLYDO_THU
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_LYDO_CHI'
            Caption = 'L'#253' do chi'
            CustomControl = CbLYDO_CHI
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'DEFAULT_LATE_DAY'
            Caption = #272#7897' tr'#7877' nh'#7853'p li'#7879'u (ng'#224'y)'
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'BL_DONVI_TIENTHOI'
            Caption = #272#417'n v'#7883' ti'#7873'n th'#7889'i b'#225'n l'#7867
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'BL_NHACNHO_TIENTHOI'
            Caption = 'Nh'#7855'c nh'#7903' nh'#226'n '#273#417'n v'#7883' ti'#7873'n th'#7889'i '
            PickList.Items.Strings = (
              'True'
              'False')
            PickList.DisplayAsCheckbox = True
            WordWrap = False
          end>
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' Th'#432' m'#7909'c'
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'FOLDER_REPORT'
            Caption = 'B'#225'o c'#225'o'
            PickList.ButtonStyle = cbsEllipsis
            WordWrap = False
            OnEditButtonClick = SelectFolder
          end
          item
            DataSource = DsPARAMS
            DataField = 'FOLDER_EXPORT'
            Caption = 'L'#432'u d'#7919' li'#7879'u g'#7903'i '#273'i'
            PickList.ButtonStyle = cbsEllipsis
            WordWrap = False
            OnEditButtonClick = SelectFolder
          end
          item
            DataSource = DsPARAMS
            DataField = 'FOLDER_IMPORT'
            Caption = 'L'#432'u d'#7919' li'#7879'u nh'#7853'n v'#7873
            PickList.ButtonStyle = cbsEllipsis
            WordWrap = False
            OnEditButtonClick = SelectFolder
          end
          item
            DataSource = DsPARAMS
            DataField = 'FOLDER_BARCODE'
            Caption = 'Xu'#7845't d'#7919' li'#7879'u in tem'
            PickList.ButtonStyle = cbsEllipsis
            WordWrap = False
            OnEditButtonClick = SelectFolder
          end>
        WordWrap = False
      end
      item
        DataSource = DsPARAMS
        Caption = ' C'#224'i '#273#7863't kh'#225'c'
        ReadOnly = True
        Items = <
          item
            DataSource = DsPARAMS
            DataField = 'SEARCH_MATCH_ANY'
            Caption = ' T'#236'm, tra c'#7913'u theo chu'#7895'i con'
            PickList.Items.Strings = (
              'True'
              'False')
            PickList.DisplayAsCheckbox = True
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            Caption = ' B'#7855't '#273#7847'u s'#7917' d'#7909'ng ch'#432#417'ng tr'#236'nh'
            Items = <
              item
                DataSource = DsPARAMS
                DataField = 'BEGIN_BALANCE'
                Caption = ' Cho ph'#233'p nh'#7853'p s'#7889' d'#432' c'#244'ng n'#7907
                PickList.Items.Strings = (
                  'True'
                  'False')
                PickList.DisplayAsCheckbox = True
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'BEGIN_MONTH'
                Caption = ' Th'#225'ng'
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'BEGIN_YEAR'
                Caption = ' N'#259'm'
                WordWrap = False
              end>
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            Caption = ' In m'#227' v'#7841'ch'
            Items = <
              item
                DataSource = DsPARAMS
                DataField = 'STAMP_PREFIX'
                Caption = 'Ti'#7873'n t'#7889' m'#227' v'#7841'ch in m'#227' v'#7841'ch'
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'STAMP_OF_PAGE'
                Caption = ' S'#7889' m'#227' v'#7841'ch tr'#234'n m'#7897't trang'
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'STAMP_CSV_HEADER'
                Caption = ' Xu'#7845't header d'#7919' li'#7879'u in m'#227' v'#7841'ch'
                PickList.Items.Strings = (
                  'True'
                  'False')
                PickList.DisplayAsCheckbox = True
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'STAMP_OVERWRITE'
                Caption = ' Ghi '#273#232' l'#234'n file m'#227' v'#7841'ch '#273#227' c'#243' s'#7861'n'
                PickList.Items.Strings = (
                  'True'
                  'False')
                PickList.DisplayAsCheckbox = True
                WordWrap = False
              end
              item
                DataSource = DsPARAMS
                DataField = 'STAMP_UTF8'
                Caption = ' M'#227' UTF8'
                PickList.Items.Strings = (
                  'True'
                  'False')
                PickList.DisplayAsCheckbox = True
                WordWrap = False
              end>
            WordWrap = False
          end
          item
            DataSource = DsPARAMS
            DataField = 'BL_SOGIO_TRAHANG'
            Caption = ' Th'#7901'i gian '#273#7883'nh tr'#7843' h'#224'ng b'#225'n l'#7867' (s'#7889' gi'#7901')'
            WordWrap = False
          end>
        WordWrap = False
      end>
    CaptionWidth = 283
    Options = [ovColumnResize, ovEnterToTab, ovCenterCaptionVert, ovShowCellHints]
    PaintOptions.AlternatingRowRegions = [arrDataColumns, arrActiveDataColumn]
    PaintOptions.AlternatingRowColor = 15794175
    PaintOptions.BackgroundOptions = [coFillDataCells]
    CaptionFont.Charset = ANSI_CHARSET
    CaptionFont.Color = clBlack
    CaptionFont.Height = -13
    CaptionFont.Name = 'Tahoma'
    CaptionFont.Style = []
    LineStyleCaption = ovDottedLine
    LineStyleData = ovDottedLine
    object CbMAKHO: TwwDBLookupCombo
      Left = 227
      Top = 174
      Width = 231
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MAKHO'#9'4'#9'MAKHO'#9'F'
        'TENKHO'#9'30'#9'TENKHO'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrDMKHO
      LookupField = 'MAKHO'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 5
      Visible = False
      AutoDropDown = False
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
      OnBeforeDropDown = CbMAKHOBeforeDropDown
      OnCloseUp = CbMAKHOCloseUp
      OnNotInList = CbMAKHONotInList
    end
    object CbPTNHAP: TwwDBLookupCombo
      Left = 179
      Top = 106
      Width = 231
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'4'#9'MA'#9'F'
        'DGIAI'#9'30'#9'DGIAI'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrPTNHAP
      LookupField = 'MA'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 1
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object CbPTXUAT: TwwDBLookupCombo
      Left = 155
      Top = 106
      Width = 231
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'4'#9'MA'#9'F'
        'DGIAI'#9'30'#9'DGIAI'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrPTXUAT
      LookupField = 'MA'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 0
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object CbDVT: TwwDBLookupCombo
      Left = 152
      Top = 144
      Width = 231
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'DGIAI'#9'32'#9'DGIAI'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrDM_DVT
      LookupField = 'MA'
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      Frame.Enabled = True
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      ParentCtl3D = False
      ParentShowHint = False
      ShowHint = False
      TabOrder = 3
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = True
      ShowMatchText = True
    end
    object CbLYDO_THU: TwwDBLookupCombo
      Left = 99
      Top = 166
      Width = 271
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'4'#9'MA'#9'F'
        'DGIAI'#9'30'#9'DGIAI'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrLYDO_THU
      LookupField = 'MA'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 4
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object CbLYDO_CHI: TwwDBLookupCombo
      Left = 99
      Top = 190
      Width = 271
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'MA'#9'4'#9'MA'#9'F'
        'DGIAI'#9'30'#9'DGIAI'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrLYDO_CHI
      LookupField = 'MA'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 6
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
    end
    object CbPTTT: TwwDBLookupCombo
      Left = 179
      Top = 138
      Width = 271
      Height = 22
      TabStop = False
      Ctl3D = False
      BorderStyle = bsNone
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'DGIAI'#9'30'#9'DGIAI'#9'F'
        'MA'#9'4'#9'MA'#9'F')
      DataSource = DsPARAMS
      LookupTable = DataMain.QrPTTT
      LookupField = 'MA'
      Options = [loColLines]
      ButtonEffects.Transparent = True
      ButtonEffects.Flat = True
      ParentCtl3D = False
      TabOrder = 2
      Visible = False
      AutoDropDown = False
      ShowButton = True
      UseTFields = False
      PreciseEditRegion = False
      AllowClearKey = False
    end
  end
  object BitBtn1: TBitBtn
    Left = 240
    Top = 275
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdSave
    Caption = 'L'#432'u'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ParentFont = False
    TabOrder = 1
  end
  object BitBtn2: TBitBtn
    Left = 344
    Top = 275
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdCancel
    Caption = 'B'#7887' qua'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFEEEDF8A6A5E06765CE4946CB4946CB6765CEA6A5E0EEEDF8FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A6E20B07CD100CED120EFE13
      0EFF130EFF120EFE100CEE0B07CDA7A6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      8887E0100EE91412FC1313FC110FE90F0DE00F0DE0110FEA1211F71312FB100E
      EA8180DFFFFFFFFFFFFFFFFFFFA7A6E41214EB1619FC1416F21F1ED4AAA9ECAE
      AEE4B5B4E73C39C71315EF1416F61518F91214EBA7A6E4FFFFFFEEEEFA1013DB
      171FFB171DF18C8CECF6F6FDFFFFFFFFFFFF918FDF1A1FE3161EF7161DF4161E
      F6171EFA1013DBEEEEFAA8A7EC151EF11B27FB2224D9FFFFFFFFFFFFFFFFFF88
      85E0141BE61924F91923F61924F5151AE71A25F8151EF2A8A7EC7373E52739FD
      1521F1AAA9EDFFFFFFFFFFFF817EE1141BE31B2CF91B2AF61B2BF81113D93B38
      CF1521F02739FD7373E56062E6536CFF202FECB4B3F1FFFFFF8885E6161EE41E
      32F81C31F61E34F9161DE4918EE5C0BFF1202FEC546CFF5F61E35F61EC607BFF
      414FF1C1BFF4928EEA1820E62139F92037F62139F81821E98985E7FFFFFFB5B4
      F2414FF1607AFF5D5FE07978F15F77FF4C60F73D37E21318E5253FFA243CF826
      41FA1B26EC837EE8FFFFFFFFFFFFADABF54C60F95C75F87675E4ABA8F55C6CFD
      5E7CFE4251F43650FA223EF82845FA202FF28A86EEFFFFFFFFFFFFFFFFFF3B40
      EE6281FF5869E9A9A7EEEEEDFD4042F57490FF5E7BFD5C78FC3D5CFB2637F393
      90F2FFFFFFFFFFFFF6F6FF9B9EF9556DFC7591FD393BD2EEEDFDFFFFFFA8A6F8
      646EFA7A94FE617EFC5A73FB4843EFB6B3F9B1AEFAB4B5FB4047F35873FC7F9D
      FF5B65DFA6A4ECFFFFFFFFFFFFFFFFFF8D89F86772F98CA8FE7695FF5A72FC4F
      64FA4E63F95870FC7D9DFF8EAAFE636EE48887DAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFA8A6F84445F17A88F793A9FD98B1FF98B2FF92A9FB7988EF4042DFA6A4
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEDFEAEACF67E7CF369
      69EF6869EE7C7CEEADABEDEEEDFDFFFFFFFFFFFFFFFFFFFFFFFF}
    ParentFont = False
    TabOrder = 2
  end
  object BitBtn3: TBitBtn
    Left = 448
    Top = 275
    Width = 101
    Height = 25
    Cursor = 1
    Action = CmdClose
    Caption = 'K'#7871't th'#250'c'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ParentFont = False
    TabOrder = 3
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 264
    Top = 126
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#7923'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdAdmin: TAction
      ShortCut = 16507
      Visible = False
      OnExecute = CmdAdminExecute
    end
  end
  object QrPARAMS: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    AfterPost = QrPARAMSAfterPost
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from SYS_CONFIG')
    Left = 264
    Top = 32
    object QrPARAMSKHOA: TAutoIncField
      FieldName = 'KHOA'
      ReadOnly = True
    end
    object QrPARAMSDEFAULT_MAKHO: TWideStringField
      FieldName = 'DEFAULT_MAKHO'
      Size = 2
    end
    object QrPARAMSIS_CENTRAL: TBooleanField
      FieldName = 'IS_CENTRAL'
    end
    object QrPARAMSSTAMP_OF_PAGE: TIntegerField
      FieldName = 'STAMP_OF_PAGE'
    end
    object QrPARAMSDEFAULT_PTNHAP: TWideStringField
      FieldName = 'DEFAULT_PTNHAP'
      Size = 2
    end
    object QrPARAMSDEFAULT_PTXUAT: TWideStringField
      FieldName = 'DEFAULT_PTXUAT'
      Size = 2
    end
    object QrPARAMSDEFAULT_PTTT: TWideStringField
      FieldName = 'DEFAULT_PTTT'
      Size = 2
    end
    object QrPARAMSDEFAULT_LATE_DAY: TIntegerField
      FieldName = 'DEFAULT_LATE_DAY'
    end
    object QrPARAMSSTOCK_DATE: TDateTimeField
      FieldName = 'STOCK_DATE'
    end
    object QrPARAMSMST: TWideStringField
      FieldName = 'MST'
      Size = 100
    end
    object QrPARAMSFOLDER_EXPORT: TWideStringField
      FieldName = 'FOLDER_EXPORT'
      Size = 250
    end
    object QrPARAMSFOLDER_IMPORT: TWideStringField
      FieldName = 'FOLDER_IMPORT'
      OnChange = QrPARAMSFOLDER_IMPORTChange
      Size = 250
    end
    object QrPARAMSFOLDER_REPORT: TWideStringField
      FieldName = 'FOLDER_REPORT'
      Size = 250
    end
    object QrPARAMSFMT_GMS_CUR: TWideStringField
      FieldName = 'FMT_GMS_CUR'
      Size = 30
    end
    object QrPARAMSFMT_GMS_USD: TWideStringField
      FieldName = 'FMT_GMS_USD'
      Size = 30
    end
    object QrPARAMSFMT_GMS_QTY: TWideStringField
      FieldName = 'FMT_GMS_QTY'
      Size = 30
    end
    object QrPARAMSFMT_GMS_DATE: TWideStringField
      FieldName = 'FMT_GMS_DATE'
      Size = 30
    end
    object QrPARAMSFMT_POS_CUR: TWideStringField
      FieldName = 'FMT_POS_CUR'
      Size = 30
    end
    object QrPARAMSFMT_POS_QTY: TWideStringField
      FieldName = 'FMT_POS_QTY'
      Size = 30
    end
    object QrPARAMSFMT_POS_DATE: TWideStringField
      FieldName = 'FMT_POS_DATE'
      Size = 30
    end
    object QrPARAMSFMT_GMS_THOUSAND_SEP: TWideStringField
      FieldName = 'FMT_GMS_THOUSAND_SEP'
      FixedChar = True
      Size = 1
    end
    object QrPARAMSFMT_GMS_DECIMAL_SEP: TWideStringField
      FieldName = 'FMT_GMS_DECIMAL_SEP'
      FixedChar = True
      Size = 1
    end
    object QrPARAMSFMT_POS_THOUSAND_SEP: TWideStringField
      FieldName = 'FMT_POS_THOUSAND_SEP'
      FixedChar = True
      Size = 1
    end
    object QrPARAMSFMT_POS_DECIMAL_SEP: TWideStringField
      FieldName = 'FMT_POS_DECIMAL_SEP'
      FixedChar = True
      Size = 1
    end
    object QrPARAMSDEFAULT_LYDO_THU: TWideStringField
      FieldName = 'DEFAULT_LYDO_THU'
      Size = 2
    end
    object QrPARAMSDEFAULT_LYDO_CHI: TWideStringField
      FieldName = 'DEFAULT_LYDO_CHI'
      Size = 2
    end
    object QrPARAMSSTAMP_CSV_HEADER: TBooleanField
      FieldName = 'STAMP_CSV_HEADER'
    end
    object QrPARAMSSTAMP_OVERWRITE: TBooleanField
      FieldName = 'STAMP_OVERWRITE'
    end
    object QrPARAMSFOLDER_BARCODE: TWideStringField
      FieldName = 'FOLDER_BARCODE'
      Size = 250
    end
    object QrPARAMSBEGIN_BALANCE: TBooleanField
      FieldName = 'BEGIN_BALANCE'
    end
    object QrPARAMSBEGIN_MONTH: TIntegerField
      FieldName = 'BEGIN_MONTH'
    end
    object QrPARAMSBEGIN_YEAR: TIntegerField
      FieldName = 'BEGIN_YEAR'
    end
    object QrPARAMSSTAMP_PREFIX: TWideStringField
      FieldName = 'STAMP_PREFIX'
      Size = 100
    end
    object QrPARAMSROUNDING_CUR: TIntegerField
      FieldName = 'ROUNDING_CUR'
    end
    object QrPARAMSSTAMP_UTF8: TBooleanField
      FieldName = 'STAMP_UTF8'
    end
    object QrPARAMSSEARCH_MATCH_ANY: TBooleanField
      FieldName = 'SEARCH_MATCH_ANY'
    end
    object QrPARAMSBL_SOGIO_TRAHANG: TFloatField
      FieldName = 'BL_SOGIO_TRAHANG'
    end
    object QrPARAMSBL_DONVI_TIENTHOI: TFloatField
      FieldName = 'BL_DONVI_TIENTHOI'
    end
    object QrPARAMSBL_NHACNHO_TIENTHOI: TBooleanField
      FieldName = 'BL_NHACNHO_TIENTHOI'
    end
    object QrPARAMSHEADER1: TWideStringField
      FieldName = 'HEADER1'
      Size = 100
    end
    object QrPARAMSHEADER2: TWideStringField
      FieldName = 'HEADER2'
      Size = 100
    end
    object QrPARAMSHEADER3: TWideStringField
      FieldName = 'HEADER3'
      Size = 100
    end
    object QrPARAMSFT_DECIMAL_SEPARATOR: TWideStringField
      FieldName = 'FT_DECIMAL_SEPARATOR'
      Size = 1
    end
    object QrPARAMSFT_THOUSAND_SEPARATOR: TWideStringField
      FieldName = 'FT_THOUSAND_SEPARATOR'
      Size = 1
    end
    object QrPARAMSFT_QTY_DECIMALS: TIntegerField
      FieldName = 'FT_QTY_DECIMALS'
    end
    object QrPARAMSFT_QTY_ROUNDING: TIntegerField
      FieldName = 'FT_QTY_ROUNDING'
    end
    object QrPARAMSFT_CUR_DECIMALS: TIntegerField
      FieldName = 'FT_CUR_DECIMALS'
    end
    object QrPARAMSFT_CUR_ROUNDING: TIntegerField
      FieldName = 'FT_CUR_ROUNDING'
    end
    object QrPARAMSDEFAULT_DVT: TIntegerField
      FieldName = 'DEFAULT_DVT'
    end
  end
  object DsPARAMS: TDataSource
    DataSet = QrPARAMS
    Left = 264
    Top = 64
  end
end
