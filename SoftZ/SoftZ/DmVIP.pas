﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmVIP;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2, wwdbedit,
  Menus, AdvMenus, AppEvnts, isPanel,
  wwfltdlg, RzSplit,
  wwdbdatetimepicker, wwDialog, Mask, ExtCtrls, RzPanel, Grids, Wwdbigrd,
  Wwdbgrid, ToolWin, Buttons, AdvEdit, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2, System.Variants;

type
  TFrmDmVIP = class(TForm)
    ActionList: TActionList;
    CmdGen: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    BtnIn: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMVIP: TADOQuery;
    DsDMVIP: TDataSource;
    Item2: TMenuItem;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    QrDMVIPMAVIP: TWideStringField;
    QrDMVIPDCHI: TWideStringField;
    QrDMVIPDTHOAI: TWideStringField;
    QrDMVIPEMAIL: TWideStringField;
    QrDMVIPCREATE_BY: TIntegerField;
    QrDMVIPUPDATE_BY: TIntegerField;
    QrDMVIPCREATE_DATE: TDateTimeField;
    QrDMVIPUPDATE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    Item0: TMenuItem;
    CmdUsed: TAction;
    CmdAll: TAction;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    CmdExport: TAction;
    GrList: TwwDBGrid2;
    CmdClearFilter: TAction;
    N1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    isPanel1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label9: TLabel;
    Label2: TLabel;
    EdHo: TwwDBEdit;
    EdDChi: TwwDBEdit;
    EdDienthoai: TwwDBEdit;
    DBEdit6: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    CmdAudit: TAction;
    QrDMVIPNGAYSINH: TDateTimeField;
    QrDMVIPDOANHSO: TFloatField;
    QrDMVIPNGAYDSO: TDateTimeField;
    CbNGAY: TwwDBDateTimePicker;
    TntLabel3: TLabel;
    CmdRefesh: TAction;
    CmdNotUsed: TAction;
    Item1: TMenuItem;
    spDM_VIP_Export: TADOStoredProc;
    PD2: TisPanel;
    TntLabel2: TLabel;
    EdNgayTG: TwwDBDateTimePicker;
    CmdDoanhSo: TAction;
    spDM_VIP_Calc_DoanhSo: TADOCommand;
    TntToolButton1: TToolButton;
    TntToolButton2: TToolButton;
    TntLabel1: TLabel;
    Label25: TLabel;
    wwDBDateTimePicker3: TwwDBDateTimePicker;
    DBEdit11: TwwDBEdit;
    QrDMVIPMAKHO: TWideStringField;
    QrDMVIPLOAI: TWideStringField;
    Label5: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label10: TLabel;
    wwDBEdit2: TwwDBEdit;
    QrDMVIPNGAY_THAMGIA: TDateTimeField;
    QrDMVIPTL_CK: TFloatField;
    QrDMVIPDOANHSO_DAU: TFloatField;
    QrDMVIPDOANHSO_MUA: TFloatField;
    QrDMVIPLOC_THAMGIA: TWideStringField;
    Label11: TLabel;
    wwDBEdit3: TwwDBEdit;
    PaDondh: TPanel;
    CmdReload: TAction;
    CmdTemnhan: TAction;
    spDM_VIP_Generate: TADOStoredProc;
    QrDMVIPMASO: TWideStringField;
    QrDMVIPHO: TWideStringField;
    QrDMVIPTEN: TWideStringField;
    QrDMVIPHOTEN: TWideStringField;
    QrDMVIPSOTIEN_MUA: TFloatField;
    QrDMVIPSOTIEN_TRA: TFloatField;
    QrDMVIPTINHTRANG: TWideStringField;
    Label7: TLabel;
    wwDBEdit4: TwwDBEdit;
    QrDMVIPCMND: TWideStringField;
    isPanel2: TisPanel;
    Item3: TMenuItem;
    Panel1: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    SpeedButton1: TSpeedButton;
    EdSearch: TAdvEdit;
    CmdHuythe: TAction;
    CmdChuyenthe: TAction;
    N3: TMenuItem;
    Chuynsangthmi1: TMenuItem;
    CmdExportExcel: TAction;
    PopPrint: TAdvPopupMenu;
    Export1: TMenuItem;
    emnhn1: TMenuItem;
    N4: TMenuItem;
    Mvch1: TMenuItem;
    N5: TMenuItem;
    CmdExportExcel1: TMenuItem;
    btnHuythe: TSpeedButton;
    CbTINHTRANG: TDbLookupComboboxEh2;
    CbLoaiVIP: TDbLookupComboboxEh2;
    CbKHO: TDbLookupComboboxEh2;
    cbbLoc: TDbLookupComboboxEh2;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdGenExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMVIPBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMVIPBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMVIPBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdExportExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure CmdNotUsedExecute(Sender: TObject);
    procedure CmdDoanhSoExecute(Sender: TObject);
    procedure QrDMVIPBeforeEdit(DataSet: TDataSet);
    procedure QrDMVIPDOANHSO_DAUChange(Sender: TField);
    procedure CmdReloadExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure CmdTemnhanExecute(Sender: TObject);
    procedure CmdHuytheExecute(Sender: TObject);
    procedure CmdChuyentheExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdExportExcelExecute(Sender: TObject);
    procedure BtnInClick(Sender: TObject);
  private
  	mCanEdit, mCreating: Boolean;
    mSql, fLoc, mSearch: String;
    mUsed: Integer;
  public
  	procedure Execute (r: WORD);
  end;

var
  FrmDmVIP: TFrmDmVIP;

implementation

uses
	ExCommon, isDb, isMsg, isStr, Rights, MainData, RepEngine, isCommon,
    Soluong, ExVip, isBarcode, isLib, isFile, OfficeData;

const
    FORM_CODE = 'DM_VIP';

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;

    // Save form state
    RegWrite(Name, 'ShowAll', IntToStr(mUsed));
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    with QrDMVIP do
    begin
	    SetDisplayFormat(QrDMVIP, sysCurFmt);
    	SetDisplayFormat(QrDMVIP, ['NGAYDSO'], ShortDateFormat + ' hh:nn');
    end;

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMVIP, FORM_CODE, Filter);
	mCreating := False;
    mSearch := '';
    mSql := QrDMVIP.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.FormShow(Sender: TObject);
begin
	DsDMVIP.AutoEdit := mCanEdit;

//	CbKho.ReadOnly := not sysIsCentral;
    CmdDoanhso.Enabled := sysIsCentral;

    with DataMain do
        OpenDataSets([QrDMKHO, QrTT_VIP, QrV_PUB_PHANHE_VIP_ALL, QrLOC]);

//    CbLoc.LookupValue := sysLoc;
    cbbLoc.Enabled := sysIsCentral;

    CbLoaiVIP.ReadOnly := sysKhttLe > 1;
    if not sysIsCentral then
        mUsed := 2
    else
        mUsed := StrToInt(RegReadString(Name, 'ShowAll', '0'));

    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        QrDMVIP.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMVIP, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NEWVIPS = 'Tạo thêm %d mã. Tiếp tục?';
    RS_INVALID_LOC_EMPTY = 'Địa điểm không được rỗng';
procedure TFrmDmVIP.CmdGenExecute(Sender: TObject);
var
	b: Boolean;
    s, pLoc: String;
    mSL: Integer;
    d: TDateTime;
begin
    if not VarIsNull(cbbLoc.Value) then
        pLoc := cbbLoc.Value
    else
        pLoc := '';

    if (pLoc = '') then
    begin
        ErrMsg(RS_INVALID_LOC_EMPTY);
        Abort;
    end;

   	mSL := Trunc(FrmSoluong.GetQuantity);
    if mSL <= 0 then
    	Exit;

    if not YesNo(Format(RS_NEWVIPS, [mSL])) then
    	Exit;

    pLoc := cbbLoc.Value;
    d := Now;
    with spDM_VIP_Generate do
    begin
        if Active then
            Close;

        Prepared := True;
        Parameters[1].Value := mSL;
        Parameters[2].Value := pLoc;
        Parameters[3].Value := 7;
        Active := True;

        if IsEmpty then
            Exit;

        mCreating := True;
        First;
        try
            while not Eof do
            begin
                QrDMVIP.Append;

                QrDMVIP.FieldByName('MASO').AsString := FieldByName('MASO').AsString;
                QrDMVIP.FieldByName('MAVIP').AsString := FieldByName('MAVIP').AsString;
                QrDMVIP.FieldByName('TINHTRANG').AsString := '01';
                QrDMVIP.FieldByName('LOC_THAMGIA').AsString := pLoc;
                QrDMVIP.FieldByName('DOANHSO_DAU').AsFloat := 0;
                QrDMVIP.FieldByName('DOANHSO_MUA').AsFloat := 0;

                QrDMVIP.FieldByName('CREATE_BY').AsInteger := sysLogonUID;
                QrDMVIP.FieldByName('CREATE_DATE').AsDateTime := d;

                QrDMVIP.Post;
                Next;
            end;
        except
        end;

		mCreating := False;

		// Requery
        if QrDMVIP.Tag = 0 then
        	CmdUsed.Execute
        else
        	CmdAll.Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_WARNING_NOT_EDIT = 'Chuyển tình trạng thẻ sang "Ngừng sử dụng" sẽ không chỉnh sửa được thông tin. Tiếp tục?';

procedure TFrmDmVIP.CmdHuytheExecute(Sender: TObject);
begin
    if not YesNo(RS_WARNING_NOT_EDIT) then
        Exit;

    with QrDMVIP do
    begin
        Edit;
        FieldByName('TINHTRANG').AsString := '03';
        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdSaveExecute(Sender: TObject);
begin
    QrDMVIP.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdCancelExecute(Sender: TObject);
begin
    QrDMVIP.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdChuyentheExecute(Sender: TObject);
begin
    //
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdDelExecute(Sender: TObject);
begin
    QrDMVIP.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdPrintExecute(Sender: TObject);
begin
    CmdSave.Execute;
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMVIP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdTemnhanExecute(Sender: TObject);
begin
    ShowReport(Caption, FORM_CODE + '_TEMNHAN', [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bBrowse, bEmpty: Boolean;
begin
    ExActionUpdate(ActionList, QrDmVip, Filter, mCanEdit);

    with QrDMVIP do
    begin
        if not Active then
            Exit;

        bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
    end;

//    CmdGen.Enabled := (bBrowse);
    CmdDel.Enabled := False;
    CmdPrint.Enabled := bBrowse and (not bEmpty);
    CmdExport.Enabled := bBrowse and (not bEmpty);
    CmdExportExcel.Enabled := bBrowse and (not bEmpty);
    CmdTemnhan.Enabled := bBrowse and (not bEmpty);
    CmdDoanhSo.Enabled := bBrowse and mCanEdit and (not bEmpty);

    CmdHuythe.Enabled := QrDMVIP.FieldByName('TINHTRANG').AsString = '02'
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.QrDMVIPBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdRefeshExecute(Sender: TObject);
var
    s, sLoc: String;
begin
    if not VarIsNull(cbbLoc.Value) then
        sLoc := cbbLoc.Value
    else
        sLoc := '';

    if (sLoc <> fLoc) or
        (EdSearch.Text <> mSearch)
        then
    begin
		fLoc    := sLoc;
        mSearch := DataMain.StripToneMark(EdSearch.Text);

		Screen.Cursor := crSQLWait;
		with QrDMVIP do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := mSql;

            // Detail Filter
            if fLoc <> '' then
                SQL.Add(' and LOC_THAMGIA in (''' + fLoc + ''')');

            if mSearch <> '' then
                SQL.Add(' and (dbo.fnStripToneMark(HOTEN) like ''%'+ mSearch +
                        '%'' or dbo.fnStripToneMark(MAVIP) like ''%' + mSearch + '%' +
                        '%'' or dbo.fnStripToneMark(DTHOAI) like ''%' + mSearch + '%' +
                        '%'' or dbo.fnStripToneMark(DCHI) like ''%' + mSearch + '%' +
                        '%'' or dbo.fnStripToneMark(CMND) like ''%' + mSearch + '%'')');

            case mUsed of
                1:
                    SQL.Add(' and isnull(TINHTRANG, '''') = ''01''');
                2:
                    SQL.Add(' and isnull(TINHTRANG, '''') = ''02''');
                3:
                    SQL.Add(' and isnull(TINHTRANG, '''') = ''03''');
                else
            end;
            SQL.Add(' order by NGAY_THAMGIA desc, MASO');
    	    Open;
    	    if s <> '' then
	        	Sort := s;
        end;

		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdReloadExecute(Sender: TObject);
begin
    fLoc := '~';
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.PopupMenu1Popup(Sender: TObject);
begin
    Item0.Checked := mUsed = 0;
    Item1.Checked := mUsed = 1;
    Item2.Checked := mUsed = 2;
    Item3.Checked := mUsed = 3;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.QrDMVIPBeforePost(DataSet: TDataSet);
var
    b: Boolean;
begin
    if mCreating then
        Exit;

	with QrDMVIP do
	begin
        b := (FieldByName('NGAY_THAMGIA').AsDateTime <> 0) or
                (FieldByName('HOTEN').AsString <> '') or
                (FieldByName('DCHI').AsString <> '') or
                (FieldByName('DTHOAI').AsString <> '');

    	if b then
        begin
        	if BlankConfirm(QrDMVIP, ['HOTEN', 'DCHI', 'DTHOAI', 'NGAY_THAMGIA']) then
            	Abort;

            if FieldByName('TINHTRANG').AsString = '01' then
                FieldByName('TINHTRANG').AsString := '02'
        end;
    end;
	SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.QrDMVIPBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

    if not mCreating then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.QrDMVIPDOANHSO_DAUChange(Sender: TField);
begin
    with QrDMVIP do
    begin
        FieldByName('DOANHSO').AsFloat := FieldByName('DOANHSO_DAU').AsFloat +
                FieldByName('DOANHSO_MUA').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdNotUsedExecute(Sender: TObject);
begin
    mUsed := (Sender as TComponent).Tag;
    CmdReload.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMVIP, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.BtnInClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdExportExcelExecute(Sender: TObject);
var
    s: String;
begin
    CmdSave.Execute;

    s := FORM_CODE + '_EXCEL';
    DataOffice.CreateReport2('\XLSX\' + s + '.xlsx', [sysLogonUID], s);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdExportExecute(Sender: TObject);
var
    s: String;
	b: Boolean;
	m1, m2: Integer;
begin
    CmdSave.Execute;
	// Export path
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	// Lay ma can export
    Application.CreateForm(TFrmExVIP, FrmExVIP);
    with FrmExVIP do
    begin
    	b := Execute(m1, m2);
        Free;
    end;

    if not b then
    	Exit;

	// Exporting
    Cursor := crSQLWait;
	with spDM_VIP_Export do
    begin
    	Prepared := True;
        Parameters[1].Value := m1;
        Parameters[2].Value := m2;
    	ExecProc;
        Active := True;
        TextExport(spDM_VIP_Export, s, TEncoding.UTF8);
//        TextExport(DMVIP_EXPORT, s);
        Active := False
    end;

	Cursor := crDefault;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;
  
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMVIP);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.CmdDoanhSoExecute(Sender: TObject);
begin
	Cursor := crSQLWait;
	with spDM_VIP_Calc_DoanhSo do
    begin
    	Prepared := True;
        Execute;
    end;
    Cursor := crDefault;

    fLoc := '~';
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmVIP.QrDMVIPBeforeEdit(DataSet: TDataSet);
var
	s: String;
begin
	with QrDMVIP do
    begin
        if FieldByName('TINHTRANG').AsString = '03' then
            Abort;

        if not sysIsCentral then
        	Abort;
	end;
end;

end.
