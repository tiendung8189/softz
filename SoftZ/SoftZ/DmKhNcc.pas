﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmKhNcc;

interface

uses
  SysUtils, Classes, Controls, Forms, Winapi.Windows, Vcl.Graphics,
  ComCtrls, ActnList, Wwdbgrid2,
  StdCtrls, DBCtrls, wwdblook, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts,
  isPanel, wwfltdlg, RzSplit, wwdbdatetimepicker, wwDialog, Mask, ExtCtrls,
  RzPanel, Grids, Wwdbigrd, Wwdbgrid, ToolWin, wwdbedit, Wwkeycb, Buttons,
  AdvEdit, fcCombo, fctreecombo, fcTreeView, wwcheckbox, DBGridEh, DBCtrlsEh,
  DBLookupEh, DbLookupComboboxEh2;

type
  TFrmDmKhNcc = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    N2: TMenuItem;
    CmdClearFilter: TAction;
    Khngspxp2: TMenuItem;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    CmdContact: TAction;
    ToolButton8: TToolButton;
    ToolButton10: TToolButton;
    QrDanhmucPLOAI: TWideStringField;
    QrDanhmucDCHI: TWideStringField;
    QrDanhmucDTHOAI: TWideStringField;
    QrDanhmucFAX: TWideStringField;
    QrDanhmucMST: TWideStringField;
    QrDanhmucTENTK: TWideStringField;
    QrDanhmucMATK: TWideStringField;
    QrDanhmucSODU: TFloatField;
    QrDanhmucGHICHU: TWideMemoField;
    QrDanhmucCREATE_BY: TIntegerField;
    QrDanhmucUPDATE_BY: TIntegerField;
    QrDanhmucCREATE_DATE: TDateTimeField;
    QrDanhmucUPDATE_DATE: TDateTimeField;
    QrDanhmucHAN_TTOAN: TIntegerField;
    QrDanhmucLK_PLOAI: TWideStringField;
    RzSizePanel1: TRzSizePanel;
    PaDetail: TScrollBox;
    PD1: TisPanel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label8: TLabel;
    Label2: TLabel;
    Label12: TLabel;
    EdTen: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    EdMa: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    PD5: TisPanel;
    DBMemo1: TDBMemo;
    PD4: TisPanel;
    Label7: TLabel;
    Label9: TLabel;
    Label11: TLabel;
    Label13: TLabel;
    DBEdit6: TwwDBEdit;
    DBEdit7: TwwDBEdit;
    DBEdit8: TwwDBEdit;
    DBEdit9: TwwDBEdit;
    QrDanhmucLOAI: TIntegerField;
    QrDanhmucMADT: TWideStringField;
    QrDanhmucTENDT: TWideStringField;
    QrDanhmucMAQG: TWideStringField;
    QrDanhmucMATINH: TWideStringField;
    QrDanhmucMAHUYEN: TWideStringField;
    QrQuocgia: TADOQuery;
    QrQuocgiaMA: TWideStringField;
    QrQuocgiaTEN: TWideStringField;
    QrTinh: TADOQuery;
    QrTinhMATINH: TWideStringField;
    QrTinhMAQG: TWideStringField;
    QrTinhMA: TWideStringField;
    QrTinhTEN: TWideStringField;
    QrHuyen: TADOQuery;
    QrHuyenMAHUYEN: TWideStringField;
    QrHuyenMATINH: TWideStringField;
    QrHuyenMA: TWideStringField;
    QrHuyenTEN: TWideStringField;
    QrDanhmucSODU_NGAY: TDateTimeField;
    QrDanhmucSODU_LIMIT: TFloatField;
    PD3: TisPanel;
    TntLabel4: TLabel;
    DBEdit10: TwwDBEdit;
    TntLabel5: TLabel;
    DBEdit11: TwwDBEdit;
    Panel1: TPanel;
    PaVIP: TPanel;
    PaNV: TPanel;
    QrDanhmucMANV: TWideStringField;
    QrDanhmucLOAI_VIP: TWideStringField;
    PaChuquan: TPanel;
    QrDanhmucMADT2: TWideStringField;
    QrDanhmucLK_TENDT2: TWideStringField;
    QrDanhmucEMAIL: TWideStringField;
    Label17: TLabel;
    wwDBEdit1: TwwDBEdit;
    N1: TMenuItem;
    CmdRefesh: TAction;
    QrDanhmucNGUNG_LIENLAC: TBooleanField;
    DBCheckBox1: TwwCheckBox;
    ItemLienlac: TMenuItem;
    PaList: TPanel;
    PaSearch: TPanel;
    Label18: TLabel;
    EdSearch: TAdvEdit;
    CHECK_DTHOAI: TADOCommand;
    SpeedButton2: TSpeedButton;
    CmdDmtk: TAction;
    QrDanhmucLK_TENTK: TWideStringField;
    QrDanhmucLK_NGANHANG: TWideStringField;
    QrDanhmucLK_CHINHANH: TWideStringField;
    CmdExportDataGrid: TAction;
    N4: TMenuItem;
    N5: TMenuItem;
    PaNhom: TisPanel;
    Label19: TLabel;
    CbNhom: TfcTreeCombo;
    txtNhom: TwwDBEdit;
    QrDanhmucLK_LOAI: TWideStringField;
    CmdDmdk: TAction;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    PD0: TisPanel;
    Label20: TLabel;
    cmbTenloai: TwwDBLookupCombo;
    ToolButton16: TToolButton;
    CmdDmpc: TAction;
    PD6: TisPanel;
    wwDBEdit2: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    Label22: TLabel;
    Label23: TLabel;
    wwDBEdit6: TwwDBEdit;
    wwDBEdit8: TwwDBEdit;
    Label24: TLabel;
    wwDBEdit9: TwwDBEdit;
    Label26: TLabel;
    Label27: TLabel;
    wwDBEdit10: TwwDBEdit;
    QrDanhmucHD_NMUA: TWideStringField;
    QrDanhmucHD_DONVI: TWideStringField;
    QrDanhmucHD_DCHI: TWideStringField;
    QrDanhmucHD_DTHOAI: TWideStringField;
    QrDanhmucHD_EMAIL: TWideStringField;
    QrDanhmucHD_HTTT: TWideStringField;
    QrDanhmucHD_MST: TWideStringField;
    QrDanhmucHD_SOTK: TWideStringField;
    LbLayThongtinHD: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label21: TLabel;
    LayTTCongTy: TLabel;
    CmdDmct: TAction;
    RadioButton1: TRadioButton;
    RadioButton2: TRadioButton;
    QrDanhmucIdx: TAutoIncField;
    CbLOAI_VIP: TDbLookupComboboxEh2;
    CbMANV: TDbLookupComboboxEh2;
    CbMADT: TDbLookupComboboxEh2;
    EdMADT2: TDBEditEh;
    EdMANV: TDBEditEh;
    EdMAQG: TDBEditEh;
    cbbMAQG: TDbLookupComboboxEh2;
    DsQuocgia: TDataSource;
    DsTinh: TDataSource;
    DsHuyen: TDataSource;
    EdMATINH: TDBEditEh;
    cbbTINH: TDbLookupComboboxEh2;
    cbbMAHUYEN: TDbLookupComboboxEh2;
    EdMAHUYEN: TDBEditEh;
    QrDanhmucLK_MATINHNGAN: TWideStringField;
    QrDanhmucLK_MAHUYENNGAN: TWideStringField;
    DBEditEh1: TDBEditEh;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CbPhanLoaiNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdContactExecute(Sender: TObject);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure QrDanhmucMAQGChange(Sender: TField);
    procedure QrDanhmucMATINHChange(Sender: TField);
    procedure CbTentinhBeforeDropDown(Sender: TObject);
    procedure CbTenHuyenBeforeDropDown(Sender: TObject);
    procedure QrDanhmucMADTChange(Sender: TField);
    procedure CmdRefeshExecute(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure ItemLienlacClick(Sender: TObject);
    procedure QrDanhmucDTHOAIValidate(Sender: TField);
    procedure CmdDmtkExecute(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure CbNhomCalcNodeAttributes(TreeView: TfcCustomTreeView;
      Node: TfcTreeNode; State: TfcItemStates);
    procedure CbNhomCloseUp(Sender: TObject; Select: Boolean);
    procedure CbNhomKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure CmdDmdkExecute(Sender: TObject);
    procedure CmdDmpcExecute(Sender: TObject);
    procedure LbLayThongtinHDClick(Sender: TObject);
    procedure LayTTCongTyClick(Sender: TObject);
    procedure cbbTINHDropDown(Sender: TObject);
    procedure cbbMAHUYENDropDown(Sender: TObject);
    procedure QrDanhmucMAHUYENChange(Sender: TField);
  private
  	mCanEdit, mRet, fixCode, mAutoCode, mClose, mObsolete: Boolean;
    mCodeLength, mLoai: Integer;
    mSQL, mPrefix, mSearch, mNhom: String;
  public
  	function Execute(r: WORD; pLoai: Integer; pClose: Boolean = True): Boolean;
  end;

var

  FrmDmKhNcc: TFrmDmKhNcc;

implementation

uses
	ExCommon, isDb, isMsg, Rights, MainData, isLib, Lienhe, isCommon, DmTK, OfficeData, DmKhNccDauKy,
    DmLoaiVip, DmnccCty, HrData;

{$R *.DFM}

const
	FORM_CODE = 'DM_KH_NCC';

(*==============================================================================
** r: Access rights
** Return value:
**		True:		Co insert or update
**		False:		Khong insert or update
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmKhNcc.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsDanhmuc.AutoEdit := mCanEdit;

    mRet := False;
    mClose := pClose;
    mLoai := pLoai;
    mNhom := IntToStr(mLoai);
    ShowModal;
    Result := mRet;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;

    mTrigger := False;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE, Filter);

    PaDetail.VertScrollBar.Position := 0;
    LbLayThongtinHD.Visible := False;
    LayTTCongTy.Visible := False;

    PD3.Collapsed := RegReadBool('PD3');
  	PD4.Collapsed := RegRead('PD4', True);
    PD5.Collapsed := RegRead('PD5', True);

    mAutoCode := FlexConfigBool(FORM_CODE, 'AutoCode');
    fixCode := FlexConfigBool(FORM_CODE, 'Fixed');
    mCodeLength := FlexConfigInteger(FORM_CODE, 'CodeLength', 0);
    mObsolete     := RegReadBool('Obsolete');
    mSearch := '';
    mSQL := QrDanhmuc.SQL.Text;

    FlexGroupComboDT(CbNhom);

    with DataMain.QrLOAI_DT do
    begin
        if Active then
            Close;
        Open;
    end;

//    if mCodeLength > 0 then
//        with QrDanhmuc.FieldByName('MADT') do
//        begin
//            DisplayWidth := mCodeLength;
//            Size := mCodeLength;
//        end
//	else
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.FormShow(Sender: TObject);
var
    bVIP, bNV, bCongnoDK: Boolean;
begin
    if mLoai >= 0 then
    begin
        CbNhom.SetSelectedNode(CbNhom.Items[mLoai]);
        CbNhom.Text := CbNhom.Items[mLoai].Text;
        if mLoai = 0 then
        begin
            Caption := 'Danh Mục Khách Hàng';
            bCongnoDK := GetFuncState('SZ_DM_KH_CONGNO');
            bVIP := GetFuncState('SZ_DM_LOAIVIP_KH');
            bNV := GetFuncState('SZ_PUB_DM_NHANVIEN');
        end
        else if mLoai = 1 then
        begin
            Caption := 'Danh Mục Nhà Cung Cấp';
            bCongnoDK := GetFuncState('SZ_DM_NCC_CONGNO');
            bVIP := False;
            bNV := False;
        end;
    end else
    begin
        CbNhom.Text := '';
        Caption := 'Danh Mục Khách Hàng/ Nhà Cung Cấp';
        bCongnoDK := GetFuncState('SZ_KH_NCC_CONGNO');
        bVIP := GetFuncState('SZ_DM_LOAIVIP_KH');
        bNV := GetFuncState('SZ_PUB_DM_NHANVIEN');
    end;

    EdMa.ReadOnly := mAutoCode;
//	PD2.Visible := GetSysParam('BEGIN_BALANCE');
    CmdDmdk.Visible := bCongnoDK;
    ToolButton14.Visible := CmdDmdk.Visible;

    PaNhom.Visible := mLoai = -1;
    PD0.Visible := mLoai = -1;

    if not bVIP then
    begin
        PaVIP.Visible := False;
        PD1.Height := PD1.Height - PaVIP.Height;
    end;

    if not bNV then
    begin
        PaNV.Visible := False;
        PD1.Height := PD1.Height - PaNV.Height;
    end;

    with DataMain do
        OpenDataSets([QrLOAI_DT, QrLOAI_KH_NCC, QrV_PUB_PHANHE_VIP_KH, QrDM_KH_NCC,
        QrDMTK, QrTINH, QrHUYEN]);

	OpenDataSets([QrQuocgia, QrTinh, QrHuyen, HrDataMain.QrDMNV_DANGLAMVIEC]);
    SetDisplayFormat(QrDanhmuc, sysCurFmt);

    CmdRefesh.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;
    with QrDanhmuc do
    begin
        if FieldByName('NGUNG_LIENLAC').AsBoolean  then
            AFont.Color := clRed
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.ItemLienlacClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_COMPANY_EMPTY = 'Bạn chưa chọn công ty quản lý để lấy thông tin.';
procedure TFrmDmKhNcc.LayTTCongTyClick(Sender: TObject);
var
    pMadt: string;
begin
    pMadt := QrDanhmuc.FieldByName('MADT2').AsString;
    if pMadt = '' then
    begin
        ErrMsg(RS_COMPANY_EMPTY);
        Exit;
    end
    else
    begin
        with DataMain.QrTEMP do
    	begin
            SQL.Text := 'select * from DM_KH_NCC where MADT = ''' + pMadt + '''';
            Open;
        end;
        with QrDanhmuc do
    	begin
            Edit;
            FieldByName('HD_NMUA').AsString := DataMain.QrTEMP.FieldByName('TENDT').AsString;
            FieldByName('HD_DCHI').AsString := DataMain.QrTEMP.FieldByName('DCHI').AsString;
            FieldByName('HD_DTHOAI').AsString := DataMain.QrTEMP.FieldByName('DTHOAI').AsString;
            FieldByName('HD_EMAIL').AsString := DataMain.QrTEMP.FieldByName('EMAIL').AsString;
            FieldByName('HD_MST').AsString := DataMain.QrTEMP.FieldByName('MST').AsString;
            FieldByName('HD_SOTK').AsString := DataMain.QrTEMP.FieldByName('MATK').AsString;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.LbLayThongtinHDClick(Sender: TObject);
begin
    with QrDanhmuc do
    begin
        Edit;
        if FieldByName('HD_NMUA').AsString = '' then
        begin
            FieldByName('HD_NMUA').AsString := FieldByName('TENDT').AsString;
        end;

        if FieldByName('HD_DONVI').AsString = '' then
        begin
            FieldByName('HD_DONVI').AsString := FieldByName('LK_TENDT2').AsString;
        end;

        if FieldByName('HD_DCHI').AsString = '' then
        begin
            FieldByName('HD_DCHI').AsString := FieldByName('DCHI').AsString;
        end;

        if FieldByName('HD_DTHOAI').AsString = '' then
        begin
            FieldByName('HD_DTHOAI').AsString := FieldByName('DTHOAI').AsString;
        end;

        if FieldByName('HD_EMAIL').AsString = '' then
        begin
            FieldByName('HD_EMAIL').AsString := FieldByName('EMAIL').AsString;
        end;

        if FieldByName('HD_MST').AsString = '' then
        begin
            FieldByName('HD_MST').AsString := FieldByName('MST').AsString;
        end;

        if FieldByName('HD_SOTK').AsString = '' then
        begin
            FieldByName('HD_SOTK').AsString := FieldByName('MATK').AsString;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrDanhmuc, QrQuocgia, QrTinh, QrHuyen]);
    finally
    end;

	//Save state panel
    RegWrite(Name, ['PD3', 'PD4', 'PD5', 'Obsolete'],
        [PD3.Collapsed, PD4.Collapsed, PD5.Collapsed, mObsolete]);
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDanhmuc, True);
end;

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdNewExecute(Sender: TObject);
begin
    QrDanhmuc.Append;
    EdTen.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdSaveExecute(Sender: TObject);
begin
    QrDanhmuc.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdCancelExecute(Sender: TObject);
begin
    QrDanhmuc.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdDelExecute(Sender: TObject);
begin
    QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdDmdkExecute(Sender: TObject);
var
    r: WORD;
begin
    if mLoai >= 0 then
    begin
        if mLoai = 0 then
        begin
            r := GetRights('SZ_DM_KH_CONGNO');
        end
        else if mLoai = 1 then
        begin
            r := GetRights('SZ_DM_NCC_CONGNO');
        end;
    end else
    begin
        r := GetRights('SZ_KH_NCC_CONGNO');
    end;

    if r = R_DENY then
        Exit;

    with QrDanhmuc do
    begin
        Application.CreateForm(TFrmDmKhNccDauKy, FrmDmKhNccDauKy);
        FrmDmKhNccDauKy.Execute(mCanEdit, FieldByName('MADT').AsString, FieldByName('TENDT').AsString,
                                 FieldByName('LOAI').Value)
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdDmpcExecute(Sender: TObject);
var
    r: WORD;
begin
    r := GetRights('SZ_DM_LOAIVIP_KH');
    if r = R_DENY then
    	Exit;

	Application.CreateForm(TFrmDmloaiVIP, FrmDmloaiVIP);
    FrmDmloaiVIP.Execute(r, 1);
    DataMain.QrV_PUB_PHANHE_VIP_KH.Requery;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdDmtkExecute(Sender: TObject);
var
    r: WORD;
    pMatk, pMadt: String;
begin
    with QrDanhmuc do
    begin
        pMadt := FieldByName('MADT').AsString;

        Application.CreateForm(TFrmDmTK, FrmDmTK);
        if FrmDmTK.Execute(R_FULL, pMatk, 'DT', pMadt, '', False) then
        begin
            DataMain.QrDMTK.Requery();

            Edit;
            FieldByName('MATK').AsString := pMatk;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrList);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdPrintExecute(Sender: TObject);
var
    nhom: String;
begin
    nhom := mNhom;

    if nhom = '' then
        nhom := '-1';

	 ShowReport(Caption, FORM_CODE, [sysLogonUID, nhom, mObsolete]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdRefeshExecute(Sender: TObject);
var
    sSQL: String;
    fLevel: Integer;
	sNhom: String;
begin
    sSQL := mSQL;

    if CbNhom.Text = '' then
    	fLevel := -1
    else
    begin
        fLevel := 0;
        sNhom := CbNhom.SelectedNode.StringData;
    end;

    mNhom := sNhom;
    mSearch := DataMain.StripToneMark(EdSearch.Text);
	with QrDanhmuc do
    begin
    	Close;

        if fLevel >= 0 then
            sSQL := sSQL + ' and LOAI = ' + sNhom;

        if mSearch <> '' then
            sSQL := sSQL + ' and (dbo.fnStripToneMark(TENDT) like ''%'+ mSearch + '%'' or dbo.fnStripToneMark(DTHOAI) like ''%' + mSearch + '%'')';

        if not mObsolete then
            sSQL := sSQL + ' and (isnull(NGUNG_LIENLAC, 0) = 0)';

        SQL.Text := sSQL;
        SQL.Add(' order by 	MADT');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bInsert: Boolean;
begin
    exActionUpdate(ActionList, QrDanhmuc, Filter, mCanEdit);

	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bInsert := State in [dsInsert];
    end;

    GrList.Enabled := bBrowse;
    CmdContact.Enabled := bBrowse and (not bEmpty);
    CmdDmdk.Enabled := bBrowse and (not bEmpty);
//    PD0.Enabled := bInsert;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDanhmuc);
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.PopupMenu1Popup(Sender: TObject);
begin
    ItemLienlac.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucBeforePost(DataSet: TDataSet);
var
    loai: Integer;
    sFormcode: string;
begin
	with QrDanhmuc do
    begin
        if State in [dsInsert] then
        begin
            if FieldByName('LOAI').AsInteger < 0 then
            begin
                ErrMsg('Phải chọn loại Khách hàng/Nhà cung cấp');
                cmbTenloai.SetFocus;
                Abort;
            end;

            if mAutoCode then
            begin
                if BlankConfirm(QrDanhmuc, ['TENDT']) then
        	        Abort;

                loai := FieldByName('LOAI').AsInteger;
                case loai of
                    0: sFormcode := 'DM_KH';
                    1: sFormcode := 'DM_NCC';
                else
                    sFormcode := 'DM_KH_NCC';
                end;

                mPrefix := FlexConfigString(sFormcode, 'Prefix', '');
                FieldByName('MADT').AsString :=
                    DataMain.AllocMADT(mCodeLength, loai, mPrefix);
            end
            else
            begin
                if fixCode then
                    if LengthConfirm(QrDanhmuc, ['MADT']) then
                        Abort;

                if BlankConfirm(QrDanhmuc, ['MADT', 'TENDT']) then
                    Abort;
            end;
        end;

	    SetNull(QrDanhmuc, ['MAQG', 'MATINH', 'MAHUYEN', 'DTHOAI']);
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_DTTHOAI_EXISTS = 'Số điện thoại đã được sử dụng.';
procedure TFrmDmKhNcc.QrDanhmucDTHOAIValidate(Sender: TField);
var
	s: String;
begin
    // Khong xet ma hang blank
	s := Sender.AsString;
    if s = '' then
        Exit;

    with CHECK_DTHOAI do
        begin
            Prepared := True;
            Parameters[0].Value := s;
            Parameters[1].Value := QrDanhmuc.FieldByName('MADT').AsString;
            if Execute.RecordCount > 0 then
            begin
                ErrMsg(RS_DTTHOAI_EXISTS);
                Abort;
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Mark as updated or inserted
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucAfterPost(DataSet: TDataSet);
begin
	mRet := True;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDanhmuc, Filter);
end;

procedure TFrmDmKhNcc.cbbMAHUYENDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDanhmuc.FieldByName('MATINH').AsString;
    QrHuyen.Filter := 'MATINH=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.cbbTINHDropDown(Sender: TObject);
var
    s: string;
begin
    s := QrDanhmuc.FieldByName('MAQG').AsString;
    QrTinh.Filter := 'MAQG=''' + s + '''';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbNhomCalcNodeAttributes(TreeView: TfcCustomTreeView;
  Node: TfcTreeNode; State: TfcItemStates);
begin
    with Node do
    	ImageIndex := Level
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbNhomCloseUp(Sender: TObject; Select: Boolean);
begin
    CmdRefesh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbNhomKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
     {$I ClearTreeCombo}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbPhanLoaiNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CmdContactExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmLienhe, FrmLienhe);
    with QrDanhmuc do
        FrmLienhe.Execute(
            mCanEdit,
        	FieldByName('Idx').AsInteger,
            FieldByName('MADT').AsString,
            FieldByName('TENDT').AsString);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('NGUNG_LIENLAC').AsBoolean := False;
        FieldByName('MAQG').AsString := '084';
//        if mLoai >= 0 then
        FieldByName('LOAI').AsInteger :=  mLoai;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucMADTChange(Sender: TField);
var
    mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

procedure TFrmDmKhNcc.QrDanhmucMAHUYENChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        EdMAHUYEN.Text := EdMAHUYEN.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucMAQGChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        FieldByName('MATINH').Clear;
        FieldByName('MAHUYEN').Clear;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.QrDanhmucMATINHChange(Sender: TField);
begin
    with QrDanhmuc do
    begin
        FieldByName('MAHUYEN').Clear;
        EdMATINH.Text := EdMATINH.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbTentinhBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MAQG').AsString;
    with QrTinh do
    begin
        Filter := Format('MAQG=''%s''', [s]);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNcc.CbTenHuyenBeforeDropDown(Sender: TObject);
var
    s: String;
begin
    s := QrDanhmuc.FieldByName('MATINH').AsString;
    with QrHuyen do
    begin
        Filter := Format('MATINH=''%s''', [s]);
    end;
end;

end.
