﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Soluong;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, ExtCtrls;

type
  TFrmSoluong = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdSoluong: TEdit;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
  public
  	function GetQuantity: Double;
  end;

var
  FrmSoluong: TFrmSoluong;

implementation

uses
	isLib;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSoluong.FormKeyPress(Sender: TObject; var Key: Char);
begin
	case Key of
    #13:
        try
			StrToFloat(EdSoluong.Text);
	    	ModalResult := mrOk;
        except
        end;
    #27:
    	ModalResult := mrCancel;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmSoluong.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmSoluong.GetQuantity : Double;
begin
	EdSoluong.Text := '';
	if ShowModal = mrOK then
    	Result := StrToFloat(EdSoluong.Text)
    else
    	Result := 0;
end;


end.
