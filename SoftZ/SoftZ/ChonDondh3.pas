﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDondh3;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, DBCtrls, Vcl.Mask,
  wwdbedit, AdvEdit, DBAdvEd, DBGridEh, DBCtrlsEh, DBLookupEh,
  DbLookupComboboxEh2, Variants, kbmMemTable, MemTableDataEh, MemTableEh,
  Vcl.Buttons;

type
  TFrmChonDondh3 = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsDONDH: TDataSource;
    QrDONDH: TADOQuery;
    QrDONDHNGAY: TDateTimeField;
    QrDONDHSCT: TWideStringField;
    QrDONDHMADT: TWideStringField;
    QrDONDHMAKHO: TWideStringField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    QrDONDHTENKHO: TWideStringField;
    QrDONDHTENDT: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrDONDHKHOA: TGuidField;
    Cbx1: TCheckBox;
    QrDONDHGhiChu: TWideMemoField;
    QrDONDHSoTienCKHD: TFloatField;
    QrDONDHSoLuong: TFloatField;
    QrDONDHThanhTien: TFloatField;
    QrDONDHThanhToan: TFloatField;
    QrDONDHNgayGiaoHangDuKien: TDateTimeField;
    QrDONDHNguoiDatHang: TWideStringField;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    DsDMKHO: TDataSource;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    DsDM_KH_NCC: TDataSource;
    SpeedButton1: TSpeedButton;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDONDHBeforeOpen(DataSet: TDataSet);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mNCC, mKHO, mSQL, mSqlDT, mLCT, mTT: String;

    fTungay, fDenngay: TDateTime;
  public
  	function Execute(var pCheck: Boolean; pFix: Boolean; pNcc, pKho: String;
    	pLoai: string = 'DM_NCC'; pLCT: String = 'DHN'; pTT: String = ''): TGUID;
  end;

var
  FrmChonDondh3: TFrmChonDondh3;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon, isMsg;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDondh3.Execute;
begin
	mNCC := pNcc;
    mKho := pKho;
    mTT := pTT;
    Cbx1.Checked := False;

    QrDM_KH_NCC.SQL.Text := Format(mSqlDT, [pLOAI]);
    mLCT := pLCT;

	if ShowModal = mrOK then
    begin
        pCheck := Cbx1.Checked;
		Result := TGuidField(QrDONDH.FieldByName('KHOA')).AsGuid
    end
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DONDH', GrBrowse);

    SetDisplayFormat(QrDONDH, sysCurFmt);
    SetShortDateFormat(QrDONDH);
    SetDisplayFormat(QrDONDH, ['NGAY'], DateTimeFmt);

    mSQL := QrDONDH.SQL.Text;
    mSqlDT := QrDM_KH_NCC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormShow(Sender: TObject);

begin
   	OpenDataSets([QrDONDH, QrDM_KH_NCC, QrDMKHO]);

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    // Smart focus
    if mNCC = '' then
		try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;

    mNCC := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.CmdRefreshExecute(Sender: TObject);
var
	s, sDonVi, sKhoHang: String;
    dTuNgay, dDenNgay: TDateTime;
begin
    sDonVi :=  EdMADV.Text;
    sKhoHang := EdMaKho.Text;
    dTuNgay :=  EdTungay.Date;
    dDenNgay :=  EdDenngay.Date;

   	if (fTungay <> dTuNgay)  or
    	(fDenngay <> dDenNgay) or
    	(mNCC <> sDonVi) or
	   	(mKHO <> sKhoHang) then
	begin
   	    mNCC := sDonVi;
        mKHO := sKhoHang;
        fTungay  := dTuNgay;
	    fDenngay := dDenNgay;

        with QrDONDH do
        begin
			Close;

            s := mSQL;

            if mNCC <> '' then
                s := s + ' and a.MADT=''' + mNCC + '''';

			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';

            if mTT <> '' then
                s := s + ' and a.TINHTRANG in (select Value from dbo.fnS_Table(''' + mTT + '''))';
            SQL.Text := s;
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.QrDONDHBeforeOpen(DataSet: TDataSet);
begin
    with QrDONDH do
    begin
        Parameters[0].Value := mLCT;
        Parameters[1].Value := fTungay;
        Parameters[2].Value := fDenngay;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDondh3.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mNCC;
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

end.
