﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FlexRep3;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Buttons, wwDBGrid2, DB, ADODb,
  kbmMemTable, ActnList, Menus, AdvMenus, StdCtrls, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmFlexRep3 = class(TForm)
    GrList: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    QrTemp: TkbmMemTable;
    DsRep: TDataSource;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    Lc1: TMenuItem;
    ActionList: TActionList;
    CmdSelectAll: TAction;
    CmdClearSelect: TAction;
    N1: TMenuItem;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmdSelectAllExecute(Sender: TObject);
    procedure CmdClearSelectExecute(Sender: TObject);
  private
    procedure Check(b: Boolean = True);
  public
  	function  Execute(DataSet: TCustomADODataSet): Boolean;
  end;

var
  FrmFlexRep3: TFrmFlexRep3;

implementation



{$R *.dfm}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep3.Check(b: Boolean);
var
    bm: TBytes;
begin
	with QrTemp do
    begin
        bm := Bookmark;
		DisableControls;
        First;
        while not Eof do
		begin
            Edit;
            Fields[1].AsBoolean := b;
            Next;
        end;
        Bookmark := bm;
		EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep3.CmdClearSelectExecute(Sender: TObject);
begin
    Check(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep3.CmdSelectAllExecute(Sender: TObject);
begin
    Check;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmFlexRep3.Execute(DataSet: TCustomADODataSet): Boolean;
var
	i, n: Integer;
    s: String;
begin
	n := DataSet.FieldCount - 1;
	with QrTemp do
    begin
        Open;
        for i := 0 to n do
		begin
            Append;
            Fields[0].AsInteger := i + 1;
            Fields[1].AsBoolean := DataSet.Fields[i].Visible;
            Fields[2].AsString := DataSet.Fields[i].FullName;
        end;
        CheckBrowseMode;
        First;
    end;

    Result := ShowModal = mrOK;
    if Result then
        with QrTemp do
        begin
            First;
            while not Eof do
            begin
                s := FieldByName('Name').AsString;
                DataSet.FieldByName(s).Visible := FieldByName('Visible').AsBoolean;
                Next;
            end;
        end;
    QrTemp.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep3.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFlexRep3.FormShow(Sender: TObject);
begin
	GrList.SetFocus;
end;

end.
