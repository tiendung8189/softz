﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Banle;

interface

uses
  SysUtils, Classes, Controls, Forms, Vcl.Graphics,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2, AppEvnts, exPrintBill,
  AdvMenus, wwfltdlg, frameNavi, wwDialog, Mask, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin, wwdbedit, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2, System.Variants;

type
  TFrmBanle = class(TForm)
    ToolMain: TToolBar;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    Panel1: TPanel;
    PaBanle: TPanel;
    QrBH: TADOQuery;
    QrCTBH: TADOQuery;
    DsBH: TDataSource;
    DsCT: TDataSource;
    CmdTotal: TAction;
    ToolButton1: TToolButton;
    CmdCancel: TAction;
    QrBHNGAY: TDateTimeField;
    QrBHCA: TWideStringField;
    QrBHPRINTED: TBooleanField;
    QrBHCREATE_DATE: TDateTimeField;
    QrBHUPDATE_DATE: TDateTimeField;
    QrBHDELETE_DATE: TDateTimeField;
    QrBHTHUNGAN: TWideStringField;
    QrBHXOA: TWideStringField;
    QrBHNG_CK: TWideStringField;
    QrCTBHSTT: TIntegerField;
    QrCTBHMAVT: TWideStringField;
    QrCTBHSOLUONG: TFloatField;
    QrCTBHDONGIA: TFloatField;
    QrCTBHTENVT: TWideStringField;
    QrCTBHDVT: TWideStringField;
    LbHH1: TLabel;
    EdHH1: TwwDBEdit;
    EdCK: TwwDBEdit;
    Label18: TLabel;
    Label19: TLabel;
    EdTienVAT: TwwDBEdit;
    EdTriGiaTT: TwwDBEdit;
    Label24: TLabel;
    Label1: TLabel;
    CmdDel: TAction;
    QrBHNG_HUY: TWideStringField;
    CmdSearch: TAction;
    ToolButton2: TToolButton;
    ToolButton4: TToolButton;
    Label65: TLabel;
    Label66: TLabel;
    EdFrom: TwwDBDateTimePicker;
    EdTo: TwwDBDateTimePicker;
    QrBHSCT: TWideStringField;
    QrBHCREATE_BY: TIntegerField;
    QrBHUPDATE_BY: TIntegerField;
    QrBHDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrBHIMG: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    CmdClearFilter: TAction;
    Hinttc: TMenuItem;
    QrBHSOLUONG: TFloatField;
    PopupMenu1: TAdvPopupMenu;
    GrDetail: TwwDBGrid2;
    QrBHMAMADT: TWideStringField;
    QrBHMAKHO: TWideStringField;
    QrBHTHANHTOAN: TFloatField;
    QrBHLK_TENDT: TWideStringField;
    CmdReRead: TAction;
    QrBHLK_TENKHO: TWideStringField;
    CmdFilterCom: TAction;
    QrBHQUAY: TWideStringField;
    N4: TMenuItem;
    QrDMQUAY: TADOQuery;
    Bevel1: TBevel;
    Chntheomthng1: TMenuItem;
    frNavi: TfrNavi;
    QrCTBHGHICHU: TWideStringField;
    QrCTBHLOAITHUE: TWideStringField;
    CmdAudit: TAction;
    DBText2: TDBText;
    CmdListRefesh: TAction;
    QrDMKHO: TADOQuery;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    DsKHO: TDataSource;
    QrCTBHMABO: TWideStringField;
    QrBHLK_TENQUAY: TWideStringField;
    QrBHLK_USERNAME: TWideStringField;
    QrBHKHOA: TGuidField;
    QrCTBHKHOACT: TGuidField;
    QrCTBHKHOA: TGuidField;
    QrBHTINHTRANG: TWideStringField;
    QrBHLK_THANHTOAN: TWideStringField;
    QrBHLOC: TWideStringField;
    QrDMKHOLOC: TWideStringField;
    EdTENDT: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    Label2: TLabel;
    wwDBEdit2: TwwDBEdit;
    Label3: TLabel;
    wwDBEdit3: TwwDBDateTimePicker;
    Label4: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label5: TLabel;
    wwDBEdit5: TwwDBEdit;
    wwDBEdit6: TwwDBEdit;
    QrBHLCT: TWideStringField;
    QrBHDELIVERY: TBooleanField;
    QrBHCN_TENDV: TWideStringField;
    QrBHCN_MST: TWideStringField;
    QrBHCN_DIACHI: TWideStringField;
    QrBHCN_DIACHI_HD: TWideStringField;
    QrBHCN_LIENHE: TWideStringField;
    QrBHCN_DTHOAI: TWideStringField;
    QrBHCN_EMAIL: TWideStringField;
    QrBHCN_MATK: TWideStringField;
    QrBHDEBT_BY: TIntegerField;
    QrBHDEBT_DATE: TDateTimeField;
    QrBHLK_DEBT_TEN: TWideStringField;
    QrBHLK_FULLNAME: TWideStringField;
    N2: TMenuItem;
    Item1: TMenuItem;
    Item2: TMenuItem;
    Item3: TMenuItem;
    N3: TMenuItem;
    ItemAll: TMenuItem;
    QrBHSoTienCK: TFloatField;
    QrBHThanhTien: TFloatField;
    QrBHSoTienThue: TFloatField;
    QrBHTyLeCKHD: TFloatField;
    QrBHNhanVienCKHD: TIntegerField;
    QrBHTongSoTienNhan: TFloatField;
    QrBHGhiChu: TWideMemoField;
    QrCTBHThueSuat: TFloatField;
    QrCTBHTyLeCKHD: TFloatField;
    QrCTBHSoTienCK: TFloatField;
    QrCTBHSoTienThue: TFloatField;
    QrCTBHThanhTien: TFloatField;
    QrCTBHTyLeCKVip: TFloatField;
    QrCTBHTyLeCKMH: TFloatField;
    QrCTBHTyLeCKVipNhomHang: TFloatField;
    QrCTBHTyLeCKPhieu: TFloatField;
    QrCTBHTyLeCKDaChieu: TFloatField;
    QrCTBHTyLeCKBo: TFloatField;
    QrCTBHTyLeCK_Max: TFloatField;
    QrCTBHTyLeCK_Them: TFloatField;
    QrCTBHTyLeCK: TFloatField;
    QrCTBHDonGiaCK: TFloatField;
    QrCTBHThanhTienSauCK: TFloatField;
    QrCTBHDonGiaTinhThue: TFloatField;
    QrCTBHThanhToanTinhThue: TFloatField;
    QrCTBHThanhToanChuaCL: TFloatField;
    QrCTBHSoTienCL: TFloatField;
    QrCTBHDonGiaChuaThue: TFloatField;
    QrCTBHThanhToanChuaThue: TFloatField;
    QrCTBHSoTienThueChuaRound: TFloatField;
    QrCTBHSoTienThue_5: TFloatField;
    QrCTBHSoTienThue_10: TFloatField;
    QrCTBHSoTienThue_Khac: TFloatField;
    QrCTBHThanhToan: TFloatField;
    QrCTBHNhanVienCKMH: TIntegerField;
    QrCTBHLOC: TWideStringField;
    QrBHMaQuay: TWideStringField;
    QrBHMaVIP: TWideStringField;
    QrBHTyLeCKVip: TFloatField;
    QrBHThanhTienSauCK: TFloatField;
    QrBHThanhToanTinhThue: TFloatField;
    QrBHThanhToanChuaThue: TFloatField;
    QrBHThanhToanChuaCL: TFloatField;
    QrBHSoTienCL: TFloatField;
    QrBHLoaiThue: TWideStringField;
    QrBHThueSuat: TFloatField;
    QrBHSoTienThueChuaRound: TFloatField;
    QrBHSoTienThue_5: TFloatField;
    QrBHSoTienThue_10: TFloatField;
    QrBHSoTienThue_Khac: TFloatField;
    QrBHKhachDuaTienMat: TFloatField;
    QrBHThoiLaiTienMat: TFloatField;
    QrBHThanhToanTienMat: TFloatField;
    QrBHThanhToanTienMat_Lan1: TFloatField;
    QrBHThanhToanTienMat_Lan2: TFloatField;
    QrBHThanhToanTheNganHang: TFloatField;
    QrBHThanhToanTheNganHang_Lan1: TFloatField;
    QrBHThanhToanTheNganHang_Lan2: TFloatField;
    QrBHThanhToanTheNganHang_SoChuanChi: TWideStringField;
    QrBHThanhToanTraHang: TFloatField;
    QrBHThanhToanTraHang_SCT: TWideStringField;
    QrBHThanhToanTraHang_Khoa: TGuidField;
    QrBHThanhToanPhieuQuaTang: TFloatField;
    QrBHThanhToanPhieuQuaTang_MaThe: TWideStringField;
    QrBHThanhToanViDienTu: TFloatField;
    QrBHThanhToanViDienTu_Loai: TWideStringField;
    QrBHThanhToanChuyenKhoan: TFloatField;
    QrBHThanhToanDiemTichLuy: TFloatField;
    QrBHQUAYKE: TWideStringField;
    QrBHNhanVienThuNgan: TIntegerField;
    QrBHPRINT_NO: TIntegerField;
    cbbKho: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    QrCTBHMaNoiBo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrBHCalcFields(DataSet: TDataSet);
    procedure QrCTBHBeforeOpen(DataSet: TDataSet);
    procedure CmdDelExecute(Sender: TObject);
    procedure QrCTBHAfterInsert(DataSet: TDataSet);
    procedure QrCTBHBeforePost(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrBHBeforeEdit(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdTotalExecute(Sender: TObject);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure QrBHBeforePost(DataSet: TDataSet);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrBHAfterScroll(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CbMaKhoNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CbMaKhoBeforeDropDown(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure ItemAllClick(Sender: TObject);
    procedure PopupMenu1Popup(Sender: TObject);
    procedure QrBHTyLeCKHDChange(Sender: TField);
    procedure cbbKhoExit(Sender: TObject);
    procedure cbbKhoDropDown(Sender: TObject);
    procedure cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
  private
    mCanEdit, mClose: Boolean;
    mVATMode: Integer;
    mPrinter: TexPrintBill;

    // List filter
    fType, mFilter: Integer;
   	fTungay, fDenngay: TDateTime;
    fKho, fSQL, fStr: String;

    procedure Total(fUpdate: Boolean);
  public
  	procedure Execute(r: WORD; bClose: Boolean = True);
  end;

var
  FrmBanle: TFrmBanle;

implementation

uses
	isDb, ExCommon, MainData, Rights, RepEngine, ChonDsma, isLib, ReceiptDesc, isMsg,
    GuidEx, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HOADON_BANLE';

    (*
    ** Forms events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.Execute(r: WORD; bClose: Boolean);
begin
	mCanEdit := rCanEdit(r);
    mClose := bClose;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    AddAllFields(QrBH, 'BANLE', 0);

    frNavi.DataSet := QrBH;

    // Initial
    fType := 2;
    fStr := '';
    mFilter := 0;
    fSQL := QrBH.SQL.Text;
	EdFrom.Date := Date - sysLateDay;
	EdTo.Date := Date;

    exBillInitial(mPrinter);
    mPrinter.PrintPreview := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrLOC, QrDMKHO]);

    cbbKho.Value := sysDefKho;
    EdMaKho.Text := sysDefKho;
	QrDMQUAY.Open;

    // Tinh thue truoc hay sau chiet khau
    mVATMode := FlexConfigInteger('POS', 'VAT Form');

    with QrBH do
    begin
	    SetDisplayFormat(QrBH, sysCurFmt);
    	SetShortDateFormat(QrBH);
	    SetDisplayFormat(QrBH, ['NGAY'], 'dd/mm/yyyy hh:nn');
    end;

    with QrCTBH do
    begin
	    SetDisplayFormat(QrCTBH, sysCurFmt);
    	SetDisplayFormat(QrCTBH, ['SOLUONG'], sysQtyFmt);
    end;

    // Customize grid
    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrBH, QrCTBH],[FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);
    
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrBH, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    exBillFinal(mPrinter);
    try
        if mClose then
    	    CloseDataSets(DataMain.Conn)
        else
            CloseDataSets([QrBH, QrCTBH, QrDMKHO, QrDMQUAY]);
    finally
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
	    Screen.Cursor := crSQLWait;
        with QrCTBH do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;
   		GrDetail.SetFocus;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.PopupMenu1Popup(Sender: TObject);
begin
    Item1.Checked := mFilter = 1;
    Item2.Checked := mFilter = 2;
    Item3.Checked := mFilter = 3;
    ItemAll.Checked := mFilter = 0;
end;

(*
    ** End: Forms events
	*)

    (*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdRefreshExecute(Sender: TObject);
var
	s, s1, sKho: String;
begin
    if not VarIsNull(cbbKho.Value) then
        sKho := cbbKho.Value
    else
        sKho := '';

   	if (EdFrom.Date <> fTungay) or (EdTo.Date <> fDenngay) or (sKho <> fKho) then
    begin
		fTungay := EdFrom.Date;
		fDenngay := EdTo.Date;
        fKho := sKho;

	    Screen.Cursor := crSQLWait;
		with QrBH do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;
			SQL.Add(' and MAKHO = '''+ sKho + '''');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add(' and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add(' and KHOA in (select a.KHOA from BANLE_CT a, DM_VT_FULL b where a.KHOA = BANLE.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add(' and KHOA in (select KHOA from BANLE_CT where KHOA = BANLE.KHOA and MAVT in (' + fStr + '))');
				end;

            s1 := '';
            case mFilter of
                1:
                    s1 := ' and isnull(DELIVERY, 0) = 1 and isnull(TINHTRANG, '''') <> ''T03''';
                2:
                    s1 := ' and isnull(DELIVERY, 0) = 1 and isnull(TINHTRANG, '''') = ''T03''';
                3:
                    s1 := ' and isnull(DELIVERY, 0) = 1';
            end;
            SQL.Add(s1);
			SQL.Add(' order by NGAY desc, SCT desc');

            Parameters[0].Value := fTungay;
            Parameters[1].Value := fDenngay;
    	    Open;

            if s <> '' then
            	Sort := s;
        end;

		Screen.Cursor := crDefault;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMaKhoBeforeDropDown(Sender: TObject);
begin
    if not sysIsCentral then
        exComboBeforeDropDown(Sender as TwwDBLookupCombo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    QrDMKHO.Requery;
	QrDMQUAY.Requery;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdSaveExecute(Sender: TObject);
begin
	QrCTBH.UpdateBatch;
	QrBH.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdCancelExecute(Sender: TObject);
begin
	QrCTBH.CancelBatch;
    QrBH.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdPrintExecute(Sender: TObject);
var
	k: TGUID;
begin
	with QrBH do
    begin
        CmdSave.Execute;
		k :=  TGuidField(FieldByName('KHOA')).AsGuid;
	end;

    if mPrinter.BillPrint(k) then
        DataMain.UpdatePrintNo(k);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdFilterExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdDelExecute(Sender: TObject);
var
    s: String;
    _khoa: TGUID;
begin
    with QrBH do
    begin

        if FieldByName('DELETE_BY').AsInteger <> 0 then
            Exit;

        exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);

        _khoa := TGuidField(FieldByName('KHOA')).AsGuid;
        s := FieldByName('GhiChu').AsString;

        if not DataMain.IsCheckDelete(_khoa, FORM_CODE) then
            Exit;

        Application.CreateForm(TFrmReceiptDesc, FrmReceiptDesc);
        if not FrmReceiptDesc.Execute(s) then
            Exit;
        if not YesNo(RS_CONFIRM_XOAPHIEU, 1) then
            Exit;
    	Edit;
        FieldByName('GhiChu').AsString := s;
        MarkDataSet(QrBH);

//        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
	    exSearch(Name, DsBH)
    else
        exSearch(Name + 'CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show selection form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Reload
    CmdReread.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdTotalExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
    	Exit;
    Total(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDelete: Boolean;
    n: Integer;
begin
	with QrBH do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;

        bDelete := FieldByName('DELETE_BY').AsInteger > 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1) and (not bDelete);
    CmdDel.Caption := GetMarkCaption(QrBH);

    CmdPrint.Enabled := not bEmpty;

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse; 
    CmdClearFilter.Enabled := Filter.FieldInfo. Count > 0;
    CmdFilterCom.Checked := fStr <> '';
end;
    (*
    ** End: Actions
    *)

    (*
    ** DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

procedure TFrmBanle.QrBHTyLeCKHDChange(Sender: TField);
begin
    Total(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHBeforeEdit(DataSet: TDataSet);
begin
	exValidClosing(QrBH.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHBeforeOpen(DataSet: TDataSet);
begin
	QrCTBH.Parameters[0].Value := QrBH.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHAfterInsert(DataSet: TDataSet);
begin
	SetEditState(QrBH);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrCTBHBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    	if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrBH.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHAfterScroll(DataSet: TDataSet);
begin
	PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.QrBHBeforePost(DataSet: TDataSet);
begin
	SetAudit(DataSet);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrBH, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.cbbKhoCloseUp(Sender: TObject; Accept: Boolean);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := '';
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;

    with QrDMQUAY do
    begin
        Close;
        Open;
    end;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.cbbKhoDropDown(Sender: TObject);
begin
    cbbKho.DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
    cbbKho.DropDownBox.ListSource.DataSet.Filtered := True;
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.cbbKhoExit(Sender: TObject);
begin
    EdMaKho.Text := cbbKho.Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrBH do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clGray;
            Exit;
        end;

        if FieldByName('DELIVERY').AsBoolean then
        begin
            if  FieldByName('TINHTRANG').AsString = 'T01' then
            begin
                AFont.Color := clRed;
                Exit;
            end;

            if FieldByName('TINHTRANG').AsString = 'T02' then
            begin
                AFont.Color := clPurple;
                Exit;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.GrBrowseDblClick(Sender: TObject);
begin
    if QrBH.IsEmpty then
    	Exit;

    with PgMain do
    begin
    	ActivePageIndex := 1;
		OnChange(nil);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.ItemAllClick(Sender: TObject);
begin
    mFilter := (Sender as TComponent).Tag;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.Total;
var
    bm: TBytes;
    xSotien, xThsuat, xCkmh, xCkhd: Double;
	mSotien, mSoluong, mCkmh, mCkhd, mTlckhd, mThue: Double;
begin
	mSotien := 0;
    mSoluong := 0;
    mCkmh := 0;
    mCkhd := 0;
    mThue := 0;
	mTlckhd := QrBH.FieldByName('TyLeCKHD').AsFloat;

	with QrCTBH do
    begin
    	DisableControls;
        bm := BookMark;

		First;
        while not Eof do
        begin
        	xSotien := FieldByName('ThanhTien').AsFloat;		// Thanh tien chua chiet khau
            xThsuat := FieldByName('ThueSuat').AsFloat;	// Thue suat
            xCkmh := FieldByName('SoTienCK').AsFloat;		// Tien CKMH
            if xCkmh = 0.0 then								// Tinh tien CKHD
	            xCkhd := xSotien * mTlckhd / 100.0
            else
            	xCkhd := 0.0;

	        mSotien := mSotien + xSotien;
	        mSoluong := mSoluong + FieldByName('SOLUONG').AsFloat;
	        mCkmh := mCkmh + xCkmh;
	        mCkhd := mCkhd + xCkhd;

            if mVATMode = 0 then	// Tinh thue truoc chiet khau
	            mThue := mThue + xSotien * xThsuat / (100.0 + xThsuat)
	    	else					// Tinh thue sau chiet khau
	            mThue := mThue + (xSotien - xCkmh - xCkhd) * xThsuat / (100.0 + xThsuat);

    	    Next;
        end;
        BookMark := bm;
    	EnableControls;
    end;

    with QrBH do
    begin
    	if State in [dsBrowse] then
        	Edit;

    	FieldByName('ThanhTien').AsFloat  := mSotien;
    	FieldByName('SOLUONG').AsFloat := mSoluong;
    	FieldByName('SoTienCK').AsFloat := mCkmh;
    	FieldByName('SoTienCK').AsFloat := mCkhd;
    	FieldByName('SoTienThue').AsFloat := mThue;
    	FieldByName('THANHTOAN').AsFloat := mSotien - mCkmh - mCkhd;

        if fUpdate then
	        Post;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsBH, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBanle.CbMaKhoNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

end.
