﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Dmkho;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Wwdbgrid2,
  StdCtrls, DBCtrls, ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, isPanel, AppEvnts,
  wwfltdlg, RzSplit, wwDialog, Wwdbigrd, Wwdbgrid, Mask, ExtCtrls, RzPanel,
  ToolWin, wwdbedit;

type
  TFrmDmkho = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    CmdPrint: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton9: TToolButton;
    ToolButton10: TToolButton;
    ToolButton11: TToolButton;
    Filter: TwwFilterDialog2;
    CmdSearch: TAction;
    CmdFilter: TAction;
    Status: TStatusBar;
    QrDMKHO: TADOQuery;
    DsDMKHO: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    N1: TMenuItem;
    CmdClearFilter: TAction;
    Lcdliu1: TMenuItem;
    Khnglcdliu1: TMenuItem;
    QrDMKHOMAKHO: TWideStringField;
    QrDMKHOTENKHO: TWideStringField;
    QrDMKHODIACHI: TWideStringField;
    QrDMKHODTHOAI: TWideStringField;
    QrDMKHOFAX: TWideStringField;
    QrDMKHOTHUKHO: TWideStringField;
    QrDMKHOGHICHU: TWideMemoField;
    QrDMKHOCREATE_BY: TIntegerField;
    QrDMKHOUPDATE_BY: TIntegerField;
    QrDMKHOCREATE_DATE: TDateTimeField;
    QrDMKHOUPDATE_DATE: TDateTimeField;
    ApplicationEvents1: TApplicationEvents;
    RzSizePanel1: TRzSizePanel;
    PD1: TisPanel;
    Label5: TLabel;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label7: TLabel;
    Label9: TLabel;
    EdMAKHO: TwwDBEdit;
    EdTENKHO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit4: TwwDBEdit;
    DBEdit6: TwwDBEdit;
    PD2: TisPanel;
    DBMemo1: TDBMemo;
    GrList: TwwDBGrid2;
    CmdAudit: TAction;
    QrDMKHOTHELENH: TWideStringField;
    QrDMKHOLOC: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrDMKHOBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrDMKHOBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDMKHOBeforeInsert(DataSet: TDataSet);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure BtThelenhClick(Sender: TObject);
    procedure QrDMKHOBeforeOpen(DataSet: TDataSet);
  private
  	mCanEdit, fixCode: Boolean;

  public
  	procedure Execute(r: WORD);
  end;

var
  FrmDmkho: TFrmDmkho;

implementation

uses
	ExCommon, isDb, isLib, isMsg, Rights, MainData, RepEngine, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_KHO';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.Execute(r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsDMKHO.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDMKHO, FORM_CODE, Filter);
    PD2.Collapsed := RegReadBool(Name, 'PD2');

	fixCode := SetCodeLength(FORM_CODE, QrDMKHO.FieldByName('MAKHO'));
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.FormShow(Sender: TObject);
begin
    QrDMKHO.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
        CloseDataSets([QrDMKHO]);
    finally
    end;

	// Save state panel
    RegWrite(Name, 'PD2', PD2.Collapsed);

	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrDMKHO, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdNewExecute(Sender: TObject);
begin
    QrDMKHO.Append;
    EdMAKHO.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdSaveExecute(Sender: TObject);
begin
    QrDMKHO.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdCancelExecute(Sender: TObject);
begin
    QrDMKHO.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdDelExecute(Sender: TObject);
begin
    QrDMKHO.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdPrintExecute(Sender: TObject);
begin
	ShowReport(Caption, FORM_CODE, [sysLogonUID]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDMKHO)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrDmkho, Filter, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.QrDMKHOBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.QrDMKHOBeforePost(DataSet: TDataSet);
begin
	with QrDMKHO do
    begin
	    if BlankConfirm(QrDMKHO, ['MAKHO']) then
   			Abort;

	    if fixCode then
			if LengthConfirm(QrDMKHO, ['MAKHO']) then
        		Abort;

	    if BlankConfirm(QrDMKHO, ['TENKHO']) then
   			Abort;

        with QrDMKHO.FieldByName('LOC') do
            if IsNull then
                AsString  := sysLoc;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.QrDMKHOBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.QrDMKHOBeforeOpen(DataSet: TDataSet);
begin
    QrDMKHO.Parameters[0].Value := sysLoc;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Field.FullName = 'MAKHO' then
    begin
        AFont.Style := [fsBold];
        if Highlight then
            AFont.Color := clWhite
        else
            AFont.Color := clPurple
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrDMKHO, Filter);
end;

procedure TFrmDmkho.BtThelenhClick(Sender: TObject);
begin
    (Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmkho.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDMKHO);
end;

end.
