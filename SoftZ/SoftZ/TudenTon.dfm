object FrmTudenTon: TFrmTudenTon
  Left = 531
  Top = 303
  HelpContext = 1
  BorderStyle = bsDialog
  Caption = 'Th'#7901'i Gian'
  ClientHeight = 257
  ClientWidth = 418
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    418
    257)
  PixelsPerInch = 96
  TextHeight = 16
  object CmdReturn: TBitBtn
    Left = 212
    Top = 211
    Width = 97
    Height = 36
    Cursor = 1
    Anchors = [akRight, akBottom]
    Caption = 'T'#237'nh t'#7891'n'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFBC7F48
      BD711DBE7220BE7321BE7321BE7322BE7322BE7322BE7322BE7322BE7322BE74
      23BB6D16B9773FFFFFFFFFFFFFCB9351E3A831DB961ADD9920DC9417DB9414DB
      9414DB9414DB9414DB9414DB9414DB9414DD9615C17F35FFFFFFFFFFFFCE9656
      E0A536D89724D99624DEA544E2AF53E4AE50E4AE50E3AE50E3AE50E3AE50E3AE
      50E4B151C88946FFFFFFFFFFFFC48852E5B863E09F2CDD9B2DE1AB4EC1844DBD
      7F4DC28650C0824AC0824AC0824AC0824AC1834CBF7F47FFFFFFFFFFFFF5E9E0
      CA925DE8BD6CE2A63BE2A538D6932FC07C44FCF9F6FFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEFDFCCB9058EBC06AE4AB41E7B14AE3
      AA47CA8B4AFDFAF7FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFF8F0E8D19A5DEDC677E8AF4DEAB755E7B150CC8C49FBF5F0FFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF7EDE3D7A365ECC372E9
      B75AECBC5FE8B85AD0944CF9F2EAFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFE1BA8FE3B261EDBF63EABA5DF2C76CE1AC52E6C6A3FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE9CAA5E5BA75F2D18CEF
      CA80F5DA9CE6BB6FE2BC8EFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFE4BC88EED29FFAE9BEF6E1B5F9E9C2EBCA94ECD2B3FFFFFFFFFFFFFFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFEFBF8E4BB80F3D9A9F9E8BEF6DFB2F7E4B9E5
      BC79F3E0C9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFE5BB7B
      F7E2B8FAE8BFF7E1B5F9E5BEEBCB8FDCA14BE2AE60E0AB5AE0AB59E0A959E0A9
      59E0AA5BE1AD60FFFFFFFFFFFFEDCD95FAE9C6F9E3B5F9E3B7F9E4B6F8E5B8FB
      ECC5FCEDC7FBECC6FBECC6FBECC6FBECC7FCEEC9EECC8DFFFFFFFFFFFFEFCF98
      FDEED3FCECC7FCECC8FCECC8FCECC9FCECC9FCECC8FCECC8FCECC8FCECC8FCEC
      C9FCEDCCEECD8FFFFFFFFFFFFFEDC982F1D195F1D198F1D199F1D199F1D198F1
      D198F1D198F1D198F1D198F1D198F1D198F1CF96EDC981FFFFFF}
    ParentFont = False
    TabOrder = 0
    OnClick = CmdReturnClick
    ExplicitLeft = 211
    ExplicitTop = 191
  end
  object BtnCancel: TBitBtn
    Left = 312
    Top = 211
    Width = 97
    Height = 36
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'K'#7871't th'#250'c'
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FF00FFFF00FF
      CAE2EC64ADCA5CAAC9DCE6E9FF00FFFF00FFFF00FFFF00FFFF00FFFF00FFFF00
      FFFF00FFFF00FFFF00FFFF00FF8DC3D861B0CE71C1DB61AFCDA5B9C0DCDCDCFF
      00FFFF00FFFF00FF519755609C62FF00FFFF00FFFF00FFFF00FF64ADCA6BBDD7
      78CCE377CAE25CAAC94987A0437C934E8FAA519CB118823318A730097A12DDEB
      F0FF00FFFF00FFFF00FF5CAAC97AD2E779D0E678CEE45CAAC95796B05690A85B
      A0B21F903D1EAF3B1CB5390A7E143D8F85FF00FFFF00FFFF00FF5CAAC97CD6EA
      7BD4E87AD2E75CAAC95A9BB64C98971F963D23B64723BB461CB53914A7290B8A
      16217C25CEE0CEFF00FF5CAAC97DD9EC7DD7EB7CD6EA5CAAC95CA9C82FAB4447
      C86829C15323BB461CB53916AF2D10A9210A9F14127715CEE0CE5CAAC97FDDEE
      7EDBED7EDAED7ED9EC77D0E530B148BBF6C048CC6A23BB461CB53916AF2D10A9
      210AA31505970A418A445CAAC981E1F180E0F080DEEF7FDEEF7FDDEE4FAAAB3D
      B753BAF5BF4DCC681CB5391093200D951B0AA315059E090475075CAAC982E4F3
      82E3F382E3F281E2F281E2F15CAAC959B9BF3DB654AAEDAF5ED06F10952165B0
      6C0A8F14059E09057A0A5CAAC984E8F683E7F583E6F583E6F483E6F45CAAC96C
      C4EA5EC2CB25AB3FA9EDAE3FAE4AFF00FF47A250059E09067F0C5CAAC985EBF8
      85EAF785EAF785E9F785E9F65CAAC96FC9F070CDF563CCD72BAD4C2BA44DFF00
      FF57B25E059E0915861E5CAAC987EEFA86EEF986EDF986EDF986EDF95CAAC970
      CCF572CFF875D5FF75D5FF5EAECEC2E3C70C9A1809951284BF8A5CAAC988F1FC
      88F1FC88F0FB87F0FB87F0FB5CAAC972D0F974D3FD75D5FF75D5FF51ADB012A0
      240E9E1B4AAA54FF00FF5CAAC989F4FD89F3FD89F3FD86EEF978D7E95CAAC98B
      DFFF8BE0FF8BE0FFABEFFF52B0B214A52979C683FF00FFFF00FF5CAAC98AF5FF
      78D9EA6AC1DA5CAAC97FC5DAA9E4EECCFFFFCCFFFFCCFFFFB8F1F769B6D3FF00
      FFFF00FFFF00FFFF00FF79B8D15CAAC95EADCD60B1D161B4D562B5D662B5D662
      B5D662B5D663B7D86BBAD8C2E1EDFF00FFFF00FFFF00FFFF00FF}
    ModalResult = 2
    ParentFont = False
    TabOrder = 1
    ExplicitLeft = 311
    ExplicitTop = 191
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 148
    Width = 402
    Height = 57
    Anchors = [akLeft, akRight, akBottom]
    Caption = 'Ch'#7885'n ng'#224'y t'#237'nh t'#7891'n'
    TabOrder = 2
    object Label15: TLabel
      Left = 19
      Top = 25
      Width = 47
      Height = 16
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label16: TLabel
      Left = 215
      Top = 25
      Width = 54
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object EdTonDen: TwwDBDateTimePicker
      Left = 280
      Top = 21
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
      OnExit = EdTonDenExit
    end
    object EdTonTu: TwwDBDateTimePicker
      Left = 76
      Top = 21
      Width = 101
      Height = 24
      TabStop = False
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 1
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 8
    Width = 401
    Height = 134
    Anchors = [akLeft, akTop, akRight]
    Caption = 'Th'#244'ng tin'
    TabOrder = 3
    object Label2: TLabel
      Left = 136
      Top = 49
      Width = 133
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y kh'#243'a s'#7893' h'#224'ng h'#243'a'
    end
    object Label3: TLabel
      Left = 141
      Top = 101
      Width = 129
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y t'#237'nh t'#7891'n g'#7847'n nh'#7845't'
    end
    object Label4: TLabel
      Left = 97
      Top = 74
      Width = 172
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y thay '#273#7893'i s'#7889' li'#7879'u g'#7847'n nh'#7845't'
    end
    object Label1: TLabel
      Left = 68
      Top = 22
      Width = 201
      Height = 16
      Alignment = taRightJustify
      Caption = 'Ng'#224'y b'#7855't '#273#7847'u s'#7917' d'#7909'ng ch'#432#417'ng tr'#236'nh'
    end
    object EdKhoaso: TwwDBDateTimePicker
      Left = 280
      Top = 43
      Width = 101
      Height = 24
      TabStop = False
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 1
      OnExit = EdTonDenExit
    end
    object EdStockDateMin: TwwDBDateTimePicker
      Left = 280
      Top = 70
      Width = 101
      Height = 24
      TabStop = False
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 2
      OnExit = EdTonDenExit
    end
    object EdStockDate: TwwDBDateTimePicker
      Left = 280
      Top = 97
      Width = 101
      Height = 24
      TabStop = False
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 3
    end
    object EdBegDate: TwwDBDateTimePicker
      Left = 280
      Top = 16
      Width = 101
      Height = 24
      TabStop = False
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      CalendarAttributes.PopupYearOptions.StartYear = 1999
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Enabled = False
      Font.Charset = ANSI_CHARSET
      Font.Color = clNavy
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ReadOnly = True
      ShowButton = False
      TabOrder = 0
      OnExit = EdTonDenExit
    end
  end
end
