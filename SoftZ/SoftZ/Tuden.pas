﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Tuden;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, Vcl.Mask, DBCtrlsEh,
  Vcl.DBCtrls, Vcl.ComCtrls, SysUtils, DateUtils;

type
  TFrmTuden = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdTu: TwwDBDateTimePicker;
    Label2: TLabel;
    EdDen: TwwDBDateTimePicker;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    CbbThoiGian: TDBComboBoxEh;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure CbbThoiGianChange(Sender: TObject);

  private
  public
  	function Execute(var ngayd, ngayc : TDateTime) : Boolean;
    function setValueDatePicker(itemIndex: Integer) : Boolean;
  end;

var
  FrmTuden: TFrmTuden;

implementation

{$R *.DFM}

uses
	MainData, MasterData, ExCommon, isStr, isMsg, GmsRep, isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTuden.CbbThoiGianChange(Sender: TObject);
begin
    setValueDatePicker(CbbThoiGian.ItemIndex);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTuden.Execute(var ngayd, ngayc : TDateTime) : Boolean;
begin

   	EdTu.Date  := ngayd;
   	EdDen.Date := ngayc;

	Result := ShowModal = mrOK;
	if Result then
    begin
    	ngayd := EdTu.Date;
    	ngayc := EdDen.Date;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTuden.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTuden.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
    CbbThoiGian.ItemIndex := CbbThoiGian.KeyItems.IndexOf('Early next month now');
    CbbThoiGianChange(CbbThoiGian);
	CbbThoiGian.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTuden.setValueDatePicker;
var
     sKey: string;
     mToDay, mDate, mDateStart, mDateEnd: TDateTime;
     mDayOfWeek, mDayOfMonth, mDayOfYear, mMonth, mQuarter, mDaySunday, mDayStart, mDayEnd: Integer;
begin

    mDaySunday := 1;
    mDayStart := 2;
    mDayEnd := 8;

    mToDay := Date;
    mMonth := MonthOf(mToDay);
    mDayOfWeek := DayOfWeek(mToDay);
    mDayOfMonth := DaysInAMonth(YearOf(mToDay), MonthOf(mToDay));
    mDayOfYear := DaysInAYear(YearOf(mToDay));

    EdTu.ReadOnly := (itemIndex < 0);
    EdDen.ReadOnly := (itemIndex < 0);

    if itemIndex < 0 then
    begin
        EdTu.Clear;
        EdDen.Clear;
    end
    else
    begin
        sKey := CbbThoiGian.KeyItems[itemIndex];
        if sKey = 'Today' then
        begin
            mDateStart  := mToDay;
            mDateEnd := mToDay;
        end
        else if sKey = 'This week' then
        begin
            if mDayOfWeek = mDaySunday then
            begin
                mDateStart  := IncDay(mToDay, - (mDayEnd - mDayStart));
                mDateEnd := mToDay;
            end
            else
            begin
                mDateStart  := IncDay(mToDay, - (mDayOfWeek - mDayStart));
                mDateEnd := IncDay(mToDay, (mDayEnd - mDayOfWeek));
            end;
        end
        else if sKey = 'Early next week now' then
        begin
            mDateStart  := EncodeDate(YearOf(mToDay), MonthOf(mToDay), 01);
            mDateEnd := mToDay;
        end
        else if sKey = 'This month' then
        begin
            mDateStart  := EncodeDate(YearOf(mToDay), MonthOf(mToDay), 01);
            mDateEnd := EncodeDate(YearOf(mToDay), MonthOf(mToDay), mDayOfMonth);
        end
        else if sKey = 'Early next month now' then
        begin
            mDateStart  := EncodeDate(YearOf(mToDay), MonthOf(mToDay), 01);
            mDateEnd := mToDay;
        end
        else if sKey = 'Last week' then
        begin
            if mDayOfWeek = mDaySunday then
            begin
                mDateEnd := IncDay(mToDay, - (mDayEnd + mDayOfWeek - mDayStart));
                mDateStart  := IncDay(mDateEnd, - (mDayEnd - mDayStart));
            end
            else
            begin
                mDateEnd := IncDay(mToDay, - (mDayOfWeek - mDaySunday));
                mDateStart  := IncDay(mDateEnd, - (mDayEnd - mDayStart));
            end;
        end
        else if sKey = 'Last month' then
        begin
            mDate := IncMonth(mToDay, - mDaySunday);
            mDayOfMonth := DaysInAMonth(YearOf(mDate), MonthOf(mDate));

            mDateStart  := EncodeDate(YearOf(mDate), MonthOf(mDate), 01);
            mDateEnd := EncodeDate(YearOf(mDate), MonthOf(mDate), mDayOfMonth);
        end
        else if sKey = 'Last quarter' then
        begin
            mQuarter := mMonth Mod (mDaySunday + mDayStart);
            if mQuarter = 0 then
                mDate := IncMonth(mToDay, - (mDaySunday + mDayStart))
            else
                mDate := IncMonth(mToDay, - mQuarter);

            mDayOfMonth := DaysInAMonth(YearOf(mDate), MonthOf(mDate));
            mDateEnd := EncodeDate(YearOf(mDate), MonthOf(mDate), mDayOfMonth);

            mDate := IncMonth(mDateEnd, - mDayStart);
            mDateStart  := EncodeDate(YearOf(mDate), MonthOf(mDate), 01);
        end
        else if sKey = 'Last year' then
        begin
            mDate := IncYear(mToDay, - mDaySunday);
            mDateStart  := EncodeDate(YearOf(mDate), 01, 01);
            mDateEnd := EncodeDate(YearOf(mDate), 12, 31);
        end
        else if sKey = 'Tomorrow' then
        begin
            mDate := IncDay(mToDay, mDaySunday);
            mDateStart  := mDate;
            mDateEnd := mDate;
        end
        else if sKey = 'Next week' then
        begin
            if mDayOfWeek = mDaySunday then
            begin
                mDateStart := IncDay(mToDay, mDaySunday);
                mDateEnd  := IncDay(mDateStart, (mDayEnd - mDayStart));
            end
            else
            begin
                mDateStart := IncDay(mToDay, (mDayEnd + mDaySunday - mDayOfWeek));
                mDateEnd  := IncDay(mDateStart, (mDayEnd - mDayStart));
            end;

        end
        else if sKey = 'Next month' then
        begin
            mDate := IncMonth(mToDay, mDaySunday);
            mDayOfMonth := DaysInAMonth(YearOf(mDate), MonthOf(mDate));

            mDateStart  := EncodeDate(YearOf(mDate), MonthOf(mDate), 01);
            mDateEnd := EncodeDate(YearOf(mDate), MonthOf(mDate), mDayOfMonth);
        end;


        EdTu.Date  := mDateStart;
        EdDen.Date := mDateEnd;
    end;
end;


end.
