﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TudenTon;

interface

uses
  Classes, Controls, Forms, StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls,
  SysUtils;

type
  TFrmTudenTon = class(TForm)
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    Label15: TLabel;
    Label16: TLabel;
    EdTonDen: TwwDBDateTimePicker;
    EdTonTu: TwwDBDateTimePicker;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    Label2: TLabel;
    EdKhoaso: TwwDBDateTimePicker;
    Label3: TLabel;
    Label4: TLabel;
    EdStockDateMin: TwwDBDateTimePicker;
    EdStockDate: TwwDBDateTimePicker;
    Label1: TLabel;
    EdBegDate: TwwDBDateTimePicker;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
    procedure EdTonDenExit(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure CmdReturnClick(Sender: TObject);
  private
    FStockDate, mNgayc: TDateTime;
    mStockControl, mMustCalc: Boolean;
  public
  	function Execute(var ngayc : TDateTime) : Boolean;
    function  IsStockControl: Boolean;
  end;

var
  FrmTudenTon: TFrmTudenTon;

implementation

{$R *.DFM}

uses
	MainData, MasterData, ExCommon, isStr, isMsg, isLib, Rights;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_NO_CALCMONTH = 'Chưa chốt tồn kho tháng %s/%d';
    RS_CANT_CALC = 'Không được tính tồn kho trước ngày bắt đầu'#13'sử dụng chương trình %s ';
    RS_CALC_CFM  = 'Xác nhận tính tồn kho đến ngày %s?';
procedure TFrmTudenTon.CmdReturnClick(Sender: TObject);
var
	mDate, mStockDateBegin, mStockDateEnd, mDateEnd: TDateTime;
    yy, mm, dd: Word;
    b: Boolean;
    	(*
	    **
    	*)
    procedure Khoaso;
    begin
		ErrMsg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
    end;
begin
    mDate := EdTonDen.Date;

    DecodeDate(IncMonth(mDate, 1), yy, mm, dd);
    mDateEnd := EncodeDate(yy, mm, 1) - 1;

    DecodeDate (FStockDate, yy, mm, dd);
    mStockDateBegin := EncodeDate(yy, mm, 1);
    mStockDateEnd := IncMonth(mStockDateBegin, 1) - 1;

    if mDate < sysBegDate then
    begin
    	Msg(Format(RS_CANT_CALC, [DateToStr(sysBegDate)]));
    	Exit;
    end;

    if sysCloseHH > mDate then	// Da khoa so
    begin
        Khoaso;
        Exit;
    end;

    // Thang moi
    if mDate > mStockDateEnd then
    begin
        b := False;
        if FStockDate = mStockDateEnd then
        begin
            if mDate > IncMonth(mStockDateBegin, 2) then
            begin
                ErrMsg(Format(RS_NO_CALCMONTH, [isInt2Str(mm + 1, 2), yy]));
                Exit;
            end
        end else
        begin
            if mDate > mStockDateEnd then
            begin
                ErrMsg(Format(RS_NO_CALCMONTH, [isInt2Str(mm, 2), yy]));
                Exit;
            end
        end;

    end;

    if not YesNo(Format(RS_CALC_CFM, [DateToStr(mDate)])) then
        Exit;

    DataMain.CalcStock(mDate);
    MsgDone;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTudenTon.EdTonDenExit(Sender: TObject);
var
	mDate, mBeg, mEnd: TDateTime;
    yy, mm, dd: Word;
    	(*
	    **
    	*)
    procedure Khoaso;
    begin
		Msg(Format(RS_BOOKCLOSED, [DateToStr(sysCloseHH)]));
    end;
begin
    mDate := EdTonDen.Date;

    DecodeDate(mDate, yy, mm, dd);
    mEnd := EncodeDate(yy, mm, 1) - 1;

	EdTonTu.Date := EncodeDate(yy, mm, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTudenTon.Execute(var ngayc : TDateTime) : Boolean;
begin
    mNgayc := ngayc;
	Result := ShowModal = mrOK;
	if Result then
    begin
    	ngayc := EdTonDen.Date;
    end;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTudenTon.FormCreate(Sender: TObject);
begin
    mStockControl := rCanEdit(GetRights('SZ_TONTT', False));
end;

procedure TFrmTudenTon.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTudenTon.FormShow(Sender: TObject);
var
    dd, mm, yy: WORD;
    d, dStock, dStockMin, dKS: TDateTime;
begin
    TMyForm(Self).Init;
    dStockMin := GetSysParam('STOCK_DATE_MIN');
    dStock := GetSysParam('STOCK_DATE');
    dKS := GetSysParam('KS_HH');

    d := dStockMin;
    if d < 10 then
        d := dStock;
    DecodeDate(d, yy, mm, dd);
    FStockDate := EncodeDate(yy, mm, dd);

    EdKhoaso.Date := dKS;
    EdStockDate.Date := dStock;
    EdStockDateMin.Date :=  dStockMin;

    EdTonDen.Date := mNgayc;
    EdTonDenExit(EdTonDen);

    EdBegDate.Date := sysBegDate;

//	EdTonDen.SetFocus;
end;

function TFrmTudenTon.IsStockControl: Boolean;
begin
    Result := mStockControl;
end;

end.
