﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonDsHHKM;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls,
  wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, Buttons, wwDialog, wwfltdlg,
  wwFltDlg2, AppEvnts, DBGridEh, DBCtrlsEh, Vcl.Mask, DBLookupEh,
  DbLookupComboboxEh2, Variants, kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmChonDsHHKM = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsPhieu: TDataSource;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdRefresh: TAction;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    SpBt1: TSpeedButton;
    SpBt2: TSpeedButton;
    QrDMKHO: TADOQuery;
    Status: TStatusBar;
    Panel1: TPanel;
    ApplicationEvents1: TApplicationEvents;
    Filter: TwwFilterDialog2;
    DsDMKHO: TDataSource;
    CbKhoHang: TDbLookupComboboxEh2;
    EdMaKho: TDBEditEh;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure SpBt1Click(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mMADT, mMakho, mString: String;
    mType: Integer;
    mKhoa: TGUID;
  public
  	function Execute(pType: Integer; pString,
            pMadt: String; pKhoa: TGUID): Boolean;
  end;

var
  FrmChonDsHHKM: TFrmChonDsHHKM;

implementation

uses
	isDb, ExCommon, isLib, Khuyenmai, isCommon, GuidEx;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonDsHHKM.Execute;
begin
    mMakho := '~';
	mMADT := pMADT;
    mString := pString;
    mType := pType;
    mKhoa := pKhoa;
    DsPhieu.DataSet := FrmKhuyenmai.QrList;

    with TbDummyEh do
    begin
        Open;
    	Append;
    end;

    Result := ShowModal = mrOK;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetCustomGrid('CHON_DS_HHKM', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
    QrDMKHO.Open;

//    CbKHO.LookupValue := sysDefKho;
    GrBrowse.SetFocus;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.SpBt1Click(Sender: TObject);
var
    bm: TBytes;
begin
    with FrmKhuyenmai.QrList do
    begin
        bm := Bookmark;
        First;
        while not Eof do
        begin
            Edit;
            FieldByName('SELECTED').AsBoolean := (Sender as TSpeedButton).Tag = 0;
            Next;
        end;
        Bookmark := bm;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.TbDummyEhAfterInsert(DataSet: TDataSet);
begin
     with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mMakho;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsPhieu);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.CmdRefreshExecute(Sender: TObject);
var
    s: String;
begin

    s := EdMaKho.Text;

    if mMakho <> s then
	begin
        mMakho := s;

        with FrmKhuyenmai.QrList do
        begin
            if Active then
    			Close;

            Parameters[1].Value := mMakho;
            Parameters[2].Value := mType;
            Parameters[3].Value := mString;
            Parameters[4].Value := mMadt;
            Parameters[5].Value := TGuidEx.ToString(mKhoa);

            Open;
        end;

        SetDisplayFormat(FrmKhuyenmai.QrList, sysCurFmt);
        SetDisplayFormat(FrmKhuyenmai.QrList, ['TL_CK'], sysPerFmt);
        SetShortDateFormat(FrmKhuyenmai.QrList);
        SetDisplayFormat(FrmKhuyenmai.QrList, ['TUGIO', 'DENGIO'], 'hh:nn');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
    Status.SimpleText := exRecordCount(FrmKhuyenmai.QrList, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonDsHHKM.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

end.
