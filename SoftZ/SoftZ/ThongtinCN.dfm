﻿object FrmThongtinCN: TFrmThongtinCN
  Left = 561
  Top = 513
  HelpContext = 1
  BorderIcons = []
  BorderStyle = bsSingle
  Caption = 'Th'#244'ng Tin C'#244'ng N'#7907' Kh'#225'ch H'#224'ng'
  ClientHeight = 647
  ClientWidth = 775
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  DesignSize = (
    775
    647)
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 8
    Top = 8
    Width = 759
    Height = 601
    Anchors = [akLeft, akTop, akRight, akBottom]
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    DesignSize = (
      759
      601)
    object GroupBox1: TGroupBox
      Left = 9
      Top = 1
      Width = 740
      Height = 162
      Anchors = [akLeft, akTop, akRight]
      Caption = ' Th'#244'ng tin h'#243'a '#273#417'n '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 0
      DesignSize = (
        740
        162)
      object Label6: TLabel
        Left = 87
        Top = 28
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'T'#234'n '#273#417'n v'#7883
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label7: TLabel
        Left = 725
        Top = 24
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object Label1: TLabel
        Left = 45
        Top = 116
        Width = 149
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' h'#243'a '#273#417'n'
        FocusControl = wwDBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 17
      end
      object Label4: TLabel
        Left = 91
        Top = 72
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'M'#227' s'#7889' thu'#7871
        FocusControl = DBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 63
      end
      object EdTEN: TwwDBEdit
        Tag = 1
        Left = 210
        Top = 24
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        AutoSize = False
        Color = 15794175
        Ctl3D = False
        DataField = 'CN_TENDV'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TwwDBEdit
        Left = 210
        Top = 112
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI_HD'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit1: TwwDBEdit
        Left = 210
        Top = 68
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_MST'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object GroupBox2: TGroupBox
      Left = 9
      Top = 163
      Width = 740
      Height = 210
      Anchors = [akLeft, akTop, akRight]
      Caption = ' Th'#244'ng tin giao h'#224'ng '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 1
      DesignSize = (
        740
        210)
      object Label15: TLabel
        Left = 143
        Top = 167
        Width = 51
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Email'
        FocusControl = wwDBEdit4
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 115
      end
      object Label2: TLabel
        Left = 57
        Top = 74
        Width = 137
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#432#7901'i li'#234'n h'#7879
        FocusControl = DBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 29
      end
      object Label3: TLabel
        Left = 14
        Top = 29
        Width = 180
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7883'a ch'#7881' giao h'#224'ng'
        FocusControl = DBEdit2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = -14
      end
      object Label5: TLabel
        Left = 87
        Top = 122
        Width = 107
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272'i'#7879'n tho'#7841'i'
        FocusControl = DBEdit5
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 59
      end
      object Label10: TLabel
        Left = 725
        Top = 114
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label8: TLabel
        Left = 725
        Top = 26
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object Label9: TLabel
        Left = 725
        Top = 70
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 697
      end
      object DBEdit1: TwwDBEdit
        Left = 210
        Top = 70
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_LIENHE'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit2: TwwDBEdit
        Left = 210
        Top = 26
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DIACHI'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit5: TwwDBEdit
        Left = 210
        Top = 114
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_DTHOAI'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit4: TwwDBEdit
        Left = 210
        Top = 158
        Width = 509
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        DataField = 'CN_EMAIL'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
    end
    object GroupBox3: TGroupBox
      Left = 9
      Top = 373
      Width = 740
      Height = 219
      Anchors = [akLeft, akTop, akRight]
      Caption = ' Th'#244'ng tin t'#224'i kho'#7843'n ng'#226'n h'#224'ng '
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = [fsItalic, fsUnderline]
      ParentFont = False
      TabOrder = 2
      DesignSize = (
        740
        219)
      object Label12: TLabel
        Left = 64
        Top = 76
        Width = 130
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ch'#7911' t'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 36
      end
      object Label13: TLabel
        Left = 116
        Top = 124
        Width = 78
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #272#7841'i di'#7879'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 88
      end
      object Label14: TLabel
        Left = 91
        Top = 180
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'Ng'#226'n h'#224'ng'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        ExplicitLeft = 63
      end
      object Label30: TLabel
        Left = 91
        Top = 33
        Width = 103
        Height = 25
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = 'T'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -21
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 63
      end
      object Label11: TLabel
        Left = 725
        Top = 29
        Width = 10
        Height = 18
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = '*'
        Font.Charset = ANSI_CHARSET
        Font.Color = clRed
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 729
      end
      object EdTENDT: TwwDBEdit
        Left = 210
        Top = 72
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_TENTK'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit2: TwwDBEdit
        Left = 210
        Top = 120
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_DAIDIEN'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit3: TwwDBEdit
        Left = 210
        Top = 165
        Width = 509
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        BorderStyle = bsNone
        Color = clBtnFace
        Ctl3D = False
        DataField = 'LK_NGANHANG'
        DataSource = FrmMain.DsBH
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlue
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        Frame.Enabled = True
        Frame.FocusBorders = [efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        ReadOnly = True
        TabOrder = 4
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBLookupCombo11: TwwDBLookupCombo
        Left = 210
        Top = 29
        Width = 312
        Height = 40
        Anchors = [akTop, akRight]
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'TENTK'#9'28'#9'TENTK'#9'F'
          'MATK'#9'11'#9'MATK'#11#9)
        DataField = 'CN_MATK'
        DataSource = FrmMain.DsBH
        LookupTable = DataMain.QrDMTK
        LookupField = 'MATK'
        Options = [loColLines]
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 0
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
      end
      object wwDBLookupCombo12: TwwDBLookupCombo
        Left = 525
        Top = 29
        Width = 194
        Height = 40
        TabStop = False
        Anchors = [akTop, akRight]
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -28
        Font.Name = 'Tahoma'
        Font.Style = []
        BorderStyle = bsNone
        DropDownAlignment = taRightJustify
        Selected.Strings = (
          'MATK'#9'11'#9'MATK'#11'F'
          'TENTK'#9'28'#9'TENTK'#9#9)
        DataField = 'CN_MATK'
        DataSource = FrmMain.DsBH
        LookupTable = DataMain.QrDMTK
        LookupField = 'MATK'
        Options = [loColLines]
        Style = csDropDownList
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
      end
    end
  end
  object CmdReturn: TBitBtn
    Left = 551
    Top = 616
    Width = 106
    Height = 25
    Anchors = [akRight, akBottom]
    Caption = 'Ti'#7871'p t'#7909'c (F5)'
    DoubleBuffered = True
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFBBE4C270CF8527B7471EBA401EBA4027B74770CF85BBE4C2FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFAFDFA4FB96219C1401FCE4C24DC5827
      DD5C27DD5C24DC581FCE4C19C1404FB962FAFDFAFFFFFFFFFFFFFFFFFFFBFDFB
      21A93A1ED04E21D45420D05304B62A18C4401DCE4A18C84420D15121D4541ED0
      4E21A93AFBFDFBFFFFFFFFFFFF4EB15B1ECE4D22D45615C9481CAC2F9DD2A137
      AF4614C13B1FD24E1ECE4B1ECD4A20D2531ECE4D4EB15BFFFFFFBDDEBE17BA3F
      21D85A13C64612A826F2F4ECFFFFFFEAF2E626AA380DC03920D24F1ECE491DCD
      4D20D75817BA3FBDDEBE6ABC7418D15214CB4E0BA01EF2F4ECFFFBFFFFFAFFFF
      FFFFEAF2E623A8350BC03A1ED3591CCF531ED25818CF516ABC7430A03F2DE172
      1BA82DF2F4ECFFF8FFEAF2E6A9D5A4EEF2EBFFFFFFD0EBD323A8340AC24218D6
      6213CF5430E17330A14130A34365EAA158B25CEAF2E6EAF2E60EB42F00BF303A
      B649F2F4ECFFFFFFEAF2E623A83307C13D24D86973F0B130A14223953778F4BC
      49CD7A74BF7F2DB64C24D3672ED87219CC5A48B558EAF2E6FFFFFFEAF2E626A7
      3125D06077F6BE23953533933D71F2B561E4A84CDB955BE1A561DEA563DDA463
      E2AB4DDA964FB860EEF2E8FFFFFFEAF2E62AB3436DF0B333933D67AB6686E3B5
      62E7A95DE2A460E2A65FE1A65FE1A65EE1A563E5AD4CDA954DB75EEAF0E5FFFF
      FF61BC6580DFAE67AB66B9D4B94EB068A8FCE15FE1A257E09F5BE0A35DE1A45D
      E1A45DE1A461E5AB4EDC9748BA605DC27096EABF4EB068B9D4B9FFFFFF458945
      7BDBA7B0FCE876E5B562E3AA5EE0A65EE1A65EE1A65EE0A566E6B06FE3AFA7F9
      E07ADCA8458945FFFFFFFFFFFFFAFDFA1572156DD6A3B3FDF0A4F5DF8CE9C78C
      E8C48AE7C28DE9C6A5F5DEB3FDF06DD6A3157215FAFDFAFFFFFFFFFFFFFFFFFF
      F9FCF945864538A75E7FE1B8A9FFECB9FFFBB9FFFBA9FFEC7FE1B838A75E4586
      45F9FCF9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFB7CEB767A567247D3328
      8738288738247D3367A567B7CEB7FFFFFFFFFFFFFFFFFFFFFFFF}
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 1
    OnClick = CmdOKExecute
  end
  object BitBtn2: TBitBtn
    Left = 661
    Top = 616
    Width = 97
    Height = 25
    Cursor = 1
    Anchors = [akRight, akBottom]
    Cancel = True
    Caption = 'Tho'#225't'
    DoubleBuffered = True
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Glyph.Data = {
      36030000424D3603000000000000360000002800000010000000100000000100
      1800000000000003000000000000000000000000000000000000FFFFFFFFFFFF
      FFFFFFFFFFFFEEEDF8A6A5E06765CE4946CB4946CB6765CEA6A5E0EEEDF8FFFF
      FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFA7A6E20B07CD100CED120EFE13
      0EFF130EFF120EFE100CEE0B07CDA7A6E2FFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
      8887E0100EE91412FC1313FC110FE90F0DE00F0DE0110FEA1211F71312FB100E
      EA8180DFFFFFFFFFFFFFFFFFFFA7A6E41214EB1619FC1416F21F1ED4AAA9ECAE
      AEE4B5B4E73C39C71315EF1416F61518F91214EBA7A6E4FFFFFFEEEEFA1013DB
      171FFB171DF18C8CECF6F6FDFFFFFFFFFFFF918FDF1A1FE3161EF7161DF4161E
      F6171EFA1013DBEEEEFAA8A7EC151EF11B27FB2224D9FFFFFFFFFFFFFFFFFF88
      85E0141BE61924F91923F61924F5151AE71A25F8151EF2A8A7EC7373E52739FD
      1521F1AAA9EDFFFFFFFFFFFF817EE1141BE31B2CF91B2AF61B2BF81113D93B38
      CF1521F02739FD7373E56062E6536CFF202FECB4B3F1FFFFFF8885E6161EE41E
      32F81C31F61E34F9161DE4918EE5C0BFF1202FEC546CFF5F61E35F61EC607BFF
      414FF1C1BFF4928EEA1820E62139F92037F62139F81821E98985E7FFFFFFB5B4
      F2414FF1607AFF5D5FE07978F15F77FF4C60F73D37E21318E5253FFA243CF826
      41FA1B26EC837EE8FFFFFFFFFFFFADABF54C60F95C75F87675E4ABA8F55C6CFD
      5E7CFE4251F43650FA223EF82845FA202FF28A86EEFFFFFFFFFFFFFFFFFF3B40
      EE6281FF5869E9A9A7EEEEEDFD4042F57490FF5E7BFD5C78FC3D5CFB2637F393
      90F2FFFFFFFFFFFFF6F6FF9B9EF9556DFC7591FD393BD2EEEDFDFFFFFFA8A6F8
      646EFA7A94FE617EFC5A73FB4843EFB6B3F9B1AEFAB4B5FB4047F35873FC7F9D
      FF5B65DFA6A4ECFFFFFFFFFFFFFFFFFF8D89F86772F98CA8FE7695FF5A72FC4F
      64FA4E63F95870FC7D9DFF8EAAFE636EE48887DAFFFFFFFFFFFFFFFFFFFFFFFF
      FFFFFFA8A6F84445F17A88F793A9FD98B1FF98B2FF92A9FB7988EF4042DFA6A4
      E9FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFEEEDFEAEACF67E7CF369
      69EF6869EE7C7CEEADABEDEEEDFDFFFFFFFFFFFFFFFFFFFFFFFF}
    ModalResult = 2
    ParentDoubleBuffered = False
    ParentFont = False
    TabOrder = 2
  end
  object MyActionList: TActionList
    Left = 316
    Top = 328
    object CmdOK: TAction
      Caption = 'Ti'#7871'p t'#7909'c'
      ShortCut = 116
      OnExecute = CmdOKExecute
    end
  end
end
