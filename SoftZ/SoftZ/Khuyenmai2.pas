﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Khuyenmai2;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2,
  ADODb, Db, Wwfltdlg2,
  Menus, AdvMenus, AppEvnts, wwDialog, wwfltdlg, Grids, Wwdbigrd, Wwdbgrid,
  ToolWin;

type
  TFrmKhuyenmai2 = class(TForm)
    ActionList: TActionList;
    CmdUpdate: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton11: TToolButton;
    Status: TStatusBar;
    QrKHUYENMAI_CT2: TADOQuery;
    DsKHUYENMAI_CT2: TDataSource;
    ToolButton7: TToolButton;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    QrKHUYENMAI_CT2MAKHO: TWideStringField;
    QrKHUYENMAI_CT2MARK: TBooleanField;
    QrKHUYENMAI_CT2LK_TENKHO: TWideStringField;
    Filter: TwwFilterDialog2;
    popCheck: TAdvPopupMenu;
    Tm1: TMenuItem;
    CmdCheckAll: TAction;
    CmdUnCheckAll: TAction;
    Bchn1: TMenuItem;
    CmdCancel: TAction;
    TntToolButton1: TToolButton;
    Bchn2: TMenuItem;
    Danhschkho1: TMenuItem;
    QrKHUYENMAI_CT2KHOA: TGuidField;
    DMVT_KHUYENMAI: TADOCommand;
    CmdUpdate2: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrDMKHO: TADOQuery;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdUpdateExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrKHUYENMAI_CT2BeforeOpen(DataSet: TDataSet);
    procedure CmdCheckAllExecute(Sender: TObject);
    procedure CmdUnCheckAllExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrKHUYENMAI_CT2AfterPost(DataSet: TDataSet);
    procedure QrKHUYENMAI_CT2BeforeEdit(DataSet: TDataSet);
    procedure CmdUpdate2Execute(Sender: TObject);
  private
  	mKhoa: TGUID;
    mChange, mChecked: Boolean;

    procedure MarkAll(bCheck: Boolean);
  public
  	procedure Execute(const pKhoa: TGUID; const pChecked: Boolean; const pSCT: String = '');
  end;

var
  FrmKhuyenmai2: TFrmKhuyenmai2;

implementation

uses
	ExCommon, isDb, isLib, isMsg, MainData, GuidEx;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.Execute;
begin
    mKhoa := pKhoa;
    mChecked := pChecked;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid('PHIEU_KHUYENMAI_BL2', GrList);
    SetDictionary(QrKHUYENMAI_CT2, 'PHIEU_KHUYENMAI_BL2');
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.FormShow(Sender: TObject);
begin
	OpenDataSets([DataMain.QrDMKHO, QrDMKHO, QrKHUYENMAI_CT2]);
    CmdUpdate.Execute;
    mChange := False;

    DsKHUYENMAI_CT2.AutoEdit := not mChecked;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        QrKHUYENMAI_CT2.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    try
    	if mChange then
            case SaveConfirm3 of
            mrYes:
                begin
                    CmdSave.Execute;
                    mChange := False;
                end;
            mrNo:
                begin
                    CmdCancel.Execute;
                    mChange := False;
                end;
            end;
    finally
	    CanClose := not mChange;
    end;       
//    CanClose := CheckBrowseDataSet(QrKHUYENMAI_CT2, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)

resourcestring
	RS_KHUYENMAI = 'Cập nhật thông tin khuyến mãi vào danh mục hàng hóa. Tiếp tục?';
	RS_ERROR = 'Cập nhật không thành công.';
procedure TFrmKhuyenmai2.CmdUpdate2Execute(Sender: TObject);
begin
    if YesNo(RS_KHUYENMAI, 1) then
    begin
        with DMVT_KHUYENMAI do
        begin
            Prepared := True;
            Parameters[1].Value := TGuidEx.ToString(mKhoa);
            Execute;
            if Parameters[0].Value = 0 then
            	MsgDone
            else
            	ErrMsg(RS_ERROR);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdUpdateExecute(Sender: TObject);
var
	kho: String;
begin
	mTrigger := True;
	with QrDMKHO do
    begin
    	First;
        while not Eof do
        begin
	    	kho := FieldByName('MAKHO').AsString;
    	    with QrKHUYENMAI_CT2 do
        		if not Locate('MAKHO', kho, []) then
	        	begin
            		Append;
	                TGuidField(FieldByName('KHOA')).AsGuid := mKhoa;
    	            FieldByName('MAKHO').AsString := kho;
        	        Post;
    	    	end;
            Next;
		end;
    end;
	mTrigger := False;
	QrKHUYENMAI_CT2.First;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdSaveExecute(Sender: TObject);
begin
    with QrKHUYENMAI_CT2 do
    begin
    	DisableControls;
    	CheckBrowseMode;
        First;
        while not Eof do
        begin
        	if not FieldByName('MARK').AsBoolean then
            	Delete
            else
            	Next;
        end;
        UpdateBatch;
        First;
    	EnableControls;
    end;
    mChange := False;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdCancelExecute(Sender: TObject);
begin
    with QrKHUYENMAI_CT2 do
    begin
        CancelBatch;
        Requery;
    end;
    mChange := False;
    CmdUpdate.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse: Boolean;
begin
    CmdSave.Enabled := mChange and not mChecked;
    CmdCancel.Enabled := mChange and not mChecked;

    with QrKHUYENMAI_CT2 do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    end;

    CmdUpdate2.Enabled := not CmdSave.Enabled;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.QrKHUYENMAI_CT2AfterPost(DataSet: TDataSet);
begin

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.QrKHUYENMAI_CT2BeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    mChange := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := exRecordCount(QrKHUYENMAI_CT2, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.QrKHUYENMAI_CT2BeforeOpen(DataSet: TDataSet);
begin
	QrKHUYENMAI_CT2.Parameters[0].Value := TGuidEx.ToString(mKhoa);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.MarkAll(bCheck: Boolean);
var
	bm: TBytes;
begin
	with QrKHUYENMAI_CT2 do
    begin
    	bm := Bookmark;
        DisableControls;
        First;
        while not Eof do
        begin
        	Edit;
        	FieldByName('MARK').AsBoolean := bCheck;
        	Next;
        end;
        Bookmark := bm;
//        CheckBrowseMode;
        EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdCheckAllExecute(Sender: TObject);
begin
	MarkAll(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmKhuyenmai2.CmdUnCheckAllExecute(Sender: TObject);
begin
	MarkAll(False);
end;


end.
