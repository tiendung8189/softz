﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit NhaptraBanleTimPhieu;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls, Vcl.Mask, wwdbedit, wwdbdatetimepicker, wwdblook,
  Data.DB, kbmMemTable, Data.Win.ADODB, Vcl.ActnList;

type
  TFrmNhaptraBanleTimPhieu = class(TForm)
    PaKho: TPanel;
    Label1: TLabel;
    BitBtn1: TBitBtn;
    BitBtn2: TBitBtn;
    EdThang: TComboBox;
    Label2: TLabel;
    EdNam: TComboBox;
    RgLoai: TRadioGroup;
    PaID: TPanel;
    Label4: TLabel;
    BitBtn3: TBitBtn;
    Panel3: TPanel;
    Label7: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    Label8: TLabel;
    wwDBEdit4: TwwDBEdit;
    LbKHO: TLabel;
    CbMaKho: TwwDBLookupCombo;
    CbTenKho: TwwDBLookupCombo;
    Label3: TLabel;
    wwDBLookupCombo1: TwwDBLookupCombo;
    wwDBLookupCombo2: TwwDBLookupCombo;
    kbmTemp: TkbmMemTable;
    kbmTempS1: TWideStringField;
    kbmTempS2: TWideStringField;
    kbmTempS3: TWideStringField;
    DsTemp: TDataSource;
    kbmTemp_id: TLargeintField;
    QrDMQUAY: TADOQuery;
    spCHECK_BILL_BANLE: TADOStoredProc;
    DsResult: TDataSource;
    EdSct: TwwDBEdit;
    wwDBEdit1: TwwDBEdit;
    spCHECK_BILL_BANLEKHOA: TGuidField;
    spCHECK_BILL_BANLENGAY: TDateTimeField;
    spCHECK_BILL_BANLEMaKho: TWideStringField;
    spCHECK_BILL_BANLEMaQuay: TWideStringField;
    spCHECK_BILL_BANLESCT: TWideStringField;
    spCHECK_BILL_BANLEMAVIP: TWideStringField;
    spCHECK_BILL_BANLENhanVienThuNgan: TIntegerField;
    QrDMKHO: TADOQuery;
    Action: TActionList;
    procedure FormShow(Sender: TObject);
    procedure BitBtn3Click(Sender: TObject);
    procedure kbmTempS1Change(Sender: TField);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
  private
    mKhoaResult: TGUID;
    mNgayResult: TDateTime;
    mSctResult, mMaVIPResult: string;
  public
  	function Execute (i_year, i_month : Integer; i_makho, i_maquay : String;
            var o_khoa: TGUID;
            var o_ngay: TDateTime;
            var o_sct: String;
            var o_mavip: String) : Boolean;
  end;

var
  FrmNhaptraBanleTimPhieu: TFrmNhaptraBanleTimPhieu;

implementation

{$R *.DFM}

uses
	isLib, isStr, MainData, isMsg, isCommon, isDb;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptraBanleTimPhieu.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    bPhieu: Boolean;
    n: Integer;
begin
    n := RgLoai.ItemIndex;
    PaKho.Enabled := n = 0;
    PaID.Enabled := n = 1;

    bPhieu := False;
    with spCHECK_BILL_BANLE do
    begin
        if Active then
        begin
            bPhieu := FieldByName('Sct').AsString <> '';
        end;
    end;

    BitBtn1.Enabled := bPhieu;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptraBanleTimPhieu.BitBtn3Click(Sender: TObject);
var
    _id: Largeint;
    n,  mm, yy: Integer;
    makho, maquay, sct, _msg: String;
begin
    n := RgLoai.ItemIndex;
    with kbmTemp do
    begin
        CheckBrowseMode;
        if n = 0 then
        begin
            _id := 0;
            makho := FieldByName('MaKho').AsString;
            maquay := FieldByName('MaQuay').AsString;
            sct := FieldByName('Sct').AsString;
            yy := StrToInt(EdNam.Text);
            mm := StrToInt(EdThang.Text);

            if (makho = '') or
                (maquay = '') or
                (sct = '') then
            begin
                ErrMsg('Phải nhập đủ thông tin tra cứu Phiếu.');
                Abort;
            end;
        end else
        begin
            _id := FieldByName('_id').AsLargeInt;
            makho := '';
            maquay := '';
            sct := '';
            yy := 0;
            mm := 0;

            if _id = 0 then
            begin
                ErrMsg('Phải nhập đủ thông tin tra cứu Phiếu.');
                Abort;
            end;
        end;
    end;

    try
        with spCHECK_BILL_BANLE do
        begin
            if Active then
                Close;

            Prepared := True;
            Parameters.ParamByName('@p_id').Value := _id;
            Parameters.ParamByName('@pMaKho').Value := makho;
            Parameters.ParamByName('@pMaQuay').Value := maquay;
            Parameters.ParamByName('@pSCT').Value := sct;
            ExecProc;

            if Parameters.ParamValues['@RETURN_VALUE'] <> 0 then
            begin
                mKhoaResult := TGUID.Empty;
                mNgayResult := 0;
                mSctResult := '';
                mMaVIPResult := '';

                _msg := Parameters.ParamValues['@returnCode'];
                if _msg <> '' then
                    ErrMsg(_msg);
                Close;
            end else
            begin
                Active := True;

                mKhoaResult := TGuidField(FieldByName('Khoa')).AsGuid;
                mNgayResult := FieldByName('Ngay').AsDateTime;
                mSctResult := FieldByName('Sct').AsString;
                mMaVIPResult := FieldByName('MaVIP').AsString;
            end;
        end;
    except on E: Exception do
        begin
            ClearWait;
            ErrMsg(e.Message);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmNhaptraBanleTimPhieu.Execute;
begin
    isYearList(EdNam, i_year);
	isMonthList(EdThang, i_month);
    OpenDataSets([QrDMKHO, QrDMQUAY]);

    with kbmTemp do
    begin
        if Active then Close;
        Open;
        Append;
        FieldByName('MaKho').AsString := i_makho;
        FieldByName('MaQuay').AsString := i_maquay;
    end;

   	Result := ShowModal = mrOK;

    if Result then
    begin
        if mSctResult <> '' then
        begin
            o_khoa := mKhoaResult;
            o_ngay := mNgayResult;
            o_sct := mSctResult;
            o_mavip := mMaVIPResult;
        end else
        begin
            o_khoa := TGUID.Empty;
            o_ngay := 0;
            o_sct := '';
            o_mavip := '';
        end;

    end;
    kbmTemp.Cancel;
    Free;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptraBanleTimPhieu.FormShow(Sender: TObject);
begin
	TMyForm(Self).Init;
    RgLoai.ItemIndex := 0;
    EdSct.SetFocus;
    EdSct.SelectAll;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmNhaptraBanleTimPhieu.kbmTempS1Change(Sender: TField);
var
    s: String;
begin
    s := Trim(kbmTemp.FieldByName('MaKho').AsString);

    with QrDMQUAY do
    begin
        if s <> kbmTemp.FieldByName('MaKho').OldValue then
            kbmTemp.FieldByName('MaQuay').Clear;

        if s = '' then
            Filter := ''
        else
            Filter := 'MaKho=' + QuotedStr(s);
//        First;
    end;
end;

end.
