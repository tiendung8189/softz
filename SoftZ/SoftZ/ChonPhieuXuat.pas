﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChonPhieuXuat;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  Menus, ActnList, AdvMenus, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, AdvEdit, DBAdvEd,
  Vcl.Mask, wwdbedit, DBGridEh, DBCtrlsEh, DBLookupEh, DbLookupComboboxEh2, Variants,
  kbmMemTable, MemTableDataEh, MemTableEh;

type
  TFrmChonPhieuXuat = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    DsKHACHHANG: TDataSource;
    QrPHIEUXUAT: TADOQuery;
    QrPHIEUXUATNGAY: TDateTimeField;
    QrPHIEUXUATSCT: TWideStringField;
    QrPHIEUXUATMAKHO: TWideStringField;
    ActionList1: TActionList;
    CmdChose: TAction;
    CmdClose: TAction;
    CmdSearch: TAction;
    PopupMenu1: TAdvPopupMenu;
    Tm1: TMenuItem;
    QrDMKHO: TADOQuery;
    QrDMKH: TADOQuery;
    QrPHIEUXUATTENKHO: TWideStringField;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    QrPHIEUXUATMAKH: TWideStringField;
    QrPHIEUXUATTENDT: TWideStringField;
    QrPHIEUXUATKHOA: TGuidField;
    QrDMKHMADT: TWideStringField;
    QrDMKHTENDT: TWideStringField;
    QrPHIEUXUATNguoiGiao: TWideStringField;
    QrPHIEUXUATNguoiNhan: TWideStringField;
    QrPHIEUXUATGhiChu: TWideMemoField;
    QrPHIEUXUATHanThanhToan: TIntegerField;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    DsDMKHO: TDataSource;
    DsDMKH: TDataSource;
    TbDummyEh: TMemTableEh;
    DsDummyEh: TDataSource;
    procedure FormShow(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CbMADTKeyPress(Sender: TObject; var Key: Char);
    procedure TbDummyEhAfterInsert(DataSet: TDataSet);
  private
	mKH, mKHO, mSQL: String;
    mTungay, mDenngay: TDateTime;
  public
  	function Execute(pFix: Boolean; pKH, pKho: String): TGUID;
  end;

var
  FrmChonPhieuXuat: TFrmChonPhieuXuat;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChonPhieuXuat.Execute;
begin
	mKH := pKH;
    mKho := pKho;
    CbbDonVi.Enabled := not pFix;
    EdMADV.Enabled := not pFix;


	if ShowModal = mrOK then
		Result := TGuidField(QrPHIEUXUAT.FieldByName('KHOA')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

	CloseDataSets([QrPHIEUXUAT, QrDMKH, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
    SetDisplayFormat(QrPHIEUXUAT, sysCurFmt);
    SetDisplayFormat(QrPHIEUXUAT, ['NGAY'], DateTimeFmt);

    SetCustomGrid('CHON_PHIEUXUAT', GrBrowse);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormShow(Sender: TObject);

begin
	mSQL := QrPHIEUXUAT.SQL.Text;

    OpenDataSets([QrPHIEUXUAT, QrDMKH, QrDMKHO]);
//    SetShortDateFormat(QrPHIEUXUAT);
    with TbDummyEh do
    begin
        Open;
    	Append;
    end;
    // Smart focus
    if mKH = '' then
    	CbbDonVi.SetFocus
    else
        GrBrowse.SetFocus;


    mKH := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsKHACHHANG);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.GrBrowseDblClick(Sender: TObject);
begin
	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CmdRefreshExecute(Sender: TObject);
var
    s, sDonVi, sKhoHang: String;
    dTungay, dDenngay: TDateTime;
begin
    sDonVi := EdMADV.Text;
    sKhoHang := EdMaKho.Text;

    dTungay  := EdTungay.Date;
    dDenngay := EdDenngay.Date;

   	if (mTungay <> dTungay)  or (mDenngay <> dDenngay) or
       (mKH <> sDonVi)          or (mKHO <> sKhoHang)         then
	begin
   	    mKH := sDonVi;
        mKHO := sKhoHang;
        mTungay := dTungay;
        mDenngay := dDenngay;

        with QrPHIEUXUAT do
        begin
			Close;

            s := mSQL;
            if mKH <> '' then
            	s := s + ' and a.MADT=''' + mKH + '''';
			if mKHO <> '' then
            	s := s + ' and a.MAKHO=''' + mKHO + '''';
            SQL.Text := s;
            SQL.Add(' order by a.NGAY, a.SCT');
            Parameters[0].Value := mTungay;
            Parameters[1].Value := mDenngay;
            Open;
        end;

    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.CbMADTKeyPress(Sender: TObject; var Key: Char);
begin
     if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChonPhieuXuat.TbDummyEhAfterInsert(DataSet: TDataSet);
var
    dd, mm, yy: Word;
begin
    DecodeDate(Date, yy, mm, dd);
    with TbDummyEh do
    begin
        FieldByName('MaKho').AsString := mKho;
        FieldByName('Madt').AsString := mKH;
        FieldByName('TuNgay').AsDateTime := EncodeDate(yy, mm, dd) - sysLateDay;
        FieldByName('DenNgay').AsDateTime := Date;
    end;
end;

end.
