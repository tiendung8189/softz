﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit TheodoiGia;

interface

uses
  SysUtils, Classes, Controls, Forms,
  Wwdbgrid2, ExtCtrls, StdCtrls, Db, ADODB,
  ActnList, wwdblook,
  ComCtrls, wwdbdatetimepicker, Grids, Wwdbigrd, Wwdbgrid, DBGridEh, DBCtrlsEh,
  Vcl.Mask, DBLookupEh, DbLookupComboboxEh2, Variants;

type
  TFrmTheodoiGia = class(TForm)
    PaDondh: TPanel;
    GrBrowse: TwwDBGrid2;
    ActionList1: TActionList;
    CmdClose: TAction;
    QrDMKHO: TADOQuery;
    QrDM_KH_NCC: TADOQuery;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label65: TLabel;
    EdTungay: TwwDBDateTimePicker;
    Label66: TLabel;
    EdDenngay: TwwDBDateTimePicker;
    spCHUNGTU_CT_List_ThamKhao: TADOStoredProc;
    CHUNGTU_CT: TDataSource;
    DsDMKHO: TDataSource;
    DsDM_KH_NCC: TDataSource;
    CbbDonVi: TDbLookupComboboxEh2;
    EdMADV: TDBEditEh;
    EdMaKho: TDBEditEh;
    CbKhoHang: TDbLookupComboboxEh2;
    procedure FormShow(Sender: TObject);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure GrBrowseKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
    procedure CbbDonViChange(Sender: TObject);
    procedure CbbDonViExit(Sender: TObject);
  private
	mMADT, mKHO, mLCT, mMABH: String;
    mTungay, mDenngay: TDateTime;
    mLoai: Integer; //0: Khach hang; 1: NCC
  public
  	function Execute(pLCT, pMABH: String; pMADT, pKho: String;
    	pLoai: Integer): TGUID;
  end;

var
  FrmTheodoiGia: TFrmTheodoiGia;

implementation

uses
	isDb, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmTheodoiGia.Execute;
begin
    if pMABH = '' then
    begin
        Result := TGuidEx.EmptyGuid;
        Close;
        Exit;
    end;

	mMADT := pMADT;
    mKho := pKho;
    mLCT := pLCT;
    mLoai := pLoai;
    mMABH := pMABH;

    CbKhoHang.Value := mKho;
    EdMaKho.Text := mKho;

    CbbDonVi.Value := pMADT;
    EdMADV.Text := pMADT;

    if pLoai = 1 then
    begin
        CbbDonVi.ControlLabel.Caption := 'Nhà cung cấp';
    	Caption := 'Tham Khảo Giá Nhập - ' + pMABH;
        SetCustomGrid('THAMKHAO_GIA_NHAP' , GrBrowse);
    end
    else
    begin
        CbbDonVi.ControlLabel.Caption := 'Khách hàng';
    	Caption := 'Tham Khảo Giá Xuất - ' + pMABH;
        SetCustomGrid('THAMKHAO_GIA_XUAT' , GrBrowse);
    end;

    if ShowModal = mrOK then
		Result := TGuidField(spCHUNGTU_CT_List_ThamKhao.FieldByName('KHOACT')).AsGuid
    else
		Result := TGuidEx.EmptyGuid;

    CloseDataSets([spCHUNGTU_CT_List_ThamKhao, QrDM_KH_NCC, QrDMKHO]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormShow(Sender: TObject);
var
    dd, mm, yy: Word;
begin
   	OpenDataSets([QrDM_KH_NCC, QrDMKHO]);

    // Smart focus
    if mMADT = '' then
    	try
    		CbbDonVi.SetFocus;
        except
        end
    else
        GrBrowse.SetFocus;
    // Default date
    EdDenngay.Date := Date;
    DecodeDate(Date, yy, mm, dd);
    EdTungay.Date := EncodeDate(yy, mm, dd) - sysLateDay;

    mMADT := '~';
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.GrBrowseDblClick(Sender: TObject);
begin
    ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CbbDonViExit(Sender: TObject);
begin
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CmdRefreshExecute(Sender: TObject);
var
	sDonVi, sKhoHang: String;
    Tungay, Denngay: TDateTime;
begin
    sDonVi := EdMADV.Text;
   	if sDonVi = '' then
        sDonVi := CbbDonVi.Text;

    sKhoHang := EdMaKho.Text;
    if sKhoHang = '' then
        sKhoHang := CbKhoHang.Text;


    Tungay  := EdTungay.Date;
    Denngay := EdDenngay.Date;

   	if	(mTungay <> Tungay) or
    	(mDenngay <> Denngay) or
       (mMADT <> sDonVi) or
       (mKHO <> sKhoHang) then
	begin
   	    mMADT := sDonVi;
        mKHO := sKhoHang;
        mTungay := Tungay;
        mDenngay := Denngay;

        with spCHUNGTU_CT_List_ThamKhao do
        begin
			Close;
            Prepared := True;
            Parameters.ParamByName('@MABH').Value := mMABH;
            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@NGAYD').Value := mTungay;
            Parameters.ParamByName('@NGAYC').Value := mDenngay;
            Parameters.ParamByName('@MADT').Value := mMADT;
            Parameters.ParamByName('@MAKHO').Value := mKHO;
            Parameters.ParamByName('@SENDER').Value := '';
            Open;
        end;
        SetShortDateFormat(spCHUNGTU_CT_List_ThamKhao);
        SetDisplayFormat(spCHUNGTU_CT_List_ThamKhao, sysCurFmt);
        SetDisplayFormat(spCHUNGTU_CT_List_ThamKhao, ['TyLeCKMH', 'TyLeCKHD'], sysPerFmt);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.CbbDonViChange(Sender: TObject);
var
	s: String;
begin
    if (Sender as TDbLookupComboboxEh2).Text = '' then
    	s := ''
    else
        s := VarToStr((Sender as TDbLookupComboboxEh2).Value);

    case (Sender as TComponent).Tag of
    0:
        EdMADV.Text := s;
    1:
        EdMaKho.Text := s;
    end;

    CmdRefresh.Execute;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.FormKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #27 then
    	ModalResult := mrCancel
    else
		Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.GrBrowseKeyPress(Sender: TObject; var Key: Char);
begin
	if Key = #13 then
    	ModalResult := mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmTheodoiGia.QrDM_KH_NCCBeforeOpen(DataSet: TDataSet);
begin
	QrDM_KH_NCC.Parameters[0].Value := mLoai;
end;

end.
