object FrmDmNganhang: TFrmDmNganhang
  Left = 131
  Top = 110
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Ng'#226'n H'#224'ng/ Chi Nh'#225'nh'
  ClientHeight = 520
  ClientWidth = 908
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel2: TPanel
    Left = 0
    Top = 0
    Width = 908
    Height = 499
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Bevel1: TBevel
      Left = 0
      Top = 38
      Width = 908
      Height = 2
      Align = alTop
      Shape = bsSpacer
      ExplicitTop = 39
      ExplicitWidth = 792
    end
    object fcDBTreeView1: TfcDBTreeView
      Left = 458
      Top = 40
      Width = 450
      Height = 459
      Align = alClient
      BorderStyle = bsSingle
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      DataSourceFirst = DsNganhang
      DataSourceLast = DsChinhanh
      DisplayFields.Strings = (
        '"MANH" - "TENNH"'
        '"MA" - "TENNH_CN"')
      Options = [dtvoAutoExpandOnDSScroll, dtvoRowSelect, dtvoShowNodeHint, dtvoShowButtons, dtvoShowLines, dtvoShowRoot, dtvoShowHorzScrollBar, dtvoShowVertScrollBar]
      LevelIndent = 30
      HideUpDownButtons = True
    end
    object ToolBar1: TToolBar
      Left = 0
      Top = 0
      Width = 908
      Height = 38
      AutoSize = True
      ButtonHeight = 36
      ButtonWidth = 54
      Caption = 'ToolBar1'
      DisabledImages = DataMain.ImageNavi
      EdgeBorders = [ebBottom]
      Font.Charset = ANSI_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Images = DataMain.ImageNavi
      ParentFont = False
      ParentShowHint = False
      ShowCaptions = True
      ShowHint = True
      TabOrder = 1
      object ToolButton1: TToolButton
        Left = 0
        Top = 0
        Cursor = 1
        Action = CmdNew
        ImageIndex = 0
      end
      object ToolButton3: TToolButton
        Left = 54
        Top = 0
        Width = 8
        Caption = 'ToolButton3'
        ImageIndex = 2
        Style = tbsSeparator
      end
      object ToolButton4: TToolButton
        Left = 62
        Top = 0
        Cursor = 1
        Action = CmdSave
        ImageIndex = 1
      end
      object ToolButton5: TToolButton
        Left = 116
        Top = 0
        Cursor = 1
        Action = CmdCancel
        ImageIndex = 2
      end
      object ToolButton6: TToolButton
        Left = 170
        Top = 0
        Width = 8
        Caption = 'ToolButton6'
        ImageIndex = 4
        Style = tbsSeparator
      end
      object ToolButton7: TToolButton
        Left = 178
        Top = 0
        Cursor = 1
        Action = CmdDel
        ImageIndex = 3
      end
      object ToolButton9: TToolButton
        Left = 232
        Top = 0
        Width = 8
        Caption = 'ToolButton9'
        ImageIndex = 7
        Style = tbsSeparator
      end
      object ToolButton2: TToolButton
        Left = 240
        Top = 0
        Cursor = 1
        Action = CmdPrint
        ImageIndex = 4
      end
      object ToolButton8: TToolButton
        Left = 294
        Top = 0
        Width = 8
        Caption = 'ToolButton8'
        ImageIndex = 6
        Style = tbsSeparator
      end
      object ToolButton11: TToolButton
        Left = 302
        Top = 0
        Cursor = 1
        Action = CmdClose
        ImageIndex = 5
      end
    end
    object RzSizePanel1: TRzSizePanel
      Left = 0
      Top = 40
      Width = 458
      Height = 459
      HotSpotHighlight = 11855600
      SizeBarWidth = 7
      TabOrder = 2
      object PgMain: TPageControl
        Left = 0
        Top = 0
        Width = 450
        Height = 459
        Cursor = 1
        ActivePage = TsQuocgia
        Align = alClient
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrack = True
        ParentFont = False
        TabOrder = 0
        OnChange = PgMainChange
        OnChanging = PgMainChanging
        ExplicitWidth = 458
        object TsQuocgia: TTabSheet
          Caption = ' Ng'#226'n h'#224'ng'
          ExplicitWidth = 450
          object GrList1: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 442
            Height = 431
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            PictureMasks.Strings = (
              'MANH'#9'*!'#9'T'#9'T')
            Selected.Strings = (
              'MANH'#9'10'#9'M'#227#9'F'
              'TENNH'#9'45'#9'T'#234'n'#9'F')
            MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly]
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = DsNganhang
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowInsert]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgShowCellHint]
            ParentFont = False
            PopupMenu = PopDetail
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 1
            TitleButtons = True
            UseTFields = False
            OnCalcCellColors = GrList1CalcCellColors
            TitleImageList = DataMain.ImageSort
            PadColumnStyle = pcsPadHeader
            ExplicitWidth = 450
          end
        end
        object TsTinh: TTabSheet
          Caption = ' Chi nh'#225'nh'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 319
          ExplicitHeight = 0
          object GrList2: TwwDBGrid2
            Left = 0
            Top = 0
            Width = 450
            Height = 431
            DittoAttributes.ShortCutDittoField = 16397
            DittoAttributes.Options = [wwdoSkipReadOnlyFields]
            PictureMasks.Strings = (
              'MA'#9'*!'#9'T'#9'T')
            Selected.Strings = (
              'MA'#9'10'#9'M'#227#9'F'
              'TENNH_CN'#9'45'#9'T'#234'n'#9'F')
            MemoAttributes = [mSizeable, mWordWrap, mGridShow, mViewOnly]
            IniAttributes.Delimiter = ';;'
            TitleColor = 13360356
            FixedCols = 0
            ShowHorzScrollBar = True
            Align = alClient
            DataSource = DsChinhanh
            Font.Charset = ANSI_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            KeyOptions = [dgEnterToTab, dgAllowInsert]
            Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgAlwaysShowSelection, dgConfirmDelete, dgWordWrap, dgShowCellHint]
            ParentFont = False
            PopupMenu = PopDetail
            TabOrder = 0
            TitleAlignment = taCenter
            TitleFont.Charset = ANSI_CHARSET
            TitleFont.Color = 8404992
            TitleFont.Height = -11
            TitleFont.Name = 'Tahoma'
            TitleFont.Style = [fsBold]
            TitleLines = 1
            TitleButtons = True
            UseTFields = False
            OnCalcCellColors = GrList2CalcCellColors
            TitleImageList = DataMain.ImageSort
            PadColumnStyle = pcsPadHeader
            ExplicitWidth = 319
          end
        end
      end
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 499
    Width = 908
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <
      item
        Width = 650
      end
      item
        Width = 50
      end>
    SimplePanel = True
    UseSystemFont = False
  end
  object QrNganhang: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrNganhangBeforeInsert
    BeforeEdit = QrNganhangBeforeEdit
    BeforePost = QrNganhangBeforePost
    BeforeDelete = QrNganhangBeforeDelete
    OnDeleteError = OnDBError
    OnEditError = OnDBError
    OnPostError = OnDBError
    Parameters = <>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_NGANHANG'
      'order by'#9'MANH')
    Left = 44
    Top = 118
    object QrNganhangCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrNganhangUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrNganhangCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrNganhangUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrNganhangMANH: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MANH'
      Size = 10
    end
    object QrNganhangTENNH: TWideStringField
      DisplayLabel = 'T'#234'n'
      FieldName = 'TENNH'
      Size = 200
    end
  end
  object DsNganhang: TDataSource
    DataSet = QrNganhang
    Left = 44
    Top = 146
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 176
    Top = 312
  end
  object QrChinhanh: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    BeforeInsert = QrNganhangBeforeInsert
    AfterInsert = QrChinhanhAfterInsert
    BeforeEdit = QrNganhangBeforeEdit
    BeforePost = QrNganhangBeforePost
    BeforeDelete = QrNganhangBeforeDelete
    OnDeleteError = OnDBError
    OnEditError = OnDBError
    OnPostError = OnDBError
    DataSource = DsNganhang
    Parameters = <
      item
        Name = 'MANH'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = 'ACB'
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_NGANHANG_CHINHANH'
      'where '#9'MANH = :MANH'
      'order by'#9'MANH_CN, MANH')
    Left = 72
    Top = 118
    object QrChinhanhCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
    end
    object QrChinhanhUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
    end
    object QrChinhanhCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
    end
    object QrChinhanhUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
    end
    object QrChinhanhMANH_CN: TWideStringField
      FieldName = 'MANH_CN'
      Size = 26
    end
    object QrChinhanhMANH: TWideStringField
      FieldName = 'MANH'
      Size = 10
    end
    object QrChinhanhTENNH_CN: TWideStringField
      DisplayLabel = 'T'#234'n'
      FieldName = 'TENNH_CN'
      Size = 200
    end
    object QrChinhanhMA: TWideStringField
      DisplayLabel = 'M'#227
      FieldName = 'MA'
      Size = 15
    end
  end
  object DsChinhanh: TDataSource
    DataSet = QrChinhanh
    Left = 72
    Top = 146
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 176
    Top = 284
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSearch: TAction
      Caption = 'T'#236'm m'#7851'u tin        '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdReload: TAction
      Caption = 'N'#7841'p l'#7841'i t'#7915' c'#417' s'#7903' d'#7919' li'#7879'u'
      ShortCut = 16466
      OnExecute = CmdReloadExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
  end
  object PopDetail: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 210
    Top = 284
    object Lctheomthng1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object FilterQuocgia: TwwFilterDialog2
    DataSource = DsNganhang
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 44
    Top = 174
  end
  object FilterTinh: TwwFilterDialog2
    DataSource = DsChinhanh
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'Filter'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MANS'
      'TENNS'
      'DCHI'
      'EMAIL'
      'FAX'
      'DTHOAI')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 72
    Top = 174
  end
end
