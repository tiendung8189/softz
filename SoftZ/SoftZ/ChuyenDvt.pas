﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ChuyenDvt;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg, Wwstr, ShellAPI,
  frameNgay, frameNavi, isDb, isPanel, frameScanCode, wwDialog, Mask, Grids,
  Wwdbgrid, ToolWin, wwdbedit, Wwdotdot, Wwdbcomb, DBGridEh, DBLookupEh,
  DbLookupComboboxEh2, DBCtrlsEh;

type
  TFrmChuyenDvt = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    SepChecked: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdSCT: TwwDBEdit;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    CbNGAY: TwwDBDateTimePicker;
    CmdSwitch: TAction;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTMAVT: TWideStringField;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXMADT: TWideStringField;
    QrNXMAKHO: TWideStringField;
    QrNXTU_MAKHO: TWideStringField;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    CmdTotal: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXLK_TENKHO: TWideStringField;
    QrNXXOA: TWideStringField;
    QrNXDELETE_DATE: TDateTimeField;
    Label5: TLabel;
    Label8: TLabel;
    EdNGUOIGIAO: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Phiuvnchuynnib1: TMenuItem;
    N2: TMenuItem;
    Phiucginhp1: TMenuItem;
    QrNXIMG: TIntegerField;
    QrNXLK_TU_TENKHO: TWideStringField;
    CmdClearFilter: TAction;
    Hinttc1: TMenuItem;
    QrCTSTT: TIntegerField;
    PopIn: TAdvPopupMenu;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    CmdFilterCom: TAction;
    Bevel1: TBevel;
    N3: TMenuItem;
    Lctheomthng1: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    vlTotal1: TisTotal;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    QrCTB1: TBooleanField;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    PopDetail: TAdvPopupMenu;
    QrCTLK_BO: TBooleanField;
    CmdEmptyDetail: TAction;
    QrCTQD1: TIntegerField;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrNXLYDO: TWideStringField;
    spCHUNGTU_Select_Full: TADOStoredProc;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton6: TToolButton;
    QrCTLOC: TWideStringField;
    N6: TMenuItem;
    ItemObsolete: TMenuItem;
    CmdCheckton: TAction;
    Kimtraslngtnkho1: TMenuItem;
    N8: TMenuItem;
    heogibn1: TMenuItem;
    CmdExportDataGrid: TAction;
    N9: TMenuItem;
    XutdliutliraExcel1: TMenuItem;
    QrNXNguoiGiao: TWideStringField;
    QrNXNguoiNhan: TWideStringField;
    QrNXThanhTien: TFloatField;
    QrNXThanhTienSi: TFloatField;
    QrNXThanhTienLe: TFloatField;
    QrNXGhiChu: TWideMemoField;
    QrCTCALC_DonGiaThamKhaoHop: TFloatField;
    QrCTDonGiaThamKhao: TFloatField;
    QrCTDonGiaHop: TFloatField;
    QrCTSoLuongHop: TFloatField;
    QrCTSoLuongLe: TFloatField;
    QrCTThanhTien: TFloatField;
    QrCTDonGiaSi: TFloatField;
    QrCTThanhTienSi: TFloatField;
    QrCTDonGiaLe: TFloatField;
    QrCTThanhTienLe: TFloatField;
    QrCTHanSuDung: TDateTimeField;
    QrCTDonGia: TFloatField;
    QrCTSoLuong: TFloatField;
    QrCTLK_Dvt: TWideStringField;
    QrCTLK_DvtHop: TWideStringField;
    QrCTLK_Tenvt: TWideStringField;
    QrCTGhiChu: TWideStringField;
    EdMaKhoXuat: TDBEditEh;
    CbKhoXuat: TDbLookupComboboxEh2;
    QrNXThanhToan: TFloatField;
    QrNXSoLuong: TFloatField;
    DBMemoEh1: TDBMemoEh;
    QrCTThanhToan: TFloatField;
    ToolButton11: TToolButton;
    QrCTLK_MaNoiBo: TWideStringField;
    N11: TMenuItem;
    Khngngi1: TMenuItem;
    PaXuat: TisPanel;
    GrXuat: TwwDBGrid2;
    QrTu_CT: TADOQuery;
    IntegerField1: TIntegerField;
    WideStringField1: TWideStringField;
    IntegerField2: TIntegerField;
    IntegerField3: TIntegerField;
    BooleanField1: TBooleanField;
    BooleanField2: TBooleanField;
    GuidField1: TGuidField;
    GuidField2: TGuidField;
    WideStringField2: TWideStringField;
    FloatField1: TFloatField;
    FloatField2: TFloatField;
    FloatField3: TFloatField;
    FloatField4: TFloatField;
    FloatField5: TFloatField;
    FloatField6: TFloatField;
    FloatField7: TFloatField;
    FloatField8: TFloatField;
    FloatField9: TFloatField;
    FloatField10: TFloatField;
    DateTimeField1: TDateTimeField;
    FloatField11: TFloatField;
    FloatField12: TFloatField;
    WideStringField3: TWideStringField;
    WideStringField4: TWideStringField;
    WideStringField5: TWideStringField;
    WideStringField6: TWideStringField;
    WideStringField7: TWideStringField;
    FloatField13: TFloatField;
    DsTu_CT: TDataSource;
    QrNXTu_SoLuong: TFloatField;
    QrNXTu_ThanhTien: TFloatField;
    QrNXTu_ThanhTienSi: TFloatField;
    QrNXTu_ThanhTienLe: TFloatField;
    QrNXTu_ThanhToan: TFloatField;
    CmdPrint: TAction;
    vlTotal_Tu: TisTotal;
    QrCTNhapXuat: TWideStringField;
    QrTu_CTNhapXuat: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTAfterCancel(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure QrCTCalcFields(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure CmdTotalExecute(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure QrCTMAVTChange(Sender: TField);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbKHOXUATNotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure CmdFilterComExecute(Sender: TObject);
    procedure QrCTSOLUONGChange(Sender: TField);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterEdit(DataSet: TDataSet);
    procedure BtnInClick(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure CmdEmptyDetailExecute(Sender: TObject);
    procedure QrCTDONGIAChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure QrCTSOLUONGValidate(Sender: TField);
    procedure CmdChecktonExecute(Sender: TObject);
    procedure CmdExportDataGridExecute(Sender: TObject);
    procedure QrCTThanhTienChange(Sender: TField);
    procedure QrCTThanhToanChange(Sender: TField);
    procedure CbKhoXuatDropDown(Sender: TObject);
    procedure CbKhoXuatCloseUp(Sender: TObject; Accept: Boolean);
    procedure WideStringField1Change(Sender: TField);
    procedure FloatField3Change(Sender: TField);
    procedure FloatField12Change(Sender: TField);
    procedure FloatField6Change(Sender: TField);
    procedure FloatField13Change(Sender: TField);
    procedure GrXuatUpdateFooter(Sender: TObject);
    procedure QrTu_CTAfterCancel(DataSet: TDataSet);
    procedure QrTu_CTAfterDelete(DataSet: TDataSet);
    procedure QrTu_CTAfterEdit(DataSet: TDataSet);
    procedure QrTu_CTBeforeDelete(DataSet: TDataSet);
    procedure QrTu_CTBeforePost(DataSet: TDataSet);
  private
  	mLCT, mPrefix: String;
	mCanEdit, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmChuyenDvt: TFrmChuyenDvt;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, ChonDsma, isLib, Sapthutu,
    isCommon, GuidEx, isFile, ImportExcel, ChonPhieunhap, InlabelChungtu,
  CheckTonkho, OfficeData;

{$R *.DFM}

const
	FORM_CODE = 'PHIEU_CHUYENDOI_DVT';

    (*
    ** Form Events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.Execute;
begin
   	mLCT := 'CDVT';

	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    DsTu_CT.AutoEdit := mCanEdit;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frDate.Init;
    frNavi.DataSet := QrNX;

    frDate.CbbLoc.Enabled := True;

    // Initial
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
  	mTrigger := False;
    mObsolete := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FormShow(Sender: TObject);
begin
	// Open Database
    with DataMain do
    begin
		OpenDataSets([QrDMVT, QrDMKHO]);
        SetDisplayFormat(QrDMVT, sysCurFmt);
        SetDisplayFormat(QrNX, ['SoLuong'], ctQtyFmt);
    end;

    with QrNX do
    begin
	    SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['NGAY'], DateTimeFmt);
    end;

    with QrTu_CT do
    begin
	    SetDisplayFormat(QrTu_CT, sysCurFmt);
    	SetDisplayFormat(QrTu_CT, ['SoLuong'], ctQtyFmt);
        SetDisplayFormat(QrTu_CT, ['DonGia', 'DonGiaSi', 'DonGiaLe'], ctPriceFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
    	SetDisplayFormat(QrCT, ['SoLuong'], ctQtyFmt);
        SetDisplayFormat(QrCT, ['DonGia', 'DonGiaSi', 'DonGiaLe'], ctPriceFmt);
    end;

    // Customize Grid
	SetCustomGrid([FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_TU_CT'], [GrBrowse, GrDetail, GrXuat]);
    SetDictionary([QrNX, QrCT, QrTu_CT], [FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_TU_CT'], [Filter, Nil, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FloatField12Change(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrTu_CT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SoLuong' then
            begin
                sl  := FieldByName('SoLuong').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;
            end
            else if (Sender.FullName = 'SoLuongHop') or (Sender.FullName = 'SoLuongLe') then
            begin
                sl2 := FieldByName('SoLuongHop').AsFloat;
                sl2le := FieldByName('SoLuongLe').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SoLuong').AsFloat * FieldByName('DonGia').AsFloat, ctCurRound);

        FieldByName('ThanhTienSi').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaSi').AsFloat, ctCurRound);
        FieldByName('ThanhTienLe').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaLe').AsFloat, ctCurRound);
        FieldByName('ThanhTien').AsFloat := d;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FloatField13Change(Sender: TField);
begin
    vlTotal_Tu.Update;
    GrXuat.InvalidateCurrentRow;
    GrXuatUpdateFooter(GrXuat);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FloatField3Change(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrTu_CT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DonGia' then
                FieldByName('DonGiaHop').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DonGiaHop' then
                FieldByName('DonGia').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    FloatField12Change(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FloatField6Change(Sender: TField);
begin
    with QrTu_CT do
    begin
         FieldByName('ThanhToan').AsFloat := FieldByName('ThanhTien').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
    	CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;
    (*
    ** End: Form Events
    *)

    (*
    **  Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
	   (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            // Detail Filter
            if fStr <> '' then
	           	case fType of
                0:	// Nganh
                	SQL.Add('and KHOA in (select a.KHOA from DIEUKHO_CT a, DM_VT_FULL b, DM_NHOM c where a.KHOA = DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM = c.MANHOM and c.MANGANH in (' + fStr + '))');
				1:	// Nhom
                	SQL.Add('and KHOA in (select a.KHOA from DIEUKHO_CT a, DM_VT_FULL b where a.KHOA = DIEUKHO.KHOA and a.MAVT = b.MAVT and b.MANHOM in (' + fStr + '))');
				else
                	SQL.Add('and KHOA in (select KHOA from DIEUKHO_CT where KHOA = DIEUKHO.KHOA and MAVT in (' + fStr + '))');
				end;
			SQL.Add('order by NGAY desc, SCT desc');

    	    Open;
            if s <> '' then
	        	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
	with DataMain do
    begin
		QrDMVT.Requery;
        QrDMKHO.Requery;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
            QrTu_CT.CancelBatch;
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
//    frScanCode1.chkCheckExists.Checked := True;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdSaveExecute(Sender: TObject);
begin
	QrCT.CheckBrowseMode;
    QrTu_CT.CheckBrowseMode;
	QrNX.Post;
    exSaveDetails(QrCT);
    exSaveDetails(QrTu_CT);

//    SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdCancelExecute(Sender: TObject);
begin
    QrCT.CancelBatch;
    QrTu_CT.CancelBatch;
    QrNX.Cancel;

    if QrNX.IsEmpty then
        ActiveSheet(PgMain, 0)
    else
        ActiveSheet(PgMain);

//    if QrNX.FieldByName('LOCKED_BY').AsInteger = sysLogonUID then
//        SetLockedBy(QrNX, False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdCheckedExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdChecktonExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
	// Tham khao gia nhap
	Application.CreateForm(TFrmCheckTonkho, FrmCheckTonkho);
    with QrCT do
	    if FrmCheckTonkho.Execute(
            FieldByName('MAVT').AsString,
        	FieldByName('LK_Tenvt').AsString,
            QrNX.FieldByName('MAKHO').AsString,
        	QrNX.FieldByName('LK_TENKHO').AsString,
            QrNX.FieldByName('NGAY').AsDateTime,
            TGuidField(QrNX.FieldByName('KHOA')).AsGuid) then
        begin

        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdDelExecute(Sender: TObject);
begin
    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(QrNX) then
//        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if YesNo(RS_CONFIRM_XOAPHIEU, 1) then
       	MarkDataSet(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_CODEIMPORTED = 'Phiếu nhập có mặt hàng sai mã. Tiếp tục?';

resourcestring
	FORMAT_CONFIRM = 'File dữ liệu phải có dạng "MA,SOLUONG".';
procedure TFrmChuyenDvt.CmdSearchExecute(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdFilterComExecute(Sender: TObject);
var
	s: String;
begin
	if fStr = '' then
    begin
		// Show Selection Form
		s := fStr;
	    if not FrmChonDsma.Get(fType, s) then
    		Exit;
	    fStr := s;
    end
    else
		fStr := '';

    // Refresh
	CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdEmptyDetailExecute(Sender: TObject);
begin
    exEmptyDetails(QrCT, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdExportDataGridExecute(Sender: TObject);
var
    n: Integer;
begin
    n := (Sender as TComponent).Tag;
    case n of
        0: DataOffice.ExportDataGrid(GrBrowse);
        1: DataOffice.ExportDataGrid(GrDetail);
        2: DataOffice.ExportDataGrid(GrXuat);
    else
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdReReadExecute(Sender: TObject);
begin
	fTuNgay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bDeleted, bEmptyCT: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);
    BtnIn.Enabled := (not bEmpty) and (not bDeleted);

    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;

    CmdFilter.Enabled := n = 0;

    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;

    CmdFilterCom.Checked := fStr <> '';
    CmdSwitch.Enabled := n = 1;

    // Detail
    with QrCT do
    begin
    	if Active then
        	bEmptyCT := IsEmpty
        else
            bEmptyCT := False;
    end;
    CmdCheckton.Enabled := not bEmptyCT;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdTotalExecute(Sender: TObject);
begin
    vlTotal_Tu.Sum;
	vlTotal1.Sum
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXAfterInsert(DataSet: TDataSet);
var
    d: TDateTime;
begin
    d := Now;
	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('LCT').AsString           := mLCT;
		FieldByName('NGAY').AsDateTime        := d;
        FieldByName('LOC').AsString           := sysLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrTu_CTAfterCancel(DataSet: TDataSet);
begin
    vlTotal_Tu.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrTu_CTAfterDelete(DataSet: TDataSet);
begin
    vlTotal_Tu.Update(True);

    GrXuat.InvalidateCurrentRow;
    GrXuatUpdateFooter(GrXuat);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrTu_CTAfterEdit(DataSet: TDataSet);
begin
    vlTotal_Tu.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrTu_CTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    vlTotal_Tu.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrTu_CTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrTu_CT do
    begin
		if BlankConfirm(QrTu_CT, ['MAVT']) then
    		Abort;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
            FieldByName('NhapXuat').AsString := 'X'
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.WideStringField1Change(Sender: TField);
var
	gia, giaSI, giaLE : Double;
begin
	exDotMavt(3, Sender);

    // Update referenced fields
    with QrTu_CT do
    begin
        gia := 0; giaSI := 0; giaLE := 0;
        with DataMain.spCHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@MABH').Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DonGia').AsFloat;
                giaSI := FieldByName('DonGiaSi').AsFloat;
                giaLE := FieldByName('DonGiaLe').AsFloat;
            end;

            QrTu_CT.FieldByName('QD1').AsInteger := FieldByName('QD1').AsInteger;
        end;

        FieldByName('DonGiaSi').AsFloat := giaSI;
        FieldByName('DonGiaLe').AsFloat := giaLE;
        FieldByName('DonGiaThamKhao').AsFloat := gia;
        FieldByName('DonGia').AsFloat := gia;
    end;

	GrXuat.InvalidateCurrentRow;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
//        Parameters[4].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_INVALID_WAREHOUSE = 'Chuyển kho không hợp lệ.';
    RS_INVALID_TRIGIAVON = 'Trị giá vốn Xuất hàng phải bằng Trị giá vốn Nhập hàng.';

procedure TFrmChuyenDvt.QrNXBeforePost(DataSet: TDataSet);
begin
	with QrNX do
    begin
		if BlankConfirm(QrNX, ['NGAY', 'MAKHO']) then
            Abort;

        if FieldByName('Tu_ThanhTien').AsFloat <> FieldByName('ThanhTien').AsFloat then
        begin
            ErrMsg(RS_INVALID_TRIGIAVON);
            Abort;
        end;

        FieldByName('TU_MAKHO').AsString := FieldByName('MAKHO').AsString;
		exValidClosing(FieldByName('NGAY').AsDateTime);
	end;

    DataMain.AllocSCT(mLCT, QrNX);
 	CmdTotal.Execute;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);

    if mTrigger then
        Exit;

    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);

//    exReSyncRecord(QrNX);
//    if not CanEditLockedBy(DataSet) then
//        Abort;
//    SetLockedBy(DataSet, True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
(*
    ** End: Master DB
    *)

	(*
    ** Detail DB
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTBeforeOpen(DataSet: TDataSet);
begin
	(DataSet as TADOQuery).Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTBeforePost(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

	with QrCT do
    begin
		if BlankConfirm(QrCT, ['MAVT']) then
    		Abort;

        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrNX.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
            FieldByName('NhapXuat').AsString := 'N'
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;

    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if not (DataSet as TADOQuery).IsEmpty then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTAfterCancel(DataSet: TDataSet);
begin
	vlTotal1.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTAfterDelete(DataSet: TDataSet);
begin
    vlTotal1.Update(True);

    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTAfterEdit(DataSet: TDataSet);
begin
    vlTotal1.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTCalcFields(DataSet: TDataSet);
begin
	with (DataSet as TADOQuery) do
    begin
        FieldByName('CALC_DonGiaThamKhaoHop').AsFloat :=
            FieldByName('DonGiaThamKhao').AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger);

    	if State in [dsInsert] then
        else
	    	FieldByName('RSTT').AsInteger := Abs(RecNo);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTDONGIAChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTrigger then
        Exit;

    with QrCT do
    begin
        if Sender <> Nil then
        begin
            bTrigger := mTrigger;
            mTrigger := True;
            if Sender.FieldName = 'DonGia' then
                FieldByName('DonGiaHop').AsFloat := exVNDRound(
                    Sender.AsFloat * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound)
            else if Sender.FieldName = 'DonGiaHop' then
                FieldByName('DonGia').AsFloat := exVNDRound(
                    Sender.AsFloat / Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctPriceRound);
            mTrigger := bTrigger;
        end;
    end;
    QrCTSOLUONGChange(Sender);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTThanhTienChange(Sender: TField);
begin
    with QrCT do
    begin
         FieldByName('ThanhToan').AsFloat := FieldByName('ThanhTien').AsFloat
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTThanhToanChange(Sender: TField);
begin
    vlTotal1.Update;
    GrDetail.InvalidateCurrentRow;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTSOLUONGChange(Sender: TField);
var
    bTrigger: Boolean;
    d, x, sl, sl2, sl2le: Double;
begin
    if mTrigger then
    	Exit;

	with QrCT do
    begin
        bTrigger := mTrigger;
        mTrigger := True;
        if Sender <> Nil then
            if Sender.FullName = 'SoLuong' then
            begin
                sl  := FieldByName('SoLuong').AsFloat;
                x := exVNDRound(sl /
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger), ctQtyRound);

                sl2 := Trunc(x);
                sl2le := sl - (sl2 * Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger));

                FieldByName('SoLuongHop').AsFloat := sl2;
                FieldByName('SoLuongLe').AsFloat := sl2le;
            end
            else if (Sender.FullName = 'SoLuongHop') or (Sender.FullName = 'SoLuongLe') then
            begin
                sl2 := FieldByName('SoLuongHop').AsFloat;
                sl2le := FieldByName('SoLuongLe').AsFloat;
                sl := exVNDRound((sl2 *
                    Iif(FieldByName('QD1').AsInteger = 0, 1,FieldByName('QD1').AsInteger)) + sl2le, ctQtyRound);

                FieldByName('SoLuong').AsFloat := sl;
            end;
        mTrigger := bTrigger;

        d := exVNDRound(FieldByName('SoLuong').AsFloat * FieldByName('DonGia').AsFloat, ctCurRound);

        FieldByName('ThanhTienSi').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaSi').AsFloat, ctCurRound);
        FieldByName('ThanhTienLe').AsFloat := exVNDRound(FieldByName('SoLuong').AsFloat
                                                * FieldByName('DonGiaLe').AsFloat, ctCurRound);
        FieldByName('ThanhTien').AsFloat := d;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTSOLUONGValidate(Sender: TField);
begin
    if not exIsCheckSL(Sender.AsFloat) then
        Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.QrCTMAVTChange(Sender: TField);
var
	gia, giaSI, giaLE : Double;
begin
	exDotMavt(3, Sender);

    // Update referenced fields
    with QrCT do
    begin
        gia := 0; giaSI := 0; giaLE := 0;
        with DataMain.spCHUNGTU_GET_REF do
        begin
            if Active then
                Close;

            Parameters.ParamByName('@LCT').Value := mLCT;
            Parameters.ParamByName('@MABH').Value := Sender.AsString;

            Open;
            if RecordCount <> 0 then
            begin
                gia := FieldByName('DonGia').AsFloat;
                giaSI := FieldByName('DonGiaSi').AsFloat;
                giaLE := FieldByName('DonGiaLe').AsFloat;
            end;

            QrCT.FieldByName('QD1').AsInteger := FieldByName('QD1').AsInteger;
        end;

        FieldByName('DonGiaSi').AsFloat := giaSI;
        FieldByName('DonGiaLe').AsFloat := giaLE;
        FieldByName('DonGiaThamKhao').AsFloat := gia;
        FieldByName('DonGia').AsFloat := gia;
    end;

	GrDetail.InvalidateCurrentRow;
end;

(*
    ** End: Detail DB
    *)

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CbKhoXuatCloseUp(Sender: TObject; Accept: Boolean);
begin
     (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CbKhoXuatDropDown(Sender: TObject);
begin
     (Sender as TDbLookupComboboxEh2).DropDownBox.ListSource.DataSet.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CbKHOXUATNotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.BtnInClick(Sender: TObject);
begin
	(Sender as TToolButton).CheckMenuDropdown;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.GrDetailUpdateFooter(Sender: TObject);
begin
	with GrDetail, QrNX do
    begin
		ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('SoLuong').AsFloat);
		ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTien').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhToan').AsFloat);
        ColumnByName('ThanhTienSi').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienSi').AsFloat);
        ColumnByName('ThanhTienLe').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('ThanhTienLe').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.GrXuatUpdateFooter(Sender: TObject);
begin
	with GrXuat, QrNX do
    begin
		ColumnByName('SoLuong').FooterValue :=
        	FormatFloat(ctQtyFmt, FieldByName('Tu_SoLuong').AsFloat);
		ColumnByName('ThanhTien').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('Tu_ThanhTien').AsFloat);
        ColumnByName('ThanhToan').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('Tu_ThanhToan').AsFloat);
        ColumnByName('ThanhTienSi').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('Tu_ThanhTienSi').AsFloat);
        ColumnByName('ThanhTienLe').FooterValue :=
        	FormatFloat(ctCurFmt, FieldByName('Tu_ThanhTienLe').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
        Screen.Cursor := crSQLWait;
        with QrTu_CT do
        begin
            Close;
            Open;
        end;
        with QrCT do
        begin
            Close;
            Open;
        end;
        Screen.Cursor := crDefault;

	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChuyenDvt.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;
    (*
    **  End: Others
    *)


end.
