object FrmDmTK: TFrmDmTK
  Left = 485
  Top = 481
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'Danh M'#7909'c T'#224'i Kho'#7843'n'
  ClientHeight = 492
  ClientWidth = 695
  Color = 16119285
  Font.Charset = ANSI_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCloseQuery = FormCloseQuery
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object ToolBar1: TToolBar
    Left = 0
    Top = 0
    Width = 695
    Height = 36
    AutoSize = True
    ButtonHeight = 36
    ButtonWidth = 54
    Caption = 'ToolBar1'
    DisabledImages = DataMain.ImageNavi
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Images = DataMain.ImageNavi
    ParentFont = False
    ShowCaptions = True
    TabOrder = 0
    object ToolButton1: TToolButton
      Left = 0
      Top = 0
      Cursor = 1
      Action = CmdNew
      ImageIndex = 0
    end
    object ToolButton3: TToolButton
      Left = 54
      Top = 0
      Width = 8
      Caption = 'ToolButton3'
      ImageIndex = 2
      Style = tbsSeparator
    end
    object ToolButton4: TToolButton
      Left = 62
      Top = 0
      Cursor = 1
      Action = CmdSave
      ImageIndex = 1
    end
    object ToolButton5: TToolButton
      Left = 116
      Top = 0
      Cursor = 1
      Action = CmdCancel
      ImageIndex = 2
    end
    object ToolButton7: TToolButton
      Left = 170
      Top = 0
      Width = 8
      Caption = 'ToolButton7'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton2: TToolButton
      Left = 178
      Top = 0
      Cursor = 1
      Action = CmdDel
      ImageIndex = 3
    end
    object ToolButton6: TToolButton
      Left = 232
      Top = 0
      Width = 8
      Caption = 'ToolButton6'
      ImageIndex = 4
      Style = tbsSeparator
    end
    object ToolButton9: TToolButton
      Left = 240
      Top = 0
      Cursor = 1
      Action = CmdPrint
      ImageIndex = 4
    end
    object ToolButton10: TToolButton
      Left = 294
      Top = 0
      Width = 8
      Caption = 'ToolButton10'
      ImageIndex = 6
      Style = tbsSeparator
    end
    object ToolButton11: TToolButton
      Left = 302
      Top = 0
      Cursor = 1
      Action = CmdClose
      ImageIndex = 5
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 471
    Width = 695
    Height = 21
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object RzSizePanel1: TRzSizePanel
    Left = 271
    Top = 36
    Width = 424
    Height = 435
    Align = alRight
    BorderOuter = fsLowered
    HotSpotHighlight = 11855600
    HotSpotIgnoreMargins = True
    HotSpotVisible = True
    LockBar = True
    SizeBarWidth = 7
    TabOrder = 2
    object PD1: TisPanel
      Left = 10
      Top = 2
      Width = 412
      Height = 327
      Align = alTop
      BevelInner = bvRaised
      BevelOuter = bvNone
      Color = 16119285
      ParentBackground = False
      TabOrder = 0
      HeaderCaption = ' :: Th'#244'ng tin'
      HeaderColor = 16119285
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object Label5: TLabel
        Left = 29
        Top = 28
        Width = 68
        Height = 14
        Alignment = taRightJustify
        Caption = 'S'#7889' t'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label6: TLabel
        Left = 22
        Top = 52
        Width = 75
        Height = 14
        Alignment = taRightJustify
        Caption = 'Ch'#7911' t'#224'i kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label3: TLabel
        Left = 42
        Top = 124
        Width = 55
        Height = 14
        Alignment = taRightJustify
        Caption = #272#7883'a ch'#7881' NH'
        FocusControl = DBEdit2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label4: TLabel
        Left = 22
        Top = 148
        Width = 75
        Height = 14
        Alignment = taRightJustify
        Caption = #272'i'#7879'n tho'#7841'i NH'
        FocusControl = DBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label1: TLabel
        Left = 54
        Top = 76
        Width = 43
        Height = 14
        Alignment = taRightJustify
        Caption = #272#7841'i di'#7879'n'
        FocusControl = DBEdit1
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object TntLabel3: TLabel
        Left = 0
        Top = 244
        Width = 97
        Height = 16
        Alignment = taRightJustify
        Caption = 'Lo'#7841'i h'#236'nh t.kho'#7843'n'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label8: TLabel
        Left = 43
        Top = 196
        Width = 54
        Height = 14
        Alignment = taRightJustify
        Caption = #272#7883'a ch'#7881' CN'
        FocusControl = wwDBEdit2
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label9: TLabel
        Left = 23
        Top = 220
        Width = 74
        Height = 14
        Alignment = taRightJustify
        Caption = #272'i'#7879'n tho'#7841'i CN'
        FocusControl = wwDBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -12
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label12: TLabel
        Left = 19
        Top = 267
        Width = 78
        Height = 16
        Alignment = taRightJustify
        Caption = 'Ng'#224'y s'#7917' d'#7909'ng'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object EdMA: TwwDBEdit
        Tag = 1
        Left = 106
        Top = 24
        Width = 165
        Height = 22
        AutoSize = False
        CharCase = ecUpperCase
        Color = 15794175
        Ctl3D = False
        DataField = 'MATK'
        DataSource = DsDanhmuc
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object EdTEN: TwwDBEdit
        Tag = 1
        Left = 106
        Top = 48
        Width = 293
        Height = 22
        AutoSize = False
        Color = 15794175
        Ctl3D = False
        DataField = 'TENTK'
        DataSource = DsDanhmuc
        Font.Charset = ANSI_CHARSET
        Font.Color = 8404992
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 2
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit2: TwwDBEdit
        Left = 106
        Top = 120
        Width = 293
        Height = 22
        Ctl3D = False
        DataField = 'NH_DIACHI'
        DataSource = DsDanhmuc
        ParentCtl3D = False
        TabOrder = 5
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit3: TwwDBEdit
        Left = 106
        Top = 144
        Width = 293
        Height = 22
        Ctl3D = False
        DataField = 'NH_DTHOAI'
        DataSource = DsDanhmuc
        ParentCtl3D = False
        TabOrder = 6
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object DBEdit1: TwwDBEdit
        Left = 106
        Top = 72
        Width = 293
        Height = 22
        Ctl3D = False
        DataField = 'DAIDIEN'
        DataSource = DsDanhmuc
        ParentCtl3D = False
        TabOrder = 3
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit2: TwwDBEdit
        Left = 106
        Top = 192
        Width = 293
        Height = 22
        Ctl3D = False
        DataField = 'CN_DIACHI'
        DataSource = DsDanhmuc
        ParentCtl3D = False
        TabOrder = 8
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBEdit3: TwwDBEdit
        Left = 106
        Top = 216
        Width = 293
        Height = 22
        Ctl3D = False
        DataField = 'CN_DTHOAI'
        DataSource = DsDanhmuc
        ParentCtl3D = False
        TabOrder = 9
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBDateTimePicker2: TwwDBDateTimePicker
        Left = 106
        Top = 264
        Width = 101
        Height = 22
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'NGAY'
        DataSource = DsDanhmuc
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = True
        TabOrder = 12
      end
      object CkbVISA: TwwCheckBox
        Left = 286
        Top = 244
        Width = 113
        Height = 17
        DisableThemes = False
        AlwaysTransparent = False
        ValueChecked = 'True'
        ValueUnchecked = 'False'
        DisplayValueChecked = 'True'
        DisplayValueUnchecked = 'False'
        NullAndBlankState = cbUnchecked
        BiDiMode = bdRightToLeft
        Caption = 'D'#249'ng m'#225'y POS'
        DataField = 'CHARGE_POS'
        DataSource = DsDanhmuc
        ParentBiDiMode = False
        TabOrder = 11
      end
      object wwDBLookupCombo1: TwwDBLookupCombo
        Left = 106
        Top = 240
        Width = 165
        Height = 22
        Ctl3D = False
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        BorderStyle = bsNone
        DropDownAlignment = taLeftJustify
        Selected.Strings = (
          'TEN_HOTRO'#9'25'#9'TEN_HOTRO'#9'F')
        DataField = 'PTTT'
        DataSource = DsDanhmuc
        LookupTable = QrPTTT
        LookupField = 'MA_HOTRO'
        Options = [loColLines]
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 10
        AutoDropDown = True
        ShowButton = True
        UseTFields = False
        PreciseEditRegion = False
        AllowClearKey = True
        ShowMatchText = True
      end
      object CbMANH: TDbLookupComboboxEh2
        Left = 106
        Top = 96
        Width = 293
        Height = 22
        ControlLabel.Width = 61
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Ng'#226'n h'#224'ng'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        DynProps = <>
        DataField = 'MANH'
        DataSource = DsDanhmuc
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            AutoFitColWidth = False
            FieldName = 'TENNH'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 290
          end
          item
            AutoFitColWidth = False
            FieldName = 'MANH'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'M'#227
            Width = 55
          end>
        DropDownBox.ListSource = DataMain.DsNganhang
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        DropDownBox.Width = 370
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MANH'
        ListField = 'TENNH'
        ListSource = DataMain.DsNganhang
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 4
        Visible = True
      end
      object CbMANH_CN: TDbLookupComboboxEh2
        Left = 106
        Top = 168
        Width = 293
        Height = 22
        ControlLabel.Width = 57
        ControlLabel.Height = 16
        ControlLabel.Caption = 'Chi nh'#225'nh'
        ControlLabel.Font.Charset = DEFAULT_CHARSET
        ControlLabel.Font.Color = clWindowText
        ControlLabel.Font.Height = -13
        ControlLabel.Font.Name = 'Tahoma'
        ControlLabel.Font.Style = []
        ControlLabel.ParentFont = False
        ControlLabel.Visible = True
        ControlLabelLocation.Spacing = 5
        ControlLabelLocation.Position = lpLeftCenterEh
        AlwaysShowBorder = True
        AutoSize = False
        BevelKind = bkFlat
        Ctl3D = False
        ParentCtl3D = False
        BorderStyle = bsNone
        DynProps = <>
        DataField = 'MANH_CN'
        DataSource = DsDanhmuc
        DropDownBox.AutoFitColWidths = False
        DropDownBox.Columns = <
          item
            AutoFitColWidth = False
            FieldName = 'TENNH_CN'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'T'#234'n'
            Width = 290
          end
          item
            AutoFitColWidth = False
            FieldName = 'MA'
            Font.Charset = DEFAULT_CHARSET
            Font.Color = clWindowText
            Font.Height = -12
            Font.Name = 'Tahoma'
            Font.Style = []
            SpecCell.Font.Charset = DEFAULT_CHARSET
            SpecCell.Font.Color = clWindowText
            SpecCell.Font.Height = -12
            SpecCell.Font.Name = 'Tahoma'
            SpecCell.Font.Style = []
            Title.Alignment = taCenter
            Title.Caption = 'M'#227
            Width = 65
          end>
        DropDownBox.ListSource = DsNganhangCN
        DropDownBox.ListSourceAutoFilter = True
        DropDownBox.ListSourceAutoFilterType = lsftContainsEh
        DropDownBox.ListSourceAutoFilterAllColumns = True
        DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
        DropDownBox.AutoDrop = True
        DropDownBox.Rows = 15
        DropDownBox.Sizable = True
        DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
        DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
        DropDownBox.SpecRow.Font.Color = clWindowText
        DropDownBox.SpecRow.Font.Height = -12
        DropDownBox.SpecRow.Font.Name = 'Tahoma'
        DropDownBox.SpecRow.Font.Style = []
        DropDownBox.Width = 380
        EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
        EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
        EmptyDataInfo.Font.Color = clSilver
        EmptyDataInfo.Font.Height = -13
        EmptyDataInfo.Font.Name = 'Tahoma'
        EmptyDataInfo.Font.Style = [fsItalic]
        EmptyDataInfo.ParentFont = False
        EmptyDataInfo.Alignment = taLeftJustify
        EditButton.DefaultAction = True
        EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
        EditButton.Style = ebsAltDropDownEh
        EditButton.Width = 20
        EditButton.DrawBackTime = edbtWhenHotEh
        EditButtons = <>
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Flat = True
        KeyField = 'MANH_CN'
        ListField = 'TENNH_CN'
        ListSource = DsNganhangCN
        ParentFont = False
        ShowHint = True
        Style = csDropDownEh
        TabOrder = 7
        Visible = True
      end
    end
    object PD2: TisPanel
      Left = 10
      Top = 345
      Width = 412
      Height = 88
      Align = alBottom
      Color = 16119285
      ParentBackground = False
      TabOrder = 2
      HeaderCaption = ' .: Ghi ch'#250
      HeaderColor = 16119285
      ImageSet = 4
      RealHeight = 99
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object DBMemo1: TDBMemo
        Left = 1
        Top = 17
        Width = 410
        Height = 70
        Align = alClient
        BorderStyle = bsNone
        DataField = 'GHICHU'
        DataSource = DsDanhmuc
        TabOrder = 1
      end
    end
    object isPanel1: TisPanel
      Left = 10
      Top = 291
      Width = 412
      Height = 54
      Align = alBottom
      Color = 16119285
      ParentBackground = False
      TabOrder = 1
      HeaderCaption = ' .: S'#7889' d'#432' '#273#7847'u k'#7923
      HeaderColor = 16119285
      ImageSet = 4
      RealHeight = 58
      ShowButton = True
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = ANSI_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object Label7: TLabel
        Left = 57
        Top = 28
        Width = 40
        Height = 16
        Alignment = taRightJustify
        Caption = 'S'#7889' ti'#7873'n'
        FocusControl = DBEdit3
        Font.Charset = ANSI_CHARSET
        Font.Color = clBlack
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object Label11: TLabel
        Left = 262
        Top = 28
        Width = 28
        Height = 16
        Alignment = taRightJustify
        Caption = 'Ng'#224'y'
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object DBEdit4: TwwDBEdit
        Left = 106
        Top = 24
        Width = 101
        Height = 22
        BorderStyle = bsNone
        Ctl3D = False
        DataField = 'SODU_TK'
        DataSource = DsDanhmuc
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        ParentCtl3D = False
        ParentFont = False
        TabOrder = 1
        UnboundDataType = wwDefault
        WantReturns = False
        WordWrap = False
      end
      object wwDBDateTimePicker1: TwwDBDateTimePicker
        Left = 297
        Top = 24
        Width = 101
        Height = 22
        BorderStyle = bsNone
        CalendarAttributes.Font.Charset = DEFAULT_CHARSET
        CalendarAttributes.Font.Color = clWindowText
        CalendarAttributes.Font.Height = -11
        CalendarAttributes.Font.Name = 'MS Sans Serif'
        CalendarAttributes.Font.Style = []
        DataField = 'SODU_NGAY'
        DataSource = DsDanhmuc
        Epoch = 1950
        ButtonEffects.Transparent = True
        ButtonEffects.Flat = True
        Frame.Enabled = True
        Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
        Font.Charset = ANSI_CHARSET
        Font.Color = clPurple
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ShowButton = True
        TabOrder = 2
      end
    end
  end
  object GrList: TwwDBGrid2
    Left = 0
    Top = 36
    Width = 271
    Height = 435
    TabStop = False
    DittoAttributes.ShortCutDittoField = 16397
    DittoAttributes.Options = [wwdoSkipReadOnlyFields]
    Selected.Strings = (
      'MATK'#9'20'#9'M'#227#9'F'
      'TENTK'#9'30'#9'T'#234'n'#9'F'
      'DAIDIEN'#9'30'#9#272#7841'i di'#7879'n'#9'F'
      'NH_DIACHI'#9'30'#9#272#7881'a ch'#7881' ng'#226'n h'#224'ng'#9'F'
      'NH_DTHOAI'#9'30'#9#272'i'#7879'n tho'#7841'i ng'#226'n h'#224'ng'#9'F'
      'FAX'#9'15'#9'S'#7889' fax'#9'F')
    IniAttributes.Delimiter = ';;'
    TitleColor = 13360356
    FixedCols = 0
    ShowHorzScrollBar = True
    Align = alClient
    DataSource = DsDanhmuc
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    KeyOptions = [dgEnterToTab, dgAllowDelete, dgAllowInsert]
    Options = [dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgShowCellHint]
    ParentFont = False
    PopupMenu = PopSort
    TabOrder = 1
    TitleAlignment = taCenter
    TitleFont.Charset = ANSI_CHARSET
    TitleFont.Color = 8404992
    TitleFont.Height = -11
    TitleFont.Name = 'Tahoma'
    TitleFont.Style = [fsBold]
    TitleLines = 1
    TitleButtons = True
    UseTFields = False
    OnCalcCellColors = GrListCalcCellColors
    TitleImageList = DataMain.ImageSort
    PadColumnStyle = pcsPadHeader
  end
  object ActionList: TActionList
    OnUpdate = ActionListUpdate
    Left = 148
    Top = 168
    object CmdNew: TAction
      Caption = 'Th'#234'm'
      Hint = 'Th'#234'm m'#7851'u tin'
      ShortCut = 16429
      OnExecute = CmdNewExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'L'#432'u c'#225'c thay '#273#7893'i'
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdCancel: TAction
      Caption = 'B'#7887' qua'
      Hint = 'B'#7887' qua c'#225'c thay '#273#7893'i'
      ShortCut = 16452
      OnExecute = CmdCancelExecute
    end
    object CmdDel: TAction
      Caption = 'X'#243'a'
      Hint = 'X'#243'a m'#7851'u tin'
      ShortCut = 16430
      OnExecute = CmdDelExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdPrint: TAction
      Caption = 'In'
      Hint = 'In danh m'#7909'c'
      ShortCut = 16464
      OnExecute = CmdPrintExecute
    end
    object CmdSearch: TAction
      Category = 'POPUP'
      Caption = 'T'#236'm m'#7851'u tin    '
      ShortCut = 16454
      OnExecute = CmdSearchExecute
    end
    object CmdFilter: TAction
      Category = 'POPUP'
      Caption = 'L'#7885'c d'#7919' li'#7879'u'
      ShortCut = 16460
      OnExecute = CmdFilterExecute
    end
    object CmdClearFilter: TAction
      Category = 'POPUP'
      Caption = 'Kh'#244'ng l'#7885'c d'#7919' li'#7879'u'
      OnExecute = CmdClearFilterExecute
    end
    object CmdAudit: TAction
      Caption = 'Th'#244'ng tin ng'#432#7901'i c'#7853'p nh'#7853't'
      ShortCut = 16449
      OnExecute = CmdAuditExecute
    end
    object CmdExportDataGrid: TAction
      Caption = 'CmdExportDataGrid'
      OnExecute = CmdExportDataGridExecute
    end
  end
  object Filter: TwwFilterDialog2
    DataSource = DsDanhmuc
    Options = [fdShowCaseSensitive, fdShowOKCancel, fdShowFieldOrder, fdShowValueRangeTab]
    SortBy = fdSortByFieldNo
    Caption = 'L'#7885'c'
    FilterMethod = fdByQueryModify
    DefaultMatchType = fdMatchAny
    DefaultFilterBy = fdSmartFilter
    DefaultField = 'MAKHO'
    FieldsFetchMethod = fmUseSQL
    FieldOperators.OrChar = 'or'
    FieldOperators.AndChar = 'and'
    FieldOperators.NullChar = 'null'
    Rounding.Epsilon = 0.000100000000000000
    Rounding.RoundingMethod = fdrmFixed
    FilterPropertyOptions.LikeWildcardChar = '%'
    SelectedFields.Strings = (
      'MAKHO'
      'TENKHO'
      'DIACHI'
      'DTHOAI'
      'FAX'
      'THUKHO')
    FilterOptimization = fdNone
    QueryFormatDateMode = qfdMonthDayYear
    SQLTables = <>
    Left = 212
    Top = 196
  end
  object QrDanhmuc: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    BeforeOpen = QrDanhmucBeforeOpen
    BeforeInsert = QrDanhmucBeforeInsert
    AfterInsert = QrDanhmucAfterInsert
    BeforePost = QrDanhmucBeforePost
    AfterPost = QrDanhmucAfterPost
    BeforeDelete = QrDanhmucBeforeDelete
    AfterDelete = QrDanhmucAfterPost
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <
      item
        Name = 'LPOAI'
        Attributes = [paNullable]
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select *'
      '  from DM_TAIKHOAN'
      'where'#9'PLOAI = :LPOAI')
    Left = 148
    Top = 196
    object QrDanhmucMATK: TWideStringField
      DisplayLabel = 'S'#7889' t'#224'i kho'#7843'n'
      FieldName = 'MATK'
      OnChange = QrDanhmucMATKChange
      Size = 30
    end
    object QrDanhmucTENTK: TWideStringField
      DisplayLabel = 'Ch'#7911' t'#224'i kho'#7843'n'
      FieldName = 'TENTK'
      Size = 100
    end
    object QrDanhmucDAIDIEN: TWideStringField
      DisplayLabel = #272#7841'i di'#7879'n'
      FieldName = 'DAIDIEN'
      Size = 100
    end
    object QrDanhmucNH_DIACHI: TWideStringField
      DisplayLabel = #272#7883'a ch'#7881
      FieldName = 'NH_DIACHI'
      Size = 100
    end
    object QrDanhmucNH_DTHOAI: TWideStringField
      DisplayLabel = #272'i'#7879'n tho'#7841'i'
      FieldName = 'NH_DTHOAI'
      Size = 100
    end
    object QrDanhmucFAX: TWideStringField
      DisplayLabel = 'Fax'
      FieldName = 'FAX'
      Size = 100
    end
    object QrDanhmucGHICHU: TWideMemoField
      DisplayLabel = 'Ghi ch'#250
      FieldName = 'GHICHU'
      BlobType = ftWideMemo
    end
    object QrDanhmucCREATE_BY: TIntegerField
      FieldName = 'CREATE_BY'
      Visible = False
    end
    object QrDanhmucUPDATE_BY: TIntegerField
      FieldName = 'UPDATE_BY'
      Visible = False
    end
    object QrDanhmucCREATE_DATE: TDateTimeField
      FieldName = 'CREATE_DATE'
      Visible = False
    end
    object QrDanhmucUPDATE_DATE: TDateTimeField
      FieldName = 'UPDATE_DATE'
      Visible = False
    end
    object QrDanhmucPLOAI: TWideStringField
      FieldName = 'PLOAI'
      Visible = False
      Size = 2
    end
    object QrDanhmucNGAY: TDateTimeField
      FieldName = 'NGAY'
      Visible = False
    end
    object QrDanhmucMADT: TWideStringField
      FieldName = 'MADT'
      Visible = False
      Size = 15
    end
    object QrDanhmucMANV: TWideStringField
      FieldName = 'MANV'
      Visible = False
      Size = 15
    end
    object QrDanhmucCHARGE_POS: TBooleanField
      FieldName = 'CHARGE_POS'
    end
    object QrDanhmucCN_DIACHI: TWideStringField
      FieldName = 'CN_DIACHI'
      Size = 200
    end
    object QrDanhmucCN_DTHOAI: TWideStringField
      FieldName = 'CN_DTHOAI'
      Size = 100
    end
    object QrDanhmucSODU_TK: TFloatField
      FieldName = 'SODU_TK'
    end
    object QrDanhmucSODU_NGAY: TDateTimeField
      FieldName = 'SODU_NGAY'
    end
    object QrDanhmucPTTT: TWideStringField
      FieldName = 'PTTT'
      Size = 2
    end
    object QrDanhmucMANH: TWideStringField
      FieldName = 'MANH'
      OnChange = QrDanhmucMANHChange
      Size = 10
    end
    object QrDanhmucMANH_CN: TWideStringField
      FieldName = 'MANH_CN'
      Size = 26
    end
    object QrDanhmucLK_TenLoaiTaiKhoan: TWideStringField
      FieldKind = fkLookup
      FieldName = 'LK_TenPTTT'
      LookupDataSet = QrPTTT
      LookupKeyFields = 'MA_HOTRO'
      LookupResultField = 'TEN_HOTRO'
      KeyFields = 'PTTT'
      Size = 200
      Lookup = True
    end
  end
  object DsDanhmuc: TDataSource
    DataSet = QrDanhmuc
    Left = 148
    Top = 224
  end
  object PopSort: TAdvPopupMenu
    AutoHotkeys = maManual
    AutoLineReduction = maManual
    Images = DataMain.ImageSmall
    MenuStyler = DataMain.AdvMenuOfficeStyler1
    Version = '2.6.6.0'
    Left = 148
    Top = 256
    object Tmmutin1: TMenuItem
      Action = CmdSearch
      ImageIndex = 31
    end
    object Lcdliu1: TMenuItem
      Action = CmdFilter
      ImageIndex = 39
    end
    object N1: TMenuItem
      Caption = '-'
    end
    object Khnglcdliu1: TMenuItem
      Action = CmdClearFilter
    end
    object N2: TMenuItem
      Caption = '-'
    end
    object XutdliutliraExcel1: TMenuItem
      Caption = 'Xu'#7845't d'#7919' li'#7879'u t'#7915' l'#432#7899'i ra Excel'
      OnClick = CmdExportDataGridExecute
    end
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 212
    Top = 224
  end
  object QrPTTT: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    OnDeleteError = OnDbError
    OnEditError = OnDbError
    OnPostError = OnDbError
    Parameters = <>
    SQL.Strings = (
      'select *'
      '  from V_TAIKHOAN_PTTT')
    Left = 164
    Top = 364
  end
  object QrNganhangCN: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    Filtered = True
    DataSource = DataMain.DsNganhang
    Parameters = <
      item
        Name = 'MANH'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 10
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'*'
      '  from  '#9'DM_NGANHANG_CHINHANH'
      'where'#9'MANH = :MANH')
    Left = 44
    Top = 316
  end
  object DsNganhangCN: TDataSource
    DataSet = QrNganhangCN
    Left = 44
    Top = 344
  end
end
