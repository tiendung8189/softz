﻿object frLoc: TfrLoc
  Left = 0
  Top = 0
  Width = 515
  Height = 49
  Align = alTop
  Color = 16119285
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  ParentBackground = False
  ParentColor = False
  ParentFont = False
  TabOrder = 0
  ExplicitWidth = 451
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 515
    Height = 49
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentBackground = False
    ParentColor = True
    TabOrder = 0
    ExplicitWidth = 451
    object Label1: TLabel
      Left = 31
      Top = 16
      Width = 51
      Height = 16
      Alignment = taRightJustify
      Caption = #272#7883'a '#273'i'#7875'&m'
      FocusControl = CbLoc
    end
    object CbLoc: TwwDBLookupCombo
      Left = 92
      Top = 12
      Width = 265
      Height = 24
      DropDownAlignment = taLeftJustify
      Selected.Strings = (
        'TEN'#9'30'#9'TEN'#9'F'
        'LOC'#9'6'#9'LOC'#9'F')
      LookupTable = DataMain.QrLOC
      LookupField = 'LOC'
      Style = csDropDownList
      TabOrder = 0
      AutoDropDown = False
      ShowButton = True
      PreciseEditRegion = False
      AllowClearKey = False
ButtonEffects.Transparent=True
ButtonEffects.Flat=True
    end
  end
end
