﻿unit CR208U;

interface
uses
	Classes, windows, SysUtils, OleCtrls;

type
  Trf_init_com = function(port,baud:integer):integer;stdcall;
  Trf_ClosePort = function ():integer;stdcall;
  Trf_beep = function (icdev: Word; msec: Byte): integer;stdcall;
  TReset_Command = function ():integer;stdcall;

  TRead_Em4001 = function (var buff: Byte):integer;stdcall;
  TT55x7_Read = function(var length: Byte; var buff: Byte):integer;stdcall;

  Trf_get_model = Function  (icdev: Word; var buff, length: Byte):integer;stdcall;
  Trf_get_device_number = Function (var icdev: Byte):integer;stdcall;
  Trf_init_device_number = Function (icdev: Integer):integer;stdcall;

  TStandard_Write = Function (opcode, lockbit: Byte; var buff: Byte; block: Byte): integer;stdcall;
  TProtected_Write = Function (opcode: Byte; var password: Byte; lockbit: Byte; var buff: Byte; block: Byte): integer;stdcall;
  TDirect_Access_PWD = function (opcode: Byte; var password: Byte; block: Byte): integer;stdcall;
  TDirect_Access = function (opcode: Byte; block: Byte): integer;stdcall;

  TCr208U = class(TObject)
    FPort: Word;
    FBaud: Integer;
    FConected: Boolean;
    FiError: Integer;
    FsError: String;
    FdllHandle : Cardinal;
    FopCode: Byte;
    FPassLen: Byte;
    FHexLen: Byte;
    FLastType: Byte;
    FNoBeep: Boolean;
  public
    function Connect: Boolean; overload;
    function Connect(const pPort, pBaud: Integer): Boolean; overload;
    function DisConnect: Boolean;
    function Get_device_model(var pData: String): Boolean;
    function Get_device_number(var pDev: String): Boolean;
    function Set_device_number(pDev: String): Boolean;
    function Reset_Command(): Boolean;

    function ReadCard(var pData: String; pBitLen: Boolean = False): Boolean;
    function ReadCardT55x7(var pData: String; pBitLen: Boolean = False): Boolean;
    function ReadCardEm4001(var pData: String): Boolean;

    function Write_Standard(pData: String): Boolean;
    function Write_Protected(pData, pPass: String): Boolean;
    function Direct_Access(pBlock: Byte = 1): Boolean;
    function Direct_Access_PWD(pPass: String = '00000000'; pBlock: Byte = 1): Boolean;
    procedure Beep(psec: Byte = 6);

    constructor Create(const APort: Word; ABaud: Integer= 19200); overload;
    destructor Destroy; override;

    property GetLastError: String read FsError;
    property Connected: Boolean read FConected;
    property NoBeep: Boolean read FNoBeep Write FNoBeep;
  end;

  TMyProgressEvent = procedure(PercentComplete: Integer) of object;
  TReturnReadCard = procedure(pResult: Boolean; pData: String)of object;
  TCr208UThread = class(TThread)
  private
    FOnMyProgress: TMyProgressEvent;
    FReturnReadCard: TReturnReadCard;
    FCr208U: TCr208U;

    FTimesStart, FTimesEnd: Integer;
    FCheckLenght: Boolean;
    function GetErrorCode: Integer;
  protected
    procedure Execute; override;
  public
    procedure StartReadCard();

    constructor Create(const APort: Word; ABaud: Integer = 19200; ATimes: Integer = 10000); overload;
    destructor Destroy; override;

    property OnMyProgress: TMyProgressEvent
      read FOnMyProgress
      write FOnMyProgress;
    property ReturnReadCard: TReturnReadCard
      read FReturnReadCard
      write FReturnReadCard;
    property WaitTime: Integer read FTimesEnd write FTimesEnd;
    property ErrorCode: Integer Read GetErrorCode;
    property CheckLength: Boolean read FCheckLenght Write FCheckLenght;
  end;

implementation

uses
    isStr;

{ TCr208U }
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TCr208U.Beep(psec: Byte);
var
    cmd: Trf_beep;
begin
    if FNoBeep then
        Exit;

    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'rf_beep');
    if not Assigned(cmd) then
        Exit;

    cmd(0, psec);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Connect: Boolean;
var
    cmd: Trf_init_com;
begin
    Result := FdllHandle <> 0;
    if not Result then
        Exit;
    FiError := -1;

    @cmd := GetProcAddress(FdllHandle, 'rf_init_com');
    if Assigned(cmd) then
       FiError := cmd(FPort, FBaud);
    Result := FiError = 0;
    FConected := Result;
    if Result then
    begin
        FsError := '';
        Exit;
    end;

    if FiError = 1 then
        FsError := 'Thiết bị đang có kết nối.'
    else
        FsError := Format('Lỗi kết nối port %d.', [FPort])

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Connect(const pPort, pBaud: Integer): Boolean;
begin
    FPort := pPort;
    FBaud := pBaud;
    Result := Connect;
end;

constructor TCr208U.Create(const APort: Word; ABaud: Integer);
begin
    FPort := APort;
    FBaud := ABaud;
    FiError := 0;
    FHexLen := 2;
    FLastType := 1;
    FConected := False;
    FNoBeep := False;
    FdllHandle := LoadLibrary('MasterRD.dll');
    if FdllHandle < 1 then
    begin
        FsError := 'Lỗi thư viện "MasterRD.dll".';
        FiError := 100;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TCr208U.Destroy;
begin
    FreeLibrary(FdllHandle);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Direct_Access(pBlock: Byte): Boolean;
var
    cmd: TDirect_Access;
begin
    Result := False;
    if FdllHandle < 1 then
        Exit;
    @cmd := GetProcAddress(FdllHandle, 'Direct_Access');
    if not Assigned(cmd) then
        Exit;

    FiError := cmd(FopCode, pBlock);
    Result := FiError = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Direct_Access_PWD(pPass: String; pBlock: Byte): Boolean;
var
    cmd: TDirect_Access_PWD;
    mPass: array[0..10] of Byte;
    i: Byte;
begin
    Result := False;
    if FdllHandle < 1 then
        Exit;
    @cmd := GetProcAddress(FdllHandle, 'Direct_Access_PWD');
    if not Assigned(cmd) then
        Exit;
    ZeroMemory(@mPass, FPassLen);
    for i := 0 to FPassLen - 1 do
        mPass[i] := StrToIntDef('$' + copy(pPass, i*2+1, 2), 0);

    FiError := cmd(FopCode, mPass[0], pBlock);
    Result := FiError = 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.DisConnect: Boolean;
var
    cmd: Trf_ClosePort;
begin
    Result := True;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'rf_ClosePort');
    if Assigned(cmd) then
    begin
        FiError := cmd();
        Result := FiError = 0;
    end;

    FConected := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Get_device_number(var pDev: String): Boolean;
var
    cmd: Trf_get_device_number;
    mBuff: array[0..2] of Byte;
begin
    Result := False; ;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'rf_get_device_number');
    if Assigned(cmd) then
    begin
        ZeroMemory(@mBuff, Length(mBuff));
        FiError := cmd(mBuff[0]);
    end;
    Result := FiError = 0;
    if not Result then
        FsError := 'Read Device Number Fail'
    else
    begin
        FsError := '';
        pDev := '';
        if mBuff[1] <> 0 then
            pDev := IntToHex(mBuff[1], 2);
        pDev := pDev + IntToHex(mBuff[0], 2);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Get_device_model(var pData: String): Boolean;
var
    cmd: Trf_get_model;
    mBuff: array[0..200] of Byte;
    i: Integer;
    l: Byte;
begin
    Result := False;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'rf_get_model');
    if Assigned(cmd) then
    begin
        ZeroMemory(@mBuff, 200);
        FiError := cmd(0, mBuff[0], l);
    end;

    pData:='';
    Result := FiError = 0;
    if not Result then
        FsError := 'Read Device Mode Fail.'
    else
    begin
        for i:=0 to l-1 do
            pData:=pData+ Chr(mBuff[i]);

        FsError := '';
        Beep(6);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.ReadCard(var pData: String; pBitLen: Boolean): Boolean;
begin
    case FLastType of
    1:
        Result := ReadCardEm4001(pData);
    else
        Result := ReadCardT55x7(pData, pBitLen);
    end;

    //Ho tro 2 loai the.
    if not Result then
    begin
        Reset_Command;
        FLastType := FLastType xor 1;
        Sleep(30);
        case FLastType of
        1:
            Result := ReadCardEm4001(pData);
        else
            Result := ReadCardT55x7(pData, pBitLen);
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.ReadCardEm4001(var pData: String): Boolean;
var
    cmd: TRead_Em4001;
    mBuff: array[0..200] of Byte;
    i: Integer;
begin
    Result := False;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'Read_Em4001');
    if Assigned(cmd) then
    begin
        ZeroMemory(@mBuff, 200);
        FiError := cmd(mBuff[0]);
        Result := FiError = 0;
    end;

    if not Result then
        FsError := 'Lỗi đọc dữ liệu.'
    else
    begin
        pData:='';
        for i:=0 to 4 do
            pData:=pData+IntToHex(mBuff[i],2);

        FsError := '';
        Beep(6);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.ReadCardT55x7(var pData: String; pBitLen: Boolean): Boolean;
var
    cmd: TT55x7_Read;
    mBuff: array[0..200] of Byte;
    l: Byte;
    i: Integer;
begin
    Result := False;
    if FdllHandle < 1 then
        Exit;

    Direct_Access;
    @cmd := GetProcAddress(FdllHandle, 'T55x7_Read');
    if Assigned(cmd) then
    begin
        ZeroMemory(@mBuff, 200);
        FiError := cmd(l, mBuff[0]);
        Result := FiError = 0;
    end;

    if not Result then
        FsError := 'Lỗi đọc dữ liệu.'
    else
    begin
        pData:='';
        for i:=0 to l - 1 do
            pData:=pData+IntToHex(mBuff[i],2);

        //byte cuoi cuu block thu 7, luu chieu dai du lieu.
        i := mBuff[l-1];
        if ((i <> 0) and (mBuff[l-2] = 0)) or pBitLen then
            pData := Copy(pData, 0, i);
        FsError := '';
        Beep(6);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Reset_Command(): Boolean;
var
    cmd: TReset_Command;
begin
    Result := True;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'Reset_Command');
    if Assigned(cmd) then
    begin
        FiError := cmd();
        Result := FiError = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Set_device_number(pDev: String): Boolean;
var
    cmd: Trf_init_device_number;
    i: Integer;
begin
    Result := False; ;
    if FdllHandle < 1 then
        Exit;

    try
        i := StrToInt('$'+pDev);
    except
        FsError := 'Device Number Error';
        FiError := 100;
        Exit;
    end;

    @cmd := GetProcAddress(FdllHandle, 'rf_init_device_number');
    if Assigned(cmd) then
    begin
        FiError := cmd(i);
        Result := FiError = 0;
    end;

    if not Result then
        FsError := 'Set Device Number Fail'
    else
        FsError := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Write_Protected(pData, pPass: String): Boolean;
var
    cmd: TProtected_Write;
    mBuff: array[0..3] of Byte;
    mPass: array[0..3] of Byte;
begin
    Result := False; ;
    if FdllHandle < 1 then
        Exit;

    @cmd := GetProcAddress(FdllHandle, 'Protected_Write');
    if Assigned(cmd) then
    begin
        FiError := cmd(10, mPass[0], 0, mBuff[0], 1);
        Result := FiError = 0;
    end;

    if not Result then
        FsError := 'Write Protected Fail'
    else
        FsError := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208U.Write_Standard(pData: String): Boolean;
var
    cmd: TStandard_Write;
    mBuff: array[0..3] of Byte;
    i, l, k, j, n: Integer;
    mData: String;
    s: AnsiString;
begin
    Result := False; ;
    if FdllHandle < 1 then
        Exit;
    mData := pData;
    n := Length(mData);

    //Kiem tra du lieu hop le
    if n > 56 then
    begin
        FsError := 'Dữ liệu không hợp lệ (phải < 56 ký tự).';
        raise Exception.Create(FsError);
        Exit;
    end;
    k := Round(n/2);
    for i:=0 to k do
    begin
        s := isSubStr(mData, i*2+1,2);
        if s = '' then
            Break;
        val('$'+s, mbuff[0], j);
        if j <> 0 then
        begin
            FsError := Format('Dữ liệu không hợp lệ ở ký tự thứ %d.', [i*2+j-1]);
            raise Exception.Create(FsError);
            Exit;
        end;
    end;

    //
    @cmd := GetProcAddress(FdllHandle, 'Standard_Write');
    if Assigned(cmd) then
    begin
        //6 Block dau, luu du lieu.
        for l := 1 to 7 do
        begin
            ZeroMemory(@mBuff, 4);
            i := 0;
            while i < 4 do
            begin
                j := (l-1)*8+i*2+1;
                if j > n then
                    Break;

                s := isSubStr(mData, j, FHexLen);
                if Length(s) < FHexLen then
                    s := isPadRight(s, FHexLen);
                Val('$'+ s, mbuff[i], k);

                inc(i);
            end;

            FiError := cmd(2, 0, mBuff[0], l);
        end;

        if n < 56 then
        begin
            //Block 7: luu chieu dai du lieu.
            ZeroMemory(@mBuff, 4);
            mBuff[3] := n;
            FiError := cmd(2, 0, mBuff[0], 7);
        end;

        Result := FiError = 0;
    end;
    Beep(6);

    if not Result then
        FsError := 'Write card error'
    else
        FsError := '';
end;

{ TCr208UThread }

(*==============================================================================
**------------------------------------------------------------------------------
*)
constructor TCr208UThread.Create(const APort: Word; ABaud: Integer; ATimes: Integer);
begin
    Inherited Create(True);
    FCr208U := TCr208U.Create(APort, ABaud);
    FCr208U.Connect;
    FTimesStart := 0;
    FTimesEnd := ATimes;
    FCheckLenght := True;
    Self.FreeOnTerminate := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
destructor TCr208UThread.Destroy;
begin
    //FCr208U.DisConnect;
    if FCr208U <> nil then
    begin
        FCr208U := nil;
        FCr208U.Free;
    end;
    inherited;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TCr208UThread.Execute;
var
    s: String;
    b: Boolean;
begin
    b := False;
    s := '';
    if FCr208U.Connected then
        while FTimesStart < FTimesEnd do
        begin
            if Terminated then
                break;

            Inc(FTimesStart);
            if Assigned(FOnMyProgress) then
                FOnMyProgress(FTimesStart);

            b := FCr208U.ReadCard(s, CheckLength);
            if b then
                Break;

            Sleep(100);
        end;

    if Assigned(FReturnReadCard) then
    with FCr208U do
    begin
        if b and CheckLength and (s = '') then
        begin
            s := 'Thẻ chưa ghi dữ liệu';
            b := False;
        end
        else
        if (FiError > 0) and (FiError <> 20) and (s = '') then
            s := GetLastError;

        FReturnReadCard(b, s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TCr208UThread.GetErrorCode: Integer;
begin
    Result := FCr208U.FiError;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
Procedure TCr208UThread.StartReadCard();
begin
    Self.Resume;
end;

end.
