﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ThongtinCN;

interface

uses
  SysUtils, Classes, Controls, Forms,
  StdCtrls, Buttons, wwdbdatetimepicker, ExtCtrls, wwdblook, Mask, wwdbedit,
  ActnList;

type
  TFrmThongtinCN = class(TForm)
    Panel1: TPanel;
    CmdReturn: TBitBtn;
    Label6: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label2: TLabel;
    Label5: TLabel;
    EdTEN: TwwDBEdit;
    DBEdit2: TwwDBEdit;
    DBEdit3: TwwDBEdit;
    DBEdit1: TwwDBEdit;
    DBEdit5: TwwDBEdit;
    Label1: TLabel;
    wwDBEdit1: TwwDBEdit;
    Label30: TLabel;
    wwDBLookupCombo11: TwwDBLookupCombo;
    wwDBLookupCombo12: TwwDBLookupCombo;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    EdTENDT: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    wwDBEdit3: TwwDBEdit;
    Label12: TLabel;
    Label13: TLabel;
    Label14: TLabel;
    MyActionList: TActionList;
    CmdOK: TAction;
    Label15: TLabel;
    wwDBEdit4: TwwDBEdit;
    GroupBox1: TGroupBox;
    GroupBox2: TGroupBox;
    GroupBox3: TGroupBox;
    BitBtn2: TBitBtn;
    procedure FormShow(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdOKExecute(Sender: TObject);
  private
    mPTTT: String;
  public
  	function Execute (pPTTT : String) : Boolean;
  end;

var
  FrmThongtinCN: TFrmThongtinCN;

implementation

uses
	isLib, PosMain, MainData, isDb, isMsg;
    
{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.CmdOKExecute(Sender: TObject);
begin
    CmdReturn.SetFocus;
    with (FrmMain.QrBH) do
    begin
        SetDefButton(1);
        if BlankConfirm(FrmMain.QrBH, ['CN_TENDV', 'CN_DIACHI', 'CN_LIENHE', 'CN_DTHOAI']) then
            Abort;

        if mPTTT = '03' then
        begin
            if BlankConfirm(FrmMain.QrBH, ['CN_MATK']) then
                Abort;
        end;
        FieldByName('TTTT').AsString := '01';
        SetDefButton(0);
    end;
    ModalResult := mrOk;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmThongtinCN.Execute;
begin
    mPTTT := pPTTT;
	Result := ShowModal = mrOK
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormKeyPress(Sender: TObject; var Key: Char);
begin
    Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmThongtinCN.FormShow(Sender: TObject);
var
    b: Boolean;
begin
    TMyForm(Self).Init;
    b := mPTTT = '03';
    Label11.Visible := b;
    if not b then
        Self.Height := Self.Height - GroupBox3.Height;
    GroupBox3.Visible := b;
end;

end.
