﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit HoadonGTGTCT;

interface

uses
  SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid;

type
  TFrmHoadonGTGTCT = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    ToolButton9: TToolButton;
    QrDetail: TADOQuery;
    Image1: TImage;
    CmdAudit: TAction;
    QrDetailKHOACT: TGuidField;
    QrDetailKHOA: TGuidField;
    QrDetailSTT: TIntegerField;
    QrDetailMAVT: TWideStringField;
    QrDetailTENVT: TWideStringField;
    QrDetailDONGIA: TFloatField;
    QrDetailSOLUONG: TFloatField;
    QrDetailTL_CK: TFloatField;
    QrDetailCHIETKHAU: TFloatField;
    QrDetailSOTIEN: TFloatField;
    QrDetailCALC_SOTIEN_SAUCK: TFloatField;
    QrDetailLK_TENVT: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure GrListURLOpen(Sender: TObject; var URLLink: String;
      Field: TField; var UseDefault: Boolean);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDetailCalcFields(DataSet: TDataSet);
  private
    mCanEdit: Boolean;
    mKhoa: TGUID;
  	mTrigger: Boolean;
  public
  	procedure Execute(pCanEdit: Boolean; pKhoa: TGUID; pMa, pTen: String);
  end;

var
  FrmHoadonGTGTCT: TFrmHoadonGTGTCT;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, GuidEx, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'HOADON_GTGT_CTHD';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.Execute;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;

    mKhoa := pKhoa;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.FormShow(Sender: TObject);
begin
	OpenDataSets([QrDetail]);
    SetDisplayFormat(QrDetail, sysCurFmt);
    SetDisplayFormat(QrDetail, ['SOLUONG'], ctQtyFmt);
    SetDisplayFormat(QrDetail, ['TL_CK'], sysPerFmt);

    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDetail, FORM_CODE);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailBeforePost(DataSet: TDataSet);
begin
	with QrDetail do
	begin
		if BlankConfirm(QrDetail, ['TENVT']) then
			Abort;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailCalcFields(DataSet: TDataSet);
begin
    with QrDetail do
        FieldByName('CALC_SOTIEN_SAUCK').AsFloat :=
            FieldByName('SOTIEN').AsFloat - FieldByName('CHIETKHAU').AsFloat
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailBeforeDelete(DataSet: TDataSet);
begin
   	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := TGuidEx.ToString(mKhoa);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.GrListURLOpen(Sender: TObject; var URLLink: String;
  Field: TField; var UseDefault: Boolean);
begin
    if URLLink = '' then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmHoadonGTGTCT.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDetail);
end;

end.
