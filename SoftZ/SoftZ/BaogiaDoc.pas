﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit BaogiaDoc;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms,
  ComCtrls, ActnList, Wwdbgrid2, ExtCtrls,
  ADODb, Db,
  Menus , AppEvnts,
  Wwdbgrid, RzDBBnEd,
  AdvMenus, StdCtrls, Mask, RzEdit, RzDBEdit, Grids, Wwdbigrd, ToolWin;

type
  TFrmBaogiaDoc = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton11: TToolButton;
    CmdSearch: TAction;
    Status: TStatusBar;
    QrList: TADOQuery;
    DsList: TDataSource;
    ToolButton2: TToolButton;
    ToolButton7: TToolButton;
    PopSort: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    GrList: TwwDBGrid2;
    Tmmutin1: TMenuItem;
    PaMaster: TPanel;
    BtImport: TRzDBButtonEdit;
    BtClearFile: TRzDBButtonEdit;
    BtView: TRzDBButtonEdit;
    QrListNAME: TWideStringField;
    QrListFILENAME: TWideStringField;
    QrListCONTENT: TBlobField;
    QrListEXT: TWideStringField;
    QrListGHICHU: TWideStringField;
    QrListCREATE_BY: TIntegerField;
    QrListUPDATE_BY: TIntegerField;
    QrListCREATE_DATE: TDateTimeField;
    QrListUPDATE_DATE: TDateTimeField;
    QrListROWGUID: TGuidField;
    QrListF1: TIntegerField;
    QrListF2: TIntegerField;
    QrListF3: TIntegerField;
    QrListCAL_STT: TIntegerField;
    QrListCAL_EXT: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure CmdCloseExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure FormShow(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure QrListBeforeDelete(DataSet: TDataSet);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure FormCreate(Sender: TObject);
    procedure QrListBeforePost(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrListBeforeInsert(DataSet: TDataSet);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure BtImportButtonClick(Sender: TObject);
    procedure BtClearFileButtonClick(Sender: TObject);
    procedure BtViewButtonClick(Sender: TObject);
  private
  	mCanEdit: Boolean;
    mKhoa: Integer;
    procedure Attach();
    procedure Dettach();
    procedure ViewAttachment();
  public
  	procedure Execute (r: WORD);
  end;

var
  FrmBaogiaDoc: TFrmBaogiaDoc;

implementation

uses
	ExCommon,  isDb, islib, isMsg, Rights, isStr, isFile,
    ShellAPI, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.Execute (r: WORD);
begin
	mCanEdit := rCanEdit(r);
	DsList.AutoEdit := mCanEdit;
    GrList.ReadOnly := not mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE: String   = 'BAOGIA_DOC';

procedure TFrmBaogiaDoc.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrList, FORM_CODE);

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.FormShow(Sender: TObject);
begin
    ForceDirectories(sysAppData);
    QrList.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.Attach();
var
	s: string;
begin
   	// Get file name
	s := isGetOpenFileName;
    if s = '' then
    	Exit;

    with QrList do
    begin
    	Edit;
		TBlobField(FieldByName('CONTENT')).LoadFromFile(s);
	    if Trim(FieldByName('FILENAME').AsString) = '' then
        	FieldByName('FILENAME').AsString := ChangeFileExt(ExtractFileName(s), '');
	    FieldByName('EXT').AsString := ExtractFileExt(s);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.BtClearFileButtonClick(Sender: TObject);
begin
    Dettach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.BtImportButtonClick(Sender: TObject);
begin
    Attach;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.BtViewButtonClick(Sender: TObject);
begin
    ViewAttachment;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.Dettach();
begin
     with QrList do
     begin
        Edit;
        FieldByName('GHICHU').Clear;
        FieldByName('CONTENT').Clear;
        FieldByName('EXT').Clear;
     end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.ViewAttachment();
var
	s, sn: String;
begin
	with QrList do
    begin
    	sn := isStripToneMark(FieldByName('GHICHU').AsString);
        s := sysAppData + sn + FieldByName('EXT').AsString;
        try
	        TBlobField(FieldByName('CONTENT')).SaveToFile(s);
        except
        	ErrMsg('Lỗi trích xuất nội dung.');
        	Exit;
        end;
    end;
    ShellExecute(Handle, 'Open', PChar(s), Nil, Nil, SW_SHOWMAXIMIZED);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
        CloseDataSets([QrList]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
    CanClose := CheckBrowseDataSet(QrList, True);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdNewExecute(Sender: TObject);
begin
    QrList.Append;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdSaveExecute(Sender: TObject);
begin
    QrList.Post;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdCancelExecute(Sender: TObject);
begin
    QrList.Cancel;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdDelExecute(Sender: TObject);
begin
    QrList.Delete;
	GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsList)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
    exActionUpdate(ActionList, QrList, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.QrListBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.QrListBeforePost(DataSet: TDataSet);
begin
	with (DataSet) do
    begin
        if BlankConfirm(QrList,['GHICHU']) then
            Abort;
    end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.QrListCalcFields(DataSet: TDataSet);
var
	s: String;
    fi: _SHFILEINFO;
begin
    with QrList do
    begin
        FieldByName('CAL_STT').AsInteger := Abs(RecNo);

        // File extension
        s := FieldByName('EXT').AsString;
        if s = '' then
        else
        begin
			SHGetFileInfo(PWideChar(s), 0, fi, SizeOf(fi),
    	    	SHGFI_USEFILEATTRIBUTES + SHGFI_TYPENAME);
			FieldByName('CAL_EXT').AsString := String(fi.szTypeName) + ' (' + s + ')';
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.QrListBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmBaogiaDoc.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrList);
end;

end.
