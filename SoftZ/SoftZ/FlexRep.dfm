object FrmFlexRep: TFrmFlexRep
  Left = 149
  Top = 112
  HelpContext = 1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'B'#225'o C'#225'o - Xu'#7845't Excel'
  ClientHeight = 573
  ClientWidth = 792
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyPress = FormKeyPress
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Bevel1: TBevel
    Left = 394
    Top = 0
    Width = 3
    Height = 552
    Align = alRight
    Shape = bsSpacer
    Style = bsRaised
    ExplicitLeft = 393
  end
  object PaLeft: TPanel
    Left = 0
    Top = 0
    Width = 394
    Height = 552
    Align = alClient
    BevelOuter = bvNone
    TabOrder = 0
    object Splitter1: TSplitter
      Left = 0
      Top = 249
      Width = 394
      Height = 3
      Cursor = crVSplit
      Align = alBottom
      ExplicitTop = 285
      ExplicitWidth = 393
    end
    object Panel4: TPanel
      Left = 0
      Top = 0
      Width = 394
      Height = 249
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 0
      object Bevel4: TBevel
        Left = 0
        Top = 28
        Width = 394
        Height = 2
        Align = alTop
        Shape = bsSpacer
        ExplicitWidth = 393
      end
      object PgGroup: TPageControl
        Left = 0
        Top = 0
        Width = 394
        Height = 28
        Cursor = 1
        ActivePage = TabSheet4
        Align = alTop
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        HotTrack = True
        ParentFont = False
        RaggedRight = True
        TabOrder = 0
        TabStop = False
        OnChange = PgGroupChange
        object TabSheet4: TTabSheet
          Caption = '0'
        end
        object TabSheet6: TTabSheet
          Caption = '1'
          ImageIndex = 1
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet11: TTabSheet
          Caption = '2'
          ImageIndex = 2
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet1: TTabSheet
          Caption = '3'
          ImageIndex = 3
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet2: TTabSheet
          Caption = '4'
          ImageIndex = 4
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet3: TTabSheet
          Caption = '5'
          ImageIndex = 5
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet5: TTabSheet
          Caption = '6'
          ImageIndex = 6
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet7: TTabSheet
          Caption = '7'
          ImageIndex = 7
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet8: TTabSheet
          Caption = '8'
          ImageIndex = 8
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
        object TabSheet9: TTabSheet
          Caption = '9'
          ImageIndex = 9
          ExplicitLeft = 0
          ExplicitTop = 0
          ExplicitWidth = 0
        end
      end
      object GrList: TwwDBGrid2
        Left = 0
        Top = 30
        Width = 394
        Height = 219
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        ControlType.Strings = (
          'IMG;ImageIndex;Original Size')
        Selected.Strings = (
          'IMG'#9'2'#9'IMG'#9'F'
          'DESC'#9'49'#9'DESC'#9'F')
        IniAttributes.Delimiter = ';;'
        TitleColor = clBtnFace
        FixedCols = 0
        ShowHorzScrollBar = False
        Align = alClient
        DataSource = DsRep
        KeyOptions = []
        Options = [dgEditing, dgColumnResize, dgTabs, dgRowSelect, dgAlwaysShowSelection, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowCellHint, dgProportionalColResize]
        TabOrder = 1
        TitleAlignment = taLeftJustify
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = clWindowText
        TitleFont.Height = -13
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = []
        TitleLines = 1
        TitleButtons = False
        UseTFields = False
        OnCalcCellColors = GrListCalcCellColors
        OnDblClick = GrListDblClick
        ImageList = DataMain.ImageSmall
        PadColumnStyle = pcsPadHeader
      end
    end
    object PaExcel: TisPanel
      Left = 0
      Top = 252
      Width = 394
      Height = 300
      Align = alBottom
      BevelOuter = bvNone
      Color = 16119285
      Font.Charset = DEFAULT_CHARSET
      Font.Color = 8404992
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentBackground = False
      ParentFont = False
      TabOrder = 1
      HeaderCaption = ' :: D'#7919' li'#7879'u'
      HeaderColor = 16119285
      ImageSet = 4
      RealHeight = 0
      ShowButton = False
      HeaderBevelInner = bvNone
      HeaderBevelOuter = bvNone
      HeaderFont.Charset = DEFAULT_CHARSET
      HeaderFont.Color = clBlue
      HeaderFont.Height = -11
      HeaderFont.Name = 'Tahoma'
      HeaderFont.Style = [fsBold]
      object Bevel3: TBevel
        Left = 0
        Top = 16
        Width = 394
        Height = 1
        Align = alTop
        Shape = bsSpacer
        ExplicitWidth = 792
      end
      object PaFirst: TPanel
        Left = 0
        Top = 54
        Width = 394
        Height = 246
        Align = alClient
        BevelOuter = bvLowered
        Caption = 
          'Ch'#7885'n t'#234'n b'#225'o c'#225'o - nh'#7853'p '#273'i'#7873'u ki'#7879'n - click "Th'#7921'c hi'#7879'n" '#273#7875' t'#7841'o b'#225'o' +
          ' c'#225'o.'
        Font.Charset = DEFAULT_CHARSET
        Font.Color = 8404992
        Font.Height = -17
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        TabOrder = 3
      end
      object ToolBar1: TToolBar
        Left = 0
        Top = 17
        Width = 394
        Height = 37
        ButtonHeight = 36
        ButtonWidth = 77
        Caption = 'ToolBar1'
        DisabledImages = DataMain.ImageNavi
        Font.Charset = ANSI_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        Images = DataMain.ImageNavi
        ParentFont = False
        ShowCaptions = True
        TabOrder = 1
        object ToolButton5: TToolButton
          Left = 0
          Top = 0
          Cursor = 1
          Action = CmdRun
        end
        object ToolButton6: TToolButton
          Left = 77
          Top = 0
          Width = 8
          Caption = 'ToolButton6'
          ImageIndex = 6
          Style = tbsSeparator
        end
        object ToolButton1: TToolButton
          Left = 85
          Top = 0
          Cursor = 1
          Action = CmdSave
        end
        object ToolButton3: TToolButton
          Left = 162
          Top = 0
          Width = 8
          Caption = 'ToolButton3'
          ImageIndex = 2
          Style = tbsSeparator
        end
        object ToolButton2: TToolButton
          Left = 170
          Top = 0
          Cursor = 1
          Action = CmdFields
        end
        object ToolButton4: TToolButton
          Left = 247
          Top = 0
          Width = 8
          Caption = 'ToolButton4'
          ImageIndex = 0
          Style = tbsSeparator
        end
        object ToolButton11: TToolButton
          Left = 255
          Top = 0
          Cursor = 1
          Action = CmdClose
        end
      end
      object GrExcel: TwwDBGrid2
        Left = 0
        Top = 54
        Width = 394
        Height = 246
        DittoAttributes.ShortCutDittoField = 16397
        DittoAttributes.Options = [wwdoSkipReadOnlyFields]
        MemoAttributes = [mSizeable, mWordWrap, mGridShow]
        IniAttributes.Delimiter = ';;'
        TitleColor = 13360356
        FixedCols = 0
        ShowHorzScrollBar = False
        Align = alClient
        DataSource = DsExcel
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        Options = [dgEditing, dgTitles, dgIndicator, dgColumnResize, dgColLines, dgRowLines, dgTabs, dgConfirmDelete, dgCancelOnExit, dgWordWrap, dgShowFooter]
        ParentFont = False
        TabOrder = 2
        TitleAlignment = taCenter
        TitleFont.Charset = DEFAULT_CHARSET
        TitleFont.Color = 8404992
        TitleFont.Height = -11
        TitleFont.Name = 'Tahoma'
        TitleFont.Style = [fsBold]
        TitleLines = 1
        TitleButtons = True
        Visible = False
        OnUpdateFooter = GrExcelUpdateFooter
        TitleImageList = DataMain.ImageSort
        FooterColor = 13360356
        FooterCellColor = 13360356
      end
    end
  end
  object Status: TStatusBar
    Left = 0
    Top = 552
    Width = 792
    Height = 21
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBtnText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    Panels = <>
    SimplePanel = True
    UseSystemFont = False
  end
  object PaRight: TisPanel
    Left = 397
    Top = 0
    Width = 395
    Height = 552
    Align = alRight
    BevelOuter = bvNone
    Color = 16119285
    ParentBackground = False
    TabOrder = 1
    HeaderCaption = ' :: '#272'i'#7873'u ki'#7879'n'
    HeaderColor = 16119285
    ImageSet = 4
    RealHeight = 0
    ShowButton = False
    HeaderBevelInner = bvNone
    HeaderBevelOuter = bvNone
    HeaderFont.Charset = DEFAULT_CHARSET
    HeaderFont.Color = clBlue
    HeaderFont.Height = -11
    HeaderFont.Name = 'Tahoma'
    HeaderFont.Style = [fsBold]
    object Bevel8: TBevel
      Left = 0
      Top = 0
      Width = 395
      Height = 4
      Align = alTop
      Shape = bsSpacer
      Style = bsRaised
    end
    object PaParam: TPanel
      Left = 0
      Top = 20
      Width = 395
      Height = 532
      Align = alClient
      BevelOuter = bvNone
      TabOrder = 1
    end
  end
  object ActionList1: TActionList
    Images = DataMain.ImageNavi
    OnUpdate = ActionList1Update
    Left = 76
    Top = 196
    object CmdRun: TAction
      Caption = 'Th'#7921'c hi'#7879'n'
      Hint = 'T'#7841'o b'#225'o c'#225'o'
      ImageIndex = 9
      ShortCut = 120
      OnExecute = CmdRunExecute
    end
    object CmdCalc: TAction
      Tag = 1
      Hint = 'T'#237'nh t'#7891'n kho'
      OnExecute = CmdCalcExecute
    end
    object CmdReread: TAction
      Hint = #208#7885'c l'#7841'i danh s'#225'ch b'#225'o c'#225'o'
      ShortCut = 16466
      OnExecute = CmdRereadExecute
    end
    object CmdClose: TAction
      Caption = 'K'#7871't th'#250'c'
      Hint = 'K'#7871't th'#250'c'
      ImageIndex = 5
      ShortCut = 32856
      OnExecute = CmdCloseExecute
    end
    object CmdSave: TAction
      Caption = 'L'#432'u'
      Hint = 'Xu'#7845't ra Excel'
      ImageIndex = 26
      ShortCut = 16467
      OnExecute = CmdSaveExecute
    end
    object CmdFields: TAction
      Caption = 'Ch'#7885'n tr'#432#7901'ng'
      Hint = 'Ch'#7885'n danh s'#225'ch tr'#432#7901'ng d'#7919' li'#7879'u c'#7847'n xu'#7845't Excel'
      ImageIndex = 11
      ShortCut = 115
      OnExecute = CmdFieldsExecute
    end
    object CmdShowRpt: TAction
      Hint = 'Xem b'#225'o c'#225'o'
      ShortCut = 116
      Visible = False
      OnExecute = CmdShowRptExecute
    end
  end
  object DsRep: TDataSource
    DataSet = QrREP
    Left = 44
    Top = 224
  end
  object QrNHOM: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    DataSource = DsNGANH
    Parameters = <
      item
        Name = 'MANGANH'
        DataType = ftString
        NumericScale = 255
        Precision = 255
        Size = 2
        Value = Null
      end>
    SQL.Strings = (
      'select '#9'MA, MANHOM, TENNHOM'
      '  from '#9'DM_NHOM'
      ' where '#9'MANGANH =:MANGANH'
      'order by MANHOM'
      ' '
      ' ')
    Left = 44
    Top = 260
  end
  object QrNGANH: TADOQuery
    Connection = DataMain.Conn
    CursorType = ctStatic
    LockType = ltReadOnly
    Parameters = <>
    SQL.Strings = (
      'select MANGANH, TENNGANH'
      '  from DM_NGANH'
      'order by MANGANH')
    Left = 44
    Top = 292
  end
  object DsNGANH: TDataSource
    DataSet = QrNGANH
    Left = 44
    Top = 324
  end
  object QrREP: TADOStoredProc
    Connection = DataMain.Conn
    Filtered = True
    AfterScroll = QrREPAfterScroll
    ProcedureName = 'USER_REP;1'
    Parameters = <
      item
        Name = '@RETURN_VALUE'
        DataType = ftInteger
        Direction = pdReturnValue
        Precision = 10
        Value = Null
      end
      item
        Name = '@UID'
        Attributes = [paNullable]
        DataType = ftInteger
        Precision = 10
        Value = Null
      end
      item
        Name = '@EN'
        Attributes = [paNullable]
        DataType = ftBoolean
        Value = Null
      end
      item
        Name = '@GROUP'
        Attributes = [paNullable]
        DataType = ftWideString
        Size = 1000
        Value = Null
      end>
    Left = 44
    Top = 195
  end
  object SMExportToExcel1: TSMExportToExcel
    AnimatedStatus = False
    DataFormats.DateOrder = doDMY
    DataFormats.DateSeparator = '/'
    DataFormats.TimeSeparator = ':'
    DataFormats.FourDigitYear = True
    DataFormats.LeadingZerosInDate = True
    DataFormats.ThousandSeparator = ','
    DataFormats.DecimalSeparator = '.'
    DataFormats.CurrencyString = '$'
    DataFormats.BooleanTrue = 'True'
    DataFormats.BooleanFalse = 'False'
    DataFormats.UseRegionalSettings = True
    KeyGenerator = 'SMExport 4.99'
    Options = [soWaitCursor, soDisableControls, soUseFieldNameAsCaption]
    TitleStatus = 'Exporting...'
    OnProgress = SMExportToExcel1Progress
    Columns = <>
    Bands = <>
    DBGrid = GrExcel
    FileName = 'C:\Program Files\CodeGear\RAD Studio\6.0\bin\SMExport.XLS'
    AddTitle = True
    CharacterSet = csANSI_WINDOWS
    DetailSources = <>
    AutoFitColumns = True
    DeleteDefaultSheets = True
    ExportStyle.Style = esNormal
    ExportStyle.OddColor = clBlack
    ExportStyle.EvenColor = clBlack
    Left = 316
    Top = 400
  end
  object DsExcel: TDataSource
    DataSet = QrExcel
    Left = 220
    Top = 456
  end
  object QrExcel: TADOStoredProc
    Connection = DataMain.Conn
    LockType = ltReadOnly
    Parameters = <>
    Left = 220
    Top = 424
  end
  object ApplicationEvents1: TApplicationEvents
    OnIdle = ApplicationEvents1Idle
    Left = 168
    Top = 192
  end
  object CheckSP: TADOCommand
    CommandText = 'select [name] from sysobjects where [name] =:ReportName'
    Connection = DataMain.Conn
    Parameters = <
      item
        Name = 'ReportName'
        DataType = ftWideString
        NumericScale = 255
        Precision = 255
        Size = 128
        Value = Null
      end>
    Left = 216
    Top = 312
  end
end
