object frKHO: TfrKHO
  Left = 0
  Top = 0
  Width = 522
  Height = 77
  Align = alTop
  AutoSize = True
  Color = 16119285
  ParentBackground = False
  ParentColor = False
  TabOrder = 0
  ExplicitWidth = 451
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 522
    Height = 77
    Align = alTop
    BevelInner = bvRaised
    BevelOuter = bvLowered
    ParentBackground = False
    ParentColor = True
    TabOrder = 0
    object Label65: TLabel
      Left = 91
      Top = 16
      Width = 40
      Height = 13
      Alignment = taRightJustify
      Caption = 'T'#7915' ng'#224'y'
    end
    object Label66: TLabel
      Left = 319
      Top = 16
      Width = 47
      Height = 13
      Alignment = taRightJustify
      Caption = #272#7871'n ng'#224'y'
    end
    object EdFrom: TwwDBDateTimePicker
      Left = 140
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.FocusBorders = []
      Frame.NonFocusBorders = []
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 0
    end
    object EdTo: TwwDBDateTimePicker
      Left = 376
      Top = 12
      Width = 101
      Height = 24
      CalendarAttributes.Font.Charset = DEFAULT_CHARSET
      CalendarAttributes.Font.Color = clWindowText
      CalendarAttributes.Font.Height = -11
      CalendarAttributes.Font.Name = 'MS Sans Serif'
      CalendarAttributes.Font.Style = []
      Epoch = 1950
      Frame.NonFocusBorders = [efLeftBorder, efTopBorder, efRightBorder, efBottomBorder]
      Font.Charset = ANSI_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      ShowButton = True
      TabOrder = 1
    end
    object cbbKho: TDbLookupComboboxEh2
      Tag = 3
      Left = 140
      Top = 40
      Width = 281
      Height = 22
      ControlLabel.Width = 87
      ControlLabel.Height = 16
      ControlLabel.Caption = #272'i'#7875'm b'#225'n h'#224'ng'
      ControlLabel.Font.Charset = DEFAULT_CHARSET
      ControlLabel.Font.Color = clWindowText
      ControlLabel.Font.Height = -13
      ControlLabel.Font.Name = 'Tahoma'
      ControlLabel.Font.Style = []
      ControlLabel.ParentFont = False
      ControlLabel.Visible = True
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      AlwaysShowBorder = True
      AutoSize = False
      BevelKind = bkFlat
      Ctl3D = False
      ParentCtl3D = False
      BorderStyle = bsNone
      DynProps = <>
      DropDownBox.AutoFitColWidths = False
      DropDownBox.Columns = <
        item
          FieldName = 'TENKHO'
          Width = 285
        end
        item
          FieldName = 'MAKHO'
          Width = 80
        end>
      DropDownBox.ListSource = DataMain.DsDMKHO
      DropDownBox.ListSourceAutoFilter = True
      DropDownBox.ListSourceAutoFilterType = lsftContainsEh
      DropDownBox.ListSourceAutoFilterAllColumns = True
      DropDownBox.Options = [dlgColumnResizeEh, dlgColLinesEh]
      DropDownBox.AutoDrop = True
      DropDownBox.Rows = 15
      DropDownBox.Sizable = True
      DropDownBox.SpecRow.CellsText = 'T'#7845't c'#7843
      DropDownBox.SpecRow.Font.Charset = DEFAULT_CHARSET
      DropDownBox.SpecRow.Font.Color = clWindowText
      DropDownBox.SpecRow.Font.Height = -12
      DropDownBox.SpecRow.Font.Name = 'Tahoma'
      DropDownBox.SpecRow.Font.Style = []
      DropDownBox.Width = 390
      EmptyDataInfo.Text = '-- Ch'#7885'n d'#7919' li'#7879'u --'
      EmptyDataInfo.Font.Charset = DEFAULT_CHARSET
      EmptyDataInfo.Font.Color = clSilver
      EmptyDataInfo.Font.Height = -13
      EmptyDataInfo.Font.Name = 'Tahoma'
      EmptyDataInfo.Font.Style = [fsItalic]
      EmptyDataInfo.ParentFont = False
      EmptyDataInfo.Alignment = taLeftJustify
      EditButton.DefaultAction = True
      EditButton.DropDownFormParams.PassParams = pspCustomValuesEh
      EditButton.Style = ebsAltDropDownEh
      EditButton.Width = 20
      EditButton.DrawBackTime = edbtWhenHotEh
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Flat = True
      KeyField = 'MAKHO'
      ListField = 'TENKHO'
      ListSource = DataMain.DsDMKHO
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Style = csDropDownEh
      TabOrder = 2
      Visible = True
      OnChange = cbbKhoExit
      OnCloseUp = cbbKhoCloseUp
      OnDropDown = cbbKhoDropDown
      OnExit = cbbKhoExit
    end
    object EdMaKho: TDBEditEh
      Left = 424
      Top = 40
      Width = 53
      Height = 22
      TabStop = False
      Alignment = taLeftJustify
      BevelKind = bkFlat
      BorderStyle = bsNone
      Color = clBtnFace
      ControlLabelLocation.Spacing = 5
      ControlLabelLocation.Position = lpLeftCenterEh
      Ctl3D = False
      DynProps = <>
      EditButtons = <>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clPurple
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      Flat = True
      ParentCtl3D = False
      ParentFont = False
      ParentShowHint = False
      ReadOnly = True
      ShowHint = True
      TabOrder = 3
      Visible = True
    end
  end
end
