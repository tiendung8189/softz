﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit PhieuQuatang;

interface

uses
  Windows, SysUtils, Classes, Controls, Forms, Variants,
  StdCtrls, ComCtrls, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  AdvMenus, AppEvnts, wwfltdlg,
  frameNgay, frameNavi,
  isPanel, isDb, wwDialog, Mask, Grids, Wwdbgrid, ToolWin, RzLaunch;

type
  TFrmPhieuQuatang = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Status: TStatusBar;
    Label27: TLabel;
    GrBrowse: TwwDBGrid2;
    Filter: TwwFilterDialog2;
    BtnXoa: TToolButton;
    QrNX: TADOQuery;
    QrCT: TADOQuery;
    DsNX: TDataSource;
    DsCT: TDataSource;
    QrCTRSTT: TIntegerField;
    QrNXNGAY: TDateTimeField;
    QrNXSCT: TWideStringField;
    QrNXDGIAI: TWideMemoField;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrNXXOA: TWideStringField;
    QrNXCREATE_BY: TIntegerField;
    QrNXUPDATE_BY: TIntegerField;
    QrNXDELETE_BY: TIntegerField;
    PaInfo: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    LbKHO: TLabel;
    EdSCT: TwwDBEdit;
    CbNGAY: TwwDBDateTimePicker;
    DBMemo1: TDBMemo;
    QrNXCREATE_DATE: TDateTimeField;
    QrNXUPDATE_DATE: TDateTimeField;
    QrNXDELETE_DATE: TDateTimeField;
    Tm1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    QrNXIMG: TIntegerField;
    Hinttc: TMenuItem;
    QrCTSTT: TIntegerField;
    SepChecked: TToolButton;
    PopMaster: TAdvPopupMenu;
    Label3: TLabel;
    ApplicationEvents1: TApplicationEvents;
    Bevel2: TBevel;
    N2: TMenuItem;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    QrCTGHICHU: TWideStringField;
    PaChitiet: TisPanel;
    GrDetail: TwwDBGrid2;
    ActionList: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    CmdRefresh: TAction;
    CmdSwitch: TAction;
    CmdFilter: TAction;
    CmdDel: TAction;
    CmdSearch: TAction;
    CmdCancel: TAction;
    CmdClearFilter: TAction;
    CmdReRead: TAction;
    QrNXLCT: TWideStringField;
    DBText2: TDBText;
    Label5: TLabel;
    CmdAudit: TAction;
    CmdListRefesh: TAction;
    Label4: TLabel;
    wwDBDateTimePicker1: TwwDBDateTimePicker;
    PopDetail: TAdvPopupMenu;
    Xachititchngt1: TMenuItem;
    QrNXKHOA: TGuidField;
    QrCTKHOACT: TGuidField;
    QrCTKHOA: TGuidField;
    QrNXLOC: TWideStringField;
    QrNXLYDO: TWideStringField;
    QrCsv: TADOQuery;
    QrNXCHECKED: TBooleanField;
    QrNXIMG2: TIntegerField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    QrCTLOC: TWideStringField;
    ItemObsolete: TMenuItem;
    QrNXMENHGIA: TFloatField;
    QrNXSOLUONG_PHATHANH: TFloatField;
    QrNXSOGIO_HIEULUC: TFloatField;
    QrNXNGAY_HETHAN: TDateTimeField;
    QrNXTL_CK_BAN_TU: TFloatField;
    QrNXTL_CK_BAN_DEN: TFloatField;
    QrCTMASO: TWideStringField;
    QrCTMATHE: TWideStringField;
    QrCTMACHU: TWideStringField;
    QrCTMENHGIA: TFloatField;
    QrCTTL_CK_BAN_TU: TFloatField;
    QrCTTL_CK_BAN_DEN: TFloatField;
    QrCTNGAY_PHATHANH: TDateTimeField;
    QrCTSOGIO_HIEULUC: TFloatField;
    QrCTNGAY_HIEULUC: TDateTimeField;
    QrCTNGAY_HETHAN: TDateTimeField;
    QrCTNGAY_SUDUNG: TDateTimeField;
    QrCTNGAY_KHOATHE: TDateTimeField;
    wwDBEdit3: TwwDBEdit;
    Label6: TLabel;
    wwDBEdit1: TwwDBEdit;
    wwDBEdit2: TwwDBEdit;
    Label7: TLabel;
    wwDBEdit4: TwwDBEdit;
    Label8: TLabel;
    wwDBEdit5: TwwDBEdit;
    Label9: TLabel;
    Label10: TLabel;
    PHIEUQUATANG_GEN_NEW: TADOCommand;
    CmdMavach: TAction;
    PHIEUQUATANG_EXPORT: TADOStoredProc;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    CmdPhathanh: TAction;
    CmdHuythe: TAction;
    spPQT_PHATHANH: TADOCommand;
    CmdPhathanhCT: TAction;
    ToolButton10: TToolButton;
    ToolButton12: TToolButton;
    ToolButton13: TToolButton;
    ToolButton14: TToolButton;
    spPQT_HUYTHE: TADOCommand;
    CmdHuytheCT: TAction;
    HyphthnhPhiu1: TMenuItem;
    Panel1: TPanel;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrNXAfterInsert(DataSet: TDataSet);
    procedure QrNXBeforeOpen(DataSet: TDataSet);
    procedure QrNXBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeOpen(DataSet: TDataSet);
    procedure QrCTBeforePost(DataSet: TDataSet);
    procedure QrCTBeforeEdit(DataSet: TDataSet);
    procedure QrCTBeforeDelete(DataSet: TDataSet);
    procedure QrCTBeforeInsert(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrNXBeforeInsert(DataSet: TDataSet);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure CmdCancelExecute(Sender: TObject);
    procedure QrNXCalcFields(DataSet: TDataSet);
    procedure QrNXBeforeEdit(DataSet: TDataSet);
    procedure QrNXAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);

    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure CbMAKHONotInList(Sender: TObject; LookupTable: TDataSet;
      NewValue: String; var Accept: Boolean);
    procedure QrNXAfterScroll(DataSet: TDataSet);
    procedure QrCTAfterDelete(DataSet: TDataSet);
    procedure QrCTAfterInsert(DataSet: TDataSet);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdListRefeshExecute(Sender: TObject);
    procedure QrNXNGAYValidate(Sender: TField);
    procedure CbMAKHOBeforeDropDown(Sender: TObject);
    procedure CbMAKHOCloseUp(Sender: TObject; LookupTable, FillTable: TDataSet;
      modified: Boolean);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure CmdMavachExecute(Sender: TObject);
    procedure CmdPhathanhExecute(Sender: TObject);
    procedure CmdHuytheExecute(Sender: TObject);
    procedure QrNXBeforeDelete(DataSet: TDataSet);
    procedure CmdPhathanhCTExecute(Sender: TObject);
    procedure CmdHuytheCTExecute(Sender: TObject);
  private
  	mLCT: String;
	mCanEdit, mObsolete, mInsert: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc: String;

    // List filter
    fType: Integer;
    fSQL, fStr: String;

    procedure OpenDetail;
    procedure GenNewPQT;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmPhieuQuatang: TFrmPhieuQuatang;

implementation

uses
	isMsg, ExCommon, MainData, RepEngine, Rights, IniFiles, GuidEx, isCommon,
    ChonDsma, isLib, wwStr, isFile, Sapthutu;

{$R *.DFM}
const
	FORM_CODE = 'PHIEU_QUATANG';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.Execute;
begin
	mLCT := 'PQT';
	mCanEdit := rCanEdit(r);
    DsNX.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.FormCreate(Sender: TObject);
begin
    // Initial
    TMyForm(Self).Init2;
    frNavi.DataSet := QrNX;
    frDate.Init;

    // Initial
  	mTrigger := False;
    mObsolete := False;
    mInsert := False;
    fType := 2;
    fStr := '';
    fSQL := QrNX.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.FormShow(Sender: TObject);
begin
	Wait(PREPARING);
    InitFmtAndRoud(mLct);

    OpenDataSets([DataMain.QrDMKHO]);

    with QrNX do
    begin
		SetDisplayFormat(QrNX, sysCurFmt);
    	SetShortDateFormat(QrNX);
        SetDisplayFormat(QrNX, ['SOLUONG_PHATHANH'], sysIntFmt);
        SetDisplayFormat(QrNX, ['TL_CK_BAN_TU', 'TL_CK_BAN_DEN'], sysPerFmt);
        SetDisplayFormat(QrNX, ['NGAY', 'NGAY_HETHAN'], DateTimeFmt);
    end;

    with QrCT do
    begin
	    SetDisplayFormat(QrCT, sysCurFmt);
        SetShortDateFormat(QrCT);
        SetDisplayFormat(QrCT, ['TL_CK_BAN_TU', 'TL_CK_BAN_DEN'], sysPerFmt);
        SetDisplayFormat(QrCT,
            ['NGAY_PHATHANH', 'NGAY_HIEULUC', 'NGAY_HETHAN',
            'NGAY_SUDUNG', 'NGAY_KHOATHE'],
            DateTimeFmt);
    end;

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT'], [GrBrowse, GrDetail]);
    SetDictionary([QrNX, QrCT], [FORM_CODE, FORM_CODE + '_CT'], [Filter, Nil]);

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    ClearWait;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrNX, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.PgMainChange(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
    		GrDetail.SetFocus;
	   	end;
    end
	else
    begin
    	GrBrowse.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*
    **  commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdRefreshExecute(Sender: TObject);
var
	s : String;
begin
   	if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrNX do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');

            SQL.Add('order by NGAY desc, SCT desc');
    	    Open;
            if s <> '' then
            	Sort := s;
        end;
        if PgMain.ActivePageIndex = 0 then
        	GrBrowse.SetFocus;
        Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdListRefeshExecute(Sender: TObject);
begin
	Screen.Cursor := crSQLWait;
    DataMain.QrDMKHO.Requery;

    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdMavachExecute(Sender: TObject);
var
    s: String;
	b: Boolean;
	m1, m2: Integer;
begin
	// Export path
    s := isGetSaveFileName('CSV');
	if s = '' then
    	Exit;

	// Exporting
    Cursor := crSQLWait;
	with PHIEUQUATANG_EXPORT do
    begin
    	Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));
    	ExecProc;
        Active := True;
//        TextExport(PHIEUQUATANG_EXPORT, s, TEncoding.UTF8);
        TextExport(PHIEUQUATANG_EXPORT, s);
        Active := False
    end;

	Cursor := crDefault;
    MsgDone;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdNewExecute(Sender: TObject);
begin
	if not (QrNX.State in [dsBrowse]) then
    	if SaveConfirm then
        	CmdSave.Execute
        else
        begin
			QrCT.CancelBatch;
			QrNX.Cancel;
        end;

	QrNX.Append;
    mInsert := True;
    ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdSaveExecute(Sender: TObject);
begin
    QrCT.CheckBrowseMode;
    QrNX.Post;
    QrCT.UpdateBatch;
//    exSaveDetails(QrCT);
    if mInsert then
    begin
        GenNewPQT;
        ActiveSheet(PgMain);
        mInsert := False;
        MsgDone;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdCancelExecute(Sender: TObject);
begin
	QrCT.CancelBatch;
	QrNX.Cancel;
    mInsert := False;

    if QrNX.IsEmpty then
	    ActiveSheet(PgMain, 0)
    else
	    ActiveSheet(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdDelExecute(Sender: TObject);
var
	s: String;
    khoa: TGUID;
begin
    khoa := TGuidField(QrNX.FieldByName('KHOA')).AsGuid;

    if not DataMain.IsCheckDelete(khoa, 'PHIEUQUATANG') then
        Exit;

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
   	if not YesNo(RS_CONFIRM_XOAPHIEU, 1) then
        Exit;

    s := TGuidEx.ToString(khoa);
    DataMain.Conn.Execute(Format('delete PHIEUQUATANG_CT where KHOA=%s', [QuotedStr(s)]));

    mInsert := False;
    mTrigger := True;
    MarkDataSet(QrNX);
    mTrigger := False;
    OpenDetail;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdPhathanhCTExecute(Sender: TObject);
begin
    if not YesNo('Phát hành Thẻ Quà Tặng này miễn phí. Tiếp tục?') then
        Exit;

    if DataMain.PhathanhPQT_CT(QrCT.FieldByName('MATHE').AsString) then
    begin
        exReSyncRecord(QrCT);
        MsgDone;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdPhathanhExecute(Sender: TObject);
var
    s: string;
begin
    if not YesNo('Phát hành tất cả Thẻ Quà Tặng đợt này miễn phí. Tiếp tục?') then
        Exit;

    with spPQT_PHATHANH do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));

        Execute;

        if Parameters[0].Value <> 0 then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end else
        begin
            OpenDetail;
            MsgDone;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdPrintExecute(Sender: TObject);
begin
	CmdSave.Execute;

    ShowReport(Caption, FORM_CODE,
    	[sysLogonUID, TGuidEx.ToStringEx(QrNX.FieldByName('KHOA'))]);
end;

(*==============================================================================
** Go~ F6 dde^? switch surround panel
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdSwitchExecute(Sender: TObject);
begin
	if ActiveControl = GrDetail then
    	try
    		CbNgay.SetFocus;
        except
        end
    else
    	GrDetail.SetFocus
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdFilterExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdHuytheCTExecute(Sender: TObject);
begin
    if not YesNo('Thẻ Quà Tặng sau khi "Khóa" sẽ không phát hành/ sử dụng được. Tiếp tục?') then
        Exit;

    if DataMain.HuyPhathanhPQT_CT(QrCT.FieldByName('MATHE').AsString) then
    begin
        exReSyncRecord(QrCT);
        MsgDone;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdHuytheExecute(Sender: TObject);
var
    s: string;
begin
    if not YesNo('"Khóa" tất cả Thẻ Quà Tặng còn lại đợt này và sẽ sẽ không phát hành/ sử dụng được. Tiếp tục?') then
        Exit;

    with spPQT_HUYTHE do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));

        Execute;

        if Parameters[0].Value <> 0 then
        begin
            s := Parameters[3].Value;
            ErrMsg(s);
        end else
        begin
            OpenDetail;
            MsgDone;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsNX)
    else
        exSearch(Name + '_CT', DsCT);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bEmpty, bEmptyCT, bDeleted, bPhathanh, bSudung, bKhoathe: Boolean;
    n: Integer;
begin
	with QrNX do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
	end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := not bBrowse;
    CmdCancel.Enabled := not bBrowse;
    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False) and (not bDeleted);

    CmdDel.Caption := GetMarkCaption(QrNX);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrNX, False);
    CmdChecked.Caption := exGetCheckedCaption(QrNX);


    CmdReRead.Enabled := bBrowse;
    CmdRefresh.Enabled := bBrowse;
    CmdClearFilter.Enabled := Filter.FieldInfo.Count > 0;
    CmdSwitch.Enabled := n = 1;

    with QrCT do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
    	bEmptyCT := IsEmpty;

        bPhathanh := FieldByName('NGAY_PHATHANH').AsDateTime <> 0;
        bSudung := FieldByName('NGAY_SUDUNG').AsDateTime <> 0;
        bKhoathe := FieldByName('NGAY_KHOATHE').AsDateTime <> 0;
	end;

    CmdMavach.Enabled := bBrowse and (not bEmptyCT);
    CmdPhathanh.Enabled := bBrowse and (not bEmptyCT);
    CmdPhathanhCT.Enabled := bBrowse and (not bEmptyCT) and
                                (not bPhathanh) and (not bSudung) and (not bKhoathe);
    CmdHuytheCT.Enabled := bBrowse and (not bEmptyCT) and
                                (not bSudung) and (not bKhoathe);


    PaInfo.Enabled := bEmptyCT;
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master DB
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXAfterInsert(DataSet: TDataSet);
var
    d, d1, d2: TDateTime;
    xx, yy: WORD;
begin
    d := Now;
    d1 := Date;
    DecodeDate(d1, yy, xx, xx);
    d2 := EncodeDate(yy, 12, 31) + EncodeTime(23,59,59,0);;

	with QrNX do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime      	:= d;
		FieldByName('NGAY_HETHAN').AsDateTime         := d2;
		FieldByName('LCT').AsString           	:= mLCT;
        FieldByName('LOC').AsString             := sysLoc;

        FieldByName('TL_CK_BAN_TU').AsFloat := 0;
        FieldByName('TL_CK_BAN_DEN').AsFloat := 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXNGAYValidate(Sender: TField);
begin
    with QrNX do
    	exValidRecordDate(FieldByName('NGAY').AsDateTime, FieldByName('SCT').AsString <> '')
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	with PgMain do
        begin
        	ActivePageIndex := 0;
	        OnChange(Nil);
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXBeforeOpen(DataSet: TDataSet);
begin
	with QrNX do
    begin
		Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXBeforePost(DataSet: TDataSet);
begin
	with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MENHGIA', 'NGAY_HETHAN']) then
    		Abort;
	    exValidClosing(FieldByName('NGAY').AsDateTime);
    end;
    DataMain.AllocSCT(mLCT, QrNX);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXBeforeDelete(DataSet: TDataSet);
begin
    if not DataMain.IsCheckDelete(TGuidField(QrNX.FieldByName('KHOA')).AsGuid,
            'PHIEUQUATANG') then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrNX);
    if mTrigger then
        Exit;
    exIsChecked(QrNX);

	exValidClosing(QrNX.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrNXBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
** Detail DB
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTBeforeOpen(DataSet: TDataSet);
begin
	QrCT.Parameters[0].Value := QrNX.FieldByName('KHOA').Value;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTBeforePost(DataSet: TDataSet);
begin
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTBeforeEdit(DataSet: TDataSet);
begin
	if mTrigger then
    	Exit;
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if not DeleteConfirm then
    	Abort;
        
    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    SetEditState(QrNX);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.GenNewPQT;
begin
    with PHIEUQUATANG_GEN_NEW do
    begin
        Prepared := True;
        Parameters[1].Value := TGuidEx.ToString(QrNX.FieldByName('KHOA'));
        Parameters[2].Value := 7;
        Execute;

        if Parameters[0].Value = 1 then
            ErrMsg('Có lỗi khi tạo mới danh sách Phiếu Quà Tặng')
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.GrBrowseDblClick(Sender: TObject);
begin
    if QrNX.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrNX, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CbMAKHOBeforeDropDown(Sender: TObject);
begin
    (Sender as TwwDBLookupCombo).LookupTable.Filter := 'LOC='+QuotedStr(sysLoc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CbMAKHOCloseUp(Sender: TObject; LookupTable,
  FillTable: TDataSet; modified: Boolean);
begin
    LookupTable.Filter := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CbMAKHONotInList(Sender: TObject;
  LookupTable: TDataSet; NewValue: String; var Accept: Boolean);
begin
	Accept := ComboValueNotInList(Sender, NewValue)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.OpenDetail;
begin
    Screen.Cursor := crSQLWait;
    with QrCT do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTAfterDelete(DataSet: TDataSet);
begin
    Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.QrCTAfterInsert(DataSet: TDataSet);
begin
	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmPhieuQuatang.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsNX, DataMain.QrLOC);
end;

end.
