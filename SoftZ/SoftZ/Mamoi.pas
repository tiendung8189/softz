﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Mamoi;

interface

uses
  Classes, Controls, Forms,
  StdCtrls, Buttons, ExtCtrls;

type
  TFrmMamoi = class(TForm)
    Panel1: TPanel;
    Label1: TLabel;
    EdMA: TEdit;
    CmdReturn: TBitBtn;
    BtnCancel: TBitBtn;
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormShow(Sender: TObject);
  private
  public
    function GetValue(mCu: String): String;
  end;

var
  FrmMamoi: TFrmMamoi;

implementation

{$R *.DFM}

uses
	isLib;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMamoi.FormShow(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmMamoi.GetValue;
begin
	EdMA.Text := mCu;
    if ShowModal = mrOK then
    	Result := EdMA.Text
    else
    	Result := '';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmMamoi.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

end.
