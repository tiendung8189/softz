﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmvtPB;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid, wwdblook;

type
  TFrmDmvtPB = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    ToolButton9: TToolButton;
    QrDetail: TADOQuery;
    Image1: TImage;
    CmdAudit: TAction;
    QrDetailMABH: TWideStringField;
    QrDetailMABH_PB: TWideStringField;
    QrDetailRSTT: TIntegerField;
    QrDetailLK_TENVT: TWideStringField;
    QrDetailLK_DVT: TWideStringField;
    QrDMHH: TADOQuery;
    QrDetailLK_GiaNhap: TFloatField;
    QrDetailLK_GiaBanChuaThue: TFloatField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDetailCalcFields(DataSet: TDataSet);
    procedure QrDetailMABH_PBChange(Sender: TField);
  private
    mCanEdit: Boolean;
    mMavt: String;
  	mTrigger: Boolean;
  public
  	procedure Execute(pCanEdit: Boolean; pMa, pTen: String);
  end;

var
  FrmDmvtPB: TFrmDmvtPB;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, isCommon;

{$R *.DFM}

const
	FORM_CODE = 'DM_HH_CT_PB';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.Execute;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;

    mMavt := pMa;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdNewExecute(Sender: TObject);
begin
	QrDetail.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.FormClose(Sender: TObject; var Action: TCloseAction);
begin
//	HideAudit;
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDetail, FORM_CODE);
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.FormShow(Sender: TObject);
begin
//    with DataMain do
        OpenDataSets([QrDMHH]);
	OpenDataSets([QrDetail]);
    SetDisplayFormat(QrDMHH, sysCurFmt);
    SetDisplayFormat(QrDetail, sysCurFmt);
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailBeforePost(DataSet: TDataSet);
begin
	with QrDetail do
	begin
		if BlankConfirm(QrDetail, ['MABH_PB']) then
			Abort;

        if FieldByName('LK_TENVT').AsString = '' then
        begin
            ErrMsg('Lỗi nhập liệu. Sai mã hàng hóa.');
            Abort;
        end;

		FieldByName('MABH').AsString := mMavt;
	end;
//    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := mMavt;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.CmdAuditExecute(Sender: TObject);
begin
//	ShowAudit(DataMain.QrUSER, DsDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailCalcFields(DataSet: TDataSet);
begin
    with QrDetail do
        FieldByName('RSTT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmvtPB.QrDetailMABH_PBChange(Sender: TField);
begin
    exDotMavt(4, QrDMHH, Sender);
    GrList.InvalidateCurrentRow;
end;

end.
