﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Chi2;

interface

uses
  SysUtils, Classes, Controls, Forms, Vcl.Graphics,
  StdCtrls, ComCtrls, Mask, DBCtrls, ExtCtrls,
  ActnList, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, wwdblook,
  wwdbdatetimepicker, wwdbedit, Wwfltdlg2,
  wwidlg, AdvMenus, AppEvnts, isPanel, wwfltdlg, Wwdbgrid, frameNgay, frameNavi,
  isDb, wwDialog, Grids, ToolWin, Buttons, AdvEdit, DBAdvEd, DBCtrlsEh,
  DBGridEh, DBLookupEh, DbLookupComboboxEh2;

type
  TFrmChi2 = class(TForm)
    ToolMain: TToolBar;
    BtnThem: TToolButton;
    ToolButton8: TToolButton;
    ToolButton3: TToolButton;
    ToolButton9: TToolButton;
    BtnIn: TToolButton;
    ToolButton11: TToolButton;
    ToolButton7: TToolButton;
    Action: TActionList;
    CmdNew: TAction;
    CmdPrint: TAction;
    CmdSave: TAction;
    CmdClose: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    CmdRefresh: TAction;
    Status: TStatusBar;
    Label27: TLabel;
    Filter: TwwFilterDialog2;
    CmdFilter: TAction;
    CmdDel: TAction;
    BtnXoa: TToolButton;
    QrTC: TADOQuery;
    DsTC: TDataSource;
    CmdSearch: TAction;
    CmdCancel: TAction;
    ToolButton1: TToolButton;
    ToolButton2: TToolButton;
    QrTCNGAY: TDateTimeField;
    QrTCSCT: TWideStringField;
    QrTCCREATE_DATE: TDateTimeField;
    QrTCUPDATE_DATE: TDateTimeField;
    QrTCDELETE_DATE: TDateTimeField;
    QrTCLK_TENDT: TWideStringField;
    Panel3: TPanel;
    GrBrowse: TwwDBGrid2;
    QrTCXOA: TWideStringField;
    QrTCMADT: TWideStringField;
    QrTCIMG: TIntegerField;
    CmdClearFilter: TAction;
    PopMaster: TAdvPopupMenu;
    ApplicationEvents1: TApplicationEvents;
    CmdReRead: TAction;
    QrTCMAKHO: TWideStringField;
    QrTCLK_TENKHO: TWideStringField;
    QrTCPTTT: TWideStringField;
    Bevel1: TBevel;
    PaMaster: TPanel;
    Label1: TLabel;
    DBText1: TDBText;
    CbNgay: TwwDBDateTimePicker;
    frNavi: TfrNavi;
    frDate: TfrNGAY;
    Hinttc1: TMenuItem;
    N1: TMenuItem;
    Lc1: TMenuItem;
    Tm1: TMenuItem;
    QrTCLCT: TWideStringField;
    QrTCLK_PTTT: TWideStringField;
    QrCHITIET: TADOQuery;
    DsCT: TDataSource;
    CmdSwitch: TAction;
    QrCHITIETCALC_STT: TIntegerField;
    QrCHITIETLK_PTTT: TWideStringField;
    QrCHITIETLK_NGAY: TDateTimeField;
    QrCHITIETLK_SCT: TWideStringField;
    QrCHITIETLK_THANHTOAN: TFloatField;
    QrCHITIETCALC_CONLAI: TFloatField;
    QrCHITIETLK_HOADON_SO: TWideStringField;
    QrCHITIETLK_HOADON_SERI: TWideStringField;
    QrCHITIETLK_HOADON_NGAY: TDateTimeField;
    CmdAudit: TAction;
    PopNT: TAdvPopupMenu;
    QrCHITIET2: TADOQuery;
    DsCT2: TDataSource;
    QrCHITIET2CALC_STT: TIntegerField;
    CmdChonCT2: TAction;
    Chnphiunhptr1: TMenuItem;
    QrTRAHANG2: TADOQuery;
    QrTCCALC_SOTIEN: TFloatField;
    QrCHITIET2LK_NGAY: TDateTimeField;
    QrCHITIET2LK_SCT: TWideStringField;
    QrCHITIET2LK_THANHTOAN: TFloatField;
    QrCHITIET2LK_DGIAI: TWideStringField;
    PhieuCT2: TwwSearchDialog;
    QrNX2: TADOStoredProc;
    QrCHITIETSCT2: TWideStringField;
    QrCHITIETLK_DGIAI: TWideStringField;
    CmdChonCT: TAction;
    QrTCCREATE_BY: TIntegerField;
    QrTCUPDATE_BY: TIntegerField;
    QrTCDELETE_BY: TIntegerField;
    PopNX: TAdvPopupMenu;
    MenuItem1: TMenuItem;
    QrTRAHANG2NGAY: TDateTimeField;
    QrTRAHANG2SCT: TWideStringField;
    CmdTotal: TAction;
    vlTotal1: TisTotal;
    QrTCCALC_CONLAI: TFloatField;
    QrTCLK_LYDO: TWideStringField;
    vlTotal2: TisTotal;
    CmdEmpty: TAction;
    N2: TMenuItem;
    Xachitit1: TMenuItem;
    N3: TMenuItem;
    Xachitit2: TMenuItem;
    QrCHITIETLK_TC_SOTIEN: TFloatField;
    QrCHITIETTHANHTOAN_NX: TFloatField;
    QrCHITIETTC_SOTIEN_NX: TFloatField;
    QrCHITIETLK_PHIEUGIAOHANG: TWideStringField;
    QrTCKHOA: TGuidField;
    QrCHITIETKHOACT: TGuidField;
    QrCHITIETKHOA: TGuidField;
    QrCHITIETKHOANX: TGuidField;
    QrCHITIET2KHOACT: TGuidField;
    QrCHITIET2KHOA: TGuidField;
    QrCHITIET2KHOANX: TGuidField;
    QrTCLOC: TWideStringField;
    QrTRAHANG2KHOA: TGuidField;
    QrTCMATK: TWideStringField;
    Panel1: TPanel;
    Panel2: TPanel;
    QrTCLK_TENTK: TWideStringField;
    QrTCIMG2: TIntegerField;
    QrTCCHECKED: TBooleanField;
    CmdChecked: TAction;
    ToolButton4: TToolButton;
    SepChecked: TToolButton;
    QrTRAHANG2HOADON_SERI: TWideStringField;
    QrTRAHANG2HOADON_SO: TWideStringField;
    QrTRAHANG2HOADON_NGAY: TDateTimeField;
    QrCHITIET2LK_HOADON_SERI: TWideStringField;
    QrCHITIET2LK_HOADON_SO: TWideStringField;
    QrCHITIET2LK_HOADON_NGAY: TDateTimeField;
    QrCHITIETLOC: TWideStringField;
    QrTCLK_NGANHANG: TWideStringField;
    QrTCLK_CHINHANH: TWideStringField;
    QrTCMALOC: TWideStringField;
    QrTCLK_TENLOC: TWideStringField;
    PaMALOC: TPanel;
    PaMADT: TPanel;
    QrCHITIET2LOC: TWideStringField;
    N4: TMenuItem;
    ItemObsolete: TMenuItem;
    CmdFBChonCT: TAction;
    ChnphiunhpFB1: TMenuItem;
    QrCHITIETLCT: TWideStringField;
    QrNXFB: TADOStoredProc;
    CmdFBChonCT2: TAction;
    ChnphiuxuttrFB1: TMenuItem;
    QrTRAHANGFB: TADOQuery;
    DateTimeField1: TDateTimeField;
    WideStringField1: TWideStringField;
    FloatField1: TFloatField;
    WideMemoField1: TWideMemoField;
    GuidField1: TGuidField;
    WideStringField2: TWideStringField;
    WideStringField3: TWideStringField;
    DateTimeField2: TDateTimeField;
    PhieuCTFB: TwwSearchDialog;
    QrCHITIET2LCT: TWideStringField;
    QrTRAHANGFBLCT: TWideStringField;
    QrTRAHANG2LCT: TWideStringField;
    PgDetail: TPageControl;
    TabSheet3: TTabSheet;
    GrDetail: TwwDBGrid2;
    TabSheet4: TTabSheet;
    GrNhaptra: TwwDBGrid2;
    QrTCSCT2: TWideStringField;
    EdSoPhieu: TDBEditEh;
    CbHinhThuc: TDbLookupComboboxEh2;
    EdMaLoc: TDBEditEh;
    CbbMaLoc: TDbLookupComboboxEh2;
    DBEditEh1: TDBEditEh;
    PaTK: TPanel;
    CbTenTK: TDbLookupComboboxEh2;
    EdSTK: TDBEditEh;
    EdChinhanh: TDBEditEh;
    EdNganhang: TDBEditEh;
    DBEditEh4: TDBEditEh;
    EdNumSoTien: TDBNumberEditEh;
    DBMemoEh1: TDBMemoEh;
    EdSOTIEN: TDBNumberEditEh;
    EdSoTienTraHang: TDBNumberEditEh;
    EhThanhToan: TDBNumberEditEh;
    CmdXemCongNo: TAction;
    CbbNhaCungCap: TDbLookupComboboxEh2;
    EdMaDT: TDBEditEh;
    QrTRAHANG2GhiChu: TWideMemoField;
    QrTRAHANG2ThanhToan: TFloatField;
    QrCHITIET2KhoaCTNX: TGuidField;
    QrCHITIET2SoTien: TFloatField;
    QrCHITIETSoDu: TFloatField;
    QrCHITIETNgayThanhToan: TDateTimeField;
    QrCHITIETKhoaCTNX: TGuidField;
    QrCHITIETTC_XONG: TBooleanField;
    QrCHITIETSoTien: TFloatField;
    QrCHITIETGhiChu: TWideStringField;
    QrTCNguoi: TWideStringField;
    QrTCGhiChu: TWideMemoField;
    QrTCMaCP: TWideStringField;
    QrTCMATK2: TWideStringField;
    QrTCNhanVienThuNgan: TIntegerField;
    QrTCSoTienTraHang: TFloatField;
    QrTCSoGiaoDichNganHang: TWideStringField;
    QrTCSoLuong: TFloatField;
    QrTCSoTien: TFloatField;
    QrTCSoDu: TFloatField;
    QrTCThanhToan: TFloatField;
    QrNX: TADOStoredProc;
    QrTRAHANG: TADOStoredProc;
    QrTCLyDo: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure CmdRefreshExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure CmdPrintExecute(Sender: TObject);
    procedure CmdSwitchExecute(Sender: TObject);
    procedure CmdFilterExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure ActionUpdate(Action: TBasicAction; var Handled: Boolean);
    procedure PgMainChange(Sender: TObject);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure QrTCAfterInsert(DataSet: TDataSet);
    procedure QrTCBeforeOpen(DataSet: TDataSet);
    procedure QrTCBeforePost(DataSet: TDataSet);
    procedure GrBrowseDblClick(Sender: TObject);
    procedure OnDbError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure CmdSearchExecute(Sender: TObject);
    procedure QrTCBeforeInsert(DataSet: TDataSet);
    procedure CmdCancelExecute(Sender: TObject);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrTCCalcFields(DataSet: TDataSet);
    procedure QrTCBeforeEdit(DataSet: TDataSet);
    procedure QrTCAfterCancel(DataSet: TDataSet);
    procedure CmdClearFilterExecute(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdReReadExecute(Sender: TObject);
    procedure QrTCAfterScroll(DataSet: TDataSet);
    procedure QrTCAfterPost(DataSet: TDataSet);
    procedure QrCHITIETBeforeOpen(DataSet: TDataSet);
    procedure QrCHITIETBeforeEdit(DataSet: TDataSet);
    procedure QrCHITIETCalcFields(DataSet: TDataSet);
    procedure QrTCMADTValidate(Sender: TField);
    procedure CmdAuditExecute(Sender: TObject);
    procedure CmdChonCT2Execute(Sender: TObject);
    procedure QrCHITIET2CalcFields(DataSet: TDataSet);
    procedure QrCHITIET2BeforeOpen(DataSet: TDataSet);
    procedure QrCHITIET2BeforeDelete(DataSet: TDataSet);
    procedure PhieuCT2InitDialog(Dialog: TwwLookupDlg);
    procedure CmdChonCTExecute(Sender: TObject);
    procedure QrCHITIETSOTIENValidate(Sender: TField);
    procedure QrNX2AfterOpen(DataSet: TDataSet);
    procedure CmdTotalExecute(Sender: TObject);
    procedure GrDetailUpdateFooter(Sender: TObject);
    procedure QrCHITIETAfterCancel(DataSet: TDataSet);
    procedure QrCHITIETAfterDelete(DataSet: TDataSet);
    procedure QrCHITIETAfterEdit(DataSet: TDataSet);
    procedure QrCHITIETBeforePost(DataSet: TDataSet);
    procedure QrCHITIETBeforeDelete(DataSet: TDataSet);
    procedure QrCHITIETSOTIENChange(Sender: TField);
    procedure QrCHITIETSODUChange(Sender: TField);
    procedure QrCHITIET2KHOANXChange(Sender: TField);
    procedure QrCHITIET2AfterCancel(DataSet: TDataSet);
    procedure QrCHITIET2AfterDelete(DataSet: TDataSet);
    procedure QrCHITIET2AfterEdit(DataSet: TDataSet);
    procedure QrCHITIET2SOTIENChange(Sender: TField);
    procedure QrTCSOTIENChange(Sender: TField);
    procedure CmdEmptyExecute(Sender: TObject);
    procedure QrTCNGAYValidate(Sender: TField);
    procedure QrTCPTTTChange(Sender: TField);
    procedure CmdCheckedExecute(Sender: TObject);
    procedure BtCongnoClick(Sender: TObject);
    procedure ItemObsoleteClick(Sender: TObject);
    procedure PopMasterPopup(Sender: TObject);
    procedure GrBrowseCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
    procedure QrCHITIETBeforeInsert(DataSet: TDataSet);
    procedure QrCHITIET2BeforeInsert(DataSet: TDataSet);
    procedure AdvEdKeyPress(Sender: TObject; var Key: Char);
    procedure CmdFBChonCTExecute(Sender: TObject);
    procedure CmdFBChonCT2Execute(Sender: TObject);
    procedure CmdXemCongNoExecute(Sender: TObject);
    procedure QrTCMATKChange(Sender: TField);
    procedure QrTCMADTChange(Sender: TField);
  private
    mPTTT, mLCT, mMakho: String;
	mCanEdit, mFuncTK, mObsolete: Boolean;
   	fTungay, fDenngay: TDateTime;
    fLoc, fSQL: String;

    procedure OpenDetail;
    function  Subtract(_pSotien: Double): Boolean;
    procedure AllowEditSomeFields;
  public
	procedure Execute(r: WORD);
  end;

var
  FrmChi2: TFrmChi2;

implementation

uses
	isMsg, ExCommon, MainData,  RepEngine, Rights, Variants, isLib, isCommon,
    ChonDsPN, GuidEx, CongnoNCC, FB_ChonDsPN;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CONFIRM = 'Chứng từ sẽ không thể chỉnh sửa sau khi lưu. Tiếp tục?';
    RS_CONFIRM1 = 'Phiếu bị xóa sẽ không phục hồi được. Tiếp tục?';
    RS_MSG = 'Số tiền không hợp lệ.';
    RS_MSG2 = 'Số tiền Tổng cộng phải nhỏ hơn hoặc bằng Tổng số dư.';
    RS_MSG3 = 'Số tiền hỗ trợ không hợp lệ.';
    RS_ERROR    = 'Số tiền không hợp lệ.';
    RS_INVALID_RECEIPT = 'Phiếu không hợp lệ.';

procedure TFrmChi2.Execute;
begin
	mCanEdit := rCanEdit(r);
    DsTC.AutoEdit := mCanEdit;
    DsCT.AutoEdit := mCanEdit;
    DsCT2.AutoEdit := mCanEdit;

    mLCT := 'CHI2';
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
const
    FORM_CODE: String = 'PHIEU_CHI2';

procedure TFrmChi2.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    frNavi.DataSet := QrTC;
    frDate.Init;

    mFuncTK := sysFuncTK;
    mPTTT := RegReadString(Name, 'Pttt', sysPTTT);
    mMakho := RegReadString(Name, 'Makho', sysDefKho);

    mObsolete := False;
    mTriggerMaster := False;
    mTriggerDetail := False;
    fSQL := QrTC.SQL.Text;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.FormShow(Sender: TObject);
begin
    with DataMain do
        OpenDataSets([QrDMNCC, QrDMTK, QrDMTK_NB, QrNganhang, QrNganhangCN]);

    SetDisplayFormat(QrTC, sysCurFmt);
    SetShortDateFormat(QrTC);
    SetDisplayFormat(QrTC, ['NGAY'], DateTimeFmt);

    SetDisplayFormat(QrCHITIET, sysCurFmt);
    SetShortDateFormat(QrCHITIET);
    SetDisplayFormat(QrCHITIET, ['LK_NGAY'], DateTimeFmt);

    SetDisplayFormat(QrCHITIET2, sysCurFmt);
    SetShortDateFormat(QrCHITIET2);
    SetDisplayFormat(QrCHITIET2, ['LK_NGAY'], DateTimeFmt);

    SetDisplayFormat(QrTRAHANG2, sysCurFmt);
    SetShortDateFormat(QrTRAHANG2);
    SetDisplayFormat(QrTRAHANG2, ['NGAY'], DateTimeFmt);

    SetDisplayFormat(QrTRAHANGFB, sysCurFmt);
    SetShortDateFormat(QrTRAHANGFB);
    SetDisplayFormat(QrTRAHANGFB, ['NGAY'], DateTimeFmt);

    SetCustomGrid([FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_TH'], [GrBrowse, GrDetail, GrNhaptra]);
    SetDictionary([QrTC, QrCHITIET, QrCHITIET2], [FORM_CODE, FORM_CODE + '_CT', FORM_CODE + '_TH'], [Filter, nil, nil]);

    if not sysIsThue then
    begin
        grRemoveFields(GrDetail, ['LK_HOADON_SO', 'LK_HOADON_SERI', 'LK_HOADON_NGAY']);
    end;

    if not sysIsChecked then
    begin
        CmdChecked.Visible := False;
        SepChecked.Visible := False;

        GrBrowse.RemoveField('IMG2');
    end;

    if not sysIsDrc then
    begin
        PaMALOC.Visible := False;
        grRemoveFields(GrBrowse, 'MALOC;LK_TENLOC', ';');
    end;

    if not mFuncTK then
    begin
        PaTK.Visible := False;
        PaMaster.Height := PaMaster.Height - PaTK.Height;
        grRemoveFields(GrBrowse, 'MATK;LK_TENTK;LK_NGANHANG', ';');
    end;

    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.AdvEdKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
	CanClose := CheckBrowseDataSet(QrTC, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    HideAudit;
    RegWrite(Name, ['Makho', 'Pttt'], [mMakho, mPTTT]);
	try
	    CloseDataSets(DataMain.Conn);
    finally
    end;
    Action := caFree;
end;

    (*
    **  Page control
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.OpenDetail;
begin
    Screen.Cursor := crSQLWait;

    // Reopen
    with QrCHITIET do
    begin
        Close;
        Open;
    end;
    with QrCHITIET2 do
    begin
        Close;
        Open;
    end;
    Screen.Cursor := crDefault;
    ActiveSheet(PgDetail, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.PgMainChange(Sender: TObject);
begin
    if PgMain.ActivePageIndex = 1 then
    begin
        OpenDetail;
	    try
    	    CbNgay.SetFocus;
	    except
            GrDetail.SetFocus;
	   	end;
    end
	else
    	GrBrowse.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.PgMainChanging(Sender: TObject; var AllowChange: Boolean);
begin
	AllowChange := exCanChange(PgMain, QrTC);
end;

    (*
    **  Commands
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdRefreshExecute(Sender: TObject);
var
	s: String;
begin
    if (frDate.edFrom.Date <> fTungay) or
       (frDate.edTo.Date   <> fDenngay) or
       (VarToStr(frDate.cbbLoc.Value)   <> fLoc) then
    begin
		fTungay  := frDate.EdFrom.Date;
        fDenngay := frDate.EdTo.Date;
        fLoc     := VarToStr(frDate.cbbLoc.Value);

		Screen.Cursor := crSQLWait;
		with QrTC do
    	begin
        	s := Sort;
	    	Close;
            SQL.Text := fSQL;

            if not mObsolete then
                SQL.Add(' and isnull(DELETE_BY,0) = 0');
    	    SQL.Add(' order by NGAY desc, SCT desc');
    	    Open;
	        if s <> '' then
    	        Sort := s
        end;
        RefreshAudit;
	    AllowEditSomeFields;
        if PgMain.ActivePageIndex = 0 then
            GrBrowse.SetFocus;
		Screen.Cursor := crDefault;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdNewExecute(Sender: TObject);
begin
	QrTC.Append;
    ActiveSheet(PgMain, 1);
    ActiveSheet(PgDetail, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdSaveExecute(Sender: TObject);
begin
	{
    // Strongly confirm
    if not YesNo(RS_CONFIRM, 1) then
        Exit;
	}

//    CmdTotal.Execute;
//    if not Subtract then
//        Exit;

	QrCHITIET.CheckBrowseMode;
	QrCHITIET2.CheckBrowseMode;


    // Delete meanless rows
    with QrCHITIET do
    begin
        DisableControls;
        First;
        DeleteConfirm(False);
        while not Eof do
        begin
            if FieldByName('SoTien').AsFloat = 0.0 then
                Delete
            else
                Next;
        end;
        CheckBrowseMode;
        DeleteConfirm(True);

        First;
        EnableControls;
    end;

    // Saving
	QrTC.Post;

    QrCHITIET.UpdateBatch;
    QrCHITIET2.UpdateBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdCancelExecute(Sender: TObject);
begin
	QrTC.Cancel;
    QrCHITIET.CancelBatch;
    QrCHITIET2.CancelBatch;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdDelExecute(Sender: TObject);
var
	khoa: string;
begin
    with QrTC do
    begin
    	exValidClosing(FieldByName('NGAY').AsDateTime);
        if not YesNo(RS_CONFIRM1, 1) then
            Exit;
    end;

	khoa := TGuidEx.ToString(QrTC.FieldByName('KHOA'));
    DataMain.Conn.Execute(Format('delete THUCHI_CT where KHOA=%s', [QuotedStr(khoa)]));
    DataMain.Conn.Execute(Format('delete THUCHI_TRAHANG where KHOA=%s', [QuotedStr(khoa)]));
    DataMain.Conn.Execute(Format('update THUCHI set SoDu=0, SoTienTraHang=0, SoTien=0, ThanhToan=0 where KHOA=%s', [QuotedStr(khoa)]));

    mTrigger := True;
    MarkDataSet(QrTC);
    mTrigger := False;
    OpenDetail;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdEmptyExecute(Sender: TObject);
var
    n: Integer;
begin
    n := PgDetail.ActivePageIndex;
    if n = 1 then
        exEmptyDetails(QrCHITIET2, GrNhaptra)
    else
        exEmptyDetails(QrCHITIET, GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdPrintExecute(Sender: TObject);
begin
    CmdSave.Execute;
    ShowReport(Caption, FORM_CODE, [sysLogonUID, TGuidEx.ToStringEx(QrTC.FieldByName('KHOA'))]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdSwitchExecute(Sender: TObject);
begin
    if ActiveControl = GrDetail then
        try
            CbNgay.SetFocus;
        except
        end
    else
        GrDetail.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdTotalExecute(Sender: TObject);
begin
    mTriggerDetail := True;
    vlTotal2.Sum;
    vlTotal1.Sum;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdXemCongNoExecute(Sender: TObject);
begin
   if GetRights('SZ_DM_NCC_CONGNO') = R_DENY then
        Exit;

    with QrTC do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdFBChonCT2Execute(Sender: TObject);
var
	khoa: TGUID;
begin
    if BlankConfirm(QrTC, ['MADT']) then
        Exit;

    with QrTRAHANGFB do
    begin
        Close;
        Parameters[0].Value := QrTC.FieldByName('MADT').AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;
    end;

    if PhieuCTFB.Execute then
    begin
    	khoa := TGuidField(QrTRAHANGFB.FieldByName('KHOA')).AsGuid;

        with QrCHITIET2 do
        	if Locate('KHOANX', TGuidEx.ToString(khoa), []) then		// Duplicate
            	Exit
            else
            begin
		        SetEditState(QrTC);
	            Append;
    	        TGuidField(FieldByName('KHOANX')).AsGuid := khoa;
                FieldByName('LCT').AsString :=   QrTRAHANGFB.FieldByName('LCT').AsString;
        	    Post;
	        end;
    end;
    QrTRAHANGFB.Close;
end;

procedure TFrmChi2.CmdFBChonCTExecute(Sender: TObject);
var
	khoa: TGUID;
    procedure AppendDetail(pKhoa: TGUID);
    var
        mSodu: Double;
    begin
        with QrCHITIET do
            if Locate('KHOANX', TGuidEx.ToString(pkhoa), []) then		// Duplicate
            begin
                if FieldByName('SoTien').OldValue = null then
                    mSodu := 0
                else
                    mSodu := FieldByName('SoTien').OldValue;

                mSodu := mSodu + QrNXFB.FieldByName('SoDu').AsVariant;
                if (mSodu <> FieldByName('SoTien').AsFloat) and (mSodu <= FieldByName('SoDu').AsFloat) then
                begin
                    SetEditState(QrCHITIET);
                    FieldByName('SoTien').Value := mSodu;
                    Post;
                end;
            end
            else
            begin
                mSodu := QrNXFB.FieldByName('SoDu').AsVariant;
                Append;
                TGuidField(FieldByName('KHOANX')).AsGuid := pkhoa;
                FieldByName('NgayThanhToan').AsVariant := QrNXFB.FieldByName('NGAYTT').AsVariant;
                FieldByName('SoDu').AsVariant := mSodu;
                FieldByName('SoTien').AsVariant := mSodu;
                FieldByName('THANHTOAN_NX').AsFloat := FieldByName('LK_THANHTOAN').AsFloat;
                FieldByName('TC_SOTIEN_NX').AsFloat := FieldByName('LK_TC_SOTIEN').AsFloat;
                FieldByName('LCT').AsString := QrNXFB.FieldByName('LCT').AsString;
                Post;
            end;
    end;
begin
    if BlankConfirm(QrTC, ['MADT']) then
        Exit;

    Application.CreateForm(TFrmFB_ChonDsPN, FrmFB_ChonDsPN);
    if not FrmFB_ChonDsPN.Execute(QrTC.FieldByName('MADT').AsString) then
        Exit;

    with QrNXFB do
    begin
        Filter := 'SELECTED=1';
            Filtered := True;
            if RecordCount > 0 then
            begin
                SetEditState(QrTC);
                First;
                while not eof do
                begin
                    khoa := TGuidField(FieldByName('KHOA')).AsGuid;
                    AppendDetail(khoa);
                    Next;
                end;
            end;
            Filter := '';
    end;

    QrNXFB.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdFilterExecute(Sender: TObject);
begin
	Filter.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsTC)
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdReReadExecute(Sender: TObject);
begin
	fTungay := 0;
    CmdRefresh.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdClearFilterExecute(Sender: TObject);
begin
    with Filter do
    begin
        FieldInfo.Clear;
        ApplyFilter;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.ActionUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bBrowse, bDeleted, bInsert, bEmpty: Boolean;
    n: Integer;
begin
	with QrTC do
    begin
    	if not Active then
        	Exit;
		bBrowse := State in [dsBrowse];
        bInsert := State in [dsInsert];
        bEmpty := IsEmpty;
        bDeleted := FieldByName('DELETE_BY').AsInteger <> 0;
    end;
    n := PgMain.ActivePageIndex;

    CmdNew.Enabled := bBrowse and mCanEdit;
    CmdSave.Enabled := (not bBrowse);
    CmdCancel.Enabled := not bBrowse;

    CmdDel.Enabled := bBrowse and mCanEdit and (not bEmpty) and
        (n = 1) and not bDeleted and exCheckLoc(QrTC, False);
    CmdDel.Caption := GetMarkCaption(QrTC);
    CmdChecked.Enabled := bBrowse and mCanEdit and (not bEmpty) and (n = 1)
                        and exCheckLoc(QrTC, False);
    CmdChecked.Caption := exGetCheckedCaption(QrTC);

    CmdSwitch.Enabled := n = 1;
    CmdReRead.Enabled := bBrowse;
    CmdSearch.Enabled := bBrowse;
    CmdFilter.Enabled := bBrowse;
    CmdClearFilter.Enabled := bBrowse and (Filter.FieldInfo.Count > 0);
    CmdTotal.Enabled := (n = 1) and not bDeleted;

    CmdChonCT2.Enabled := mCanEdit and not bDeleted;
    CmdChonCT.Enabled := mCanEdit and not bDeleted;
    CmdEmpty.Enabled := mCanEdit;
    CmdFBChonCT.Enabled := mCanEdit and not bDeleted;
    CmdFBChonCT2.Enabled := mCanEdit and not bDeleted;

    if mFuncTK then
        PaTK.Enabled := QrTC.FieldByName('PTTT').AsString = '02'
end;

	(*
    ** Db events
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.OnDbError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**  Master
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCAfterInsert(DataSet: TDataSet);
begin
    AllowEditSomeFields;

	with QrTC do
    begin
       	TGuidEx.NewGuidDate(FieldByName('KHOA'));
		FieldByName('NGAY').AsDateTime  := Now;
		FieldByName('LCT').AsString     := mLCT;
        FieldByName('MAKHO').AsString   := mMakho;
        FieldByName('MALOC').AsString   := sysLoc;
        FieldByName('LOC').AsString     := sysLoc;
		FieldByName('PTTT').AsString    := mPTTT;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCBeforeOpen(DataSet: TDataSet);
begin
	with QrTC do
    begin
        Parameters[0].Value := mLCT;
		Parameters[1].Value := fTungay;
		Parameters[2].Value := fDenngay;
        Parameters[3].Value := fLoc;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCBeforePost(DataSet: TDataSet);
var
    pPTTT: string;
begin
	if mTrigger then
    	Exit;

    with DataSet do
    begin
		if BlankConfirm(DataSet, ['NGAY', 'MALOC', 'MADT']) then
	    	Abort;

        if mFuncTK and (FieldByName('PTTT').AsString = '02') then // Co DMTK & chon CK
            if BlankConfirm(QrTC, ['MATK']) then
	    	    Abort;

	    // Closed validate
    	exValidClosing(FieldByName('NGAY').AsDateTime);

    	if FieldByName('SoTien').AsFloat < 0 then
        begin
        	ErrMsg(RS_MSG);
            EdSOTIEN.SetFocus;
        	Abort;
        end;

        if FieldByName('SoTien').AsFloat > FieldByName('SoDu').AsFloat then
        begin
            ErrMsg(RS_MSG2);
            Abort;
        end;

        if FieldByName('PTTT').AsString = '02' then
            pPTTT := 'CHICK'
        else if FieldByName('PTTT').AsString = '01' then
            pPTTT := 'CHITM'
        else
            pPTTT := '';

	    SetNull(DataSet, ['PTTT', 'LyDo', 'MAKHO']);
    end;

    // Alloc receipt no.
    DataMain.AllocSCT(mLCT, QrTC);
    DataMain.AllocSCT2(pPTTT, QrTC);
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCBeforeEdit(DataSet: TDataSet);
begin
    exCheckLoc(QrTC);
	if mTrigger then
        Exit;

    exIsChecked(QrTC);

    if not mCanEdit then
        Abort;

    if QrTC.FieldByName('DELETE_BY').AsInteger <> 0 then
        Abort;
	exValidClosing(QrTC.FieldByName('NGAY').AsDateTime);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCCalcFields(DataSet: TDataSet);
begin
	{$I XCalc}
    {$I XCalc2}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCAfterCancel(DataSet: TDataSet);
begin
	if DataSet.Eof then
    	ActiveSheet(PgMain, 0);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCAfterScroll(DataSet: TDataSet);
begin
    PgMainChange(PgMain);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCAfterPost(DataSet: TDataSet);
begin
    with QrTC do
    begin
    	mMakho := FieldByName('MAKHO').AsString;
        mPTTT := FieldByName('PTTT').AsString;
    end;
    AllowEditSomeFields;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCMADTChange(Sender: TField);
begin
    OpenDetail;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCMADTValidate(Sender: TField);
begin
    if QrTC.State in [dsInsert] then
    begin
        if not QrCHITIET2.IsEmpty then
            Abort;
    end
    else
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCMATKChange(Sender: TField);
begin
    EdChinhanh.Text := EdChinhanh.Field.AsString;
    EdNganhang.Text := EdNganhang.Field.AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCNGAYValidate(Sender: TField);
begin
//    if IsLicenseExpired(Sender.AsDateTime) then
//        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCPTTTChange(Sender: TField);
begin
    if mFuncTK and (Sender.AsString <> '02') then // Co danh muc Tai khoan & Chon <> CK
    begin
        QrTC.FieldByName('MATK').Clear;
        EdChinhanh.Text := EdChinhanh.Field.AsString;
        EdNganhang.Text := EdNganhang.Field.AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrTCSOTIENChange(Sender: TField);
var
    bTrigger: Boolean;
begin
    if mTriggerMaster then
        Exit;

    with QrTC do
    begin
        if (Sender.FieldName = 'SoTien') and (not mTriggerDetail) then
        begin
            mTriggerMaster := True;
            Subtract(Sender.AsFloat)
        end;

        if mTrigger then
            Exit;

        bTrigger := mTrigger;
        mTrigger := True;
        if Sender.FieldName = 'ThanhToan' then
        begin
            FieldByName('SoTien').AsFloat :=
                FieldByName('ThanhToan').AsFloat +
                FieldByName('SoTienTraHang').AsFloat
        end else
        begin
            FieldByName('ThanhToan').AsFloat :=
                FieldByName('SoTien').AsFloat -
                FieldByName('SoTienTraHang').AsFloat;
        end;
        mTrigger := bTrigger;

        mTriggerMaster := False;
    end;

    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETBeforeOpen(DataSet: TDataSet);
var
    khoa: TGUID;
begin
    with QrNX do
        if Active then
            Close;

    khoa := TGuidField(QrTC.FieldByName('KHOA')).AsGuid;
    with QrCHITIET do
        Parameters[0].Value := TGuidEx.ToString(khoa);

    with QrNX do
    begin
        Parameters.ParamByName('@pKHOA').Value := TGuidEx.ToString(khoa);
        Parameters.ParamByName('@pMADT').Value := QrTC.FieldByName('MADT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETBeforePost(DataSet: TDataSet);
begin
    with DataSet do
        if State in [dsInsert] then
        begin
            FieldByName('KHOA').Value := QrTC.FieldByName('KHOA').Value;
            TGuidEx.NewGuidDate(FieldByName('KHOACT'));
            FieldByName('LOC').AsString := sysLoc;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETBeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

    if mTrigger then
        Exit;

    if not DeleteConfirm then
        Abort;

    SetEditState(QrTC);
    mTriggerDetail := True;
    vlTotal1.Keep;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETBeforeEdit(DataSet: TDataSet);
begin
    if mTrigger then
    	Exit;
    SetEditState(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETBeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETAfterCancel(DataSet: TDataSet);
begin
    mTriggerDetail := True;
    vlTotal1.Reset;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETAfterDelete(DataSet: TDataSet);
begin
    mTriggerDetail := True;
    vlTotal1.Update(True);
    mTriggerDetail := False;
    GrDetailUpdateFooter(GrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETAfterEdit(DataSet: TDataSet);
begin
    mTriggerDetail := True;
    vlTotal1.Keep;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETCalcFields(DataSet: TDataSet);
begin
    with QrCHITIET do
    begin
        FieldByName('CALC_STT').AsInteger := Abs(RecNo);
        FieldByName('CALC_CONLAI').AsFloat :=
            FieldByName('SoDu').AsFloat -
            FieldByName('SoTien').AsFloat;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETSODUChange(Sender: TField);
begin
    mTriggerDetail := True;
    vlTotal1.Update;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETSOTIENChange(Sender: TField);
begin
    if mTriggerMaster then
        Exit;

    mTriggerDetail := True;
    vlTotal1.Update;
    mTriggerDetail := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIETSOTIENValidate(Sender: TField);
begin
        with QrCHITIET do
        if FieldByName('SoTien').AsFloat > FieldByName('SoDu').AsFloat then
        begin
            InvalidMsg(Sender);
            Abort;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrNX2AfterOpen(DataSet: TDataSet);
begin
    SetDisplayFormat(QrNX2, sysCurFmt);
    SetShortDateFormat(QrNX2);
end;

    (*
    **  Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
   	Status.SimpleText := exRecordCount(QrTC, Filter);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.BtCongnoClick(Sender: TObject);
begin
    if GetRights('SZ_CONGNO_NCC') = R_DENY then
        Exit;

    with QrTC do
    begin
        Application.CreateForm(TFrmCongnoNCC, FrmCongnoNCC);
        FrmCongnoNCC.Execute(FieldByName('NGAY').AsDateTime,
                    FieldByName('MADT').AsString);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.GrBrowseCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrTC do
    begin
        if FieldByName('DELETE_BY').AsInteger <> 0 then
        begin
            AFont.Color := clGray;
            Exit;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.GrBrowseDblClick(Sender: TObject);
begin
    if QrTC.IsEmpty then
    	Exit;
	ActiveSheet(PgMain, 1);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.GrDetailUpdateFooter(Sender: TObject);
begin
    with GrDetail, QrTC do
    begin
        ColumnByName('SoDu').FooterValue := FormatFloat(ctCurFmt, FieldByName('SoDu').AsFloat);
        ColumnByName('SoTien').FooterValue := FormatFloat(ctCurFmt, FieldByName('SoTien').AsFloat);
        ColumnByName('CALC_CONLAI').FooterValue := FormatFloat(ctCurFmt, FieldByName('CALC_CONLAI').AsFloat);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.ItemObsoleteClick(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReRead.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmChi2.Subtract;
var
    _sotien, _sodu, _thanhtoan: Double;
begin
    Result := _pSotien > 0.0;

    if not Result then
    begin
        ErrMsg(RS_ERROR);
        Exit;
    end;

    _sotien := _pSotien;
    with QrCHITIET do
    begin
        if not IsEmpty then
        begin
            First;
            while not Eof do
            begin
                _sodu := FieldByName('SoDu').AsFloat;
                if (_sodu <= _sotien) then
                    _thanhtoan := _sodu
                else
                    _thanhtoan := _sotien;

                Edit;
                FieldByName('SoTien').AsFloat := _thanhtoan;

                _sotien := _sotien - _thanhtoan;

                Next;
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdAuditExecute(Sender: TObject);
begin
    ShowAudit(DataMain.QrUSER, DsTC, DataMain.QrLOC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    DOT_TRAHANG =
        'KHOA'#9'10'#9''#13 +
        'NGAY'#9'10'#9'Ngày'#13 +
        'SCT'#9'18'#9'Số phiếu'#13 +
        'ThanhToan'#9'13'#9'Số tiền'#13 +
        'GhiChu'#9'40'#9'Diễn giải';

procedure TFrmChi2.CmdCheckedExecute(Sender: TObject);
begin
    exChecked(QrTC);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdChonCT2Execute(Sender: TObject);
var
	khoa: TGUID;
begin
    if BlankConfirm(QrTC, ['MADT']) then
        Exit;

    with QrTRAHANG2 do
    begin
        Close;
        Parameters[0].Value := QrTC.FieldByName('MADT').AsString;
        Open;
        if IsEmpty then
        begin
            Close;
            Exit;
        end;
    end;

    if PhieuCT2.Execute then
    begin
    	khoa := TGuidField(QrTRAHANG2.FieldByName('KHOA')).AsGuid;

        with QrCHITIET2 do
        	if Locate('KHOANX', TGuidEx.ToString(khoa), []) then		// Duplicate
            	Exit
            else
            begin
		        SetEditState(QrTC);
	            Append;
    	        TGuidField(FieldByName('KHOANX')).AsGuid := khoa;
                FieldByName('LCT').AsString :=   QrTRAHANG2.FieldByName('LCT').AsString;
        	    Post;
	        end;
    end;
    QrTRAHANG2.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.CmdChonCTExecute(Sender: TObject);
var
	khoa: TGUID;
    procedure AppendDetail(pKhoa: TGUID);
    var
        mSodu: Double;
    begin
        with QrCHITIET do
            if Locate('KHOANX', TGuidEx.ToString(pkhoa), []) then		// Duplicate
            begin
                if FieldByName('SoTien').OldValue = null then
                    mSodu := 0
                else
                    mSodu := FieldByName('SoTien').OldValue;

                mSodu := mSodu + QrNX2.FieldByName('SoDu').AsVariant;
                if (mSodu <> FieldByName('SoTien').AsFloat) and (mSodu <= FieldByName('SoDu').AsFloat) then
                begin
                    SetEditState(QrCHITIET);
                    FieldByName('SoTien').Value := mSodu;
                    Post;
                end;
            end
            else
            begin
                mSodu := QrNX2.FieldByName('SoDu').AsVariant;
                Append;
                TGuidField(FieldByName('KHOANX')).AsGuid := pkhoa;
                FieldByName('NgayThanhToan').AsVariant := QrNX2.FieldByName('NgayThanhToan').AsVariant;
                FieldByName('SoDu').AsVariant := mSodu;
                FieldByName('SoTien').AsVariant := mSodu;
                FieldByName('THANHTOAN_NX').AsFloat := FieldByName('LK_THANHTOAN').AsFloat;
                FieldByName('TC_SOTIEN_NX').AsFloat := FieldByName('LK_TC_SOTIEN').AsFloat;
                FieldByName('LCT').AsString := QrNX2.FieldByName('LCT').AsString;
                Post;
            end;
    end;
begin
    if BlankConfirm(QrTC, ['MADT']) then
        Exit;

    Application.CreateForm(TFrmChonDsPN, FrmChonDsPN);
    if not FrmChonDsPN.Execute(QrTC.FieldByName('MADT').AsString) then
        Exit;

    with QrNX2 do
    begin
        Filter := 'SELECTED=1';
            Filtered := True;
            if RecordCount > 0 then
            begin
                SetEditState(QrTC);
                First;
                while not eof do
                begin
                    khoa := TGuidField(FieldByName('KHOA')).AsGuid;
                    AppendDetail(khoa);
                    Next;
                end;
            end;
            Filter := '';
    end;

    QrNX2.Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2CalcFields(DataSet: TDataSet);
begin
    with DataSet do
        FieldByName('CALC_STT').AsInteger := Abs(RecNo);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2KHOANXChange(Sender: TField);
begin
    with QrCHITIET2 do
        FieldByName('SoTien').AsFloat := FieldByName('LK_THANHTOAN').AsFloat;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2SOTIENChange(Sender: TField);
begin
    vlTotal2.Update;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2BeforeOpen(DataSet: TDataSet);
var
    khoa: TGUID;
begin
    with QrTRAHANG do
        if Active then
            Close;

    khoa := TGuidField(QrTC.FieldByName('KHOA')).AsGuid;
    with QrCHITIET2 do
        Parameters[0].Value := TGuidEx.ToString(khoa);

    with QrTRAHANG do
    begin
        Parameters.ParamByName('@pKHOA').Value := TGuidEx.ToString(khoa);
        Parameters.ParamByName('@pMADT').Value := QrTC.FieldByName('MADT').AsString;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2AfterCancel(DataSet: TDataSet);
begin
    vlTotal2.Reset;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2AfterDelete(DataSet: TDataSet);
begin
    vlTotal2.Update(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2AfterEdit(DataSet: TDataSet);
begin
    vlTotal2.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2BeforeDelete(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;

	if mTrigger then
    	Exit;

//    if not (QrTC.State in [dsInsert]) then
//        Abort;

    if not DeleteConfirm then
        Abort;

    SetEditState(QrTC);
    vlTotal2.Keep;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.QrCHITIET2BeforeInsert(DataSet: TDataSet);
begin
    if not mCanEdit then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.PhieuCT2InitDialog(Dialog: TwwLookupDlg);
begin
    InitSearchDialog(Dialog);
    Dialog.wwIncrementalSearch1.SetSearchField('SCT');
    with Dialog.wwDBGrid1 do
    begin
        KeyOptions := KeyOptions - [dgAllowInsert, dgAllowDelete];
        EditControlOptions := EditControlOptions + [ecoCheckboxSingleClick];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.PopMasterPopup(Sender: TObject);
begin
    ItemObsolete.Checked := mObsolete;
end;

(*==============================================================================
** Chi cho chinh mot so fields
**------------------------------------------------------------------------------
*)
procedure TFrmChi2.AllowEditSomeFields;
var
    b: Boolean;
begin
	with QrTC do
    begin
    	b := not (State in [dsInsert]);

        with CbbNhaCungCap do
        begin
        	ReadOnly := b;
            TabStop := not b;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
end.

