﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DmKhNccDauKy;

interface

uses
  SysUtils, Classes, Controls, Forms, Variants,
  ComCtrls, ActnList, ExtCtrls, StdCtrls,
  Db, Wwdbgrid2, ADODb,
  AppEvnts, ToolWin, Graphics, Grids, Wwdbigrd, Wwdbgrid, wwdblook, Vcl.Menus,
  AdvMenus;

type
  TFrmDmKhNccDauKy = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdSave: TAction;
    CmdCancel: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton3: TToolButton;
    ToolButton4: TToolButton;
    ToolButton5: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton11: TToolButton;
    DsDetail: TDataSource;
    Panel2: TPanel;
    GrList: TwwDBGrid2;
    ApplicationEvents1: TApplicationEvents;
    Status: TStatusBar;
    Panel1: TPanel;
    Label4: TLabel;
    LbMA: TLabel;
    Label5: TLabel;
    LbTEN: TLabel;
    QrDetail: TADOQuery;
    Image1: TImage;
    CmdAudit: TAction;
    CmdDmvtNPL: TAction;
    ToolButton8: TToolButton;
    CmdImportExcel: TAction;
    QrDetailNGAY_DK_KH: TDateTimeField;
    QrDetailSOTIEN_DK_KH: TFloatField;
    QrDetailNGAY_DK_NCC: TDateTimeField;
    QrDetailSOTIEN_DK_NCC: TFloatField;
    QrDetailGHICHU: TWideStringField;
    QrDetailMADT: TWideStringField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdSaveExecute(Sender: TObject);
    procedure CmdCancelExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDetailBeforePost(DataSet: TDataSet);
    procedure QrDetailBeforeDelete(DataSet: TDataSet);
    procedure QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure QrDetailAfterOpen(DataSet: TDataSet);
    procedure QrDetailBeforeOpen(DataSet: TDataSet);
    procedure FormCreate(Sender: TObject);
    procedure CmdAuditExecute(Sender: TObject);
    procedure QrDetailAfterDelete(DataSet: TDataSet);
  private
    mCanEdit: Boolean;
    mMADT: String;
  	mRet: Boolean;
    mLoai: Integer;
  public
  	function Execute(pCanEdit: Boolean; pMa, pTen: String; pLoai: Integer): Boolean;
  end;

var
  FrmDmKhNccDauKy: TFrmDmKhNccDauKy;

implementation

uses
	MainData, isDb, isMsg, ExCommon, isLib, isCommon, Rights,
    ImportExcel, OfficeData, isFile;

{$R *.DFM}

const
	FORM_CODE = 'DM_KH_NCC_DK';
    TABLE_NAME  = 'DM_KH_NCC_DK';
    REPORT_NAME = FORM_CODE;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmDmKhNccDauKy.Execute(pCanEdit: Boolean; pMa, pTen: String; pLoai: Integer): Boolean;
begin
    mCanEdit := pCanEdit;
    GrList.ReadOnly := not mCanEdit;
    mMADT := pMa;
    mLoai := pLoai;
    LbMA.Caption := pMa;
    LbTEN.Caption := pTen;
    ShowModal;
    Result := mRet;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdNewExecute(Sender: TObject);
begin
	QrDetail.Append;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdSaveExecute(Sender: TObject);
begin
	QrDetail.Post;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdCancelExecute(Sender: TObject);
begin
	QrDetail.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdDelExecute(Sender: TObject);
begin
	QrDetail.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
begin
	exActionUpdate(ActionList, QrDetail, mCanEdit);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    try
	    CloseDataSets([QrDetail]);
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    AddAllFields(QrDetail, TABLE_NAME);
    mRet := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.FormShow(Sender: TObject);
begin
    OpenDataSets([QrDetail]);
    with QrDetail do
    begin
	    SetDisplayFormat(QrDetail, sysCurFmt);
    	SetShortDateFormat(QrDetail);
    end;

//    Msg(IntToStr(mLoai));

    SetCustomGrid(FORM_CODE, GrList);
    GrList.SetFocus;

    if mLoai = 1 then
    begin
        grRemoveFields(GrList, ['NGAY_DK_KH', 'SOTIEN_DK_KH']);
    end;

    if mLoai = 0 then
    begin
        grRemoveFields(GrList, ['NGAY_DK_NCC', 'SOTIEN_DK_NCC']);
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDetail, CmdSave, CmdCancel);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailBeforePost(DataSet: TDataSet);
begin
	with QrDetail do
	begin
		FieldByName('MADT').AsString := mMADT;
	end;
    SetAudit(DataSet);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDetail);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailAfterDelete(DataSet: TDataSet);
begin
    mRet := True;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailAfterOpen(DataSet: TDataSet);
begin
    QrDetail.Last;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.QrDetailBeforeOpen(DataSet: TDataSet);
begin
	QrDetail.Parameters[0].Value := mMADT;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDmKhNccDauKy.CmdAuditExecute(Sender: TObject);
begin
	ShowAudit(DataMain.QrUSER, DsDetail);
end;

end.
