(*==============================================================================
**------------------------------------------------------------------------------
*)
unit AddList;

interface

uses
  Classes,Controls, Forms, Buttons, ComCtrls, Db, ADODB, ActnList, StdCtrls;

type
  TFrmAddList = class(TForm)
    List1: TListView;
    List2: TListView;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    QrUser: TADOQuery;
    QrGroup: TADOQuery;
    BtnOK: TBitBtn;
    BitBtn2: TBitBtn;
    ActionList1: TActionList;
    CmdAdd: TAction;
    CmdRem: TAction;
    EdSearch: TEdit;
    CmdSearch: TAction;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure CmdAddExecute(Sender: TObject);
    procedure CmdRemExecute(Sender: TObject);
    procedure List2DragDrop(Sender, Source: TObject; X, Y: Integer);
    procedure List2DragOver(Sender, Source: TObject; X, Y: Integer;
      State: TDragState; var Accept: Boolean);
    procedure FormCreate(Sender: TObject);
    procedure EdSearchKeyPress(Sender: TObject; var Key: Char);
    procedure CmdSearchExecute(Sender: TObject);
  private
    it: TListItem;
    procedure SearchText;
  public
  	function  GetList(pUID: Integer; pGroup: Boolean = False): TStrings;
  end;

var
  FrmAddList: TFrmAddList;

implementation

uses
	ExAdminCommon, isLib, isCommon, AdminData, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmAddList.GetList(pUID: Integer; pGroup: Boolean): TStrings;
var
	i: Integer;
    s: String;
  	mQuery: TADOQuery;
begin
	if pGroup then
		mQuery := QrGroup
	else
    	mQuery := QrUser;

    // List prepare
    with mQuery do
    begin
        Close;
        Parameters[0].Value := sysAdminId;
        Parameters[1].Value := sysEveryoneId;
        Parameters[2].Value := pUID;
    	Open;
        while not Eof do
        begin
	        it := List1.Items.Add;
            s := FieldByName('USERNAME').AsString;
    	    it.Caption := s;

            if FieldByName('IS_GROUP').AsBoolean then
	            it.ImageIndex := 1
            else
	            it.ImageIndex := 0;

            it.SubItems.Add(FieldByName('FULLNAME').AsString);
            it.SubItems.Add(FieldByName('UID').AsString);
            Next;
        end;
        Close;
    end;

    // Return value
	Result := TStringList.Create;
    if ShowModal = mrOK then
		with List2.Items do
        	for i := 0 to Count - 1 do
            begin
            	s := Item[i].SubItems[1];
                Result.Add(s);
            end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.FormCreate(Sender: TObject);
begin
{$warnings off}
    TMyForm(Self).Init;
{$warnings on} 
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.FormShow(Sender: TObject);
begin
	EdSearch.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.CmdAddExecute(Sender: TObject);
var
	i: Integer;
begin
	with List1 do
    begin
    	if SelCount <= 0 then
	    	Exit;

        for i := 0 to Items.Count - 1 do
            if Items.Item[i].Selected then
            begin
		        it := List2.Items.Add;
                it.Assign(Items.Item[i]);
	        end;

        i := 0;
        while i < Items.Count do
        begin
            if Items.Item[i].Selected then
            	Items.Delete(i)
            else
            	Inc(i);
        end;
    end;
    List2.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.CmdRemExecute(Sender: TObject);
var
	i: Integer;
begin
	with List2 do
    begin
    	if SelCount <= 0 then
	    	Exit;

        for i := 0 to Items.Count - 1 do
            if Items.Item[i].Selected then
            begin
		        it := List1.Items.Add;
                it.Assign(Items.Item[i]);
	        end;

        i := 0;
        while i < Items.Count do
        begin
            if Items.Item[i].Selected then
            	Items.Delete(i)
            else
            	Inc(i);
        end;
    end;
    List1.SetFocus;
end;

procedure TFrmAddList.CmdSearchExecute(Sender: TObject);
begin
    EdSearch.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.EdSearchKeyPress(Sender: TObject; var Key: Char);
begin
    if Key = #13 then
        SearchText;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.List2DragDrop(Sender, Source: TObject; X,
  Y: Integer);
begin
    if (Source as TComponent).Name = List1.Name then
        CmdAdd.Execute
    else
        CmdRem.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmAddList.List2DragOver(Sender, Source: TObject; X, Y: Integer;
  State: TDragState; var Accept: Boolean);
begin
    Accept := (Sender as TComponent).Name <> (Source as TComponent).Name;
end;

procedure TFrmAddList.SearchText;
var
    s: String;
    it: TListItem;
begin
    s := EdSearch.Text;
    it := List1.FindCaption(EdSearch.Tag+1, s, True, True, True);
    if it = nil then
        EdSearch.Tag := 0
    else
    begin
        EdSearch.Tag := it.Index;
        List1.ClearSelection;
        List1.Selected := it;
        List1.Scroll(0, it.GetPosition.Y);
    end;
end;

end.
