﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Users;

interface

uses
  Windows, Classes, Controls, Forms, ComCtrls, ExtCtrls,
  StdCtrls, Buttons, Mask, DBCtrls, ActnList, ADODb, Wwdbigrd,
  Wwdbgrid, Db, wwDBGrid2, SysUtils, Grids, wwdbedit, rDBComponents;

type
  TFrmUser = class(TForm)
    ActionList: TActionList;
    BtnOK: TBitBtn;
    BitBtn2: TBitBtn;
    CmdOK: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    EdName: TwwDBEdit;
    EdPassword: TEdit;
    EdConfirmPas: TEdit;
    EdUser: TwwDBEdit;
    GrList: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    QrList: TADOQuery;
    QrListIDX: TIntegerField;
    DsList: TDataSource;
    CmdRemove: TAction;
    CmdAdd: TAction;
    QrListDESC: TWideStringField;
    ChkDisable: TDBCheckBox;
    QrListIS_GROUP: TBooleanField;
    QrListGROUP: TIntegerField;
    QrListMEMBER: TIntegerField;
    QrListGROUP_NAME: TWideStringField;
    Label5: TLabel;
    EdBrief: TwwDBEdit;
    Label6: TLabel;
    EdEmail: TwwDBEdit;
    ChkPwdNerver: TDBCheckBox;
    ChkMustChgPwd: TDBCheckBox;
    ChkIsEmpoyee: TrDBCheckBox;
    procedure FormShow(Sender: TObject);
    procedure CmdOKExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure CmdRemoveExecute(Sender: TObject);
    procedure CmdAddExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
    procedure EdPasswordChange(Sender: TObject);
  private
    mIsNew, mIsEncode: Boolean;

    function  IsValidInput: Boolean;
    procedure SetFakePass(const FakeText: String = '');
  public
    procedure Execute(mNew: Boolean; mEncode: Boolean = True);
  end;

var
  FrmUser: TFrmUser;

implementation

uses
	isMsg, AdminData, islib, AddList, ExAdminCommon, isDb, isEnCoding, MainData,
    isType, ListUser;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_NEW		= 'Thêm';
    RS_ACCEPT	= 'Đồng ý';

procedure TFrmUser.Execute;
var
    s: String;
begin
	mIsNew := mNew;
    mIsEncode := mEncode;

	if mIsNew then
    	CmdOK.Caption := RS_NEW
    else
    	CmdOK.Caption := RS_ACCEPT;

    s := FrmListUser.QrDanhmuc.FieldByName('USERNAME').AsString;
    PgMain.Pages[1].TabVisible := not mIsNew and
        (s <> IS_USER_ADMIN) and
        (s <> IS_GROUP_EVERYONE);

    with EdUser do
    begin
    	ReadOnly := not mIsNew;
        TabStop  := mIsNew;
    end;

    ChkDisable.Visible := mIsNew or (s <> IS_USER_ADMIN);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.SetFakePass(const FakeText: String);
begin
    with EdPassword do
    begin
        Tag := 1;   // Skip event OnChange
        Text := FakeText;
        Tag := 0;
        EdConfirmPas.Text := Text;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.FormShow(Sender: TObject);
begin
	if mIsNew then
    begin
        with FrmListUser.QrDanhmuc do
        begin
            Append;
            FieldByName('IS_GROUP').AsBoolean := False;
            if mIsEncode then
                FieldByName('PASSWD').AsString := isEncodeMD5('');
        end;
		SetFakePass;
        EdUser.SetFocus;
	end
    else
    begin
    	FrmListUser.QrDanhmuc.Edit;
        QrList.Open;
        SetFakePass('!@#$%^&*()');
        EdName.SetFocus;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FrmListUser.QrDanhmuc.Cancel;
	CloseDataSets([QrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_RETYPE = 'Mật khẩu lặp lại không đúng.';

procedure TFrmUser.CmdOKExecute(Sender: TObject);
begin
    if EdConfirmPas.Text <> EdPassword.Text Then
    begin
    	Msg(RS_RETYPE);
        Exit;
    end;

    // Post user info. and list of members
    FrmListUser.QrDanhmuc.CheckBrowseMode;
    with QrList do
        if Active then
        begin
            CheckBrowseMode;
            UpdateBatch;
        end;

	if mIsNew then
	begin
    	with FrmListUser.QrDanhmuc do
        begin
            // Prepare for continuing Create User
            Append;
            FieldByName('IS_GROUP').AsBoolean := False;
            if mIsEncode then
                FieldByName('PASSWD').AsString := isEncodeMD5('');
        end;
		SetFakePass;
        EdUser.SetFocus;
    end
	else
    	ModalResult := mrOK;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.CmdRemoveExecute(Sender: TObject);
var
    i: Integer;
begin
    with GrList.SelectedList, QrList do
        for i := 0 to Count - 1 do
        begin
            try
                GotoBookmark(TBookmark(Items[i]));
//                if FieldByName('GROUP').AsInteger = sysAdminId then
                    Delete;
            except
            end;
        end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.CmdAddExecute(Sender: TObject);
var
    mUID, i: Integer;
    s: String;
begin
    mUID := FrmListUser.QrDanhmuc.FieldByName('UID').AsInteger;
    Application.CreateForm(TFrmAddList, FrmAddList);
    with FrmAddList.GetList(mUID) do
    begin
        for i := 0 to Count - 1 do
        begin
            s := Strings[i];
            with QrList do
                if not Locate('GROUP', s, []) then
                begin
                    begin
                        Append;
                        FieldByName('MEMBER').AsInteger := mUID;
                        FieldByName('GROUP').AsString := s;
                        Post;
                    end;
                end;
        end;
        Free;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmUser.IsValidInput: Boolean;
begin
	Result := (Trim(EdUser.Text) <> '') and (Trim(EdName.Text) <> '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    n: Integer;
begin
	CmdOK.Enabled := IsValidInput;
    n := PgMain.ActivePageIndex;
    CmdAdd.Enabled := n = 1;

	with QrList do
    begin
    	if not Active then
	    	Exit;
    	CmdRemove.Enabled := (not IsEmpty) and (n = 1);
	end;        
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.QrListBeforeOpen(DataSet: TDataSet);
begin
    QrList.Parameters[0].Value :=
        FrmListUser.QrDanhmuc.FieldByName('UID').AsInteger;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.QrListCalcFields(DataSet: TDataSet);
begin
	with DataSet do
    	if FieldByName('IS_GROUP').AsBoolean then
			FieldByName('IDX').AsInteger := 59
        else if FieldByName('GROUP').AsInteger = sysAdminId then
		    	FieldByName('IDX').AsInteger := 9
        	else
	    		FieldByName('IDX').AsInteger := 8
end;
        
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := IsValidInput
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmUser.EdPasswordChange(Sender: TObject);
begin
    if EdPassword.Tag <> 0  then    // Trigger for skipping event
        Exit;

    SetEditState(FrmListUser.QrDanhmuc);
    with FrmListUser.QrDanhmuc do
    begin
        if mIsEncode then
            FieldByName('PASSWD').AsString := isEncodeMD5(EdPassword.Text)
        else
            FieldByName('PASSWD').AsString := EdPassword.Text;
    end;
end;

end.
