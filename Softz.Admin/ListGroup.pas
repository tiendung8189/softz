(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ListGroup;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus, AdvMenus,
  AppEvnts, ToolWin, Wwdbigrd, Wwdbgrid;

type
  TFrmListGroup = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdSearch: TAction;
    ApplicationEvents1: TApplicationEvents;
    QrDanhmucUSERNAME: TWideStringField;
    QrDanhmucFULLNAME: TWideStringField;
    QrDanhmucBRIEFNAME: TWideStringField;
    QrDanhmucEMAIL: TWideStringField;
    QrDanhmucPASSWD: TWideStringField;
    QrDanhmucIS_GROUP: TBooleanField;
    QrDanhmucDISABLED: TBooleanField;
    QrDanhmucMUST_CHG_PWD: TBooleanField;
    QrDanhmucPWD_NEVER: TBooleanField;
    QrDanhmucLAST_PWD_CHG: TDateTimeField;
    CmdEdit: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    QrDanhmucUID: TIntegerField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
  private
  	mCanEdit: Boolean;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmListGroup: TFrmListGroup;

implementation

uses
	MainData, AdminData, isDb, isMsg, ExAdminCommon, Rights, isLib, isCommon,
    Groups, isType;

{$R *.DFM}

const
	FORM_CODE = 'LIST_GROUP';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init1;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE);

    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
	QrDanhmuc.Open;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.CmdNewExecute(Sender: TObject);
begin
	Application.CreateForm(TFrmGroup, FrmGroup);
    FrmGroup.Execute(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.CmdDelExecute(Sender: TObject);
var
    n: Integer;
begin
    QrDanhmuc.Delete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.CmdEditExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmGroup, FrmGroup);
    FrmGroup.Execute(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bReserve: Boolean;
	s: String;
begin
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		s := FieldByName('USERNAME').AsString;
    end;

	bReserve := (s = IS_USER_ADMIN) or (s = IS_GROUP_EVERYONE);
    CmdDel.Enabled := not bReserve;
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
        if BlankConfirm(QrDanhmuc, ['USERNAME', 'FULLNAME']) then
    		Abort;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('UID').AsInteger := NewUID;
        FieldByName('DISABLED').AsBoolean := False;
        FieldByName('MUST_CHG_PWD').AsBoolean := False;
        FieldByName('PWD_NEVER').AsBoolean := True;
        FieldByName('Is_Employee').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrDanhmuc do
    begin
        if FieldByName('USERNAME').AsString = IS_GROUP_EVERYONE then
            AFont.Style := [fsBold];
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListGroup.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

end.
