﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit Groups;

interface

uses
  Classes, Controls, Forms, ComCtrls, ExtCtrls, StdCtrls, Buttons,
  DBCtrls, ActnList, ADODb, Wwdbigrd, Wwdbgrid, Db, Mask,
  wwDBGrid2, SysUtils, Grids, wwdbedit;

type
  TFrmGroup = class(TForm)
    ActionList: TActionList;
    BtnOK: TBitBtn;
    BitBtn2: TBitBtn;
    CmdOK: TAction;
    PgMain: TPageControl;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    Panel1: TPanel;
    Label1: TLabel;
    Label2: TLabel;
    EdName: TwwDBEdit;
    EdUser: TwwDBEdit;
    GrList: TwwDBGrid2;
    BitBtn1: TBitBtn;
    BitBtn3: TBitBtn;
    QrList: TADOQuery;
    DsList: TDataSource;
    QrListIDX: TIntegerField;
    CmdAdd: TAction;
    CmdRemove: TAction;
    QrListDESC: TWideStringField;
    QrListGROUP: TIntegerField;
    QrListIS_GROUP: TBooleanField;
    QrListMEMBER: TIntegerField;
    QrListMEMBER_NAME: TWideStringField;
    procedure FormShow(Sender: TObject);
    procedure CmdOKExecute(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: Char);
    procedure FormCreate(Sender: TObject);
    procedure QrListCalcFields(DataSet: TDataSet);
    procedure CmdRemoveExecute(Sender: TObject);
    procedure CmdAddExecute(Sender: TObject);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure QrListBeforeOpen(DataSet: TDataSet);
    procedure PgMainChanging(Sender: TObject; var AllowChange: Boolean);
  private
    mIsNew : Boolean;

    function  IsValidInput: Boolean;
  public
    procedure Execute(mNew : Boolean);
  end;

var
  FrmGroup: TFrmGroup;

implementation

uses
	AdminData, isLib, AddList, ExAdminCommon, isDb, isType, ListGroup, MainData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_NEWGROUP		= 'Thêm';
    RS_ACCEPTGROUP	= 'Đồng ý';

procedure TFrmGroup.Execute(mNew : Boolean);
var
    s: String;
begin
	mIsNew := mNew;
	if mIsNew then
    	CmdOK.Caption := RS_NEWGROUP
    else
    	CmdOK.Caption := RS_ACCEPTGROUP;

    s := FrmListGroup.QrDanhmuc.FieldByName('USERNAME').AsString;
    PgMain.Pages[1].TabVisible := not mIsNew and
        (s <> IS_USER_ADMIN) and
        (s <> IS_GROUP_EVERYONE);

    with EdUser do
    begin
    	ReadOnly := not mIsNew;
        TabStop  := mIsNew;
    end;

    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.FormShow(Sender: TObject);
begin
	if mIsNew then
    begin
		with FrmListGroup.QrDanhmuc do
        begin
            Append;
            FieldByName('IS_GROUP').AsBoolean := True;
        end;

        EdUser.SetFocus;
	end
    else
    begin
    	FrmListGroup.QrDanhmuc.Edit;
        EdName.SetFocus;
        QrList.Open;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	FrmListGroup.QrDanhmuc.Cancel;
	CloseDataSets([QrList]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.CmdRemoveExecute(Sender: TObject);
var
    i: Integer;
begin
    with GrList.SelectedList, QrList do
    begin
        for i := 0 to Count - 1 do
        begin
            try
                GotoBookmark(TBookmark(Items[i]));
                if FieldByName('GROUP').AsString <> IS_GROUP_EVERYONE then
                    Delete;
            except
            end;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.CmdAddExecute(Sender: TObject);
var
    i, mUID: Integer;
    s: String;
begin
    mUID := FrmListGroup.QrDanhmuc.FieldByName('UID').AsInteger;
    Application.CreateForm(TFrmAddList, FrmAddList);
    with FrmAddList.GetList(mUID, True) do
    begin
        for i := 0 to Count - 1 do
        begin
            s := Strings[i];
            with QrList do
            if not Locate('MEMBER', s, []) then
                begin
                    begin
                        Append;
                        FieldByName('GROUP').AsInteger := mUID;
                        FieldByName('MEMBER').AsString := s;
                        Post;
                    end;
                end;
        end;
        Free;
    end;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.CmdOKExecute(Sender: TObject);
begin
    with FrmListGroup.QrDanhmuc do
    begin
        CheckBrowseMode;
        with QrList do
        	if Active then
            begin
		        CheckBrowseMode;
	            UpdateBatch;
            end;

        if mIsNew then
        begin
            // Prepare for continuing Create Group
            Append;
            FieldByName('IS_GROUP').AsBoolean := True;
            EdUser.SetFocus;
        end
        else
            ModalResult := mrOK;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TFrmGroup.IsValidInput: Boolean;
begin
	Result := (Trim(EdUser.Text) <> '') and (Trim(EdName.Text) <> '');
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
    n: Integer;
begin
	CmdOK.Enabled := IsValidInput;

    n := PgMain.ActivePageIndex;
    CmdAdd.Enabled := n = 1;

    with QrList do
    begin
    	if not Active then
        	Exit;
	    CmdRemove.Enabled := (not IsEmpty) and (n = 1);
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.FormKeyPress(Sender: TObject; var Key: Char);
begin
	Enter2Tab(Self, Key);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.QrListBeforeOpen(DataSet: TDataSet);
begin
    QrList.Parameters[0].Value :=
        FrmListGroup.QrDanhmuc.FieldByName('UID').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.QrListCalcFields(DataSet: TDataSet);
begin
//	with DataSet do
//    	if FieldByName('IS_GROUP').AsBoolean then
//			FieldByName('IDX').AsInteger := 1
//        else if FieldByName('MEMBER').AsInteger = sysAdminId then
//		    	FieldByName('IDX').AsInteger := 9
//        	else
//	    		FieldByName('IDX').AsInteger := 8
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmGroup.PgMainChanging(Sender: TObject;
  var AllowChange: Boolean);
begin
	AllowChange := IsValidInput;
end;

end.
