﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit DataAccess;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ExtCtrls, Grids, Db, ADODB, Menus, Wwdbigrd, Wwdbgrid2, ActnList,
  Wwdbgrid, AppEvnts, AdvMenus;

type
  TFrmDataAccess = class(TForm)
    PgMain: TPageControl;
    TsUser: TTabSheet;
    TsFunc: TTabSheet;
    Bevel3: TBevel;
    QrRightByUser: TADOQuery;
    DsRightByUser: TDataSource;
    DsRightByData: TDataSource;
    QrRightByData: TADOQuery;
    QrData: TADOQuery;
    DsData: TDataSource;
    QrUser: TADOQuery;
    DsUser: TDataSource;
    GrByUser: TwwDBGrid2;
    Panel2: TPanel;
    GrDataRight: TwwDBGrid2;
    ActionList1: TActionList;
    CmdSelectAll: TAction;
    CmdUnSelectAll: TAction;
    PopRights: TAdvPopupMenu;
    CmdSearch: TAction;
    CmdClose: TAction;
    CmdShowPopup: TAction;
    StatusBar: TStatusBar;
    QrUserIMG: TIntegerField;
    QrRightByDataRIGHT: TIntegerField;
    QrRightByDataXRIGHT: TWideStringField;
    Panel4: TPanel;
    SYNC_DATA_RIGHTS: TADOCommand;
    QrRightByDataFULLNAME: TWideStringField;
    QrUserIS_GROUP: TBooleanField;
    QrUserUID: TIntegerField;
    QrRightByDataUID: TIntegerField;
    QrRightByDataIS_GROUP: TBooleanField;
    QrRightByDataIMG: TIntegerField;
    GrByData: TwwDBGrid2;
    GrUserRight: TwwDBGrid2;
    Bevel5: TBevel;
    QrRightByUserUID: TIntegerField;
    QrRightByUserRIGHT: TIntegerField;
    ApplicationEvents1: TApplicationEvents;
    Khngchnttc1: TMenuItem;
    Chnttc1: TMenuItem;
    N1: TMenuItem;
    ItemFull: TMenuItem;
    ItemNone: TMenuItem;
    QrUserUSERNAME: TWideStringField;
    QrUserFULLNAME: TWideStringField;
    QrRightByDataDATA_CODE: TWideStringField;
    QrRightByUserDATA_CODE: TWideStringField;
    QrRightByUserDESCRIPTION: TWideStringField;
    QrRightByUserXRIGHT: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrRightByUserCalcFields(DataSet: TDataSet);
    procedure ItemNoneClick(Sender: TObject);
    procedure TsFuncEnter(Sender: TObject);
    procedure TsUserEnter(Sender: TObject);
    procedure PgMainChange(Sender: TObject);
    procedure QrRightByUserNewRecord(DataSet: TDataSet);
    procedure CmdSelectAllExecute(Sender: TObject);
    procedure CmdUnSelectAllExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrByUserUpdateFooter(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure QrUserCalcFields(DataSet: TDataSet);
    procedure QrRightByDataBeforeOpen(DataSet: TDataSet);
    procedure ApplicationEvents1Hint(Sender: TObject);
    procedure GrByDataCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
  private
	mQuery: TADOQuery;
    mGrid: TwwDBGrid2;
    mUID: Integer;
  public
	procedure Execute(UID: Integer);
  end;

var
  FrmDataAccess: TFrmDataAccess;

implementation

uses
	isLib, Rights, isDb, isMsg, ExAdminCommon, AdminData, isCommon;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.Execute;
begin
	mUID := UID;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	try
		CloseDataSets([QrUser, QrData, QrRightByUser, QrRightByData]);
    except
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.FormShow(Sender: TObject);
begin
	// Synchro rights
	Wait(PREPARING);
	SYNC_DATA_RIGHTS.Execute;

    with QrData do
    	SQL.Text := Format(SQL.Text, [Iif(sysEnglish, 'DESCRIPTION1', 'DESCRIPTION')]);

    // Open database
	OpenDataSets([QrUser, QrData, QrRightByUser, QrRightByData]);

	// Done
    QrData.First;

    with QrUser do
    	if not Locate('UID', mUID, []) then
        	First;

    ClearWait;
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_CALC = 'Toàn quyền';

procedure TFrmDataAccess.QrRightByUserCalcFields(DataSet: TDataSet);
var
	r: DWORD;
    s: String;
begin
	with Dataset do
	begin
		s := '';
        r := FieldByName('RIGHT').AsInteger;
        if (r and R_EDIT) <> 0 then
                s := RS_CALC;

        FieldByName('XRIGHT').AsString := s;
    end;

    if DataSet.Name = QrRightByData.Name then
    begin
		{$I IMGCal}
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.ItemNoneClick(Sender: TObject);
var
    r : DWORD;
    n, i : Integer;
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	mQuery := QrRightByData;
        mGrid := GrUserRight;
    end
    else
    begin
    	mQuery := QrRightByUser;
        mGrid := GrDataRight;
    end;

    if (mQuery = Nil) and (not mGrid.Focused) then
    	Abort;

    with mQuery do
    begin
    	DisableControls;
    	n := mGrid.SelectedList.Count - 1;
        for i := 0 to n do
        begin
        	try
            	mQuery.GotoBookMark(TBookMark(mGrid.SelectedList.Items[i]));
                r := FieldByName('RIGHT').AsInteger;

                case (Sender as TMenuItem).Tag of
                0:
                    r := R_DENY;
                1:
                    r := R_EDIT;
                end;

                if FieldByName('RIGHT').AsInteger <> r then
                begin
	                Edit;
    	            FieldByName('RIGHT').AsInteger := r;
                    Post;
                end;
            except;
            end;
        end;
    	EnableControls;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.TsFuncEnter(Sender: TObject);
begin
	GrByData.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.TsUserEnter(Sender: TObject);
begin
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
       	with QrRightByUser do
        begin
        	CheckBrowseMode;
			Close;
			Open;
        end;
    1:
    	with QrRightByData do
		begin
        	CheckBrowseMode;
			Close;
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.QrRightByUserNewRecord(DataSet: TDataSet);
begin
	DataSet.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.CmdSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrDataRight.SelectAll;
    1:
    	GrUserRight.SelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.CmdUnSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrDataRight.UnSelectAll;
    1:
    	GrUserRight.UnSelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsUser);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.GrByDataCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
	if Highlight then
        Exit;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.GrByUserUpdateFooter(Sender: TObject);
begin
	GrByUser.Columns[0].FooterValue := ' >> ' + QrUser.FieldByName('USERNAME').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.QrUserCalcFields(DataSet: TDataSet);
begin
	{$I IMGCal}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.QrRightByDataBeforeOpen(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    	Parameters[1].Value := sysAdminId;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmDataAccess.ApplicationEvents1Hint(Sender: TObject);
begin
    StatusBar.Panels[0].Text := Application.Hint;
end;

end.
