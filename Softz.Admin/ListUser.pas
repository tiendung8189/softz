﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ListUser;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ActnList, Grids, Db,
  Wwdbgrid2, ADODb, Menus, AdvMenus,
  AppEvnts, ToolWin, Wwdbigrd, Wwdbgrid;

type
  TFrmListUser = class(TForm)
    ActionList: TActionList;
    CmdNew: TAction;
    CmdDel: TAction;
    CmdClose: TAction;
    Status: TStatusBar;
    GrList: TwwDBGrid2;
    ToolBar1: TToolBar;
    ToolButton1: TToolButton;
    ToolButton6: TToolButton;
    ToolButton7: TToolButton;
    ToolButton8: TToolButton;
    ToolButton11: TToolButton;
    QrDanhmuc: TADOQuery;
    DsDanhmuc: TDataSource;
    PopSort: TAdvPopupMenu;
    Tmmutin1: TMenuItem;
    CmdSearch: TAction;
    ApplicationEvents1: TApplicationEvents;
    QrDanhmucUSERNAME: TWideStringField;
    QrDanhmucFULLNAME: TWideStringField;
    QrDanhmucBRIEFNAME: TWideStringField;
    QrDanhmucEMAIL: TWideStringField;
    QrDanhmucPASSWD: TWideStringField;
    QrDanhmucIS_GROUP: TBooleanField;
    QrDanhmucDISABLED: TBooleanField;
    QrDanhmucMUST_CHG_PWD: TBooleanField;
    QrDanhmucPWD_NEVER: TBooleanField;
    QrDanhmucLAST_PWD_CHG: TDateTimeField;
    CmdEdit: TAction;
    ToolButton2: TToolButton;
    ToolButton3: TToolButton;
    QrDanhmucUID: TIntegerField;
    CmdDisable: TAction;
    CmdReload: TAction;
    N1: TMenuItem;
    mnObsolete: TMenuItem;
    QrDanhmucIs_Employee: TBooleanField;
    QrDanhmucManv: TWideStringField;
    QrDanhmucIMG: TIntegerField;
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdNewExecute(Sender: TObject);
    procedure CmdDelExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure GrListCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure ActionListUpdate(Action: TBasicAction;
      var Handled: Boolean);
    procedure FormCloseQuery(Sender: TObject; var CanClose: Boolean);
    procedure QrDanhmucBeforePost(DataSet: TDataSet);
    procedure QrDanhmucBeforeDelete(DataSet: TDataSet);
    procedure QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
      var Action: TDataAction);
    procedure QrDanhmucBeforeInsert(DataSet: TDataSet);
    procedure CmdSearchExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure ApplicationEvents1Idle(Sender: TObject; var Done: Boolean);
    procedure CmdEditExecute(Sender: TObject);
    procedure QrDanhmucAfterPost(DataSet: TDataSet);
    procedure QrDanhmucAfterInsert(DataSet: TDataSet);
    procedure CmdDisableExecute(Sender: TObject);
    procedure CmdReloadExecute(Sender: TObject);
    procedure PopSortPopup(Sender: TObject);
    procedure QrDanhmucUSERNAMEChange(Sender: TField);
    procedure QrDanhmucCalcFields(DataSet: TDataSet);
  private
  	mCanEdit, mInsert, mObsolete: Boolean;
    mSQL: String;
  public
  	procedure Execute(r: WORD);
  end;

var
  FrmListUser: TFrmListUser;

implementation

uses
	MainData, MasterData, AdminData, isDb, isMsg, ExAdminCommon, Rights, isLib, isCommon,
    Users, isType;

{$R *.DFM}

const
	FORM_CODE = 'LIST_USER';

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.Execute (r : WORD);
begin
	mCanEdit := rCanEdit (r);
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.FormCreate(Sender: TObject);
begin
	TMyForm(Self).Init2;
    SetCustomGrid(FORM_CODE, GrList);
    SetDictionary(QrDanhmuc, FORM_CODE);

    mObsolete := False;
    mTrigger := False;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	HideAudit;
    try
	    QrDanhmuc.Close;
    finally
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.FormShow(Sender: TObject);
begin
	GrList.ReadOnly := not mCanEdit;
    mSQL := QrDanhmuc.SQL.Text;

	CmdReload.Execute;
    GrList.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.FormCloseQuery(Sender: TObject; var CanClose: Boolean);
begin
   	CanClose := CheckBrowseDataset(QrDanhmuc, True);
end;

	(*
    ** Actions
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdNewExecute(Sender: TObject);
begin
    if not sysIsCentral then
        Abort;

	Application.CreateForm(TFrmUser, FrmUser);
    FrmUser.Execute(True);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdReloadExecute(Sender: TObject);
var
    sSQL: String;
begin
    sSQL := mSQL;
	with QrDanhmuc do
    begin
    	Close;
        if not mObsolete then
            sSQL := sSQL + ' and (isnull([DISABLED], 0) = 0)';
        SQL.Text := sSQL;
        SQL.Add(' order by 	USERNAME');
        Open;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdDelExecute(Sender: TObject);
var
    n: Integer;
begin
    with QrDanhmuc do
    begin
        n := FieldByName('UID').AsInteger;
        DataAdmin.CleanupGroup(n);
	    Delete;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdDisableExecute(Sender: TObject);
begin
    mObsolete := not mObsolete;
    CmdReload.Execute;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdEditExecute(Sender: TObject);
begin
    Application.CreateForm(TFrmUser, FrmUser);
    FrmUser.Execute(False);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.CmdSearchExecute(Sender: TObject);
begin
	exSearch(Name, DsDanhmuc);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.ActionListUpdate(Action: TBasicAction;
  var Handled: Boolean);
var
	bReserve: Boolean;
	s: String;
begin
	with QrDanhmuc do
    begin
    	if not Active then
        	Exit;
		s := FieldByName('USERNAME').AsString;
    end;

	bReserve := (s = IS_USER_ADMIN) or (s = IS_GROUP_EVERYONE);
    CmdNew.Enabled := sysIsCentral;
    CmdDel.Enabled :=  not bReserve;
end;

	(*
    ** Database
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucBeforePost(DataSet: TDataSet);
begin
	with QrDanhmuc do
    begin
        if BlankConfirm(QrDanhmuc, ['USERNAME', 'FULLNAME']) then
    		Abort;

        if (State in [dsInsert]) and not FieldByName('IS_GROUP').AsBoolean then
        	mInsert := True
        else
        	mInsert := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucCalcFields(DataSet: TDataSet);
begin
    {$I IMGCal}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucAfterInsert(DataSet: TDataSet);
begin
    with QrDanhmuc do
    begin
        FieldByName('UID').AsInteger := NewUID;
        FieldByName('DISABLED').AsBoolean := False;
        FieldByName('MUST_CHG_PWD').AsBoolean := False;
        FieldByName('PWD_NEVER').AsBoolean := True;
        FieldByName('Is_Employee').AsBoolean := False;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucAfterPost(DataSet: TDataSet);
begin
//    if mInsert then
//	    with Data.DEFAULT_EVERYONE do
//    	begin
//        	Parameters[0].Value := sysEveryoneId;
//	        Parameters[1].Value := QrDanhmuc.FieldByName('UID').AsInteger;
//    	    Execute;
//	    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucBeforeDelete(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;

	if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucPostError(DataSet: TDataSet; E: EDatabaseError;
  var Action: TDataAction);
begin
	Action := DbeMsg
end;

procedure TFrmListUser.QrDanhmucUSERNAMEChange(Sender: TField);
var
    n: Integer;
	mMa, s: String;
begin
    s := Sender.AsString;

	// Strip ký tự lạ
    mMa := StripInvalidChar(s);
    if s <> mMa then
    	Sender.AsString := mMa;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.QrDanhmucBeforeInsert(DataSet: TDataSet);
begin
	if not mCanEdit then
    	Abort;
end;

	(*
    ** Others
    *)
(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.GrListCalcCellColors(Sender: TObject; Field: TField;
  State: TGridDrawState; Highlight: Boolean; AFont: TFont; ABrush: TBrush);
begin
    if Highlight then
        Exit;

    with QrDanhmuc do
    begin
        if FieldByName('USERNAME').AsString = IS_USER_ADMIN then
            AFont.Style := [fsBold];

        if FieldByName('DISABLED').AsBoolean then
            AFont.Color := clRed;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.PopSortPopup(Sender: TObject);
begin
    mnObsolete.Checked := mObsolete;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmListUser.ApplicationEvents1Idle(Sender: TObject;
  var Done: Boolean);
begin
	Status.SimpleText := RecordCount(QrDanhmuc);
end;

end.
