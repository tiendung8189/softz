﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit FunctionAccess;

interface

uses
  SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ExtCtrls, Grids, Db, ADODB,
  Menus, Wwdbigrd, Wwdbgrid, ActnList, wwDBGrid2, AppEvnts,
  AdvMenus;

type
  TFrmFuncAccess = class(TForm)
    PgMain: TPageControl;
    TsUser: TTabSheet;
    TsFunc: TTabSheet;
    Bevel3: TSplitter;
    Bevel5: TSplitter;
    Panel3: TPanel;
    QrRightByUser: TADOQuery;
    DsRightByUser: TDataSource;
    DsRightByFunc: TDataSource;
    QrRightByFunc: TADOQuery;
    PgFunc: TPageControl;
    QrFunc: TADOQuery;
    DsFunc: TDataSource;
    QrUser: TADOQuery;
    DsUser: TDataSource;
    GrByUser: TwwDBGrid2;
    GrByFunc: TwwDBGrid2;
    GrUserRight: TwwDBGrid2;
    Panel2: TPanel;
    PgUserFunc: TPageControl;
    GrFunctionRight: TwwDBGrid2;
    QrRightByUserFUNC_CODE: TStringField;
    QrRightByUserXRIGHTS: TWideStringField;
    QrRightByFuncFUNC_CODE: TStringField;
    QrRightByFuncFULLNAME: TWideStringField;
    QrRightByFuncDESCRIPTION: TWideStringField;
    QrRightByFuncXRIGHTS: TWideStringField;
    ActionList1: TActionList;
    CmdSelectAll: TAction;
    CmdUnSelectAll: TAction;
    PopRights: TAdvPopupMenu;
    CmdSearch: TAction;
    CmdClose: TAction;
    SYNC_RIGHTS: TADOCommand;
    CmdShowPopup: TAction;
    StatusBar: TStatusBar;
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel4: TBevel;
    QrUserIDX: TIntegerField;
    QrRightByFuncIDX: TIntegerField;
    QrUserIS_GROUP: TBooleanField;
    QrRightByUserUID: TIntegerField;
    QrRightByUserRIGHT: TIntegerField;
    QrUserUID: TIntegerField;
    QrRightByFuncUID: TIntegerField;
    QrRightByFuncRIGHT: TIntegerField;
    QrRightByFuncIS_GROUP: TBooleanField;
    QrRightByUserCAT: TIntegerField;
    QrRightByFuncCAT: TIntegerField;
    QrRightByUserUSERNAME: TWideStringField;
    QrRightByUserACTION_TYPE: TIntegerField;
    QrRightByFuncACTION_TYPE: TIntegerField;
    QrRightByFuncUSERNAME: TWideStringField;
    ApplicationEvents1: TApplicationEvents;
    Khngchnttc1: TMenuItem;
    Chnttc1: TMenuItem;
    N1: TMenuItem;
    ItemFull: TMenuItem;
    ItemRead: TMenuItem;
    ItemNone: TMenuItem;
    QrUserUSERNAME: TWideStringField;
    QrUserFULLNAME: TWideStringField;
    QrRightByUserDESCRIPTION: TWideStringField;
    QrUserCALC_DISPLAYNAME: TWideStringField;
    ItemEdit: TMenuItem;
    ItemCreate: TMenuItem;
    QrCat: TADOQuery;
    QrRightByUserDENY_FA: TBooleanField;
    QrFuncFUNC_CODE: TWideStringField;
    QrFuncCAT: TIntegerField;
    QrFuncSORT: TIntegerField;
    QrFuncDESCRIPTION: TWideStringField;
    QrFuncDESCRIPTION1: TWideStringField;
    QrFuncPARENT: TWideStringField;
    QrFuncFORM: TIntegerField;
    QrFuncACTION_TYPE: TIntegerField;
    QrFuncENABLED: TBooleanField;
    QrFuncDENY_FA: TBooleanField;
    QrFuncTDESC: TWideStringField;
    QrFuncDENY_CENTRAL: TBooleanField;
    QrFuncDENY_BRANCH: TBooleanField;
    QrRightByUserDENY_CENTRAL: TBooleanField;
    QrRightByUserDENY_BRANCH: TBooleanField;
    QrRightByFuncDENY_CENTRAL: TBooleanField;
    QrRightByFuncDENY_BRANCH: TBooleanField;
    QrFuncFUNC_CAT: TWideStringField;
    QrRightByFuncFUNC_CAT: TWideStringField;
    QrRightByUserFUNC_CAT: TWideStringField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrRightByUserFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure PgUserFuncChange(Sender: TObject);
    procedure PgFuncChange(Sender: TObject);
    procedure QrFuncFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QrRightByUserCalcFields(DataSet: TDataSet);
    procedure ItemNoneClick(Sender: TObject);
    procedure QrRightByFuncFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TsFuncEnter(Sender: TObject);
    procedure TsUserEnter(Sender: TObject);
    procedure GrByFuncCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure PgMainChange(Sender: TObject);
    procedure QrRightByUserNewRecord(DataSet: TDataSet);
    procedure CmdSelectAllExecute(Sender: TObject);
    procedure CmdUnSelectAllExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure GrByUserUpdateFooter(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure CmdShowPopupExecute(Sender: TObject);
    procedure QrUserCalcFields(DataSet: TDataSet);
    procedure QrRightByFuncBeforeOpen(DataSet: TDataSet);
    procedure ApplicationEvents1Hint(Sender: TObject);
  private
    mUserFuncTab, mFuncUserTab: string;
	mQuery: TADOQuery;
    mGrid: TwwDBGrid2;
  public
	procedure Execute();
  end;

var
  FrmFuncAccess: TFrmFuncAccess;

implementation

uses
	isLib, Rights, isDb, isMsg, ExAdminCommon, isCommon, MainData, AdminData;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.Execute;
begin
	ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	try
		CloseDataSets([QrUser, QrFunc, QrRightByUser, QrRightByFunc]);
    except
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.FormShow(Sender: TObject);
var
    i: Integer;
    txt: TStringList;
    key, s: String;

    procedure CreateTab(pPageCtrl: TPageControl; pCaption, pKey: string);
    begin
        with TTabSheet.Create(Self) do
        begin
            Parent := pPageCtrl;
            PageControl := pPageCtrl;
            HelpKeyword := pKey;
            Caption := pCaption;
        end;
    end;

begin

	// Synchro rights
	Wait(PREPARING);
	SYNC_RIGHTS.Execute;

    // Open database
    with QrFunc do
    	SQL.Text := Format(SQL.Text, [Iif(sysEnglish, 'DESCRIPTION1', 'DESCRIPTION')]);
	OpenDataSets([QrCat, QrUser, QrFunc, QrRightByUser, QrRightByFunc]);

    // Create groups
    txt := TStringList.Create;
    txt.Text := exReadFuncCat;

    for i := 0 to txt.Count - 1 do
    begin
        mFuncUserTab := txt.Names[i];
        with QrCat do
        if (not IsEmpty) and (mFuncUserTab <> '')
            and Locate('FUNC_CAT', mFuncUserTab, []) then
        begin
            s := txt.ValueFromIndex[i];
            CreateTab(PgFunc, s, mFuncUserTab);
            CreateTab(PgUserFunc, s, mFuncUserTab);
        end
    end;

    ClearWait;
    txt.Free;

    if PgFunc.PageCount = 0 then
        key := ''
    else
   	    key := PgFunc.Pages[0].HelpKeyword;

    mUserFuncTab := key;
    mFuncUserTab := key;
    QrFunc.First;
    QrUser.First;

	PgUserFunc.OnChange(Nil);
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    ResetRowColor([GrByUser, GrByUser, GrByFunc, GrFunctionRight, GrUserRight]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrRightByUserFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	Accept := DataSet.FieldByName('FUNC_CAT').AsString = mUserFuncTab;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.PgUserFuncChange(Sender: TObject);
begin
	mUserFuncTab := PgUserFunc.ActivePage.HelpKeyword;
    GrFunctionRight.UnselectAll;
	QrRightByUser.First;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.PgFuncChange(Sender: TObject);
begin
	mFuncUserTab := PgFunc.ActivePage.HelpKeyword;
    QrFunc.First;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrFuncFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	Accept := QrFunc.FieldByName('FUNC_CAT').AsString = mFuncUserTab;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
    RS_READ     = 'Chỉ xem';
    RS_CREATE   = 'Thêm';
    RS_EDIT     = 'Sửa';
    RS_FULL     = 'Toàn quyền';

procedure TFrmFuncAccess.QrRightByUserCalcFields(DataSet: TDataSet);
var
    s: String;
begin
	with Dataset do
	begin
		s := '';
        if FieldByName('ACTION_TYPE').AsInteger <> 0 then
            case FieldByName('RIGHT').AsInteger of
            R_FULL:
                s := RS_FULL;
			R_EDIT:
                s := RS_EDIT;
//            R_CREATE:
//                s := RS_CREATE;
			R_READ:
                s := RS_READ;
            end;
        FieldByName('XRIGHTS').AsString := s;
    end;

    if DataSet.Name = QrRightByFunc.Name then
    begin
        {$I IMGCal}
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.ItemNoneClick(Sender: TObject);
var
    r: WORD;
    n, i: Integer;
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	mQuery := QrRightByFunc;
        mGrid := GrUserRight;
    end
    else
    begin
    	mQuery := QrRightByUser;
        mGrid := GrFunctionRight;
    end;

    if (mQuery = Nil) and (not mGrid.Focused) then
    	Abort;

    Screen.Cursor := crSQLWait;
    with mQuery do
    begin
    	DisableControls;
    	n := mGrid.SelectedList.Count - 1;
        for i := 0 to n do
        begin
        	try
            	GotoBookMark(TBookMark(mGrid.SelectedList.Items[i]));
   	            r := FieldByName('RIGHT').AsInteger;

                case (Sender as TMenuItem).Tag of
                0:
                    r := R_DENY;
                1:	// Xem
                    r := R_READ;
                2:	// Thêm
                    r := R_CREATE;
                4:	// Sửa
                    r := R_EDIT;
                31:	// Toàn quyền
                begin
                    r := R_FULL;
                end;
                end;

                if FieldByName('RIGHT').AsInteger <> r then
                begin
	                Edit;
    	            FieldByName('RIGHT').AsInteger := r;
                    Post;
                end;
            except
           	end;
        end;
    	EnableControls;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrRightByFuncFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	Accept := QrFunc.FieldByName('ACTION_TYPE').AsInteger <> 0;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.TsFuncEnter(Sender: TObject);
begin
	GrByFunc.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.TsUserEnter(Sender: TObject);
begin
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.GrByFuncCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Field.DataSet.FieldByName('ACTION_TYPE').AsInteger = 0 then
    begin
    	AFont.Style := AFont.Style + [fsBold];
        if Highlight then
        	AFont.Color := clWhite
        else
        	AFont.Color := clBlue;
	end
    else begin
        if (sysIsCentral and Field.DataSet.FieldByName('DENY_CENTRAL').AsBoolean) or
            ((not sysIsCentral) and Field.DataSet.FieldByName('DENY_BRANCH').AsBoolean) then
        begin
            AFont.Style := AFont.Style + [fsItalic];
            if Highlight then
                AFont.Color := clWhite
            else
                AFont.Color := clSilver;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
       	with QrRightByUser do
        begin
        	CheckBrowseMode;
			Close;
			Open;
        end;
    1:
    	with QrRightByFunc do
		begin
        	CheckBrowseMode;
			Close;
            Open;
        end;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrRightByUserNewRecord(DataSet: TDataSet);
begin
	DataSet.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.CmdSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrFunctionRight.SelectAll;
    1:
    	GrUserRight.SelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.CmdUnSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrFunctionRight.UnSelectAll;
    1:
    	GrUserRight.UnSelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsUser);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.GrByUserUpdateFooter(Sender: TObject);
begin
	GrByUser.Columns[0].FooterValue := ' >> ' + QrUser.FieldByName('USERNAME').AsString;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_NONE   = 'Cấm truy cập';
    RS_NONE_2 = 'Cấm truy cập';
    RS_NONE_3 = 'Cấm truy cập';

//    RS_FULL   = 'Toàn quyền';
    RS_FULL_2 = 'Toàn quyền';
    RS_FULL_3 = 'Toàn quyền';

procedure TFrmFuncAccess.CmdShowPopupExecute(Sender: TObject);
var
	act, i: Integer;
    b1, b2, b3, b4, bDeny: Boolean;
    	(*
		act:
            0: meanless
            1: normal form, 3 levels of rights
            2: manipulation
            3: receipt form, many levels of rights
        *)
begin
    // Quyền theo chức năng
	if PgMain.ActivePageIndex = 1 then
    begin
        bDeny := False;
		act := QrFunc.FieldByName('ACTION_TYPE').AsInteger;
//        bDeny := QrFunc.FieldByName('DENY_FA').AsBoolean;
        if sysIsCentral then
            bDeny := QrFunc.FieldByName('DENY_CENTRAL').AsBoolean
        else
            bDeny := QrFunc.FieldByName('DENY_BRANCH').AsBoolean;
        b1 := act = 1;
        b2 := act = 2;
        b3 := act = 4;
        b4 := act = 31;
    end
    else
    // Quyền theo tài khoản
    begin
    	with QrRightByUser, GrFunctionRight do
        begin
		    b1 := False;
		    b2 := False;
		    b3 := False;
            b4 := False;
            bDeny := False;

            for i := 0 to SelectedList.Count - 1 do
    	    begin
	        	GotoBookMark(TBookMark(SelectedList.Items[i]));
            	act := FieldByName('ACTION_TYPE').AsInteger;

		        b1 := b1 or (act = 1);
		        b2 := b2 or (act = 2);
		        b3 := b3 or (act = 4);
                b4 := b4 or (act = 31);

//                bDeny := bDeny or FieldByName('DENY_FA').AsBoolean;
                if sysIsCentral then
                    bDeny := bDeny or FieldByName('DENY_CENTRAL').AsBoolean
                else
                    bDeny := bDeny or FieldByName('DENY_BRANCH').AsBoolean;
	        end;
        end;
	end;

	if not(b1 or b2 or b3 or b4) then
		Abort;

	ItemRead.Visible := b1 or b3 or b4;
	ItemEdit.Visible := False; //b3 or b4;
    ItemCreate.Visible := False; //b4;

	ItemRead.Enabled := not b2;
	ItemEdit.Enabled := (b3 or b4) and (not b1) and (not b2);
    ItemCreate.Enabled := b4 and (not b1) and (not b2) and (not b3);
    ItemFull.Enabled := not bDeny;

    if not b2 then
    begin
    	ItemNone.Caption := RS_NONE;
        ItemFull.Caption := RS_FULL;
    end
    else if b1 or b3 or b4 then
    begin
    	ItemNone.Caption := RS_NONE_3;
        ItemFull.Caption := RS_FULL_3;
    end
    else
    begin
    	ItemNone.Caption := RS_NONE_2;
        ItemFull.Caption := RS_FULL_2;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrUserCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    	FieldByName('CALC_DISPLAYNAME').AsString :=
	    	FieldByName('FULLNAME').AsString + ' - [' +
    		FieldByName('USERNAME').AsString + ']';
    {$I IMGCal}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.QrRightByFuncBeforeOpen(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    begin
    	Parameters[1].Value := sysAdminId;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmFuncAccess.ApplicationEvents1Hint(Sender: TObject);
begin
	StatusBar.Panels[0].Text := Application.Hint
end;

end.
