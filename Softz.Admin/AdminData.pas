(*==============================================================================
**------------------------------------------------------------------------------
*)
unit AdminData;

interface

uses
  SysUtils, Classes,
  Db, ADODB, ImgList, Controls;

type
  TDataAdmin = class(TDataModule)
    CLEANUP_GROUP: TADOCommand;
    DEFAULT_EVERYONE: TADOCommand;
    QrTEMP: TADOQuery;
    ImageMed: TImageList;
    SYS_COPY_RIGHTS: TADOCommand;
    SYS_COPY_REP_RIGHTS: TADOCommand;
    SYS_COPY_DATA_RIGHTS: TADOCommand;
    QrUser: TADOQuery;
    QrUserUSERNAME: TWideStringField;
    QrUserFULLNAME: TWideStringField;
    QrUserBRIEFNAME: TWideStringField;
    QrUserIS_GROUP: TBooleanField;
    QrUserDISABLED: TBooleanField;
    QrUserUID: TAutoIncField;
    QrUserIDX: TIntegerField;
    QrUserPASSWD: TStringField;
    QrUserEMAIL: TWideStringField;
    QrUserMUST_CHG_PWD: TBooleanField;
    QrUserPWD_NEVER: TBooleanField;
    ImageUser: TImageList;
    procedure QrUserBeforeDelete(DataSet: TDataSet);
    procedure DataModuleCreate(Sender: TObject);
    procedure QrUserCalcFields(DataSet: TDataSet);
  private
	mInsert: Boolean;
  public
    procedure Initial;
    procedure CleanupGroup(pUID: Integer);

    function CopyRightFunc(pUID: Integer; pFrom: String): Boolean;
    function CopyRightRep(pUID: Integer; pFrom: String): Boolean;
    function CopyRightData(pUID: Integer; pFrom: String): Boolean;
  end;

var
	DataAdmin: TDataAdmin;

implementation

uses
	isMsg, Variants, isDb, isType, ExAdminCommon, MainData, isLib;

{$R *.DFM}

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataAdmin.Initial;
begin
    // Open user and get ID
    with QrUser do
    begin
        Open;
        // Admin
        sysAdminId := Lookup('USERNAME', IS_USER_ADMIN, 'UID');
        // Everyone
        sysEveryoneId := 0;//Lookup('USERNAME', IS_GROUP_EVERYONE, 'UID');
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataAdmin.CopyRightData(pUID: Integer; pFrom: String): Boolean;
begin
    with SYS_COPY_DATA_RIGHTS do
    begin
        Prepared := True;
        Parameters[1].Value := pUID;
        Parameters[2].Value := pFrom;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataAdmin.CopyRightFunc(pUID: Integer; pFrom: String): Boolean;
begin
    with SYS_COPY_RIGHTS do
    begin
        Prepared := True;
        Parameters[1].Value := pUID;
        Parameters[2].Value := pFrom;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function TDataAdmin.CopyRightRep(pUID: Integer; pFrom: String): Boolean;
begin
    with SYS_COPY_REP_RIGHTS do
    begin
        Prepared := True;
        Parameters[1].Value := pUID;
        Parameters[2].Value := pFrom;
        Execute;
        Result := Parameters[0].Value = 0;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataAdmin.QrUserBeforeDelete(DataSet: TDataSet);
begin
    if not DeleteConfirm then
    	Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataAdmin.DataModuleCreate(Sender: TObject);
begin
    GetLocaleFormatSettings(0, fmt);
    fmt.DateSeparator := '/';
    fmt.ShortDateFormat := 'mm/dd/yyyy';
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataAdmin.CleanupGroup;
begin
    with CLEANUP_GROUP do
    begin
        Parameters[0].Value := pUID;
        Execute;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TDataAdmin.QrUserCalcFields(DataSet: TDataSet);
begin
//    {$I IMGCal}
end;

end.
