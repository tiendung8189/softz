﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ExAdminCommon;

interface

uses
	SysUtils, Forms, DB, ActnList, ADODb;
resourcestring
    RS_KEEP_OLD_RIGHT = 'Giữ lại các quyền cũ?';

const
    MY_ADMIN: String = 'ADMIN';
    MY_EVERYONE: String = 'Everyone';

var
	sysIni: String;
    sysAdminId, sysEveryoneId: Integer;
    sysEnglish, mTrigger: Boolean;
    fmt: TFormatSettings;

    function  exReadFuncCat(const sys: Integer = 0): String;
    function  exReadRepCat(const sys: Integer = 0): String;

    procedure exTextImport(DataSet: TDataSet; FileName, KeyName: String);
    function  exTextExport(DataSet: TDataSet; FileName: String): Integer;

    procedure exClearLog;

implementation

uses
    isStr, isLib, isDB, Classes, AdminData, isMsg, Variants, MainData;

const
	DELIMITER: String = ',';

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_CLEANUP = 'Đang dọn dẹp';

procedure exClearLog;
var
	cond: String;
    n: Integer;
begin
    n := 30;

    if n = 0 then
        Exit
    else
        cond := 'datediff(dd, TIME_START, getdate()) > ' + IntToStr(n);

    with TADOQuery.Create(Nil) do
    begin
      	Connection := DataMain.Conn;
        SQL.Text := 'select top 1 1 from SYS_LOG where ' + cond;
        Open;
        if not IsEmpty then
        begin
            Wait(RS_CLEANUP);
            DataMain.Conn.Execute('delete SYS_LOG where ' + cond);
            ClearWait;
        end;
        Close;
        Free;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exReadFuncCat(const sys: Integer): String;
var
	s: String;
begin
	s := Iif(sysEnglish, 'Function English Categories', 'Function Categories');
    if sys > 0 then
    	s := s + IntToStr(sys);
    Result := isStrReplace(FlexConfigString('Admin', s), ';', #13);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
function exReadRepCat(const sys: Integer): String;
var
	s: String;
begin
	s := Iif(sysEnglish, 'Report English Categories', 'Report Categories');
    if sys > 0 then
    	s := s + IntToStr(sys);
    Result := isStrReplace(FlexConfigString('Admin', s), ';', #13);
end;

(*==============================================================================
** Import text file
** DataSet	: Table
** FileName	: Text file
** KeyName	: Key name on table
** KeyCol	: Key column on text file
**------------------------------------------------------------------------------
*)
procedure exTextImport(DataSet: TDataSet; FileName, KeyName: String);
var
    ls,				// List of raw records
    hs,				// Header
    rs,				// Detail of record
    ks: TStrings;	// List of key values
    i, k, n: Integer;
    KeyCol: Integer;

    procedure QuoteStrip(ls: TStrings);
    var
    	i, n: Integer;
    begin
    	for i := 0 to ls.Count - 1 do
        begin
        	n := Length(ls[i]);
        	if n > 1 then
				ls[i] := Copy(ls[i], 2, n - 2);
		end;                
    end;

begin
    // Load file
    ls := TStringList.Create;
    ls.LoadFromFile(FileName, TEncoding.UTF8);
    rs := TStringList.Create;
    hs := TStringList.Create;
    ks := TStringList.Create;

    // Import
    MyBreakApart(ls[0], hs);	// Header
    QuoteStrip(hs);

    n := hs.Count - 1;
    KeyCol := hs.IndexOf(KeyName);

    for i := 1 to ls.Count - 1 do
    begin
        MyBreakApart(ls[i], rs);
	    QuoteStrip(rs);

        with DataSet do
        begin
            // Use to delete orphan records in table
            ks.Add(rs[KeyCol]);

        	if Locate(KeyName, rs[KeyCol], []) then
            	Edit
            else
	            Append;

            for k := 0 to n do
                with FieldByName(hs[k]) do
                    case DataType of
                    ftAutoInc:
                    	;
                    else
                        AsString := rs[k];
				end;
            Post;
        end;
	end;

    // Delete orphan records
    with DataSet do
    begin
        First;
		while not Eof do
            if ks.IndexOf(FieldByName(KeyName).AsString) < 0 then
                Delete
            else
        		Next;
        First;
    end;

    // Done
    ks.Free;
    ls.Free;
    rs.Free;
    hs.Free;
end;


(*==============================================================================
** Export ra Text file
**------------------------------------------------------------------------------
*)
function exTextExport(DataSet: TDataSet; FileName: String): Integer;
begin
    Result := TextExport(DataSet, FileName);
end;


(*==============================================================================
**------------------------------------------------------------------------------
*)
initialization
	sysIni := ChangeFileExt(Application.ExeName, '.ini');
end.

