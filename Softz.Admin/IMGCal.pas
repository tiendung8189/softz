	with DataSet do
    	if FieldByName('IS_GROUP').AsBoolean then
			FieldByName('IMG').AsInteger := 1
        else if FieldByName('UID').AsInteger = sysAdminId then
		    	FieldByName('IMG').AsInteger := 2
        	else
	    		FieldByName('IMG').AsInteger := 0