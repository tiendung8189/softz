﻿(*==============================================================================
**------------------------------------------------------------------------------
*)
unit ReportAccess;

interface

uses
  Windows, SysUtils, Classes, Graphics, Controls, Forms,
  ComCtrls, ExtCtrls, Grids, Db, ADODB,
  Menus, Wwdbigrd, Wwdbgrid, ActnList, wwDBGrid2, AppEvnts,
  AdvMenus;

type
  TFrmRepAccess = class(TForm)
    PgMain: TPageControl;
    TsUser: TTabSheet;
    TsFunc: TTabSheet;
    Bevel3: TSplitter;
    Bevel5: TSplitter;
    Panel3: TPanel;
    QrRightByUser: TADOQuery;
    DsRightByUser: TDataSource;
    DsRightByRep: TDataSource;
    PgRep: TPageControl;
    QrRep: TADOQuery;
    DsRep: TDataSource;
    QrUser: TADOQuery;
    DsUser: TDataSource;
    GrByUser: TwwDBGrid2;
    GrByRep: TwwDBGrid2;
    GrUserRight: TwwDBGrid2;
    Panel2: TPanel;
    PgUserRep: TPageControl;
    GrRepRight: TwwDBGrid2;
    QrRightByUserXRIGHTS: TWideStringField;
    ActionList1: TActionList;
    CmdSelectAll: TAction;
    CmdUnSelectAll: TAction;
    PopRights: TAdvPopupMenu;
    CmdSearch: TAction;
    CmdClose: TAction;
    CmdShowPopup: TAction;
    QrRepSYS: TBooleanField;
    QrRightByUserSys: TBooleanField;
    QrRightByRep: TADOQuery;
    QrRightByRepXRIGHTS: TWideStringField;
    QrRightByRepSYS: TBooleanField;
    StatusBar: TStatusBar;
    Bevel1: TBevel;
    Bevel2: TBevel;
    SYNC_REP_RIGHTS: TADOCommand;
    QrRepTDESC: TWideStringField;
    QrUserIDX: TIntegerField;
    QrRightByRepIDX: TIntegerField;
    QrUserIS_GROUP: TBooleanField;
    QrUserUID: TIntegerField;
    QrRepCAT: TIntegerField;
    QrRepREP_NAME: TStringField;
    QrRepREP_CODE: TIntegerField;
    QrRightByUserCAT: TIntegerField;
    QrRightByUserREP_CODE: TIntegerField;
    QrRightByUserUID: TIntegerField;
    QrRightByUserRIGHT: TIntegerField;
    QrRightByRepREP_CODE: TIntegerField;
    QrRightByRepUID: TIntegerField;
    QrRightByRepRIGHT: TIntegerField;
    QrRightByRepUSERNAME: TWideStringField;
    QrRightByRepFULLNAME: TWideStringField;
    QrRightByRepIS_GROUP: TBooleanField;
    ApplicationEvents1: TApplicationEvents;
    Khngchnttc1: TMenuItem;
    Chnttc1: TMenuItem;
    N1: TMenuItem;
    FullAccess1: TMenuItem;
    ItemAssign: TMenuItem;
    QrUserUSERNAME: TWideStringField;
    QrUserFULLNAME: TWideStringField;
    QrRightByUserDESC: TWideStringField;
    QrCat: TADOQuery;
    QrUserCALC_DISPLAYNAME: TWideStringField;
    cin1: TMenuItem;
    onquyn1: TMenuItem;
    QrRightByUserIMG: TIntegerField;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure QrRightByUserFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure PgUserRepChange(Sender: TObject);
    procedure PgRepChange(Sender: TObject);
    procedure QrRepFilterRecord(DataSet: TDataSet; var Accept: Boolean);
    procedure QrRightByUserCalcFields(DataSet: TDataSet);
    procedure ItemAssignClick(Sender: TObject);
    procedure QrRightByRepFilterRecord(DataSet: TDataSet;
      var Accept: Boolean);
    procedure TsFuncEnter(Sender: TObject);
    procedure TsUserEnter(Sender: TObject);
    procedure GrByRepCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure PgMainChange(Sender: TObject);
    procedure QrRightByUserNewRecord(DataSet: TDataSet);
    procedure CmdSelectAllExecute(Sender: TObject);
    procedure CmdUnSelectAllExecute(Sender: TObject);
    procedure CmdSearchExecute(Sender: TObject);
    procedure CmdCloseExecute(Sender: TObject);
    procedure PopRightsPopup(Sender: TObject);
    procedure QrUserCalcFields(DataSet: TDataSet);
    procedure GrByUserCalcCellColors(Sender: TObject; Field: TField;
      State: TGridDrawState; Highlight: Boolean; AFont: TFont;
      ABrush: TBrush);
    procedure QrRightByRepBeforeOpen(DataSet: TDataSet);
    procedure ApplicationEvents1Hint(Sender: TObject);
  private
    mUserRepTab, mRepUserTab: Integer;
	mQuery: TADOQuery;
    mGrid: TwwDBGrid2;
    mUID: Integer;
  public
  	procedure Execute(UID: Integer);
  end;

var
  FrmRepAccess: TFrmRepAccess;

implementation

{$R *.DFM}

uses
	isLib, Rights, isDb, isMsg, ExAdminCommon, isCommon, MainData, AdminData;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.Execute;
begin
	mUID := UID;
    ShowModal;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.FormClose(Sender: TObject; var Action: TCloseAction);
begin
	try
		CloseDataSets([QrUser, QrRep, QrRightByUser, QrRightByRep]);
    except
    end;
	Action := caFree;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.FormShow(Sender: TObject);
var
	i: Integer;
    ls: TStrings;
    s: String;

    procedure CreateTab(pPageCtrl: TPageControl; const pCaption: string; const pTag: Integer);
    begin
        with TTabSheet.Create(Self) do
        begin
            Parent := pPageCtrl;
            PageControl := pPageCtrl;
            Tag := pTag;
            Caption := pCaption;
        end;
    end;
begin
	// Synchro rights
	Wait(PREPARING);
    SYNC_REP_RIGHTS.Execute;

	// Open database
    with QrRep do
    	SQL.Text := Format(SQL.Text, [Iif(sysEnglish, 'DESCRIPTION1', 'DESCRIPTION')]);
	OpenDataSets([QrUser, QrRep, QrRightByUser, QrRightByRep]);

    // Create groups
    ls := TStringList.Create;
    ls.Text := exReadRepCat;

    with QrRep do
    begin
        for i := 0 to ls.Count - 1 do
        begin
            mRepUserTab := StrToInt(ls.Names[i]);
            First;
            if not IsEmpty then
            begin
                s := ls.ValueFromIndex[i];
                CreateTab(PgRep, s, mRepUserTab);
                CreateTab(PgUserRep, s, mRepUserTab);
            end
        end;
    end;
    ls.Free;
    ClearWait;

    // Done
    if PgRep.PageCount = 0 then
        i := -1
    else
   	    i := PgRep.Pages[0].Tag;

    mUserRepTab := i;
    mRepUserTab := i;
    QrRep.First;

    with QrUser do
    	if not Locate('UID', mUID, []) then
        	First;
    PgMainChange(PgMain);
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.FormCreate(Sender: TObject);
begin
    TMyForm(Self).Init2;
    ResetRowColor([GrByUser, GrByUser, GrByRep, GrRepRight, GrUserRight]);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrRightByUserFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	Accept := QrRightByUser.FieldByName('CAT').AsInteger = mUserRepTab;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.PgUserRepChange(Sender: TObject);
begin
	mUserRepTab := PgUserRep.ActivePage.Tag;
    QrRightByUser.First;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.PgRepChange(Sender: TObject);
begin
	mRepUserTab := PgRep.ActivePage.Tag;
    QrRep.First;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrRepFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	Accept := QrRep.FieldByName('CAT').AsInteger = mRepUserTab;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
resourcestring
	RS_S1 = 'Chỉ xem';
	RS_S2 = 'Được in';
	RS_S3 = 'Toàn quyền';

procedure TFrmRepAccess.QrRightByUserCalcFields(DataSet: TDataSet);
var
	r: DWORD;
    s: String;
begin
	with Dataset do
	begin
		s := '';
        if not FieldByName('SYS').AsBoolean then
        begin
			r := FieldByName('RIGHT').AsInteger;
            if (r and R_READ) <> 0 then
            	s := RS_S1
            else if (r and R_PRINT) <> 0 then
            	s := RS_S2
            else if (r and R_REPORT) <> 0 then
            	s := RS_S3;
		end;
        FieldByName('XRIGHTS').AsString := s;
    end;
    if DataSet.Name = QrRightByRep.Name then
    begin
        {$I IMGCal}
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.ItemAssignClick(Sender: TObject);
var
    r: WORD;
	n, i: Integer;
begin
	if PgMain.ActivePageIndex = 1 then
    begin
    	mQuery := QrRightByRep;
        mGrid := GrUserRight;
    end
    else
    begin
    	mQuery := QrRightByUser;
        mGrid := GrRepRight;
    end;

    if (mQuery = Nil) and (not mGrid.Focused) then
    	Abort;

    Screen.Cursor := crSQLWait;
    with mQuery do
    begin
    	DisableControls;
    	n := mGrid.SelectedList.Count - 1;
        for i := 0 to n do
        begin
        	try
            	GotoBookMark(TBookMark(mGrid.SelectedList.Items[i]));
                r := FieldByName('RIGHT').AsInteger;
                case (Sender as TMenuItem).Tag of
                0:
                    r := R_DENY;
                1:
                    r := R_READ;
                2:
                    r := R_PRINT;
                4:
                    r := R_REPORT;
                end;

                if FieldByName('RIGHT').AsInteger <> r then
                begin
	                Edit;
    	            FieldByName('RIGHT').AsInteger := r;
        	        Post;
                end;
            except
            end;
        end;
    	EnableControls;
    end;
    Screen.Cursor := crDefault;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrRightByRepFilterRecord(DataSet: TDataSet;
  var Accept: Boolean);
begin
	with QrRightByRep do
    begin
    	if FieldByName('SYS').AsBoolean then
        	Accept := False
        else
			Accept :=
    			FieldByName('REP_CODE').AsString =
        	    	QrRep.FieldByName('REP_CODE').AsString;
 	end;

    {$I IMGCal}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.TsFuncEnter(Sender: TObject);
begin
	GrByRep.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.TsUserEnter(Sender: TObject);
begin
	GrByUser.SetFocus;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.GrByRepCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
    if Field.DataSet.FieldByName('SYS').AsBoolean then
    begin
    	AFont.Style := AFont.Style + [fsBold];
        if Highlight then
        	AFont.Color := clWhite
        else
        	AFont.Color := clBlue;
	end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.PgMainChange(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
       	with QrRightByUser do
        begin
        	CheckBrowseMode;
			Close;
			Open;
        end;
    1:
    	with QrRightByRep do
		begin
        	CheckBrowseMode;
			Close;
            Open;
        end;
    end;

end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrRightByUserNewRecord(DataSet: TDataSet);
begin
	DataSet.Cancel;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrUserCalcFields(DataSet: TDataSet);
begin
    with DataSet do
    	FieldByName('CALC_DISPLAYNAME').AsString :=
	    	FieldByName('FULLNAME').AsString + ' - [' +
    		FieldByName('USERNAME').AsString + ']';
    {$I IMGCal}
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.CmdSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrRepRight.SelectAll;
    1:
    	GrUserRight.SelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.CmdUnSelectAllExecute(Sender: TObject);
begin
    case PgMain.ActivePageIndex of
    0:
    	GrRepRight.UnSelectAll;
    1:
    	GrUserRight.UnSelectAll;
    end;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.CmdSearchExecute(Sender: TObject);
begin
	if PgMain.ActivePageIndex = 0 then
		exSearch(Name, DsUser);
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.CmdCloseExecute(Sender: TObject);
begin
	Close;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.PopRightsPopup(Sender: TObject);
var
	bSys: Boolean;
    n, i: Integer;
begin
	if PgMain.ActivePageIndex = 1 then
	begin
    	bSys := QrRep.FieldByName('SYS').AsBoolean;
        if bSys then
			Abort;
	end
    else
    begin
    	bSys := true;
    	with QrRightByUser, GrRepRight do
        begin
			n := SelectedList.Count - 1;
            for i := 0 to n do
    	    begin
				try
		        	GotoBookMark(TBookMark(SelectedList.Items[i]));
	            	bSys := FieldByName('SYS').AsBoolean;
                except
                	Abort;
                end;
        	    if not bSys then
    	            break;
	        end;
        end;
    end;
    
    if bSys then
        Abort;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.GrByUserCalcCellColors(Sender: TObject;
  Field: TField; State: TGridDrawState; Highlight: Boolean; AFont: TFont;
  ABrush: TBrush);
begin
	{
	if Field.DataSet.FieldByName('IS_GROUP').AsBoolean and (Field.FullName = 'FULLNAME') then
    	AFont.Style := [fsBold];
    }
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.QrRightByRepBeforeOpen(DataSet: TDataSet);
begin
	with DataSet as TADOQuery do
    	Parameters[1].Value := sysAdminId;
end;

(*==============================================================================
**------------------------------------------------------------------------------
*)
procedure TFrmRepAccess.ApplicationEvents1Hint(Sender: TObject);
begin
    StatusBar.Panels[0].Text := Application.Hint
end;

end.